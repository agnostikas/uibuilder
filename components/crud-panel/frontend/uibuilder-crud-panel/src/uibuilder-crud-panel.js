/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { IronResizableBehavior } from '@polymer/iron-resizable-behavior/iron-resizable-behavior.js';
import * as PolymerLegacy from '@polymer/polymer/lib/legacy/class.js'
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/uibuilder-i18n.js';
import '@vaadin/vaadin-split-layout/src/vaadin-split-layout.js';
import { UibuilderDetailPanel } from '@vaadin/flow-frontend/uibuilder-detail-panel/src/uibuilder-detail-panel.js';


window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.DEFAULT_MASTER_PERCENTAGE = window.Uibuilder.DEFAULT_MASTER_PERCENTAGE || 40;

/**
 * @typedef {string} CrudPanelOrientation
 * @enum {CrudPanelOrientation}
 * @readonly
 */
window.Uibuilder.CrudPanelOrientation = window.Uibuilder.CrudPanelOrientation || {
    Horizontal: 'horizontal',
    Vertical: 'vertical'
};

/**
 * @property {CrudPanelOrientation} orientation
 * @property {boolean} hasSeparator
 * @property {number} masterPercent
 */
class UibuilderCrudPanel extends ThemableMixin(Uibuilder.I18NBaseMixin('crud-panel',
    PolymerLegacy.mixinBehaviors([IronResizableBehavior], PolymerElement))) {

    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                }
            </style>

            <div part="crud-panel-container">
                <slot></slot>
            </div>
        `;
    }

    static get is() {
        return 'crud-panel';
    }

    static get properties() {
        return {
            orientation: {
                type: String,
                value: Uibuilder.CrudPanelOrientation.Horizontal,
                observer: '_onOrientationChange',
            },
            hasSeparator: {
                type: Boolean,
                observer: '_onHasSeparatorChange',
            },
            masterPercent: {
                type: Number,
                value: window.Uibuilder.DEFAULT_MASTER_PERCENTAGE,
                observer: '_masterPercentChanged'
            },
        };
    }

    ready() {
        super.ready();

        this._rebuildWrapperIfNeeded();
    }

    _onHasSeparatorChange() {
        this._rebuildWrapperIfNeeded();
    }

    _onOrientationChange() {
        const wrapper = this.querySelector('[part="wrapper"]');
        if (wrapper) {
            if (this.hasSeparator) {
                wrapper.setAttribute('orientation', this.orientation);
            } else {
                this._setFlexDirection(wrapper);
            }
            this._masterPercentChanged();
        }
    }

    _rebuildWrapperIfNeeded() {
        const wrapper = this.querySelector('[part="wrapper"]');
        const master = this.querySelector('[part="master"]');
        const detail = this._getDetailElement();
        const wrapperRebuildNeeded = !wrapper && master && detail ||
            wrapper && (
                !master ||
                !detail ||
                this.hasSeparator && !this.querySelector('vaadin-split-layout[part="wrapper"]') ||
                !this.hasSeparator && !this.querySelector('div[part="wrapper"]')
            );

        if (wrapperRebuildNeeded) {
            this._removeWrapper(wrapper, master, detail);

            if (master && detail)
                this._createNewWrapper(master, detail);
        }
    }

    /**
     * @protected
     * @return {HTMLElement}
     */
    _getDetailElement() {
        const detail = this.querySelector('[part="detail"]');
        return detail instanceof UibuilderDetailPanel ? detail : null;
    }

    _removeWrapper(wrapper, master, detail) {
        if (wrapper) {
            wrapper.parentElement.insertBefore(master, wrapper);
            wrapper.parentElement.insertBefore(detail, wrapper);
            wrapper.remove();
        }
    }

    _createNewWrapper(master, detail) {
        let wrapper;
        if (this.hasSeparator) {
            wrapper = document.createElement('vaadin-split-layout');
            wrapper.setAttribute('orientation', this.orientation);
        } else {
            wrapper = document.createElement('div');
            wrapper.style.display = 'flex';
            this._setFlexDirection(wrapper);
        }

        wrapper.setAttribute('part', 'wrapper');
        const divHtml = '<div style="min-width: 100px; display: flex; flex-direction: column; overflow: auto;"></div>';
        wrapper.innerHTML = divHtml + divHtml;

        master.parentElement.insertBefore(wrapper, master);
        setTimeout(() => {
            wrapper.firstElementChild.appendChild(master);
            wrapper.lastElementChild.appendChild(detail);
            setTimeout(() => this._masterPercentChanged());
        });
    }

    _setFlexDirection(wrapper) {
        wrapper.style.flexDirection = this.orientation === Uibuilder.CrudPanelOrientation.Vertical ? 'column' : 'row';
    }

    _masterPercentChanged() {
        const wrapper = this.querySelector('[part="wrapper"]');
        if (wrapper) {
            const masterPercent = typeof this.masterPercent === 'number' ? this.masterPercent : Uibuilder.DEFAULT_MASTER_PERCENTAGE;

            if (this.orientation === Uibuilder.CrudPanelOrientation.Vertical) {
                const sumHeight = wrapper.firstElementChild.offsetHeight + wrapper.lastElementChild.offsetHeight;
                wrapper.firstChild.style.flex = `1 1 ${sumHeight * masterPercent / 100}px`;
                wrapper.lastChild.style.flex = `1 1 ${sumHeight * (100 - masterPercent) / 100}px`;
            } else {
                wrapper.firstChild.style.flex = masterPercent;
                wrapper.lastChild.style.flex = 100 - masterPercent;
            }

            this.notifyResize();
        }
    }
}

customElements.define(UibuilderCrudPanel.is, UibuilderCrudPanel);
