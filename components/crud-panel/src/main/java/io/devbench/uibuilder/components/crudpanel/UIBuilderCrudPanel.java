/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.crudpanel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import io.devbench.uibuilder.api.components.HasItemType;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import io.devbench.uibuilder.api.crud.GenericCrudControllerBean;
import io.devbench.uibuilder.components.crudpanel.exception.CrudPanelCannotFindMasterDetailControllerException;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.core.utils.HtmlElementAwareComponent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.internal.StringUtil;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Provider;
import java.io.Serializable;
import java.util.Optional;

@Tag(UIBuilderCrudPanel.TAG_NAME)
@JsModule("./uibuilder-crud-panel/src/uibuilder-crud-panel.js")
public class UIBuilderCrudPanel<T extends Serializable> extends HasRawElementComponent implements HasComponents, HasElement, HasItemType {

    public static final String TAG_NAME = "crud-panel";

    private String masterDetailControllerId;
    private UIBuilderMasterDetailController<T> masterDetailController;
    private Class<T> itemType;

    @Inject
    private Provider<GenericCrudControllerBean<?>> genericCrudControllerBean;

    @PostConstruct
    @SuppressWarnings("unchecked")
    public void registerGenericInlineItemSaveConsumer() {
        genericCrudControllerBean.get().registerNestedInlineItemSaveHandler(memberCrudPanelId -> {
            if (memberCrudPanelId != null) {
                ElementCollector.getById(memberCrudPanelId)
                    .ifPresent(crudPanelComponent -> {
                        UIBuilderCrudPanel<T> memberCrudPanel = (UIBuilderCrudPanel<T>) crudPanelComponent.getComponent();
                        UIBuilderMasterDetailController<T> mdc = memberCrudPanel.getMasterDetailController();
                        if (mdc.isOnlyOneItemSelected()) {
                            mdc.getDetailComponent()
                                .filter(UIBuilderDetailPanel.class::isInstance)
                                .map(UIBuilderDetailPanel.class::cast)
                                .ifPresent(detailPanel -> {
                                    detailPanel.setItem(mdc.getSelectedItem());
                                    detailPanel.setInternalFormDirty();
                                });
                        } else {
                            mdc.findParentMdc().ifPresent(UIBuilderMasterDetailController::markDetailDirty);
                        }
                    });
            }
        });
    }

    protected String getMasterDetailControllerId() {
        return masterDetailControllerId;
    }

    public void setMasterDetailControllerId(String masterDetailControllerId) {
        this.masterDetailControllerId = masterDetailControllerId;
    }

    void registerGenericCrudData(@NotNull String datasourceName) {
        genericCrudControllerBean.get().registerGenericCrudData(getMasterDetailControllerId(), datasourceName);
    }

    @SuppressWarnings("unchecked")
    public UIBuilderMasterDetailController<T> getMasterDetailController() {
        if (masterDetailController == null) {
            synchronized (this) {
                if (masterDetailController == null) {
                    if (masterDetailControllerId == null) {
                        throw new CrudPanelCannotFindMasterDetailControllerException("Master detail controller's component ID is missing");
                    }
                    ElementCollector.getById(masterDetailControllerId).ifPresent(htmlElementAwareComponent -> {
                        Component component = htmlElementAwareComponent.getComponent();
                        if (component instanceof UIBuilderMasterDetailController) {
                            masterDetailController = (UIBuilderMasterDetailController<T>) component;
                        }
                    });
                    if (masterDetailController == null) {
                        throw new CrudPanelCannotFindMasterDetailControllerException();
                    }
                }
            }
        }
        return masterDetailController;
    }

    @Override
    public Class<T> getItemType() {
        return itemType;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setItemType(Class<?> itemType) {
        this.itemType = (Class<T>) itemType;
    }

    public static Class<?> findCrudClassType(String elementId) {
        if (StringUtil.isBlank(elementId)) {
            return null;
        }

        Optional<Component> foundComponent = ElementCollector
            .getById(elementId)
            .map(HtmlElementAwareComponent::getComponent);

        if (foundComponent.isPresent()) {
            Component component = foundComponent.get();

            if (component instanceof UIBuilderCrudPanel) {
                Class<?> itemType = getHasItemTypeComponentType(component);
                if (itemType != null) {
                    return itemType;
                } else {
                    String mdcId = component.getId().orElse(elementId) + "-mdc";
                    return findCrudClassType(mdcId);
                }
            } else if (component instanceof UIBuilderMasterDetailController) {
                Class<?> itemType = getHasItemTypeComponentType(component);
                if (itemType != null) {
                    return itemType;
                } else {
                    return findCrudClassType(((UIBuilderMasterDetailController<?>) component).getDetailId());
                }
            } else if (component instanceof HasItemType) {
                return getHasItemTypeComponentType(component);
            }
        }

        return null;
    }

    @Nullable
    private static Class<?> getHasItemTypeComponentType(Component component) {
        return component instanceof HasItemType ? ((HasItemType) component).getItemType() : null;
    }

}
