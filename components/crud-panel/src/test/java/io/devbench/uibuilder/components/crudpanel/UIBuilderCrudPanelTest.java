/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.crudpanel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.api.crud.GenericCrudControllerBean;
import io.devbench.uibuilder.components.crudpanel.exception.CrudPanelCannotFindMasterDetailControllerException;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.core.utils.HtmlElementAwareComponent;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.inject.Provider;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class UIBuilderCrudPanelTest {

    @InjectMocks
    private UIBuilderCrudPanel<String> crudPanel;

    @Mock
    private GenericCrudControllerBean<?> genericCrudControllerBean;

    @Mock
    private Provider<GenericCrudControllerBean<?>> genericCrudControllerBeanProvider;

    @BeforeEach
    void setUp() {
        doReturn(genericCrudControllerBean).when(genericCrudControllerBeanProvider).get();
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @DisplayName("should return mdc if the id is set and it exists")
    void test_should_return_mdc_if_the_id_is_set_and_it_exists() {
        crudPanel.setMasterDetailControllerId("mdc-id");
        assertEquals("mdc-id", crudPanel.getMasterDetailControllerId());

        UIBuilderMasterDetailController mdc = mock(UIBuilderMasterDetailController.class);

        HtmlElementAwareComponent heac = mock(HtmlElementAwareComponent.class);
        doReturn(mdc).when(heac).getComponent();
        Map<String, HtmlElementAwareComponent> elementCollectorMap = new HashMap<>();
        elementCollectorMap.put("mdc-id", heac);

        VaadinSession.getCurrent().setAttribute(ElementCollector.ELEMENT_COLLECTOR_COMPONENT_MAP, elementCollectorMap);

        UIBuilderMasterDetailController<String> masterDetailController = crudPanel.getMasterDetailController();
        assertNotNull(masterDetailController);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @DisplayName("should throw exception if the found mdc is not an mdc, or there is not a registered component with the id")
    void test_should_throw_exception_if_the_found_mdc_is_not_an_mdc_or_there_is_not_a_registered_component_with_the_id() {
        crudPanel.setMasterDetailControllerId("mdc-id");

        assertThrows(CrudPanelCannotFindMasterDetailControllerException.class, () -> crudPanel.getMasterDetailController(), "Should throw exception");

        HtmlElementAwareComponent heac = mock(HtmlElementAwareComponent.class);
        Component mdc = mock(Component.class);
        doReturn(mdc).when(heac).getComponent();
        Map<String, HtmlElementAwareComponent> elementCollectorMap = new HashMap<>();
        elementCollectorMap.put("mdc-id", heac);
        VaadinSession.getCurrent().setAttribute(ElementCollector.ELEMENT_COLLECTOR_COMPONENT_MAP, elementCollectorMap);

        assertThrows(CrudPanelCannotFindMasterDetailControllerException.class, () -> crudPanel.getMasterDetailController(), "Should throw exception");
    }

    @Test
    @DisplayName("should throw exception of there is no mdc id defined")
    void test_should_throw_exception_of_there_is_no_mdc_id_defined() {
        CrudPanelCannotFindMasterDetailControllerException exception = assertThrows(
            CrudPanelCannotFindMasterDetailControllerException.class, () -> crudPanel.getMasterDetailController(), "Should throw exception");
        assertNotNull(exception);
        String message = exception.getMessage();
        assertNotNull(message);
        assertTrue(message.toLowerCase().contains("id is missing"));
    }

    @Test
    @DisplayName("Should register inline save consumer and should set item and mark form dirty if inline edited item is selected")
    void test_should_register_inline_save_consumer_and_should_set_item_and_mark_form_dirty_if_inline_edited_item_is_selected() {
        crudPanel.registerGenericInlineItemSaveConsumer();

        ArgumentCaptor<Consumer<String>> captor = ArgumentCaptor.forClass(Consumer.class);
        verify(genericCrudControllerBean).registerNestedInlineItemSaveHandler(captor.capture());
        Consumer<String> consumer = captor.getValue();
        assertNotNull(consumer);

        UIBuilderCrudPanel<?> crudPanel = mock(UIBuilderCrudPanel.class);
        doReturn(Optional.of("test-crud-panel-id")).when(crudPanel).getId();

        Element crudPanelElement = mock(Element.class);
        ElementCollector.register(crudPanel, crudPanelElement);

        UIBuilderMasterDetailController<Serializable> mdc = mock(UIBuilderMasterDetailController.class);
        doReturn(mdc).when(crudPanel).getMasterDetailController();
        doReturn(true).when(mdc).isOnlyOneItemSelected();

        UIBuilderDetailPanel<Serializable> detailPanel = mock(UIBuilderDetailPanel.class);
        doReturn(Optional.of(detailPanel)).when(mdc).getDetailComponent();

        Serializable selectedItem = "item";
        doReturn(selectedItem).when(mdc).getSelectedItem();

        consumer.accept("test-crud-panel-id");
        verify(detailPanel).setItem(eq(selectedItem));
        verify(detailPanel).setInternalFormDirty();
    }

    @Test
    @DisplayName("Should register inline save consumer and should call mark dirty on the parent mdc if it is found")
    void test_should_register_inline_save_consumer_and_should_call_mark_dirty_on_the_parent_mdc_if_it_is_found() {
        crudPanel.registerGenericInlineItemSaveConsumer();

        ArgumentCaptor<Consumer<String>> captor = ArgumentCaptor.forClass(Consumer.class);
        verify(genericCrudControllerBean).registerNestedInlineItemSaveHandler(captor.capture());
        Consumer<String> consumer = captor.getValue();
        assertNotNull(consumer);

        UIBuilderCrudPanel<?> crudPanel = mock(UIBuilderCrudPanel.class);
        doReturn(Optional.of("test-crud-panel-id")).when(crudPanel).getId();

        Element crudPanelElement = mock(Element.class);
        ElementCollector.register(crudPanel, crudPanelElement);

        UIBuilderMasterDetailController<Serializable> mdc = mock(UIBuilderMasterDetailController.class);
        doReturn(mdc).when(crudPanel).getMasterDetailController();
        doReturn(false).when(mdc).isOnlyOneItemSelected();

        UIBuilderMasterDetailController<Serializable> parentMdc = mock(UIBuilderMasterDetailController.class);
        doReturn(Optional.of(parentMdc)).when(mdc).findParentMdc();

        Serializable selectedItem = "item";
        doReturn(selectedItem).when(mdc).getSelectedItem();

        consumer.accept("test-crud-panel-id");
        verify(parentMdc).markDetailDirty();
    }

}
