/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.crudpanel;

import com.vaadin.flow.component.textfield.TextField;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(BaseUIBuilderTestExtension.class)
class UIBuilderCrudPanelFindCrudClassTypeTest {

    @Test
    @DisplayName("Should return null if element is null")
    void test_should_return_null_if_element_is_null() {
        Class<?> classType = UIBuilderCrudPanel.findCrudClassType(null);
        assertNull(classType);
    }

    @Test
    @DisplayName("Should return null if element is has not been collected")
    void test_should_return_null_if_element_is_has_not_been_collected() {
        Class<?> classType = UIBuilderCrudPanel.findCrudClassType("random-id");
        assertNull(classType);
    }

    @Test
    @DisplayName("Should return item class if id is a crud panel id, and has item class defined")
    void test_should_return_item_class_if_id_is_a_crud_panel_id_and_has_item_class_defined() {
        UIBuilderCrudPanel<?> component = mock(UIBuilderCrudPanel.class);
        doReturn(String.class).when(component).getItemType();
        ElementCollector.register(component, createElement("crud-panel", "random-id"));

        Class<?> classType = UIBuilderCrudPanel.findCrudClassType("random-id");
        assertNotNull(classType);
        assertTrue(String.class.isAssignableFrom(classType));
    }

    @Test
    @DisplayName("Should return item class if id is a curd panel id, but it has not item class defined, but the generated mdc has")
    void test_should_return_item_class_if_id_is_a_curd_panel_id_but_it_has_not_item_class_defined_but_the_generated_mdc_has() {
        UIBuilderCrudPanel<?> component = mock(UIBuilderCrudPanel.class);
        UIBuilderMasterDetailController<?> mdc = mock(UIBuilderMasterDetailController.class);
        doReturn(String.class).when(mdc).getItemType();
        ElementCollector.register(component, createElement("crud-panel", "random-id"));
        ElementCollector.register(mdc, createElement("master-detail-controller", "random-id-mdc"));

        Class<?> classType = UIBuilderCrudPanel.findCrudClassType("random-id");
        assertNotNull(classType);
        assertTrue(String.class.isAssignableFrom(classType));
    }

    @Test
    @DisplayName("Should return item class if id is an mdc, but it has not defined an item class, but related detail component has")
    void test_should_return_item_class_if_id_is_an_mdc_but_it_has_not_defined_an_item_class_but_related_detail_component_has() {
        UIBuilderMasterDetailController<?> component = mock(UIBuilderMasterDetailController.class);
        doReturn("detail-id").when(component).getDetailId();

        UIBuilderDetailPanel<?> detailPanel = mock(UIBuilderDetailPanel.class);
        doReturn(String.class).when(detailPanel).getItemType();

        ElementCollector.register(component, createElement("master-detail-controller", "random-id"));
        ElementCollector.register(detailPanel, createElement("detail-panel", "detail-id"));

        Class<?> classType = UIBuilderCrudPanel.findCrudClassType("random-id");
        assertNotNull(classType);
        assertTrue(String.class.isAssignableFrom(classType));
    }

    @Test
    @DisplayName("Should return item class if id is a has type component and has item class defined")
    void test_should_return_item_class_if_id_is_a_has_type_component_and_has_item_class_defined() {
        UIBuilderForm<?> component = mock(UIBuilderForm.class);
        doReturn(String.class).when(component).getItemType();

        ElementCollector.register(component, createElement("uibuilder-form", "random-id"));

        Class<?> classType = UIBuilderCrudPanel.findCrudClassType("random-id");
        assertNotNull(classType);
        assertTrue(String.class.isAssignableFrom(classType));
    }

    @Test
    @DisplayName("Should return null if there is an id defined by a component, but is is not a hasItemType component")
    void test_should_return_null_if_there_is_an_id_defined_by_a_component_but_is_is_not_a_has_item_type_component() {
        TextField component = mock(TextField.class);
        ElementCollector.register(component, createElement("random-tag", "random-id"));

        Class<?> classType = UIBuilderCrudPanel.findCrudClassType("random-id");
        assertNull(classType);
    }


    private Element createElement(String tag, String id) {
        Element element = new Element(tag);
        element.attr("id", id);
        return element;
    }

}
