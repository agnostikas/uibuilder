/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.crudpanel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.api.components.form.UIBuilderDetailCapable;
import io.devbench.uibuilder.api.components.masterconnector.UIBuilderMasterConnector;
import io.devbench.uibuilder.api.crud.CrudControllerBean;
import io.devbench.uibuilder.api.crud.GenericCrudControllerBean;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.crudpanel.exception.CrudPanelCannotFindMasterDetailControllerException;
import io.devbench.uibuilder.components.crudpanel.exception.CrudPanelInvalidContollerBeanException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.startup.ComponentTagRegistry;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.data.common.datasource.DataSourceChangeNotifiable;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import javax.inject.Provider;
import java.lang.reflect.Field;
import java.util.*;
import static io.devbench.uibuilder.api.crud.GenericCrudControllerBean.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, JsoupExtension.class})
public class UIBuilderCrudPanelInterceptorTest {

    private UIBuilderCrudPanelInterceptor testObj;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Mock
    @SingletonInstance(ComponentTagRegistry.class)
    private ComponentTagRegistry componentTagRegistry;

    @Mock
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    @Mock
    private Provider<GenericCrudControllerBean> genericCrudControllerBeanProvider;

    @Mock
    private GenericCrudControllerBean genericCrudControllerBean;

    @BeforeEach
    void setUp() {
        doReturn(genericCrudControllerBean).when(genericCrudControllerBeanProvider).get();
        doReturn(Optional.of(TestTag.class)).when(componentTagRegistry).getComponentClassByTag("test-tag");
        doReturn(Optional.of(DetailCapableComponent.class)).when(componentTagRegistry).getComponentClassByTag("some-detail");
        doReturn(Optional.of(DetailCapableComponent.class)).when(componentTagRegistry).getComponentClassByTag("detail-panel");
        doReturn(Optional.of(FilterComponent.class)).when(componentTagRegistry).getComponentClassByTag("some-filter");
        doReturn(Optional.of(MasterCapableComponent.class)).when(componentTagRegistry).getComponentClassByTag("some-grid");
        doReturn(Optional.of(MasterCapableComponent.class)).when(componentTagRegistry).getComponentClassByTag("inner-grid");
        doReturn(Optional.of(MasterCapableComponent.class)).when(componentTagRegistry).getComponentClassByTag("vaadin-uibuilder-grid");

        Set<Class<?>> connectorClasses = new HashSet<>(Collections.singletonList(MasterCapableComponentConnector.class));
        doReturn(connectorClasses).when(memberScanner).findClassesBySuperType(UIBuilderMasterConnector.class);

        testObj = new UIBuilderCrudPanelInterceptor();
    }

    @Test
    @DisplayName("should be applicable only for the crud panel")
    void test_should_be_applicable_only_for_the_crud_panel() {
        Element element = mock(Element.class);

        when(element.tagName()).thenReturn("other-tag").thenReturn(UIBuilderCrudPanel.TAG_NAME);

        assertFalse(testObj.isApplicable(element));
        assertTrue(testObj.isApplicable(element));
    }

    @Test
    @DisplayName("intercept should throw exception if controller bean is invalid")
    void test_intercept_should_throw_exception_if_controller_bean_is_invalid() {
        Element element = Jsoup
            .parse("<crud-panel controller-bean=\"testCb\"><master-detail-controller id=\"mdc-id\"></master-detail-controller></crud-panel>")
            .getElementsByTag("crud-panel").first();

        doReturn(new Object()).when(controllerBeanManager).getControllerBean("testCb");

        UIBuilderCrudPanel<?> crudPanel = new UIBuilderCrudPanel<>();

        CrudPanelInvalidContollerBeanException exception = assertThrows(
            CrudPanelInvalidContollerBeanException.class, () -> testObj.intercept(crudPanel, element), "Should throw exception");

        assertNotNull(exception);
        String message = exception.getMessage();
        assertNotNull(message);
        assertTrue(message.matches("(.*testCb.*" + CrudControllerBean.class.getSimpleName() + ".*)"),
            "Exception message should contain the detailed info");
    }

    @Test
    @DisplayName("intercept should set MDC id and ensure controller bean compatibility")
    void test_intercept_should_ensure_controller_bean_compatibility() {
        Element element = Jsoup
            .parse("<crud-panel controller-bean=\"testCb\"><master-detail-controller id=\"mdc-id\"></master-detail-controller></crud-panel>")
            .getElementsByTag("crud-panel").first();

        CrudControllerBean<?> cb = mock(CrudControllerBean.class);
        doReturn(cb).when(controllerBeanManager).getControllerBean("testCb");

        UIBuilderCrudPanel<?> crudPanel = new UIBuilderCrudPanel<>();
        testObj.intercept(crudPanel, element);

        assertEquals("mdc-id", crudPanel.getMasterDetailControllerId());
    }

    @Test
    @DisplayName("intercept should register generic controller bean if controller bean attribute is absent")
    void test_intercept_should_register_generic_controller_bean_if_controller_bean_attribute_is_absent() throws Exception {
        Element element = Jsoup
            .parse("<crud-panel id=\"test-cp-id\">" +
                "<data-source name=\"testDS\"></data-source>" +
                "<some-grid></some-grid>" +
                "<some-detail></some-detail>" +
                "</crud-panel>")
            .getElementsByTag("crud-panel").first();

        CrudControllerBean<?> cb = mock(CrudControllerBean.class);
        doReturn(cb).when(controllerBeanManager).getControllerBean(BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME);

        UIBuilderCrudPanel<?> crudPanel = new UIBuilderCrudPanel<>();
        Field genericCrudControllerBeanProviderField = UIBuilderCrudPanel.class.getDeclaredField("genericCrudControllerBean");
        genericCrudControllerBeanProviderField.setAccessible(true);
        genericCrudControllerBeanProviderField.set(crudPanel, genericCrudControllerBeanProvider);

        testObj.transform(element);
        testObj.intercept(crudPanel, element);

        assertEquals("test-cp-id-mdc", crudPanel.getMasterDetailControllerId());
        assertEquals(BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME + "::onSave", element.getElementById("test-cp-id-mdc").attr("on-save"));
        assertEquals(BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME + "::onDelete", element.getElementById("test-cp-id-mdc").attr("on-delete"));
        assertEquals(BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME + "::onRefresh", element.getElementById("test-cp-id-mdc").attr("on-refresh"));
        assertEquals(BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME + "::onCreate", element.getElementById("test-cp-id-mdc").attr("on-create"));
    }

    @Test
    @DisplayName("intercept should throw exception if MDC is mising")
    void test_intercept_should_throw_exception_if_mdc_is_mising() {
        Element element = Jsoup
            .parse("<crud-panel controller-bean=\"testCb\"><something-else></something-else></crud-panel>")
            .getElementsByTag("crud-panel").first();

        CrudControllerBean<?> cb = mock(CrudControllerBean.class);
        doReturn(cb).when(controllerBeanManager).getControllerBean("testCb");

        UIBuilderCrudPanel<?> crudPanel = new UIBuilderCrudPanel<>();

        assertThrows(CrudPanelCannotFindMasterDetailControllerException.class, () -> testObj.intercept(crudPanel, element), "Should throw exception");
    }

    @Test
    @DisplayName("instantiator is always true")
    void test_instantiator_is_always_true() {
        assertTrue(testObj.isInstantiator(null));
        assertTrue(testObj.isInstantiator(mock(Element.class)));
    }

    @Test
    @DisplayName("should transform crud panel and generate inner mdc")
    void test_should_transform_crud_panel_and_generate_inner_mdc(
        @LoadElement(value = "/crud-panel-test-1.html", id = "cpid") Element crudPanelElement) {

        testObj.transform(crudPanelElement);

        assertNotNull(crudPanelElement);

        assertEquals("cpid-mdc", getChild(crudPanelElement, "crud-toolbar").attr("master-detail-controller"));
        assertElementHasDataSourceWithName("someDs", getChild(crudPanelElement, "some-filter"));
        assertElementHasDataSourceWithName("someDs", getChild(crudPanelElement, "some-grid"));
        Element masterElement = getChild(crudPanelElement, "some-grid");
        assertEquals("cpid-master", masterElement.attr("id"));
        assertEquals("cpid-detail", getChild(crudPanelElement, "some-detail").attr("id"));

        Element mdc = getChild(crudPanelElement, "master-detail-controller");

        assertEquals("cpid-mdc", mdc.id());
        assertEquals("cpid-master", mdc.attr("master"));
        assertEquals("cpid-detail", mdc.attr("detail"));
        assertEquals("anotherBean::saveOverridden", mdc.attr("on-save"));
        assertEquals("testContBean::onDelete", mdc.attr("on-delete"));
        assertEquals("testContBean::onRefresh", mdc.attr("on-refresh"));
        assertEquals("{{testContBean::create}}", mdc.attr("item-supplier"));
        assertEquals("anotherBean::itemSelected", mdc.attr("on-item-selected"));

        Element dialog = mdc.getElementsByTag("some-dialog").first();
        assertNotNull(dialog);

        Element someDetail = getChild(crudPanelElement, "some-detail");
        Element innerCp = getChild(someDetail, "crud-panel");

        assertEquals("inner-cpid", innerCp.id());
        assertTrue(innerCp.getElementsByTag("master-detail-controller").isEmpty(), "Inner crud panel should NOT contain a generated MDC");

        assertEquals("someDs", crudPanelElement.attr("generic-datasource-name"));
        assertEquals("someDs", masterElement.attr("generic-datasource-name"));
    }

    @Test
    @DisplayName("Should add inline save handler if it is missing, and when master is a uibuilder grid")
    void test_should_add_inline_save_handler_if_it_is_missing_and_when_master_is_a_uibuilder_grid(
        @LoadElement(value = "/crud-panel-test-1.html", id = "cpid") Element crudPanelElement) {

        crudPanelElement.getElementsByTag("some-grid").forEach(Node::remove);
        Element gridElement = crudPanelElement.ownerDocument().createElement("vaadin-uibuilder-grid");
        crudPanelElement.appendChild(gridElement);

        testObj.transform(crudPanelElement);

        assertTrue(gridElement.hasAttr("on-inline-item-saved"));
        assertEquals(BUILT_IN_GENERIC_CRUD_PANEL_CONTROLLER_BEAN_NAME + "::handleNestedInlineItemSave", gridElement.attr("on-inline-item-saved"));
    }

    @Test
    @DisplayName("should transform crud panel and generate inner mdc according to manage-nested")
    void test_should_transform_crud_panel_and_generate_inner_mdc_according_to_manage_nested(
        @LoadElement(value = "/crud-panel-test-1.html", id = "cpid") Element crudPanelElement) {

        crudPanelElement.attr("manage-nested", true);
        Element innerCp = crudPanelElement.getElementById("inner-cpid");

        testObj.transform(innerCp);
        testObj.transform(crudPanelElement);

        Element innerDetail = getChild(innerCp, "detail-panel");
        Element innerMdc = getChild(innerCp, "master-detail-controller");

        assertNotNull(innerDetail);
        assertNotNull(innerMdc);
        assertTrue(innerDetail.hasAttr("hide-form-controls"));
        assertTrue(innerMdc.hasAttr("disable-master-enabled-control"));
    }

    private void assertElementHasDataSourceWithName(String name, Element element) {
        assertTrue(element.children().stream()
                .filter(c -> c.tagName().equalsIgnoreCase("data-source"))
                .anyMatch(c -> c.attr("name").equals(name)),
            "Element (" + element.tagName() + ") should contain the data-source tag with name: " + name);
    }

    private Element getChild(Element element, String childTagName) {
        Optional<Element> child = element.children().stream().filter(c -> childTagName.equalsIgnoreCase(c.tagName())).findFirst();
        if (!child.isPresent()) {
            fail("Cannot find child tag: " + childTagName);
        }
        return child.get();
    }

    @Tag("test-tag")
    private static class TestTag extends Component {

    }

    @Tag("some-detail")
    private static class DetailCapableComponent extends Component implements UIBuilderDetailCapable {

    }

    @Tag("some-filter")
    private static class FilterComponent extends Component implements DataSourceChangeNotifiable {
        @Override
        public void dataSourceChanged(@NotNull String dataSourceId, @NotNull CommonDataSourceSelector selector, @Nullable CommonDataSource dataSource) {

        }
    }

    @Tag("some-grid")
    public static class MasterCapableComponent extends AbstractDataSourceComponent {

        @Override
        protected void setSelectedItemsIfItemWasSetPreviously() {

        }
    }

    public static class MasterCapableComponentConnector implements UIBuilderMasterConnector {

        @Override
        public boolean isApplicable(Class componentClass) {
            return MasterCapableComponent.class.isAssignableFrom(componentClass);
        }

        @Override
        public void connect(Component masterComponent) {

        }

        @Override
        public void disconnect() {

        }

        @Override
        public Component getMasterComponent() {
            return null;
        }

        @Override
        public Collection getSelectedItems() {
            return null;
        }

        @Override
        public void setSelectedItems(Collection items) {

        }

        @Override
        public Registration addSelectionChangedListener(MasterSelectionChangedListener selectionChangedListener) {
            return null;
        }

        @Override
        public void refresh() {

        }

        @Override
        public void refresh(Object item) {

        }

        @Override
        public void setEnabled(boolean enabled) {

        }

        @Override
        public boolean isEnabled() {
            return false;
        }

        @Override
        public boolean isDirectModifiable() {
            return false;
        }

        @Override
        public void addItem(Object item) {

        }

        @Override
        public void removeItem(Object item) {

        }
    }

}
