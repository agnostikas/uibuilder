/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filterrow;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultFilterDescriptorTest {

    @Test
    @DisplayName("Should create filter descriptor with single value")
    void test_should_create_filter_descriptor_with_single_value() {
        JsonObject json = Json.createObject();
        json.put("added", true);
        json.put("nonRemovable", false);
        json.put("operator", "between");
        json.put("filter", "test.path");
        json.put("value", "single value");

        DefaultFilterDescriptor defaultFilterDescriptor = DefaultFilterDescriptor.of(json);

        assertNotNull(defaultFilterDescriptor);
        assertNotNull(defaultFilterDescriptor.getValues(), "Values should not be null");
        assertEquals(1, defaultFilterDescriptor.getValues().size());
        assertEquals("single value", defaultFilterDescriptor.getValues().get(0));
        assertEquals("test.path", defaultFilterDescriptor.getFilterPath());
        assertEquals(OperatorType.BETWEEN, defaultFilterDescriptor.getOperator());
        assertTrue(defaultFilterDescriptor.isAdded(), "Added should be true");
        assertFalse(defaultFilterDescriptor.isReadOnly(), "ReadOnly should be false");
        assertFalse(defaultFilterDescriptor.isNonRemovable(), "NonRemovable should be false");
    }

    @Test
    @DisplayName("Should create filter descriptor with multi value")
    void test_should_create_filter_descriptor_with_multi_value() {
        JsonObject json = Json.createObject();
        json.put("added", false);
        json.put("readOnly", true);
        json.put("operator", "in");
        json.put("filter", "test.path");
        JsonArray valueArray = Json.createArray();
        valueArray.set(0, "first value");
        valueArray.set(1, "second value");
        valueArray.set(2, "third");
        json.put("value", valueArray);

        DefaultFilterDescriptor defaultFilterDescriptor = DefaultFilterDescriptor.of(json);

        assertNotNull(defaultFilterDescriptor);
        assertNotNull(defaultFilterDescriptor.getValues(), "Values should not be null");
        assertEquals(3, defaultFilterDescriptor.getValues().size());
        assertEquals("first value", defaultFilterDescriptor.getValues().get(0));
        assertEquals("second value", defaultFilterDescriptor.getValues().get(1));
        assertEquals("third", defaultFilterDescriptor.getValues().get(2));
        assertEquals("test.path", defaultFilterDescriptor.getFilterPath());
        assertEquals(OperatorType.IN, defaultFilterDescriptor.getOperator());
        assertFalse(defaultFilterDescriptor.isAdded(), "Added should be false");
        assertTrue(defaultFilterDescriptor.isReadOnly(), "ReadOnly should be true");
        assertFalse(defaultFilterDescriptor.isNonRemovable(), "NonRemovable should be false");
    }

    @Test
    @DisplayName("Should create filter descriptor with null value")
    void test_should_create_filter_descriptor_with_null_value() {
        JsonObject json = Json.createObject();
        json.put("operator", "in");
        json.put("filter", "test.path");
        json.put("value", Json.createNull());

        DefaultFilterDescriptor defaultFilterDescriptor = DefaultFilterDescriptor.of(json);

        assertNotNull(defaultFilterDescriptor);
        assertNotNull(defaultFilterDescriptor.getValues(), "Values should not be null");
        assertEquals(1, defaultFilterDescriptor.getValues().size());
        assertNull(defaultFilterDescriptor.getValues().get(0), "Value should be null");
    }

    @Test
    @DisplayName("Should create filter descriptor even if boolean value is null")
    void test_should_create_filter_descriptor_even_if_boolean_value_is_null() {
        JsonObject json = Json.createObject();
        json.put("filter", "test.path");
        json.put("operator", "in");
        json.put("value", "val");
        json.put("added", Json.createNull());

        DefaultFilterDescriptor defaultFilterDescriptor = DefaultFilterDescriptor.of(json);
        assertNotNull(defaultFilterDescriptor);
        assertFalse(defaultFilterDescriptor.isAdded(), "Added should be false");
    }

    @Test
    @DisplayName("Should throw NPE when value is not defined")
    void test_should_throw_npe_when_value_is_not_defined() {
        JsonObject json = Json.createObject();
        json.put("operator", "in");
        json.put("filter", "test.path");

        NullPointerException npe = assertThrows(NullPointerException.class, () -> DefaultFilterDescriptor.of(json), "Should throw NPE");
        assertEquals(npe.getMessage(), "Property value is mandatory!");
    }

    @Test
    @DisplayName("Should throw NPE when operator is not defined")
    void test_should_throw_npe_when_operator_is_not_defined() {
        JsonObject json = Json.createObject();
        json.put("filter", "test.path");
        json.put("value", "val");

        NullPointerException npe = assertThrows(NullPointerException.class, () -> DefaultFilterDescriptor.of(json), "Should throw NPE");
        assertEquals(npe.getMessage(), "Property operator is mandatory!");
    }

    @Test
    @DisplayName("Should throw NPE when filter is not defined")
    void test_should_throw_npe_when_filter_is_not_defined() {
        JsonObject json = Json.createObject();
        json.put("operator", "in");
        json.put("value", "val");

        NullPointerException npe = assertThrows(NullPointerException.class, () -> DefaultFilterDescriptor.of(json), "Should throw NPE");
        assertEquals(npe.getMessage(), "Property filter is mandatory!");
    }

    @Test
    @DisplayName("Should throw NPE when operator is null")
    void test_should_throw_npe_when_operator_is_null() {
        JsonObject json = Json.createObject();
        json.put("filter", "test.path");
        json.put("operator", Json.createNull());
        json.put("value", "val");

        NullPointerException npe = assertThrows(NullPointerException.class, () -> DefaultFilterDescriptor.of(json), "Should throw NPE");
        assertEquals(npe.getMessage(), "Property operator is mandatory!");
    }

    @Test
    @DisplayName("Should throw NPE when filter is null")
    void test_should_throw_npe_when_filter_is_null() {
        JsonObject json = Json.createObject();
        json.put("filter", Json.createNull());
        json.put("operator", "in");
        json.put("value", "val");

        NullPointerException npe = assertThrows(NullPointerException.class, () -> DefaultFilterDescriptor.of(json), "Should throw NPE");
        assertEquals(npe.getMessage(), "Property filter is mandatory!");
    }

}
