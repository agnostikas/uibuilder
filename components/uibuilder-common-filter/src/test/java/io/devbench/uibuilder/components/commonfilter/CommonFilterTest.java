/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventBus;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.dom.Element;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.impl.JreJsonFactory;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder;
import io.devbench.uibuilder.components.commonfilter.events.FilterAdditionRequestedEvent;
import io.devbench.uibuilder.components.commonfilter.events.FilterPropertiesReadyEvent;
import io.devbench.uibuilder.components.commonfilter.events.SearchButtonClickedEvent;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterTypeInstantiationException;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterTypeNotFoundException;
import io.devbench.uibuilder.components.commonfilter.filterrow.DefaultFilterDescriptor;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.components.commonfilter.filterrow.HasFilterValidationReportSupport;
import io.devbench.uibuilder.components.commonfilter.filtertype.CustomFilterType;
import io.devbench.uibuilder.components.commonfilter.filtertype.FilterType;
import io.devbench.uibuilder.components.commonfilter.filtertype.SupportsDefaultFilter;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationExecutor;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationReport;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.data.common.filter.comperingfilters.BinaryOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import io.devbench.uibuilder.data.common.filter.validators.ValueNotNullFilterValidator;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, ResourceReaderExtension.class, SingletonProviderForTestsExtension.class})
class CommonFilterTest {

    @Mock
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Mock
    @SingletonInstance(FilterValidationExecutor.class)
    private FilterValidationExecutor filterValidationExecutor;

    @Mock
    private ComponentEventBus mockEventBus;

    @Mock
    private CommonDataSource<?, ?, FilterExpression<?>, ?> dataSource;

    @Mock
    private FilterExpressionFactory expressionFactory;

    @Mock
    private BindingMetadataProvider metadataProvider;

    private Map<String, FilterType> filterTypes;

    private CommonFilter testObj;

    @BeforeEach
    private void setup() {
        TestCustomFilterType.initializeThrows = null;
        filterTypes = new HashMap<>();
        testObj = spy(new TestCommonFilter());
    }

    @Test
    @DisplayName("Should register filter properties ready listener after attached")
    public void should_register_filter_properties_ready_listener_after_attached() {
        mockEventBusOnTestObject();

        testObj.onAttached();

        verify(mockEventBus).addListener(eq(FilterPropertiesReadyEvent.class), any(ComponentEventListener.class));
    }

    @Test
    @DisplayName("Should register search button clicked listener after attached")
    public void should_register_search_button_clicked_listener_after_attached() {
        mockEventBusOnTestObject();

        testObj.onAttached();

        verify(mockEventBus).addListener(eq(SearchButtonClickedEvent.class), any(ComponentEventListener.class));
    }

    @Test
    @DisplayName("Should register filter addition request listener after attached")
    public void should_register_filter_addition_request_listener_after_attached() {
        mockEventBusOnTestObject();

        testObj.onAttached();

        verify(mockEventBus).addListener(eq(FilterAdditionRequestedEvent.class), any(ComponentEventListener.class));
    }

    @Test
    @DisplayName("Should set the validator name when the filter properties ready fired")
    public void should_set_the_validator_name_when_the_filter_properties_ready_fired() {
        ComponentEventBus realEventBus = getRealEventBus();
        testObj.onAttached();
        FilterPropertiesReadyEvent propertiesReadyEvent = createFilterPropertiesReadyEventWithValidator(jsonArray().build());

        realEventBus.fireEvent(propertiesReadyEvent);

        assertEquals("testValidator", testObj.getValidatorName());
    }

    @Test
    public void when_calling_the_datasource_change_callback_with_null_datasource_it_should_add_no_option_to_the_add_button() {
        Element mockElement = spy(testObj.getElement());
        ArgumentCaptor<Serializable> argumentCaptor = ArgumentCaptor.forClass(Serializable.class);
        when(testObj.getElement()).thenReturn(mockElement);
        when(dataSourceManager.getDataSource(any(), any())).thenReturn(null);

        testObj.dataSourceChanged("testDataSource", mock(CommonDataSourceSelector.class), null);

        verify(mockElement, atLeastOnce()).callJsFunction(eq("_renderMenuItems"), argumentCaptor.capture());
        assertNotEquals(0, argumentCaptor.getAllValues());
        argumentCaptor
            .getAllValues()
            .stream()
            .map(it -> (JsonArray) it)
            .forEach(array -> assertEquals(0, array.length()));
    }

    @Test
    public void when_calling_the_datasource_change_callback_with_datasource_without_element_type_it_should_add_no_option_to_the_add_button() {
        Element mockElement = spy(testObj.getElement());
        ArgumentCaptor<Serializable> argumentCaptor = ArgumentCaptor.forClass(Serializable.class);
        when(testObj.getElement()).thenReturn(mockElement);

        CommonDataSource dataSource = mock(CommonDataSource.class);
        FilterExpressionFactory filterExpressionFactory = mock(FilterExpressionFactory.class);
        when(dataSource.getElementType()).thenReturn(Object.class);
        when(dataSource.getFilterExpressionFactory()).thenReturn(filterExpressionFactory);
        when(filterExpressionFactory.create(any())).thenAnswer(i -> mock(i.getArgument(0)));

        testObj.dataSourceChanged("testDataSource", mock(CommonDataSourceSelector.class), dataSource);

        verify(mockElement, atLeastOnce()).callJsFunction(eq("_renderMenuItems"), argumentCaptor.capture());
        assertNotEquals(0, argumentCaptor.getAllValues());
        argumentCaptor
            .getAllValues()
            .stream()
            .map(it -> (JsonArray) it)
            .forEach(array -> assertEquals(0, array.length()));
    }

    @Test
    @DisplayName("Should collect the data source instance when the filter properties are collected on the frontend and sent to the backend")
    public void should_collect_the_data_source_instance_when_the_filter_properties_are_collected_on_the_frontend_and_sent_to_the_backend() {
        ComponentEventBus realEventBus = getRealEventBus();
        testObj.onAttached();
        FilterPropertiesReadyEvent propertiesReadyEvent = createFilterPropertiesReadyEvent(jsonArray().build());

        realEventBus.fireEvent(propertiesReadyEvent);
        testObj.getDataSource();

        ArgumentCaptor<CommonDataSourceSelector> selectorCaptor = ArgumentCaptor.forClass(CommonDataSourceSelector.class);
        verify(dataSourceManager, atLeastOnce()).getDataSource(eq("testDataSourceId"), selectorCaptor.capture());
        assertAll(
            () -> assertNull(selectorCaptor.getValue().getDefaultQuery()),
            () -> assertSame(testObj, selectorCaptor.getValue().getContextHolder())
        );
    }

    @Test
    @DisplayName("Should collect and instantiate filter types based on the filter property ready event sent from the frontend, and call the frontend component to render the menu elements for the types")
    public void should_collect_and_instantiate_filter_types_based_on_the_filter_property_ready_event_sent_from_the_frontend_and_call_the_frontend_component_to_render_the_menu_elements_for_the_types(
        @ReadResource("/test-filter-types.json") String testFilterTypesString
    ) {
        ComponentEventBus realEventBus = getRealEventBus();
        testObj.onAttached();
        doReturn(Collections.singleton(TestCustomFilterType.class)).when(memberScanner).findClassesByAnnotation(CustomFilterType.class);
        JsonArray filterTypesDescriptors = new JreJsonFactory().parse(testFilterTypesString);
        FilterPropertiesReadyEvent propertiesReadyEvent = createFilterPropertiesReadyEvent(filterTypesDescriptors);

        Element mockElement = mock(Element.class);
        when(testObj.getElement()).thenReturn(mockElement);

        realEventBus.fireEvent(propertiesReadyEvent);

        assertAll(
            () -> verify(memberScanner).findClassesByAnnotation(CustomFilterType.class),
            () -> assertNotNull(testObj.getFilterTypeUuidMap()),
            () -> assertEquals(3, testObj.getFilterTypeUuidMap().size()),
            () -> {
                ArgumentCaptor<JsonArray> menuItemsCaptor = ArgumentCaptor.forClass(JsonArray.class);
                verify(mockElement).callJsFunction(eq("_renderMenuItems"), menuItemsCaptor.capture());
                JsonArray menuItems = menuItemsCaptor.getValue();
                assertTrue(testObj.getFilterTypeUuidMap().keySet().containsAll(streamFromJsonArray(menuItems)
                    .map(item -> item.getString("id"))
                    .collect(Collectors.toList())));
                assertTrue(Arrays.asList("test label 1", "test label 2", "my custom filter")
                    .containsAll(streamFromJsonArray(menuItems)
                        .map(item -> item.getString("label"))
                        .collect(Collectors.toList())));
            },
            () -> assertEquals(
                jsonObject()
                    .put("type", "testCustomFilterType")
                    .put("customConfiguration", jsonObject().put("myCustomConfig", "test custom config").build()),
                testObj.getFilterTypeUuidMap().values().stream()
                    .filter(TestCustomFilterType.class::isInstance)
                    .map(filterType -> ((TestCustomFilterType) filterType).initializedWith)
                    .findAny()
                    .map(JsonBuilder::jsonObjectFrom)
                    .orElse(null))
        );
    }

    @Test
    @DisplayName("Should throw exception when filter type requested with unrecognized type name")
    public void should_throw_exception_when_filter_type_requested_with_unrecognized_type_name() {
        testObj.onAttached();
        JsonArray filterTypesDescriptor = jsonArray().add(jsonObject().put("type", "UNKNOWN_FILTER_TYPE").build()).build();
        NullPointerException exception = assertThrows(NullPointerException.class, () -> createFilterPropertiesReadyEvent(filterTypesDescriptor));
        assertEquals("Requested filter type cannot be found with name: UNKNOWN_FILTER_TYPE", exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception when the creation of the requested custom filter type throws an exception")
    public void should_throw_exception_when_the_creation_of_the_requested_custom_filter_type_throws_an_exception() {
        testObj.onAttached();
        JsonArray filterTypesDescriptor = jsonArray().add(jsonObject().put("type", "testCustomFilterType").build()).build();
        TestCustomFilterType.initializeThrows = new IllegalStateException();
        doReturn(Collections.singleton(TestCustomFilterType.class)).when(memberScanner).findClassesByAnnotation(CustomFilterType.class);
        FilterTypeInstantiationException exception = assertThrows(
            FilterTypeInstantiationException.class,
            () -> createFilterPropertiesReadyEvent(filterTypesDescriptor));
        assertEquals(TestCustomFilterType.initializeThrows, exception.getCause());
    }

    @Test
    @DisplayName("Should handle filter addition requests, by creating the filter row using the right filter type and adding the row as component to itself")
    public void should_handle_filter_addition_requests_by_creating_the_filter_row_using_the_right_filter_type_and_adding_the_row_as_component_to_itself() {
        doReturn(dataSource).when(testObj).getDataSource();
        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        when(dataSource.getMetadataProvider()).thenReturn(metadataProvider);
        ComponentEventBus realEventBus = getRealEventBus();
        testObj.onAttached();
        doNothing().when(testObj).add(any(Component.class));
        doReturn(filterTypes).when(testObj).getFilterTypeUuidMap();
        TestCustomFilterType testFilterType = new TestCustomFilterType();
        filterTypes.put("testFilter", testFilterType);
        FilterAdditionRequestedEvent additionRequest = new FilterAdditionRequestedEvent(testObj, true, "testFilter");

        realEventBus.fireEvent(additionRequest);

        assertAll(
            () -> assertTrue(testFilterType.createFilterRowCalled),
            () -> assertSame(expressionFactory, testFilterType.expressionFactory),
            () -> assertSame(metadataProvider, testFilterType.metadataProvider),
            () -> verify(testObj).add(any(TestFilterRow.class))
        );
    }

    @Test
    @DisplayName("Should throw exception when the filter addition request points to a filter type that doesn't exists")
    public void should_throw_exception_when_the_filter_addition_request_points_to_a_filter_type_that_doesn_t_exists() {
        ComponentEventBus realEventBus = getRealEventBus();
        testObj.onAttached();
        doReturn(filterTypes).when(testObj).getFilterTypeUuidMap();
        FilterAdditionRequestedEvent additionRequest = new FilterAdditionRequestedEvent(testObj, true, "unknown-filter-id");

        FilterTypeNotFoundException exception = assertThrows(FilterTypeNotFoundException.class, () -> realEventBus.fireEvent(additionRequest));
        assertEquals("Filter type requested for addition not found. Id used in the menu: unknown-filter-id", exception.getMessage());
    }

    @Test
    @DisplayName("Should call search method when search button clicked event fired")
    public void should_call_search_method_when_search_button_clicked_event_fired() {
        ComponentEventBus realEventBus = getRealEventBus();
        testObj.onAttached();
        doNothing().when(testObj).search();

        realEventBus.fireEvent(new SearchButtonClickedEvent(testObj, true));

        verify(testObj).search();
    }

    @Test
    @DisplayName("Should create an 'and' filter and collect the filter expressions into it from the filter rows, than send it to the data source when search called")
    public void should_create_an_and_filter_and_collect_the_filter_expressions_into_it_from_the_filter_rows_than_send_it_to_the_data_source_when_search_called() {
        doReturn(dataSource).when(testObj).getDataSource();
        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        AndFilterExpression mockAnd = mock(AndFilterExpression.class);
        when(expressionFactory.create(AndFilterExpression.class)).thenReturn(mockAnd);

        FilterRow mockFilterRow1 = mock(FilterRow.class, Answers.RETURNS_DEEP_STUBS);
        when(mockFilterRow1.isEmpty()).thenReturn(false);
        when(mockFilterRow1.getFilterType().getValidatorName()).thenReturn(Optional.empty());
        FilterExpression mockFilterExpression1 = mock(FilterExpression.class);
        when(mockFilterRow1.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression1));

        FilterRow mockFilterRow2 = mock(FilterRow.class, Answers.RETURNS_DEEP_STUBS);
        when(mockFilterRow2.isEmpty()).thenReturn(false);
        when(mockFilterRow2.getFilterType().getValidatorName()).thenReturn(Optional.empty());
        FilterExpression mockFilterExpression2 = mock(FilterExpression.class);
        when(mockFilterRow2.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression2));

        doAnswer(invocation -> Stream.of(mockFilterRow1, mockFilterRow2)).when(testObj).getChildren();

        when(testObj.getDataSourceId()).thenReturn("testDS");
        when(testObj.getDefaultQueryName()).thenReturn("testQuery");

        testObj.search();

        assertAll(
            () -> verify(expressionFactory).create(AndFilterExpression.class),
            () -> verify(mockAnd).add(mockFilterExpression1),
            () -> verify(mockAnd).add(mockFilterExpression2),
            () -> verify(dataSource).registerFilter(mockAnd),
            () -> {
                ArgumentCaptor<CommonDataSourceSelector> selectorCaptor = ArgumentCaptor.forClass(CommonDataSourceSelector.class);
                verify(dataSourceManager).requestRefresh(eq("testDS"), selectorCaptor.capture());
                assertEquals("testQuery", selectorCaptor.getValue().getDefaultQuery());
                assertSame(testObj, selectorCaptor.getValue().getContextHolder());
            }
        );
    }

    @Test
    @DisplayName("Should ensure there isn't any empty rows present before search")
    public void should_ensure_there_isn_t_any_empty_rows_present_before_search() {
        FilterRow mockFilterRow1 = mock(FilterRow.class, Answers.RETURNS_DEEP_STUBS);
        when(mockFilterRow1.isEmpty()).thenReturn(false);
        when(mockFilterRow1.getFilterType().getValidatorName()).thenReturn(Optional.empty());
        FilterRow mockFilterRow2 = mock(FilterRow.class, Answers.RETURNS_DEEP_STUBS);
        when(mockFilterRow2.getFilterType().getValidatorName()).thenReturn(Optional.empty());
        when(mockFilterRow2.isEmpty()).thenReturn(true);

        doReturn(Stream.of(mockFilterRow1, mockFilterRow2)).when(testObj).getChildren();

        testObj.search();

        verifyZeroInteractions(expressionFactory);
    }

    @Test
    @DisplayName("Should validate individual filter expressions and notify the rows about the results if supports")
    public void should_validate_individual_filter_expressions_and_notify_the_rows_about_the_results_if_supports() {
        Element mockElement = mock(Element.class);
        when(testObj.getElement()).thenReturn(mockElement);
        doReturn(dataSource).when(testObj).getDataSource();
        TestFilterRow mockFilterRow1 = mock(TestFilterRow.class);
        when(mockFilterRow1.isEmpty()).thenReturn(false);
        FilterExpression mockFilterExpression1 = mock(FilterExpression.class);
        when(mockFilterRow1.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression1));
        TestCustomFilterType mockFilterType1 = mock(TestCustomFilterType.class);
        when(mockFilterType1.getValidatorName()).thenReturn(Optional.of("validator1"));
        when(mockFilterRow1.getFilterType()).thenReturn(mockFilterType1);

        FilterRow mockFilterRow2 = mock(FilterRow.class);
        when(mockFilterRow2.isEmpty()).thenReturn(false);
        FilterExpression mockFilterExpression2 = mock(FilterExpression.class);
        when(mockFilterRow2.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression2));
        FilterType mockFilterType2 = mock(FilterType.class);
        when(mockFilterType2.getValidatorName()).thenReturn(Optional.of("validator2"));
        when(mockFilterRow2.getFilterType()).thenReturn(mockFilterType2);

        doAnswer(invocation -> Stream.of(mockFilterRow1, mockFilterRow2)).when(testObj).getChildren();

        FilterValidationReport report1 = mock(FilterValidationReport.class);
        when(report1.isInvalid()).thenReturn(true);
        when(report1.hasErrors()).thenReturn(true);
        when(filterValidationExecutor.validate("validator1", mockFilterExpression1)).thenReturn(report1);

        FilterValidationReport report2 = mock(FilterValidationReport.class);
        when(report2.isInvalid()).thenReturn(true);
        when(report2.hasErrors()).thenReturn(true);
        when(report2.hasWarnings()).thenReturn(true);
        when(report2.getErrors()).thenReturn(Collections.singletonList("error message"));
        when(report2.getWarnings()).thenReturn(Collections.singletonList("warning message"));
        when(filterValidationExecutor.validate("validator2", mockFilterExpression2)).thenReturn(report2);

        testObj.search();

        assertAll(
            () -> verify(mockFilterRow1).reportValidationResult(report1),
            () -> verifyZeroInteractions(dataSource)
        );

        ArgumentCaptor<JsonObject> jsonCaptor = ArgumentCaptor.forClass(JsonObject.class);
        verify(mockElement).setPropertyJson(eq("_validationResult"), jsonCaptor.capture());
        JsonObjectBuilder expectedBuilder = jsonObject()
            .put("hasErrors", true)
            .put("errors", jsonArray().add("error message").build())
            .put("hasWarnings", true)
            .put("warnings", jsonArray().add("warning message").build())
            .put("errorHeader", "Filtering resulted errors:")
            .put("warnHeader", "Filtering resulted warnings:");
        assertEquals(expectedBuilder, jsonObjectFrom(jsonCaptor.getValue()));
    }

    @Test
    @DisplayName("Should validate the whole filter expression, when search is requested and build the validation result and send it to the frontend")
    public void should_validate_the_whole_filter_expression_when_search_is_requested_and_build_the_validation_result_and_send_it_to_the_frontend() {
        Element mockElement = mock(Element.class);
        when(testObj.getElement()).thenReturn(mockElement);
        when(testObj.getValidatorName()).thenReturn("validator");
        doReturn(dataSource).when(testObj).getDataSource();
        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        AndFilterExpression mockAnd = mock(AndFilterExpression.class);
        when(expressionFactory.create(AndFilterExpression.class)).thenReturn(mockAnd);

        FilterRow mockFilterRow1 = mock(FilterRow.class);
        when(mockFilterRow1.isEmpty()).thenReturn(false);
        TestCustomFilterType mockFilterType1 = mock(TestCustomFilterType.class);
        when(mockFilterType1.getValidatorName()).thenReturn(Optional.empty());
        when(mockFilterRow1.getFilterType()).thenReturn(mockFilterType1);
        FilterExpression mockFilterExpression1 = mock(FilterExpression.class);
        when(mockFilterRow1.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression1));

        FilterRow mockFilterRow2 = mock(FilterRow.class);
        when(mockFilterRow2.isEmpty()).thenReturn(false);
        TestCustomFilterType mockFilterType2 = mock(TestCustomFilterType.class);
        when(mockFilterType2.getValidatorName()).thenReturn(Optional.empty());
        when(mockFilterRow2.getFilterType()).thenReturn(mockFilterType2);
        FilterExpression mockFilterExpression2 = mock(FilterExpression.class);
        when(mockFilterRow2.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression2));

        doAnswer(invocation -> Stream.of(mockFilterRow1, mockFilterRow2)).when(testObj).getChildren();

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasErrors()).thenReturn(true);
        when(report.hasWarnings()).thenReturn(false);
        when(report.getErrors()).thenReturn(Collections.singletonList("error message"));
        when(filterValidationExecutor.validate("validator", mockAnd)).thenReturn(report);

        testObj.search();

        verify(filterValidationExecutor).validate("validator", mockAnd);

        ArgumentCaptor<JsonObject> jsonCaptor = ArgumentCaptor.forClass(JsonObject.class);
        verify(mockElement).setPropertyJson(eq("_validationResult"), jsonCaptor.capture());

        JsonObjectBuilder compositeValidationResult = jsonObject()
            .put("hasErrors", true)
            .put("errors", jsonArray().add("error message").build())
            .put("hasWarnings", false)
            .put("errorHeader", "Filtering resulted errors:")
            .put("warnHeader", "Filtering resulted warnings:");
        assertEquals(compositeValidationResult, jsonObjectFrom(jsonCaptor.getValue()));

        verify(dataSource, times(0)).registerFilter(any());

        verifyZeroInteractions(dataSourceManager);
    }

    @Test
    @DisplayName("Should still register filter if only warnings are resulted from validators")
    public void should_still_register_filter_if_only_warnings_are_resulted_from_validators() {
        Element mockElement = mock(Element.class);
        when(testObj.getElement()).thenReturn(mockElement);
        doReturn(dataSource).when(testObj).getDataSource();

        when(testObj.getValidatorName()).thenReturn("validator");
        when(testObj.getDataSourceId()).thenReturn("testDS");
        when(testObj.getDefaultQueryName()).thenReturn("testQuery");

        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        AndFilterExpression mockAnd = mock(AndFilterExpression.class);
        when(expressionFactory.create(AndFilterExpression.class)).thenReturn(mockAnd);

        TestFilterRow mockFilterRow1 = mock(TestFilterRow.class);
        when(mockFilterRow1.isEmpty()).thenReturn(false);
        FilterExpression mockFilterExpression1 = mock(FilterExpression.class);
        when(mockFilterRow1.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression1));
        TestCustomFilterType mockFilterType1 = mock(TestCustomFilterType.class);
        when(mockFilterType1.getValidatorName()).thenReturn(Optional.of("validator1"));
        when(mockFilterRow1.getFilterType()).thenReturn(mockFilterType1);

        FilterRow mockFilterRow2 = mock(FilterRow.class);
        when(mockFilterRow2.isEmpty()).thenReturn(false);
        FilterExpression mockFilterExpression2 = mock(FilterExpression.class);
        when(mockFilterRow2.createFilterExpression()).thenReturn(Optional.of(mockFilterExpression2));
        FilterType mockFilterType2 = mock(FilterType.class);
        when(mockFilterType2.getValidatorName()).thenReturn(Optional.of("validator2"));
        when(mockFilterRow2.getFilterType()).thenReturn(mockFilterType2);

        doAnswer(invocation -> Stream.of(mockFilterRow1, mockFilterRow2)).when(testObj).getChildren();

        FilterValidationReport report1 = mock(FilterValidationReport.class);
        when(report1.isInvalid()).thenReturn(true);
        when(report1.hasErrors()).thenReturn(false);
        when(report1.hasWarnings()).thenReturn(true);
        when(filterValidationExecutor.validate("validator1", mockFilterExpression1)).thenReturn(report1);

        FilterValidationReport report2 = mock(FilterValidationReport.class);
        when(report2.isInvalid()).thenReturn(true);
        when(report2.hasErrors()).thenReturn(false);
        when(report2.hasWarnings()).thenReturn(true);
        when(report2.getErrors()).thenReturn(Collections.emptyList());
        when(report2.getWarnings()).thenReturn(Collections.singletonList("warning message"));
        when(filterValidationExecutor.validate("validator2", mockFilterExpression2)).thenReturn(report2);

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasErrors()).thenReturn(false);
        when(report.hasWarnings()).thenReturn(true);
        when(report.getWarnings()).thenReturn(Collections.singletonList("composite warning message"));
        when(filterValidationExecutor.validate("validator", mockAnd)).thenReturn(report);

        testObj.search();

        verify(mockFilterRow1).reportValidationResult(report1);

        ArgumentCaptor<JsonObject> jsonCaptor = ArgumentCaptor.forClass(JsonObject.class);
        verify(mockElement).setPropertyJson(eq("_validationResult"), jsonCaptor.capture());
        JsonObjectBuilder expectedBuilder = jsonObject()
            .put("hasErrors", false)
            .put("hasWarnings", true)
            .put("warnings",
                jsonArray()
                    .add("warning message")
                    .add("composite warning message")
                    .build())
            .put("errorHeader", "Filtering resulted errors:")
            .put("warnHeader", "Filtering resulted warnings:");
        assertEquals(expectedBuilder, jsonObjectFrom(jsonCaptor.getValue()));

        ArgumentCaptor<CommonDataSourceSelector> selectorCaptor = ArgumentCaptor.forClass(CommonDataSourceSelector.class);
        verify(dataSourceManager).requestRefresh(eq("testDS"), selectorCaptor.capture());
        assertEquals("testQuery", selectorCaptor.getValue().getDefaultQuery());
        assertSame(testObj, selectorCaptor.getValue().getContextHolder());
    }

    @Test
    @DisplayName("Should run value-not-null filter validator if there are no custom validators specified")
    void test_should_run_value_not_null_filter_validator_if_there_are_no_custom_validators_specified() {
        Element mockElement = mock(Element.class);
        FilterRow<?> filterRow = mock(FilterRow.class, RETURNS_DEEP_STUBS);
        BinaryOperandFilterExpression<?> filterExpression = mock(BinaryOperandFilterExpression.class);
        AndFilterExpression<?, ?> mockAnd = mock(AndFilterExpression.class);

        when(testObj.getElement()).thenReturn(mockElement);
        when(filterRow.getFilterType().getValidatorName()).thenReturn(Optional.empty());
        when(filterRow.createFilterExpression()).thenReturn(Optional.of(filterExpression));

        doReturn(expressionFactory).when(dataSource).getFilterExpressionFactory();
        doReturn(mockAnd).when(expressionFactory).create(AndFilterExpression.class);

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasErrors()).thenReturn(true);
        when(report.hasWarnings()).thenReturn(false);
        when(report.getErrors()).thenReturn(Collections.singletonList("NULL value"));
        when(report.getWarnings()).thenReturn(Collections.emptyList());
        when(filterValidationExecutor.validate(ValueNotNullFilterValidator.NAME, filterExpression)).thenReturn(report);

        testObj.search(Collections.singletonList(filterRow), dataSource);

        ArgumentCaptor<JsonObject> jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JsonObject.class);
        verify(mockElement).setPropertyJson(eq(CommonFilter.VALIDATION_RESULT_PROPERTY_NAME), jsonObjectArgumentCaptor.capture());

        JsonObject reportJson = jsonObjectArgumentCaptor.getValue();

        assertNotNull(reportJson);
        assertTrue(reportJson.getBoolean("hasErrors"));
        assertFalse(reportJson.getBoolean("hasWarnings"));
        assertEquals("NULL value", reportJson.getArray("errors").getString(0));
    }

    private Stream<JsonObject> streamFromJsonArray(JsonArray jsonArray) {
        return IntStream.range(0, jsonArray.length())
            .mapToObj(jsonArray::get)
            .map(JsonObject.class::cast);
    }

    private FilterPropertiesReadyEvent createFilterPropertiesReadyEvent(JsonArray filterTypesDescriptors) {
        return new FilterPropertiesReadyEvent(
            testObj, true, "testDataSourceId", "testDataSource", null, null, filterTypesDescriptors, Json.createArray(), false
        );
    }

    private FilterPropertiesReadyEvent createFilterPropertiesReadyEvent(JsonArray filterTypesDescriptors,
                                                                        JsonArray defaultFilterDescriptors,
                                                                        boolean autoSearch) {
        return new FilterPropertiesReadyEvent(
            testObj, true, "testDataSourceId", "testDataSource", null, null, filterTypesDescriptors, defaultFilterDescriptors, autoSearch
        );
    }

    private FilterPropertiesReadyEvent createFilterPropertiesReadyEventWithValidator(JsonArray filterTypesDescriptors) {
        return new FilterPropertiesReadyEvent(
            testObj, true, "testDataSourceId", "testDataSource", null, "testValidator", filterTypesDescriptors, Json.createArray(), false
        );
    }

    private ComponentEventBus getRealEventBus() {
        return ((TestCommonFilter) testObj).getEventBus();
    }

    private void mockEventBusOnTestObject() {
        when(((TestCommonFilter) testObj).getEventBus()).thenReturn(mockEventBus);
    }


    @CustomFilterType("testCustomFilterType")
    public static class TestCustomFilterType implements FilterType {

        private static RuntimeException initializeThrows;

        private JsonObject initializedWith;
        private boolean createFilterRowCalled;

        private FilterExpressionFactory expressionFactory;
        private BindingMetadataProvider metadataProvider;

        private DefaultFilterDescriptor defaultFilterDescriptor;

        @Override
        public void initialize(@NotNull JsonObject frontendValues) {
            if (initializeThrows != null) {
                throw initializeThrows;
            }
            this.initializedWith = frontendValues;
        }

        @Override
        public String getLabel() {
            return "my custom filter";
        }

        @Override
        public FilterRow createFilterRow(FilterExpressionFactory expressionFactory, BindingMetadataProvider metadataProvider) {
            createFilterRowCalled = true;
            this.expressionFactory = expressionFactory;
            this.metadataProvider = metadataProvider;
            return new TestFilterRow(this, mock(FilterExpressionFactory.class), mock(BindingMetadata.class));
        }
    }

    @CustomFilterType("testCustomDefSupportFilterType")
    public static class TestCustomDefSupportFilterType extends TestCustomFilterType implements SupportsDefaultFilter {

        private DefaultFilterDescriptor defaultFilterDescriptor;

        @Override
        public String getFilterPath() {
            return "test.path";
        }

        @Override
        public void setDefaultFilterDescriptor(DefaultFilterDescriptor defaultFilterDescriptor) {
            this.defaultFilterDescriptor = defaultFilterDescriptor;
        }

    }

    private static class TestFilterRow extends FilterRow<TestCustomFilterType> implements HasFilterValidationReportSupport {
        private TestFilterRow(@NotNull TestCustomFilterType filterType, @NotNull FilterExpressionFactory expressionFactory, @NotNull BindingMetadata metadata) {
            super(filterType, expressionFactory, metadata);
        }

        @Override
        public Optional<FilterExpression<?>> createFilterExpression() {
            return Optional.empty();
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public void reportValidationResult(FilterValidationReport report) {

        }
    }


    private static class TestCommonFilter extends CommonFilter {
        @Override
        protected ComponentEventBus getEventBus() {
            return super.getEventBus();
        }
    }

    @Test
    @DisplayName("Should setup and add default filters")
    void should_setup_and_add_default_filters() {
        testObj.onAttached();

        doReturn(dataSource).when(testObj).getDataSource();
        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        when(dataSource.getMetadataProvider()).thenReturn(metadataProvider);

        doReturn(Collections.singleton(TestCustomDefSupportFilterType.class)).when(memberScanner).findClassesByAnnotation(CustomFilterType.class);

        JsonArray filterPropertyDescriptorsArray = jsonArray()
            .add(jsonObject().put("type", "testCustomDefSupportFilterType").build())
            .build();

        JsonArray defaultFilterDescriptorsArray = jsonArray()
            .add(jsonObject()
                .put("added", true)
                .put("filter", "test.path")
                .put("operator", "=")
                .put("value", "testValue")
                .build())
            .build();

        FilterPropertiesReadyEvent readyEvent = createFilterPropertiesReadyEvent(filterPropertyDescriptorsArray, defaultFilterDescriptorsArray, false);
        ComponentUtil.fireEvent(testObj, readyEvent);

        FilterType filterType = readyEvent.getFilterTypes().get(0);
        assertTrue(filterType instanceof TestCustomFilterType);
        assertNotNull(((TestCustomDefSupportFilterType) filterType).defaultFilterDescriptor, "Default filter descriptor should be set");
        assertTrue(((TestCustomFilterType) filterType).createFilterRowCalled, "Create filter row should be called");
    }

    @Test
    @DisplayName("Should run auto search if auto-search is true")
    void test_should_run_auto_search_if_auto_search_is_true() {
        testObj.onAttached();

        doNothing().when(testObj).search(any(), any());

        doReturn(dataSource).when(testObj).getDataSource();
        when(dataSource.getFilterExpressionFactory()).thenReturn(expressionFactory);
        when(dataSource.getMetadataProvider()).thenReturn(metadataProvider);

        doReturn(Collections.singleton(TestCustomDefSupportFilterType.class)).when(memberScanner).findClassesByAnnotation(CustomFilterType.class);

        JsonArray filterPropertyDescriptorsArray = jsonArray()
            .add(jsonObject().put("type", "testCustomDefSupportFilterType").build())
            .build();

        JsonArray defaultFilterDescriptorsArray = jsonArray()
            .add(jsonObject()
                .put("added", true)
                .put("filter", "test.path")
                .put("operator", "=")
                .put("value", "testValue")
                .build())
            .build();

        FilterPropertiesReadyEvent readyEvent = createFilterPropertiesReadyEvent(filterPropertyDescriptorsArray, defaultFilterDescriptorsArray, true);
        ComponentUtil.fireEvent(testObj, readyEvent);

        verify(testObj).search(any(), any());
    }
}
