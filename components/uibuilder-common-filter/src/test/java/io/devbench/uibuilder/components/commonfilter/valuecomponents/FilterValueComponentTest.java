/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.valuecomponents;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentException;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentInternalComponentInstantiationException;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import io.devbench.uibuilder.data.common.filter.operator.OperatorValueType;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({MockitoExtension.class})
class FilterValueComponentTest {

    private FilterValueComponent<TextField, ?> testObj;

    @Test
    @DisplayName("should have the minimum components instantiated after attached")
    void test_should_have_the_minimum_components_instantiated_after_attached() {
        testObj = new StringFilterValueComponent();
        testObj.setMinimumSize(3);

        assertEquals(0, testObj.getComponents().size());
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));
        assertEquals(3, testObj.getComponents().size());
    }

    @Test
    @DisplayName("should set all remove buttons visibility on setDynamic")
    void test_should_set_all_remove_buttons_visibility_on_set_dynamic() {
        testObj = new NumberFilterValueComponent();
        testObj.setMinimumSize(2);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        testObj.getRemoveButtons().forEach(removeButton -> {
            if (removeButton.isVisible()) {
                fail("Remove button should not be visible at this point");
            }
        });

        testObj.setDynamic(true);

        testObj.getRemoveButtons().forEach(removeButton -> {
            if (!removeButton.isVisible()) {
                fail("Remove button should be visible at this point");
            }
        });
    }

    @Test
    @DisplayName("should set the minimumSize and dynamic according to operator value type")
    void test_should_set_the_minimum_size_and_dynamic_according_to_operator_value_type() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.TWO);
        assertEquals(2, testObj.getMinimumSize());
        assertFalse(testObj.isDynamic());
        testObj.setOperatorValueType(OperatorValueType.SINGLE);
        assertEquals(1, testObj.getMinimumSize());
        assertFalse(testObj.isDynamic());
        testObj.setOperatorValueType(OperatorValueType.ANY);
        assertEquals(0, testObj.getMinimumSize());
        assertTrue(testObj.isDynamic());
        testObj.setOperatorValueType(OperatorValueType.NONE);
        assertEquals(0, testObj.getMinimumSize());
        assertFalse(testObj.isDynamic());
    }

    @Test
    @DisplayName("should call the value change listener if internal value change occures")
    void test_should_call_the_value_change_listener_if_internal_value_change_occures() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.TWO);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        TextField textField1 = testObj.getComponents().get(0);
        TextField textField2 = testObj.getComponents().get(1);

        AtomicInteger counter = new AtomicInteger(0);

        Registration registration = testObj.addValueChangeListener(event -> {
            assertSame(textField1, event.getHasValue());
            assertEquals("5", event.getValue());
            counter.incrementAndGet();
        });
        textField1.setValue("5");
        registration.remove();

        registration = testObj.addValueChangeListener(event -> {
            assertSame(textField2, event.getHasValue());
            assertEquals("10", event.getValue());
            counter.incrementAndGet();
        });
        textField2.setValue("10");
        registration.remove();

        assertEquals(2, counter.get(), "Listener call counter should be 2");
    }

    @Test
    @DisplayName("should add removable component if add button clicked")
    void test_should_add_removable_component_if_add_button_clicked() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.ANY);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        assertEquals(0, testObj.getComponents().size());

        ComponentUtil.fireEvent(getAddButton(testObj), new ClickEvent<>(testObj));

        assertEquals(1, testObj.getComponents().size());
    }

    @Test
    @DisplayName("should remove the removable component if the remove button clicked")
    void test_should_remove_the_removable_component_if_the_remove_button_clicked() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.ANY);
        testObj.setMinimumSize(3);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));
        testObj.setMinimumSize(2);

        assertEquals(3, testObj.getComponents().size());
        assertEquals(3, testObj.getRemoveButtons().size());

        ComponentUtil.fireEvent(testObj.getRemoveButtons().get(0), new ClickEvent<>(testObj));

        assertEquals(2, testObj.getComponents().size());
        assertEquals(2, testObj.getRemoveButtons().size());
    }

    @Test
    @DisplayName("should not remove removable component on remove button click if there will be less than the minimum component size")
    void test_should_not_remove_removable_component_on_remove_button_click_if_there_will_be_less_than_the_minimum_component_size() {
        AtomicBoolean removeComponentFailedMethodInvoked = new AtomicBoolean(false);

        testObj = new NumberFilterValueComponent() {
            @Override
            public void onRemoveComponentFailed(ClickEvent<Button> removeButtonClickEvent) {
                super.onRemoveComponentFailed(removeButtonClickEvent);
                removeComponentFailedMethodInvoked.set(true);
            }
        };

        testObj.setOperatorValueType(OperatorValueType.ANY);
        testObj.setMinimumSize(3);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        assertEquals(3, testObj.getComponents().size());
        assertEquals(3, testObj.getRemoveButtons().size());

        ComponentUtil.fireEvent(testObj.getRemoveButtons().get(0), new ClickEvent<>(testObj));

        assertTrue(removeComponentFailedMethodInvoked.get(), "onRemoveComponentFailed should have been invoked");
    }

    @Test
    @DisplayName("should get single value with single value compoennt")
    void test_should_get_single_value_with_single_value_compoennt() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.SINGLE);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));
        testObj.getComponents().get(0).setValue("5");

        Object value = testObj.getValue();

        assertNotNull(value);
        assertTrue(value instanceof String, "Value should be a string");
        assertEquals("5", value);
    }

    @Test
    @DisplayName("should throw exception if trying to get value from a non value component")
    void test_should_throw_exception_if_trying_to_get_value_from_a_non_value_component() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.NONE);

        assertThrows(FilterValueComponentException.class, () -> testObj.getValue(), "Should throw exception");
    }

    @Test
    @DisplayName("should get collection as value with multi value component")
    void test_should_get_collection_as_value_with_multi_value_component() {
        testObj = new NumberFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.TWO);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));
        testObj.getComponents().get(0).setValue("5");
        testObj.getComponents().get(1).setValue("10");

        Object value = testObj.getValue();

        assertNotNull(value);
        assertTrue(value instanceof Collection, "Value should be a collection");
        Collection collection = (Collection) value;
        assertEquals(2, collection.size());

        Iterator iterator = collection.iterator();
        Object value1 = iterator.next();
        Object value2 = iterator.next();

        assertNotNull(value1);
        assertNotNull(value2);
        assertEquals("5", value1);
        assertEquals("10", value2);
    }

    @Test
    @DisplayName("should set single value if there is already a component")
    void test_should_set_single_value_if_there_is_already_a_component() {
        UI.getCurrent().setLocale(Locale.getDefault());

        FilterValueComponent<DatePicker, LocalDate> testObj = new LocalDateFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.SINGLE);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        DatePicker alreadyCreatedComponent = testObj.getComponents().get(0);

        LocalDate oldComponentValue = alreadyCreatedComponent.getValue();

        LocalDate date = LocalDate.now().plus(2, ChronoUnit.DAYS);
        testObj.setValue(date);

        LocalDate newComponentValue = alreadyCreatedComponent.getValue();

        assertNotSame(oldComponentValue, newComponentValue);
        assertEquals(date, newComponentValue);
    }

    @Test
    @DisplayName("should create component and set single value if there is no component")
    void test_should_create_component_and_set_single_value_if_there_is_no_component() {
        UI.getCurrent().setLocale(Locale.getDefault());

        FilterValueComponent<DatePicker, LocalDate> testObj = new LocalDateFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.SINGLE);

        assertTrue(testObj.getComponents().isEmpty(), "Components should be empty at this point");

        LocalDate date = LocalDate.now().plus(2, ChronoUnit.DAYS);
        testObj.setValue(date);

        assertEquals(1, testObj.getComponents().size());

        DatePicker datePicker = testObj.getComponents().get(0);
        assertNotNull(datePicker);
        assertEquals(date, datePicker.getValue());
    }

    @Test
    @DisplayName("should throw exception when trying to set single value on a multi value component")
    void test_should_throw_exception_when_trying_to_set_single_value_on_a_multi_value_component() {
        testObj = new StringFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.TWO);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        assertThrows(FilterValueComponentException.class, () -> testObj.setValue("value"), "Should throw exception");
    }

    @Test
    @DisplayName("should recreate all components and set the collection's values")
    void test_should_recreate_all_components_and_set_the_collection_s_values() {
        testObj = new StringFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.ANY);
        ComponentUtil.fireEvent(getAddButton(testObj), new ClickEvent<>(testObj));

        assertEquals(1, testObj.getComponents().size());

        List<String> values = Arrays.asList("one", "two", "three");
        testObj.setValue(values);

        assertEquals(3, testObj.getComponents().size());

        String value1 = testObj.getComponents().get(0).getValue();
        String value2 = testObj.getComponents().get(1).getValue();
        String value3 = testObj.getComponents().get(2).getValue();

        assertEquals("one", value1);
        assertEquals("two", value2);
        assertEquals("three", value3);
    }

    @Test
    @DisplayName("should set all components and button to disabled and back to enabled when callign setReadOnly")
    void test_should_set_all_components_and_button_to_disabled_and_back_to_enabled_when_callign_set_read_only() {
        testObj = new StringFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.ANY);
        ComponentUtil.fireEvent(getAddButton(testObj), new ClickEvent<>(testObj));

        assertEquals(1, testObj.getComponents().size());

        List<String> values = Arrays.asList("one", "two", "three");
        testObj.setValue(values);

        assertEquals(3, testObj.getComponents().size());

        String value1 = testObj.getComponents().get(0).getValue();
        String value2 = testObj.getComponents().get(1).getValue();
        String value3 = testObj.getComponents().get(2).getValue();

        assertEquals("one", value1);
        assertEquals("two", value2);
        assertEquals("three", value3);

        assertFalse(testObj.isReadOnly(), "ReadOnly should be false");

        testObj.setReadOnly(true);

        assertTrue(testObj.isReadOnly(), "FilterValueComponent should be read only");
        assertTrue(testObj.getComponents().get(0).isReadOnly(), "Component (0) should be read only");
        assertTrue(testObj.getComponents().get(1).isReadOnly(), "Component (1) should be read only");
        assertTrue(testObj.getComponents().get(2).isReadOnly(), "Component (2) should be read only");
        assertFalse(testObj.getRemoveButtons().get(0).isEnabled(), "Remove button (0) should NOT be enabled");
        assertFalse(testObj.getRemoveButtons().get(1).isEnabled(), "Remove button (1) should NOT be enabled");
        assertFalse(testObj.getRemoveButtons().get(2).isEnabled(), "Remove button (2) should NOT be enabled");
        assertFalse(testObj.getAddButton().isEnabled(), "Add button should NOT be enabled");

        testObj.setReadOnly(false);

        assertFalse(testObj.isReadOnly(), "FilterValueComponent should NOT be read only");
        assertFalse(testObj.getComponents().get(0).isReadOnly(), "Component (0) should be NOT read only");
        assertFalse(testObj.getComponents().get(1).isReadOnly(), "Component (1) should be NOT read only");
        assertFalse(testObj.getComponents().get(2).isReadOnly(), "Component (2) should be NOT read only");
        assertTrue(testObj.getRemoveButtons().get(0).isEnabled(), "Remove button (0) should be enabled");
        assertTrue(testObj.getRemoveButtons().get(1).isEnabled(), "Remove button (1) should be enabled");
        assertTrue(testObj.getRemoveButtons().get(2).isEnabled(), "Remove button (2) should be enabled");
        assertTrue(testObj.getAddButton().isEnabled(), "Add button should be enabled");
    }

    @Test
    @DisplayName("should throw exception if trying to set a two value component with a different size collection")
    void test_should_throw_exception_if_trying_to_set_a_two_value_component_with_a_different_size_collection() {
        testObj = new StringFilterValueComponent();
        testObj.setOperatorValueType(OperatorValueType.TWO);
        ComponentUtil.fireEvent(testObj, new AttachEvent(testObj, true));

        assertThrows(FilterValueComponentException.class, () -> testObj.setValue(Arrays.asList("one", "two", "three")), "Should throw exception");
    }

    @Test
    @DisplayName("should return default priority")
    void test_should_return_default_priority() {
        testObj = new StringFilterValueComponent();
        assertEquals(FilterValueComponent.DEFAULT_PRIORITY, testObj.getPriority());
    }

    @Test
    @DisplayName("should throw exception if internal component cannot be instantiated")
    void test_should_throw_exception_if_internal_component_cannot_be_instantiated() {
        FilterValueComponent<NoninstanctiableComponent, String> customTestObj = new NoninstantiableFilterValueComponent();

        assertThrows(
            FilterValueComponentInternalComponentInstantiationException.class,
            () -> ComponentUtil.fireEvent(customTestObj, new AttachEvent(customTestObj, true)),
            "Should throw exception");
    }

    @Test
    @DisplayName("should init component with property minimalSize and dynamic flag by operator value type")
    void test_should_init_component_with_property_minimal_size_and_dynamic_flag_by_operator_value_type() {
        testObj = new FilterValueComponent<TextField, Object>(TextField.class, OperatorValueType.TWO) {
            @Override
            public List<OperatorType> getOperators() {
                return Collections.emptyList();
            }

            @Override
            public boolean isApplicableOnPropertyType(Class<?> propertyType) {
                return true;
            }
        };

        assertEquals(2, testObj.getMinimumSize());
        assertFalse(testObj.isDynamic());
    }

    private Button getAddButton(FilterValueComponent<?, ?> filterValueComponent) {
        Optional<Button> addButton = filterValueComponent.getAddButtonContainer().getChildren()
            .filter(component -> component instanceof Button)
            .map(Button.class::cast)
            .findFirst();

        if (!addButton.isPresent()) {
            fail("Could not retrieve add button from add button container");
        }

        return addButton.get();
    }

    public static class NoninstanctiableComponent extends TextField {
        NoninstanctiableComponent() throws InstantiationException {
            throw new InstantiationException();
        }
    }

    public static class NoninstantiableFilterValueComponent extends FilterValueComponent<NoninstanctiableComponent, String> {

        NoninstantiableFilterValueComponent() {
            super(NoninstanctiableComponent.class);
        }

        @Override
        public List<OperatorType> getOperators() {
            return Arrays.asList(OperatorType.EQUALS, OperatorType.LIKE);
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }

    }
}
