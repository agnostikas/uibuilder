/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filterrow;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.commonfilter.exceptions.CustomFilterValueComponentNotFoundException;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentInvalidDefaultValuesSizeException;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentNotFoundException;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentRequiredException;
import io.devbench.uibuilder.components.commonfilter.filtertype.DefaultFilterType;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.CustomFilterValueComponent;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.EnumFilterValueComponent;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.FilterValueComponent;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationReport;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import io.devbench.uibuilder.data.common.filter.operator.OperatorValueType;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
public class DefaultFilterRowTest {

    @Mock
    private FilterExpressionFactory expressionFactory;

    @Mock
    private BindingMetadata metadata;

    @Mock
    private DefaultFilterType filterType;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    private DefaultFilterRow testObj;

    @BeforeEach
    private void setup() {
        when(filterType.getLabel()).thenReturn("test label");
    }

    @AfterEach
    private void cleanup() {
        TestDefaultFilterRow.filterComponent = new MockFilterValueComponent();
        MockFilterValueComponent.operatorTypes = Collections.emptyList();
    }

    @Test
    @DisplayName("Should set basic configuration on default components")
    public void should_set_basic_configuration_on_default_components() {
        when(filterType.getLabel()).thenReturn("test label");

        createTestObj();

        assertAll(
            () -> assertEquals("test label", testObj.getLabelComponent().getValue()),
            () -> assertTrue(testObj.getLabelComponent().isReadOnly()),
            () -> assertEquals("label-component", testObj.getLabelComponent().getClassName()),
            () -> assertEquals("Operator:", testObj.getOperatorsComboBox().getPlaceholder()),
            () -> assertEquals("operators-component", testObj.getOperatorsComboBox().getClassName()),
            () -> assertEquals("filter-component-holder", testObj.getFilterValueComponentHolder().getClassName()),
            () -> assertTrue(testObj.getContentHolder().getChildren().allMatch(component ->
                Arrays.asList(testObj.getLabelComponent(), testObj.getOperatorsComboBox(), testObj.getFilterValueComponentHolder()).contains(component)))
        );
    }

    @Test
    @DisplayName("Should create the filter value component using the supplier and setup the operators according to the value component's definitions, when there is no custom value component specified")
    public void should_create_the_filter_value_component_using_the_supplier_and_setup_the_operators_according_to_the_value_component_s_definitions_when_there_is_no_custom_value_component_specified() {
        createTestObjWithLikeOperator();
        setOperationType(OperatorType.LIKE);

        assertAll(
            () -> assertEquals(MockFilterValueComponent.class, testObj.getFilterValueComponent().orElseThrow(AssertionError::new).getClass()),
            () -> assertEquals(1, testObj.getFilterValueComponentHolder().getChildren().count()),
            () -> assertTrue(testObj.getFilterValueComponentHolder().getChildren().allMatch(component -> component instanceof MockFilterValueComponent)),
            () -> assertEquals(1, ((ListDataProvider<OperatorType>) testObj.getOperatorsComboBox().getDataProvider()).getItems().size()),
            () -> assertEquals(OperatorType.LIKE,
                ((ListDataProvider<OperatorType>) testObj.getOperatorsComboBox().getDataProvider()).getItems().iterator().next())
        );
    }

    @Test
    @DisplayName("Should create default filter row")
    void test_should_create_default_filter_row() {
        DefaultFilterDescriptor defaultFilterDescriptor = mock(DefaultFilterDescriptor.class);

        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.LIKE);

        doReturn(null).when(filterType).getValueComponent();
        doReturn(String.class).when(metadata).getPropertyType();
        doReturn(PropertyConverters.getConverterByType(String.class)).when(metadata).getConverter();
        doReturn(defaultFilterDescriptor).when(filterType).getDefaultFilterDescriptor();

        doReturn(true).when(defaultFilterDescriptor).isAdded();
        doReturn(OperatorType.LIKE).when(defaultFilterDescriptor).getOperator();
        doReturn("").when(defaultFilterDescriptor).getFilterPath();
        doReturn(Collections.singletonList("val1")).when(defaultFilterDescriptor).getValues();

        createTestObj();

        Optional<FilterValueComponent<?, ?>> filterValueComponent = testObj.getFilterValueComponent();
        assertTrue(filterValueComponent.isPresent());
        MockFilterValueComponent valueComponent = (MockFilterValueComponent) filterValueComponent.get();
        assertEquals("val1", valueComponent.getValue());
    }

    @Test
    @DisplayName("Should create default filter row with setting the row to read only and non removable")
    void test_should_create_default_filter_row_with_setting_the_row_to_read_only_and_non_removable() {
        DefaultFilterDescriptor defaultFilterDescriptor = mock(DefaultFilterDescriptor.class);

        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.LIKE);

        doReturn(null).when(filterType).getValueComponent();
        doReturn(String.class).when(metadata).getPropertyType();
        doReturn(PropertyConverters.getConverterByType(String.class)).when(metadata).getConverter();
        doReturn(defaultFilterDescriptor).when(filterType).getDefaultFilterDescriptor();

        doReturn(true).when(defaultFilterDescriptor).isReadOnly();
        doReturn(true).when(defaultFilterDescriptor).isNonRemovable();
        doReturn(true).when(defaultFilterDescriptor).isAdded();
        doReturn(OperatorType.LIKE).when(defaultFilterDescriptor).getOperator();
        doReturn("").when(defaultFilterDescriptor).getFilterPath();
        doReturn(Collections.singletonList("val1")).when(defaultFilterDescriptor).getValues();

        createTestObj();

        Optional<FilterValueComponent<?, ?>> filterValueComponent = testObj.getFilterValueComponent();
        assertTrue(filterValueComponent.isPresent());
        MockFilterValueComponent valueComponent = (MockFilterValueComponent) filterValueComponent.get();
        assertEquals("val1", valueComponent.getValue());
        assertTrue(valueComponent.isReadOnly());
        assertTrue(testObj.getOperatorsComboBox().isReadOnly());
        assertFalse(testObj.getRemoveButton().isVisible());
    }

    @Test
    @DisplayName("Should get proper default value to set")
    void test_should_get_proper_default_value_to_set() {
        createTestObj();

        Object defaultValue;
        FilterValueComponent filterValueComponent = mock(FilterValueComponent.class);

        when(filterValueComponent.isDynamic())
            .thenReturn(true)
            .thenReturn(false);
        when(filterValueComponent.getMinimumSize())
            .thenReturn(1)
            .thenReturn(1)
            .thenReturn(1)
            .thenReturn(3)
            .thenReturn(4);

        defaultValue = testObj.getDefaultValueToSet(filterValueComponent, Collections.singletonList("val1"));
        assertTrue(defaultValue instanceof List);
        assertEquals(1, ((List) defaultValue).size());
        assertEquals("val1", ((List) defaultValue).get(0));

        defaultValue = testObj.getDefaultValueToSet(filterValueComponent, Collections.singletonList("val1"));
        assertEquals("val1", defaultValue);

        defaultValue = testObj.getDefaultValueToSet(filterValueComponent, Collections.singletonList("val1"));
        assertEquals("val1", defaultValue);

        FilterValueComponentInvalidDefaultValuesSizeException exception1 = assertThrows(
            FilterValueComponentInvalidDefaultValuesSizeException.class,
            () -> testObj.getDefaultValueToSet(filterValueComponent, Collections.emptyList()), "Should throw exception");

        assertEquals("Cannot set fixed number of values to component (required: 1, defined: 0)", exception1.getMessage());

        defaultValue = testObj.getDefaultValueToSet(filterValueComponent, Arrays.asList("One", "Two", "Three"));
        assertTrue(defaultValue instanceof List);
        assertEquals(3, ((List) defaultValue).size());
        assertEquals("One", ((List) defaultValue).get(0));
        assertEquals("Two", ((List) defaultValue).get(1));
        assertEquals("Three", ((List) defaultValue).get(2));

        FilterValueComponentInvalidDefaultValuesSizeException exception2 = assertThrows(
            FilterValueComponentInvalidDefaultValuesSizeException.class,
            () -> testObj.getDefaultValueToSet(filterValueComponent, Arrays.asList("One", "Two", "Three")), "Should throw exception");

        assertEquals("Cannot set fixed number of values to component (required: 4, defined: 3)", exception2.getMessage());
    }


    @Test
    @DisplayName("Should create the custom filter value component named in the filter type and set the operators based on that component")
    public void should_create_the_custom_filter_value_component_named_in_the_filter_type_and_set_the_operators_based_on_that_component() {
        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.LIKE);
        when(filterType.getValueComponent()).thenReturn("testFilterComponent");
        when(memberScanner.findClassesByAnnotation(CustomFilterValueComponent.class))
            .thenReturn(Collections.singleton(MockCustomFilterValueComponent.class));

        createTestObj();
        setOperationType(OperatorType.LIKE);

        assertAll(
            () -> verify(memberScanner, Mockito.times(2)).findClassesByAnnotation(CustomFilterValueComponent.class),
            () -> assertTrue(testObj.getFilterValueComponent().orElse(null) instanceof MockCustomFilterValueComponent),
            () -> assertEquals(1, ((ListDataProvider<OperatorType>) testObj.getOperatorsComboBox().getDataProvider()).getItems().size()),
            () -> assertEquals(OperatorType.LIKE,
                ((ListDataProvider<OperatorType>) testObj.getOperatorsComboBox().getDataProvider()).getItems().iterator().next())
        );
    }

    @Test
    @DisplayName("Should throw exception when the filter value component named in the filter type cannot be found")
    public void should_throw_exception_when_the_filter_value_component_named_in_the_filter_type_cannot_be_found() {
        when(filterType.getValueComponent()).thenReturn("UNKNOWN_FILTER_COMPONENT");
        when(memberScanner.findClassesByAnnotation(CustomFilterValueComponent.class)).thenReturn(Collections.singleton(MockCustomFilterValueComponent.class));

        assertThrows(CustomFilterValueComponentNotFoundException.class, () -> createTestObj());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should build filter expression based on the selected operator and the filter value")
    public void should_build_filter_expression_based_on_the_selected_operator_and_the_filter_value() {
        ExpressionTypes.Like mockLike = mock(ExpressionTypes.Like.class);
        when(expressionFactory.create(ExpressionTypes.Like.class)).thenReturn(mockLike);
        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.LIKE);
        when(metadata.getPath()).thenReturn("test.path");
        doReturn(PropertyConverters.getConverterByType(String.class)).when(metadata).getConverter();
        when(filterType.getValueComponent()).thenReturn(null);
        doReturn(String.class).when(metadata).getPropertyType();
        createTestObj();
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);

        FilterValueComponent<?, ?> filterValueComponent = getAttachedFilterValueComponent();

        filterValueComponent.getComponents().get(0).setValue("testValue");

        FilterExpression<?> filterExpression = testObj.createFilterExpression().get();

        assertAll(
            () -> assertSame(mockLike, filterExpression),
            () -> verify(mockLike).setPath("test.path"),
            () -> verify(mockLike).setValue("testValue")
        );
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should build filter expression with multi value operator")
    void should_build_filter_expression_with_multi_value_operator() {
        ExpressionTypes.Between mockLike = mock(ExpressionTypes.Between.class);
        when(expressionFactory.create(ExpressionTypes.Between.class)).thenReturn(mockLike);
        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.BETWEEN);
        when(metadata.getPath()).thenReturn("test.path");
        doReturn(PropertyConverters.getConverterByType(Integer.class)).when(metadata).getConverter();
        when(filterType.getValueComponent()).thenReturn(null);
        doReturn(String.class).when(metadata).getPropertyType();
        createTestObj();
        testObj.getOperatorsComboBox().setValue(OperatorType.BETWEEN);

        FilterValueComponent<?, ?> filterValueComponent = testObj.getFilterValueComponent().orElseThrow(AssertionError::new);

        filterValueComponent.setOperatorValueType(OperatorValueType.TWO);
        ComponentUtil.fireEvent(filterValueComponent, new AttachEvent(filterValueComponent, true));

        filterValueComponent.getComponents().get(0).setValue("123");
        filterValueComponent.getComponents().get(1).setValue("321");

        FilterExpression<?> filterExpression = testObj.createFilterExpression().get();

        ArgumentCaptor<Collection> valuesCaptor = ArgumentCaptor.forClass(Collection.class);

        assertAll(
            () -> assertSame(mockLike, filterExpression),
            () -> verify(mockLike).setPath("test.path")
        );
        verify(mockLike).setValues(valuesCaptor.capture());

        Collection value = valuesCaptor.getValue();
        assertNotNull(value);
        assertEquals(2, value.size());

        Iterator iterator = value.iterator();
        Object val1 = iterator.next();
        Object val2 = iterator.next();

        assertEquals(123, val1);
        assertEquals(321, val2);
    }

    @Test
    @DisplayName("Should create value component with higher priority if multiple componenets are applicable")
    public void should_create_value_component_with_higher_priority_if_multiple_componenets_are_applicable() {
        doReturn(TestEnum.class).when(metadata).getPropertyType();
        when(memberScanner.findClassesBySuperType(FilterValueComponent.class))
            .thenReturn(new HashSet<>(Arrays.asList(SameOneFilterValueComponent.class, SameTwoFilterValueComponent.class)));

        testObj = new DefaultFilterRow(filterType, expressionFactory, metadata);
        FilterValueComponent<?, ?> filterValueComponent = testObj.createFilterValueComponent(OperatorValueType.SINGLE);

        assertNotNull(filterValueComponent);
        assertTrue(filterValueComponent instanceof SameTwoFilterValueComponent);
    }

    @Test
    @DisplayName("Should create value component with enum type")
    public void should_create_value_component_with_enum_type() {
        doReturn(TestEnum.class).when(metadata).getPropertyType();
        when(memberScanner.findClassesBySuperType(FilterValueComponent.class))
            .thenReturn(Collections.singleton(TestEnumFilterValueComponent.class));

        testObj = new DefaultFilterRow(filterType, expressionFactory, metadata);
        FilterValueComponent<?, ?> filterValueComponent = testObj.createFilterValueComponent(OperatorValueType.SINGLE);

        assertNotNull(filterValueComponent);
        assertTrue(filterValueComponent instanceof TestEnumFilterValueComponent);
        assertEquals(TestEnum.class, ((TestEnumFilterValueComponent) filterValueComponent).getEnumType());
    }

    @Test
    @DisplayName("Should throw exception if cannot instantiate filter value component")
    public void should_throw_exception_if_cannot_instantiate_filter_value_component() {
        doReturn(String.class).when(metadata).getPropertyType();
        when(memberScanner.findClassesBySuperType(FilterValueComponent.class))
            .thenReturn(Collections.singleton(NotInstantiableFilterValueComponent.class));

        assertThrows(FilterValueComponentNotFoundException.class,
            () -> new DefaultFilterRow(filterType, expressionFactory, metadata),
            "Should throw " + FilterValueComponentNotFoundException.class.getSimpleName() + " exception");
    }

    @Test
    @DisplayName("Should throw exception if there is not filter value component when calling create excpression")
    public void should_throw_exception_if_there_is_not_filter_value_component_when_calling_create_excpression() {
        doReturn(String.class).when(metadata).getPropertyType();
        when(memberScanner.findClassesBySuperType(FilterValueComponent.class))
            .thenReturn(Collections.singleton(SameOneFilterValueComponent.class));
        doReturn("test.path").when(metadata).getPath();

        testObj = new DefaultFilterRow(filterType, expressionFactory, metadata) {
            @Override
            protected Optional<FilterValueComponent<?, ?>> getFilterValueComponent() {
                return Optional.empty();
            }
        };

        setOperationType(OperatorType.LIKE);

        FilterValueComponentRequiredException exception = assertThrows(
            FilterValueComponentRequiredException.class, () -> testObj.createFilterExpression());
        assertNotNull(exception);
        assertNotNull(exception.getMessage());
        assertEquals("Filter value component missing but required (test.path)", exception.getMessage());
    }

    @Test
    @DisplayName("Should return null value into the expression when the value component is null")
    void test_should_return_null_value_into_the_expression_when_the_value_component_is_null() {
        ExpressionTypes.Like<?> mockLike = mock(ExpressionTypes.Like.class);
        doReturn(mockLike).when(expressionFactory).create(ExpressionTypes.Like.class);
        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.LIKE);
        when(metadata.getPath()).thenReturn("test.path");
        doReturn(PropertyConverters.getConverterByType(String.class)).when(metadata).getConverter();
        when(filterType.getValueComponent()).thenReturn(null);
        doReturn(String.class).when(metadata).getPropertyType();
        createTestObj();
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);

        getAttachedFilterValueComponent();

        FilterExpression<?> filterExpression = testObj.createFilterExpression().get();
        assertNotNull(filterExpression);

        ArgumentCaptor<String> likeValueCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockLike).setValue(likeValueCaptor.capture());
        String value = likeValueCaptor.getValue();

        assertNull(value);
    }

    @Test
    @DisplayName("Should return empty optional expression when there is no operator selected")
    public void should_return_empty_optional_expression_when_there_is_no_operator_selected() {
        doReturn(String.class).when(metadata).getPropertyType();
        when(memberScanner.findClassesBySuperType(FilterValueComponent.class))
            .thenReturn(Collections.singleton(SameOneFilterValueComponent.class));

        testObj = new DefaultFilterRow(filterType, expressionFactory, metadata);

        Optional<FilterExpression<?>> filterExpression = testObj.createFilterExpression();
        assertNotNull(filterExpression);
        assertFalse(filterExpression.isPresent(), "Filter expression should not be present");
    }

    @Test
    @DisplayName("Should report isEmpty if the operator is not selected or the value is not set")
    public void should_report_is_empty_if_the_operator_is_not_selected_or_the_value_is_not_set() {
        TestDefaultFilterRow.filterComponent = new MockComboBoxFilterComponent();
        createTestObjWithLikeOperator();
        setOperationType(OperatorType.LIKE);
        FilterValueComponent<?, ?> filterValueComponent = getAttachedFilterValueComponent();

        ((ComboBox<String>) filterValueComponent.getComponents().get(0)).setItems("testValue", "");

        boolean shouldBeTrue = testObj.isEmpty();
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);
        boolean shouldBeStillTrue = testObj.isEmpty();
        filterValueComponent.getComponents().get(0).setValue("testValue");
        boolean shouldBeFalse = testObj.isEmpty();

        assertAll(
            () -> assertTrue(shouldBeTrue, "should be true"),
            () -> assertTrue(shouldBeStillTrue, "should be still true"),
            () -> assertFalse(shouldBeFalse, "should be false")
        );
    }

    private FilterValueComponent<?, ?> getAttachedFilterValueComponent() {
        FilterValueComponent<?, ?> filterValueComponent = testObj.getFilterValueComponent().orElseThrow(AssertionError::new);
        ComponentUtil.fireEvent(filterValueComponent, new AttachEvent(filterValueComponent, true));
        return filterValueComponent;
    }

    @Test
    @DisplayName("Should report validation result containing errors by setting all components to invalid, and setting the tooltip text to contain the error messages")
    public void should_report_validation_result_containing_errors_by_setting_all_components_to_invalid_and_setting_the_tooltip_text_to_contain_the_error_messages() {
        createTestObjWithInOperator();
        setOperationType(OperatorType.LIKE);

        FilterValueComponent<TextField, ?> filterValueComponent = (FilterValueComponent<TextField, ?>) getAttachedFilterValueComponent();
        filterValueComponent.getComponents().add(new TextField());
        filterValueComponent.getComponents().add(new TextField());

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasErrors()).thenReturn(true);
        when(report.getErrors()).thenReturn(Arrays.asList("error message1", null, "error message2"));

        testObj.reportValidationResult(report);

        assertAll(
            () -> assertTrue(testObj.getLabelComponent().isInvalid()),
            () -> assertTrue(testObj.getOperatorsComboBox().isInvalid()),
            () -> assertTrue((filterValueComponent.getComponents().get(0)).isInvalid()),
            () -> assertTrue((filterValueComponent.getComponents().get(1)).isInvalid()),
            () -> assertEquals("error message1", (filterValueComponent.getComponents().get(0)).getErrorMessage()),
            () -> assertEquals("", (filterValueComponent.getComponents().get(1)).getErrorMessage()),
            () -> assertEquals("error message2", (filterValueComponent.getComponents().get(2)).getErrorMessage()),
            () -> assertTrue(testObj.hasClassName("error-indicator")),
            () -> assertEquals("Error: error message1<br/>Error: error message2", testObj.getTooltipText()),
            () -> assertTrue(testObj.getTooltip().isActive())
        );
    }

    @Test
    @DisplayName("Should report validation result containing warnings by setting the tooltip text to contain the warning messages and set the warning indicator")
    public void should_report_validation_result_containing_warnings_by_setting_the_tooltip_text_to_contain_the_warning_messages_and_set_the_warning_indicator() {
        createTestObjWithLikeOperator();

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasWarnings()).thenReturn(true);
        when(report.getWarnings()).thenReturn(Arrays.asList("warning message1", "warning message2"));
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);

        testObj.reportValidationResult(report);

        assertAll(
            () -> assertTrue(testObj.hasClassName("warning-indicator")),
            () -> assertEquals("Warning: warning message1<br/>Warning: warning message2", testObj.getTooltipText()),
            () -> assertTrue(testObj.getTooltip().isActive())
        );
    }

    @Test
    @DisplayName("Should cleanup previous validation indicators, when the operator component changes")
    public void should_cleanup_previous_validation_indicators_when_the_operator_component_changes() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        createTestObjWithLikeOperator();
        setOperationType(OperatorType.LIKE);

        FilterValueComponent<?, ?> filterValueComponent = getAttachedFilterValueComponent();

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasErrors()).thenReturn(true);
        when(report.getErrors()).thenReturn(Arrays.asList("error message1", "error message2"));
        when(report.hasWarnings()).thenReturn(true);
        when(report.getWarnings()).thenReturn(Arrays.asList("warning message1", "warning message2"));
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);
        testObj.reportValidationResult(report);

        Method fireEventMethod = Component.class.getDeclaredMethod("fireEvent", ComponentEvent.class);
        fireEventMethod.setAccessible(true);
        fireEventMethod.invoke(testObj.getOperatorsComboBox(),
            new AbstractField.ComponentValueChangeEvent<ComboBox, OperatorType>(
                testObj.getOperatorsComboBox(),
                testObj.getOperatorsComboBox(),
                OperatorType.LIKE,
                true
            ));

        assertAll(
            () -> assertFalse(testObj.getLabelComponent().isInvalid()),
            () -> assertFalse(testObj.getOperatorsComboBox().isInvalid()),
            () -> assertFalse(((TextField) filterValueComponent.getComponents().get(0)).isInvalid()),
            () -> assertFalse(testObj.hasClassName("warning-indicator")),
            () -> assertFalse(testObj.hasClassName("error-indicator")),
            () -> assertEquals("", testObj.getTooltipText()),
            () -> assertFalse(testObj.getTooltip().isActive())
        );
    }

    @Test
    @DisplayName("Should cleanup previous validation indicators, when the filter value component changes")
    public void should_cleanup_previous_validation_indicators_when_the_filter_value_component_changes() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        createTestObjWithLikeOperator();
        setOperationType(OperatorType.LIKE);
        FilterValueComponent<?, ?> filterValueComponent = getAttachedFilterValueComponent();

        FilterValidationReport report = mock(FilterValidationReport.class);
        when(report.isInvalid()).thenReturn(true);
        when(report.hasErrors()).thenReturn(true);
        when(report.getErrors()).thenReturn(Arrays.asList("error message1", "error message2"));
        when(report.hasWarnings()).thenReturn(true);
        when(report.getWarnings()).thenReturn(Arrays.asList("warning message1", "warning message2"));
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);
        testObj.reportValidationResult(report);

        Method fireEventMethod = Component.class.getDeclaredMethod("fireEvent", ComponentEvent.class);
        fireEventMethod.setAccessible(true);
        TextField firstComponent = (TextField) filterValueComponent.getComponents().get(0);
        fireEventMethod.invoke(firstComponent,
            new AbstractField.ComponentValueChangeEvent<>(
                firstComponent,
                firstComponent,
                "",
                true
            ));

        assertAll(
            () -> assertFalse(testObj.getLabelComponent().isInvalid()),
            () -> assertFalse(testObj.getOperatorsComboBox().isInvalid()),
            () -> assertFalse(((TextField) filterValueComponent.getComponents().get(0)).isInvalid()),
            () -> assertFalse(testObj.hasClassName("warning-indicator")),
            () -> assertFalse(testObj.hasClassName("error-indicator")),
            () -> assertEquals("", testObj.getTooltipText()),
            () -> assertFalse(testObj.getTooltip().isActive())
        );
    }

    private void createTestObjWithLikeOperator() {
        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.LIKE);
        when(filterType.getValueComponent()).thenReturn(null);
        doReturn(String.class).when(metadata).getPropertyType();

        createTestObj();
    }

    private void createTestObjWithInOperator() {
        MockFilterValueComponent.operatorTypes = Collections.singletonList(OperatorType.IN);
        when(filterType.getValueComponent()).thenReturn(null);
        doReturn(String.class).when(metadata).getPropertyType();

        createTestObj();
    }

    @Test
    public void should_only_allow_the_operators_to_be_choosen_that_matches_the_ones_specified_on_the_frontend_and_allowed_in_the_filter_component() {
        TestDefaultFilterRow.filterComponent = new MockComboBoxFilterComponent();
        when(filterType.getOperators()).thenReturn(Collections.singleton(OperatorType.EQUALS));
        createTestObj();
        assertTrue(testObj.getOperatorTypes().isEmpty());
        when(filterType.getOperators()).thenReturn(Collections.singleton(OperatorType.LIKE));
        createTestObj();
        assertFalse(testObj.getOperatorTypes().isEmpty());
        assertEquals(testObj.getOperatorTypes().iterator().next(), OperatorType.LIKE);
    }

    @Test
    void should_not_throw_exception_when_the_operator_is_deleted() {
        createTestObj();
        testObj.getOperatorsComboBox().setValue(OperatorType.LIKE);
        assertDoesNotThrow(() -> testObj.getOperatorsComboBox().setValue(null));
    }

    private void createTestObj() {
        testObj = new TestDefaultFilterRow(filterType, expressionFactory, metadata);
    }

    @SuppressWarnings("unchecked")
    private void setOperationType(OperatorType operationType) {
        testObj.getOperatorsComboBox().setValue(operationType);
        ComponentUtil.fireEvent(testObj.getOperatorsComboBox(),
            new AbstractField.ComponentValueChangeEvent(
                testObj.getOperatorsComboBox(),
                testObj.getOperatorsComboBox(),
                null, false));
    }


    public static class TestDefaultFilterRow extends DefaultFilterRow {

        private static FilterValueComponent<?, ?> filterComponent = new MockFilterValueComponent();

        public TestDefaultFilterRow(@NotNull DefaultFilterType filterType,
                                    @NotNull FilterExpressionFactory expressionFactory,
                                    @NotNull BindingMetadata metadata) {

            super(filterType, expressionFactory, metadata);
        }

        @Override
        protected FilterValueComponent<?, ?> createFilterValueComponent(OperatorValueType operatorValueType) {
            if (StringUtils.isBlank(getFilterType().getValueComponent())) {
                return filterComponent;
            } else {
                return super.createFilterValueComponent(operatorValueType);
            }
        }
    }

    public static class MockFilterValueComponent extends FilterValueComponent<TextField, String> {

        private static List<OperatorType> operatorTypes = Collections.emptyList();

        public MockFilterValueComponent() {
            super(TextField.class);
        }

        @Override
        public List<OperatorType> getOperators() {
            return operatorTypes;
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }
    }

    @CustomFilterValueComponent("testFilterComponent")
    public static class MockCustomFilterValueComponent extends MockFilterValueComponent {
    }

    public static class MockComboBoxFilterComponent extends FilterValueComponent<ComboBox, String> {

        public MockComboBoxFilterComponent() {
            super(ComboBox.class);
        }

        @Override
        public List<OperatorType> getOperators() {
            return Collections.singletonList(OperatorType.LIKE);
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }
    }

    public static class NotInstantiableFilterValueComponent extends FilterValueComponent<TextField, String> {
        public NotInstantiableFilterValueComponent() {
            super(TextField.class);
            throw new UnsupportedOperationException("Should not instantiate");
        }

        @Override
        public List<OperatorType> getOperators() {
            return Collections.singletonList(OperatorType.LIKE);
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }
    }

    public enum TestEnum {
        ONE, TWO
    }

    public static class TestEnumFilterValueComponent extends EnumFilterValueComponent {
        public Class<? extends Enum> getEnumType() {
            return enumType;
        }
    }

    public static class SameOneFilterValueComponent extends FilterValueComponent<TextField, String> {
        public SameOneFilterValueComponent() {
            super(TextField.class);
        }

        @Override
        public List<OperatorType> getOperators() {
            return Collections.singletonList(OperatorType.LIKE);
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }
    }

    public static class SameTwoFilterValueComponent extends FilterValueComponent<TextField, String> {
        public SameTwoFilterValueComponent() {
            super(TextField.class);
        }

        @Override
        public List<OperatorType> getOperators() {
            return Collections.singletonList(OperatorType.LIKE);
        }

        @Override
        public boolean isApplicableOnPropertyType(Class<?> propertyType) {
            return true;
        }

        @Override
        public int getPriority() {
            return DEFAULT_PRIORITY - 1;
        }
    }

}
