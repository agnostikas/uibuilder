/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.events;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import io.devbench.uibuilder.components.commonfilter.CommonFilter;
import lombok.Getter;

@Getter
@DomEvent("filter-addition-requested")
public class FilterAdditionRequestedEvent extends ComponentEvent<CommonFilter> {

    private final String itemId;

    public FilterAdditionRequestedEvent(CommonFilter source, boolean fromClient, @EventData("event.detail.itemId") String itemId) {
        super(source, fromClient);
        this.itemId = itemId;
    }
}
