/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filterrow;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.textfield.TextField;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.commonfilter.exceptions.*;
import io.devbench.uibuilder.components.commonfilter.filtertype.DefaultFilterType;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.CustomFilterValueComponent;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.EnumFilterValueComponent;
import io.devbench.uibuilder.components.commonfilter.valuecomponents.FilterValueComponent;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationReport;
import io.devbench.uibuilder.data.common.filter.comperingfilters.AnyOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.BinaryOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ComperingFilterExpression;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import io.devbench.uibuilder.data.common.filter.operator.OperatorValueType;
import io.devbench.uibuilder.i18n.core.I;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.stream.Collectors.*;


@Slf4j
public class DefaultFilterRow extends FilterRow<DefaultFilterType> implements HasFilterValidationReportSupport {

    private static final String FILTER_OPERATOR_TRANSLATION_CONTEXT = "Filter operator";
    private static final String FILTER_VALIDATION_TRANSLATION_CONTEXT = "Filter validation";

    @Getter(AccessLevel.PACKAGE)
    private final TextField labelComponent;

    @Getter(AccessLevel.PACKAGE)
    private final ComboBox<OperatorType> operatorsComboBox;

    @Getter(AccessLevel.PACKAGE)
    private final Div filterValueComponentHolder;

    @Getter
    private final DefaultFilterType filterType;

    @Getter
    private Collection<OperatorType> operatorTypes;

    private Map<OperatorType, FilterValueComponent<?, ?>> filterValueComponentMap;
    private BindingMetadata metadata;

    public DefaultFilterRow(
        @NotNull DefaultFilterType filterType,
        @NotNull FilterExpressionFactory<FilterExpression<?>> expressionFactory,
        @NotNull BindingMetadata metadata
    ) {
        super(filterType, expressionFactory, metadata);
        this.filterType = filterType;
        this.metadata = metadata;
        this.filterValueComponentMap = new EnumMap<>(OperatorType.class);
        labelComponent = new TextField();
        operatorsComboBox = new ComboBox<>();
        filterValueComponentHolder = new Div();
        setupComponents();

        if (filterType.getDefaultFilterDescriptor() != null) {
            setupOperatorsComboBox();
            setupNonRemovable();
            setupFilterValueComponent();
        }

        add(labelComponent, operatorsComboBox, filterValueComponentHolder);
    }

    private void setupComponents() {
        labelComponent.setValue(filterType.getLabel());
        labelComponent.setReadOnly(true);
        labelComponent.addClassName("label-component");
        operatorsComboBox.setPlaceholder(I.trc(FILTER_OPERATOR_TRANSLATION_CONTEXT, "Operator:"));
        operatorsComboBox.setClassName("operators-component");
        filterValueComponentHolder.setClassName("filter-component-holder");
        FilterValueComponent<?, ?> fixedFilterValueComponent = createFilterValueComponent(OperatorValueType.NONE);
        operatorsComboBox.setItems(fixedFilterValueComponent.getOperators());
        operatorsComboBox.setItemLabelGenerator(OperatorType::getDisplayName);
        operatorTypes = getOperatorTypes(fixedFilterValueComponent.getOperators());
        operatorsComboBox.setItems(operatorTypes);
        operatorsComboBox.addValueChangeListener(this::handleOperatorChangedEvent);
    }

    private void setupOperatorsComboBox() {
        operatorsComboBox.setValue(filterType.getDefaultFilterDescriptor().getOperator());
        if (filterType.getDefaultFilterDescriptor().isReadOnly()) {
            operatorsComboBox.setReadOnly(true);
        }
    }

    private void setupNonRemovable() {
        if (filterType.getDefaultFilterDescriptor().isNonRemovable()) {
            getRemoveButton().setVisible(false);
        }
    }

    private Collection<OperatorType> getOperatorTypes(Collection<OperatorType> operatorList) {
        Set<OperatorType> requestedOperations = filterType.getOperators();
        return requestedOperations.isEmpty() ? operatorList : operatorList.stream().filter(requestedOperations::contains).collect(toList());
    }

    private void setupFilterValueComponent() {
        getFilterValueComponent().ifPresent(filterValueComponent -> {
            DefaultFilterDescriptor defaultFilterDescriptor = filterType.getDefaultFilterDescriptor();

            if (defaultFilterDescriptor.isReadOnly()) {
                filterValueComponent.setReadOnly(true);
            }

            List<Object> convertedValues = defaultFilterDescriptor.getValues()
                .stream()
                .map(getMetadata().getConverter()::convertFrom)
                .collect(toList());

            filterValueComponent.setValue(getDefaultValueToSet(filterValueComponent, convertedValues));
        });
    }

    protected Object getDefaultValueToSet(FilterValueComponent<?, ?> filterValueComponent, List<Object> convertedValues) {
        if (filterValueComponent.isDynamic()) {
            return convertedValues;
        } else {
            int minimumSize = filterValueComponent.getMinimumSize();
            if (minimumSize == 1) {
                if (convertedValues.size() == 1) {
                    return convertedValues.get(0);
                }
            } else if (minimumSize > 1) {
                if (convertedValues.size() == minimumSize) {
                    return convertedValues;
                }
            }
            throw new FilterValueComponentInvalidDefaultValuesSizeException(minimumSize, convertedValues.size());
        }
    }

    private void handleOperatorChangedEvent(HasValue.ValueChangeEvent<OperatorType> event) {
        filterValueComponentHolder.removeAll();

        final OperatorType operatorType = event.getValue();
        if (operatorType != null) {
            final FilterValueComponent<?, ?> filterValueComponent = filterValueComponentMap
                .computeIfAbsent(operatorType, opType -> createFilterValueComponent(opType.getValueType()));

            filterValueComponentHolder.add((Component) filterValueComponent);
        }
    }

    @SuppressWarnings("unchecked")
    FilterValueComponent<?, ?> createFilterValueComponent(OperatorValueType operatorValueType) {
        if (StringUtils.isBlank(filterType.getValueComponent())) {

            Class<?> propertyType = metadata.getPropertyType();
            FilterValueComponent<?, ?> filterValueComponent = MemberScanner
                .getInstance()
                .findClassesBySuperType(FilterValueComponent.class)
                .stream()
                .map(this::tryToCreateFilterValueComponentInstance)
                .filter(Objects::nonNull)
                .filter(c -> c.isApplicableOnPropertyType(propertyType))
                .min(Comparator.comparing(FilterValueComponent::getPriority))
                .orElseThrow(FilterValueComponentNotFoundException::new);

            filterValueComponent.setOperatorValueType(operatorValueType);
            if (filterValueComponent instanceof EnumFilterValueComponent) {
                ((EnumFilterValueComponent) filterValueComponent).setEnumType((Class<Enum>) propertyType);
            }

            return filterValueComponent;
        } else {
            FilterValueComponent customFilterValueComponent = createCustomFilterValueComponent();
            customFilterValueComponent.setOperatorValueType(operatorValueType);
            return customFilterValueComponent;
        }
    }

    private FilterValueComponent createCustomFilterValueComponent() {
        return MemberScanner.getInstance().findClassesByAnnotation(CustomFilterValueComponent.class).stream()
            .filter(FilterValueComponent.class::isAssignableFrom)
            .filter(clazz -> filterType.getValueComponent().equals(clazz.getDeclaredAnnotation(CustomFilterValueComponent.class).value()))
            .map(this::tryToCreateFilterValueComponentInstance)
            .filter(Objects::nonNull)
            .map(FilterValueComponent.class::cast)
            .min(Comparator.comparing(FilterValueComponent::getPriority))
            .orElseThrow(CustomFilterValueComponentNotFoundException::new);
    }

    private FilterValueComponent<?, ?> tryToCreateFilterValueComponentInstance(Class<?> clazz) {
        try {
            return (FilterValueComponent<?, ?>) clazz.newInstance();
        } catch (Exception e) {
            log.warn("Filter value component class cannot be instantiated: " + clazz.getName(), e);
            return null;
        }
    }

    @Override
    public Optional<FilterExpression<?>> createFilterExpression() {
        OperatorType operator = operatorsComboBox.getValue();
        if (operator != null) {
            Optional<FilterValueComponent<?, ?>> filterValueComponent = getFilterValueComponent();
            OperatorValueType valueType = operator.getValueType();
            String path = getMetadata().getPath();
            if (!filterValueComponent.isPresent() && valueType != OperatorValueType.NONE) {
                throw new FilterValueComponentRequiredException(path);
            }

            ComperingFilterExpression<?> expression = getExpressionFactory().create(operator.getExpressionType());
            expression.setPath(path);

            if (valueType == OperatorValueType.SINGLE) {
                ((BinaryOperandFilterExpression) expression).setValue(getConvertedValue(filterValueComponent.get()));
            } else if (valueType == OperatorValueType.TWO || valueType == OperatorValueType.ANY) {
                ((AnyOperandFilterExpression) expression).setValues(getConvertedValues(filterValueComponent.get()));
            } else if (valueType != OperatorValueType.NONE) {
                throw new FilterInvalidExpressionTypeException("Invalid expression type for operand: " + operator.name());
            }

            return Optional.of(expression);
        }
        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    private <T> T getConvertedValue(FilterValueComponent<?, ?> filterValueComponent) {
        Object value = filterValueComponent.getValue();
        return value == null ? null : (T) metadata.getConverter().convertFrom(String.valueOf(value));
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> getConvertedValues(FilterValueComponent<?, ?> filterValueComponent) {
        return ((Collection<Serializable>) filterValueComponent.getValue()).stream()
            .map(String::valueOf)
            .map(value -> (T) metadata.getConverter().convertFrom(value))
            .collect(toList());
    }

    protected Optional<FilterValueComponent<?, ?>> getFilterValueComponent() {
        return Optional.ofNullable(filterValueComponentMap.get(operatorsComboBox.getValue()));
    }

    private boolean isValueMissingButRequired() {
        OperatorType operatorType = operatorsComboBox.getValue();
        return operatorType.getValueType() != OperatorValueType.NONE
            && !getFilterValueComponent()
            .map(FilterValueComponent::getValue)
            .isPresent();

    }

    @Override
    public boolean isEmpty() {
        return operatorsComboBox.isEmpty() || isValueMissingButRequired();
    }

    @Override
    public void reportValidationResult(FilterValidationReport report) {
        if (report.isInvalid()) {
            cleanUpValidationIndicators();
            showTooltip();
            handleValidationErrors(report);
            handleValidationWarnings(report);
            setupValueChangeListenersToCleanUp();
        }
    }

    private void setupValueChangeListenersToCleanUp() {
        operatorsComboBox.addValueChangeListener(event -> {
            cleanUpValidationIndicators();
            event.unregisterListener();
        });

        getFilterValueComponent().ifPresent(component ->
            component.addValueChangeListener(event -> {
                cleanUpValidationIndicators();
                event.unregisterListener();
            }));
    }

    private void handleValidationWarnings(FilterValidationReport report) {
        if (report.hasWarnings()) {
            addWarningIndicator();
            addWarningMessagesToTooltipText(report.getWarnings());
        }
    }

    private void handleValidationErrors(FilterValidationReport report) {
        if (report.hasErrors()) {
            addErrorIndicator();
            setComponentsToInvalid(true, report.getErrors());
            addErrorMessagesToTooltipText(report.getErrors());
        }
    }

    private void cleanUpValidationIndicators() {
        if (hasIndicators()) {
            clearIndicators();
            setComponentsToValid();
            hideTooltip();
            setTooltipText("");
        }
    }

    private void addWarningMessagesToTooltipText(List<String> warningMessages) {
        String warningPrefix = I.trc(FILTER_VALIDATION_TRANSLATION_CONTEXT, "Warning:");
        addMessagesToTooltipText(warningPrefix, warningMessages);
    }

    private void addErrorMessagesToTooltipText(List<String> errorMessages) {
        String errorPrefix = I.trc(FILTER_VALIDATION_TRANSLATION_CONTEXT, "Error:");
        addMessagesToTooltipText(errorPrefix, errorMessages);
    }

    private void addMessagesToTooltipText(String prefix, List<String> messages) {
        setTooltipText(
            Stream.concat(
                Stream.of(getTooltipText())
                    .filter(StringUtils::isNotBlank),
                messages.stream()
                    .filter(StringUtils::isNoneBlank)
                    .map(message -> prefix + " " + message)
            ).collect(Collectors.joining("<br/>"))
        );
    }

    private void setComponentsToValid() {
        setComponentsToInvalid(false, Collections.emptyList());
    }

    private void setComponentsToInvalid(boolean invalid, List<String> errorMessages) {
        labelComponent.setInvalid(invalid);
        operatorsComboBox.setInvalid(invalid);
        int errorMessagesSize = errorMessages.size();
        getFilterValueComponent().ifPresent(filterValueComponent -> {
            List<?> components = filterValueComponent.getComponents();
            for (int componentIndex = 0; componentIndex < components.size(); componentIndex++) {
                Object component = components.get(componentIndex);
                if (component instanceof HasValidation) {
                    HasValidation componentWithValidation = (HasValidation) component;
                    componentWithValidation.setInvalid(invalid);
                    componentWithValidation.setErrorMessage(
                        errorMessagesSize > componentIndex && !StringUtils.isBlank(errorMessages.get(componentIndex)) ?
                            errorMessages.get(componentIndex) : null
                    );
                }
            }

        });
    }

}
