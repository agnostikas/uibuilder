/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter;

import com.vaadin.flow.dom.Element;
import io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.components.commonfilter.filterrow.HasFilterValidationReportSupport;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationExecutor;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationReport;
import io.devbench.uibuilder.data.common.filter.validators.ValueNotNullFilterValidator;
import io.devbench.uibuilder.i18n.core.I;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import static io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder.*;


class FilterRowsValidator {

    private final List<String> validationErrors;

    private final List<String> validationWarnings;

    private final List<FilterRow<?>> filterRows;

    FilterRowsValidator(List<FilterRow<?>> filterRows) {
        this.filterRows = filterRows;
        validationWarnings = new ArrayList<>();
        validationErrors = new ArrayList<>();
    }

    static void clearValidationOnFrontend(Element element) {
        if (element.getPropertyRaw(CommonFilter.VALIDATION_RESULT_PROPERTY_NAME) != null) {
            new FilterRowsValidator(Collections.emptyList()).reportToFrontend(element);
        }
    }

    boolean isValid() {
        return validationErrors.isEmpty();
    }

    private void setValidationErrorsAndWarnings(FilterValidationReport report, FilterRow<?> filterRow) {
        if (filterRow instanceof HasFilterValidationReportSupport) {
            ((HasFilterValidationReportSupport) filterRow).reportValidationResult(report);
        } else {
            if (report.hasErrors()) {
                validationErrors.addAll(report.getErrors());
            }
            if (report.hasWarnings()) {
                validationWarnings.addAll(report.getWarnings());
            }
        }
    }

    private Pair<Optional<FilterExpression<?>>, Optional<FilterValidationReport>> createValidatedExpressionWithReport(FilterRow<?> filterRow) {
        Optional<FilterExpression<?>> filterExpression = filterRow.createFilterExpression();
        Optional<FilterValidationReport> filterValidationReport = filterExpression
            .map(expression -> FilterValidationExecutor.getInstance()
                .validate(filterRow.getFilterType().getValidatorName().orElse(ValueNotNullFilterValidator.NAME), expression));

        filterValidationReport
            .filter(FilterValidationReport::isInvalid)
            .ifPresent(report -> setValidationErrorsAndWarnings(report, filterRow));

        return Pair.of(filterExpression, filterValidationReport);
    }

    List<FilterExpression<?>> collectValidatedFilterExpressions() {
        AtomicBoolean hasErrors = new AtomicBoolean();

        List<FilterExpression<?>> filterExpressions = filterRows.stream()
            .map(this::createValidatedExpressionWithReport)
            .peek(pair -> pair.getValue()
                .ifPresent(report -> hasErrors.compareAndSet(false, report.hasErrors())))
            .map(Pair::getKey)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());

        return hasErrors.get() ? Collections.emptyList() : filterExpressions;
    }

    boolean isCompositeExpressionValid(FilterExpression<?> expression, String validatorName) {
        if (StringUtils.isNotBlank(validatorName)) {
            FilterValidationReport report = FilterValidationExecutor.getInstance().validate(validatorName, expression);
            if (report.isInvalid()) {
                validationErrors.addAll(report.getErrors());
                validationWarnings.addAll(report.getWarnings());
                return !report.hasErrors();
            }
        }
        return true;
    }

    void reportToFrontend(Element element) {
        boolean hasErrors = !validationErrors.isEmpty();
        boolean hasWarnings = !validationWarnings.isEmpty();

        JsonBuilder.JsonObjectBuilder validationResultBuilder = jsonObject()
            .put("hasErrors", hasErrors)
            .putIf(hasErrors, "errors", jsonArray().addAllStrings(validationErrors).build())
            .put("hasWarnings", hasWarnings)
            .putIf(hasWarnings, "warnings", jsonArray().addAllStrings(validationWarnings).build())
            .put("errorHeader", I.trc(CommonFilter.TAG_NAME, "Filtering resulted errors:"))
            .put("warnHeader", I.trc(CommonFilter.TAG_NAME, "Filtering resulted warnings:"));

        element.setPropertyJson(CommonFilter.VALIDATION_RESULT_PROPERTY_NAME, validationResultBuilder.build());
    }

}
