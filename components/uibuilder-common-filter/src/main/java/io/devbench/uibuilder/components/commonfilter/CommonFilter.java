/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.listeners.BackendAttachListener;
import io.devbench.uibuilder.components.commonfilter.events.FilterAdditionRequestedEvent;
import io.devbench.uibuilder.components.commonfilter.events.FilterPropertiesReadyEvent;
import io.devbench.uibuilder.components.commonfilter.events.SearchButtonClickedEvent;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterTypeNotFoundException;
import io.devbench.uibuilder.components.commonfilter.filterrow.DefaultFilterDescriptor;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.components.commonfilter.filtertype.FilterType;
import io.devbench.uibuilder.components.commonfilter.filtertype.SupportsDefaultFilter;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.api.exceptions.DataSourceNotFoundException;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceContext;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.data.common.datasource.DataSourceChangeNotifiable;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import lombok.AccessLevel;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder.*;


@Getter(AccessLevel.PACKAGE)
@Tag(CommonFilter.TAG_NAME)
@JsModule("./uibuilder-common-filter/src/uibuilder-common-filter.js")
public class CommonFilter
    extends Component
    implements HasComponents, BackendAttachListener, DataSourceChangeNotifiable<CommonDataSource<?, ?, FilterExpression<?>, ?>> {

    public static final String TAG_NAME = "uibuilder-common-filter";
    public static final String VALIDATION_RESULT_PROPERTY_NAME = "_validationResult";

    private String dataSourceId;
    private String defaultQueryName;
    private String validatorName;

    private Map<String, FilterType> filterTypeUuidMap;
    private List<FilterType> filterTypes;
    private List<DefaultFilterDescriptor> defaultFilterDescriptors;
    private final Map<DefaultFilterDescriptor, FilterRow<?>> defaultFilterComponents = new HashMap<>();

    @Override
    public void onAttached() {
        addListener(FilterPropertiesReadyEvent.class, this::onFilterReady);
        addListener(SearchButtonClickedEvent.class, this::onSearchClicked);
        addListener(FilterAdditionRequestedEvent.class, this::onFilterAdditionRequested);
    }

    private void onFilterReady(FilterPropertiesReadyEvent event) {
        validatorName = event.getValidatorName();
        findDataSourceFilterParameters(event);
        registerFilterTypesByFilterTypeDescriptors(event.getFilterTypes());
        setupDefaultFilters(event.getFilterTypes(), event.getDefaultFilterDescriptors());

        if (event.isAutoSearch()) {
            List<FilterRow<?>> filterRows = getFilterRows();
            if (!filterRows.isEmpty()) {
                search(filterRows, getDataSource());
            }
        }

        registerAsDataSourceReferenceHolder();
    }

    private void registerAsDataSourceReferenceHolder() {
        CommonDataSourceContext<?> dataSourceContext = CommonDataSourceContext.getInstance();
        dataSourceContext.findDataSource(
            dataSourceId,
            new CommonDataSourceSelector(defaultQueryName, this)
        );
    }

    private void onSearchClicked(SearchButtonClickedEvent event) {
        search();
    }

    private void onFilterAdditionRequested(FilterAdditionRequestedEvent event) {
        CommonDataSource<?, ?, FilterExpression<?>, ?> dataSource = getDataSource();
        Map<String, FilterType> filterTypeUuidMap = getFilterTypeUuidMap();
        String menuItemId = Objects.requireNonNull(event.getItemId());
        if (filterTypeUuidMap.containsKey(menuItemId)) {
            FilterType filterType = filterTypeUuidMap.get(menuItemId);
            add(filterType.createFilterRow(dataSource.getFilterExpressionFactory(), dataSource.getMetadataProvider()));
        } else {
            throw new FilterTypeNotFoundException(menuItemId);
        }
    }

    private void registerFilterTypesByFilterTypeDescriptors(List<FilterType> filterTypes) {
        filterTypeUuidMap = Collections
            .unmodifiableMap(
                filterTypes
                    .stream()
                    .collect(Collectors.toMap(filterType -> UUID.randomUUID().toString(), Function.identity()))
            );

        initAddButtonOnFrontend();
    }

    private void initAddButtonOnFrontend() {
        CommonDataSource<?, ?, ?, ?> dataSource = getDataSourceOrNullIfNotExists();

        JsonArray menuItemDescriptors;
        if (dataSource != null && !Objects.equals(dataSource.getElementType(), Object.class)) {
            menuItemDescriptors = filterTypeUuidMap.entrySet().stream().map(this::buildMenuItemJson).collect(jsonArrayCollector());
        } else {
            menuItemDescriptors = Json.createArray();
        }
        getElement().callJsFunction("_renderMenuItems", menuItemDescriptors);
    }

    private CommonDataSource<?, ?, FilterExpression<?>, ?> getDataSourceOrNullIfNotExists() {
        CommonDataSource<?, ?, FilterExpression<?>, ?> dataSource = null;
        try {
            dataSource = getDataSource();
        } catch (DataSourceNotFoundException ignore) {}
        return dataSource;
    }

    private void setupDefaultFilters(List<FilterType> filterTypes, List<DefaultFilterDescriptor> defaultFilterDescriptors) {
        this.filterTypes = filterTypes;
        this.defaultFilterDescriptors = defaultFilterDescriptors;
        addDefaultFiltersBasedOnDescriptors();
    }

    private void addDefaultFiltersBasedOnDescriptors() {
        CommonDataSource<?, ?, FilterExpression<?>, ?> dataSource = getDataSourceOrNullIfNotExists();
        if (dataSource != null && !Objects.equals(dataSource.getElementType(), Object.class)) {
            remove(defaultFilterComponents.values().toArray(new Component[0]));
            defaultFilterDescriptors.forEach(defaultFilterDescriptor -> {
                filterTypes.stream()
                    .filter(SupportsDefaultFilter.class::isInstance)
                    .map(filterType -> (SupportsDefaultFilter & FilterType) filterType)
                    .filter(filterType -> Objects.equals(filterType.getFilterPath(), defaultFilterDescriptor.getFilterPath()))
                    .findAny()
                    .ifPresent(filterType -> {
                        filterType.setDefaultFilterDescriptor(defaultFilterDescriptor);
                        if (defaultFilterDescriptor.isAdded()) {
                            FilterRow filterRow = filterType.createFilterRow(dataSource.getFilterExpressionFactory(), dataSource.getMetadataProvider());
                            defaultFilterComponents.put(defaultFilterDescriptor, filterRow);
                            add(filterRow);
                        }
                    });
            });
        }
    }

    private JsonObject buildMenuItemJson(Map.Entry<String, FilterType> filterTypeIdEntry) {
        return jsonObject()
            .put("id", filterTypeIdEntry.getKey())
            .put("label", filterTypeIdEntry.getValue().getLabel())
            .build();
    }

    private void findDataSourceFilterParameters(FilterPropertiesReadyEvent filterPropertiesReadyEvent) {
        dataSourceId = filterPropertiesReadyEvent.getDataSourceId();
        defaultQueryName = filterPropertiesReadyEvent.getDefaultQueryName();
    }

    @SuppressWarnings("unchecked")
    final CommonDataSource<?, ?, FilterExpression<?>, ?> getDataSource() {
        return (CommonDataSource<?, ?, FilterExpression<?>, ?>) DataSourceManager
            .getInstance()
            .getDataSource(dataSourceId, new CommonDataSourceSelector(defaultQueryName, this));
    }


    void search() {
        List<FilterRow<?>> filterRows = getFilterRows();
        search(filterRows, getDataSource());
    }

    @SuppressWarnings("unchecked")
    void search(List<FilterRow<?>> filterRows, CommonDataSource<?, ?, FilterExpression<?>, ?> dataSource) {
        FilterRowsValidator.clearValidationOnFrontend(getElement());
        FilterRowsValidator filterRowsValidator = new FilterRowsValidator(filterRows);
        List<FilterExpression<?>> expressions = filterRowsValidator.collectValidatedFilterExpressions();

        if (dataSource != null && ensureNoEmptyRowsPresent(filterRows) && filterRowsValidator.isValid()) {
            FilterExpressionFactory<FilterExpression<?>> filterExpressionFactory = dataSource.getFilterExpressionFactory();
            AndFilterExpression<FilterExpression<?>, ?> andFilterExpression = filterExpressionFactory.create(AndFilterExpression.class);
            expressions.forEach(andFilterExpression::add);
            if (filterRowsValidator.isCompositeExpressionValid(andFilterExpression, getValidatorName())) {
                dataSource.registerFilter(andFilterExpression);
                DataSourceManager.getInstance().requestRefresh(getDataSourceId(), new CommonDataSourceSelector(getDefaultQueryName(), this));
            }
        }
        filterRowsValidator.reportToFrontend(getElement());
    }

    private List<FilterRow<?>> getFilterRows() {
        return getChildren()
                .filter(FilterRow.class::isInstance)
                .map(component -> (FilterRow<?>) component)
                .collect(Collectors.toList());
    }

    private boolean ensureNoEmptyRowsPresent(List<FilterRow<?>> filterRows) {
        return filterRows.stream()
            .noneMatch(FilterRow::isEmpty);
    }

    @Override
    public void dataSourceChanged(
        @NotNull String dataSourceId,
        @NotNull CommonDataSourceSelector selector,
        @Nullable CommonDataSource<?, ?, FilterExpression<?>, ?> dataSource
    ) {
        initAddButtonOnFrontend();
        addDefaultFiltersBasedOnDescriptors();
        search(getFilterRows(), dataSource);
    }
}
