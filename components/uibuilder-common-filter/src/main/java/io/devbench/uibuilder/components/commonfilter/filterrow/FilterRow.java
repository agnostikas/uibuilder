/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filterrow;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import io.devbench.uibuilder.components.commonfilter.CommonFilter;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterRowParentNotFoundException;
import io.devbench.uibuilder.components.commonfilter.filtertype.FilterType;
import io.devbench.uibuilder.components.tooltip.Tooltip;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import lombok.AccessLevel;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@CssImport("./uibuilder-common-filter/filter-row-style.css")
public abstract class FilterRow<FILTER_TYPE extends FilterType> extends Div {

    private static final String ERROR_INDICATOR_CLASS_NAME = "error-indicator";
    private static final String WARNING_INDICATOR_CLASS_NAME = "warning-indicator";

    @Getter
    @NotNull
    private final FilterExpressionFactory<FilterExpression<?>> expressionFactory;

    @Getter
    @NotNull
    private final BindingMetadata metadata;

    @Getter(AccessLevel.PACKAGE)
    private final HorizontalLayout contentHolder;

    @Getter(AccessLevel.PACKAGE)
    private final Tooltip tooltip;

    @Getter(AccessLevel.PACKAGE)
    private final Button removeButton;

    @Getter
    private final FILTER_TYPE filterType;


    public FilterRow(
        @NotNull FILTER_TYPE filterType,
        @NotNull FilterExpressionFactory<FilterExpression<?>> expressionFactory,
        @NotNull BindingMetadata metadata
    ) {
        this.filterType = filterType;
        this.expressionFactory = expressionFactory;
        this.metadata = metadata;
        setClassName("filter-row");
        contentHolder = new HorizontalLayout();
        contentHolder.setClassName("row-content");
        removeButton = new Button(new Icon(VaadinIcon.CLOSE));
        removeButton.setClassName("remove-button");
        removeButton.addClickListener(this::onRemoveButtonClicked);
        tooltip = new Tooltip();
        super.add(contentHolder, removeButton, tooltip);
    }

    private void onRemoveButtonClicked(ClickEvent<Button> event) {
        CommonFilter filterComponent = this.getParent()
            .filter(CommonFilter.class::isInstance)
            .map(CommonFilter.class::cast)
            .orElseThrow(FilterRowParentNotFoundException::new);
        filterComponent.remove(this);
    }

    public void add(Component... components) {
        contentHolder.add(components);
    }

    protected void showTooltip() {
        tooltip.setActive(true);
    }

    protected void hideTooltip() {
        tooltip.setActive(false);
    }

    protected void addErrorIndicator() {
        addClassName(ERROR_INDICATOR_CLASS_NAME);
    }

    protected void addWarningIndicator() {
        addClassName(WARNING_INDICATOR_CLASS_NAME);
    }

    protected void clearIndicators() {
        removeClassNames(ERROR_INDICATOR_CLASS_NAME, WARNING_INDICATOR_CLASS_NAME);
    }

    protected String getTooltipText() {
        return tooltip.getText();
    }

    protected void setTooltipText(@NotNull String text) {
        tooltip.setText(text);
    }

    protected boolean hasIndicators() {
        return hasClassName(ERROR_INDICATOR_CLASS_NAME) || hasClassName(WARNING_INDICATOR_CLASS_NAME);
    }

    public abstract Optional<FilterExpression<?>> createFilterExpression();

    public abstract boolean isEmpty();

}
