/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filterrow;

import elemental.json.*;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import lombok.Getter;
import lombok.ToString;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
@ToString
public class DefaultFilterDescriptor {
    private final String filterPath;
    private final OperatorType operator;
    private final List<String> values;
    private final boolean added;
    private final boolean nonRemovable;
    private final boolean readOnly;

    public static DefaultFilterDescriptor of(JsonObject json) {
        return new DefaultFilterDescriptor(json);
    }

    private DefaultFilterDescriptor(JsonObject json) {
        added = readJsonBoolean(json, "added");
        nonRemovable = readJsonBoolean(json, "nonRemovable");
        readOnly = readJsonBoolean(json, "readOnly");
        filterPath = readMandatoryJsonString(json, "filter");
        operator = OperatorType.fromName(readMandatoryJsonString(json, "operator"));
        values = readJsonValues(json);
    }

    private static List<String> readJsonValues(JsonObject json) {
        if (json.hasKey("value")) {
            JsonValue value = json.get("value");
            if (value instanceof JsonArray) {
                JsonArray array = (JsonArray) value;
                return IntStream.range(0, array.length())
                    .mapToObj(array::get)
                    .map(JsonValue.class::cast)
                    .map(JsonValue::asString)
                    .collect(Collectors.toList());
            } else {
                return Collections.singletonList(value instanceof JsonNull ? null : value.asString());
            }
        }
        throw new NullPointerException("Property value is mandatory!");
    }

    private static String readMandatoryJsonString(JsonObject json, String property) {
        if (json.hasKey(property) && !(json.get(property) instanceof JsonNull)) {
            return json.getString(property);
        }
        throw new NullPointerException("Property " + property + " is mandatory!");
    }

    private static boolean readJsonBoolean(JsonObject json, String property) {
        return json.hasKey(property) && (json.get(property) instanceof JsonBoolean && json.getBoolean(property));
    }

}
