/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.valuecomponents;

import com.vaadin.flow.component.combobox.ComboBox;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterValueComponentException;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

public class EnumFilterValueComponent extends FilterValueComponent<ComboBox, Enum> {

    @Setter
    protected Class<? extends Enum> enumType;

    public EnumFilterValueComponent() {
        super(ComboBox.class);
    }

    @Override
    public boolean isApplicableOnPropertyType(Class<?> propertyType) {
        return propertyType.isEnum();
    }

    @Override
    public ComboBox afterComponentCreated(ComboBox component) {
        if (enumType == null) {
            throw new FilterValueComponentException("Enum type is not set for component: " + EnumFilterValueComponent.class.getName());
        }

        @SuppressWarnings("unchecked")
        ComboBox<Enum> enumComboBox = component;
        enumComboBox.setItems(Arrays.asList(enumType.getEnumConstants()));
        enumComboBox.setItemLabelGenerator(Enum::name);
        return enumComboBox;
    }

    @Override
    public List<OperatorType> getOperators() {
        return Arrays
            .asList(
                OperatorType.EQUALS,
                OperatorType.NOT_EQUALS,
                OperatorType.IN,
                OperatorType.NOT_IN,
                OperatorType.IS_NULL,
                OperatorType.IS_NOT_NULL
            );
    }

}
