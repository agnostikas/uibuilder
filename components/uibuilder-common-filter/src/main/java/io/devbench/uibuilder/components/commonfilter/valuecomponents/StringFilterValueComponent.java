/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.valuecomponents;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;

import java.util.Arrays;
import java.util.List;

public class StringFilterValueComponent extends FilterValueComponent<TextField, String> {

    public StringFilterValueComponent() {
        super(TextField.class);
    }

    @Override
    public boolean isApplicableOnPropertyType(Class<?> propertyType) {
        return String.class.isAssignableFrom(propertyType);
    }

    @Override
    public List<OperatorType> getOperators() {
        return Arrays
            .asList(
                OperatorType.I_LIKE,
                OperatorType.LIKE,
                OperatorType.EQUALS,
                OperatorType.BETWEEN,
                OperatorType.IN,
                OperatorType.NOT_IN,
                OperatorType.IS_NULL,
                OperatorType.IS_NOT_NULL
            );
    }

    @Override
    public TextField afterComponentCreated(TextField component) {
        component.setValueChangeMode(ValueChangeMode.EAGER);
        return component;
    }
}
