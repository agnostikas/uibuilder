/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.filtertype;

import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.JsonString;
import io.devbench.uibuilder.components.commonfilter.exceptions.FilterOperatorTypeNotFoundException;
import io.devbench.uibuilder.components.commonfilter.exceptions.InvalidFilterOperatorsAttributeValueException;
import io.devbench.uibuilder.components.commonfilter.filterrow.DefaultFilterDescriptor;
import io.devbench.uibuilder.components.commonfilter.filterrow.DefaultFilterRow;
import io.devbench.uibuilder.components.commonfilter.filterrow.FilterRow;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.common.filter.operator.OperatorType;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static java.util.stream.Collectors.*;

@Getter
public class DefaultFilterType implements FilterType, SupportsDefaultFilter {

    private final String label;
    private final String filterPath;
    private final String valueComponent;
    private final String validatorName;

    private final Set<OperatorType> operators;

    @Setter
    private DefaultFilterDescriptor defaultFilterDescriptor;

    public DefaultFilterType(@NotNull JsonObject frontendValues) {
        Objects.requireNonNull(frontendValues);
        this.label = getFrontendFilterTypeLabel(frontendValues);
        this.filterPath = safeGetAttribute(this::getStringAttributeOrNull, frontendValues, "filter");
        this.valueComponent = getStringAttributeOrNull(frontendValues, "valueComponent");
        this.operators = unwrapOperators(frontendValues);
        this.validatorName = getStringAttributeOrNull(frontendValues, "validatorName");
    }

    private String getFrontendFilterTypeLabel(@NotNull JsonObject frontendValues) {
        return safeGetAttribute(this::getStringAttributeOrNull, frontendValues, "label");
    }

    private Set<OperatorType> unwrapOperators(@NotNull JsonObject frontendValues) {
        List<String> operatorNames = Optional.ofNullable(getArrayAttributeOrNull(frontendValues, "operators"))
            .orElse(Collections.emptyList())
            .stream()
            .filter(Objects::nonNull)
            .map(String::trim)
            .collect(toList());

        Set<OperatorType> operatorTypes = new HashSet<>();
        for (String operatorName : operatorNames) {
            OperatorType operatorType = OperatorType.fromName(operatorName);
            if (operatorType == null) {
                String filterTypeLabel = getFrontendFilterTypeLabel(frontendValues);
                throw new FilterOperatorTypeNotFoundException(operatorName, filterTypeLabel);
            }
            operatorTypes.add(operatorType);
        }

        return operatorTypes;
    }

    private <T> T safeGetAttribute(BiFunction<JsonObject, String, T> function, @NotNull JsonObject jsonObject, String key) {
        return Objects.requireNonNull(function.apply(jsonObject, key));
    }

    private String getStringAttributeOrNull(@NotNull JsonObject jsonObject, String key) {
        if (jsonObject.hasKey(key) && jsonObject.get(key) instanceof JsonString) {
            return jsonObject.getString(key);
        }
        return null;
    }

    private List<String> getArrayAttributeOrNull(@NotNull JsonObject jsonObject, String key) {
        if (jsonObject.hasKey(key)) {
            if (jsonObject.get(key) instanceof JsonArray) {
                final JsonArray jsonArray = jsonObject.getArray(key);
                return IntStream.range(0, jsonArray.length()).mapToObj(jsonArray::getString).collect(Collectors.toList());
            } else {
                String filterTypeLabel = getFrontendFilterTypeLabel(jsonObject);
                throw new InvalidFilterOperatorsAttributeValueException(filterTypeLabel);
            }
        }
        return null;
    }

    @Override
    public FilterRow createFilterRow(FilterExpressionFactory<FilterExpression<?>> expressionFactory, BindingMetadataProvider metadataProvider) {
        return new DefaultFilterRow(this, expressionFactory, metadataProvider.getMetadataForPath(filterPath));
    }

    @Override
    public Optional<String> getValidatorName() {
        return Optional.ofNullable(validatorName);
    }
}
