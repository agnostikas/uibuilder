/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.commonfilter.events;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.vaadin.flow.dom.DisabledUpdateMode;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.components.commonfilter.CommonFilter;
import io.devbench.uibuilder.components.commonfilter.filterrow.DefaultFilterDescriptor;
import io.devbench.uibuilder.components.commonfilter.filtertype.FilterType;
import io.devbench.uibuilder.components.commonfilter.filtertype.FilterTypesFactory;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
@DomEvent(value = "filter-properties-ready", allowUpdates = DisabledUpdateMode.ALWAYS)
public class FilterPropertiesReadyEvent extends ComponentEvent<CommonFilter> {

    private final String dataSourceId;
    private final String dataSourceName;
    private final String defaultQueryName;
    private final String validatorName;
    private final List<FilterType> filterTypes;
    private final List<DefaultFilterDescriptor> defaultFilterDescriptors;
    private final boolean autoSearch;

    public FilterPropertiesReadyEvent(
        CommonFilter source, boolean fromClient,
        @EventData("event.detail.dataSource.id") String dataSourceId,
        @EventData("event.detail.dataSource.name") String dataSourceName,
        @EventData("event.detail.dataSource.defaultQuery") String defaultQueryName,
        @EventData("event.detail.validatorName") String validatorName,
        @EventData("event.detail.filterTypes") JsonArray filterTypeDescriptors,
        @EventData("event.detail.defaultFilterTypes") JsonArray defaultFilterDescriptors,
        @EventData("event.detail.autoSearch") boolean autoSearch) {

        super(source, fromClient);
        this.dataSourceId = dataSourceId;
        this.dataSourceName = dataSourceName;
        this.defaultQueryName = defaultQueryName;
        this.validatorName = validatorName;
        this.autoSearch = autoSearch;

        FilterTypesFactory filterTypesFactory = new FilterTypesFactory();

        this.filterTypes = IntStream.range(0, filterTypeDescriptors.length())
            .mapToObj(filterTypeDescriptors::get)
            .map(JsonObject.class::cast)
            .map(filterTypesFactory::instantiateFilterType)
            .collect(Collectors.toList());

        this.defaultFilterDescriptors = IntStream.range(0, defaultFilterDescriptors.length())
            .mapToObj(defaultFilterDescriptors::get)
            .map(JsonObject.class::cast)
            .map(DefaultFilterDescriptor::of)
            .collect(Collectors.toList());
    }

}
