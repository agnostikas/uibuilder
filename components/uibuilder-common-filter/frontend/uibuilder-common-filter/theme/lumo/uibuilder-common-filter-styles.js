/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

const $_documentContainer = html`
<dom-module id="uibuilder-common-filter-style" theme-for="uibuilder-common-filter">
    <template>
        <style>
            div {
                --warn-background-color: #fffae3; /* no warn color in lumo so I will use the default */
            }

            [part="add-button-label"]::before {
                font-family: "lumo-icons";
                content: var(--lumo-icons-plus);
            }

            [part="search-button-label"]::before {
                font-family: "lumo-icons";
                content: var(--lumo-icons-search);
            }

            [part="error-container"] {
                background-color: var(--lumo-error-color-10pct);
                color: var(--lumo-error-color);
                border-color: var(--lumo-contrast-20pct);
            }

            [part="error-container"]::before {
                font-family: "lumo-icons";
                content: var(--lumo-icons-error);
            }

            [part="warn-container"] {
                background-color: var(--warn-background-color);
                color: var(--lumo-shade);
                border-color: var(--lumo-contrast-20pct);
            }

            [part="warn-container"]::before {
                font-family: "lumo-icons";
                content: var(--lumo-icons-error);
            }
        </style>
    </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
