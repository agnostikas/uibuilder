/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PolymerElement } from '@polymer/polymer/polymer-element.js';

export class UIBuilderDefaultFilterTypesElement extends PolymerElement {

    static get is() {
        return 'default-filters';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {};
    }
}

export class UIBuilderDefaultFilterTypeElement extends PolymerElement {

    static get is() {
        return 'default-filter';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {
            filter: {
                type: String,
            },
            operator: {
                type: String
            },
            value: {
                type: Object
            },
            added: {
                type: Boolean
            },
            nonRemovable: {
                type: Boolean
            },
            readOnly: {
                type: Boolean
            },
        };
    }

    _getPropertiesObject() {
        return {
            filter: this.filter,
            operator: this.operator,
            value: this.value,
            added: this.added,
            nonRemovable: this.nonRemovable,
            readOnly: this.readOnly,
        };
    }
}

customElements.define(UIBuilderDefaultFilterTypesElement.is, UIBuilderDefaultFilterTypesElement);
customElements.define(UIBuilderDefaultFilterTypeElement.is, UIBuilderDefaultFilterTypeElement);
