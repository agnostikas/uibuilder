/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { UIBuilderCommonFilterTypeElement, UIBuilderCommonFilterTypesElement } from './uibuilder-filter-types.js';
import { UIBuilderDefaultFilterTypeElement, UIBuilderDefaultFilterTypesElement } from './uibuilder-default-filter-types.js';
import { DataSource } from "@vaadin/flow-frontend/uibuilder/data/data-source.js";
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/uibuilder-i18n.js';


export class UIBuilderCommonFilter extends ThemableMixin(Uibuilder.I18NBaseMixin('uibuilder-common-filter', PolymerElement)) {

    static get template() {
        return html`
            <style>
                :host {
                    width: 100%;
                    display: flex;
                    box-sizing: border-box;
                    flex-direction: column;
                    align-items: flex-start;
                    margin-top: 1em;
                    margin-bottom: 1em;
                }

                [part="add-button-label"]::before {
                    content: "\\e900";
                    font-size: 16pt;
                    font-weight: bold;
                    vertical-align: middle;
                    position: relative;
                }

                [part="search-button"] {
                    float: right;
                }

                [part="search-button-label"]::before {
                    content: '\\e901';
                    font-size: 16pt;
                    font-weight: bold;
                    vertical-align: middle;
                    position: relative;
                    margin-right: 3pt;
                }

                [part="error-container"] {
                    background-color: #ffcccc;
                    color: #a4000f;
                    border-radius: 6px;
                    width: 100%;
                    border-style: solid;
                    border-width: 1px;
                    border-color: hsla(214, 53%, 23%, 0.16);
                    display: flex;
                }

                [part="error-container"]::before {
                    content: '\\e902';
                    font-size: 26pt;
                    vertical-align: middle;
                    position: relative;
                    margin-left: 3pt;
                }

                [part="warn-container"] {
                    background-color: #fffae3;
                    color: #000000;
                    border-radius: 6px;
                    width: 100%;
                    border-style: solid;
                    border-width: 1px;
                    border-color: hsla(214, 53%, 23%, 0.16);
                    display: flex;
                }

                [part="warn-container"]::before {
                    content: '\\e903';
                    font-size: 26pt;
                    vertical-align: middle;
                    position: relative;
                    margin-left: 3pt;
                }

                [part="error-header"] {
                    margin-left: 1em;
                }

                [part="error-entry"] {
                    margin-left: 3em;
                }

                [part="warn-header"] {
                    margin-left: 1em;
                }

                [part="warn-entry"] {
                    margin-left: 3em;
                }

                .button-container {
                    width: 100%;
                }

                .content-container {
                    width: 100%;
                    display: block;
                    overflow: auto;
                    max-height: 20vh;
                }

                .validation-container-disabled {
                    display: none;
                }

            </style>

            <vaadin-context-menu id="add-filter-ctx-menu">
            </vaadin-context-menu>

            <div id="errors" part="error-container" class="validation-container-disabled">
                <div>
                    <div part="error-header">[[_validationResult.errorHeader]]</div>
                    <dom-repeat items="[[_validationResult.errors]]">
                        <template>
                            <div part="error-entry">&#x25B8; [[item]]</div>
                        </template>
                    </dom-repeat>
                </div>
            </div>

            <div id="warns" part="warn-container" class="validation-container-disabled">
                <div>
                    <div part="warn-header">[[_validationResult.warnHeader]]</div>
                    <dom-repeat items="[[_validationResult.warnings]]">
                        <template>
                            <div part="warn-entry">&#x25B8; [[item]]</div>
                        </template>
                    </dom-repeat>
                </div>
            </div>

            <div class="button-container">
                <vaadin-button id="add" on-click="_openAddMenu" part="add-button">
                    <span part="add-button-label">{{addButtonText}}</span>
                </vaadin-button>
                <vaadin-button id="search" part="search-button" on-click="_search">
                    <span part="search-button-label">Search</span>
                </vaadin-button>
            </div>
            <div class="content-container">
                <slot></slot>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-common-filter';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {
            validator: {
                type: String
            },
            _dataSource: {
                type: Object
            },
            _filterTypes: {
                type: Array
            },
            autoSearch: {
                type: Boolean
            },
            _validationResult: {
                type: Object,
                value: () => {
                    return {
                        hasErrors: false,
                        hasWarnings: false,
                        warnHeader: "",
                        errorHeader: "",
                        warnings: [],
                        errors: []
                    }
                },
                observer: "_validationResultChanged"
            },
            addButtonText:{
                type: String
            }
        };
    }

    _uibuilderReady() {
        this._collectPropertiesToSendToBackend();
    }

    _collectPropertiesToSendToBackend() {
        const ds = this.querySelector(DataSource.is);

        if (!ds) {
            throw new Error(`${DataSource.is} is missing from ${this.tagName} with id:'${this.id}'`);
        } else {
            this._dataSource = {
                id: ds.datasourceId,
                name: ds.name,
                defaultQuery: ds.defaultQuery
            };
        }

        this._filterTypes = UIBuilderCommonFilter._getPropertiesOf(this._copyChildren(
            it => it instanceof UIBuilderCommonFilterTypesElement),
            it => it instanceof UIBuilderCommonFilterTypeElement);

        this._defaultFilterTypes = UIBuilderCommonFilter._getPropertiesOf(this._copyChildren(
            it => it instanceof UIBuilderDefaultFilterTypesElement),
            it => it instanceof UIBuilderDefaultFilterTypeElement);

        this.dispatchEvent(new CustomEvent('filter-properties-ready', {
            detail: {
                dataSource: this._dataSource,
                filterTypes: this._filterTypes,
                defaultFilterTypes: this._defaultFilterTypes,
                autoSearch: this.autoSearch,
                validatorName: this.validator
            }
        }));
    }

    static _getPropertiesOf(array, type, mapper = (it) => it._getPropertiesObject()) {
        if (array) {
            return array.filter(type).map(mapper);
        }
        return false;
    }

    _copyChildren(type) {
        return Array.from(this.children)
            .filter(type)
            .map(it => Array.from(it.children))
            .reduce((acc, cur) => {
                if (Array.isArray(cur)) {
                    return acc.concat(cur);
                } else {
                    acc.push(cur);
                    return acc;
                }
            }, []);
    }

    _openAddMenu(event) {
        let menu = this.shadowRoot.querySelector("#add-filter-ctx-menu");
        if (!menu.opened) {
            menu.open(event);
        } else {
            menu.close();
        }
    }

    _search() {
        this.dispatchEvent(new CustomEvent("search-button-clicked", {}));
    }

    _renderMenuItems(menuItems) {
        let listBox = document.createElement("vaadin-list-box");

        Array.from(menuItems)
            .forEach(item => this._createVaadinItem(item, listBox));

        const menu = this.shadowRoot.querySelector("#add-filter-ctx-menu");
        Array.from(menu.$.overlay.children).forEach(it => menu.$.overlay.removeChild(it));
        menu.$.overlay.appendChild(listBox);
        menu.$.overlay.content = listBox;
    }

    _createVaadinItem(item, listBox) {
        let menuItemElement = document.createElement("vaadin-item");
        menuItemElement.textContent = item.label;
        menuItemElement.id = item.id;
        menuItemElement.addEventListener('click', () => this._requestAdditionOfFilterRow(item.id));
        listBox.appendChild(menuItemElement);
    }

    _requestAdditionOfFilterRow(itemId) {
        this.dispatchEvent(new CustomEvent("filter-addition-requested", {detail: {itemId}}));
    }

    _validationResultChanged(newValue, oldValue) {
        if (!newValue || !newValue.hasErrors) {
            this.$.errors.classList.add('validation-container-disabled');
        } else {
            this.$.errors.classList.remove('validation-container-disabled');
        }

        if (!newValue || !newValue.hasWarnings) {
            this.$.warns.classList.add('validation-container-disabled');
        } else {
            this.$.warns.classList.remove('validation-container-disabled');
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this._translate();
    }

    _translate() {
        this.$.search.querySelector('span').textContent = this.tr('Search');
    }

}

customElements.define(UIBuilderCommonFilter.is, UIBuilderCommonFilter);
