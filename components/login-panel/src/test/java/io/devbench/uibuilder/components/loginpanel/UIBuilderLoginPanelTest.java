/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.loginpanel;

import io.devbench.uibuilder.components.loginpanel.event.LoginEvent;
import io.devbench.uibuilder.security.api.LoginService;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.inject.Provider;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class UIBuilderLoginPanelTest {

    @Mock
    private Provider<LoginService> loginServicesProvider;

    @Mock
    private LoginService loginService;

    @InjectMocks
    private UIBuilderLoginPanel testObj;

    @BeforeEach
    void setUp() {
        when(loginServicesProvider.get()).thenReturn(loginService);
    }

    @Test
    @DisplayName("should call login")
    void test_should_call_login() {
        LoginEvent loginEvent = Mockito.mock(LoginEvent.class);
        testObj.login(loginEvent);

        Mockito.verify(loginService).login(any(), any(), any());
    }
}
