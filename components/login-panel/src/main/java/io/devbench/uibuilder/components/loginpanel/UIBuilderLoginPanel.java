/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.loginpanel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import io.devbench.uibuilder.components.loginpanel.event.LoginEvent;
import io.devbench.uibuilder.security.api.LoginService;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Provider;

import static io.devbench.uibuilder.components.loginpanel.UIBuilderLoginPanel.*;

@Slf4j
@Tag(TAG_NAME)
@JsModule("./uibuilder-login-panel/src/uibuilder-login-panel.js")
public class UIBuilderLoginPanel extends Component implements HasComponents, HasElement {

    public static final String TAG_NAME = "uibuilder-login-panel";

    @Inject
    private Provider<LoginService> loginServicesProvider;

    public UIBuilderLoginPanel() {
        addListener(LoginEvent.class, this::login);
    }

    void login(LoginEvent event) {
        LoginService loginService;
        try {
            loginService = loginServicesProvider.get();
        } catch (Exception e) {
            loginService = null;
            log.error("Could not find login service", e);
        }
        if (loginService != null) {
            loginService.login(
                event.getUsername(),
                event.getPassword(),
                success -> getElement().callJsFunction(success ? "_onSucceedLogin" : "_onFailedLogin"));
        }
    }
}
