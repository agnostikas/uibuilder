/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.loginpanel.event;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import io.devbench.uibuilder.components.loginpanel.UIBuilderLoginPanel;
import lombok.Getter;

@DomEvent("login")
public class LoginEvent extends ComponentEvent<UIBuilderLoginPanel> {

    @Getter
    private String username;

    @Getter
    private String password;

    public LoginEvent(UIBuilderLoginPanel source, boolean fromClient,
                      @EventData("event.detail.username") String username,
                      @EventData("event.detail.password") String password) {
        super(source, fromClient);
        this.username = username;
        this.password = password;
    }
}
