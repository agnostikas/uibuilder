/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import * as PolymerFlush from '@polymer/polymer/lib/utils/flush.js';
import * as RenderStatus from '@polymer/polymer/lib/utils/render-status.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/uibuilder-i18n.js';


export class UiBuilderLoginPanel extends ThemableMixin(Uibuilder.I18NBaseMixin('uibuilder-login-panel', PolymerElement)) {

    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                }

                .center {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                .imgContainer {
                    text-align: center;
                    align-items: center;
                    padding: 20px;
                }

                .imgContainer img {
                    width: var(--login-image-width, 100%);
                    height: var(--login-image-height, 100%);
                    border-radius: var(--login-image-border-radius, 0%);
                }

                #defaultErrorMessage {
                    color: red;
                    padding: 1em;
                }
            </style>
            <div part="uibuilder-login-panel-container">
                <template is="dom-if" if="[[image]]">
                    <div class="imgContainer">
                        <img src="[[image]]" alt="Login image">
                    </div>
                </template>
                <vaadin-vertical-layout class="center">
                    <vaadin-vertical-layout>
                        <vaadin-text-field id="usernameField" label="Username"
                                           value="{{_username}}"></vaadin-text-field>
                        <vaadin-password-field id="passwordField" label="Password"
                                               value="{{_password}}"></vaadin-password-field>
                        <vaadin-button id="loginButton" on-click="_login" theme="primary">
                            Login
                        </vaadin-button>
                    </vaadin-vertical-layout>
                    <template is="dom-if" if="{{!hideDefaultLoginFailedMessage}}">
                        <div id="defaultErrorMessage" hidden$="[[_errorHidden]]">
                            [[errorMessage]]
                        </div>
                    </template>
                    <template is="dom-if" if="{{hideDefaultLoginFailedMessage}}">
                        <div id="customErrorMessage" hidden$="[[_errorHidden]]">
                            <slot></slot>
                        </div>
                    </template>
                </vaadin-vertical-layout>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-login-panel'
    }

    static get properties() {
        return {
            image: {
                type: String,
                value: () => this.image ? location.host + this.image : null
            },
            _username: {
                type: String
            },
            _password: {
                type: String
            },
            hideDefaultLoginFailedMessage: {
                type: Boolean,
                value: false
            },
            errorMessage: {
                type: String
            },
            _errorHidden: {
                type: Boolean,
                value: true
            },

        };
    }

    constructor() {
        super();
        this.addEventListener("keyup", (event) => {
            if (event.keyCode === 13) {
                this._login();
            }
        });
    }

    ready() {
        super.ready();
        PolymerFlush.flush();
    }

    connectedCallback() {
        super.connectedCallback();
        this._translate();
        RenderStatus.afterNextRender(this, () => {
            this._initError();
        });
    }

    _translate() {
        this._getElementById('usernameField').label = this.tr('Username');
        this._getElementById('passwordField').label = this.tr('Password');
        this._getElementById('loginButton').textContent = this.tr('Login');
        if (!this.hideDefaultLoginFailedMessage && !this.errorMessage) {
            this.errorMessage = this.tr('Username or password doesn\'t match.');
        }
    }

    _initError() {
        this._errorElement = this.hideDefaultLoginFailedMessage ? this._getCustomErrorElement() : this._getElementById('defaultErrorMessage');
    }

    _getElementById(id) {
        return this.shadowRoot.getElementById(id);
    }

    _getCustomErrorElement() {
        let slotChild = null;
        this.shadowRoot.querySelector('slot').assignedNodes().forEach(slotNode => {
            if (slotNode.tagName && slotNode.tagName.toLowerCase() === 'vaadin-notification') {
                slotChild = slotNode;
            }
        });
        if (slotChild) {
            return slotChild;
        } else {
            return this._getElementById('customErrorMessage');
        }
    }

    _login() {
        this.dispatchEvent(new CustomEvent('login', {
            detail: {
                username: this._username,
                password: this._password
            }
        }));
        this._hideNotification();
    }

    _onSucceedLogin() {
        this.dispatchEvent(new CustomEvent('success'));
        this._username = '';
        this._password = '';
    }

    _onFailedLogin() {
        if (this._errorElement.tagName && this._errorElement.tagName.toLowerCase() === 'vaadin-notification') {
            this._errorElement.open();
        } else {
            this._errorHidden = false;
        }
        this.dispatchEvent(new CustomEvent('failed'));
        this._password = '';
    }

    _hideNotification() {
        if (!this._errorHidden) {
            this._errorHidden = true;
        }
    }
}

customElements.define(UiBuilderLoginPanel.is, UiBuilderLoginPanel);
