/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.calendar.data;

import com.vaadin.flow.component.JsonSerializable;
import com.vaadin.flow.internal.JsonSerializer;
import elemental.json.JsonObject;
import elemental.json.JsonType;
import elemental.json.JsonValue;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

abstract class AbstractCalendarData implements JsonSerializable {
    JsonValue toJson(Instant instant) {
        return JsonSerializer.toJson(
            instant != null ?
                DateTimeFormatter.ISO_INSTANT.format(instant) :
                null
        );
    }

    Instant toInstant(JsonValue jsonValue) {
        return jsonValue.getType().equals(JsonType.NULL) ?
            null :
            Instant.from(DateTimeFormatter.ISO_INSTANT.parse(jsonValue.asString()));
    }

    Instant getInstant(JsonObject jsonObject, String key) {
        return jsonObject.hasKey(key) ?
            Instant.from(DateTimeFormatter.ISO_INSTANT.parse(jsonObject.getString(key))) :
            null;
    }

    String getString(JsonObject jsonObject, String key) {
        return jsonObject.hasKey(key) ?
            jsonObject.getString(key) :
            null;
    }
}
