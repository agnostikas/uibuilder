/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.calendar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import io.devbench.uibuilder.components.util.common.ReactingEvent;
import lombok.Getter;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

@Getter
@DomEvent("entry-request")
public class EntryRequestEvent extends ReactingEvent<Component> {

    private final Instant start;
    private final Instant end;

    public EntryRequestEvent(Component source, boolean fromClient,
                             @EventData(REQUEST_ID) String requestId,
                             @EventData("event.detail.start") String start,
                             @EventData("event.detail.end") String end) {
        super(source, fromClient, requestId);

        this.start = Instant.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(start));
        this.end = Instant.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(end));
    }
}
