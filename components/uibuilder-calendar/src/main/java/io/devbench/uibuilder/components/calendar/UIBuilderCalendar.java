/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.calendar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Synchronize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.internal.JsonSerializer;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.calendar.data.Entry;
import io.devbench.uibuilder.components.calendar.event.TimeSlotSelectEvent;
import io.devbench.uibuilder.components.calendar.event.TimeSlotUnselectEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Tag(UIBuilderCalendar.TAG_NAME)
@NpmPackage(value = "@fullcalendar/core", version = "4.3.1")
@NpmPackage(value = "@fullcalendar/moment", version = "4.3.0")
@NpmPackage(value = "@fullcalendar/interaction", version = "4.3.0")
@NpmPackage(value = "@fullcalendar/daygrid", version = "4.3.0")
@NpmPackage(value = "@fullcalendar/timegrid", version = "4.3.0")
@NpmPackage(value = "@fullcalendar/list", version = "4.3.0")
@JsModule("./uibuilder-calendar/src/uibuilder-calendar.js")
@CssImport(value = "@fullcalendar/core/main.css")
@CssImport(value = "@fullcalendar/core/main.css", themeFor = UIBuilderCalendar.TAG_NAME)
@CssImport(value = "@fullcalendar/daygrid/main.css", themeFor = UIBuilderCalendar.TAG_NAME)
@CssImport(value = "@fullcalendar/timegrid/main.css", themeFor = UIBuilderCalendar.TAG_NAME)
@CssImport(value = "@fullcalendar/list/main.css", themeFor = UIBuilderCalendar.TAG_NAME)
public class UIBuilderCalendar extends Component {
    static final String TAG_NAME = "uibuilder-calendar";

    @Setter
    private EntryRepository entryRepository;

    public UIBuilderCalendar() {
        addListener(EntryRequestEvent.class, event -> {
            if (entryRepository != null) {
                Collection<Entry> entries = entryRepository.getEntries(event.getStart(), event.getEnd());
                event.sendDataResponse(JsonSerializer.toJson(entries));
            }
        });
    }

    @Synchronize(value = "locale-changed", property = "locale")
    public String getClientLocale() {
        return getElement().getProperty("locale");
    }

    public void setClientLocale(String locale) {
        getElement().setProperty("locale", locale);
    }

    public void refetchEvents() {
        getElement().callJsFunction("refetchEvents");
    }

    public Registration addTimeSlotSelectListener(ComponentEventListener<TimeSlotSelectEvent> listener) {
        return addListener(TimeSlotSelectEvent.class, listener);
    }

    public Registration addTimeSlotUnselectListener(ComponentEventListener<TimeSlotUnselectEvent> listener) {
        return addListener(TimeSlotUnselectEvent.class, listener);
    }
}
