/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.calendar.data;

import com.vaadin.flow.component.JsonSerializable;
import com.vaadin.flow.internal.JsonSerializer;
import elemental.json.Json;
import elemental.json.JsonObject;
import lombok.*;

import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Entry extends AbstractCalendarData {
    private String id;
    private String title;
    private Instant start;
    private Instant end;
    private boolean allDay;
    private boolean editable;
    private String color;

    @Override
    public JsonObject toJson() {
        JsonObject jsonObject = Json.createObject();
        jsonObject.put("id", JsonSerializer.toJson(id));
        jsonObject.put("title", JsonSerializer.toJson(title));
        jsonObject.put("allDay", JsonSerializer.toJson(allDay));
        jsonObject.put("start", toJson(start));
        jsonObject.put("end", toJson(end));
        jsonObject.put("editable", JsonSerializer.toJson(editable));
        jsonObject.put("color", JsonSerializer.toJson(color));
        return jsonObject;
    }

    @Override
    public JsonSerializable readJson(JsonObject value) {
        id = getString(value, "id");
        start = getInstant(value, "start");
        end = getInstant(value, "end");
        return this;
    }

}
