/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/util/z-index-manager/popup-mixin.js';
import { Calendar } from '@fullcalendar/core/main.js';

import momentPlugin from '@fullcalendar/moment/main.js';
import interactionPlugin from '@fullcalendar/interaction/main.js';
import dayGridPlugin from '@fullcalendar/daygrid/main.js';
import timeGridPlugin from '@fullcalendar/timegrid/main.js';
import listPlugin from '@fullcalendar/list/main.js';

import allLocales from '@fullcalendar/core/locales-all';

export class UIBuilderCalendar extends ThemableMixin(PolymerElement) {

    static get template() {
        return html`
            <style>
                #calendarWrapper {
                    height: inherit;
                    width: inherit;
                }
            </style>

            <div id="calendarWrapper">
                <div id="calendar"></div>
            </div>
        `;
    }

    static get properties() {
        return {
            header: {
                type: Object,
                value: () => {
                    return {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
                    }
                }
            },
            footer: Object,
            locale: {
                type: String,
                observer: '_onLocaleChange',
                notify: true,
            },
            titleFormat: Object,
            eventTimeFormat: {
                type: Object,
                value: () => {
                    return {
                        hour: '2-digit',
                        minute: '2-digit',
                        hour12: false
                    }
                }
            },
            slotLabelFormat: {
                type: Object,
                value: () => {
                    return {
                        hour: '2-digit',
                        minute: '2-digit',
                        hour12: false
                    }
                }
            },
            eventLimit: {
                type: Object,
                value: 6,
            },
            hideWeekends: Boolean,
            businessHours: Object,
            validRange: Object,

            selectable: Boolean,
            selectMirror: Boolean,

            nowIndicator: Boolean,

            allDaySlot: Boolean,
            slotEventOverlap: Boolean,
            slotDuration: {
                type: String,
                value: '00:30:00'
            },
            scrollTime: {
                type: String,
                value: '06:00:00',
            },
            minTime: {
                type: String,
                value: '00:00:00'
            },
            maxTime: {
                type: String,
                value: '24:00:00'
            },

            longPressDelay: {
                type: Number,
                value: 1000,
            },
            eventLongPressDelay: {
                type: Number,
                value: 1000,
            },
            selectLongPressDelay: {
                type: Number,
                value: 1000,
            },

            defaultView: {
                type: String,
                value: 'timeGridWeek'
            },
        };
    }

    ready() {
        super.ready();

        this._calendar = new Calendar(this.$.calendar, {
            height: 'parent',
            header: this.header,
            footer: this.footer,
            locales: allLocales,
            locale: this.locale || navigator.languages,
            titleFormat: this.titleFormat,
            eventTimeFormat: this.eventTimeFormat,
            slotLabelFormat: this.slotLabelFormat,
            eventLimit: this.eventLimit,
            weekends: !this.hideWeekends,
            businessHours: this.businessHours,
            validRange: this.validRange,

            selectable: this.selectable,
            selectMirror: this.selectMirror,

            nowIndicator: this.nowIndicator,

            allDaySlot: this.allDaySlot,
            slotEventOverlap: this.slotEventOverlap,
            slotDuration: this.slotDuration,
            scrollTime: this.scrollTime,
            minTime: this.minTime,
            maxTime: this.maxTime,

            longPressDelay: this.longPressDelay,
            eventLongPressDelay: this.eventLongPressDelay,
            selectLongPressDelay: this.selectLongPressDelay,

            defaultView: this.defaultView,
            plugins: [momentPlugin, interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin],
            eventRender: info => this.dispatchEvent(new CustomEvent('entry-render', {detail: info})),
            events: (fetchInfo, successCallback, failureCallback) => {
                const event = new Uibuilder.ReactingEvent('entry-request', {
                    detail: {
                        start: fetchInfo.start.toISOString(),
                        end: fetchInfo.end.toISOString()
                    }
                });
                this.dispatchEvent(event);
                event.onResponse()
                    .then(events => successCallback(events))
                    .catch(err => console.error(err));
            }
        });

        this._calendar.on('select', selectionInfo => {
            this.dispatchEvent(new UIBuilderCalendar.ItemHolderEvent('time-slot-select', {
                start: selectionInfo.start.toISOString(),
                end: selectionInfo.end.toISOString(),
                allDay: selectionInfo.allDay,
            }));
        });
        this._calendar.on('unselect', () => this.dispatchEvent(new CustomEvent('time-slot-unselect')));

        this._calendar.on('eventClick', ({event: entry}) => {
            this.dispatchEvent(new UIBuilderCalendar.ItemHolderEvent('entry-click', {
                id: entry.id,
                start: entry.start,
                end: entry.end
            }));
        });
        this._calendar.on('eventMouseEnter', ({event}) => {
            this.dispatchEvent(new CustomEvent('entry-mouse-enter', {detail: {entry: event}}));
        });
        this._calendar.on('eventMouseLeave', ({event}) => {
            this.dispatchEvent(new CustomEvent('entry-mouse-leave', {detail: {entry: event}}));
        });

        this._calendar.on('eventResize', ({event: entry, prevEvent: oldEntry, startDelta, endDelta}) => {
            this.dispatchEvent(new UIBuilderCalendar.ItemHolderEvent('entry-resize', {
                entry: {
                    id: entry.id,
                    start: entry.start,
                    end: entry.end
                },
                oldEntry: {
                    id: oldEntry.id,
                    start: oldEntry.start,
                    end: oldEntry.end
                },
                startDelta,
                endDelta
            }));
        });

        this._calendar.on('eventDrop', ({event: entry, oldEvent: oldEntry, delta}) => {
            this.dispatchEvent(new UIBuilderCalendar.ItemHolderEvent('entry-drop', {
                entry: {
                    id: entry.id,
                    start: entry.start,
                    end: entry.end
                },
                oldEntry: {
                    id: oldEntry.id,
                    start: oldEntry.start,
                    end: oldEntry.end
                },
                delta
            }));
        });

        // Promise.all([
        //     this._loadCss("@fullcalendar/core/main.css"),
        //     this._loadCss("@fullcalendar/core/main.css", document.head), // load fonts
        //     this._loadCss("@fullcalendar/daygrid/main.css"),
        //     this._loadCss("@fullcalendar/timegrid/main.css"),
        //     this._loadCss("@fullcalendar/list/main.css"),
        // ])
        //     .then(() => {
        //         this._calendar.render();
        //     });

        this._calendar.render();
    }

    _uibuilderReady() {
        this._calendar.refetchEvents();
    }

    _loadCss(url, parent = this.shadowRoot) {
        return new Promise((resolve, reject) => {
            const link = document.createElement('link');
            link.rel = 'stylesheet';
            link.href = url;
            link.onload = resolve;
            link.onerror = reject;
            parent.appendChild(link);
        });
    }

    disconnectedCallback() {
        this._calendar.destroy();
    }

    refetchEvents() {
        this._calendar.refetchEvents();
        this._calendar.unselect();
    }

    updateSize() {
        this._calendar.updateSize();
    }

    _onLocaleChange() {
        this._calendar && this._calendar.setOption('locale', this.locale);
    }

    static get is() {
        return 'uibuilder-calendar';
    }
}

UIBuilderCalendar.ItemHolderEvent = class extends Event {
    constructor(type, item) {
        super(type);
        this.model = {item};
    }
};

customElements.define(UIBuilderCalendar.is, UIBuilderCalendar);
