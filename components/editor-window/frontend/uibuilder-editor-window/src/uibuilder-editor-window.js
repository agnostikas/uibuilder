/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';


export class UibuilderEditorWindow extends ThemableMixin(Uibuilder.I18NBaseMixin('uibuilder-editor-window', PolymerElement)) {

    static get template() {
        return html`
            <style>
                uibuilder-window{
                    width: inherit;
                    height: inherit;
                }
            </style>

            <uibuilder-window id="window"
                              header-text="[[headerText]]"
                              on-close-button-pressed="_onClose"
                              keep-open-on-close-button
                              closeable
                              resize="{{resize}}">
                <div slot="layout">
                    <detail-panel id="detailPanel"
                                  interruptible-reset="[[interruptibleReset]]"
                                  interruptible-cancel="[[interruptibleCancel]]"
                                  interruptible-save="[[interruptibleSave]]"
                                  save-ack-required="[[saveAckRequired]]"
                                  on-value-changed="_formValueChanged"
                                  on-save="_onSave"
                                  on-reset="_onReset"
                                  on-cancel="_onCancel"
                                  hide-default-cancel="[[hideDefaultCancel]]"
                                  hide-default-reset="[[hideDefaultReset]]"
                                  hide-default-save="[[hideDefaultSave]]"
                                  readonly="[[readonly]]">
                        <slot></slot>
                    </detail-panel>
                </div>
            </uibuilder-window>

            <confirm-dialog id="confirm">
                The form has unsaved changes. If you leave before saving, your changes will be lost. Do you really want to exit?
            </confirm-dialog>
        `;
    }

    static get is() {
        return 'uibuilder-editor-window'
    }

    static get properties() {
        return {
            headerText: {
                type: String,
            },
            interruptibleReset: {
                type: Boolean,
                value: false
            },
            interruptibleCancel: {
                type: Boolean,
                value: false
            },
            interruptibleSave: {
                type: Boolean,
                value: false
            },
            saveAckRequired: {
                type: Boolean,
                value: false
            },
            _valueChanged: {
                type: Boolean,
                value: false
            }, resize: {
                type: String,
                value: "none"
            }, hideDefaultCancel: {
                type: Boolean,
                value: false
            }, hideDefaultReset: {
                type: Boolean,
                value: false
            }, hideDefaultSave: {
                type: Boolean,
                value: false
            }, readonly: {
                type: Boolean,
                value: false,
                notify: true
            }
        };
    }

    connectedCallback() {
        super.connectedCallback();
        this.shadowRoot.querySelector("#confirm").textContent = this.tr(
            'The form has unsaved changes. If you leave before saving, your changes will be lost. Do you really want to exit?'
        );
    }

    _onSave(e) {
        if (this.$.detailPanel.$.__internalForm._newItem) {
            this.$.detailPanel.dispatchEvent(new CustomEvent("value-changed", {
                detail: {
                    propertyName: "",
                    oldValue: "",
                    newValue: ""
                }
            }));
        }
        this.dispatchEvent(new CustomEvent("save", e));
    }

    _onReset(e) {
        this.dispatchEvent(new CustomEvent("reset", e));
    }

    _onCancel(e) {
        if (e.detail && e.detail.internal) {
            this.dispatchEvent(new CustomEvent("cancel", e));
        } else {
            this._onClose();
        }
    }

    _resetValueChanged() {
        this._valueChanged = false;
    }

    _onClose() {
        if (this._valueChanged)
            this.$.confirm.open(() => this._close());
        else
            this._close();
    }

    _close() {
        this.$.detailPanel.forceResetForm();
        this.dispatchEvent(new CustomEvent("close"));
    }

    _formValueChanged(e) {
        this._valueChanged = true;
        this.dispatchEvent(new CustomEvent("value-changed", e));
    }

    markNewItem() {
        this.$.detailPanel.markNewItem();
    }
}

customElements.define(UibuilderEditorWindow.is, UibuilderEditorWindow);

window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.formContainerTags = window.Uibuilder.formContainerTags || new Set();
window.Uibuilder.formContainerTags.add(UibuilderEditorWindow.is);
