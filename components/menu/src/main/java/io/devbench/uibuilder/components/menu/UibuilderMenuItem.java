/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.menu;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;

@Tag("menu-item")
@JsModule("./uibuilder-menu/src/menu-item.js")
public class UibuilderMenuItem extends Component implements HasComponents, HasElement {

    public UibuilderMenuItem() {

    }

    public UibuilderMenuItem(Component... components) {
        this();
        addComponents(components);
    }

    protected void addComponents(Component... components) {
        for (Component component : components) {
            getElement().appendChild(component.getElement());
        }
    }

    @Synchronize("name-changed")
    public String getName() {
        addComponents();
        return getElement().getProperty("name");
    }

    public void setName(String name) {
        getElement().setProperty("name", name);
    }

    @Synchronize("disabled-changed")
    public boolean isDisabled() {
        return getElement().getProperty("disabled", false);
    }

    public void setDisabled(boolean disabled) {
        getElement().setProperty("disabled", disabled);
    }

    @Synchronize("right-changed")
    public boolean isRight() {
        return getElement().getProperty("right", false);
    }

    public void setRight(boolean right) {
        getElement().setProperty("right", right);
    }
}
