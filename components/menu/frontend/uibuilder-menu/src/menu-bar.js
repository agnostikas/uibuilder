/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import * as RenderStatus from '@polymer/polymer/lib/utils/render-status.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { MenuItem } from './menu-item.js';

export class MenuBar extends ThemableMixin(PolymerElement) {

    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                    padding: 4px;
                    border: 1px solid #E0E0E0;
                    background-color: #F0F0F0;
                }

                .uibuilder-menu-bar-clear {
                    clear: both;
                }
            </style>

            <div part="items">
                <slot></slot>
            </div>
            <div class="uibuilder-menu-bar-clear"></div>
            `;
    }

    static get is() {
        return 'menu-bar'
    }

    static get properties() {
        return {
            moreItemName: {
                type: String,
                value: '↓'
            },
            collapseDelta: {
                type: Number,
                value: 100
            },
        }
    }

    constructor() {
        super();
        this._openedMenuItems = Array();
    }

    ready() {
        super.ready();
        window.addEventListener('click', this._windowClickListener = (event) => {
            if (this._openedMenuItems.length > 0) {
                if (event.composedPath().filter(e => e instanceof MenuItem).length === 0) {
                    this._openedMenuItems.forEach(menuItem => menuItem._close());
                    this._openedMenuItems.length = 0;
                }
            }
        });

        this._windowResizeListener = () => this._rearrangeMenuBar(this);
        RenderStatus.beforeNextRender(this, () => {
            this._rearrangeMenuBar(this);
            if (this._windowResizeListener) {
                window.addEventListener("resize", this._windowResizeListener);
            }
        });
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener('click', this._windowClickListener);
        window.removeEventListener('resize', this._windowResizeListener);
        this._windowResizeListener = null;
    }

    _markOpenRight(menuItem, openRight = true) {
        menuItem.openRight = openRight;
        menuItem._initStyle();
        menuItem._postProcessMenuItem();
        [...menuItem.children]
            .filter(child => child instanceof MenuItem)
            .forEach(menuItem => this._markOpenRight(menuItem, openRight));
    }

    _calculateRightSize(menuBar) {
        const rightSize = [...menuBar.children]
            .filter(child => child instanceof MenuItem)
            .filter(menuItem => menuItem.hasAttribute("right"))
            .reduce((acc, menuItem) => acc + menuItem.offsetWidth, 0);
        return menuBar.offsetWidth - rightSize - this.collapseDelta;
    }

    _calculateMoreMenuItemWidth(menuBar) {
        return [...menuBar.children]
            .filter(child => child instanceof MenuItem)
            .filter(menuItem => menuItem.hasAttribute("more"))
            .reduce((acc, menuItem) => acc + menuItem.offsetWidth, 0);
    }

    _calculateMenuItemsWidth(menuBar) {
        return [...menuBar.children]
            .filter(child => child instanceof MenuItem)
            .filter(menuItem => !menuItem.hasAttribute("right"))
            .reduce((acc, menuItem) => acc + menuItem.offsetWidth, 0);
    }

    _calculateMenuItemsToMove(menuBar, maxLeft) {
        let menuItemsToMove = [];
        let menuItemsWidthSum = this._calculateMoreMenuItemWidth(menuBar);

        [...menuBar.children]
            .filter(child => child instanceof MenuItem)
            .filter(menuItem => !menuItem.hasAttribute("right"))
            .filter(menuItem => !menuItem.hasAttribute("more"))
            .forEach(menuItem => {
                menuItemsWidthSum += menuItem.offsetWidth;
                if (menuItemsWidthSum > maxLeft && !menuItem.hasAttribute("more")) {
                    menuItemsToMove.push(menuItem);
                }
            });
        return menuItemsToMove;
    }

    _createOrGetMenuItemMore(menuBar) {
        const moreMenuItems = [...menuBar.children]
            .filter(child => child instanceof MenuItem)
            .filter(menuItem => menuItem.hasAttribute("more"));
        if (moreMenuItems.length >= 1) {
            if (moreMenuItems.length > 1) {
                console.warn(`The menu bar (${menuBar.attr['menubar-id']}) contains more than one menu item used for 'more'`);
            }
            return moreMenuItems[0];
        } else {
            const moreMenuItem = new MenuItem();
            moreMenuItem.name = '↓';
            moreMenuItem.openRight = true;
            moreMenuItem.setAttribute('more', true);
            return moreMenuItem;
        }
    }

    _rearrangeMenuBar(menuBar) {
        const maxLeft = this._calculateRightSize(menuBar);
        const menuItemsToMove = this._calculateMenuItemsToMove(menuBar, maxLeft);

        const moreMenuItem = this._createOrGetMenuItemMore(menuBar);
        if (menuItemsToMove.length) {
            menuItemsToMove.reverse().forEach(menuItem => {
                this._moveMenuItemUnderMore(menuItem, moreMenuItem);
            });
            menuBar.appendChild(moreMenuItem);
        }

        this._tryToRestoreNextMenuItemToBar(menuBar, moreMenuItem, maxLeft);
    }

    _tryToRestoreNextMenuItemToBar(menuBar, moreMenuItem, maxLeft) {
        if (menuBar.contains(moreMenuItem)) {
            if (moreMenuItem.children.length) {
                const menuItem = moreMenuItem.firstChild;
                if (this._calculateMenuItemsWidth(menuBar) + menuItem.offsetWidth + this.collapseDelta < maxLeft) {
                    this._moveMenuItemBackToBar(menuItem, moreMenuItem, menuBar);
                    this._tryToRestoreNextMenuItemToBar(menuBar, moreMenuItem, maxLeft);
                }
            } else {
                menuBar.removeChild(moreMenuItem);
            }
        }
    }

    _moveMenuItemUnderMore(menuItem, moreMenuItem) {
        this._markOpenRight(menuItem);
        moreMenuItem.insertBefore(menuItem, moreMenuItem.firstChild);
        menuItem._initStyle();
        menuItem._postProcessMenuItem();
    }

    _moveMenuItemBackToBar(menuItem, moreMenuItem, menuBar) {
        menuItem.style.width = '';
        menuItem.openRight = false;
        menuBar.insertBefore(menuItem, moreMenuItem);
        this._markOpenRight(menuItem, false);
        menuItem._initStyle();
        menuItem._postProcessMenuItem();
    }

}

customElements.define(MenuBar.is, MenuBar);
