/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { MenuBar } from './menu-bar.js';

export class MenuItem extends ThemableMixin(PolymerElement) {

    static get template() {
        return html`
            <style>
                @keyframes showMenu {
                    0% {
                        filter: opacity(0);
                    }
                    100% {
                        filter: opacity(1);
                    }
                }

                :host {
                    color: #303030;
                }

                [part="button"] {
                    text-transform: none;
                    white-space: nowrap;
                    user-select: none;
                    padding: 8px 24px;
                    position: relative;
                }

                .btn-unselected {
                    transition: background-color .5s ease;
                }

                .btn-unselected:hover {
                    background-color: #E0E0F0;
                    transition: background-color .5s ease;
                }

                .btn-selected {
                    color: #101010;
                    background-color: #D0E0F0;
                    transition: all .5s ease;
                }

                .submenuRightIndicator {
                    display: none;
                    position: absolute;
                    right: 0;
                    padding-right: 4px;
                }

                .submenuLeftIndicator {
                    display: none;
                    position: absolute;
                    left: 2px;
                }

                .submenuLeftIndicator::after {
                    font-style: normal;
                    content: "<"
                }

                .submenuRightIndicator::after {
                    font-style: normal;
                    content: ">"
                }

                [part="submenu"] {
                    --menu-border: 1px solid #E0E0E0;
                    clear: both;
                    position: absolute;
                    padding: 4px;
                    min-width: 120px;
                    z-index: 100;
                    animation: .2s ease showMenu;
                    background-color: #F0F0F0;
                    border-left: var(--menu-border);
                    border-right: var(--menu-border);
                    border-bottom: var(--menu-border);
                }

                .submenu-first {
                    top: 100%;
                    left: 0;
                }

                .submenu-left {
                    top: 0;
                    left: 100%;
                }

                .submenu-right {
                    top: 0;
                    right: 0;
                }
            </style>

            <template is="dom-if" if="[[disabled]]" on-dom-change="_postProcessMenuItem">
                <div id="menuBtn" part="button" class="disabled">
                    <i id="submenuLeftIndicator" class="submenuLeftIndicator"></i>
                    <span part="title">[[name]]</span>
                    <i id="submenuRightIndicator" class="submenuRightIndicator"></i>
                </div>
            </template>

            <template is="dom-if" if="[[!disabled]]" on-dom-change="_postProcessMenuItem">
                <div id="menuBtn"
                     part="button"
                     on-click="_onTap"
                     on-mouseover="_onOver"
                     on-mouseout="_onOut"
                     class="btn-unselected">

                    <i id="submenuLeftIndicator" class="submenuLeftIndicator"></i>
                    <span part="title">[[name]]</span>
                    <i id="submenuRightIndicator" class="submenuRightIndicator"></i>
                    <div id="submenu"
                         part="submenu"
                         hidden$="[[!submenuVisible]]"
                         class$="submenu [[_containerClass]]">

                        <slot></slot>
                    </div>
                </div>
            </template>
                `;
    }

    static get is() {
        return 'menu-item'
    }

    static get properties() {
        return {
            name: {
                type: String,
                notify: true,
                value: ''
            },
            disabled: {
                type: Boolean,
                notify: true,
                value: false
            },
            right: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_onRightChanged'
            },
            openRight: {
                type: Boolean,
                value: false
            },
            submenuVisible: {
                type: Boolean,
                notify: true,
                value: false
            },
            _containerClass: {
                type: String,
                value: 'submenu-first'
            }
        }
    }

    getSubmenuItems() {
        const submenuItems = [];
        this.shadowRoot.querySelector('slot').assignedNodes().forEach(e => {
            if (e instanceof MenuItem) {
                submenuItems.push(e);
            }
        });

        return submenuItems;
    }

    isSubmenu() {
        const submenuItems = this.getSubmenuItems();
        const len = submenuItems.length;
        return len > 0;
    }

    _onTap(e) {
        if (this.isSubmenu()) {
            if (!this.submenuVisible) {
                this._open();
                this._closeMenuItems(false, this);
            }
        } else {
            this.dispatchEvent(new CustomEvent('activate', {detail: {menuItem: this}}));
            this._closeMenuItems(true);
            e.stopPropagation();
        }
    }

    _onOver() {
        if ((this.parentNode instanceof MenuItem) || this._getOpenedMenuItems().length > 0) {
            if (this.isSubmenu()) {
                if (!this.submenuVisible) {
                    this._menuOpenTimer = setTimeout(() => this._open(), 50);
                    this._closeMenuItems(false, this);
                }
            } else {
                this._closeMenuItems(false, this);
            }
        }
    }

    _onOut() {
        clearTimeout(this._menuOpenTimer);
    }

    _open() {
        this.set('submenuVisible', true);
        const menuBtn = this.shadowRoot.querySelector('#menuBtn');
        menuBtn.classList.remove('btn-unselected');
        menuBtn.classList.add('btn-selected');
        const submenu = this._getSubmenu();
        if (this._isRightMenu()) {
            this._openRightMenu(menuBtn, submenu);
        } else {
            this._openLeftMenu(menuBtn, submenu);
        }
    }

    _getSubmenu() {
        return this.shadowRoot.querySelector('#menuBtn #submenu');
    }

    _openRightMenu(menuBtn, submenu) {
        if (this.parentNode instanceof MenuBar) {
            if (!this.hasAttribute('more')) {
                submenu.style.right = menuBtn.offsetWidth + 'px';
            }
            const submenuX = submenu.getBoundingClientRect().x;
            const submenuWidth = submenu.getBoundingClientRect().width;
            const windowWidth = window.innerWidth;
            if (windowWidth - submenuX < submenuWidth) {
                submenu.style.left = (windowWidth - (submenuX + submenuWidth)) + 'px';
            }
        } else {
            const submenuWidth = submenu.getBoundingClientRect().width;
            submenu.style.left = -submenuWidth + 'px';
            const padding = (
                parseInt(window.getComputedStyle(submenu, null).getPropertyValue('padding-left'), 10) +
                parseInt(window.getComputedStyle(submenu, null).getPropertyValue('border-left-width'), 10)) * 2;
            submenu.style.width = (submenuWidth - padding) + 'px';

            const currentX = submenu.getBoundingClientRect().x;
            if (currentX < 0) {
                submenu.style.left = (-submenuWidth - 8 - currentX) + 'px';
            }
        }
    }

    _openLeftMenu(menuBtn, submenu) {
        submenu.style.right = '';
        submenu.style.left = '';

        const submenuX = submenu.getBoundingClientRect().x;
        const submenuWidth = submenu.getBoundingClientRect().width;
        const windowWidth = window.innerWidth;
        if (windowWidth - submenuX < submenuWidth) {
            submenu.style.left = (submenu.offsetLeft - ((submenuX + submenuWidth) - windowWidth)) + 'px';
        }
    }

    _close() {
        this.set('submenuVisible', false);
        const menuButtonClassList = this.shadowRoot.querySelector('#menuBtn').classList;
        menuButtonClassList.remove('btn-selected');
        menuButtonClassList.add('btn-unselected');
    }

    _closeMenuItems(all, markOpenMenuItem = null) {
        const openedMenuItems = this._getOpenedMenuItems();
        if (all) {
            openedMenuItems.forEach(menuItem => {
                menuItem._close();
            });
            openedMenuItems.length = 0;
        } else {
            openedMenuItems.reduceRight((acc, menuItem, index, object) => {
                if (!this._isMenuItemMyParent(menuItem)) {
                    menuItem._close();
                    object.splice(index, 1);
                }
            }, []);
        }
        if (markOpenMenuItem != null) {
            openedMenuItems.push(markOpenMenuItem);
        }
    }

    _isMenuItemMyParent(menuItem) {
        let pn = this.parentNode;
        while (pn !== menuItem && pn != null) {
            pn = pn.parentNode;
        }
        return pn === menuItem;
    }

    _getOpenedMenuItems() {
        let pn = this.parentNode;
        while (!(pn instanceof MenuBar) && pn != null) {
            pn = pn.parentNode;
        }
        if (pn === undefined) {
            console.error("Could not find parent menu-bar");
            return null;
        } else {
            return pn._openedMenuItems;
        }
    }

    _isRightMenu() {
        if (this.openRight) {
            return true;
        } else {
            let node = this;
            while (node != null && !(node.parentNode instanceof MenuBar)) {
                node = node.parentNode;
            }
            return node != null ? node.right : false;
        }
    }

    _postProcessMenuItem() {
        let displayLeft = 'none';
        let displayRight = 'none';
        if (this.children.length > 0 && this.parentNode instanceof MenuItem) {
            displayLeft = this._isRightMenu() ? 'inline' : 'none';
            displayRight = this._isRightMenu() ? 'none' : 'inline';
        }
        this.shadowRoot.querySelector('#submenuLeftIndicator').style.display = displayLeft;
        this.shadowRoot.querySelector('#submenuRightIndicator').style.display = displayRight;
    }

    _onRightChanged() {
        if (this.parentNode instanceof MenuBar) {
            if (this.right) {
                this.style.float = 'right';
            } else {
                this.style.float = 'left';
            }
        }
    }

    ready() {
        super.ready();
        this._initStyle();

        new MutationObserver(() => this._postProcessMenuItem())
            .observe(this, {childList: true});
    }

    _initStyle() {
        if (this.parentNode instanceof MenuItem) {
            this._containerClass = this._isRightMenu() ?
                'submenu-right' :
                'submenu-left';
            this.style.width = '100%';
        }
        if (this.parentNode instanceof MenuBar) {
            this._containerClass = 'submenu-first';
            if (this.right) {
                this.style.float = 'right';
            } else {
                this.style.float = 'left';
            }
        } else {
            const submenu = this._getSubmenu();
            if (submenu) {
                submenu.style.left = '';
                submenu.style.width = '';
            }
        }
    }
}

customElements.define(MenuItem.is, MenuItem);
