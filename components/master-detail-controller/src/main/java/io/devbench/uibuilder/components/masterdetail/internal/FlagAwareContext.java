/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.internal;

import java.util.concurrent.atomic.AtomicBoolean;

public class FlagAwareContext {

    private AtomicBoolean progressAllowed = new AtomicBoolean(true);

    public boolean canProgress() {
        return progressAllowed.get();
    }

    public void progress(Runnable runnable) {
        if (canProgress()) {
            runnable.run();
        }
    }

    public void run(Runnable runnable) {
        if (progressAllowed.getAndSet(false)) {
            runnable.run();
            progressAllowed.set(true);
        }
    }
}
