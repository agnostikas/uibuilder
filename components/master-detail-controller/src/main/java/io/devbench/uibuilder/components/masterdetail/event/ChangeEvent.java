/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.event;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;

@DomEvent(ChangeEvent.VALUE_CHANGED)
public class ChangeEvent extends ComponentEvent<UIBuilderMasterDetailController> {

    public static final String VALUE_CHANGED = "value-changed";

    public ChangeEvent(UIBuilderMasterDetailController source) {
        super(source, false);
    }
}
