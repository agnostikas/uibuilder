/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.event;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;


/**
 * UIBuilder qualifier aware Refresh event class, @DomEvent is intentionally missing
 */
public class RefreshEvent extends QualifierAwareComponentEvent {

    public RefreshEvent(UIBuilderMasterDetailController source, String... customQualifiers) {
        super(source, customQualifiers);
    }

    public RefreshEvent(UIBuilderMasterDetailController source, EventQualifier... eventQualifiers) {
        super(source, eventQualifiers);
    }

    public RefreshEvent(UIBuilderMasterDetailController source, EventQualifier[] eventQualifiers, String[] customQualifiers) {
        super(source, eventQualifiers, customQualifiers);
    }

}
