/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.exception;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.exceptions.ComponentException;

public class MasterConnectorNotFoundException extends ComponentException {

    public MasterConnectorNotFoundException(Component component) {
        super(component.getClass().getName());
    }

    public MasterConnectorNotFoundException(Class<? extends Component> componentClass) {
        super("Master connector cannot be found for component: " + componentClass.getName());
    }

}
