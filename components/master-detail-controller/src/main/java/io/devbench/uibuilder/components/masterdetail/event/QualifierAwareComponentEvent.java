/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.event;

import com.vaadin.flow.component.ComponentEvent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;

import java.util.*;

public abstract class QualifierAwareComponentEvent extends ComponentEvent<UIBuilderMasterDetailController> {

    private Set<String> customQualifiers = new HashSet<>();
    private Set<EventQualifier> eventQualifiers = new HashSet<>();

    public QualifierAwareComponentEvent(UIBuilderMasterDetailController source, String... customQualifiers) {
        super(source, false);
        Collections.addAll(this.customQualifiers, customQualifiers);
    }

    public QualifierAwareComponentEvent(UIBuilderMasterDetailController source, EventQualifier... eventQualifiers) {
        super(source, false);
        Collections.addAll(this.eventQualifiers, eventQualifiers);
    }

    public QualifierAwareComponentEvent(UIBuilderMasterDetailController source, EventQualifier[] eventQualifiers, String[] customQualifiers) {
        super(source, false);
        Collections.addAll(this.customQualifiers, customQualifiers);
        Collections.addAll(this.eventQualifiers, eventQualifiers);
    }

    public Collection<String> getCustomQualifiers() {
        return customQualifiers;
    }

    public Collection<EventQualifier> getEventQualifiers() {
        return eventQualifiers;
    }

    public boolean isEventQualifiersPresent(EventQualifier... eventQualifiers) {
        return this.eventQualifiers.containsAll(Arrays.asList(eventQualifiers));
    }

    public boolean isCustomQualifiersPresent(String... stringQualifiers) {
        return this.customQualifiers.containsAll(Arrays.asList(stringQualifiers));
    }

}
