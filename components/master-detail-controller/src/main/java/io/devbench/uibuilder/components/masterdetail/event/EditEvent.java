/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.event;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;


/**
 * UIBuilder qualifier aware Edit event class, @DomEvent is intentionally missing
 */
public class EditEvent extends QualifierAwareComponentEvent {

    private String propertyName;
    private String oldValue;
    private String newValue;

    public EditEvent(UIBuilderMasterDetailController source, String... customQualifiers) {
        super(source, customQualifiers);
    }

    public EditEvent(UIBuilderMasterDetailController source, EventQualifier... eventQualifiers) {
        super(source, eventQualifiers);
    }

    public EditEvent(UIBuilderMasterDetailController source, EventQualifier[] eventQualifiers, String[] customQualifiers) {
        super(source, eventQualifiers, customQualifiers);
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
}
