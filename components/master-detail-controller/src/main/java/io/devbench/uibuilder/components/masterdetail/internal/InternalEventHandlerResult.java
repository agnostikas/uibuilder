/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.internal;

import io.devbench.uibuilder.api.exceptions.ProgressInterruptException;

import java.util.function.Consumer;

public class InternalEventHandlerResult implements EventHandlerResult {

    private ProgressInterruptException progressInterruptException;

    @Override
    public EventHandlerResult onProgress(Runnable runOnProgress) {
        if (progressInterruptException == null) {
            runOnProgress.run();
        }
        return this;
    }

    @Override
    public EventHandlerResult onError(Consumer<ProgressInterruptException> handleError) {
        if (progressInterruptException != null) {
            handleError.accept(progressInterruptException);
        }
        return this;
    }

    public static EventHandlerResult create(ProgressInterruptException progressInterruptException) {
        InternalEventHandlerResult internalEventHandlerResult = new InternalEventHandlerResult();
        internalEventHandlerResult.progressInterruptException = progressInterruptException;
        return internalEventHandlerResult;
    }
}
