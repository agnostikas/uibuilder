/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.connector;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import io.devbench.uibuilder.api.components.masterconnector.UIBuilderMasterConnector;
import io.devbench.uibuilder.api.exceptions.MasterConnectorNotDirectlyModifiableException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GridMasterConnectorTest {

    private UIBuilderMasterConnector<?, ?> testObj;

    @SuppressWarnings("unchecked")
    private GridMasterConnector<String> getStringGridConnector() {
        return (GridMasterConnector<String>) testObj;
    }

    @BeforeEach
    void setUp() {
        testObj = new GridMasterConnector<String>();
    }

    @Test
    @DisplayName("should be applicable for Grid")
    @SuppressWarnings("unchecked")
    void test_should_be_applicable_for_grid() {
        assertFalse(testObj.isApplicable(DatePicker.class));
        assertTrue(testObj.isApplicable(Grid.class));
        assertFalse(testObj.isApplicable(TextField.class));
    }

    @Test
    @DisplayName("should create connection with selection change listener")
    void test_should_create_connection_with_selection_change_listener() {
        GridMasterConnector<String> connector = getStringGridConnector();
        Grid<String> grid = new Grid<>();

        connector.connect(grid);
        AtomicInteger callCounter = new AtomicInteger(0);

        connector.addSelectionChangedListener(event -> {
            int currentRun = callCounter.incrementAndGet();
            assertFalse(event.isFromClient());
            if (currentRun == 1) {
                assertIterableEquals(Collections.emptyList(), event.getPreviouslySelectedItems());
                assertIterableEquals(Collections.singleton("Selected"), event.getNewlySelectedItems());
            } else if (currentRun == 2) {
                assertIterableEquals(Collections.singleton("Selected"), event.getPreviouslySelectedItems());
                assertIterableEquals(Collections.singleton("Another"), event.getNewlySelectedItems());
            } else {
                fail("Should not be called more that two times");
            }
        });

        grid.select("Selected");

        assertEquals(1, callCounter.get());

        grid.select("Another");

        assertEquals(2, callCounter.get());

        connector.disconnect();
        grid.select("Ignored");

        assertEquals(2, callCounter.get());
    }

    @Test
    @DisplayName("should get master component or throw npe if not specified")
    void test_should_get_master_component_or_throw_npe_if_not_specified() {
        GridMasterConnector<String> connector = getStringGridConnector();

        assertThrows(NullPointerException.class, connector::getMasterComponent, "Should throw NPE");

        Grid<String> grid = new Grid<>();
        connector.connect(grid);

        assertSame(grid, connector.getMasterComponent());
    }

    @Test
    @DisplayName("should set selected items")
    void test_should_set_selected_items() {
        GridMasterConnector<String> connector = getStringGridConnector();
        @SuppressWarnings("unchecked")
        Grid<String> grid = mock(Grid.class);
        @SuppressWarnings("unchecked")
        GridSelectionModel<String> selectionModel = mock(GridSelectionModel.class);

        doReturn(selectionModel).when(grid).getSelectionModel();

        connector.connect(grid);
        assertIterableEquals(Collections.emptyList(), connector.getSelectedItems());

        @SuppressWarnings("ConstantConditions")
        String message = assertThrows(NullPointerException.class, () -> connector.setSelectedItems(null),
            "Should throw NPE when calling set selected items with null").getMessage();
        assertEquals("Selected items collection cannot be null", message);

        connector.setSelectedItems(Arrays.asList("One", "Two"));

        verify(selectionModel).select(eq("One"));
        verify(selectionModel).select(eq("Two"));
        verify(selectionModel, never()).deselect(any());
        assertIterableEquals(Arrays.asList("One", "Two"), connector.getSelectedItems());
    }

    @Test
    @DisplayName("should call refresh on grid")
    void test_should_call_refresh_on_grid() {
        GridMasterConnector<String> connector = getStringGridConnector();
        @SuppressWarnings("unchecked")
        Grid<String> grid = mock(Grid.class);
        @SuppressWarnings("unchecked")
        DataProvider<String, ?> dataProvider = mock(DataProvider.class);

        connector.connect(grid);

        doReturn(dataProvider).when(grid).getDataProvider();

        connector.refresh();

        verify(dataProvider).refreshAll();

        connector.refresh("Test");

        verify(dataProvider).refreshItem(eq("Test"));
    }

    @Test
    @DisplayName("should set and get property enabled status")
    void test_should_set_and_get_property_enabled_status() {
        GridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(new Grid<>());

        assertTrue(connector.isEnabled(), "Enabled should be true");

        connector.setEnabled(false);

        assertFalse(connector.isEnabled(), "Enabled should be false");

        connector.setEnabled(true);

        assertTrue(connector.isEnabled(), "Enabled should be true again");
    }

    @Test
    @DisplayName("should return direct modifiable false if data provider is not list data provider")
    void test_should_return_direct_modifiable_false_if_data_provider_is_not_list_data_provider() {
        GridMasterConnector<String> connector = getStringGridConnector();
        Grid<String> grid = new Grid<>();

        @SuppressWarnings("unchecked")
        DataProvider<String, ?> notAListDataProvider = mock(DataProvider.class);
        grid.setDataProvider(notAListDataProvider);

        connector.connect(grid);
        assertFalse(connector.isDirectModifiable());
    }

    @Test
    @DisplayName("should return direct modifiable true if data provider is list data provider")
    void test_should_return_direct_modifiable_true_if_data_provider_is_list_data_provider() {
        GridMasterConnector<String> connector = getStringGridConnector();
        Grid<String> grid = new Grid<>();

        connector.connect(grid);
        assertTrue(connector.isDirectModifiable());
    }

    @Test
    @DisplayName("should throw exception when trying to add item with non direct modifiable data provider")
    void test_should_throw_exception_when_trying_to_add_item_with_non_direct_modifiable_data_provider() {
        GridMasterConnector<String> connector = getStringGridConnector();
        Grid<String> grid = new Grid<>();

        @SuppressWarnings("unchecked")
        DataProvider<String, ?> notAListDataProvider = mock(DataProvider.class);
        grid.setDataProvider(notAListDataProvider);

        connector.connect(grid);

        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> connector.addItem("New Item"));
    }

    @Test
    @DisplayName("should add new item if direct modifiable")
    void test_should_add_new_item_if_direct_modifiable() {
        GridMasterConnector<String> connector = getStringGridConnector();
        Grid<String> grid = new Grid<>();
        grid.setItems(new ArrayList<>(Collections.singletonList("Initial")));

        assertTrue(grid.getDataProvider() instanceof ListDataProvider);
        @SuppressWarnings("unchecked")
        ListDataProvider<String> listDataProvider = (ListDataProvider<String>) grid.getDataProvider();

        connector.connect(grid);
        connector.addItems(Arrays.asList("Item 1", "Item 2"));

        assertIterableEquals(Arrays.asList("Initial", "Item 1", "Item 2"), listDataProvider.getItems());
    }

    @Test
    @DisplayName("should remove item if direct modifiable")
    void test_should_remove_item_if_direct_modifiable() {
        GridMasterConnector<String> connector = getStringGridConnector();
        Grid<String> grid = new Grid<>();
        grid.setItems(new ArrayList<>(Arrays.asList("Item 1", "Item 2", "Item 3")));

        assertTrue(grid.getDataProvider() instanceof ListDataProvider);
        @SuppressWarnings("unchecked")
        ListDataProvider<String> listDataProvider = (ListDataProvider<String>) grid.getDataProvider();

        connector.connect(grid);
        connector.removeItem("Item 2");

        assertIterableEquals(Arrays.asList("Item 1", "Item 3"), listDataProvider.getItems());
    }

}
