/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.interceptor;

import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailControllerEventHandlerMethod;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(JsoupExtension.class)
class MasterDetailControllerParseInterceptorTest {

    private MasterDetailControllerParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new MasterDetailControllerParseInterceptor();
    }

    @Test
    @DisplayName("should be applicable by tag name")
    void test_should_be_applicable_by_tag_name() {
        assertTrue(testObj.isApplicable(new Element(UIBuilderMasterDetailController.TAG_NAME)));
    }

    @Test
    @DisplayName("should NOT be applicable if tag name doesn't match")
    void test_should_not_be_applicable_if_tag_name_doesn_t_match() {
        assertFalse(testObj.isApplicable(new Element("anything-else")));
    }

    @Test
    @DisplayName("should always be an instantiator interceptor")
    void test_should_always_be_an_instantiator_interceptor() {
        assertTrue(testObj.isInstantiator(new Element("anything")));
        assertTrue(testObj.isInstantiator(null));
    }

    @Test
    @DisplayName("should ensure component ID and fill master and detail attribtues")
    void test_should_ensure_component_id_and_fill_master_and_detail_attribtues(@LoadElement("/test.html") Element page) {
        Element mdcElement = page.getElementById("mdc");
        UIBuilderMasterDetailController<?> mdc = new UIBuilderMasterDetailController<>();

        testObj.intercept(mdc, mdcElement);

        assertEquals("mdc", mdc.getId().orElse(null));
        assertEquals("grid", mdc.getMasterId());
        assertEquals("dp", mdc.getDetailId());
        assertEquals("item", mdc.getDetailProperty());
    }

    @Test
    @DisplayName("should set detail property if element attribute is present")
    void test_should_set_detail_property_if_element_attribute_is_present(@LoadElement("/test.html") Element page) {
        Element mdcElement = page.getElementById("mdc-with-d-prop");
        UIBuilderMasterDetailController<?> mdc = new UIBuilderMasterDetailController<>();

        testObj.intercept(mdc, mdcElement);
        assertEquals("customValue", mdc.getDetailProperty());
    }

    @Test
    @DisplayName("should parse only the on-item-selected")
    void test_should_parse_only_the_on_item_selected(@LoadElement("/test.html") Element page) {
        Element mdcElement = page.getElementById("mdc");
        UIBuilderMasterDetailController<?> mdc = Mockito.spy(new UIBuilderMasterDetailController<>());

        testObj.intercept(mdc, mdcElement);

        Mockito.verify(mdc, new Times(1)).registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.CREATE, "bean::create");
        assertFalse(mdcElement.hasAttr("on-create"));
    }

    @Test
    @DisplayName("Should find outer MDC")
    void test_should_find_outer_mdc(@LoadElement("/nestedMdc.html") Element page) {
        Element innerMdc = page.getElementById("innerMdc");
        UIBuilderMasterDetailController<?> mdc = Mockito.spy(new UIBuilderMasterDetailController<>());
        assertNotNull(innerMdc);

        testObj.intercept(mdc, innerMdc);

        ArgumentCaptor<String> parentMdcIdCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(mdc).setParentMdcId(parentMdcIdCaptor.capture());
        assertEquals("outerMdc", parentMdcIdCaptor.getValue());
    }

}
