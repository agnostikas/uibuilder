/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.internal.nodefeature.ElementListenerMap;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import io.devbench.uibuilder.api.components.masterconnector.UIBuilderMasterConnector;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.exceptions.ComponentInternalException;
import io.devbench.uibuilder.api.exceptions.ProgressInterruptException;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.masterdetail.connector.GridMasterConnector;
import io.devbench.uibuilder.components.masterdetail.connector.HasValueMasterConnector;
import io.devbench.uibuilder.components.masterdetail.event.*;
import io.devbench.uibuilder.components.masterdetail.exception.MasterDetailControllerInterruptException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.UIEventHandlerContext;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.page.Page;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.core.utils.HtmlElementAwareComponent;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.creation.MockSettingsImpl;
import org.mockito.internal.verification.Times;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith({JsoupExtension.class, MockitoExtension.class, SingletonProviderForTestsExtension.class})
class UIBuilderMasterDetailControllerTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    private UIBuilderMasterDetailController testObj;

    @BeforeEach
    void setUp(@LoadElement("/test.html") Element page) {
        doReturn(new HashSet<>(Arrays.asList(HasValueMasterConnector.class, GridMasterConnector.class)))
            .when(memberScanner).findClassesBySuperType(UIBuilderMasterConnector.class);

        ElementCollector.register(new Grid(), page.getElementById("grid"));
        ElementCollector.register(new MockDetailPanel(), page.getElementById("dp"));
        ElementCollector.register(new ComboBox(), page.getElementById("cb"));

        testObj = new UIBuilderMasterDetailController();
    }

    @Test
    @DisplayName("should find or revive master and detail component by their id on attach")
    void test_should_find_or_revive_master_and_detail_component_by_their_id_on_attach() {
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        assertTrue(testObj.getMasterComponent().isPresent());
        assertTrue(testObj.getDetailComponent().isPresent());
    }

    @Test
    @DisplayName("should set selected item in detail on selected item event")
    void test_should_set_selected_item_in_detail_on_selected_item_event() {
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        Grid grid = (Grid) testObj.getMasterComponent().get();
        assertNotNull(grid);

        grid.setItems("Line1", "Line2", "Line3");
        grid.select("Line2");

        fireDomEvent(testObj, "selected-item-changed");

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        Serializable dpItem = dp.getItem();

        assertNotNull(dpItem);
        assertEquals("Line2", dpItem);
    }

    @Test
    @DisplayName("detail panel should return null if mdc knows an invalid detail property")
        // TODO: or should throw an exception?
    void test_detail_panel_should_return_null_if_mdc_knows_an_invalid_detail_property() {
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.setDetailProperty("customItemProp");
        testObj.onAttached();

        Grid grid = (Grid) testObj.getMasterComponent().get();
        assertNotNull(grid);

        grid.setItems("Line1", "Line2", "Line3");
        grid.select("Line2");
        ComponentUtil.fireEvent(testObj, new ItemSelectedEvent(testObj, "custom"));

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        Serializable dpItem = dp.getItem();

        assertNull(dpItem);
    }

    @Test
    @DisplayName("detail panel should return null if mdc knows a detail property that is not accessible")
        // TODO: or should throw an exception?
    void test_detail_panel_should_return_null_if_mdc_knows_a_detail_property_that_is_not_accessible() {
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.setDetailProperty("privItem");
        testObj.onAttached();

        Grid grid = (Grid) testObj.getMasterComponent().get();
        assertNotNull(grid);

        grid.setItems("Line1", "Line2", "Line3");
        grid.select("Line2");
        ComponentUtil.fireEvent(testObj, new ItemSelectedEvent(testObj, "custom"));

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        Serializable dpItem = dp.getItem();

        assertNull(dpItem);
    }

    @Test
    @DisplayName("should set null to detail's property on multiple selection")
    void test_should_set_null_to_detail_s_property_on_multiple_selection() {
        ElementCollector.getById("grid").ifPresent(
            heac -> ((Grid) heac.getComponent()).setSelectionMode(Grid.SelectionMode.MULTI)
        );

        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        Grid grid = (Grid) testObj.getMasterComponent().get();
        assertAll(
            () -> assertNotNull(grid),
            () -> assertTrue(grid.getSelectionModel() instanceof GridMultiSelectionModel)
        );

        grid.setItems("Line1", "Line2", "Line3");
        grid.asMultiSelect().select("Line3", "Line2");

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        Serializable dpItem = dp.getItem();

        assertNull(dpItem);
        assertTrue(testObj.getSelectedItems().contains("Line3"));
        assertTrue(testObj.getSelectedItems().contains("Line2"));
    }

    @Test
    @DisplayName("should call fireSelectedItemChanged JS function on every selection and set getSelectedItem and getSelectedItems")
    void test_should_call_fire_selected_item_changed_js_function_on_every_selection_and_set_get_selected_item_and_get_selected_items() {
        testObj = Mockito.spy(testObj);
        com.vaadin.flow.dom.Element mdcElement = spy(testObj.getElement());
        doReturn(mdcElement).when(testObj).getElement();

        testObj.setSelectedItems(new HashSet<>(Arrays.asList("One", "Two")));

        assertAll(
            () -> assertFalse(testObj.isOnlyOneItemSelected()),
            () -> assertNotNull(testObj.getSelectedItems()),
            () -> assertEquals(2, testObj.getSelectedItems().size()),
            () -> assertTrue(testObj.getSelectedItems().contains("One")),
            () -> assertTrue(testObj.getSelectedItems().contains("Two")),
            () -> assertNull(testObj.getSelectedItem()),
            () -> verify(mdcElement, new Times(1)).callJsFunction(eq("_fireSelectedItemChanged"), eq(true), eq(false))
        );

        testObj.setSelectedItem("Three");

        assertAll(
            () -> assertTrue(testObj.isOnlyOneItemSelected()),
            () -> assertNotNull(testObj.getSelectedItems()),
            () -> assertEquals(1, testObj.getSelectedItems().size()),
            () -> assertTrue(testObj.getSelectedItems().contains("Three")),
            () -> assertNotNull(testObj.getSelectedItem()),
            () -> assertEquals("Three", testObj.getSelectedItem()),
            () -> verify(mdcElement, new Times(2)).callJsFunction(eq("_fireSelectedItemChanged"), eq(true), eq(false))
        );

        testObj.setSelectedItem(null);

        assertAll(
            () -> assertFalse(testObj.isOnlyOneItemSelected()),
            () -> assertNull(testObj.getSelectedItems()),
            () -> assertNull(testObj.getSelectedItem()),
            () -> verify(mdcElement, new Times(1)).callJsFunction(eq("_fireSelectedItemChanged"), eq(false), eq(false))
        );

        testObj.setSelectedItems(null);

        assertAll(
            () -> assertFalse(testObj.isOnlyOneItemSelected()),
            () -> assertNull(testObj.getSelectedItems()),
            () -> assertNull(testObj.getSelectedItem()),
            () -> verify(mdcElement, new Times(2)).callJsFunction(eq("_fireSelectedItemChanged"), eq(false), eq(false))
        );

        testObj.setSelectedItems(Collections.emptySet());

        assertAll(
            () -> assertFalse(testObj.isOnlyOneItemSelected()),
            () -> assertNotNull(testObj.getSelectedItems()),
            () -> assertTrue(testObj.getSelectedItems().isEmpty()),
            () -> assertNull(testObj.getSelectedItem()),
            () -> verify(mdcElement, new Times(3)).callJsFunction(eq("_fireSelectedItemChanged"), eq(false), eq(false))
        );
    }

    @Test
    @DisplayName("masterEnabledControlDisabled property should be configurable")
    void test_master_enabled_control_disabled_property_should_be_configurable() {
        assertFalse(testObj.isMasterEnabledControlDisabled());
        testObj.setMasterEnabledControlDisabled(true);
        assertTrue(testObj.isMasterEnabledControlDisabled());
    }

    @Test
    @DisplayName("should set detail item when master is hasValue and selectedItemChanged event occures")
    void test_should_set_detail_item_when_master_is_has_value_and_selected_item_changed_event_occures() {
        testObj.setMasterId("cb");
        testObj.setDetailId("dp");
        testObj.onAttached();

        ComboBox comboBox = (ComboBox) testObj.getMasterComponent().get();
        assertNotNull(comboBox);

        comboBox.setItems("Line1", "Line2", "Line3");
        comboBox.setValue("Line2");

        fireDomEvent(testObj, "selected-item-changed");

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        Serializable dpItem = dp.getItem();

        assertNotNull(dpItem);
        assertEquals("Line2", dpItem);
    }

    @Test
    public void should_call_frontend_error_handler_on_error() {
        UIBuilderMasterDetailController spiedObj = Mockito.spy(testObj);
        com.vaadin.flow.dom.Element elementMock = mock(com.vaadin.flow.dom.Element.class);
        when(spiedObj.getElement()).thenReturn(elementMock);

        JsonObject expected = Json.createObject();
        expected.put("message", "Test message");

        doAnswer(i -> {
            JsonObject actual = i.getArgument(0);
            assertTrue(actual.jsEquals(expected));
            return null;
        }).when(elementMock).callJsFunction(eq("_onError"), any());

        testObj.showErrorOnFrontend(new ProgressInterruptException("Test message"));
    }

    @Test
    @DisplayName("should create item and set created flag")
    void test_should_create_item_and_set_created_flag() {
        testObj.setMasterId("cb");
        testObj.setDetailId("dp");
        testObj.onAttached();

        ((ComboBox<String>) testObj.getMasterComponent().get()).setItems("This is the new item");

        testObj.createItem("This is the new item");
        fireDomEvent(testObj, "selected-item-changed");

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        Serializable dpItem = dp.getItem();

        assertNotNull(dpItem);
        assertEquals("This is the new item", dpItem);
        assertTrue(testObj.isSelectedItemCreated());
    }

    @Test
    @DisplayName("should ignore selected item changes after detached")
    void test_should_ignore_selected_item_changes_after_detached() {
        testObj.setMasterId("cb");
        testObj.setDetailId("dp");
        testObj.onAttached();

        MockDetailPanel dp = (MockDetailPanel) testObj.getDetailComponent().get();
        dp.setItem("original");

        @SuppressWarnings("unchecked")
        ComboBox<String> cb = (ComboBox<String>) testObj.getMasterComponent().get();
        cb.setItems("This is the new item");
        testObj.onDetach(new DetachEvent(testObj));

        ComponentUtil.fireEvent(cb, new AbstractField.ComponentValueChangeEvent<>(cb, cb, "old", false));

        Serializable dpItem = dp.getItem();
        assertNotNull(dpItem);
        assertEquals("original", dpItem);
    }

    @Test
    @DisplayName("should register and listener be called on master enabled changed event")
    void test_should_register_and_listener_be_called_on_master_enabled_changed_event() {
        List<Class<? extends ComponentEvent>> calledEvents = new ArrayList<>();
        testObj.addMasterEnabledChangedEventListener(event -> calledEvents.add(event.getClass()));
        ComponentUtil.fireEvent(testObj, new MasterEnabledChangedEvent(testObj, false, true));
        assertIterableEquals(Arrays.asList(MasterEnabledChangedEvent.class), calledEvents);
    }

    @Test
    @DisplayName("should fire edit event with two proper qualifiers and event details on first and second edit")
    void test_should_fire_edit_event_with_two_proper_qualifiers_and_event_details_on_first_and_second_edit() {
        testObj = Mockito.spy(testObj);

        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.EDIT, "bean_edit");
        UIEventHandlerContext modStartedCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext formValChangeCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::edit").when(map).get(eq("bean_edit"));
        doReturn(Optional.of(modStartedCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::edit"), eq(EventQualifier.MODIFICATION_STARTED));
        doReturn(Optional.of(formValChangeCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::edit"), eq(EventQualifier.FORM_FIELD_VALUE_CHANGED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        List<Class<? extends ComponentEvent>> calledEvents = new ArrayList<>();

        testObj.addEditEventListener(event -> {
            calledEvents.add(event.getClass());
            assertTrue(event instanceof EditEvent);

            EditEvent editEvent = (EditEvent) event;
            assertNotNull(editEvent.getPropertyName());

            if (editEvent.getPropertyName().equals("prop")) {
                assertAll(
                    () -> assertEquals("new", editEvent.getNewValue()),
                    () -> assertNull(editEvent.getOldValue())
                );
            } else if (editEvent.getPropertyName().equals("prop2")) {
                assertAll(
                    () -> assertEquals("new2", editEvent.getNewValue()),
                    () -> assertNull(editEvent.getOldValue())
                );
            } else {
                fail("Illegal property name: " + editEvent.getPropertyName());
            }
        });

        Component detailComponent = (Component) testObj.getDetailComponent().get();

        JsonObject eventData = Json.createObject();
        eventData.put("event.detail.propertyName", "prop");
        eventData.put("event.detail.oldValue", Json.createNull());
        eventData.put("event.detail.newValue", "new");
        fireDomEvent(detailComponent, "value-changed", eventData);

        eventData.put("event.detail.propertyName", "prop2");
        eventData.remove("event.detail.oldValue");
        eventData.put("event.detail.newValue", "new2");
        fireDomEvent(detailComponent, "value-changed", eventData);

        assertIterableEquals(Arrays.asList(EditEvent.class, EditEvent.class, EditEvent.class), calledEvents);
        verify(modStartedCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        verify(formValChangeCtx, new Times(2)).callEventHandlerWithItem(any(), any());
    }

    @Test
    @DisplayName("item selection should fire four event with the proper order and qualifiers")
    void test_item_selection_should_fire_four_events_with_the_proper_order_and_qualifiers() {
        testObj = Mockito.spy(testObj);

        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addItemSelectedEventListener(event -> {
            assertAll(
                () -> assertTrue(event instanceof ItemSelectedEvent),
                () -> assertFalse(((ItemSelectedEvent) event).isCreated(), "Created should be false"),
                () -> assertTrue(((ItemSelectedEvent) event).isItemSelected(), "Item selected should be true")
            );
            qualifiers.add(((ItemSelectedEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.ITEM_SELECTED, "bean_itemSelected");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext mdcSetCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext dpSetCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext dpReadyCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::itemSelected").when(map).get(eq("bean_itemSelected"));
        doReturn(Optional.of(reqCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.REQUESTED));
        doReturn(Optional.of(mdcSetCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET));
        doReturn(Optional.of(dpSetCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.DETAIL_ITEM_SET));
        doReturn(Optional.of(dpReadyCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.DETAIL_READY));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        //noinspection unchecked
        ((Grid) testObj.getMasterComponent().get()).getSelectionModel().select("ITEM");

        fireDomEvent(testObj, "selected-item-changed");
        fireDomEvent((Component) testObj.getDetailComponent().get(), "detail-ready");

        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED, EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET,
            EventQualifier.DETAIL_ITEM_SET, EventQualifier.DETAIL_READY), qualifiers);
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        verify(mdcSetCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        verify(dpSetCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        verify(dpReadyCtx, new Times(1)).callEventHandlerWithItem(any(), any());
    }

    @Test
    @DisplayName("create method should fire three item-selected event with the proper order and qualifiers")
    void test_create_method_should_fire_three_item_selected_events_with_the_proper_order_and_qualifiers() {
        testObj = Mockito.spy(testObj);

        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addItemSelectedEventListener(event -> {
            assertAll(
                () -> assertTrue(event instanceof ItemSelectedEvent),
                () -> assertTrue(((ItemSelectedEvent) event).isCreated()),
                () -> assertTrue(((ItemSelectedEvent) event).isItemSelected())
            );
            qualifiers.add(((ItemSelectedEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.CREATE, "bean_itemCreated");
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.ITEM_SELECTED, "bean_itemSelected");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext mdcSetCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext dpSetCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext dpReadyCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::itemSelected").when(map).get(eq("bean_itemSelected"));
        doReturn("bean::itemCreated").when(map).get(eq("bean_itemCreated"));
        doReturn(Optional.of(reqCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemCreated"), eq(EventQualifier.REQUESTED));
        doReturn(Optional.of(mdcSetCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET));
        doReturn(Optional.of(dpSetCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.DETAIL_ITEM_SET));
        doReturn(Optional.of(dpReadyCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.DETAIL_READY));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        testObj.createItem("NEW ITEM");

        fireDomEvent(testObj, "selected-item-changed");
        fireDomEvent((Component) testObj.getDetailComponent().get(), "detail-ready");

        ArgumentCaptor<String> cap = ArgumentCaptor.forClass(String.class);

        verify(mdcSetCtx, new Times(1)).callEventHandlerWithItem(cap.capture(), any());
        verify(dpSetCtx, new Times(1)).callEventHandlerWithItem(cap.capture(), any());
        verify(dpReadyCtx, new Times(1)).callEventHandlerWithItem(cap.capture(), any());

        assertIterableEquals(
            Arrays.asList(EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET, EventQualifier.DETAIL_ITEM_SET, EventQualifier.DETAIL_READY), qualifiers);

        List<String> allValues = cap.getAllValues();
        assertIterableEquals(Arrays.asList(Collections.singleton("NEW ITEM"), Collections.singleton("NEW ITEM"), Collections.singleton("NEW ITEM")), allValues);
    }

    @Test
    @DisplayName("should fire componenet event and call event handler method on event edit")
    void test_should_fire_componenet_event_and_call_event_handler_method_on_event_edit() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addEditEventListener(event -> {
            assertTrue(event instanceof EditEvent, "Event should be EditEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.EDIT, "bean_edit");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::edit").when(map).get(eq("bean_edit"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::edit"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent(testObj, "edit");

        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED), qualifiers);
    }

    @Test
    @DisplayName("should fire componenet event and call event handler method on event refresh")
    void test_should_fire_componenet_event_and_call_event_handler_method_on_event_refresh() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addRefreshEventListener(event -> {
            assertTrue(event instanceof RefreshEvent, "Event should be EditEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.REFRESH, "bean_refresh");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::refresh").when(map).get(eq("bean_refresh"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::refresh"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent(testObj, "refresh");

        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED), qualifiers);
    }

    @Test
    @DisplayName("should fire componenet event and call event handler method on event delete")
    void test_should_fire_componenet_event_and_call_event_handler_method_on_event_delete() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        testObj.setSelectedItems(Arrays.asList("SEL1", "SEL2"));

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addDeleteEventListener(event -> {
            assertTrue(event instanceof DeleteEvent, "Event should be DeleteEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.DELETE, "bean_delete");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::delete").when(map).get(eq("bean_delete"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::delete"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent(testObj, "delete");

        ArgumentCaptor<Collection> captor = ArgumentCaptor.forClass(Collection.class);
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(captor.capture(), any());
        Collection collection = captor.getValue();

        assertAll(
            () -> assertNotNull(collection),
            () -> assertTrue(collection.contains("SEL1") && collection.contains("SEL2"), "Collection should contain selected items"),
            () -> assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED), qualifiers),
            () -> assertNull(testObj.getSelectedItems()),
            () -> assertNull(testObj.getSelectedItem())
        );
    }

    @Test
    @DisplayName("should fire componenet event and call event handler method on event save")
    void test_should_fire_componenet_event_and_call_event_handler_method_on_event_save() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        testObj.setSelectedItem("ITEM TO SAVE");

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addSaveEventListener(event -> {
            assertTrue(event instanceof SaveEvent, "Event should be SaveEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.SAVE, "bean_save");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::save").when(map).get(eq("bean_save"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::save"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent(testObj, "save");

        ArgumentCaptor<Collection> captor = ArgumentCaptor.forClass(Collection.class);
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(captor.capture(), any());
        Collection collection = captor.getValue();

        assertNotNull(collection);
        assertEquals(1, collection.size(), "Collection should be a singleton");
        assertTrue(collection.contains("ITEM TO SAVE"), "Collection should contain selected item");
        assertIterableEquals(Collections.singletonList(EventQualifier.REQUESTED), qualifiers);
        Collection selectedItems = testObj.getMasterConnector().getSelectedItems();
        assertNotNull(selectedItems);
        assertFalse(selectedItems.isEmpty());
        assertEquals(1, selectedItems.size());
        assertEquals("ITEM TO SAVE", selectedItems.iterator().next());
    }

    @Test
    @DisplayName("should sync state node when save even handler method is called")
    void test_should_sync_state_node_when_save_even_handler_method_is_called() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        testObj.setSelectedItem("ITEM TO SAVE");

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addSaveEventListener(event -> {
            assertTrue(event instanceof SaveEvent, "Event should be SaveEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.SAVE, "bean_save");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::save").when(map).get(eq("bean_save"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::save"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        Component domBindComponent = mock(Component.class);
        Page pageComponent = mock(Page.class);
        StateNodeManager stateNodeManager = mock(StateNodeManager.class);

        doReturn(Optional.of(domBindComponent)).when(testObj).getParent();
        doReturn(Optional.of(pageComponent)).when(domBindComponent).getParent();
        doReturn(stateNodeManager).when(pageComponent).getStateNodeManager();

        fireDomEvent(testObj, "save");

        ArgumentCaptor<Collection> captor = ArgumentCaptor.forClass(Collection.class);
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(captor.capture(), any());
        Collection collection = captor.getValue();

        assertNotNull(collection);
        assertEquals(1, collection.size(), "Collection should be a singleton");
        assertTrue(collection.contains("ITEM TO SAVE"), "Collection should contain selected item");
        assertIterableEquals(Collections.singletonList(EventQualifier.REQUESTED), qualifiers);

        verify(stateNodeManager).synchronizeStateNodes();
        verify(stateNodeManager).synchronizeProperties();
    }

    @Test
    @DisplayName("should fire componenet event and call event handler method on event reset")
    void test_should_fire_componenet_event_and_call_event_handler_method_on_event_reset() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addResetEventListener(event -> {
            assertTrue(event instanceof ResetEvent, "Event should be ResetEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.RESET, "bean_reset");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::reset").when(map).get(eq("bean_reset"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::reset"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent((Component) testObj.getDetailComponent().get(), "reset");
        fireDomEvent(testObj, "reset");

        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED), qualifiers);
    }

    @Test
    @DisplayName("should fire component event and call event handler method on event cancel")
    void test_should_fire_componenet_event_and_call_event_handler_method_on_event_cancel() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addCancelEventListener(event -> {
            assertTrue(event instanceof CancelEvent, "Event should be CancelEvent");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.CANCEL, "bean_cancel");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::cancel").when(map).get(eq("bean_cancel"));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::cancel"), eq(EventQualifier.REQUESTED));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent(testObj, "cancel");

        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED), qualifiers);
    }

    @Test
    @DisplayName("should throw exception if event handler method name not found")
    void test_should_throw_exception_if_event_handler_method_name_not_found() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.EDIT, "bean_notfound");
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn(null).when(map).get(eq("bean_notfound"));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        assertThrows(ComponentInternalException.class, () -> fireDomEvent(testObj, "edit"), "Should throw exception");
    }

    @Test
    @DisplayName("should call frontend error handler if event handler method throws progress interrupt exception")
    void test_should_call_fronend_error_handler_if_event_handler_method_throws_progress_interrupt_exception() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addItemSelectedEventListener(event -> {
            qualifiers.add(((ItemSelectedEvent) event).getEventQualifiers().iterator().next());
        });

        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.ITEM_SELECTED, "bean_itemSel");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext mdcCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::itemSel").when(map).get(eq("bean_itemSel"));
        doReturn(Optional.of(reqCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSel"), eq(EventQualifier.REQUESTED));
        doReturn(Optional.of(mdcCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSel"), eq(EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        ProgressInterruptException pie = new ProgressInterruptException("Test exception");
        doThrow(pie).when(reqCtx).callEventHandlerWithItem(any(), any());

        //noinspection unchecked
        ((Grid) testObj.getMasterComponent().get()).getSelectionModel().select("ITEM");

        assertTrue(qualifiers.isEmpty(), "Component event listeners should not be called");
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        verify(mdcCtx, new Times(0)).callEventHandlerWithItem(any(), any());
    }

    @Test
    @DisplayName("should call frontend error handler if component event listener throws progress interrupt exception")
    void test_should_call_fronend_error_handler_if_component_event_listener_throws_progress_interrupt_exception() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addItemSelectedEventListener(event -> {
            if (((ItemSelectedEvent) event).isEventQualifiersPresent(EventQualifier.REQUESTED)) {
                throw new ProgressInterruptException("Test ex");
            }
            qualifiers.add(((ItemSelectedEvent) event).getEventQualifiers().iterator().next());
        });

        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.ITEM_SELECTED, "bean_itemSel");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext mdcCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::itemSel").when(map).get(eq("bean_itemSel"));
        doReturn(Optional.of(reqCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSel"), eq(EventQualifier.REQUESTED));
        doReturn(Optional.of(mdcCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSel"), eq(EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        //noinspection unchecked
        ((Grid) testObj.getMasterComponent().get()).getSelectionModel().select("ITEM");

        assertTrue(qualifiers.isEmpty(), "Component event listeners should not be called after exception");
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        verify(mdcCtx, new Times(0)).callEventHandlerWithItem(any(), any());
    }

    @Test
    @DisplayName("event handler method should be found by default qualifier")
    void test_event_handler_method_should_be_found_by_default_qualifier() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addRefreshEventListener(event -> {
            assertTrue(event instanceof RefreshEvent, "Event should be Refresh");
            qualifiers.add(((QualifierAwareComponentEvent) event).getEventQualifiers().iterator().next());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.REFRESH, "bean_refresh");
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::refresh").when(map).get(eq("bean_refresh"));
        doReturn(Optional.empty()).when(cbm).getEventHandlerContext(eq("bean::refresh"), eq(EventQualifier.REQUESTED));
        doReturn(Optional.of(reqCtx)).when(cbm).getEventHandlerContext(eq("bean::refresh"));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        fireDomEvent(testObj, "refresh");

        verify(reqCtx, new Times(1)).callEventHandlerWithItem(any(), any());
        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED), qualifiers);
    }

    @Test
    @DisplayName("should create item and call one create and three selected item changed event with the right qualifiers")
    void test_should_create_item_and_call_one_create_and_three_selected_item_changed_events_with_the_right_qualifiers() {
        testObj = Mockito.spy(testObj);

        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        AtomicBoolean createEventCalled = new AtomicBoolean(false);
        List<EventQualifier> qualifiers = new ArrayList<>();
        testObj.addCreateEventListener(event -> {
            assertAll(
                () -> assertTrue(event instanceof CreateEvent)
            );
            if (createEventCalled.get()) {
                fail("Create event should be called only once");
            } else {
                createEventCalled.set(true);
                qualifiers.addAll(((CreateEvent) event).getEventQualifiers());
            }
        });
        testObj.addItemSelectedEventListener(event -> {
            assertAll(
                () -> assertTrue(event instanceof ItemSelectedEvent),
                () -> assertTrue(((ItemSelectedEvent) event).isCreated(), "Created should be true"),
                () -> assertTrue(((ItemSelectedEvent) event).isItemSelected(), "Item selected should be true")
            );
            qualifiers.addAll(((ItemSelectedEvent) event).getEventQualifiers());
        });
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.CREATE, "bean_create");
        testObj.registerEventHandlerMethodName(UIBuilderMasterDetailControllerEventHandlerMethod.ITEM_SELECTED, "bean_itemSelected");
        UIEventHandlerContext reqCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext mdcSetCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext dpSetCtx = mock(UIEventHandlerContext.class);
        UIEventHandlerContext dpReadyCtx = mock(UIEventHandlerContext.class);
        ControllerBeanManager cbm = mock(ControllerBeanManager.class);
        Map map = mock(Map.class);
        doReturn(map).when(cbm).getEvenHandlerNormalizedNameToQualifiedNameMap();
        doReturn("bean::create").when(map).get(eq("bean_create"));
        doReturn("bean::itemSelected").when(map).get(eq("bean_itemSelected"));
        doReturn(Optional.of(reqCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::create"), eq(EventQualifier.REQUESTED));
        doReturn(Optional.of(mdcSetCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET));
        doReturn(Optional.of(dpSetCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.DETAIL_ITEM_SET));
        doReturn(Optional.of(dpReadyCtx)).when(cbm)
            .getEventHandlerContext(eq("bean::itemSelected"), eq(EventQualifier.DETAIL_READY));
        doReturn(cbm).when(testObj).getControllerBeanManager();

        testObj.setItemSupplier((SerializableSupplier) () -> "SUPPL");

        fireDomEvent(testObj, "create");
        fireDomEvent(testObj, "selected-item-changed");
        fireDomEvent((Component) testObj.getDetailComponent().get(), "detail-ready");

        Collection items = Collections.singleton("SUPPL");
        assertIterableEquals(Arrays.asList(EventQualifier.REQUESTED, EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET,
            EventQualifier.DETAIL_ITEM_SET, EventQualifier.DETAIL_READY), qualifiers);
        verify(reqCtx, new Times(1)).callEventHandlerWithItem(isNull(), any());
        verify(testObj, new Times(1)).getItemSupplier();
        verify(mdcSetCtx, new Times(1)).callEventHandlerWithItem(eq(items), any());
        verify(dpSetCtx, new Times(1)).callEventHandlerWithItem(eq(items), any());
        verify(dpReadyCtx, new Times(1)).callEventHandlerWithItem(eq(items), any());
    }

    @Test
    @DisplayName("should call refresh on master connector")
    void test_should_call_refresh_on_master_connector() {
        testObj = Mockito.spy(testObj);
        UIBuilderMasterConnector connector = mock(UIBuilderMasterConnector.class);
        doReturn(connector).when(testObj).getMasterConnector();

        testObj.refresh();

        verify(connector).refresh();
    }

    @Test
    @DisplayName("should select higher priority master connector")
    void test_should_select_higher_priority_master_connector() {
        MockSettingsImpl<Grid> componentMockSettings = new MockSettingsImpl<>();
        componentMockSettings.extraInterfaces(HasValue.class);
        Grid gridMock = mock(Grid.class, componentMockSettings);

        assertTrue(gridMock instanceof HasValue);

        HtmlElementAwareComponent mockHtmlElementAwareComponent = mock(HtmlElementAwareComponent.class);
        doReturn(gridMock).when(mockHtmlElementAwareComponent).getComponent();

        Set<Class<? extends UIBuilderMasterConnector>> connectorClasses = new LinkedHashSet<>();
        connectorClasses.add(HasValueMasterConnector.class);
        connectorClasses.add(GridMasterConnector.class);
        doReturn(connectorClasses).when(memberScanner).findClassesBySuperType(UIBuilderMasterConnector.class);

        testObj.setMaster(mockHtmlElementAwareComponent);

        UIBuilderMasterConnector masterConnector = testObj.findMasterConnector();

        assertNotNull(masterConnector);
        assertTrue(masterConnector instanceof GridMasterConnector);
    }

    @Test
    @DisplayName("should return false when master connector is not directly modifiable when adding a new item")
    void test_should_return_false_when_master_connector_is_not_directly_modifiable_when_adding_a_new_item() {
        UIBuilderMasterConnector masterConnector = mock(UIBuilderMasterConnector.class);
        testObj = spy(testObj);
        doReturn(masterConnector).when(testObj).getMasterConnector();
        doReturn(false).when(masterConnector).isDirectModifiable();

        Object newObject = new Object();
        boolean result = testObj.addToMasterList(newObject);

        assertFalse(result);
        verify(masterConnector, never()).addItem(any());
        verify(masterConnector, never()).refresh();
    }

    @Test
    @DisplayName("should return true when master conector is directly modifiable when addign a new item")
    void test_should_return_true_when_master_conector_is_directly_modifiable_when_addign_a_new_item() {
        UIBuilderMasterConnector masterConnector = mock(UIBuilderMasterConnector.class);
        testObj = spy(testObj);
        doReturn(masterConnector).when(testObj).getMasterConnector();
        doReturn(true).when(masterConnector).isDirectModifiable();

        Object newObject = new Object();
        boolean result = testObj.addToMasterList(newObject);

        assertTrue(result);
        verify(masterConnector).addItem(same(newObject));
        verify(masterConnector).refresh();
    }

    @Test
    @DisplayName("should find parent MDC")
    void test_should_find_parent_mdc(@LoadElement("/nestedMdc.html") Element page) {
        UIBuilderMasterDetailController<?> outerMdc = new UIBuilderMasterDetailController<>();
        outerMdc.setMasterId("outerGrid");
        outerMdc.setDetailId("outerDp");

        testObj = new UIBuilderMasterDetailController<>();
        testObj.setMasterId("innerGrid");
        testObj.setDetailId("innerDp");
        testObj.setParentMdcId("outerMdc");


        ElementCollector.register(outerMdc, page.getElementById("outerMdc"));
        ElementCollector.register(new Grid<>(), page.getElementById("outerGrid"));
        ElementCollector.register(new MockDetailPanel(), page.getElementById("outerDp"));
        ElementCollector.register(testObj, page.getElementById("innerMdc"));
        ElementCollector.register(new Grid<>(), page.getElementById("innerGrid"));
        ElementCollector.register(new MockDetailPanel(), page.getElementById("innerDp"));

        testObj.onAttached();

        @SuppressWarnings("unchecked")
        Optional<UIBuilderMasterDetailController<?>> foundParentMdc = testObj.findParentMdc();

        assertNotNull(foundParentMdc);
        assertTrue(foundParentMdc.isPresent());
        UIBuilderMasterDetailController<?> parentMdc = foundParentMdc.get();
        assertSame(outerMdc, parentMdc);
    }

    @Test
    @DisplayName("should throw MasterDetailControllerInterruptException on handle-error event")
    void test_should_throw_master_detail_controller_interrupt_exception_on_handle_error_event() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        JsonObject eventData = Json.createObject();
        eventData.put("event.detail.errorMessage", "Test error message");

        MasterDetailControllerInterruptException exception = assertThrows(
            MasterDetailControllerInterruptException.class,
            () -> fireDomEvent(testObj, "handle-error", eventData));

        assertNotNull(exception);
        assertEquals("Test error message", exception.getMessage());
    }

    @Test
    @DisplayName("should throw MasterDetailControllerInterruptException on handle-error event without message")
    void test_should_throw_master_detail_controller_interrupt_exception_on_handle_error_event_without_message() {
        testObj = Mockito.spy(testObj);
        testObj.setMasterId("grid");
        testObj.setDetailId("dp");
        testObj.onAttached();

        MasterDetailControllerInterruptException exception = assertThrows(
            MasterDetailControllerInterruptException.class,
            () -> fireDomEvent(testObj, "handle-error"));

        assertNotNull(exception);
        assertNull(exception.getMessage());
    }


    private void fireDomEvent(Component component, String eventType) {
        fireDomEvent(component, eventType, Json.createObject());
    }

    private void fireDomEvent(Component component, String eventType, JsonObject eventData) {
        com.vaadin.flow.dom.Element element = component.getElement();
        element.getNode().getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(element, eventType, eventData));
    }

    @Tag("detail-panel")
    public static class MockDetailPanel extends HasRawElementComponent {

        private Serializable item;

        public Serializable getItem() {
            return item;
        }

        public void setItem(Serializable item) {
            this.item = item;
        }

        public Serializable getPrivItem() {
            return null;
        }

        private void setPrivItem(Serializable item) throws IllegalAccessException {
            throw new IllegalAccessException("Mock");
        }
    }

}
