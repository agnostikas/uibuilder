/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.masterdetail.connector;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.binder.HasDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.api.components.masterconnector.UIBuilderMasterConnector;
import io.devbench.uibuilder.api.exceptions.MasterConnectorNotDirectlyModifiableException;
import io.devbench.uibuilder.components.masterdetail.exception.MasterConnectorInternalException;
import io.devbench.uibuilder.components.masterdetail.exception.MasterConnectorNotHasEnabledComponentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class HasValueMasterConnectorTest {

    private UIBuilderMasterConnector<?, ?> testObj;

    @SuppressWarnings("unchecked")
    private HasValueMasterConnector<ComboBox<String>, String> getStringComboBoxMasterConnector() {
        return (HasValueMasterConnector<ComboBox<String>, String>) testObj;
    }

    @SuppressWarnings("unchecked")
    private <T> Optional<DataProvider<T, ?>> callGetDataProvider(HasValueMasterConnector hasValueMasterConnector) {
        try {
            Method getDataProvider = HasValueMasterConnector.class.getDeclaredMethod("getDataProvider");
            getDataProvider.setAccessible(true);
            return (Optional<DataProvider<T, ?>>) getDataProvider.invoke(hasValueMasterConnector);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            fail("Could not found getDataProvider method in HasValueMasterConnector");
            return Optional.empty();
        } catch (InvocationTargetException e) {
            throw (RuntimeException) e.getTargetException();
        }
    }

    @BeforeEach
    void setUp() {
        testObj = new HasValueMasterConnector<>();
    }

    @Test
    @DisplayName("should be applicable for Grid")
    void test_should_be_applicable_for_grid() {
        assertFalse(testObj.isApplicable(NoValueComponent.class));
        assertTrue(testObj.isApplicable(ComboBox.class));
    }

    @Test
    @DisplayName("should create connection with selection change listener")
    void test_should_create_connection_with_selection_change_listener() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setItems("Selected", "Another", "Ignored");
        comboBox.setValue(null);

        connector.connect(comboBox);
        AtomicInteger callCounter = new AtomicInteger(0);

        connector.addSelectionChangedListener(event -> {
            int currentRun = callCounter.incrementAndGet();
            assertFalse(event.isFromClient());
            if (currentRun == 1) {
                assertIterableEquals(Collections.singleton(null), event.getPreviouslySelectedItems());
                assertIterableEquals(Collections.singleton("Selected"), event.getNewlySelectedItems());
            } else if (currentRun == 2) {
                assertIterableEquals(Collections.singleton("Selected"), event.getPreviouslySelectedItems());
                assertIterableEquals(Collections.singleton("Another"), event.getNewlySelectedItems());
            } else {
                fail("Should not be called more that two times");
            }
        });

        comboBox.setValue("Selected");

        assertEquals(1, callCounter.get());

        comboBox.setValue("Another");

        assertEquals(2, callCounter.get());

        connector.disconnect();
        comboBox.setValue("Ignored");

        assertEquals(2, callCounter.get());
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @DisplayName("should get master component or throw npe if not specified")
    void test_should_get_master_component_or_throw_npe_if_not_specified() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();

        assertThrows(NullPointerException.class, connector::getMasterComponent, "Should throw NPE");

        ComboBox<String> comboBox = new ComboBox<>();
        connector.connect(comboBox);

        assertSame(comboBox, connector.getMasterComponent());
    }

    @Test
    @DisplayName("should set selected items")
    void test_should_set_selected_items() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>("test", "One", "Two");

        connector.connect(comboBox);
        assertIterableEquals(Collections.singleton(null), connector.getSelectedItems());

        String message = assertThrows(NullPointerException.class, () -> connector.setSelectedItems(null),
            "Should throw NPE when callig set selected items with null").getMessage();
        assertEquals("Selected items collection cannot be null", message);

        connector.setSelectedItems(Collections.singleton("One"));
        assertIterableEquals(Collections.singleton("One"), connector.getSelectedItems());

        connector.setSelectedItems(Collections.singleton("Two"));
        assertIterableEquals(Collections.singleton("Two"), connector.getSelectedItems());
    }

    @Test
    @DisplayName("should call refresh all and refresh item on data provider")
    void test_should_call_refresh_all_and_refresh_item_on_data_provider() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>("test", "One", "Two");

        @SuppressWarnings("unchecked")
        DataProvider<String, String> dataProvider = mock(DataProvider.class);

        comboBox.setDataProvider(dataProvider);
        connector.connect(comboBox);

        connector.refresh();
        connector.refresh("One");

        verify(dataProvider).refreshAll();
        verify(dataProvider).refreshItem(eq("One"));
    }

    @Test
    @DisplayName("should set end get enabled be called on supported component")
    void test_should_set_end_get_enabled_be_called_on_supported_component() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();
        connector.connect(comboBox);

        assertTrue(connector.isEnabled(), "Enabled should be true");

        connector.setEnabled(false);

        assertFalse(connector.isEnabled(), "Enabled should be false");

        connector.setEnabled(true);

        assertTrue(connector.isEnabled(), "Enabled should be true again");
    }

    @Test
    @DisplayName("should set and get enabled throw exception on not supported component")
    void test_should_set_and_get_enabled_throw_exception_on_not_supported_component() {
        @SuppressWarnings("unchecked")
        HasValueMasterConnector<NoEnableSupportValueComponent, String> connector = (HasValueMasterConnector<NoEnableSupportValueComponent, String>) testObj;
        NoEnableSupportValueComponent component = new NoEnableSupportValueComponent();
        connector.connect(component);

        assertThrows(MasterConnectorNotHasEnabledComponentException.class, () -> connector.setEnabled(true));
        assertThrows(MasterConnectorNotHasEnabledComponentException.class, () -> connector.setEnabled(false));
        assertThrows(MasterConnectorNotHasEnabledComponentException.class, connector::isEnabled);
    }

    @Test
    @DisplayName("should return direct modifiable false if data provider is not list data provider")
    void test_should_return_direct_modifiable_false_if_data_provider_is_not_list_data_provider() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();

        @SuppressWarnings("unchecked")
        DataProvider<String, String> notAListDataProvider = mock(DataProvider.class);
        comboBox.setDataProvider(notAListDataProvider);

        connector.connect(comboBox);
        assertFalse(connector.isDirectModifiable());
    }

    @Test
    @DisplayName("should return direct modifiable true if data provider is list data provider")
    void test_should_return_direct_modifiable_true_if_data_provider_is_list_data_provider() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();

        @SuppressWarnings("unchecked")
        DataProvider<String, String> dataProvider = mock(ListDataProvider.class);

        comboBox.setDataProvider(dataProvider);

        connector.connect(comboBox);
        assertTrue(connector.isDirectModifiable());
    }

    @Test
    @DisplayName("should throw exception when trying to add item with non direct modifiable data provider")
    void test_should_throw_exception_when_trying_to_add_item_with_non_direct_modifiable_data_provider() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();

        @SuppressWarnings("unchecked")
        DataProvider<String, String> notAListDataProvider = mock(DataProvider.class);
        comboBox.setDataProvider(notAListDataProvider);

        connector.connect(comboBox);

        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> connector.addItem("New Item"));
    }

    @Test
    @DisplayName("should add new item if direct modifiable")
    void test_should_add_new_item_if_direct_modifiable() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setItems(new ArrayList<>(Collections.singletonList("Initial")));

        assertTrue(comboBox.getDataProvider() instanceof ListDataProvider);
        @SuppressWarnings("unchecked")
        ListDataProvider<String> listDataProvider = (ListDataProvider<String>) comboBox.getDataProvider();

        connector.connect(comboBox);
        connector.addItems(Arrays.asList("Item 1", "Item 2"));

        assertIterableEquals(Arrays.asList("Initial", "Item 1", "Item 2"), listDataProvider.getItems());
    }

    @Test
    @DisplayName("should remove item if direct modifiable")
    void test_should_remove_item_if_direct_modifiable() {
        HasValueMasterConnector<ComboBox<String>, String> connector = getStringComboBoxMasterConnector();
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setItems(new ArrayList<>(Arrays.asList("Item 1", "Item 2", "Item 3")));

        assertTrue(comboBox.getDataProvider() instanceof ListDataProvider);
        @SuppressWarnings("unchecked")
        ListDataProvider<String> listDataProvider = (ListDataProvider<String>) comboBox.getDataProvider();

        connector.connect(comboBox);
        connector.removeItem("Item 1");

        assertIterableEquals(Arrays.asList("Item 2", "Item 3"), listDataProvider.getItems());
    }

    @Test
    @DisplayName("should throw exception if cannot retrieve data provider from a hasDataProvider class")
    void test_should_throw_exception_if_cannot_retrieve_data_provider_from_a_has_data_provider_class() {
        @SuppressWarnings("unchecked")
        HasValueMasterConnector<InvalidDataProviderComponent, String> connector1 = (HasValueMasterConnector<InvalidDataProviderComponent, String>) testObj;
        connector1.connect(new InvalidDataProviderComponent());

        assertThrows(MasterConnectorInternalException.class, () -> callGetDataProvider(connector1));

        @SuppressWarnings("unchecked")
        HasValueMasterConnector<PrivateDataProviderComponent, String> connector2 = (HasValueMasterConnector<PrivateDataProviderComponent, String>) testObj;
        connector2.connect(new PrivateDataProviderComponent());

        assertThrows(MasterConnectorInternalException.class, () -> callGetDataProvider(connector2));
    }

    @Test
    @DisplayName("should return empty optional if the component is not a hasDataProvider")
    void test_should_return_empty_optional_if_the_component_is_not_a_has_data_provider() {
        @SuppressWarnings("unchecked")
        HasValueMasterConnector<NoEnableSupportValueComponent, String> connector = (HasValueMasterConnector<NoEnableSupportValueComponent, String>) testObj;
        connector.connect(new NoEnableSupportValueComponent());

        Optional<DataProvider<Object, ?>> dataProvider = callGetDataProvider(connector);

        assertNotNull(dataProvider);
        assertFalse(dataProvider.isPresent(), "DataProvider instance should not be present");
    }

    private static class NoValueComponent extends Component {
    }

    @Tag("no-enable-support")
    public static class NoEnableSupportValueComponent extends Component
        implements HasValue<AbstractField.ComponentValueChangeEvent<NoEnableSupportValueComponent, String>, String> {

        @Override
        public void setValue(String value) {

        }

        @Override
        public String getValue() {
            return null;
        }

        @Override
        public Registration addValueChangeListener(
            ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<NoEnableSupportValueComponent, String>> listener) {
            return null;
        }

        @Override
        public void setReadOnly(boolean readOnly) {

        }

        @Override
        public boolean isReadOnly() {
            return false;
        }

        @Override
        public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {

        }

        @Override
        public boolean isRequiredIndicatorVisible() {
            return false;
        }
    }

    @Tag("invalid-data-provider")
    private static class InvalidDataProviderComponent extends Component
        implements HasValue<AbstractField.ComponentValueChangeEvent<InvalidDataProviderComponent, String>, String>, HasDataProvider<String> {
        @Override
        public void setValue(String value) {

        }

        @Override
        public String getValue() {
            return null;
        }

        @Override
        public Registration addValueChangeListener(
            ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<InvalidDataProviderComponent, String>> listener) {
            return null;
        }

        @Override
        public void setReadOnly(boolean readOnly) {

        }

        @Override
        public boolean isReadOnly() {
            return false;
        }

        @Override
        public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {

        }

        @Override
        public boolean isRequiredIndicatorVisible() {
            return false;
        }

        @Override
        public void setDataProvider(DataProvider<String, ?> dataProvider) {

        }
    }

    @Tag("private-data-provider")
    private static class PrivateDataProviderComponent extends Component
        implements HasValue<AbstractField.ComponentValueChangeEvent<PrivateDataProviderComponent, String>, String>, HasDataProvider<String> {
        @Override
        public void setValue(String value) {

        }

        @Override
        public String getValue() {
            return null;
        }

        @Override
        public Registration addValueChangeListener(
            ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<PrivateDataProviderComponent, String>> listener) {
            return null;
        }

        @Override
        public void setReadOnly(boolean readOnly) {

        }

        @Override
        public boolean isReadOnly() {
            return false;
        }

        @Override
        public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {

        }

        @Override
        public boolean isRequiredIndicatorVisible() {
            return false;
        }

        @Override
        public void setDataProvider(DataProvider<String, ?> dataProvider) {

        }

        @SuppressWarnings("unused")
        private DataProvider<String, ?> getDataProvider() {
            return null;
        }
    }

}
