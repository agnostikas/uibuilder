/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.crudtoolbar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.core.utils.ElementCollector;

@Tag(CrudToolbar.TAG_NAME)
@JsModule("./uibuilder-crud-toolbar/src/uibuilder-crud-toolbar.js")
public class CrudToolbar extends Component implements HasComponents {
    static final String TAG_NAME = "crud-toolbar";

    private String masterDetailControllerId;

    public void setEditEnabled(boolean enabled) {
        getElement().callJsFunction("setEditEnabled", enabled);
    }

    public void setDeleteEnabled(boolean enabled) {
        getElement().callJsFunction("setDeleteEnabled", enabled);
    }

    private UIBuilderMasterDetailController getMasterDetailController() {
        return ElementCollector.getById(masterDetailControllerId)
            .map(elementAware -> (UIBuilderMasterDetailController) elementAware.getComponent())
            .orElse(null);
    }

    public String getMasterDetailControllerId() {
        return masterDetailControllerId;
    }

    public void setMasterDetailControllerId(String masterDetailControllerId) {
        this.masterDetailControllerId = masterDetailControllerId;
    }
}
