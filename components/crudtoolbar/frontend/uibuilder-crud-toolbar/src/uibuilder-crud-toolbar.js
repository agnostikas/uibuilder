/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import * as RenderStatus from '@polymer/polymer/lib/utils/render-status.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';


export class CrudToolbar extends ThemableMixin(Uibuilder.I18NBaseMixin('crud-toolbar', PolymerElement)) {

    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                }
            </style>

            <div>
                <slot name="prefix"></slot>

                <template is="dom-if" if="[[!refreshHidden]]">
                    <template is="dom-if" if="[[!hideDefaultRefresh]]">
                        <vaadin-button id="refreshButton" on-click="_onRefresh" theme$="[[_resolveTheme(refreshTheme)]]"
                                       part="refreshButton"
                                       disabled="[[_or(disabled, _disabled, refreshDisabled)]]">
                            <template is="dom-if" if="[[!iconsOnly]]">
                                <i class="icon" slot="prefix"></i>
                                <span>Refresh</span>
                            </template>
                            <template is="dom-if" if="[[iconsOnly]]">
                                <i class="icon"></i>
                            </template>
                        </vaadin-button>
                    </template>
                    <template is="dom-if" if="[[_or(hideAllDefaultButtons, hideDefaultRefresh)]]">
                        <slot name="refresh"></slot>
                    </template>
                </template>
                <template is="dom-if" if="[[!createHidden]]">
                    <template is="dom-if" if="[[!hideDefaultCreate]]">
                        <vaadin-button id="createButton" on-click="_onCreate" theme$="[[_resolveTheme(createTheme)]]"
                                       part="createButton"
                                       disabled="[[_or(disabled, _disabled, createDisabled)]]">
                            <template is="dom-if" if="[[!iconsOnly]]">
                                <i class="icon" slot="prefix"></i>
                                <span>Create</span>
                            </template>
                            <template is="dom-if" if="[[iconsOnly]]">
                                <i class="icon"></i>
                            </template>
                        </vaadin-button>
                    </template>
                    <template is="dom-if" if="[[_or(hideAllDefaultButtons, hideDefaultCreate)]]">
                        <slot name="create"></slot>
                    </template>
                </template>
                <template is="dom-if" if="[[!editHidden]]">
                    <template is="dom-if" if="[[!hideDefaultEdit]]">
                        <vaadin-button id="editButton" on-click="_onEdit" theme$="[[_resolveTheme(editTheme)]]"
                                       part="editButton"
                                       disabled="[[_or(disabled, _disabled, _editDisabled, editDisabled)]]">
                            <template is="dom-if" if="[[!iconsOnly]]">
                                <i class="icon" slot="prefix"></i>
                                <span>Edit</span>
                            </template>
                            <template is="dom-if" if="[[iconsOnly]]">
                                <i class="icon"></i>
                            </template>
                        </vaadin-button>
                    </template>
                    <template is="dom-if" if="[[_or(hideAllDefaultButtons, hideDefaultEdit)]]">
                        <slot name="edit"></slot>
                    </template>
                </template>
                <template is="dom-if" if="[[!deleteHidden]]">
                    <template is="dom-if" if="[[!hideDefaultDelete]]">
                        <vaadin-button id="deleteButton" on-click="_onDelete" theme$="[[_resolveTheme(deleteTheme)]]"
                                       part="deleteButton"
                                       disabled="[[_or(disabled, _disabled, _deleteDisabled, deleteDisabled)]]">
                            <template is="dom-if" if="[[!iconsOnly]]">
                                <i class="icon" slot="prefix"></i>
                                <span>Delete</span>
                            </template>
                            <template is="dom-if" if="[[iconsOnly]]">
                                <i class="icon"></i>
                            </template>
                        </vaadin-button>
                    </template>
                    <template is="dom-if" if="[[_or(hideAllDefaultButtons, hideDefaultDelete)]]">
                        <slot name="delete"></slot>
                    </template>
                </template>

                <slot name="suffix"></slot>
            </div>
        `;
    }

    static get is() {
        return 'crud-toolbar'
    }

    static get properties() {
        return {
            masterDetailController: {
                type: String,
                value: ''
            },
            disabled: {
                type: Boolean,
                value: false
            },
            refreshDisabled: {
                type: Boolean,
                value: false
            },
            refreshHidden: {
                type: Boolean,
                value: false
            },
            createDisabled: {
                type: Boolean,
                value: false
            },
            createHidden: {
                type: Boolean,
                value: false
            },
            editDisabled: {
                type: Boolean,
                value: false
            },
            editHidden: {
                type: Boolean,
                value: false
            },
            deleteDisabled: {
                type: Boolean,
                value: false
            },
            deleteHidden: {
                type: Boolean,
                value: false
            },
            hideDefaultRefresh: {
                type: Boolean,
                value: false
            },
            hideDefaultCreate: {
                type: Boolean,
                value: false
            },
            hideDefaultEdit: {
                type: Boolean,
                value: false
            },
            hideDefaultDelete: {
                type: Boolean,
                value: false
            },
            hideAllDefaultButtons: {
                type: Boolean,
                value: false
            },
            iconsOnly: {
                type: Boolean,
                value: false
            },
            allTheme: {
                type: String,
                value: 'primary'
            },
            refreshTheme: {
                type: String,
                value: ''
            },
            createTheme: {
                type: String,
                value: 'success'
            },
            editTheme: {
                type: String,
                value: 'contrast'
            },
            deleteTheme: {
                type: String,
                value: 'error'
            }
        };
    }

    ready() {
        super.ready();

        if (this.masterDetailController) {
            this._mdc = this.getRootNode().getElementById(this.masterDetailController);

            this._mdc.addEventListener("ready", () => {
                this._mdc.detailComponent.cancelVisible = true;
            });
            this._mdc.addEventListener("selected-item-changed", e => {
                const hasSelectedItem = e.detail.hasSelectedItem;
                this._editDisabled = !hasSelectedItem;
                this._deleteDisabled = !hasSelectedItem;
                if (this._customEditButton) {
                    this._customEditButton.disabled = this._editDisabled;
                }
                if (this._customDeleteButton) {
                    this._customDeleteButton.disabled = this._deleteDisabled;
                }
            });
            this._mdc.addEventListener("master-enabled-changed", e => {
                this._disabled = !e.detail.enabled;
                if (this._customRefreshButton) {
                    this._customRefreshButton.disabled = this._disabled;
                }
                if (this._customCreateButton) {
                    this._customCreateButton.disabled = this._disabled;
                }
                if (this._customEditButton) {
                    this._customEditButton.disabled = this._disabled;
                }
                if (this._customDeleteButton) {
                    this._customDeleteButton.disabled = this._disabled;
                }
            });
            this._editDisabled = true;
            this._deleteDisabled = true;
        }
    }

    connectedCallback() {
        super.connectedCallback();

        RenderStatus.afterNextRender(this, () => {
            this._initButtons();
        });
    }

    _initButtons() {
        this._investigateHiddenProperties();
        if (!this.hideAllDefaultButtons) {
            this.__translate();
        }
        if (this._isAnyDefaultButtonHidden()) {
            this._customizeButtons();
        }
    }

    _customizeButtons() {
        this._findCustomButtons();
        this._setupCustomButtons();
    }

    _findCustomButtons() {
        this.shadowRoot.querySelectorAll('slot').forEach(slotNode => {
            if (this.hideDefaultRefresh && slotNode.name === 'refresh') {
                this._customRefreshButton = slotNode.assignedNodes().find(this._isButtonNode);
            } else if (this.hideDefaultCreate && slotNode.name === 'create') {
                this._customCreateButton = slotNode.assignedNodes().find(this._isButtonNode);
            } else if (this.hideDefaultEdit && slotNode.name === 'edit') {
                this._customEditButton = slotNode.assignedNodes().find(this._isButtonNode);
            } else if (this.hideDefaultDelete && slotNode.name === 'delete') {
                this._customDeleteButton = slotNode.assignedNodes().find(this._isButtonNode);
            }
        });
    }

    _isButtonNode(node) {
        return node.tagName && node.tagName.toLowerCase() === 'vaadin-button' ? node : null;
    }

    _setupCustomButtons() {
        if (this._customRefreshButton) {
            this._customRefreshButton.disabled = this.refreshDisabled || this.disabled;
            this._customRefreshButton.addEventListener('click', () => this._onRefresh());
        }

        if (this._customCreateButton) {
            this._customCreateButton.disabled = this.createDisabled || this.disabled;
            this._customCreateButton.addEventListener('click', () => this._onCreate());
        }

        if (this._customEditButton) {
            this._customEditButton.disabled = this.editDisabled || this.disabled || this._editDisabled;
            this._customEditButton.addEventListener('click', () => this._onEdit());
        }

        if (this._customDeleteButton) {
            this._customDeleteButton.disabled = this.deleteDisabled || this.disabled || this._deleteDisabled;
            this._customDeleteButton.addEventListener('click', () => this._onDelete());
        }
    }

    _investigateHiddenProperties() {
        if (this.hideAllDefaultButtons) {
            this.hideDefaultRefresh = true;
            this.hideDefaultCreate = true;
            this.hideDefaultEdit = true;
            this.hideDefaultDelete = true;
        } else {
            if (this.hideDefaultRefresh && this.hideDefaultCreate && this.hideDefaultEdit && this.hideDefaultDelete) {
                this.hideAllDefaultButtons = true;
            }
        }
    }

    _isAnyDefaultButtonHidden() {
        return this.hideDefaultRefresh || this.hideDefaultCreate || this.hideDefaultEdit || this.hideDefaultDelete;
    }

    __translate() {
        const refreshButtonSpan = this.shadowRoot.querySelector("#refreshButton span");
        if (refreshButtonSpan)
            refreshButtonSpan.textContent = this.tr('Refresh');

        const createButtonSpan = this.shadowRoot.querySelector("#createButton span");
        if (createButtonSpan)
            createButtonSpan.textContent = this.tr('Create');

        const editButtonSpan = this.shadowRoot.querySelector("#editButton span");
        if (editButtonSpan)
            editButtonSpan.textContent = this.tr('Edit');

        const deleteButtonSpan = this.shadowRoot.querySelector("#deleteButton span");
        if (deleteButtonSpan)
            deleteButtonSpan.textContent = this.tr('Delete');
    }

    _onRefresh() {
        this.dispatchEvent(new CustomEvent('refresh'));
        if (this._mdc) {
            this._mdc.refresh();
        }
    }

    _onCreate() {
        this.dispatchEvent(new CustomEvent('create'));
        if (this._mdc) {
            this._mdc.create();
            this._mdc.enableDetail();
        }
    }

    _onEdit() {
        this.dispatchEvent(new CustomEvent('edit'));
        if (this._mdc) {
            this._mdc.edit();
            this._mdc.enableDetail();
        }
    }

    _onDelete() {
        this.dispatchEvent(new CustomEvent('delete'));
        if (this._mdc) {
            this._mdc.delete();
        }
    }

    _or(...args) {
        return args.find(it => it);
    }

    _resolveTheme(theme) {
        return this.allTheme + ' ' + theme;
    }
}

customElements.define(CrudToolbar.is, CrudToolbar);
