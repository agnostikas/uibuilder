## Hideable-layout


A content alatt megjelenik egy gomb, aminek hatására a content eltűnik, és csak a gomb marad látható.  
Főleg filtereknél használt komponens. (A szűrőfeltételek néha a képernyő harmadát foglalták el, 
és a felhasználók inkább a táblázat sorait nézték a szűrőfeltételek helyett, ezért ez volt a szimpatikus megoldás.)

Használata:

- Html

```html
<hideable-layout id="layout">
    <!-- content -->            
</hideable-layout> 
```

- Java

```
    @UIComponent("hideable-layout")
    private HideableLayout hideableLayout;
    

    @UIEventHandler("showLayout")
    public void showLayout() {
        ...
        hideableLayout.setCollapsed({true|false});
        ...
    }

```