/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.hidablelayout;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.shared.Registration;

@Tag("hideable-layout")
@JsModule("./uibuilder-hideable-layout/src/uibuilder-hideable-layout.js")
public class HideableLayout extends Component implements HasComponents, HasElement, HasSize, HasStyle {

    public HideableLayout(Component... components) {
        addComponents(components);
    }

    @Synchronize("collapsed-changed")
    public String getCollapsed() {
        return getElement().getProperty("collapsed");
    }

    public void setCollapsed(Boolean collapsed) {
        getElement().setProperty("collapsed", collapsed);
        this.fireEvent(new ChangeEvent(this, false));
    }

    protected void addComponents(Component... components) {
        for (Component component : components) {
            component.getElement().setAttribute("slot", "layout");
            getElement().appendChild(component.getElement());
        }
    }

    public Registration addChangeListener(
            ComponentEventListener<ChangeEvent> listener) {
        return addListener(ChangeEvent.class, listener);
    }


    @DomEvent("value-changed")
    public class ChangeEvent extends ComponentEvent<HideableLayout> {
        public ChangeEvent(HideableLayout source, boolean fromClient) {
            super(source, fromClient);
        }
    }
}
