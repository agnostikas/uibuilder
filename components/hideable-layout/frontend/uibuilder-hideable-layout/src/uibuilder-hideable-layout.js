/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { GestureEventListeners } from '@polymer/polymer/lib/mixins/gesture-event-listeners.js';
import { FlattenedNodesObserver } from '@polymer/polymer/lib/utils/flattened-nodes-observer.js';
import { IronResizableBehavior } from '@polymer/iron-resizable-behavior/iron-resizable-behavior.js';
import * as PolymerLegacy from '@polymer/polymer/lib/legacy/class.js'
import { ElementMixin } from '@vaadin/vaadin-element-mixin/vaadin-element-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';


export class HideableLayoutElement extends ElementMixin(ThemableMixin(GestureEventListeners(
    PolymerLegacy.mixinBehaviors([ IronResizableBehavior ], PolymerElement)))) {

    static get template() {
        return html`
            <style>
                :host {
                    display: flex;
                    flex-direction: column;
                }

                [part="toggleButton"] {
                    display: block;
                }

                [part="container"] {
                    border: 1px solid lightgrey;
                    flex-grow: 1;
                }
            </style>

            <div id="main" part="container">
                <slot id="layout" name="layout"></slot>
            </div>
            <vaadin-button part="toggleButton" id="actionButton" theme="primary">
                <i class$="icon {{_icon(collapsed)}}"></i>
            </vaadin-button>
        `;
    }

    static get is() {
        return 'hideable-layout';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {
            collapsed: {
                type: Boolean,
                value: false,
                notify: true,
                observer: "_collapsed_observer"
            }
        };
    }

    ready() {
        super.ready();
        new FlattenedNodesObserver(this, this._processChildren);
        this.$.actionButton.addEventListener("click", () => this._buttonClickEvent(), false);
    }

    _icon() {
        return this.collapsed ? "collapsed" : "";
    }

    _buttonClickEvent() {
        if (this.$.main.style.display === "none") {
            this.$.main.style.display = "block";
            this.collapsed = false
        } else {
            this.$.main.style.display = "none";
            this.collapsed = true
        }
        this.dispatchEvent(new CustomEvent('value-changed', { collapsed: this.collapsed }));
    }

    _collapsed_observer() {
        if (this.collapsed) {
            this.$.main.style.display = "none";
        } else {
            this.$.main.style.display = "block";
        }
    }

    _processChildren() {
        this.getEffectiveChildren().forEach((child, i) => {
            this._primaryChild = child;
            child.setAttribute('slot', 'layout');
        });
    }
}

customElements.define(HideableLayoutElement.is, HideableLayoutElement);
