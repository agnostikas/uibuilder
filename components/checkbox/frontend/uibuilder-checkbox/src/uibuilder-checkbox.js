/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

export class UIBuilderCheckboxElement extends ThemableMixin(PolymerElement) {

    static get template() {
        return html`
            <div class="uibuilder-checkbox-container">
                <vaadin-checkbox id="checkbox" checked="{{checked}}" readonly="{{readonly}}" indeterminate="{{indeterminate}}">
                    <slot></slot>
                </vaadin-checkbox>
                <div part="error-message" id="[[_errorId]]" aria-live="assertive" aria-hidden$="[[!invalid]]" hidden$="[[!invalid]]">
                    [[errorMessage]]
                </div>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-checkbox';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {
            value: {
                type: String,
                value: 'false',
                notify: true,
                observer: '_value_changed'
            },
            checked: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_checked_changed'
            },
            unchecked: {
                type: Boolean,
                value: true,
                notify: true,
                observer: '_unchecked_changed'
            },
            indeterminate: {
                type: Boolean,
            },
            readonly: {
                type: Boolean,
                value: false,
            },
            invalid: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_invalid_changed'
            },
            errorMessage: {
                type: String,
                value: null,
                notify: true,
                observer: '_error_message_changed'
            }
        };
    }

    ready() {
        super.ready();
        this._value_changed();
        this.$.checkbox.__interactionsAllowed = this.__interactionsAllowed;
    }

    _invalid_changed() {
        this.$.checkbox.invalid = this.invalid;
    }

    _error_message_changed() {
        this.$.checkbox.errorMessage = this.errorMessage;
    }

    _isChecked() {
        return (this.value === 'true' || this.value === true);
    }

    _isIndeterminate() {
        return (this.value === null);
    }

    _value_changed() {
        this.checked = this._isChecked();
        this.indeterminate = this._isIndeterminate();
    }

    _checked_changed() {
        this.value = this.checked ? 'true' : 'false';
        this.unchecked = !this.checked;
    }

    _unchecked_changed() {
        if (this.unchecked === "") {
            this.unchecked = true;
        }
        this.checked = !this.unchecked;
        this.value = this.checked ? 'true' : 'false';
    }

    __interactionsAllowed(e) {
        if (this.disabled) {
            return false;
        }
        if (this.readonly) {
            e.preventDefault();
            return false;
        }
        // https://github.com/vaadin/vaadin-checkbox/issues/63
        if (e.target.localName === 'a') {
            return false;
        }
        return true;
    }
}

customElements.define(UIBuilderCheckboxElement.is, UIBuilderCheckboxElement);
