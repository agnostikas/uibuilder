/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

const $_documentContainer = html`
<dom-module id="uibuilder-checkbox-style" theme-for="uibuilder-checkbox">
    <template>
        <style>
            .uibuilder-checkbox-container {
                display: flex;
                flex-direction: column;
                min-width: 100%;
                max-width: 100%;
            }

            .uibuilder-checkbox-container [part="error-message"] {
                margin-left: calc(var(--lumo-border-radius-m) / 4);
                font-size: var(--lumo-font-size-xs);
                line-height: var(--lumo-line-height-xs);
                color: var(--lumo-error-text-color);
                will-change: max-height;
                transition: 0.4s max-height;
                max-height: 5em;
            }
        </style>
    </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
