/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.api.parse.BindingHandlerParseInterceptor;
import io.devbench.uibuilder.data.api.datasource.DataSourceDeposit;
import io.devbench.uibuilder.data.api.exceptions.DataSourceFilterOptionException;
import io.devbench.uibuilder.data.common.datasource.BaseDataSourceBindingContext;
import io.devbench.uibuilder.data.common.datasource.DefaultFilterDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.Optional;

public abstract class BaseTagBasedParseInterceptor<T extends BaseDataSourceBindingContext> implements BindingHandlerParseInterceptor<T> {

    public static final String NAME_ATTRIBUTE_NAME = "name";
    public static final String ITEM_LABEL_ATTRIBUTE_NAME = "item-label-path";

    @Override
    public final boolean handleBinding(@NotNull String binding, @NotNull BaseDataSourceBindingContext bindingContext) {
        if (binding.startsWith("item.") || binding.equals("item")) {
            bindingContext.getBindings().add(binding);
            return true;
        }
        return false;
    }

    protected final void parseCommonBindingContextElements(Element dataSourceElement, Element element, T context) {
        context.setDataSourceName(dataSourceElement.attr(NAME_ATTRIBUTE_NAME));
        context.setParsedElement(element);

        if (element.hasAttr(ITEM_LABEL_ATTRIBUTE_NAME)) {
            context.getBindings().add("item." + element.attr(ITEM_LABEL_ATTRIBUTE_NAME));
        }

        parseDefaultFilterOptions(dataSourceElement, context.getDefaultFilterDescriptor());
    }

    private void parseDefaultFilterOptions(Element dataSourceElement, DefaultFilterDescriptor defaultFilterDescriptor) {
        Elements defaultFilterOptions = dataSourceElement.getElementsByTag("default-filter-option");
        if (defaultFilterOptions.size() == 1) {
            Element defaultFilterOption = defaultFilterOptions.get(0);
            String filterType = defaultFilterOption.attr("filter").trim();
            String filterMode = defaultFilterOption.attr("mode").trim();
            if (StringUtils.isNotBlank(filterType)) {
                defaultFilterDescriptor.setDefaultFilterType(filterType, filterMode);
            }
        } else {
            if (defaultFilterOptions.size() > 1) {
                throw new DataSourceFilterOptionException("Multiple datasource default filter option, only one allowed");
            }
        }
        dataSourceElement.getElementsByTag("filter-option")
            .forEach(filterOption -> {
                String path = filterOption.attr("path").trim();
                String filterType = filterOption.attr("filter").trim();
                String filterMode = filterOption.attr("mode").trim();
                defaultFilterDescriptor.addFilterType(path, filterType, filterMode);
            });
    }

    @SuppressWarnings("unchecked")
    protected <CTX extends BaseDataSourceBindingContext> Optional<CTX> tryBindingContextFromDeposit(Element dataSourceElement) {
        if (!dataSourceElement.attr(DataSourceUtils.DATASOURCE_NAME).isEmpty()) {
            BindingContext bindingContext = DataSourceDeposit.getInstance().get(dataSourceElement.attr(DataSourceUtils.DATASOURCE_NAME));
            if (bindingContext instanceof BaseDataSourceBindingContext) {
                return Optional.of((CTX) bindingContext);
            }
        }
        return Optional.empty();
    }
}
