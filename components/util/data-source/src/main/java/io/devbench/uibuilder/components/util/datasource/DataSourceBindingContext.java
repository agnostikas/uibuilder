/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import io.devbench.uibuilder.data.common.datasource.BaseDataSourceBindingContext;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

public final class DataSourceBindingContext extends BaseDataSourceBindingContext {

    @Getter
    @Nullable
    @Setter
    private String defaultQueryName; //don't like this here because this has to be a orm like implementation for data source support

    @Getter
    @Setter
    private BaseNestedBinding nestedBinding;

    @Override
    protected <T extends BaseDataSourceBindingContext> void copy(T into) {
        super.copy(into);
        ((DataSourceBindingContext) into).setDefaultQueryName(defaultQueryName);
        ((DataSourceBindingContext) into).setNestedBinding(nestedBinding);
    }
}
