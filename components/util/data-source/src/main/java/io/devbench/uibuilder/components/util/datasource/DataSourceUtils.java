/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import org.jsoup.nodes.Element;

import static java.util.UUID.nameUUIDFromBytes;
import static java.util.UUID.randomUUID;

public final class DataSourceUtils {
    public static final String DATASOURCE_NAME = "name";
    public static final String DATASOURCE_ID = "datasource-id";

    public static String injectDataSourceId(Element element) {
        final String id;
        if (element.hasAttr(DATASOURCE_ID)) {
            id = element.attr(DATASOURCE_ID);
        } else {
            id = randomUUID().toString();
            element.attr(DATASOURCE_ID, id);
        }
        return id;
    }

    public static String replaceDataSourceId(Element element) {
        return replaceDataSourceId(element, false);
    }

    public static String replaceDataSourceId(Element element, boolean alwaysNewId) {
        final String id;
        if (element.hasAttr(DATASOURCE_ID) && !alwaysNewId) {
            id = nameUUIDFromBytes(element.attr(DATASOURCE_ID).getBytes()).toString();
        } else {
            id = randomUUID().toString();
        }
        element.attr(DATASOURCE_ID, id);
        return id;
    }

    public static Element findOneChildren(Element element, String tag) {
        return element.children().stream().filter(child -> tag.equalsIgnoreCase(child.tagName())).findAny()
            .orElseThrow(() -> new ElementNotFoundException(tag, element));
    }

    public static boolean isChildrenExist(Element element, String tag) {
        return element.children().stream()
            .anyMatch(child -> tag.equalsIgnoreCase(child.tagName()));
    }
}
