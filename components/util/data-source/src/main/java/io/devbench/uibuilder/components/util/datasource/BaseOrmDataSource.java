/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.order.OrderExpression;
import io.devbench.uibuilder.data.common.dataprovidersupport.OrmKeyMapper;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataProviderRequest;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import lombok.Getter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class BaseOrmDataSource<ELEMENT, ORDER_EXPRESSION_TYPE extends OrderExpression, FILTER_EXPRESSION_TYPE extends FilterExpression<?>>
    extends CommonDataSource<ELEMENT, ORDER_EXPRESSION_TYPE, FILTER_EXPRESSION_TYPE, DataSourceBindingContext> {

    @Getter
    private final List<String> keyPaths;

    protected BaseOrmDataSource(DataSourceBindingContext bindings, List<String> keyPaths) {
        super(bindings);
        this.keyPaths = Collections.unmodifiableList(keyPaths);
    }

    @Override
    protected final OrmKeyMapper<ELEMENT, FILTER_EXPRESSION_TYPE> createKeyMapper() {
        return new OrmKeyMapper<>(this, getMetadataProvider(), keyPaths);
    }

    private FILTER_EXPRESSION_TYPE parentIdentification() {
        FILTER_EXPRESSION_TYPE hierarchicalExpression;
        hierarchicalExpression = createExpression(ExpressionTypes.IsNull.class);
        ((ExpressionTypes.IsNull) hierarchicalExpression).setPath(Objects.requireNonNull(getBindingContext().getNestedBinding()).getParent());
        return hierarchicalExpression;
    }

    private FILTER_EXPRESSION_TYPE childIdentification(Object parent) {
        FILTER_EXPRESSION_TYPE hierarchicalExpression;
        hierarchicalExpression = createExpression(ExpressionTypes.Equals.class);
        ((ExpressionTypes.Equals) hierarchicalExpression).setPath(Objects.requireNonNull(getBindingContext().getNestedBinding()).getParent());
        ((ExpressionTypes.Equals) hierarchicalExpression).setValue(parent);
        return hierarchicalExpression;
    }

    protected final void handleHierarchicalRequest(DataProviderRequest request) {
        if (isHierarchical()) {
            final FILTER_EXPRESSION_TYPE hierarchicalExpression;
            if (request.getBody().getParentItem() != null) {
                hierarchicalExpression = childIdentification(getObjectFromRequest(request));
            } else {
                hierarchicalExpression = parentIdentification();
            }
            registerHierarchical(hierarchicalExpression);
        }
    }

    protected abstract void registerHierarchical(FILTER_EXPRESSION_TYPE hierarchicalExpression);

    @Override
    public boolean isHierarchical()  {
        return getBindingContext().getNestedBinding() != null;
    }

    @Override
    public boolean hasChildren(ClassMetadata<ELEMENT> element)  {
        if (isHierarchical()) {
            return hasChildren(mapParentIdToValue(element, (AndFilterExpression) createExpression(AndFilterExpression.class)));
        } else {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public FILTER_EXPRESSION_TYPE mapParentIdToValue(ClassMetadata<ELEMENT> element, AndFilterExpression containerExpression) {
        getKeyPaths().forEach(key ->
            element.getPropertyValue(key).ifPresent(it ->
                containerExpression.add(mapParentKeyToParentValue(key, it))
            ));
        return (FILTER_EXPRESSION_TYPE) containerExpression;
    }

    public FILTER_EXPRESSION_TYPE mapParentKeyToParentValue(String key, Object propertyValue) {
        final FILTER_EXPRESSION_TYPE hierarchicalExpression = createExpression(ExpressionTypes.Equals.class);
        ((ExpressionTypes.Equals) hierarchicalExpression)
            .setPath(Objects.requireNonNull(getBindingContext().getNestedBinding()).getParent() + "." + key);
        ((ExpressionTypes.Equals) hierarchicalExpression).setValue(propertyValue);
        return hierarchicalExpression;
    }

    @SuppressWarnings("unchecked")
    protected FILTER_EXPRESSION_TYPE wrapFilters(FILTER_EXPRESSION_TYPE left, FILTER_EXPRESSION_TYPE righ) {
        final AndFilterExpression expression = (AndFilterExpression) createExpression(AndFilterExpression.class);
        expression.add(left);
        expression.add(righ);
        return (FILTER_EXPRESSION_TYPE) expression;
    }

    @SuppressWarnings("unchecked")
    private FILTER_EXPRESSION_TYPE createExpression(Class<? extends FilterExpression> expressionTypeClass) {
        final FilterExpressionFactory<FilterExpression<?>> expressionFactory = (FilterExpressionFactory<FilterExpression<?>>) this.getFilterExpressionFactory();
        return (FILTER_EXPRESSION_TYPE) expressionFactory.create(expressionTypeClass);
    }
}
