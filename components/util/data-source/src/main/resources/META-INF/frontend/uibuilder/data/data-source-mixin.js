import { DataSource } from './data-source.js';
import { ItemDataSource } from '@vaadin/flow-frontend/uibuilder/data/item-data-source.js';

(function () {
    window.Uibuilder = window.Uibuilder || {};

    class PrimitiveValueSupportingItem {
        constructor(wrappedObject) {
            Object.assign(this, wrappedObject);
        }

        toString() {
            if (this.___PRIMITIVE_VALUE) {
                return this.___PRIMITIVE_VALUE;
            }
            return super.toString();
        }

    }

    window.Uibuilder.DataSourceMixin = superClass => class DataSourceMixin extends superClass {

        constructor() {
            super();
            this.itemIdPath = "___ITEM_KEY";
        }

        _processDataSourceTag() {
            const ds = this.querySelector(DataSource.is);
            if (!ds) {
                const itemDs = this.querySelector(ItemDataSource.is);
                if (!itemDs) {
                    console.log(`${DataSource.is} and ${ItemDataSource.is} is missing from ${this.tagName} with id:'${this.id}'`);
                }
            } else {
                this.dispatchEvent(new CustomEvent("dataSourceReady", {
                    detail: {
                        dataSource: {
                            name: ds.name,
                            id: ds.datasourceId,
                            defaultQuery: ds.defaultQuery
                        }
                    }
                }));
            }

            this._itemsEqual = (item1, item2) => DataSourceMixin.isEquivalent(
                this.getItemId(item1),
                this.getItemId(item2));

        }

        _setEndpointInfo(url, csrfId) {
            this.url = url;
            this.dataProvider = (params, callback) => {
                Promise.all([this._fetchData(params, csrfId), this._fetchSize(params, csrfId)])
                    .then(callbackParams => {
                        callbackParams[0] = callbackParams[0].map(it => new PrimitiveValueSupportingItem(it));
                        callback(...callbackParams)
                    });
            };
        }

        _fetchData(params, csrfId) {
            return fetch(this.url, {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    event: 'fetchData',
                    csrfId: csrfId,
                    detail: this._processParams(params)
                })
            }).then(response => response.json());
        }

        _fetchSize(params, csrfId) {
            return fetch(this.url, {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    event: 'fetchSize',
                    csrfId: csrfId,
                    detail: this._processParams(params)
                })
            }).then(response => response.text())
                .then(text => parseInt(text));
        }

        static isEquivalent(a, b) {
            if (!a || !b) {
                return a === b;
            }
            if (Array.isArray(a) && Array.isArray(b)) {
                if (a.length === b.length) {
                    return !a.find((value, index) => !DataSourceMixin.isEquivalent(value, b[index]));
                } else {
                    return false;
                }
            } else if (Array.isArray(a) || Array.isArray(b)) {
                return false;
            }
            const aProps = Object.getOwnPropertyNames(a);
            const bProps = Object.getOwnPropertyNames(b);
            if (aProps.length !== bProps.length) {
                return false;
            }
            for (let i = 0; i < aProps.length; i++) {
                const propName = aProps[i];
                const aElement = a[propName];
                const bElement = b[propName];
                if (typeof a === "object" && typeof b === "object") {
                    if (!DataSourceMixin.isEquivalent(aElement, bElement)) {
                        return false;
                    }
                } else if (aElement !== bElement) {
                    return false;
                }
            }
            return true;
        }

        _processParams(params) {
            params.resetFilters = params.filter === "";
            if (params.filter && this.itemLabelPath) {
                params.filters = [
                    {
                        "path": this.itemLabelPath,
                        "value": params.filter
                    }
                ]
            }
            return params;
        }
    }
})();
