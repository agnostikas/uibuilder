/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.data.api.exceptions.DataSourceFilterOptionException;
import io.devbench.uibuilder.data.common.datasource.DefaultFilterDescriptor;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({ResourceReaderExtension.class, JsoupExtension.class})
class BaseTagBasedParseInterceptorTest {

    private final TestTagBasedParseInterceptor testObj = new TestTagBasedParseInterceptor();

    @Test
    public void should_add_item_bindings_to_binding_context() {
        DataSourceBindingContext bindingContext = new DataSourceBindingContext();
        testObj.handleBinding("item.foobar", bindingContext);
        testObj.handleBinding("some.random.string", bindingContext);

        assertEquals(Sets.newHashSet("item.foobar"), bindingContext.getBindings());
    }

    @Test
    public void should_parse_datasource_name_from_element_correctly() {
        DataSourceBindingContext bindingContext = new DataSourceBindingContext();
        Element element = new Element("div");
        element.html("<data-source name=\"todoDS\"></data-source>");
        Element dataSourceElement = element.getElementsByTag("data-source").first();
        testObj.parseCommonBindingContextElements(dataSourceElement, element, bindingContext);

        assertEquals("todoDS", bindingContext.getDataSourceName());
    }

    @Test
    @DisplayName("Should parse item-label-path correctly")
    public void should_parse_item_label_path_correctly(
        @ReadResource("/UIBuilderComboBoxSpecificFeatures/should_parse_item_label_path_correctly.html") String htmlUibuilderCombobox
    ) {
        TestTagBasedParseInterceptor testObj = new TestTagBasedParseInterceptor();
        DataSourceBindingContext context = new DataSourceBindingContext();

        Element element = new Element("div");
        element.html(htmlUibuilderCombobox);
        Element testElement = element.getElementById("testElement");
        Element dataSourceElement = testElement.children().stream()
            .filter(child -> "data-source".equalsIgnoreCase(child.tagName()))
            .findAny().orElseThrow(IllegalStateException::new);

        testObj.parseCommonBindingContextElements(dataSourceElement, testElement, context);
        assertEquals(Sets.newHashSet("item.deadline"), context.getBindings());
    }

    @Test
    @DisplayName("Should parse simple item bind correctly")
    void should_parse_simple_item_bind_correctly() {
        DataSourceBindingContext bindingContext = new DataSourceBindingContext();
        testObj.handleBinding("item", bindingContext);

        assertEquals(Sets.newHashSet("item"), bindingContext.getBindings());
    }

    @Test
    @DisplayName("Should parse datasource with default filter option")
    void test_should_parse_datasource_with_default_filter_option(
        @LoadElement(value = "/datasource-with-default-filter-option.html", id = "testElement") Element element) {

        TestTagBasedParseInterceptor testObj = new TestTagBasedParseInterceptor();
        DataSourceBindingContext context = new DataSourceBindingContext();
        Element dataSourceElement = element.getElementsByTag("data-source").get(0);

        testObj.parseCommonBindingContextElements(dataSourceElement, element, context);

        DefaultFilterDescriptor defaultFilterDescriptor = context.getDefaultFilterDescriptor();

        DefaultFilterDescriptor.FilterType defaultFilterType = defaultFilterDescriptor.getDefaultFilterType(false);

        assertEquals("ilike", defaultFilterType.getName());
        assertEquals("ends-with", defaultFilterType.getMode());

        DefaultFilterDescriptor.FilterType filterType = defaultFilterDescriptor.getFilterType("some-non-exist-path");

        assertEquals("ilike", filterType.getName());
        assertEquals("ends-with", filterType.getMode());
    }

    @Test
    @DisplayName("Should throw exception when parsing data-source and there are multiple default options defined")
    void test_should_throw_exception_when_parsing_data_source_and_there_are_multiple_default_options_defined(
        @LoadElement(value = "/datasource-with-multiple-default-filter-options.html", id = "testElement") Element element) {

        TestTagBasedParseInterceptor testObj = new TestTagBasedParseInterceptor();
        DataSourceBindingContext context = new DataSourceBindingContext();
        Element dataSourceElement = element.getElementsByTag("data-source").get(0);

        assertThrows(DataSourceFilterOptionException.class, () -> testObj.parseCommonBindingContextElements(dataSourceElement, element, context));
    }

    @Test
    @DisplayName("Should parse datasource with filter options")
    void test_should_parse_datasource_with_filter_options(
        @LoadElement(value = "/datasource-with-filter-options.html", id = "testElement") Element element) {

        TestTagBasedParseInterceptor testObj = new TestTagBasedParseInterceptor();
        DataSourceBindingContext context = new DataSourceBindingContext();
        Element dataSourceElement = element.getElementsByTag("data-source").get(0);

        testObj.parseCommonBindingContextElements(dataSourceElement, element, context);

        DefaultFilterDescriptor defaultFilterDescriptor = context.getDefaultFilterDescriptor();

        DefaultFilterDescriptor.FilterType defaultFilterType = defaultFilterDescriptor.getDefaultFilterType(true);
        assertEquals("like", defaultFilterType.getName());
        assertEquals("contains", defaultFilterType.getMode());

        defaultFilterType = defaultFilterDescriptor.getDefaultFilterType(false);
        assertEquals("=", defaultFilterType.getName());
        assertEquals("none", defaultFilterType.getMode());

        DefaultFilterDescriptor.FilterType todoFilterType = defaultFilterDescriptor.getFilterType("todo");
        assertEquals("ilike", todoFilterType.getName());
        assertEquals("starts-with", todoFilterType.getMode());

        DefaultFilterDescriptor.FilterType deadlineFilterType = defaultFilterDescriptor.getFilterType("deadline");
        assertEquals(">=", deadlineFilterType.getName());
        assertEquals("none", deadlineFilterType.getMode());

        DefaultFilterDescriptor.FilterType nonExistsFilterType = defaultFilterDescriptor.getFilterType("non-exists");
        assertEquals("=", nonExistsFilterType.getName());
        assertEquals("none", nonExistsFilterType.getMode());
    }

    @Test
    @DisplayName("Should parse datasource with filter options and default filter option")
    void test_should_parse_datasource_with_filter_options_and_default_filter_option(
        @LoadElement(value = "/datasource-with-default-filter-and-filter-options.html", id = "testElement") Element element) {

        TestTagBasedParseInterceptor testObj = new TestTagBasedParseInterceptor();
        DataSourceBindingContext context = new DataSourceBindingContext();
        Element dataSourceElement = element.getElementsByTag("data-source").get(0);

        testObj.parseCommonBindingContextElements(dataSourceElement, element, context);

        DefaultFilterDescriptor defaultFilterDescriptor = context.getDefaultFilterDescriptor();

        DefaultFilterDescriptor.FilterType defaultFilterType = defaultFilterDescriptor.getDefaultFilterType(false);
        assertEquals("ilike", defaultFilterType.getName());
        assertEquals("contains", defaultFilterType.getMode());

        DefaultFilterDescriptor.FilterType todoFilterType = defaultFilterDescriptor.getFilterType("todo");
        assertEquals("ilike", todoFilterType.getName());
        assertEquals("contains", todoFilterType.getMode());

        DefaultFilterDescriptor.FilterType deadlineFilterType = defaultFilterDescriptor.getFilterType("deadline");
        assertEquals(">=", deadlineFilterType.getName());
        assertEquals("none", deadlineFilterType.getMode());
    }

    private static class TestTagBasedParseInterceptor extends BaseTagBasedParseInterceptor<DataSourceBindingContext> {
        @Override
        public DataSourceBindingContext createBindingContext(Element element) {
            return null;
        }

        @Override
        public void commitBindingParse(DataSourceBindingContext bindingContext, Component component) {

        }

        @Override
        public void intercept(Component component, Element element) {

        }

        @Override
        public boolean isApplicable(Element element) {
            return false;
        }
    }
}
