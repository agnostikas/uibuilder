/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, BaseUIBuilderTestExtension.class})
class DataSourceTagBasedParseInterceptorTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    @InjectMocks
    private DataSourceTagBasedParseInterceptor testObj;

    @Test
    public void should_apply_to_elements_with_item_data_source_children() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(DataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);

        assertTrue(testObj.isApplicable(parentElement));
    }

    @Test
    public void should_not_apply_to_elements_without_item_data_source_children() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Collections.singleton(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn("foo-bar");

        assertFalse(testObj.isApplicable(parentElement));
    }

    @Test
    public void should_return_binding_context_with_correct_name() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(DataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(DataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr(DataSourceTagBasedParseInterceptor.DEFAULT_QUERY_ATTRIBUTE_NAME)).thenReturn(true);
        when(dataSourceElement.attr(DataSourceTagBasedParseInterceptor.DEFAULT_QUERY_ATTRIBUTE_NAME)).thenReturn("defQueryName");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        DataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertEquals("testDs", bindingContext.getDataSourceName());
        assertEquals("defQueryName", bindingContext.getDefaultQueryName());
    }

    @Test
    public void should_handle_bindings_correctly() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(DataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(DataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        DataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);
        testObj.handleBinding("item.foo", bindingContext);
        testObj.handleBinding("bar.xyz", bindingContext);

        assertEquals("testDs", bindingContext.getDataSourceName());
        assertEquals(Sets.newHashSet("item.foo"), bindingContext.getBindings());
    }

    @Test
    public void commin_binding_should_register_data_source_provider() {
        DataSourceBindingContext context = mock(DataSourceBindingContext.class);
        Component component = mock(Component.class);

        when(context.getDefaultQueryName()).thenReturn(null);
        testObj.commitBindingParse(context, component);

        verify(dataSourceManager.getDataSourceProvider()).registerBindingContextForDataSource(context, null);
    }

    @Test
    @DisplayName("applies sub_elements must always return true")
    void applies_sub_elements_must_always_return_true() {
        assertTrue(testObj.appliesToSubElements());
    }

    @Test
    @DisplayName("commit should store binding context into deposit and when the same datasource has been requested to be created")
    void test_commit_should_store_binding_context_into_deposit_and_when_the_same_datasource_has_been_requested_to_be_created() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Collections.singletonList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(DataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(DataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        DataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertNotNull(bindingContext);

        String datasourceId = bindingContext.getDataSourceId();
        assertTrue(StringUtils.isNotBlank(datasourceId));

        testObj.commitBindingParse(bindingContext, null);

        DataSourceBindingContext anotherBindingContext = testObj.createBindingContext(parentElement);

        assertNotSame(bindingContext, anotherBindingContext);

        String anotherDatasourceId = anotherBindingContext.getDataSourceId();
        assertTrue(StringUtils.isNotBlank(anotherDatasourceId));

        assertNotEquals(anotherDatasourceId, datasourceId);
        assertEquals("testDs", anotherBindingContext.getDataSourceName());
    }
}
