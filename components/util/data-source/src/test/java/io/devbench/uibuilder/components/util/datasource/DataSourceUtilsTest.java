/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.datasource;

import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.devbench.uibuilder.components.util.datasource.DataSourceUtils.DATASOURCE_ID;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({JsoupExtension.class})
class DataSourceUtilsTest {
    @LoadElement(value = "/UIBuilderComboBoxSpecificFeatures/should_parse_item_label_path_correctly.html", id = "testElement")
    private Element testElement;

    @LoadElement(value = "/UIBuilderComboBoxSpecificFeatures/should_parse_item_label_path_correctly.html", id = "testDs")
    private Element testDs;

    @LoadElement(value = "/UIBuilderComboBoxSpecificFeatures/should_parse_item_label_path_correctly.html", id = "testContainer")
    private Element testContainter;

    @Test
    void shouldNotModifyIdWhenItIsExisting() {
        final String baseId = testElement.id();
        assertNotNull(baseId);
        final String id = DataSourceUtils.injectDataSourceId(testElement);
        assertEquals(baseId, testElement.id());
        assertNotNull(id);
    }

    @Test
    void shouldCreateAndInjectIdWhenItIsNotExisting() {
        final Element dataSource = DataSourceUtils.findOneChildren(testElement.clone(), "data-source");
        final String dataSourceId = dataSource.id();
        assertTrue(dataSourceId.isEmpty());
        final String id = DataSourceUtils.injectDataSourceId(dataSource);
        assertEquals(id, dataSource.attr(DATASOURCE_ID));
    }

    @Test
    void shouldCreateANewDSIdBasedOnTheExistingWhenItIsExisting() {
        testDSIdIsNewAndIdIsSimilar(testElement.clone());
        testDSIdIsNewAndIdIsSimilar(testDs.clone());
    }

    private void testDSIdIsNewAndIdIsSimilar(Element element) {
        final String baseId = element.attr(DATASOURCE_ID);
        assertNotNull(baseId);
        final String id = DataSourceUtils.replaceDataSourceId(element);
        assertNotNull(id);
        assertNotEquals(baseId, element.attr(DATASOURCE_ID));
    }

    @Test
    void shouldCreateAndInjectADSIdWhenItIsNotExisting() {
        final Element dataSource = DataSourceUtils.findOneChildren(testElement.clone(), "data-source");
        final String dataSourceId = dataSource.id();
        final String id = DataSourceUtils.replaceDataSourceId(dataSource);
        assertEquals(id, dataSource.attr(DATASOURCE_ID));
        assertTrue(dataSourceId.isEmpty());
    }

    @Test
    void findOrElseThrow() {
        assertThrows(ElementNotFoundException.class,
            () -> DataSourceUtils.findOneChildren(testElement, "something-not-in"));
        assertNotNull(DataSourceUtils.findOneChildren(testElement, "data-source"));
        assertEquals(testElement.child(0), DataSourceUtils.findOneChildren(testElement, "data-source"));
    }

    @Test
    void childrenIsExist() {
        assertFalse(DataSourceUtils.isChildrenExist(testElement, "something-not-in"));
        assertTrue(DataSourceUtils.isChildrenExist(testElement, "data-source"));
        assertFalse(DataSourceUtils.isChildrenExist(testContainter, "data-source"));
    }
}
