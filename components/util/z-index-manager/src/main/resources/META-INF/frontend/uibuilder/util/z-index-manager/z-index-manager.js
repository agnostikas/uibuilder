export class ZIndexMngr {
    constructor() {
        this.registry = [];

        let baseIndex = 100;
        Object.defineProperty(this, 'baseIndex', {
            enumerable: true,
            get: () => baseIndex,
            set(v) {
                baseIndex = v;
                this.refresh();
            }
        });
    }

    raise(e) {
        this.remove(e);
        this.add(e);
    }

    add(e) {
        this.registry.push(e);
        this.refresh();
    }

    remove(e) {
        this.registry.splice(this.registry.indexOf(e), 1);
    }

    refresh() {
        this.registry.forEach((e, i) => e.style.zIndex = this.baseIndex + i);
    }
}

window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.zIndexMngr = window.Uibuilder.zIndexMngr || new ZIndexMngr();
