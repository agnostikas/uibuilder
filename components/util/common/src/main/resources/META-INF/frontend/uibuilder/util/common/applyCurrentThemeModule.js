window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.applyCurrentThemeModule = window.Uibuilder.applyCurrentThemeModule || function (element) {
    const themeFor = element.getAttribute('theme-for');
    const customElement = customElements.get(themeFor);

    if (customElement) {
        const styleEl = element.querySelector('template').content.querySelector('style');

        const memoizedTemplate = customElement.prototype._template;
        memoizedTemplate && memoizedTemplate.content.appendChild(styleEl.cloneNode(true));
    }
};
