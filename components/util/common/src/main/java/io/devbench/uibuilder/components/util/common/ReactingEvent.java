/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.util.common;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import elemental.json.JsonValue;

public abstract class ReactingEvent<T extends Component> extends ComponentEvent<T> {
    protected static final String REQUEST_ID = "event._reqId";

    private final String requestId;

    public ReactingEvent(T source, boolean fromClient, String requestId) {
        super(source, fromClient);
        this.requestId = requestId;
    }

    public void sendDataResponse(JsonValue response) {
        executeJavaScript("Uibuilder.EventResponseHandler.resolve($0, $1)", response);
    }

    public void sendErrorResponse(JsonValue response) {
        executeJavaScript("Uibuilder.EventResponseHandler.reject($0, $1)", response);
    }

    private void executeJavaScript(String expression, JsonValue response) {
        ((Component) source).getElement().executeJs(expression, this.requestId, response);
    }
}
