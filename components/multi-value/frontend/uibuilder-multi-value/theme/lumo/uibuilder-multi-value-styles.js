/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

const $_documentContainer = html`
<dom-module id="lumo-uibuilder-uibuilder-multi-value" theme-for="uibuilder-multi-value">
    <template>
        <style>

            [part="component"] {
                border-radius: var(--lumo-border-radius);
                border: var(--uibuilder-lumo-border);
                background-color: var(--uibuilder-lumo-modal-content-bg-color);
                padding: 6px;

                display: flex;
                flex-direction: row;
            }

            [part="addButtonContainer"] {
                order: 0;
            }

            [part="container"] {
                order: 1;
                margin-left: 8px;
            }

        </style>
    </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
