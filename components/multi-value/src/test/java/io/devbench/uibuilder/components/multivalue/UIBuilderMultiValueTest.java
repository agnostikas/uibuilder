/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.multivalue;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ElementData;
import com.vaadin.flow.internal.nodefeature.ElementListenerMap;
import com.vaadin.flow.internal.nodefeature.VirtualChildrenList;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.api.utils.CollectionUtil;
import io.devbench.uibuilder.components.crudpanel.UIBuilderCrudPanel;
import io.devbench.uibuilder.components.multivalue.exception.IllegalMultiValueItemInstantiationException;
import io.devbench.uibuilder.components.multivalue.exception.IllegalMultiValueItemsStateException;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, BaseUIBuilderTestExtension.class})
class UIBuilderMultiValueTest {

    private UIBuilderMultiValue<?, ?> testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderMultiValue<>();
    }

    @Test
    @DisplayName("Should set value item path")
    void test_should_set_value_item_path() {
        String originalValueItemPath = testObj.getValueItemPath();
        testObj.setValueItemPath("newValueItemPath");
        String newValueItemPath = testObj.getValueItemPath();

        assertNotNull(originalValueItemPath);
        assertEquals("value", originalValueItemPath);
        assertNotNull(newValueItemPath);
        assertEquals("newValueItemPath", newValueItemPath);
    }

    @Test
    @DisplayName("Should create default item supplier if there is not a defined one")
    void test_should_create_default_item_supplier_if_there_is_not_a_defined_one() {
        Supplier<?> itemSupplier = testObj.getItemSupplier();
        assertNotNull(itemSupplier);
    }

    @Test
    @DisplayName("Should return null by the default item supplier, if there is no item class defined")
    void test_should_return_null_by_the_default_item_supplier_if_there_is_no_item_class_defined() {
        Supplier<?> itemSupplier = testObj.getItemSupplier();
        Object item = itemSupplier.get();
        assertNull(item);
    }

    @Test
    @DisplayName("Should throw exception if item type cannot be created by the default item supplier")
    void test_should_throw_exception_if_item_type_cannot_be_created_by_the_default_item_supplier() {
        testObj.setItemType(TestNonInstantiableMultiItem.class);
        Supplier<?> itemSupplier = testObj.getItemSupplier();
        IllegalMultiValueItemInstantiationException exception =
            assertThrows(IllegalMultiValueItemInstantiationException.class, itemSupplier::get);
        assertNotNull(exception);
        assertTrue(exception.getMessage().contains("Could not create item instance"));
    }

    @Test
    @SuppressWarnings({"rawtypes", "unchecked"})
    @DisplayName("Should return item created by the custom item supplier")
    void test_should_return_item_created_by_the_custom_item_supplier() {
        SerializableSupplier supplier = () -> new TestMultiItem(
            "test",
            10,
            LocalDateTime.parse("2021-04-06T13:03:01", DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        testObj.setItemSupplier(supplier);

        Object newItem = testObj.getItemSupplier().get();

        assertNotNull(newItem);
        assertTrue(newItem instanceof TestMultiItem);
        assertEquals("test", ((TestMultiItem) newItem).getText());
        assertEquals(10, ((TestMultiItem) newItem).getNum());
        assertEquals("2021-04-06T13:03:01", ((TestMultiItem) newItem).getDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }

    @Test
    @DisplayName("Should return default instance of the defined item class by the default item supplier")
    void test_should_return_default_instance_of_the_defined_item_class_by_the_default_item_supplier() {
        testObj.setItemType(TestMultiItem.class);
        Object instance = testObj.getItemSupplier().get();
        assertTrue(instance instanceof TestMultiItem);
    }

    @Test
    @DisplayName("Should find item type by crud-panel item class")
    void test_should_find_item_type_by_crud_panel_item_class() {
        testObj.setParentCrudElementId("test-crud-panel");
        testObj.setParentCrudElementBindingPropertyPath("testItems");

        UIBuilderCrudPanel<?> crudPanel = mock(UIBuilderCrudPanel.class);
        Mockito.doReturn(Optional.of("test-crud-panel")).when(crudPanel).getId();

        ElementCollector.register(crudPanel, new Element("crud-panel"));

        Class<?> itemType = testObj.getItemType();
        assertNull(itemType);

        Mockito.doReturn(TestMultiItemHolder.class).when(crudPanel).getItemType();

        itemType = testObj.getItemType();
        assertNotNull(itemType);
        assertTrue(TestMultiItem.class.isAssignableFrom(itemType));
    }

    @Test
    @DisplayName("Should return value type as String if there is no item type defined")
    void test_should_return_value_type_as_string_if_there_is_no_item_type_defined() {
        assertEquals(String.class, testObj.getValueType());
    }

    @Test
    @DisplayName("Should return value type by the value item type attribute if item type is defined")
    void test_should_return_value_type_by_the_value_item_type_attribute_if_item_type_is_defined() {
        testObj.setItemType(TestMultiItem.class);
        testObj.setValueItemPath("num");
        assertEquals(Integer.class, testObj.getValueType());
    }

    @Test
    @DisplayName("Should get String.class if there is an illegal item value path defined")
    void test_should_get_string_class_if_there_is_an_illegal_item_value_path_defined() {
        testObj.setItemType(TestMultiItem.class);
        testObj.setValueItemPath("notValidPath");
        Class<?> valueType = testObj.getValueType();
        assertEquals(String.class, valueType);
    }

    @Test
    @DisplayName("Should return an empty list if items is null")
    void test_should_return_an_empty_list_if_items_is_null() {
        List<?> items = testObj.getItems();
        assertNotNull(items);
        assertTrue(items.isEmpty());
    }

    @Test
    @SuppressWarnings({"rawtypes", "unchecked"})
    @DisplayName("Should call frontend when setItems is called")
    void test_should_call_frontend_when_set_items_is_called() {
        testObj = spy(testObj);

        com.vaadin.flow.dom.Element element = mock(com.vaadin.flow.dom.Element.class);
        doReturn(element).when(testObj).getElement();

        UIBuilderMultiValueBindingContext bindingContext = mock(UIBuilderMultiValueBindingContext.class);
        testObj.setBindingContext(bindingContext);

        List<TestMultiItem> items = Arrays.asList(
            new TestMultiItem("item 1", 10, LocalDateTime.parse("2021-04-06T13:35:49", DateTimeFormatter.ISO_LOCAL_DATE_TIME)),
            new TestMultiItem("item 2", 20, LocalDateTime.parse("2021-04-06T13:45:49", DateTimeFormatter.ISO_LOCAL_DATE_TIME)),
            null,
            new TestMultiItem("item 3", 30, LocalDateTime.parse("2021-04-06T13:55:49", DateTimeFormatter.ISO_LOCAL_DATE_TIME)));

        List listOfItems = new ArrayList<>(items);
        Set setOfItems = new HashSet<>(items);

        testObj.setItems((Collection) null);
        assertTrue(testObj.getItems() instanceof ArrayList);
        assertTrue(testObj.getItems().isEmpty());

        testObj.setItems(listOfItems);
        assertSame(listOfItems, testObj.getItems());

        testObj.setItems(setOfItems);
        assertTrue(testObj.getItems() instanceof ArrayList);
        assertFalse(testObj.getItems().isEmpty());
        assertTrue(CollectionUtil.isCollectionsEqual((List) items, testObj.getItems()));

        verify(bindingContext, times(6)).createJsonItem(anyInt(), any(), eq("value"));
        verify(element, times(3)).callJsFunction(eq("setItems"), any(JsonValue.class));
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should handle instance added event")
    void test_should_handle_instance_added_event() {
        UIBuilderMultiValueBindingContext bindingContext = new UIBuilderMultiValueBindingContext("element");
        bindingContext.getBindings().add("element.text");
        bindingContext.getBindings().add("element.num");
        bindingContext.getBindings().add("element.dateTime");

        List<TestMultiItem> items = new ArrayList<>(Collections.singletonList(
            new TestMultiItem("item 1", 10, LocalDateTime.now())
        ));

        com.vaadin.flow.dom.Element testObjElement = spy(testObj.getElement());
        testObj = spy(testObj);
        doReturn(testObjElement).when(testObj).getElement();

        testObj.setBindingContext(bindingContext);
        testObj.setItemType(TestMultiItem.class);
        testObj.setItems((List) items);
        testObj.onAttached();

        Component componentWithoutDatasource = mock(Component.class, withSettings().extraInterfaces(HasValue.class));
        Element componentWithoutDatasourceElement = new Element("vaadin-text-field");
        componentWithoutDatasourceElement.attr("id", "num-index-3");
        ElementCollector.register(componentWithoutDatasource, componentWithoutDatasourceElement);

        AbstractDataSourceComponent componentWithDatasource = mock(AbstractDataSourceComponent.class, withSettings().extraInterfaces(HasValue.class));
        Element componentWithDatasourceElement = new Element("uibuilder-combobox");
        componentWithDatasourceElement.attr("id", "dateTime-index-3");
        ElementCollector.register(componentWithDatasource, componentWithDatasourceElement);


        JsonArray innerBindings = Json.createArray();
        JsonObject innerBindingsElementIdMap = Json.createObject();
        innerBindingsElementIdMap.put("element.num", "num-index-3");
        innerBindingsElementIdMap.put("element.dateTime", "dateTime-index-3");

        JsonObject eventData = Json.createObject();
        eventData.put("event.detail.index", 1);
        eventData.put("event.detail.userAdded", true);
        eventData.put("event.detail.templateHtmlContent", "");
        eventData.put("event.detail.innerBindings", innerBindings);
        eventData.put("event.detail.innerBindingsElementIdMap", innerBindingsElementIdMap);
        fireEvent("instance-added", eventData);

        assertEquals(2, items.size());
        assertNotNull(items.get(1));

        verify(testObjElement).callJsFunction(eq("_pushItemValue"), eq(1), any(JsonObject.class));
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should handle instance removed event")
    void test_should_handle_instance_removed_event() {
        VirtualChildrenList virtualChildrenList = mock(VirtualChildrenList.class);

        StateNode node1 = new StateNode(ElementData.class);
        ElementData elementData1 = node1.getFeature(ElementData.class);
        JsonObject elementData1Payload = Json.createObject();
        elementData1Payload.put("type", "@id");
        elementData1Payload.put("payload", "some-id");
        elementData1.setPayload(elementData1Payload);

        StateNode node2 = new StateNode(ElementData.class);
        ElementData elementData2 = node2.getFeature(ElementData.class);
        JsonObject elementData2Payload = Json.createObject();
        elementData2Payload.put("type", "@id");
        elementData2Payload.put("payload", "some-index-1");
        elementData2.setPayload(elementData2Payload);

        testObj = spy(testObj);
        com.vaadin.flow.dom.Element testObjElement = spy(testObj.getElement());
        StateNode testObjElementStateNode = spy(testObjElement.getNode());

        doReturn(testObjElement).when(testObj).getElement();
        doReturn(testObjElementStateNode).when(testObjElement).getNode();
        doReturn(virtualChildrenList).when(testObjElementStateNode).getFeature(eq(VirtualChildrenList.class));
        doAnswer(invocationOnMock -> {
            Consumer<StateNode> consumer = invocationOnMock.getArgument(0);
            Stream.of(node1, node2).forEach(consumer);
            return null;
        }).when(virtualChildrenList).forEachChild(any());
        doAnswer(invocationOnMock -> {
            StateNode stateNode = invocationOnMock.getArgument(0);
            if (stateNode == node1) {
                return 0;
            } else if (stateNode == node2) {
                return 1;
            } else {
                return -1;
            }
        }).when(virtualChildrenList).indexOf(any());

        UIBuilderMultiValueBindingContext bindingContext = mock(UIBuilderMultiValueBindingContext.class);
        testObj.setBindingContext(bindingContext);

        List<TestMultiItem> items = new ArrayList<>(Arrays.asList(
            new TestMultiItem("item 1", 10, null),
            new TestMultiItem("item 2", 20, null),
            new TestMultiItem("item 3", 30, null)
        ));

        testObj.setItemType(TestMultiItem.class);
        testObj.setItems((List) items);
        testObj.onAttached();

        JsonObject eventData = Json.createObject();
        eventData.put("event.detail.index", 1);
        fireEvent("instance-removed", eventData);

        assertEquals(2, items.size());
        assertEquals("item 1", items.get(0).getText());
        assertEquals("item 3", items.get(1).getText());

        verify(virtualChildrenList).remove(eq(1));
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should throw exception if index is out of bounds when instance change event is being handled")
    void test_should_throw_exception_if_index_is_out_of_bounds_when_instance_change_event_is_being_handled() {
        UIBuilderMultiValueBindingContext bindingContext = mock(UIBuilderMultiValueBindingContext.class);
        testObj.setBindingContext(bindingContext);

        List<TestMultiItem> items = new ArrayList<>(Arrays.asList(
            new TestMultiItem("item 1", 10, null),
            new TestMultiItem("item 2", 20, null)
        ));

        testObj.setItemType(TestMultiItem.class);
        testObj.setItems((List) items);
        testObj.onAttached();

        JsonObject eventData = Json.createObject();
        eventData.put("event.detail.index", 2);
        eventData.put("event.detail.propertyPath", "text");
        eventData.put("event.detail.propertyValue", "item 3");

        IllegalMultiValueItemsStateException exception =
            assertThrows(IllegalMultiValueItemsStateException.class, () -> fireEvent("instance-changed", eventData));

        assertNotNull(exception);
        assertTrue(exception.getMessage().contains("index: 2, backend size: 2"));
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should handle instance change event")
    void test_should_handle_instance_change_event() {
        UIBuilderMultiValueBindingContext bindingContext = mock(UIBuilderMultiValueBindingContext.class);
        testObj.setBindingContext(bindingContext);

        List<TestMultiItem> items = new ArrayList<>(Arrays.asList(
            new TestMultiItem("item 1", 10, null),
            new TestMultiItem("item 2", 20, null)
        ));

        testObj.setItemType(TestMultiItem.class);
        testObj.setItems((List) items);
        testObj.onAttached();

        JsonObject eventData = Json.createObject();
        eventData.put("event.detail.index", 1);
        eventData.put("event.detail.propertyElementId", "");
        eventData.put("event.detail.propertyPath", "element.num");
        eventData.put("event.detail.propertyValue", "someText");

        fireEvent("instance-changed", eventData);
        assertEquals(20, items.get(1).getNum());

        eventData.put("event.detail.propertyValue", "25");
        fireEvent("instance-changed", eventData);
        assertEquals(25, items.get(1).getNum());

        eventData.put("event.detail.index", 0);
        eventData.put("event.detail.propertyPath", "element.text");
        eventData.put("event.detail.propertyValue", "item modified");
        fireEvent("instance-changed", eventData);
        assertEquals("item modified", items.get(0).getText());
    }

    @Test
    @SuppressWarnings({"rawtypes", "unchecked"})
    @DisplayName("Should get values based on item type and item value path")
    void test_should_get_values_based_on_item_type_and_item_value_path() {

        testObj.setValueItemPath("num");
        testObj.setItemType(TestMultiItem.class);

        JsonArray array = Json.createArray();
        array.set(0, Json.create(15));
        array.set(1, Json.create(25));
        array.set(2, Json.create(35));
        testObj.getElement().setPropertyJson("value", array);

        assertIterableEquals(Arrays.asList(15, 25, 35), testObj.getValue());

        List values = new ArrayList<>();
        values.add(10);
        values.add(20);
        testObj.setValue(values);

        Serializable value = testObj.getElement().getPropertyRaw("value");

        assertTrue(value instanceof JsonArray);
        JsonArray jsonArrayValue = (JsonArray) value;

        assertEquals(2, jsonArrayValue.length());
        assertEquals("10", jsonArrayValue.getString(0));
        assertEquals("20", jsonArrayValue.getString(1));

        List<?> doubles = testObj.getValues(Double.class);
        assertEquals(2, doubles.size());
        assertTrue(doubles.get(0) instanceof Double);
        assertEquals(10d, doubles.get(0));
        assertEquals(20d, doubles.get(1));
    }

    private void fireEvent(String eventType, JsonObject eventData) {
        testObj.getElement()
            .getNode()
            .getFeature(ElementListenerMap.class)
            .fireEvent(new DomEvent(testObj.getElement(), eventType, eventData));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestMultiItem {
        private String text;
        private Integer num;
        private LocalDateTime dateTime;
    }

    public static class TestNonInstantiableMultiItem {
        TestNonInstantiableMultiItem() {
            throw new UnsupportedOperationException();
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestMultiItemHolder {
        private List<TestMultiItem> testItems;
    }

}
