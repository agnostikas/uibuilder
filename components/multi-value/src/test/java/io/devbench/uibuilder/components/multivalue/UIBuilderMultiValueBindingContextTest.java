/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.multivalue;

import elemental.json.JsonObject;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderMultiValueBindingContextTest {

    private UIBuilderMultiValueBindingContext testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderMultiValueBindingContext("element");
    }

    @Test
    @DisplayName("Should create json object from item instance according to the specified bindings")
    void test_should_create_json_object_from_item_instance_according_to_the_specified_bindings() {
        Set<String> bindings = testObj.getBindings();
        bindings.add("element.text");
        bindings.add("element.sub.dateTime");

        TestMultiItem item = new TestMultiItem("item text", 100,
            new TestMultiSubItem(LocalDateTime.parse("2021-04-06T16:49:27", DateTimeFormatter.ISO_LOCAL_DATE_TIME)));

        JsonObject jsonObject = testObj.createJsonItem(0, item);

        assertNotNull(jsonObject);

        JsonObject element = jsonObject.getObject("element");
        assertNotNull(element);

        assertEquals("item text", element.getString("text"));
        assertFalse(element.hasKey("num"));

        JsonObject sub = element.getObject("sub");
        assertNotNull(sub);
        assertEquals("2021-04-06T16:49:27", sub.getString("dateTime"));
    }

    @Test
    @DisplayName("Should create json object from item instance according to the specified bindings and possible backend value suppliers")
    void test_should_create_json_object_from_item_instance_according_to_the_specified_bindings_and_possible_backend_value_suppliers() {
        Set<String> bindings = testObj.getBindings();
        bindings.add("element.text");

        AtomicReference<String> initializerResponse = new AtomicReference<>();

        TestMultiItem item = new TestMultiItem("item text", 100, null);
        testObj.registerBackendComponentValueSupplier(0, "element.text",
            () -> "alternative value",
            initialItem -> initializerResponse.set(((TestMultiItem) initialItem).getText()));

        JsonObject jsonObject = testObj.createJsonItem(0, item);

        assertNotNull(jsonObject);
        assertEquals("alternative value", jsonObject.getObject("element").getString("text"));
        assertEquals("item text", initializerResponse.get());
    }

    @Test
    @DisplayName("Should create jeson object from primitive")
    void test_should_create_jeson_object_from_primitive() {

        JsonObject jsonObject = testObj.createJsonItem(0, 156, "data");

        assertNotNull(jsonObject);
        assertEquals(156, jsonObject.getObject("element").getNumber("data"));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestMultiItem {
        private String text;
        private Integer num;
        private TestMultiSubItem sub;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestMultiSubItem {
        private LocalDateTime dateTime;
    }

}
