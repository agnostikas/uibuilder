/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { LegacyElementMixin } from '@polymer/polymer/lib/legacy/legacy-element-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';
import './prism.js';


export class UibuilderCodeBlock extends ThemableMixin(Uibuilder.I18NBaseMixin('uibuilder-code-block', PolymerElement)) {

    static get template() {
        return html`
            <style include="prism-theme-default">
                :host {
                    display: block;
                    width: calc(100% - 16px);
                    margin: 4px;
                    padding: 4px;
                }
            </style>

            <div part="codeBlock" class="codeBlock">
                <pre part="codeContent" id="codeContent" class="code line-numbers"></pre>
                <vaadin-button part="copyButton" id="copyButton" title="Copy to clipboard" on-click="_copyToClipboard">
                    <iron-icon icon="lumo:edit" slot="prefix"></iron-icon>
                    <span>Copy</span>
                </vaadin-button>
                <slot></slot>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-code-block';
    }

    static get properties() {
        return {
            lang: {
                type: String,
                value: null
            },
            href: {
                type: String,
                value: null
            }
        };
    }

    connectedCallback() {
        super.connectedCallback();
        this.shadowRoot.querySelector("#copyButton span").textContent = this.tr('Copy');
        this.shadowRoot.querySelector("#copyButton").title = this.tr('Copy to clipboard');

        if (this.href) {
            fetch(this.href)
                .then(response => {
                    if (response.status === 200) {
                        response.text().then(code => {
                            code = UibuilderCodeBlock._removeIndent(code);
                            code = UibuilderCodeBlock._removeStartingAndTrailingEmptyLines(code);
                            this.$.codeContent.innerHTML = this._highlight(code, this.lang);
                        });
                    } else {
                        console.warn(`Not able to fetch: ${this.href}; status code is: ${response.status}`)
                    }
                })
        }

        this.childNodes.forEach(node => {
            if (node.tagName && node.tagName.toLowerCase() === 'script') {
                let code = node.innerText;
                code = UibuilderCodeBlock._removeIndent(code);
                code = UibuilderCodeBlock._removeStartingAndTrailingEmptyLines(code);
                code = UibuilderCodeBlock._fixClosingScriptTag(code);

                this.$.codeContent.innerHTML = this._highlight(code, this.lang);
            }
        });
    }

    static _removeIndent(text) {
        const minRemovableIndent = Math.min(...text.split('\n').map(line => line.match(/\S+/) ? line.search(/\S|$/) : 999));
        return text.split('\n').map((line) => line.length >= minRemovableIndent ? line.substring(minRemovableIndent) : line).join('\n');
    }

    static _removeStartingAndTrailingEmptyLines(text) {
        let start = false;
        let end = false;
        return text.split('\n')
            .filter(line => start = start || line.match(/\S+/)).reverse()
            .filter(line => end = end || line.match(/\S+/)).reverse().join('\n');
    }

    static _fixClosingScriptTag(text) {
        return text.replace('<\/\/script>', '<\/script>').replace('<s-script>', '<script>').replace('<\/s-script>', '<\/script>');
    }

    _highlight(code, lang) {
        const lowLang = lang ? lang.toLowerCase() : 'xml';
        let language = Prism.languages.xml;

        if (lowLang === 'java') {
            language = Prism.languages.java;
        } else if (lowLang === 'javascript' || lowLang === 'js') {
            language = Prism.languages.javascript;
        } else if (lowLang === 'html') {
            language = Prism.languages.html;
        }

        return Prism.highlight(code, language);
    }

    _copyToClipboard() {
        const codeElement = this.shadowRoot.querySelector('#codeContent');
        const codeRange = document.createRange();
        codeRange.selectNodeContents(codeElement);

        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(codeRange);
        try {
            document.execCommand('copy');
        } finally {
            selection.removeAllRanges();
        }
    }
}

customElements.define(UibuilderCodeBlock.is, UibuilderCodeBlock);
