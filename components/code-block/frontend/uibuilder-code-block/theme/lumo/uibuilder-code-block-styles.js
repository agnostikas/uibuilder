/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
import '../uibuilder-core-block-styles-default.js';

const $_documentContainer = html`
<dom-module id="lumo-code-block" theme-for="uibuilder-code-block">
    <template>
        <style include="prism-theme-default">
            :host {
                background-color: var(--lumo-base-color);
                font-size: 1.2rem;
                font-family: monospace;
                border: 1px solid var(--lumo-shade-10pct);
                line-height: normal;
            }

            [part="codeContent"] {
                padding-left: 8px;
                padding-right: 8px;
            }

            [part="codeBlock"] {
                position: relative;
            }

            [part="copyButton"] {
                position: absolute;
                top: -8px;
                right: 0;
                margin: 4px;
                opacity: 0.2;
            }

            [part="copyButton"]:hover {
                opacity: 1.0;
            }
        </style>
    </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
