/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
window.Uibuilder = window.Uibuilder || {};

Uibuilder.FormLocalDate = class {

    static get properties() {
        return {
            year: Number,
            month: Number,
            day: Number,
            hour: Number,
            minute: Number,
            second: Number,
            millisecond: Number,
        }
    }

    static getIsoOrRaw(value, func) {
        func(Uibuilder.FormLocalDate._isLocalDate(value) ? new Uibuilder.FormLocalDate(value).toIso() : value);
    }

    static _isLocalDate(value) {
        return value != null && value.hasOwnProperty('year') && value.hasOwnProperty('dayOfMonth');
    }

    static _isLocalDateTime(value) {
        return value != null && value.hasOwnProperty('hour') && value.hasOwnProperty('minute');
    }

    static normalizeDateTime(value) {
        if (!value) {
            return '';
        } else {
            let [date, time] = value.split('T');
            let divSym = time.indexOf('.') !== -1 ? '.' : (time.indexOf(',') !== -1 ? ',' : null)
            if (divSym) {
                let [seconds, milliseconds] = time.split(divSym);
                return date + 'T' + seconds + '.' + milliseconds.substr(0, 3);
            } else {
                return date + 'T' + time;
            }
        }
    }

    constructor(value = null) {
        this.raw = value;
    }

    set raw(value) {
        this._raw = value;

        let r = this._raw;
        if (Uibuilder.FormLocalDate._isLocalDate(r)) {
            this._setDate(r.year, r.monthValue, r.dayOfMonth);
            if (Uibuilder.FormLocalDate._isLocalDateTime(r)) {
                this._setTime(r.hour, r.minute, r.second, r.nano);
            }
        } else {
            this._setDate(null);
        }
    }

    toIso() {
        let iso = this.year + '-' + this.month.toString().padStart(2, "0") + '-' + this.day.toString().padStart(2, "0");
        if (this.hour) {
            iso = iso + "T" + this.hour.toString().padStart(2, "0") + ':' + this.minute.toString().padStart(2, "0");
            if (this.second) {
                iso = iso + ":" + this.second.toString().padStart(2, "0");
                if (this.millisecond) {
                    iso = iso + "." + this.millisecond;
                }
            }
        }
        return iso;
    }

    _setDate(year, month = null, day = null) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    _setTime(hour, minute, second = null, nano = null) {
        this.hour = hour;
        this.minute = minute;
        this.second = second ? second : undefined;
        this.millisecond = nano ? Math.round(nano / 1000) : undefined;
    }

};
