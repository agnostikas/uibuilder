/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UibuilderForm } from '@vaadin/flow-frontend/uibuilder-form/src/uibuilder-form.js';

window.Uibuilder = window.Uibuilder || {};

Uibuilder.FormField = class {

    constructor(form, itemBind, element, valueSourcePropertyName = 'value', valid = true, errorMessage = null) {

        if (element && element.tagName && element.tagName.toLowerCase() === 'item-data-source') {
            this._element = element.parentElement;
        } else {
            this._element = element;
        }

        this._form = form;
        this._itemBind = itemBind;
        this._valueSourcePropertyName = valueSourcePropertyName;
        this._valid = valid;
        this._errorMessage = errorMessage;
        this._value = null;
        this._type = typeof (this._element[this._valueSourcePropertyName]);
    }

    static parseNode(form, node) {
        let propName = node.attributes['item-bind'].value;
        let valueSourcePropName = 'value';
        if (propName.indexOf(':') > 0) {
            let propArr = propName.split(':');
            valueSourcePropName = propArr[0];
            propName = propArr[1];
        }
        return new Uibuilder.FormField(form, propName, node, valueSourcePropName);
    }

    reset() {
        Uibuilder.FormLocalDate.getIsoOrRaw(this._value, value => {
            if (this._element instanceof UibuilderForm) {
                this._element.set(this._valueSourcePropertyName, value);
                this._element.reset(false);
            } else if (this._valueSourcePropertyName === 'items') {
                // "items" indicates that this is a collection, and has to be handled by the backend
            } else if (this._valueSourcePropertyName === 'selected' || this._valueSourcePropertyName === 'backend') {
                // "selected" indicates that this is a value set selector field, and has to be handled by the backend
            } else {
                if (this._valueSourcePropertyName === 'innerHTML') {
                    this._element.innerHTML = value === undefined ? '' : value;
                } else {
                    if (this._type === 'boolean') {
                        this._element.set(this._valueSourcePropertyName, value === true || value === "true");
                    } else if (this._element.tagName.toLowerCase() === 'vaadin-time-picker') {
                        this._element.set(this._valueSourcePropertyName, !value ? '' : value);
                    } else if (this._element.tagName.toLowerCase() === 'vaadin-date-time-picker') {
                        this._element.set(this._valueSourcePropertyName, Uibuilder.FormLocalDate.normalizeDateTime(value));
                    } else {
                        this._element.set(this._valueSourcePropertyName, value);
                    }
                }
            }
            this.errorMessage = null;
            this.valid = true;
        });
    }

    init() {
        this._value = this._form.get('formItem.' + this._itemBind);
        this.reset();
    }

    updateValidity() {
        this._element.invalid = !this._valid;
        this._element.errorMessage = this._errorMessage;
    }

    get valid() {
        return this._valid;
    }

    set valid(value) {
        this._valid = value;
        this.updateValidity();
    }

    get disabled() {
        return this._element.disabled;
    }

    set disabled(disabled) {
        if (this._element.hasAttribute('field-disabled') && !disabled) {
            this._element.disabled = this._element.attributes['field-disabled'];
        } else {
            this._element.disabled = disabled;
        }
    }

    get readonly() {
        return this._element.readonly;
    }

    set readonly(readonly) {
        if (this._element.hasAttribute('field-readonly') && !readonly) {
            this._element.readonly = this._element.attributes['field-readonly'];
        } else {
            this._element.readonly = readonly;
        }
    }

    get errorMessage() {
        return this._errorMessage;
    }

    set errorMessage(value) {
        this._errorMessage = value;
        this.updateValidity();
    }

    get componentId() {
        return this._element && this._element.hasAttribute('id') ?
            this._element.attributes['id'].value :
            null;
    }
};

Uibuilder.FormFieldsController = class {
    constructor() {
        this._formFields = [];
    }

    add(formField) {
        if (formField instanceof Uibuilder.FormField) {
            this._formFields.push(formField);
        } else {
            throw new ErrorEvent('Cannot add a non formField instance');
        }
    }

    remove(formField) {
        if (formField instanceof Uibuilder.FormField) {
            let idx = this._formFields.indexOf(formField);
            if (idx > -1) {
                this._formFields.splice(idx, 1);
                formField._itemBind = null;
            } else {
                throw new ErrorEvent('Form field not found')
            }
        } else {
            throw new ErrorEvent('Cannot remove a non formField instance');
        }
    }

    getByItemBind(itemBind) {
        let res = this._formFields.filter(formField => formField._itemBind === itemBind);
        return res.length > 0 ? res[0] : null;
    }

    getByElement(element) {
        let res = this._formFields.filter(formField => formField._element === element);
        return res.length > 0 ? res[0] : null;
    }

    reset() {
        this._formFields.forEach(formField => formField.reset());
    }

    init() {
        this._formFields.forEach(formField => formField.init());
    }

    updateValidity() {
        this._formFields.forEach(formField => formField.updateValidity());
    }

    set disabled(disabled) {
        this._formFields.forEach(formField => formField.disabled = disabled);
    }

    set readonly(readonly) {
        this._formFields.forEach(formField => formField.readonly = readonly);
    }

    bindings() {
        let b = [];
        this._formFields.forEach(formField => b.push({
            componentId: formField.componentId,
            itemBind: formField._itemBind,
            valueSourcePropertyName: formField._valueSourcePropertyName
        }));
        return b;
    }
};
