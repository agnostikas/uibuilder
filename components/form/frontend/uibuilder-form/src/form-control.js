/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
window.Uibuilder = window.Uibuilder || {};

Uibuilder.FormControl = class {

    constructor(type, disabled = false, readonly = false) {
        this._type = type;
        this._nodes = [];
        this._disabled = disabled;
        this._readonly = readonly;
    }

    get disabled() {
        return this._disabled;
    }

    set disabled(value) {
        this._disabled = value;
        this.updateNodes();
    }

    get readonly() {
        return this._readonly;
    }

    set readonly(value) {
        this._readonly = value;
        this.updateNodes();
    }

    get nodes() {
        return this._nodes;
    }

    set nodes(value) {
        this._nodes = value == null || value === undefined ? [] : value;
        this.updateNodes();
    }

    addNode(node, form) {
        if (!node.hasAttribute('form-control-connected')) {
            node.addEventListener('click', () => this.activated(form));
            node.setAttribute('form-control-connected', true);
            this._nodes.push(node);
            this._updateNode(node);
        } else {
            console.warn("Skipping already added form control element", node);
        }
    }

    removeNode(node) {
        node.removeAttribute('form-control-connected');
        const nodeIdx = this._nodes.indexOf(node);
        if (nodeIdx !== -1) {
            this._nodes.splice(nodeIdx, 1);
        }
    }

    updateNodes() {
        this._nodes.forEach(node => this._updateNode(node));
    }

    _updateNode(node) {
        node.set('disabled', this._disabled || this._readonly);
    }

    activated(form) {
        form[this._type]();
    }
};
