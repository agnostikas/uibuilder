/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import * as PolymerAsync from '@polymer/polymer/lib/utils/async.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import './form-base-mixin.js'


export class UibuilderForm extends ThemableMixin(Uibuilder.FormBaseMixin(PolymerElement)) {

    static get template() {
        return html`
            <style>
                :root {
                    --form-border: 1px solid #E0E0E0;
                }

                :host {
                    display: block;
                }

                [part="content"] {
                    display: flex;
                    flex-direction: column;
                    box-sizing: border-box;
                    height: inherit;
                    width: inherit;
                    margin-top: 18px;
                    padding: 10px;
                    position: relative;
                }

                .no-legend {
                    margin-top: 4px;
                }

                .no-legend [part="legend"] {
                    display: none;
                }

                .no-legend [part="content"] {
                    border: none;
                }

                .uibuilder-form-content-border {
                    border-radius: 10px;
                    background-color: #FAFAFA;
                    border: var(--form-border);
                }

                :host([disabled]) [part="legend"] {
                    background-color: #E8E7E2;
                }

                [part="legend"] {
                    position: absolute;
                    left: 20px;
                    top: -14px;
                    padding: 4px 8px;
                    background-color: #FAFAFA;
                    color: #505050;
                    border: var(--form-border);
                }
            </style>

            <div id="formContent" part="content" class="uibuilder-form-content-border">
                <div part="legend">[[legend]]</div>
                <slot></slot>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-form'
    }

    static get properties() {
        return {
            legend: {
                type: String,
                value: 'Form',
                notify: true,
                observer: '_onLegendChange'
            },
            hideBorder: {
                type: Boolean,
                notify: true,
                value: false,
                observer: '_onBorderVisibleChange'
            },
            typeTimeout: {
                notify: true,
                type: Number,
                value: 300
            },
            formItem: {
                type: Object,
                notify: true
            },
            formItemAssigned: {
                type: Boolean,
                value: false,
                observer: '_onFormItemAssigned'
            },
            valid: {
                type: Boolean,
                notify: true,
                value: true,
                observer: '_onValidPropertyChange'
            },
            _validityDescriptors: {
                type: Array,
                value: [],
                observer: '_onValidityDescriptorsChange'
            },
            disabled: {
                type: Boolean,
                value: false,
                observer: '_onDisabledChange'
            },
            interruptibleReset: {
                type: Boolean,
                value: false
            },
            interruptibleCancel: {
                type: Boolean,
                value: false
            },
            interruptibleSave: {
                type: Boolean,
                value: false
            },
            saveAckRequired: {
                type: Boolean,
                value: false
            },
            readonly: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_onReadonlyChange'
            },
            changedOnlyValidation: {
                type: Boolean,
                value: false,
                notify: true
            }
        };
    }

    constructor() {
        super();
        this._propertyChangedEventEnabled = true;
        this._childForms = [];
        this._typeTimeoutHandles = {};
        this._resetInProgress = false;

        this._errorDialog = null;
    }

    ready() {
        super.ready();
    }

    connectedCallback() {
        super.connectedCallback();

        this._mutationObserver = new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                mutation.addedNodes.forEach(node => {
                    this._scanNode(node);
                });
                mutation.removedNodes.forEach(node => {
                    this._removeNode(node);
                });
            });
            this._fireFormFieldChangeEvent();
        });
        this._mutationObserver.observe(this, {childList: true});
        this._onBorderVisibleChange();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        this._mutationObserver.disconnect();
    }

    markNewItem() {
        this._newItem = true;
        this._onValidityDescriptorsChange();
        this.formControls.SAVE.disabled = !this.valid;
    }

    reset(formItemChange = false) {
        if (this.interruptibleReset && !formItemChange)
            this.dispatchEvent(new CustomEvent("reset", {
                detail: {
                    proceedProcessing: () => this._doReset(formItemChange, false)
                }
            }));
        else {
            this._doReset(formItemChange);
        }
    }

    _doReset(formItemChange, dispatchResetEvent = true) {
        this._resetInProgress = true;
        if (formItemChange) {
            this.formFields.init();
        } else {
            this.formFields.reset();
            if (dispatchResetEvent) {
                this.dispatchEvent(new CustomEvent("reset"));
            }
        }
        this.formControls.RESET.disabled = true;
        this.formControls.SAVE.disabled = this._newItem ? !this.valid : true;
        this._childForms.forEach(childForm => childForm.reset(formItemChange));
        this._resetInProgress = false;
        this.dispatchEvent(new CustomEvent("reset-confirmed"));
    }

    save(continueChangedOnlyValidationSave = false) {
        if (!continueChangedOnlyValidationSave && this.changedOnlyValidation) {
            this.dispatchEvent(new CustomEvent("validate", {
                detail: {
                    changedOnlyValidationSave: true
                }
            }));
        } else {
            if (this.interruptibleSave) {
                if (this.saveAckRequired) {
                    this.dispatchEvent(new CustomEvent("save", {
                        detail: {
                            proceedProcessing: () => {
                            },
                            ackSave: () => this._doSave(false)
                        }
                    }));
                } else {
                    this.dispatchEvent(new CustomEvent("save", {
                        detail: {
                            proceedProcessing: () => this._doSave(false)
                        }
                    }));
                }
            } else {
                if (this.saveAckRequired) {
                    this.dispatchEvent(new CustomEvent("save", {
                        detail: {
                            ackSave: () => this._doSave(false)
                        }
                    }));
                } else {
                    this._doSave();
                }
            }
        }
    }

    _doSave(dispatchSaveEvent = true) {
        if (this.formItem != null) {
            this.formFields.init();
        }
        this.formControls.SAVE.disabled = true;
        this.formControls.RESET.disabled = true;
        if (dispatchSaveEvent) {
            this.dispatchEvent(new CustomEvent("save"));
        }
        this.dispatchEvent(new CustomEvent("save-confirmed"));
    }

    cancel() {
        if (this.interruptibleCancel) {
            this.dispatchEvent(new CustomEvent("cancel", {
                detail: {
                    proceedProcessing: () => this._doCancel(false)
                }
            }));
        } else {
            this._doCancel();
        }
    }

    markDirty() {
        this._fireItemPropertyChangedEvent("", "", "");
    }

    _doCancel(dispatchCancelEvent = true) {
        this._doReset(false, false);
        if (dispatchCancelEvent) {
            this.dispatchEvent(new CustomEvent("cancel"));
        }
    }

    _fireFormFieldChangeEvent() {
        this._scanChildren(this);

        let childFormIds = [];
        this._childForms.forEach(childForm => {
            if (childForm.id) {
                childFormIds.push(childForm.id);
            }
        });

        this.dispatchEvent(new CustomEvent("form-field-change", {
            detail: {
                bindings: this.formFields.bindings(),
                childFormIds: childFormIds
            }
        }));
        this._onReadonlyChange();
    }

    _onLegendChange(legend) {
        if (legend) {
            this.$.formContent.classList.remove('no-legend');
        } else {
            this.$.formContent.classList.add('no-legend');
        }
    }

    static _nodeIsForm(node) {
        return node.tagName
            && window.Uibuilder
            && window.Uibuilder.formContainerTags
            && window.Uibuilder.formContainerTags.has(node.tagName.toLowerCase());
    }

    static _nodeIsSlot(node) {
        return node.tagName && (node.tagName.toLowerCase() === 'slot');
    }

    _scanChildren(node) {
        if (node.hasChildNodes()) {
            node.childNodes.forEach(node => {
                this._scanNode(node);
            });
        }
    }

    _scanNode(node) {
        if (node.attributes) {
            if (node.hasAttribute('item-bind')) {
                this._addFormFieldByNode(node);
            } else if (node.hasAttribute('form-control')) {
                this._addFormControlByNode(node);
            } else if (node.tagName
                && window.Uibuilder
                && window.Uibuilder.mdcTags
                && window.Uibuilder.mdcTags.has(node.tagName.toLowerCase())) {
                this._addEmbeddedMasterDetailController(node);
            }
        }

        if (node.tagName && node.tagName.toLowerCase() === 'error-handler-dialog') {
            this._errorDialog = node;
        }

        if (!UibuilderForm._nodeIsForm(node) && node.hasChildNodes()) {
            this._scanChildren(node);
        }

        if (UibuilderForm._nodeIsSlot(node)) {
            node.assignedNodes().forEach(slotNode => this._scanNode(slotNode, true));
        }
    }

    static _findParentForm(node) {
        let parentForm = node.parentNode;
        while (parentForm) {
            if (parentForm.tagName && window.Uibuilder.formContainerTags.has(parentForm.tagName.toLowerCase())) {
                return parentForm;
            }
            parentForm = parentForm.parentNode;
        }
        return null;
    }

    static _isMdcDetailSelfNested(node) {
        const detailId = node.detail;
        const parentForm = UibuilderForm._findParentForm(node);
        return parentForm && parentForm.id === detailId;
    }

    _addEmbeddedMasterDetailController(node) {
        if (UibuilderForm._isMdcDetailSelfNested(node)) {
            console.warn(`MDC (${node.id}) detail id (${node.detail}) is the parent of the MDC. ` +
                `Passing field value change notification to this MDC has been disabled `);
        } else {
            node.addEventListener('value-changed', e => {
                this._fireItemPropertyChangedEvent("", "", "");
            });
        }
        node.addEventListener('create', e => {
            this.formControls.RESET.disabled = false;
            this.dispatchEvent(new CustomEvent('validate'));
        });
        node.addEventListener('delete', e => {
            this.formControls.RESET.disabled = false;
            this.dispatchEvent(new CustomEvent('validate'));
        });
    }

    _addFormFieldByNode(node) {
        let formField = Uibuilder.FormField.parseNode(this, node);
        if (formField._valueSourcePropertyName === 'items') {
            this.formFields.add(formField);
        } else if (formField._valueSourcePropertyName === 'selected' || formField._valueSourcePropertyName === 'backend') {
            this.formFields.add(formField);
            node.addEventListener('change', e => {
                this._fireItemPropertyChangedEvent(formField._itemBind, null, e.target.value);
                this.formControls.SAVE.disabled = !this.valid;
                this.formControls.RESET.disabled = false;
            });
        } else if (node.tagName.toLowerCase() === UibuilderForm.is) {
            this._childForms.push(node);
            node.addEventListener('form-field-value-change', () => {
                this.formControls.SAVE.disabled = !this.valid;
                this.formControls.RESET.disabled = false;
            });
        } else {
            this.formFields.add(formField);
            if (node._createPropertyObserver) {
                const multiValueNode = node.tagName.toLowerCase() === 'uibuilder-multi-value';
                node._createPropertyObserver(formField._valueSourcePropertyName,
                    (newValue, oldValue) => {
                        let safeNewValue = multiValueNode ? newValue : this._safeStringValue(newValue);
                        let safeOldValue = multiValueNode ? oldValue : this._safeStringValue(oldValue);
                        this._fireItemPropertyChangedEvent(formField._itemBind, safeNewValue, safeOldValue)
                    });
            } else {
                console.warn('Could not set property observer for form field: ' + formField._itemBind);
            }
        }
    }

    _safeStringValue(value) {
        if (value === null || value === undefined) {
            return null;
        } else if (typeof(value) === 'boolean') {
            return value ? 'true' : 'false';
        } else if (typeof (value) !== 'string') {
            return value.toString();
        }
        return value;
    }

    _addFormControlByNode(node) {
        let formControlName = node.attributes['form-control'].value;
        let formControl = this._formControlByName(formControlName);
        if (formControl != null) {
            formControl.addNode(node, this);
        } else {
            throw new Error('unsupported form-control value: ' + formControlName);
        }
    }

    _removeNode(node) {
        if (node.hasAttribute('form-control')) {
            let formControl = this._formControlByName(node.attributes['form-control'].value);
            if (formControl != null) {
                formControl.removeNode(node);
            }
        }
        if (node.hasAttribute('item-bind')) {
            this.formFields.remove(this.formFields.getByElement(node));
            if (node.tagName.toLowerCase() === UibuilderForm.is) {
                let idx = this._childForms.indexOf(node);
                if (idx > -1) {
                    this._childForms.splice(idx, 1);
                }
            }
        }
    }

    _onBorderVisibleChange() {
        if (this.hideBorder) {
            this.$.formContent.classList.remove('uibuilder-form-content-border');
        } else {
            this.$.formContent.classList.add('uibuilder-form-content-border');
        }
    }

    _onFormItemAssigned() {
        this._newItem = false;
        this._propertyChangedEventEnabled = false;
        this.reset(true);
        this._propertyChangedEventEnabled = true;
        this.dispatchEvent(new CustomEvent("form-item-assigned"));
    }

    _onFormReady() {
        this.dispatchEvent(new CustomEvent("form-ready"));
    }

    _onValidPropertyChange() {
        this.formControls.SAVE.disabled = !this.valid;
    }

    _onValidityDescriptorsChange() {

        let multiValueValidityDescriptors = this._validityDescriptors.filter(validityDescriptor => {
            let formField = this.formFields.getByItemBind(validityDescriptor.p);
            return formField && formField._element && formField._element.tagName && formField._element.tagName.toLowerCase() === 'uibuilder-multi-value';
        });
        let singleValueValidityDescriptors = this._validityDescriptors.filter(validityDescriptor => {
            return !multiValueValidityDescriptors.find(descriptor => descriptor === validityDescriptor);
        });

        let multiValuePropertyPaths = multiValueValidityDescriptors.map(descriptor => descriptor.p);
        multiValuePropertyPaths.forEach(multiValuePropertyPath => {
            let formField = this.formFields.getByItemBind(multiValuePropertyPath);
            formField._element.resetInstanceValidity();
        });

        multiValueValidityDescriptors.forEach(validityDescriptor => {
            let formField = this.formFields.getByItemBind(validityDescriptor.p);
            formField._element.applyInstanceValidity(validityDescriptor.s, validityDescriptor.m, validityDescriptor.i);
        });

        let errorDialogMessage = null;
        singleValueValidityDescriptors.forEach(validityDescriptor => {
            let formField = this.formFields.getByItemBind(validityDescriptor.p);
            if (formField != null) {
                formField.errorMessage = validityDescriptor.m ? validityDescriptor.m : null;
                formField.valid = validityDescriptor.v;
            } else {
                if (!validityDescriptor.v) {
                    errorDialogMessage = validityDescriptor.m;
                }
            }
        });

        if (this._errorDialog && errorDialogMessage) {
            const errorMethod = this._errorDialog.getAttribute('listener') || 'handleError';
            this._errorDialog[errorMethod](errorDialogMessage.split('\n').join('<br/>'));
        }
    }

    _showNotification(property, message) {
        let notificationTemplate = document.createElement("template");
        notificationTemplate.innerHTML = '<b>Property: ' + property + '</b>\nMessage: "' + message + '"';

        let notification = document.createElement("vaadin-notification");
        notification.setAttribute("position", "top-end");
        notification.appendChild(notificationTemplate);

        this.appendChild(notification);
        notification.opened = true;
    }

    _fireItemPropertyChangedEvent(propertyPath, newPropertyValue, oldPropertyValue) {
        if (propertyPath != null && this._propertyChangedEventEnabled && this.formItem != null) {
            if (this._resetInProgress) {
                this._dispatchFormFieldValueChange(propertyPath, newPropertyValue, oldPropertyValue);
                this.formControls.RESET.disabled = false;
                this.formControls.SAVE.disabled = !this.valid;
            } else {
                if (this._typeTimeoutHandles[propertyPath]) {
                    PolymerAsync.timeOut.cancel(this._typeTimeoutHandles[propertyPath]);
                }
                this._typeTimeoutHandles[propertyPath] = PolymerAsync.timeOut.run(() => {
                    this._typeTimeoutHandles[propertyPath] = null;
                    this._dispatchFormFieldValueChange(propertyPath, newPropertyValue, oldPropertyValue);
                    this.formControls.SAVE.disabled = !this.valid;
                }, this.typeTimeout);
                this.formControls.RESET.disabled = false;
            }
        }
    }

    _dispatchFormFieldValueChange(propertyPath, newPropertyValue, oldPropertyValue) {
        let eventData = {
            detail: {
                propertyName: propertyPath,
                newPropertyValue: newPropertyValue,
                oldPropertyValue: oldPropertyValue,
                resetInProgress: this._resetInProgress
            }
        };
        if (propertyPath) {
            this.set('formItem.' + propertyPath, newPropertyValue);
        }
        this.dispatchEvent(new CustomEvent("form-field-value-change", eventData));
    }

    _onDisabledChange() {
        this._childForms.forEach(childForm => childForm.disabled = this.disabled);
        this.formFields.disabled = this.disabled;
    }

    _onReadonlyChange() {
        this._childForms.forEach(childForm => childForm.readonly = this.readonly);
        this.formFields.readonly = this.readonly;
        this.formControls.RESET.readonly = this.readonly;
        this.formControls.SAVE.readonly = this.readonly;
        this.formControls.CANCEL.readonly = this.readonly;
    }

    _setFieldReachable(itemBind, reachable) {
        const formField = this.formFields.getByItemBind(itemBind);
        if (formField) {
            if (reachable && formField.disabled) {
                formField.disabled = false;
            } else if (!reachable && !formField.disabled) {
                formField.disabled = true;
            }
        }
    }
}

customElements.define(UibuilderForm.is, UibuilderForm);

window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.formContainerTags = window.Uibuilder.formContainerTags || new Set();
window.Uibuilder.formContainerTags.add(UibuilderForm.is);
