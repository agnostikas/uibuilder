/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.internal.JsonSerializer;
import io.devbench.uibuilder.api.exceptions.ComponentException;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.form.event.FormFieldBinding;
import io.devbench.uibuilder.components.form.event.FormFieldChangeEvent;
import io.devbench.uibuilder.components.form.event.FormResetEvent;
import io.devbench.uibuilder.components.form.event.FormSaveEvent;
import io.devbench.uibuilder.components.form.testutils.FormTestHelper;
import io.devbench.uibuilder.components.form.testutils.component.HasItemsComponent;
import io.devbench.uibuilder.components.form.testutils.component.HasValueComponent;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItemWrongGeneric;
import io.devbench.uibuilder.components.form.testutils.model.TestFormSubItem;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BeanNodeInstanceFactory;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class UIBuilderFormSpecialBindTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @BeforeEach
    void setUp() {
        doReturn(Collections.emptySet()).when(memberScanner).findInstancesBySuperType(BeanNodeInstanceFactory.class);
    }

    @Test
    @DisplayName("should reset hasItem collection on reset event")
    void test_should_reset_has_item_collection_on_reset_event() {
        FormTestHelper.createForms(true);
        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsWithDatas();

        HasItemsComponent component = new HasItemsComponent("comp-id");
        ElementCollector.register(component, new Element("form-test-has-items"));
        bindings.stream().filter(b -> "items".equals(b.getValueSourcePropertyName())).findFirst().get().setComponentId("comp-id");

        UIBuilderForm<TestFormItem> form = UIBuilderFormRegistry.getById("another-form", TestFormItem.class).get();
        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));

        form.setFormItem(FormTestHelper.createItem("Bruce Banner", 35, "the data", "data 1", "data 2"));

        List<TestFormSubItem> componentItemsBefore = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertNotNull(componentItemsBefore),
            () -> assertEquals(2, componentItemsBefore.size()),
            () -> assertEquals("data 1", componentItemsBefore.get(0).getData()),
            () -> assertEquals("data 2", componentItemsBefore.get(1).getData())
        );

        componentItemsBefore.get(1).setData("data MOD");
        componentItemsBefore.remove(0);

        List<TestFormSubItem> componentItemsAfter = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertEquals(1, componentItemsAfter.size()),
            () -> assertEquals("data MOD", componentItemsAfter.get(0).getData())
        );

        ComponentUtil.fireEvent(form, new FormResetEvent(form, false, ""));

        List<TestFormSubItem> componentItemsAfterReset = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertNotNull(componentItemsAfterReset),
            () -> assertEquals(2, componentItemsAfterReset.size()),
            () -> assertEquals("data 1", componentItemsAfterReset.get(0).getData()),
            () -> assertEquals("data 2", componentItemsAfterReset.get(1).getData())
        );


        assertAll(
            () -> assertNotNull(component.getItems(), "Component items should not be null"),
            () -> assertFalse(component.getItems().isEmpty(), "Component items should not be empty collection")
        );

        UIBuilderForm<TestFormItem> spyForm = Mockito.spy(form);
        Mockito.doReturn(Optional.of(component)).when(spyForm).findComponentById(Mockito.eq("comp-id"));

        spyForm.setFormItem(null);

        assertNotNull(component.getItems(), "Component items should not be null");
        assertTrue(component.getItems().isEmpty(), "Component items should be an empty collection");

    }

    @Test
    @DisplayName("should store hasItem collection on save event")
    void test_should_store_has_item_collection_on_save_event() {
        FormTestHelper.createForms(true);
        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsWithDatas();

        HasItemsComponent component = new HasItemsComponent("comp-id");
        ElementCollector.register(component, new Element("form-test-has-items"));
        bindings.stream().filter(b -> "items".equals(b.getValueSourcePropertyName())).findFirst().get().setComponentId("comp-id");

        UIBuilderForm<TestFormItem> form = UIBuilderFormRegistry.getById("another-form", TestFormItem.class).get();
        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));

        form.setFormItem(FormTestHelper.createItem("Bruce Banner", 35, "the data", "data 1", "data 2"));

        List<TestFormSubItem> componentItemsBefore = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertNotNull(componentItemsBefore),
            () -> assertEquals(2, componentItemsBefore.size()),
            () -> assertEquals("data 1", componentItemsBefore.get(0).getData()),
            () -> assertEquals("data 2", componentItemsBefore.get(1).getData())
        );

        componentItemsBefore.get(1).setData("data MOD");
        componentItemsBefore.remove(0);

        List<TestFormSubItem> componentItemsAfter = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertEquals(1, componentItemsAfter.size()),
            () -> assertEquals("data MOD", componentItemsAfter.get(0).getData())
        );

        ComponentUtil.fireEvent(form, new FormSaveEvent(form, false, ""));

        List<TestFormSubItem> componentItemsAfterSave = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertEquals(1, componentItemsAfterSave.size()),
            () -> assertEquals("data MOD", componentItemsAfterSave.get(0).getData()),
            () -> assertEquals(1, form.getFormItem().getDatas().size()),
            () -> assertEquals("data MOD", form.getFormItem().getDatas().get(0).getData())
        );

        ComponentUtil.fireEvent(form, new FormResetEvent(form, false, ""));

        List<TestFormSubItem> componentItemsAfterReset = (List<TestFormSubItem>) component.getItems();

        assertAll(
            () -> assertEquals(1, componentItemsAfterReset.size()),
            () -> assertEquals("data MOD", componentItemsAfterReset.get(0).getData())
        );
    }

    @Test
    @DisplayName("should throw exception when hasItem collection init has no generic type")
    void test_should_throw_exception_when_has_item_collection_init_has_no_generic_type() {
        FormTestHelper.createForms(true);
        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsWithDatas();

        HasItemsComponent component = new HasItemsComponent("comp-id");
        ElementCollector.register(component, new Element("form-test-has-items"));
        bindings.stream().filter(b -> "items".equals(b.getValueSourcePropertyName())).findFirst().get().setComponentId("comp-id");

        UIBuilderForm<TestFormItemWrongGeneric> form = UIBuilderFormRegistry.getById("another-form", TestFormItemWrongGeneric.class).get();
        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));

        TestFormItemWrongGeneric formItem = new TestFormItemWrongGeneric();
        formItem.setName("Bruce banner");
        formItem.setAge(35);
        formItem.setSubItem(FormTestHelper.createSubItem("the data"));
        List<TestFormSubItem> subItems = new ArrayList<>();
        subItems.add(FormTestHelper.createSubItem("data 1"));
        subItems.add(FormTestHelper.createSubItem("data 2"));
        formItem.setDatas(subItems);

        assertThrows(ComponentException.class, () -> form.setFormItem(formItem),
            "Form item should not contain collection without generic type information if binded");
    }

    @Test
    @DisplayName("should reset hasValue object on reset event")
    void test_should_reset_has_value_object_on_reset_event() {
        FormTestHelper.createForms(true);
        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsSubItemSelected();

        HasValueComponent component = new HasValueComponent("comp-id");
        ElementCollector.register(component, new Element("form-test-has-value"));
        bindings.stream().filter(b -> "selected".equals(b.getValueSourcePropertyName())).findFirst().get().setComponentId("comp-id");

        UIBuilderForm<TestFormItem> form = UIBuilderFormRegistry.getById("another-form", TestFormItem.class).get();
        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));

        form.setFormItem(FormTestHelper.createItem("Bruce Banner", 35, "the data"));

        assertAll(
            () -> assertNotNull(component.getValue()),
            () -> assertEquals("the data", ((TestFormSubItem) component.getValue()).getData())
        );

        component.setValue(FormTestHelper.createSubItem("MOD"));
        form.updateHasValueField("subItem", component.getValue());

        assertAll(
            () -> assertNotNull(form.getFormItem().getSubItem()),
            () -> assertEquals("MOD", form.getFormItem().getSubItem().getData())
        );

        ComponentUtil.fireEvent(form, new FormResetEvent(form, false, ""));

        assertAll(
            () -> assertNotNull(component.getValue()),
            () -> assertEquals("the data", ((TestFormSubItem) component.getValue()).getData())
        );

        form.updateHasValueField("subItem", component.getValue());

        assertAll(
            () -> assertNotNull(form.getFormItem().getSubItem()),
            () -> assertEquals("the data", form.getFormItem().getSubItem().getData())
        );


        assertNotNull(component.getValue());

        UIBuilderForm<TestFormItem> spyForm = Mockito.spy(form);
        Mockito.doReturn(Optional.of(component)).when(spyForm).findComponentById(Mockito.eq("comp-id"));

        spyForm.setFormItem(null);

        assertNull(component.getValue());
    }

}
