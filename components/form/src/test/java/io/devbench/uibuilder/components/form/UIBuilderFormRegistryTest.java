/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import io.devbench.uibuilder.components.form.testutils.FormTestHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UIBuilderFormRegistryTest {

    @Test
    @DisplayName("should parse form in proper hierarchy")
    void test_should_parse_form_in_proper_hierarchy() {
        FormTestHelper.createForms(true);

        Optional<UIBuilderForm<Serializable>> subform1 = UIBuilderFormRegistry.getById("subform1");
        Optional<UIBuilderForm<Serializable>> subform2 = UIBuilderFormRegistry.getById("subform2");
        Optional<UIBuilderForm<Serializable>> subform3 = UIBuilderFormRegistry.getById("subform3");
        Optional<UIBuilderForm<Serializable>> subsubForm = UIBuilderFormRegistry.getById("subsub");
        Optional<UIBuilderForm<Serializable>> rootForm = UIBuilderFormRegistry.getById("parent-form");
        Optional<UIBuilderForm<Serializable>> anotherForm = UIBuilderFormRegistry.getById("another-form");
        Optional<UIBuilderForm<Serializable>> wrongForm = UIBuilderFormRegistry.getById("no-form-with-this-name");

        assertAll(
            () -> assertFalse(wrongForm.isPresent()),
            () -> assertTrue(subform1.isPresent()),
            () -> assertTrue(subform2.isPresent()),
            () -> assertTrue(subform3.isPresent()),
            () -> assertTrue(subsubForm.isPresent()),
            () -> assertTrue(rootForm.isPresent()),
            () -> assertTrue(anotherForm.isPresent())
        );

        UIBuilderForm<Serializable> sub1 = subform1.get();
        UIBuilderForm<Serializable> sub2 = subform2.get();
        UIBuilderForm<Serializable> sub3 = subform3.get();
        UIBuilderForm<Serializable> subsub = subsubForm.get();
        UIBuilderForm<Serializable> root = rootForm.get();
        UIBuilderForm<Serializable> another = anotherForm.get();


        assertAll(
            () -> assertFalse(another.hasChildren()),
            () -> assertFalse(sub1.hasChildren()),
            () -> assertFalse(sub3.hasChildren()),
            () -> assertFalse(subsub.hasChildren()),
            () -> assertTrue(root.hasChildren()),
            () -> assertTrue(sub2.hasChildren())
        );

        assertAll(
            () -> assertTrue(root.getChildForms().contains(sub1)),
            () -> assertTrue(root.getChildForms().contains(sub2)),
            () -> assertTrue(root.getChildForms().contains(sub3)),
            () -> assertFalse(root.getChildForms().contains(subsub)),
            () -> assertFalse(root.getChildForms().contains(another))
        );

        assertAll(
            () -> assertTrue(sub2.getChildForms().contains(subsub)),
            () -> assertFalse(sub2.getChildForms().contains(sub1)),
            () -> assertFalse(sub2.getChildForms().contains(sub2)),
            () -> assertFalse(sub2.getChildForms().contains(sub3)),
            () -> assertFalse(sub2.getChildForms().contains(another))
        );

        assertAll(
            () -> assertSame(sub2, subsub.getParentForm()),
            () -> assertNotSame(root, subsub.getParentForm()),
            () -> assertSame(root, sub1.getParentForm()),
            () -> assertSame(root, sub2.getParentForm()),
            () -> assertSame(root, sub3.getParentForm()),
            () -> assertNull(root.getParentForm()),
            () -> assertNull(another.getParentForm())
        );
    }

    @Test
    @DisplayName("class should prevent instantiation")
    void test_class_should_prevent_instantiation() {
        InvocationTargetException e = assertThrows(InvocationTargetException.class, () -> {
            Constructor<UIBuilderFormRegistry> constructor = UIBuilderFormRegistry.class.getDeclaredConstructor();
            assertTrue(Modifier.isPrivate(constructor.getModifiers()));
            constructor.setAccessible(true);
            constructor.newInstance();
        }, "Class should not be able to instantiate");

        assertTrue(e.getCause() instanceof UnsupportedOperationException);
    }

    @Test
    @SuppressWarnings("VariableDeclarationUsageDistance")
    @DisplayName("should find proper top-most form")
    void test_should_find_proper_top_most_form() {
        FormTestHelper.createForms(true);

        Optional<UIBuilderForm<Serializable>> subform1 = UIBuilderFormRegistry.getById("subform1");
        Optional<UIBuilderForm<Serializable>> subform2 = UIBuilderFormRegistry.getById("subform2");
        Optional<UIBuilderForm<Serializable>> subform3 = UIBuilderFormRegistry.getById("subform3");
        Optional<UIBuilderForm<Serializable>> subsubForm = UIBuilderFormRegistry.getById("subsub");
        Optional<UIBuilderForm<Serializable>> rootForm = UIBuilderFormRegistry.getById("parent-form");
        Optional<UIBuilderForm<Serializable>> anotherForm = UIBuilderFormRegistry.getById("another-form");
        Optional<UIBuilderForm<Serializable>> wrongForm = UIBuilderFormRegistry.getById("no-form-with-this-name");

        UIBuilderForm<Serializable> sub1 = subform1.get();
        UIBuilderForm<Serializable> sub2 = subform2.get();
        UIBuilderForm<Serializable> sub3 = subform3.get();
        UIBuilderForm<Serializable> subsub = subsubForm.get();
        UIBuilderForm<Serializable> root = rootForm.get();
        UIBuilderForm<Serializable> another = anotherForm.get();

        assertFalse(wrongForm.isPresent());
        assertSame(root, UIBuilderFormRegistry.findTopMostForm(sub1));
        assertSame(root, UIBuilderFormRegistry.findTopMostForm(sub2));
        assertSame(root, UIBuilderFormRegistry.findTopMostForm(sub3));
        assertSame(root, UIBuilderFormRegistry.findTopMostForm(subsub));
        assertSame(root, UIBuilderFormRegistry.findTopMostForm(root));
        assertSame(another, UIBuilderFormRegistry.findTopMostForm(another));
    }

}
