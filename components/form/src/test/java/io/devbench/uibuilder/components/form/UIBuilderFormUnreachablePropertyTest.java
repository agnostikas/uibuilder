/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.internal.JsonSerializer;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.form.event.FormFieldBinding;
import io.devbench.uibuilder.components.form.event.FormFieldChangeEvent;
import io.devbench.uibuilder.components.form.event.FormItemAssignedEvent;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BeanNodeInstanceFactory;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class UIBuilderFormUnreachablePropertyTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @BeforeEach
    void setUp() {
        doReturn(Collections.emptySet()).when(memberScanner).findInstancesBySuperType(BeanNodeInstanceFactory.class);
    }

    @Test
    @DisplayName("Should call javascript function to set unreachable properties")
    void test_should_call_javascript_function_to_set_unreachable_properties() {
        UIBuilderForm<TestBean> form = new UIBuilderForm<TestBean>() {
            private Element spyedElement;

            @Override
            public Element getElement() {
                if (spyedElement == null) {
                    spyedElement = spy(super.getElement());
                }
                return spyedElement;
            }
        };
        form.onAttached();

        // setting the form bindings

        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()),
            JsonSerializer.toJson(Arrays.asList(
                createFormFieldBinding("text"),
                createFormFieldBinding("detail.data")))));

        // detail is null, thus detail.data is unreachable

        TestBean testBean = new TestBean();
        testBean.setText("sample text");
        testBean.setDetail(null);

        form.setFormItem(testBean);
        ComponentUtil.fireEvent(form, new FormItemAssignedEvent(form, false));

        verify(form.getElement()).callJsFunction(eq("_onFormItemAssigned"));
        verify(form.getElement()).callJsFunction(eq("_setFieldReachable"), eq("detail.data"), eq(false));
        verify(form.getElement()).callJsFunction(eq("_onFormReady"));

        // detail has been created, thus detail.data is no more unreachable

        TestDetail detail = new TestDetail();
        detail.setData("sample data");
        testBean.setDetail(detail);

        form.setFormItem(testBean);

        @SuppressWarnings("unchecked")
        PropertyMetadata<TestDetail> detailDataPropertyMetadata = (PropertyMetadata<TestDetail>) form.getFormItemClassMetadata().property("detail.data").get();
        detailDataPropertyMetadata.setInstance(detail);
        ComponentUtil.fireEvent(form, new FormItemAssignedEvent(form, false));

        verify(form.getElement(), times(2)).callJsFunction(eq("_onFormItemAssigned"));
        verify(form.getElement()).callJsFunction(eq("_setFieldReachable"), eq("detail.data"), eq(true));
        verify(form.getElement(), times(2)).callJsFunction(eq("_onFormReady"));
    }

    @Test
    @DisplayName("Should call javascript function to reset unreachable properties")
    void test_should_call_javascript_function_to_reset_unreachable_properties() {
        UIBuilderForm<TestBean> form = new UIBuilderForm<TestBean>() {
            private Element spyedElement;

            @Override
            public Element getElement() {
                if (spyedElement == null) {
                    spyedElement = spy(super.getElement());
                }
                return spyedElement;
            }
        };
        form.onAttached();

        // setting the form bindings

        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()),
            JsonSerializer.toJson(Arrays.asList(
                createFormFieldBinding("text"),
                createFormFieldBinding("detail.data")))));

        // settings an item to initialize the form and then clear

        form.setFormItem(new TestBean());
        form.setFormItem(null);
        ComponentUtil.fireEvent(form, new FormItemAssignedEvent(form, false));

        verify(form.getElement(), times(2)).callJsFunction(eq("_onFormItemAssigned"));
        verify(form.getElement()).callJsFunction(eq("_setFieldReachable"), eq("detail.data"), eq(true));
        verify(form.getElement()).callJsFunction(eq("_setFieldReachable"), eq("detail.data"), eq(true));
        verify(form.getElement()).callJsFunction(eq("_onFormReady"));
    }

    private FormFieldBinding createFormFieldBinding(String itemBind) {
        FormFieldBinding formFieldBinding = new FormFieldBinding();
        formFieldBinding.setItemBind(itemBind);
        formFieldBinding.setValueSourcePropertyName("value");
        return formFieldBinding;
    }

    @Data
    private static class TestBean implements Serializable {
        private String text;
        private TestDetail detail;
    }

    @Data
    private static class TestDetail implements Serializable {
        private String data;
    }

}
