/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.testutils.component;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.data.binder.HasItems;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Tag("form-test-has-value-has-items")
public class HasValueHasItemsComponent extends HasValueComponent implements HasItems {

    private List<?> items = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    public void setItems(Collection collection) {
        items = (List<?>) collection;
    }

    public Collection getItems() {
        return items;
    }

}
