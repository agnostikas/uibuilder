/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import io.devbench.uibuilder.components.form.testutils.FormTestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UIBuilderFormSetVaadinSessionProviderTest {

    private UIBuilderFormSetVaadinSessionProvider testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderFormSetVaadinSessionProvider();
    }

    @Test
    @DisplayName("test formSet")
    void test_form_set() {
        FormTestHelper.createForms(true);

        Set<UIBuilderForm<?>> formSet = testObj.getFormSet();

        assertAll(
            () -> assertNotNull(formSet),
            () -> assertEquals(0, formSet.size())
        );
        formSet.add(UIBuilderFormRegistry.getById("subform1").get());

        Set<UIBuilderForm<?>> formSet2 = testObj.getFormSet();
        assertAll(
            () -> assertNotNull(formSet2),
            () -> assertEquals(1, formSet2.size())
        );
    }

}
