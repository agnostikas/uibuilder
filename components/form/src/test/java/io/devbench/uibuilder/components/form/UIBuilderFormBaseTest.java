/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.page.PendingJavaScriptResult;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.form.event.*;
import io.devbench.uibuilder.components.form.testutils.FormTestHelper;
import io.devbench.uibuilder.components.form.testutils.component.HasValueComponent;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UIBuilderFormBaseTest {

    @Test
    @DisplayName("asFormControl should create components with propert attributes")
    void test_as_form_control_should_create_components_with_propert_attributes() {
        HasValueComponent comp1 = new HasValueComponent();
        HasValueComponent comp2 = new HasValueComponent();

        Component[] components = UIBuilderFormBase.asFormControl(UIBuilderFormBase.Control.SAVE, comp1, comp2);

        assertAll(
            () -> assertNotNull(components),
            () -> assertEquals(2, components.length),
            () -> assertSame(comp1, components[0]),
            () -> assertSame(comp2, components[1]),
            () -> assertEquals("save", comp1.getElement().getAttribute(UIBuilderFormBase.ATTR_FORM_CONTROL)),
            () -> assertEquals("save", comp2.getElement().getAttribute(UIBuilderFormBase.ATTR_FORM_CONTROL))
        );

    }

    @Test
    @DisplayName("properties should return proper defaults and should be modified properly")
    void test_properties_should_return_proper_defaults_and_should_be_modified_properly() {
        FormTestHelper.createForms();
        UIBuilderForm<TestFormItem> form = UIBuilderFormRegistry.getById("another-form", TestFormItem.class).get();

        assertAll(
            () -> assertEquals("", form.getLegend()),
            () -> assertTrue(form.isValid()),
            () -> assertFalse(form.isBorderHidden()),
            () -> assertEquals(300, form.getTypeTimeout()),
            () -> assertFalse(form.isReadonly()),
            () -> assertFalse(form.isChangedOnlyValidation())
        );


        form.setLegend("test legend");
        form.setValid(true);
        form.setBorderHidden(true);
        form.setTypeTimeout(2000);
        form.setReadonly(true);
        form.setChangedOnlyValidation(true);

        assertAll(
            () -> assertEquals("test legend", form.getElement().getProperty(UIBuilderFormBase.PROP_LEGEND)),
            () -> assertTrue(form.getElement().getProperty(UIBuilderFormBase.PROP_VALID, false)),
            () -> assertTrue(form.getElement().getProperty(UIBuilderFormBase.PROP_HIDE_BORDER, false)),
            () -> assertEquals(2000, form.getElement().getProperty(UIBuilderFormBase.PROP_TYPE_TIMEOUT, 0)),
            () -> assertTrue(form.getElement().getProperty(UIBuilderFormBase.PROP_READONLY, false)),
            () -> assertTrue(form.getElement().getProperty(UIBuilderFormBase.PROP_CHANGED_ONLY_VALIDATION, false))
        );
    }

    @Test
    @DisplayName("invoked js methods should be called")
    void test_invoked_js_methods_should_be_called() {
        FormTestHelper.createForms();
        MockElement mockElement = new MockElement("uibuilder-form");

        UIBuilderForm<TestFormItem> form = new UIBuilderForm<TestFormItem>() {
            @Override
            public Element getElement() {
                return mockElement;
            }
        };

        form.onAttached();
        form.reset();
        form.cancel();
        form.save();

        assertAll(
            () -> assertEquals(4, mockElement.getCalledFunctions().size()),
            () -> assertEquals("_fireFormFieldChangeEvent", mockElement.getCalledFunctions().get(0)),
            () -> assertEquals("reset", mockElement.getCalledFunctions().get(1)),
            () -> assertEquals("cancel", mockElement.getCalledFunctions().get(2)),
            () -> assertEquals("save", mockElement.getCalledFunctions().get(3))
        );
    }

    @Test
    @DisplayName("listeners should be added")
    void test_listeners_should_be_added() {
        FormTestHelper.createForms();

        List addedEventListeners = new ArrayList<>();

        UIBuilderForm<TestFormItem> form = new UIBuilderForm<TestFormItem>() {
            @Override
            protected <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
                addedEventListeners.add(eventType);
                return null;
            }
        };

        form.addFormSaveListener(event -> {
        });
        assertEquals(FormSaveEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));

        form.addFormResetListener(event -> {
        });
        assertEquals(FormResetEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));

        form.addFormCancelListener(event -> {
        });
        assertEquals(FormCancelEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));

        form.addFormFieldValueChangeListener(event -> {
        });
        assertEquals(FormFieldValueChangeEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));

        form.addFormFieldChangeListener(event -> {
        });
        assertEquals(FormFieldChangeEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));

        form.addFormItemAssignedListener(event -> {
        });
        assertEquals(FormItemAssignedEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));

        form.addFormReadyListener(event -> {
        });
        assertEquals(FormReadyEvent.class, addedEventListeners.get(addedEventListeners.size() - 1));
    }

    public static class MockElement extends Element {

        private List<String> calledFunctions = new ArrayList<>();

        MockElement(String tag) {
            super(tag);
        }

        @Override
        public PendingJavaScriptResult callJsFunction(String functionName, Serializable... arguments) {
            calledFunctions.add(functionName);
            return null;
        }

        public List<String> getCalledFunctions() {
            return calledFunctions;
        }
    }

}
