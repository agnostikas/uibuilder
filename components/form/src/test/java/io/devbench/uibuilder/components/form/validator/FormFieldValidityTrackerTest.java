/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.validator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.*;

class FormFieldValidityTrackerTest {

    @Test
    @DisplayName("Should track validity by result set and track")
    void test_should_track_validity_by_result_set_and_track() {
        FormFieldValidityTracker testObj = new FormFieldValidityTracker();
        assertTrue(testObj.isAllValid());

        FormValidatorResult result = FormValidatorResult.create(Collections.emptyList());
        result.getDescriptors().add(PropertyValidityDescriptor.create("validProp"));
        result.getDescriptors().add(PropertyValidityDescriptor.create("invalidProp", "Error 1"));
        result.getDescriptors().add(PropertyValidityDescriptor.create("anotherInvalid", "Error 2"));

        testObj.track(result);
        assertFalse(testObj.isAllValid());

        testObj.track("anotherInvalid", true);
        assertFalse(testObj.isAllValid());

        testObj.track("invalidProp", true);
        assertTrue(testObj.isAllValid());
    }

    @Test
    @DisplayName("Should track validity by result set")
    void test_should_track_validity_by_result_set() {
        FormFieldValidityTracker testObj = new FormFieldValidityTracker();
        assertTrue(testObj.isAllValid());

        FormValidatorResult result = FormValidatorResult.create(Collections.emptyList());
        result.getDescriptors().add(PropertyValidityDescriptor.create("validProp"));
        result.getDescriptors().add(PropertyValidityDescriptor.create("invalidProp", "Error 1"));
        result.getDescriptors().add(PropertyValidityDescriptor.create("anotherInvalid", "Error 2"));

        testObj.track(result);
        assertFalse(testObj.isAllValid());

        result = FormValidatorResult.create(Collections.emptyList());
        result.getDescriptors().add(PropertyValidityDescriptor.create("invalidProp"));
        result.getDescriptors().add(PropertyValidityDescriptor.create("anotherInvalid"));
        testObj.track(result);
        assertTrue(testObj.isAllValid());
    }

}
