/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.testutils;

import javax.validation.*;
import javax.validation.valueextraction.ValueExtractor;
import java.io.InputStream;

public class MockValidatorConfiguration implements Configuration<MockValidatorConfiguration> {

    @Override
    public MockValidatorConfiguration ignoreXmlConfiguration() {
        return null;
    }

    @Override
    public MockValidatorConfiguration messageInterpolator(MessageInterpolator interpolator) {
        return null;
    }

    @Override
    public MockValidatorConfiguration traversableResolver(TraversableResolver resolver) {
        return null;
    }

    @Override
    public MockValidatorConfiguration constraintValidatorFactory(ConstraintValidatorFactory constraintValidatorFactory) {
        return null;
    }

    @Override
    public MockValidatorConfiguration parameterNameProvider(ParameterNameProvider parameterNameProvider) {
        return null;
    }

    @Override
    public MockValidatorConfiguration clockProvider(ClockProvider clockProvider) {
        return null;
    }

    @Override
    public MockValidatorConfiguration addValueExtractor(ValueExtractor<?> extractor) {
        return null;
    }

    @Override
    public MockValidatorConfiguration addMapping(InputStream stream) {
        return null;
    }

    @Override
    public MockValidatorConfiguration addProperty(String name, String value) {
        return null;
    }

    @Override
    public MessageInterpolator getDefaultMessageInterpolator() {
        return null;
    }

    @Override
    public TraversableResolver getDefaultTraversableResolver() {
        return null;
    }

    @Override
    public ConstraintValidatorFactory getDefaultConstraintValidatorFactory() {
        return null;
    }

    @Override
    public ParameterNameProvider getDefaultParameterNameProvider() {
        return null;
    }

    @Override
    public ClockProvider getDefaultClockProvider() {
        return null;
    }

    @Override
    public BootstrapConfiguration getBootstrapConfiguration() {
        return null;
    }

    @Override
    public ValidatorFactory buildValidatorFactory() {
        return new ValidatorFactory() {
            @Override
            public Validator getValidator() {
                return new MockValidator();
            }

            @Override
            public ValidatorContext usingContext() {
                return null;
            }

            @Override
            public MessageInterpolator getMessageInterpolator() {
                return null;
            }

            @Override
            public TraversableResolver getTraversableResolver() {
                return null;
            }

            @Override
            public ConstraintValidatorFactory getConstraintValidatorFactory() {
                return null;
            }

            @Override
            public ParameterNameProvider getParameterNameProvider() {
                return null;
            }

            @Override
            public ClockProvider getClockProvider() {
                return null;
            }

            @Override
            public <T> T unwrap(Class<T> type) {
                return null;
            }

            @Override
            public void close() {

            }
        };
    }
}
