/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.components.HasRawElement;
import io.devbench.uibuilder.api.exceptions.ParserException;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.core.utils.ElementCollector;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FormParseInterceptorTest {

    private FormParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new FormParseInterceptor();
    }

    @Test
    @DisplayName("Element should be applicable")
    void test_element_should_be_applicable() {
        Element validElement = createValidElement();
        boolean applicable = testObj.isApplicable(validElement);

        assertAll(
            () -> assertTrue(applicable)
        );
    }

    @Test
    @DisplayName("Element should not be applicable")
    void test_element_should_not_be_applicable() {
        Element invalidElement = createInvalidElement();
        boolean applicable = testObj.isApplicable(invalidElement);

        assertAll(
            () -> assertFalse(applicable)
        );
    }

    @Test
    @DisplayName("interceptor should always be instantiator")
    void test_interceptor_should_always_be_instantiator() {
        assertAll(
            () -> assertTrue(testObj.isInstantiator(null)),
            () -> assertTrue(testObj.isInstantiator(createInvalidElement())),
            () -> assertTrue(testObj.isInstantiator(createValidElement()))
        );
    }

    @Test
    @DisplayName("should instantiate correct type")
    void test_should_instantiate_correct_type() {
        Component component = testObj.instantiateComponent();

        assertAll(
            () -> assertNotNull(component),
            () -> assertTrue(component instanceof UIBuilderForm)
        );
    }

    @Test
    @DisplayName("intercept should set raw element")
    void test_intercept_should_set_raw_element() {
        Component component = testObj.instantiateComponent();
        Element element = createValidElement();
        testObj.intercept(component, element);

        assertAll(
            () -> assertTrue(component instanceof HasRawElement, "Form must implement the HasRawElement interface"),
            () -> assertNotNull(((HasRawElement) component).getRawElement()),
            () -> assertEquals(element, ((HasRawElement) component).getRawElement())
        );
    }

    @Test
    @DisplayName("intercept should set id, when comp no id, element has id, element without item-bind")
    void test_intercept_should_set_id_when_comp_no_id_element_has_id_element_without_item_bind() {
        Component component = testObj.instantiateComponent();
        Element element = createValidElement();
        testObj.intercept(component, element);

        String id = component.getId().orElseGet(() -> fail("Component (form) should got an ID assigned"));
        assertAll(
            () -> assertNotNull(id),
            () -> assertEquals("test-id", id)
        );
    }

    @Test
    @DisplayName("intercept shoud set id and item-bind, with comp no id, element has id, element with item-bind")
    void test_intercept_shoud_set_id_and_item_bind_with_comp_no_id_element_has_id_element_with_item_bind() {
        Component component = testObj.instantiateComponent();
        Element element = createValidElement("test-id", "formItem:anotherBean");
        testObj.intercept(component, element);

        String id = component.getId().orElseGet(() -> fail("Component (form) should got an ID assigned"));
        String itemBind = ((UIBuilderForm) component).getItemBind();
        assertAll(
            () -> assertNotNull(id),
            () -> assertEquals("test-id", id),
            () -> assertNotNull(itemBind),
            () -> assertEquals("formItem:anotherBean", itemBind)
        );
    }

    @Test
    @DisplayName("intercept should override id, with comp with id and element has different id")
    void test_intercept_should_override_id_with_comp_with_id_and_element_has_different_id() {
        Component component = testObj.instantiateComponent();
        component.setId("diff-id");
        Element element = createValidElement();
        testObj.intercept(component, element);

        String id = component.getId().orElseGet(() -> fail("Component (form) should got an ID assigned"));
        assertAll(
            () -> assertNotNull(id),
            () -> assertEquals("diff-id", id)
        );
    }

    @Test
    @DisplayName("intercept should keep the id, with comp with id and element has same id")
    void test_intercept_should_keep_the_id_with_comp_with_id_and_element_has_same_id() {
        Component component = testObj.instantiateComponent();
        component.setId("test-id");
        Element element = createValidElement();
        testObj.intercept(component, element);

        String id = component.getId().orElseGet(() -> fail("Component (form) should got an ID assigned"));
        assertAll(
            () -> assertNotNull(id),
            () -> assertEquals("test-id", id)
        );
    }

    @Test
    @DisplayName("intercept should throw exception when comp no id, element no id")
    void test_intercept_should_throw_exception_when_comp_no_id_element_no_id() {
        Component component = testObj.instantiateComponent();
        Element element = createValidElement(null, null);
        assertThrows(ParserException.class, () -> testObj.intercept(component, element), "Should throw exception there is no id to set");
    }

    @Test
    @DisplayName("intercept should set initial changed-only-validation if present")
    void test_intercept_should_set_initial_changed_only_validation_if_present() {
        UIBuilderForm<?> form = (UIBuilderForm<?>) testObj.instantiateComponent();
        Element element = createValidElement("test-id", null);
        element.attr("changed-only-validation", true);

        assertFalse(form.isChangedOnlyValidation());
        testObj.intercept(form, element);
        assertTrue(form.isChangedOnlyValidation());
    }

    private Element createValidElement() {
        return createValidElement("test-id", null);
    }

    private Element createValidElement(String id, String itemBind) {
        Element element = new Element(UIBuilderForm.TAG_NAME);
        if (id != null) {
            element.attr(ElementCollector.ID, id);
        }
        if (itemBind != null) {
            element.attr("item-bind", itemBind);
        }
        return element;
    }

    private Element createInvalidElement() {
        Element element = new Element(UIBuilderForm.TAG_NAME + "-invalid");
        return element;
    }

}
