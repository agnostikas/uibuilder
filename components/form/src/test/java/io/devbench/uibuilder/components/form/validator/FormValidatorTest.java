/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.validator;

import com.typesafe.config.Config;
import elemental.json.*;
import io.devbench.uibuilder.components.form.event.FormFieldBinding;
import io.devbench.uibuilder.components.form.exception.FormInvalidValidatorFactoryClassException;
import io.devbench.uibuilder.components.form.testutils.*;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import io.devbench.uibuilder.components.form.testutils.model.TestFormSubItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class FormValidatorTest {

    private FormValidator testObj;

    @BeforeEach
    void setUp() {
        testObj = new FormValidator();
    }

    @Test
    @DisplayName("should return mock validator")
    void test_should_return_mock_validator() {
        ((MockValidator) testObj.getValidator()).getMockViolations().clear();
        Validator validator = testObj.getValidator();

        assertAll(
            () -> assertNotNull(validator),
            () -> assertTrue(validator instanceof MockValidator)
        );
    }

    @Test
    @DisplayName("should return configured validator factory")
    void test_should_return_configured_validator_factory() {
        testObj = spy(testObj);

        Config configMock = mock(Config.class);
        when(testObj.getConfig()).thenReturn(configMock);
        when(configMock.hasPath(eq(FormValidator.FORM_VALIDATOR_FACTORY))).thenReturn(true);
        when(configMock.getString(eq(FormValidator.FORM_VALIDATOR_FACTORY))).thenReturn(MockValidatorFactory.class.getName());

        Validator validator = testObj.getValidator();

        assertNotNull(validator);
        assertTrue(validator instanceof MockValidatorFactory.InternalMockValidator);
    }

    @Test
    @DisplayName("should throw exception if validator factory class is invalid")
    void test_should_throw_exception_if_validator_factory_class_is_invalid() {
        testObj = spy(testObj);

        Config configMock = mock(Config.class);
        when(testObj.getConfig()).thenReturn(configMock);
        when(configMock.hasPath(eq(FormValidator.FORM_VALIDATOR_FACTORY))).thenReturn(true);
        when(configMock.getString(eq(FormValidator.FORM_VALIDATOR_FACTORY))).thenReturn("com.does.not.exist.ValidatorFactory");

        FormInvalidValidatorFactoryClassException exception;

        exception = assertThrows(FormInvalidValidatorFactoryClassException.class, () -> testObj.getValidator(), "Should throw exception");
        assertTrue(exception.getCause() instanceof ClassNotFoundException);

        when(configMock.getString(eq(FormValidator.FORM_VALIDATOR_FACTORY))).thenReturn(MockValidatorFactory.Noninstantiable.class.getName());

        exception = assertThrows(FormInvalidValidatorFactoryClassException.class, () -> testObj.getValidator(), "Should throw exception");
        assertEquals("Could not instantiate validator factory", exception.getMessage());

        when(configMock.getString(eq(FormValidator.FORM_VALIDATOR_FACTORY))).thenReturn(MockPath.class.getName());

        exception = assertThrows(FormInvalidValidatorFactoryClassException.class, () -> testObj.getValidator(), "Should throw exception");
        assertNotNull(exception);
    }

    @Test
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @DisplayName("validator should return proper validity descriptors validating an invalid bean")
    void test_validator_should_return_proper_validity_descriptors_validating_an_invalid_bean() {
        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        TestFormItem item = FormTestHelper.createItem("Bruce Banner", 35, "This is the data");

        Set<ConstraintViolation<?>> mockViolations = ((MockValidator) testObj.getValidator()).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Message for age", "age"));
        mockViolations.add(new MockConstraintViolation<>("Message for subItem", "subItem.data"));
        FormValidatorResult result = testObj.validate(bindings, item);

        Optional<PropertyValidityDescriptor> ageDescriptor = result.getDescriptors()
            .stream().filter(p -> p.getPropertyName().equals("age")).findFirst();

        Optional<PropertyValidityDescriptor> dataDescriptor = result.getDescriptors()
            .stream().filter(p -> p.getPropertyName().equals("subItem.data")).findFirst();

        assertAll(
            () -> assertNotNull(result),
            () -> assertNotNull(result.getDescriptors()),
            () -> assertEquals(3, result.getDescriptors().size()),
            () -> assertFalse(result.isValid()),
            () -> assertTrue(ageDescriptor.isPresent()),
            () -> assertFalse(ageDescriptor.get().isValid()),
            () -> assertNotNull(ageDescriptor.get().getErrorMessage()),
            () -> assertEquals("Message for age", ageDescriptor.get().getErrorMessage()),
            () -> assertTrue(dataDescriptor.isPresent()),
            () -> assertFalse(dataDescriptor.get().isValid()),
            () -> assertNotNull(dataDescriptor.get().getErrorMessage()),
            () -> assertEquals("Message for subItem", dataDescriptor.get().getErrorMessage())
        );
    }

    @Test
    @DisplayName("should return form validator result even if bindings is null")
    void test_bindings_is_null() {
        TestFormItem item = FormTestHelper.createItem("Bruce Banner", 35, "This is the data");
        FormValidatorResult result = testObj.validate(null, item);

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result.isValid()),
            () -> assertNotNull(result.getDescriptors()),
            () -> assertTrue(result.getDescriptors().isEmpty())
        );
    }

    @Test
    @DisplayName("should return form validator result even if bindings is empty")
    void test_should_return_form_validator_result_even_if_bindings_is_empty() {
        Set<FormFieldBinding> bindings = Collections.emptySet();
        TestFormItem item = FormTestHelper.createItem("Bruce Banner", 35, "This is the data");
        FormValidatorResult result = testObj.validate(bindings, item);

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result.isValid()),
            () -> assertNotNull(result.getDescriptors()),
            () -> assertTrue(result.getDescriptors().isEmpty())
        );
    }

    @Test
    @DisplayName("should return form validator result even if instance is null")
    void test_should_return_form_validator_result_even_if_instance_is_null() {
        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        FormValidatorResult result = testObj.validate(bindings, null);

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result.isValid()),
            () -> assertNotNull(result.getDescriptors()),
            () -> assertTrue(result.getDescriptors().isEmpty())
        );
    }

    @Test
    @DisplayName("validator should return proper validity descriptors validating a valid bean")
    void test_validator_should_return_proper_validity_descriptors_validating_a_valid_bean() {
        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        TestFormItem item = FormTestHelper.createItem("Bruce Banner", 35, "This is the data");

        ((MockValidator) testObj.getValidator()).getMockViolations().clear();
        testObj.validate(bindings, item);

        FormValidatorResult result = testObj.validate(bindings, item);

        assertAll(
            () -> assertNotNull(result),
            () -> assertNotNull(result.getDescriptors()),
            () -> assertEquals(3, result.getDescriptors().size()),
            () -> assertTrue(result.isValid())
        );
    }

    @Test
    @DisplayName("validator result should generate proper json array with invalid bean")
    void test_validator_result_should_generate_proper_json_array_with_invalid_bean() {
        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        TestFormItem item = FormTestHelper.createItem("Bruce Banner", 35, "This is the data");

        Set<ConstraintViolation<?>> mockViolations = ((MockValidator) testObj.getValidator()).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Message for age", "age"));
        mockViolations.add(new MockConstraintViolation<>("Message for subItem", "subItem.data"));
        FormValidatorResult result = testObj.validate(bindings, item);

        assertNotNull(result);

        JsonArray jsonArray = result.toJson();

        assertAll(
            () -> assertNotNull(jsonArray),
            () -> assertEquals(3, jsonArray.length())
        );

        for (int i = 0; i < jsonArray.length(); i++) {
            JsonValue value = jsonArray.get(i);
            assertTrue(value instanceof JsonObject);
            JsonObject o = (JsonObject) value;
            assertTrue(o.hasKey("p"));
            assertTrue(o.hasKey("v"));

            JsonValue p = o.get("p");
            assertTrue(p instanceof JsonString);

            JsonValue v = o.get("v");
            assertTrue(v instanceof JsonBoolean);

            if (p.asString().equals("age")) {
                assertFalse(v.asBoolean());
                assertTrue(o.hasKey("m"));
                JsonValue m = o.get("m");
                assertTrue(m instanceof JsonString);
                assertEquals("Message for age", m.asString());
            } else if (p.asString().equals("subItem.data")) {
                assertFalse(v.asBoolean());
                assertTrue(o.hasKey("m"));
                JsonValue m = o.get("m");
                assertTrue(m instanceof JsonString);
                assertEquals("Message for subItem", m.asString());
            } else {
                assertTrue(v.asBoolean());
            }
        }
    }

    @Test
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @DisplayName("Should return result with error message without binding")
    void test_should_return_result_with_error_message_without_binding() {
        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        TestFormItem item = FormTestHelper.createItem("Bruce Banner", 35, "This is the data");

        Set<ConstraintViolation<?>> mockViolations = ((MockValidator) testObj.getValidator()).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Message for complex validation", ""));

        FormValidatorResult result = testObj.validate(bindings, item);
        Optional<PropertyValidityDescriptor> descriptor = result.getDescriptors()
            .stream().filter(p -> p.getPropertyName().equals("")).findFirst();

        assertAll(
            () -> assertNotNull(result),
            () -> assertNotNull(result.getDescriptors()),
            () -> assertEquals(4, result.getDescriptors().size()),
            () -> assertFalse(result.isValid()),
            () -> assertTrue(descriptor.isPresent()),
            () -> assertFalse(descriptor.get().isValid()),
            () -> assertNotNull(descriptor.get().getErrorMessage()),
            () -> assertEquals("", descriptor.get().getPropertyName()),
            () -> assertEquals("Message for complex validation", descriptor.get().getErrorMessage())
        );
    }

    @Test
    @DisplayName("validator should handle collections and create an appropriate result")
    void test_validator_should_handle_collections_and_create_an_appropriate_result() {
        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsWithNameAndDatasAndNums();
        TestFormItem item = new TestFormItem("Some Name", 0, null, Arrays.asList(
            new TestFormSubItem("data 1"),
            new TestFormSubItem(""),
            new TestFormSubItem("data 3")
        ), Arrays.asList(30, 50, 101, 99));

        Set<ConstraintViolation<?>> mockViolations = ((MockValidator) testObj.getValidator()).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Message for name", "name"));
        mockViolations.add(new MockConstraintViolation<>("Message for nums", "nums[2].<list item>"));
        mockViolations.add(new MockConstraintViolation<>("Message for datas", "datas[1].data"));

        FormValidatorResult result = testObj.validate(bindings, item);

        assertNotNull(result);
        assertNotNull(result.getDescriptors());
        assertEquals(3, result.getDescriptors().size());
        Optional<PropertyValidityDescriptor> nameDescriptor = result.getDescriptors().stream().filter(d -> "name".equals(d.getPropertyName())).findFirst();
        Optional<PropertyValidityDescriptor> numsDescriptor = result.getDescriptors().stream().filter(d -> "nums".equals(d.getPropertyName())).findFirst();
        Optional<PropertyValidityDescriptor> datasDescriptor = result.getDescriptors().stream().filter(d -> "datas".equals(d.getPropertyName())).findFirst();
        assertTrue(nameDescriptor.isPresent());
        assertTrue(numsDescriptor.isPresent());
        assertTrue(datasDescriptor.isPresent());
        PropertyValidityDescriptor nameDesc = nameDescriptor.get();
        PropertyValidityDescriptor numsDesc = numsDescriptor.get();
        PropertyValidityDescriptor datasDesc = datasDescriptor.get();

        assertNull(nameDesc.getSubPropertyPath());
        assertEquals(0, nameDesc.getIndex());
        assertFalse(nameDesc.isCollection());

        assertNull(numsDesc.getSubPropertyPath());
        assertEquals(2, numsDesc.getIndex());
        assertTrue(numsDesc.isCollection());

        assertEquals("data", datasDesc.getSubPropertyPath());
        assertEquals(1, datasDesc.getIndex());
        assertTrue(datasDesc.isCollection());

        JsonArray array = result.toJson();

        assertNotNull(array);
        assertEquals(3, array.length());

        List<JsonObject> objects = Arrays.asList(
            array.getObject(0),
            array.getObject(1),
            array.getObject(2));

        Optional<JsonObject> foundNameObject = objects.stream().filter(object -> "name".equals(object.getString("p"))).findFirst();
        Optional<JsonObject> foundNumsObject = objects.stream().filter(object -> "nums".equals(object.getString("p"))).findFirst();
        Optional<JsonObject> foundDatasObject = objects.stream().filter(object -> "datas".equals(object.getString("p"))).findFirst();
        assertTrue(foundNameObject.isPresent());
        assertTrue(foundNumsObject.isPresent());
        assertTrue(foundDatasObject.isPresent());
        JsonObject nameObject = foundNameObject.get();
        JsonObject numsObject = foundNumsObject.get();
        JsonObject datasObject = foundDatasObject.get();

        assertFalse(nameObject.hasKey("i"));
        assertFalse(nameObject.hasKey("s"));

        assertTrue(numsObject.hasKey("i"));
        assertFalse(numsObject.hasKey("s"));
        assertEquals(2, numsObject.getNumber("i"));

        assertTrue(datasObject.hasKey("i"));
        assertTrue(datasObject.hasKey("s"));
        assertEquals(1, datasObject.getNumber("i"));
        assertEquals("data", datasObject.getString("s"));

    }

}
