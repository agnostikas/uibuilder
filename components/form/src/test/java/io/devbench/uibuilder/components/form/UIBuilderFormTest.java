/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.internal.JsonSerializer;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.exceptions.ComponentException;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.components.form.event.FormFieldBinding;
import io.devbench.uibuilder.components.form.event.FormFieldChangeEvent;
import io.devbench.uibuilder.components.form.event.FormFieldValueChangeEvent;
import io.devbench.uibuilder.components.form.event.FormItemAssignedEvent;
import io.devbench.uibuilder.components.form.exception.FormCollectionSpecialBindException;
import io.devbench.uibuilder.components.form.testutils.FormTestHelper;
import io.devbench.uibuilder.components.form.testutils.MockConstraintViolation;
import io.devbench.uibuilder.components.form.testutils.MockValidator;
import io.devbench.uibuilder.components.form.testutils.model.TestFormItem;
import io.devbench.uibuilder.components.form.testutils.model.TestFormSubItem;
import io.devbench.uibuilder.components.form.validator.CustomFormValidator;
import io.devbench.uibuilder.components.form.validator.FormValidator;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import javax.validation.ConstraintViolation;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class UIBuilderFormTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @BeforeEach
    void setUp() {
        Mockito.doReturn(Collections.emptySet()).when(memberScanner).findClassesBySuperType(TestFormItem.class);
        Mockito.doReturn(Collections.emptySet()).when(memberScanner).findClassesBySuperType(TestFormSubItem.class);
    }

    @Test
    @DisplayName("form item statenode should be null after clear")
    void test_form_item_statenode_should_be_null_after_clear() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));
        subform2.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));
        Serializable formItemStateNode = subform2.getElement().getNode().getFeature(ElementPropertyMap.class).getProperty(UIBuilderFormBase.PROP_FORM_ITEM);
        subform2.setFormItem(null);
        Serializable formItemStateNodeAfter = subform2
            .getElement()
            .getNode()
            .getFeature(ElementPropertyMap.class)
            .getProperty(UIBuilderFormBase.PROP_FORM_ITEM);

        assertAll(
            () -> assertNotNull(formItemStateNode),
            () -> assertTrue(formItemStateNode instanceof StateNode),
            () -> assertNull(formItemStateNodeAfter)
        );
    }

    @Test
    @DisplayName("form item statenode should be another instance after reinitialized")
    void test_form_item_statenode_should_be_another_instance_after_reinitialized() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));
        subform2.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));

        Serializable formItemStateNode = subform2.getElement().getNode().getFeature(ElementPropertyMap.class).getProperty(UIBuilderFormBase.PROP_FORM_ITEM);
        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));
        Serializable formItemStateNodeAfter = subform2
            .getElement()
            .getNode()
            .getFeature(ElementPropertyMap.class)
            .getProperty(UIBuilderFormBase.PROP_FORM_ITEM);

        assertAll(
            () -> assertNotNull(formItemStateNode),
            () -> assertTrue(formItemStateNode instanceof StateNode),
            () -> assertNotNull(formItemStateNodeAfter),
            () -> assertTrue(formItemStateNodeAfter instanceof StateNode),
            () -> assertNotSame(formItemStateNodeAfter, formItemStateNode)
        );
    }

    @Test
    @DisplayName("form item bean field value should change after value change event")
    void test_form_item_bean_field_value_should_change_after_value_change_event() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));
        subform2.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));

        StateNode formItemStateNode =
            (StateNode) subform2.getElement().getNode().getFeature(ElementPropertyMap.class).getProperty(UIBuilderFormBase.PROP_FORM_ITEM);
        formItemStateNode.getFeature(ElementPropertyMap.class).setProperty("name", "New Name");

        ComponentUtil.fireEvent(
            subform2,
            new FormFieldValueChangeEvent(subform2, false, "name", Json.create("New Name"), Json.create("test name"), false)
        );

        assertEquals("New Name", subform2.getFormItem().getName());
    }

    @Test
    @DisplayName("form item bean field value should change after value change event targeted a collection")
    void test_form_item_bean_field_value_should_change_after_value_change_event_targeted_a_collection() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = new HashSet<>();
        FormFieldBinding binding = new FormFieldBinding();
        binding.setItemBind("nums");
        bindings.add(binding);

        UIBuilderForm<TestFormItem> form = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        ComponentUtil.fireEvent(form,
            new FormFieldChangeEvent(form, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));
        form.setFormItem(new TestFormItem("test name", 40, null, null, Arrays.asList(10, 20, 30)));

        JsonArray array = Json.createArray();
        array.set(0, "15");
        array.set(1, "25");
        array.set(2, "35");
        array.set(3, "45");

        StateNode formItemStateNode =
            (StateNode) form.getElement().getNode().getFeature(ElementPropertyMap.class).getProperty(UIBuilderFormBase.PROP_FORM_ITEM);
        formItemStateNode.getFeature(ElementPropertyMap.class).setProperty("nums", array);

        ComponentUtil.fireEvent(form,
            new FormFieldValueChangeEvent(form, false, "nums", array, null, false));


        List<Integer> modifiedNums = form.getFormItem().getNums();
        assertNotNull(modifiedNums);
        assertEquals(4, modifiedNums.size());
        assertEquals(15, modifiedNums.get(0));
        assertEquals(25, modifiedNums.get(1));
        assertEquals(35, modifiedNums.get(2));
        assertEquals(45, modifiedNums.get(3));
    }

    @Test
    @DisplayName("form item bean value by multi level path should change after value change event")
    void test_form_item_bean_value_by_multi_level_path_should_change_after_value_change_event() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(bindings)));
        subform2.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));

        StateNode formItemStateNode =
            (StateNode) subform2.getElement().getNode().getFeature(ElementPropertyMap.class).getProperty(UIBuilderFormBase.PROP_FORM_ITEM);
        StateNode subItemStateNode =
            (StateNode) formItemStateNode.getFeature(ElementPropertyMap.class).getProperty("subItem");

        subItemStateNode.getFeature(ElementPropertyMap.class).setProperty("data", "New Data");

        ComponentUtil.fireEvent(
            subform2,
            new FormFieldValueChangeEvent(subform2, false, "subItem.data", Json.create("New Data"), Json.create("the data"), false)
        );

        assertEquals("New Data", subform2.getFormItem().getSubItem().getData());
    }

    @Test
    @DisplayName("subform should be initialized on form item assigned event")
    void test_subform_should_be_initialized_on_form_item_assigned_event() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        Set<FormFieldBinding> subBindings = FormTestHelper.createBindingsNestedFormSub();
        List<String> childFormIds = FormTestHelper.createChildFormList("subsub");

        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        UIBuilderForm<TestFormSubItem> subsub = UIBuilderFormRegistry.getById("subsub", TestFormSubItem.class).get();
        subsub.setItemBind("formItem:subItem");

        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(childFormIds), JsonSerializer.toJson(bindings)));
        ComponentUtil.fireEvent(subsub,
            new FormFieldChangeEvent(subsub, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(subBindings)));

        subform2.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));

        ComponentUtil.fireEvent(subform2, new FormItemAssignedEvent(subform2, false));

        TestFormSubItem subFormItem = subsub.getFormItem();
        assertAll(
            () -> assertNotNull(subFormItem),
            () -> assertEquals("the data", subFormItem.getData())
        );
    }

    @Test
    @DisplayName("subform should be initialized on direct getFormItem call")
    void test_subform_should_be_initialized_on_direct_get_form_item_call() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        Set<FormFieldBinding> subBindings = FormTestHelper.createBindingsNestedFormSub();
        List<String> childFormIds = FormTestHelper.createChildFormList("subsub");

        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        UIBuilderForm<TestFormSubItem> subsub = UIBuilderFormRegistry.getById("subsub", TestFormSubItem.class).get();
        subsub.setItemBind("formItem:subItem");

        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(childFormIds), JsonSerializer.toJson(bindings)));
        ComponentUtil.fireEvent(subsub,
            new FormFieldChangeEvent(subsub, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(subBindings)));

        TestFormItem item = FormTestHelper.createItem("test name", 55, "the data");
        subform2.setFormItem(item);

        TestFormSubItem subItem = subsub.getFormItem();

        assertAll(
            () -> assertNotNull(subItem),
            () -> assertSame(subItem, item.getSubItem())
        );
    }

    @Test
    @DisplayName("form should set the formitem before received bindings")
    void test_form_should_set_the_formitem_before_received_bindings() {
        FormTestHelper.createForms(true);
        UIBuilderForm<TestFormSubItem> form = UIBuilderFormRegistry.getById("subsub", TestFormSubItem.class).get();
        form = spy(form);
        Element element = mock(Element.class, RETURNS_DEEP_STUBS);
        doReturn(element).when(form).getElement();

        TestFormSubItem item = new TestFormSubItem();
        item.setData("the data");

        assertNull(form.getFormItemClassMetadata());

        form.setFormItem(item);

        assertNotNull(form.getFormItemClassMetadata());
        verify(element, never()).callJsFunction("_onFormItemAssigned");
    }

    @Test
    @DisplayName("form should send state node when binding is received and if formitem is set")
    void test_form_should_send_state_node_when_binding_is_received_and_if_formitem_is_set() {
        Element element = mock(Element.class, RETURNS_DEEP_STUBS);
        StateNode node = mock(StateNode.class);
        ElementPropertyMap elementPropertyMap = mock(ElementPropertyMap.class);
        doReturn(node).when(element).getNode();
        doReturn(elementPropertyMap).when(node).getFeature(ElementPropertyMap.class);

        UIBuilderForm<TestFormSubItem> form = new UIBuilderForm<TestFormSubItem>() {
            @Override
            public Element getElement() {
                return element;
            }
        };
        form.onAttached();

        TestFormSubItem item = new TestFormSubItem();
        item.setData("the data");
        form.setFormItem(item);

        assertEquals(item, form.getFormItem());
        assertNotNull(form.getFormItemClassMetadata());

        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, false,
            JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(FormTestHelper.createBindingsNestedFormSub())));

        verify(element).callJsFunction("_onFormItemAssigned");
        verify(elementPropertyMap).setProperty(eq(UIBuilderFormBase.PROP_FORM_ITEM), any(StateNode.class));
    }


    @Test
    @DisplayName("validation on the form and subforms should be made on form item assigned event")
    void test_validation_on_the_form_and_subforms_should_be_made_on_form_item_assigned_event() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        Set<FormFieldBinding> subBindings = FormTestHelper.createBindingsNestedFormSub();
        List<String> childFormIds = FormTestHelper.createChildFormList("subsub");

        UIBuilderForm<TestFormItem> subform2 = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        UIBuilderForm<TestFormSubItem> subsub = UIBuilderFormRegistry.getById("subsub", TestFormSubItem.class).get();
        subsub.setItemBind("formItem:subItem");

        ComponentUtil.fireEvent(subform2,
            new FormFieldChangeEvent(subform2, false, JsonSerializer.toJson(childFormIds), JsonSerializer.toJson(bindings)));
        ComponentUtil.fireEvent(subsub,
            new FormFieldChangeEvent(subsub, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(subBindings)));

        subform2.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));

        Set<ConstraintViolation<?>> mockViolations = getMockValidator(subform2).getMockViolations();
        mockViolations.clear();
        mockViolations = getMockValidator(UIBuilderFormRegistry.getById("parent-form").get()).getMockViolations();
        mockViolations.clear();
        mockViolations = getMockValidator(subsub).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation("Data is not valid", "data"));

        ComponentUtil.fireEvent(subform2, new FormItemAssignedEvent(subform2, false));

        boolean valid = subform2.validate();
        boolean subsubValid = subsub.isValid();
        Serializable parentPropValidityDescriptors = subform2.getElement().getPropertyRaw(UIBuilderFormBase.PROP_VALIDITY_DESCRIPTORS);
        Serializable subPropValidityDescriptors = subsub.getElement().getPropertyRaw(UIBuilderFormBase.PROP_VALIDITY_DESCRIPTORS);

        assertAll(
            () -> assertFalse(valid, "Form should not be valid due to mocked errors"),
            () -> assertFalse(subsubValid, "Sub form should not be valid due to mocked errors"),
            () -> assertNotNull(parentPropValidityDescriptors, "Form validation descriptors should not be null"),
            () -> assertTrue(parentPropValidityDescriptors instanceof JsonArray, "Form validation descriptors should be JsonArray"),
            () -> assertNotNull(subPropValidityDescriptors, "Sub from property validation descriptors should not be null"));

        assertAll(
            () -> assertEquals(2, ((JsonArray) parentPropValidityDescriptors).length()),
            () -> assertTrue(((JsonArray) parentPropValidityDescriptors).getObject(0).getBoolean("v")),
            () -> assertTrue(((JsonArray) parentPropValidityDescriptors).getObject(1).getBoolean("v")),
            () -> assertFalse(((JsonArray) parentPropValidityDescriptors).getObject(0).hasKey("m")),
            () -> assertFalse(((JsonArray) parentPropValidityDescriptors).getObject(1).hasKey("m"))
        );
    }

    @Test
    @DisplayName("child form validation result must affect the parent form valid status")
    void test_child_form_validation_result_must_affect_the_parent_form_valid_status() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        Set<FormFieldBinding> subBindings = FormTestHelper.createBindingsNestedFormSub();
        List<String> childFormIds = FormTestHelper.createChildFormList("subsub");

        UIBuilderForm<TestFormItem> parentForm = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        UIBuilderForm<TestFormSubItem> childForm = UIBuilderFormRegistry.getById("subsub", TestFormSubItem.class).get();
        childForm.setItemBind("formItem:subItem");

        ComponentUtil.fireEvent(parentForm,
            new FormFieldChangeEvent(parentForm, false, JsonSerializer.toJson(childFormIds), JsonSerializer.toJson(bindings)));
        ComponentUtil.fireEvent(childForm,
            new FormFieldChangeEvent(childForm, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(subBindings)));

        parentForm.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));
        ComponentUtil.fireEvent(parentForm, new FormItemAssignedEvent(parentForm, false));

        assertTrue(parentForm.isValid());
        assertTrue(childForm.isValid());

        TestFormSubItem childFormItem = childForm.getFormItem();
        assertNotNull(childFormItem);

        childFormItem.setData("");
        Set<ConstraintViolation<?>> mockViolations = getMockValidator(childForm).getMockViolations();
        mockViolations.add(new MockConstraintViolation<>("Data cannot be empty", "data"));
        childForm.validate();

        assertFalse(parentForm.isValid());
        assertFalse(childForm.isValid());
    }

    @Test
    @DisplayName("validation on parent must return false if there is at least on invalid child form")
    void test_validation_on_parent_must_return_false_if_there_is_at_least_on_invalid_child_form() {
        FormTestHelper.createForms(true);

        Set<FormFieldBinding> bindings = FormTestHelper.createBindingsNestedForm();
        Set<FormFieldBinding> subBindings = FormTestHelper.createBindingsNestedFormSub();
        List<String> childFormIds = FormTestHelper.createChildFormList("subsub");

        UIBuilderForm<TestFormItem> parentForm = UIBuilderFormRegistry.getById("subform2", TestFormItem.class).get();
        UIBuilderForm<TestFormSubItem> childForm = UIBuilderFormRegistry.getById("subsub", TestFormSubItem.class).get();
        childForm.setItemBind("formItem:subItem");

        ComponentUtil.fireEvent(parentForm,
            new FormFieldChangeEvent(parentForm, false, JsonSerializer.toJson(childFormIds), JsonSerializer.toJson(bindings)));
        ComponentUtil.fireEvent(childForm,
            new FormFieldChangeEvent(childForm, false, JsonSerializer.toJson(Collections.emptyList()), JsonSerializer.toJson(subBindings)));

        parentForm.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));
        ComponentUtil.fireEvent(parentForm, new FormItemAssignedEvent(parentForm, false));

        assertTrue(parentForm.isValid());
        assertTrue(childForm.isValid());

        childForm.setValid(false);
        parentForm.validate();

        assertFalse(parentForm.isValid());
        assertFalse(childForm.isValid());
    }

    @Test
    @DisplayName("custom validation must set the form invalid if at least one return false")
    void test_custom_validation_must_set_the_form_invalid_if_at_least_one_return_false() {
        FormTestHelper.createForms(true);

        @SuppressWarnings("OptionalGetWithoutIsPresent")
        UIBuilderForm<TestFormItem> parentForm = UIBuilderFormRegistry.getById("parent-form", TestFormItem.class).get();

        Set<FormFieldBinding> bindings = FormTestHelper.createBindings();
        ComponentUtil.fireEvent(parentForm,
            new FormFieldChangeEvent(parentForm, false,
                JsonSerializer.toJson(FormTestHelper.createChildFormList()),
                JsonSerializer.toJson(bindings)));

        Set<ConstraintViolation<?>> mockViolations = Objects.requireNonNull(getMockValidator(parentForm)).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Data is not valid", "data"));

        parentForm.setValid(false);
        parentForm.setFormItem(FormTestHelper.createItem("test name", 55, "the data"));
        ComponentUtil.fireEvent(parentForm, new FormItemAssignedEvent(parentForm, false));

        assertFalse(parentForm.validate());

        mockViolations.clear();
        assertTrue(parentForm.validate());

        AtomicBoolean customValidatorExecuted = new AtomicBoolean(false);

        parentForm.addCustomValidator(() -> {
            customValidatorExecuted.set(true);
            return CustomFormValidator.Result.builder().valid(true).build();
        });
        assertTrue(parentForm.validate());
        assertTrue(customValidatorExecuted.get());

        HasValidation component = mock(HasValidation.class);

        CustomFormValidatorRegistration registration = parentForm
            .addCustomValidator(() -> CustomFormValidator.Result.builder().valid(false).errorMessage("Sample error").build())
            .apply(component);
        assertFalse(parentForm.validate());

        verify(component).setInvalid(eq(true));
        verify(component).setErrorMessage(eq("Sample error"));

        registration.remove();
        assertTrue(parentForm.validate());
    }

    @Test
    @DisplayName("ensure collection should return the same collection")
    void test_ensure_collection_should_return_the_same_collection() {
        List<String> list = new ArrayList<>();

        Collection<Object> result = UIBuilderForm.ensureCollection(list, List.class);

        assertSame(list, result);
    }

    @Test
    @DisplayName("ensure collection should throw exception if given object is not assignable from given type")
    void test_ensure_collection_should_throw_exception_if_given_object_is_not_assignable_from_given_type() {
        List<String> list = new ArrayList<>();
        assertThrows(ComponentException.class, () -> UIBuilderForm.ensureCollection(list, Set.class), "Should throw exception");
    }

    @Test
    @DisplayName("ensure collection should return an empty list")
    void test_ensure_collection_should_return_an_empty_list() {
        Collection<String> result = UIBuilderForm.ensureCollection(null, Set.class);
        assertAll(
            () -> assertNotNull(result, "Collection cannot be null"),
            () -> assertTrue(result instanceof Set, "Collection should be a Set"),
            () -> assertTrue(result.isEmpty(), "Collection should be empty")
        );
    }

    @Test
    @DisplayName("ensure collection should return an empty set")
    void test_ensure_collection_should_return_an_empty_set() {
        Collection<String> result = UIBuilderForm.ensureCollection(null, List.class);
        assertAll(
            () -> assertNotNull(result, "Collection cannot be null"),
            () -> assertTrue(result instanceof List, "Collection should be a List"),
            () -> assertTrue(result.isEmpty(), "Collection should be empty")
        );
    }

    @Test
    @DisplayName("ensure collection should throw exception if type is unsupported")
    void test_ensure_collection_should_throw_exception_if_type_is_unsupported() {
        Assertions.assertThrows(
            FormCollectionSpecialBindException.class,
            () -> UIBuilderForm.ensureCollection(null, Map.class), "Should throw exception"
        );
    }

    @Test
    @DisplayName("should call save on frontend when receiving validation event with changedOnlyValidationSave flag")
    void test_should_call_save_on_frontend_when_receiving_validation_event_with_changed_only_validation_save_flag() {
        UIBuilderForm<?> form = spy(new UIBuilderForm<>());
        Element formElement = spy(form.getElement());
        when(form.getElement()).thenReturn(formElement);

        JsonObject eventData = Json.createObject();
        eventData.put(UIBuilderForm.CHANGED_ONLY_VALIDATION_SAVE, true);
        form.validate(new DomEvent(form.getElement(), "validate", eventData));

        verify(formElement).callJsFunction(eq("save"), eq(true));
    }

    @Test
    @DisplayName("should send validation result only for changed property")
    void test_should_send_validation_result_only_for_changed_property() {
        UIBuilderForm<ValidationTestItem> form = new UIBuilderForm<>();
        form = spy(form);
        Element formElement = spy(form.getElement());
        doReturn(formElement).when(form).getElement();

        form.onAttached();
        form.setChangedOnlyValidation(true);

        Set<FormFieldBinding> bindings = new HashSet<>();
        FormFieldBinding binding = new FormFieldBinding();
        binding.setItemBind("name");
        bindings.add(binding);
        binding = new FormFieldBinding();
        binding.setItemBind("changedProperty");
        bindings.add(binding);

        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, true, Json.createArray(), JsonSerializer.toJson(bindings)));
        form.setFormItem(new ValidationTestItem());

        Set<ConstraintViolation<?>> mockViolations = Objects.requireNonNull(getMockValidator(form)).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Error 1", "name"));
        mockViolations.add(new MockConstraintViolation<>("Error 2", "changedProperty"));

        ComponentUtil.fireEvent(form, new FormFieldValueChangeEvent(form, true, "changedProperty", Json.createNull(), Json.createNull(), false));

        ArgumentCaptor<JsonArray> validityDescriptorsCaptor = ArgumentCaptor.forClass(JsonArray.class);
        verify(formElement, times(2)).setPropertyJson(eq(UIBuilderFormBase.PROP_VALIDITY_DESCRIPTORS), validityDescriptorsCaptor.capture());

        JsonArray value = validityDescriptorsCaptor.getValue();
        assertNotNull(value);
        assertEquals(1, value.length());
        JsonObject object = value.getObject(0);
        assertEquals("changedProperty", object.getString("p"));
        assertEquals("Error 2", object.getString("m"));
        assertFalse(object.getBoolean("v"));
    }

    @Test
    @DisplayName("should handle events from child forms in changed-only-validation mode")
    void test_should_handle_events_from_child_forms_in_changed_only_validation_mode() {
        UIBuilderForm<?> childForm = new UIBuilderForm<>();
        UIBuilderForm<ValidationTestItem> form = new UIBuilderForm<>();
        form = spy(form);
        Element formElement = spy(form.getElement());
        doReturn(formElement).when(form).getElement();

        form.onAttached();
        form.setChangedOnlyValidation(true);

        Set<FormFieldBinding> bindings = new HashSet<>();
        FormFieldBinding binding = new FormFieldBinding();
        binding.setItemBind("name");
        bindings.add(binding);
        binding = new FormFieldBinding();
        binding.setItemBind("changedProperty");
        bindings.add(binding);

        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, true, Json.createArray(), JsonSerializer.toJson(bindings)));
        form.setFormItem(new ValidationTestItem());

        Set<ConstraintViolation<?>> mockViolations = Objects.requireNonNull(getMockValidator(form)).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Error 1", "name"));
        mockViolations.add(new MockConstraintViolation<>("Error 2", "changedProperty"));

        ComponentUtil.fireEvent(form, FormFieldValueChangeEvent.nullEventWithFormChildValid(childForm, true));

        ArgumentCaptor<JsonArray> validityDescriptorsCaptor = ArgumentCaptor.forClass(JsonArray.class);
        verify(formElement, times(2)).setPropertyJson(eq(UIBuilderFormBase.PROP_VALIDITY_DESCRIPTORS), validityDescriptorsCaptor.capture());

        JsonArray value = validityDescriptorsCaptor.getValue();
        assertNotNull(value);
        assertEquals(0, value.length());
    }

    @Test
    @DisplayName("Should check child form's tracked fields validity when in changed-only-validation mode and handling the save-after-validation event")
    void test_should_check_child_form_s_tracked_fields_validity_when_in_changed_only_validation_mode_and_handling_the_save_after_validation_event() {
        UIBuilderForm<ValidationTestItem> childForm = new UIBuilderForm<>();
        childForm = spy(childForm);
        Element childFormElement = spy(childForm.getElement());
        doReturn(childFormElement).when(childForm).getElement();

        childForm.setId("subForm");
        childForm.onAttached();
        childForm.setChangedOnlyValidation(true);

        UIBuilderForm<ValidationTestItem> form = new UIBuilderForm<>();
        form = spy(form);
        Element formElement = spy(form.getElement());
        doReturn(formElement).when(form).getElement();

        form.onAttached();
        form.setChangedOnlyValidation(true);

        Set<FormFieldBinding> bindings = new HashSet<>();
        FormFieldBinding binding = new FormFieldBinding();
        binding.setItemBind("name");
        bindings.add(binding);
        binding = new FormFieldBinding();
        binding.setItemBind("changedProperty");
        bindings.add(binding);

        ComponentUtil.fireEvent(childForm, new FormFieldChangeEvent(childForm, true,
            Json.createArray(),
            JsonSerializer.toJson(bindings)));

        ComponentUtil.fireEvent(form, new FormFieldChangeEvent(form, true,
            JsonSerializer.toJson(Collections.singletonList("subForm")),
            Json.createArray()));

        form.setFormItem(new ValidationTestItem());
        childForm.setFormItem(new ValidationTestItem());

        Set<ConstraintViolation<?>> mockViolations = Objects.requireNonNull(getMockValidator(childForm)).getMockViolations();
        mockViolations.clear();
        mockViolations.add(new MockConstraintViolation<>("Error 1", "name"));
        mockViolations.add(new MockConstraintViolation<>("Error 2", "changedProperty"));

        JsonObject eventData = Json.createObject();
        eventData.put(UIBuilderForm.CHANGED_ONLY_VALIDATION_SAVE, true);
        form.validate(new DomEvent(form.getElement(), "validate", eventData));

        ArgumentCaptor<JsonArray> validityDescriptorsCaptor = ArgumentCaptor.forClass(JsonArray.class);
        verify(childFormElement, times(1)).setPropertyJson(eq(UIBuilderFormBase.PROP_VALIDITY_DESCRIPTORS), validityDescriptorsCaptor.capture());

        JsonArray value = validityDescriptorsCaptor.getValue();
        assertNotNull(value);
        assertEquals(2, value.length());
    }

    static class ValidationTestItem implements Serializable {
        private String name;
        private String changedProperty;
    }

    private MockValidator getMockValidator(UIBuilderForm<?> form) {
        FormValidator validator = form.getValidator();
        try {
            Method getValidatorMethod = FormValidator.class.getDeclaredMethod("getValidator");
            getValidatorMethod.setAccessible(true);
            return (MockValidator) getValidatorMethod.invoke(validator);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            fail("Could not get mock validator from FormValidator", e);
            return null;
        }
    }
}
