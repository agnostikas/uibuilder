/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.validator;

import io.devbench.uibuilder.api.exceptions.ComponentException;
import io.devbench.uibuilder.components.form.UIBuilderFormBase;
import io.devbench.uibuilder.components.form.exception.FormInvalidPropertyPathException;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BindingNodeSyncError;
import io.devbench.uibuilder.i18n.core.I;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PropertyValidityDescriptorTest {

    @Test
    @DisplayName("should create property validity descriptor which is invalid and has an error message")
    void test_should_create_property_validity_descriptor_which_is_invalid_and_has_an_error_message() {
        PropertyValidityDescriptor propertyValidityDescriptor = PropertyValidityDescriptor.create("prop_name", "The error message");
        assertAll(
            () -> assertNotNull(propertyValidityDescriptor),
            () -> assertEquals("The error message", propertyValidityDescriptor.getErrorMessage()),
            () -> assertEquals("prop_name", propertyValidityDescriptor.getPropertyName()),
            () -> assertFalse(propertyValidityDescriptor.isValid(), "PropertyValidityDescriptor isValid should return false")
        );
    }

    @Test
    @DisplayName("should create property validity descriptor by sync error")
    void test_should_create_property_validity_descriptor_by_sync_error() {
        BindingNodeSyncError syncError = new BindingNodeSyncError(null, new ComponentException("Test"), UIBuilderFormBase.PROP_FORM_ITEM + ".prop");
        PropertyValidityDescriptor propertyValidityDescriptor = PropertyValidityDescriptor.create(syncError);
        assertAll(
            () -> assertNotNull(propertyValidityDescriptor),
            () -> assertEquals("prop", propertyValidityDescriptor.getPropertyName()),
            () -> assertEquals(ComponentException.class.getSimpleName() + ": " + "Test", propertyValidityDescriptor.getErrorMessage()),
            () -> assertFalse(propertyValidityDescriptor.isValid(), "PropertyValidityDescriptor isValid should return false")
        );
    }

    @Test
    @DisplayName("should create property validity descriptor by sync error when exception is null")
    void test_should_create_property_validity_descriptor_by_sync_error_when_exception_is_null() {
        BindingNodeSyncError syncError = new BindingNodeSyncError(null, null, UIBuilderFormBase.PROP_FORM_ITEM + ".prop");
        PropertyValidityDescriptor propertyValidityDescriptor = PropertyValidityDescriptor.create(syncError);
        assertAll(
            () -> assertNotNull(propertyValidityDescriptor),
            () -> assertEquals("prop", propertyValidityDescriptor.getPropertyName()),
            () -> assertEquals(I.tr("Synchronization error"), propertyValidityDescriptor.getErrorMessage()),
            () -> assertFalse(propertyValidityDescriptor.isValid(), "PropertyValidityDescriptor isValid should return false")
        );
    }

    @Test
    @DisplayName("should throw exception if property path is invalid")
    void test_should_throw_exception_if_property_path_is_invalid() {
        BindingNodeSyncError syncError = new BindingNodeSyncError(null, null, "invalid");
        assertThrows(FormInvalidPropertyPathException.class, () -> PropertyValidityDescriptor.create(syncError), "Should throw exception");
    }

}
