/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.exceptions.ParserException;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import org.jsoup.nodes.Element;

import static io.devbench.uibuilder.core.utils.ElementCollector.*;

public class FormParseInterceptor implements ParseInterceptor {

    @Override
    public boolean isApplicable(Element element) {
        return UIBuilderForm.TAG_NAME.equals(element.tagName());
    }

    @Override
    public void intercept(Component component, Element element) {
        UIBuilderForm<?> form = (UIBuilderForm<?>) component;
        form.setRawElement(element);
        if (element.hasAttr(UIBuilderForm.ATTR_ITEM_BIND)) {
            form.setItemBind(element.attr(UIBuilderForm.ATTR_ITEM_BIND));
        }
        if (!form.getId().isPresent()) {
            if (element.hasAttr(ID)) {
                form.setId(element.attr(ID));
            } else {
                throw new ParserException("Could not set component ID, element ID is missing");
            }
        }
        if (element.hasAttr("changed-only-validation")) {
            form.setChangedOnlyValidation(true);
        }
    }

    @Override
    public boolean isInstantiator(Element element) {
        return true;
    }

    @Override
    public Component instantiateComponent() {
        return new UIBuilderForm<>();
    }
}
