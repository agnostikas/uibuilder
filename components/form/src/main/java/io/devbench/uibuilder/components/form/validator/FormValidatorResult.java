/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.validator;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FormValidatorResult {

    private Collection<PropertyValidityDescriptor> descriptors;
    private boolean valid;

    public JsonArray toJson() {
        JsonArray array = Json.createArray();
        descriptors.stream().map(this::toJson)
            .forEachOrdered(json -> array.set(array.length(), json));
        return array;
    }

    private JsonObject toJson(PropertyValidityDescriptor pvd) {
        JsonObject json = Json.createObject();
        json.put("p", pvd.getPropertyName());
        json.put("v", pvd.isValid());
        if (pvd.isCollection()) {
            json.put("i", pvd.getIndex());
        }
        if (pvd.getSubPropertyPath() != null) {
            json.put("s", pvd.getSubPropertyPath());
        }
        if (pvd.getErrorMessage() != null && !pvd.getErrorMessage().trim().isEmpty()) {
            json.put("m", pvd.getErrorMessage());
        }
        return json;
    }

    public Collection<PropertyValidityDescriptor> getDescriptors() {
        return descriptors;
    }

    public boolean isValid() {
        return valid;
    }

    public void filter(Predicate<PropertyValidityDescriptor> predicate) {
        descriptors = descriptors.stream().filter(predicate).collect(Collectors.toList());
        valid = isValid(descriptors);
    }

    private static boolean isValid(Collection<PropertyValidityDescriptor> descriptors) {
        return descriptors.stream().allMatch(PropertyValidityDescriptor::isValid);
    }

    public static FormValidatorResult create(Collection<PropertyValidityDescriptor> descriptors) {
        FormValidatorResult result = new FormValidatorResult();
        result.descriptors = new ArrayList<>(descriptors);
        result.valid = isValid(result.descriptors);
        return result;
    }

    public static void add(FormValidatorResult result, Collection<PropertyValidityDescriptor> descriptors) {
        result.descriptors.addAll(descriptors);
        result.valid = isValid(result.descriptors);
    }
}
