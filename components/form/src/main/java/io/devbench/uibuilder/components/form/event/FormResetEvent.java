/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.event;

import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import io.devbench.uibuilder.components.form.UIBuilderForm;

@DomEvent("reset")
public class FormResetEvent extends ProceedProcessingComponentEvent<UIBuilderForm> {

    public FormResetEvent(UIBuilderForm source, boolean fromClient,
                          @EventData("typeof(event.detail ? event.detail.proceedProcessing : null)") String proceedProcessingType) {

        super(source, fromClient, proceedProcessingType);
    }

}
