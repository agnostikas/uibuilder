/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.event;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.vaadin.flow.dom.DisabledUpdateMode;
import com.vaadin.flow.internal.JsonSerializer;
import elemental.json.JsonArray;
import io.devbench.uibuilder.components.form.UIBuilderForm;

import java.util.HashSet;
import java.util.Set;

@DomEvent(value = "form-field-change", allowUpdates = DisabledUpdateMode.ALWAYS)
public class FormFieldChangeEvent extends ComponentEvent<UIBuilderForm> {

    private Set<String> childFormIds;
    private Set<FormFieldBinding> bindings;

    public FormFieldChangeEvent(UIBuilderForm source, boolean fromClient,
                                @EventData("event.detail.childFormIds") JsonArray childFormIds,
                                @EventData("event.detail.bindings") JsonArray bindingsJson) {

        super(source, fromClient);
        this.childFormIds = new HashSet<>(JsonSerializer.toObjects(String.class, childFormIds));
        this.bindings = new HashSet<>(JsonSerializer.toObjects(FormFieldBinding.class, bindingsJson));
    }

    public Set<String> getChildFormIds() {
        return childFormIds;
    }

    public Set<FormFieldBinding> getBindings() {
        return bindings;
    }
}
