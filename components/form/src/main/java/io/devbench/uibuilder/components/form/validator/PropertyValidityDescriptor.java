/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.validator;

import io.devbench.uibuilder.components.form.exception.FormInvalidPropertyPathException;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BindingNodeSyncError;
import io.devbench.uibuilder.i18n.core.I;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

import static io.devbench.uibuilder.components.form.UIBuilderFormBase.*;

public class PropertyValidityDescriptor implements Serializable {

    private String propertyName;
    private String subPropertyPath;
    private String errorMessage;
    private int index;
    private boolean valid;
    private boolean collection = false;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getSubPropertyPath() {
        return subPropertyPath;
    }

    public void setSubPropertyPath(String subPropertyPath) {
        this.subPropertyPath = subPropertyPath;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.collection = true;
        this.index = index;
    }

    public boolean isCollection() {
        return collection;
    }

    public void markInvalid(String errorMessage) {
        this.valid = false;
        this.errorMessage = this.errorMessage != null && !this.errorMessage.trim().isEmpty() ?
            this.errorMessage + "\n" + errorMessage :
            errorMessage;
    }

    public static PropertyValidityDescriptor create(String propertyName) {
        PropertyValidityDescriptor pvd = new PropertyValidityDescriptor();
        pvd.setPropertyName(propertyName);
        pvd.setValid(true);
        return pvd;
    }

    public static PropertyValidityDescriptor create(String propertyName, String errorMessage) {
        PropertyValidityDescriptor pvd = new PropertyValidityDescriptor();
        pvd.setPropertyName(propertyName);
        pvd.markInvalid(errorMessage);
        return pvd;
    }

    public static PropertyValidityDescriptor create(@NotNull BindingNodeSyncError syncError) {
        Objects.requireNonNull(syncError.getPropertyPath());
        if (!syncError.getPropertyPath().startsWith(PROP_FORM_ITEM + ".")) {
            throw new FormInvalidPropertyPathException("PropertyPath must start with \"" + PROP_FORM_ITEM + ".\"");
        }
        return PropertyValidityDescriptor.create(
            syncError.getPropertyPath().substring(PROP_FORM_ITEM.length() + 1),
            syncError.getException() != null ?
                syncError.getException().getClass().getSimpleName() + ": " + syncError.getException().getMessage() :
                I.tr("Synchronization error"));
    }

}
