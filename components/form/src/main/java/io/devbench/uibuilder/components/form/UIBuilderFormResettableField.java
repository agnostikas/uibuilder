/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.HasItems;
import io.devbench.uibuilder.api.utils.CollectionUtil;
import io.devbench.uibuilder.components.form.exception.FormException;
import io.devbench.uibuilder.components.form.exception.FormResettableFieldException;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import org.apache.commons.lang3.SerializationUtils;
import java.io.Serializable;
import java.util.Collection;

public class UIBuilderFormResettableField<T extends Serializable> {

    private UIBuilderForm<T> form;
    private Component component;
    private PropertyMetadata<?> propertyMetadata;
    private String propertyPath;
    private Serializable original;
    private boolean forceValue = false;

    public UIBuilderFormResettableField(UIBuilderForm<T> form, Component component,
                                        PropertyMetadata<?> propertyMetadata, String propertyPath, boolean forceValue) {
        this(form, component, propertyMetadata, propertyPath, propertyMetadata.getValue());
        this.forceValue = forceValue;
    }

    public UIBuilderFormResettableField(UIBuilderForm<T> form, Component component,
                                        PropertyMetadata<?> propertyMetadata, String propertyPath, Serializable original) {
        this.form = form;
        this.component = component;
        this.propertyPath = propertyPath;
        this.propertyMetadata = propertyMetadata;
        this.original = SerializationUtils.clone(original);
    }

    public boolean isForceValue() {
        return forceValue;
    }

    public void setForceValue(boolean forceValue) {
        this.forceValue = forceValue;
    }

    protected Serializable getOriginal() {
        return original;
    }

    public void mark() {
        this.original = SerializationUtils.clone(getCurrentValue());
    }

    public void reset() {
        Serializable currentValue = getCurrentValue();
        if (original != null) {
            if (!original.equals(currentValue)) {
                setCurrentValue(original);
                original = SerializationUtils.clone(original);
            }
        } else {
            if (currentValue != null) {
                setCurrentValue(null);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private PropertyMetadata<T> ensurePropertyMetadata() {
        return (PropertyMetadata<T>) form.getFormItemClassMetadata()
            .property(propertyPath)
            .orElseThrow(() -> new FormException("Unknown form property metadata: " + propertyMetadata.getName()));
    }

    private Serializable getCurrentValue() {
        return ensurePropertyMetadata().getValue();
    }

    private void setCurrentValue(Serializable value) {
        ensurePropertyMetadata().setValue(value);
        setComponentValue(value);
    }

    @SuppressWarnings("unchecked")
    private void setComponentValue(Serializable value) {
        if (!forceValue && component instanceof HasItems) {
            ((HasItems) component).setItems((Collection) value);
        } else if (component instanceof HasValue) {
            ((HasValue) component).setValue(value);
        } else {
            throw new FormResettableFieldException("Cannot set value (" + value + ") on component: " + component.getClass().getName());
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public boolean isValueChanged() {
        Serializable currentValue = getCurrentValue();
        if (original != null) {
            if (currentValue == null) {
                return true;
            } else {
                if (original instanceof Collection && currentValue instanceof Collection) {
                    return !CollectionUtil.isCollectionsEqual((Collection) original, (Collection) currentValue);
                } else {
                    return !original.equals(currentValue);
                }
            }
        } else {
            return currentValue != null;
        }
    }



}
