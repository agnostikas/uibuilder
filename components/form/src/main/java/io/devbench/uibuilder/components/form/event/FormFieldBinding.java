/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.event;

import java.io.Serializable;
import java.util.Objects;

public class FormFieldBinding implements Serializable {
    private String componentId;
    private String itemBind;
    private String valueSourcePropertyName;

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public String getItemBind() {
        return itemBind;
    }

    public String getValueSourcePropertyName() {
        return valueSourcePropertyName;
    }

    public void setItemBind(String itemBind) {
        this.itemBind = itemBind;
    }

    public void setValueSourcePropertyName(String valueSourcePropertyName) {
        this.valueSourcePropertyName = valueSourcePropertyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FormFieldBinding that = (FormFieldBinding) o;
        return Objects.equals(getComponentId(), that.getComponentId()) &&
            Objects.equals(getItemBind(), that.getItemBind()) &&
            Objects.equals(getValueSourcePropertyName(), that.getValueSourcePropertyName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getComponentId(), getItemBind(), getValueSourcePropertyName());
    }
}
