/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.data.binder.HasItems;
import com.vaadin.flow.dom.DisabledUpdateMode;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ComponentMapping;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import com.vaadin.flow.shared.Registration;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonValue;
import io.devbench.uibuilder.api.components.HasItemType;
import io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder;
import io.devbench.uibuilder.components.form.event.*;
import io.devbench.uibuilder.components.form.exception.FormCollectionSpecialBindException;
import io.devbench.uibuilder.components.form.exception.FormSpecialBindException;
import io.devbench.uibuilder.components.form.validator.*;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BeanNode;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BindingNodeSyncError;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.utils.ElementCollector;
import io.devbench.uibuilder.core.utils.HtmlElementAwareComponent;
import io.devbench.uibuilder.core.utils.JsonSerializer;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import org.apache.commons.lang3.StringUtils;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Tag(UIBuilderForm.TAG_NAME)
@JsModule("./uibuilder-form/src/uibuilder-form.js")
public class UIBuilderForm<T extends Serializable> extends UIBuilderFormBase implements PropertyValidityDescriptorFilterable, HasItemType {

    static final String CHANGED_ONLY_VALIDATION_SAVE =
        "event && event.detail && event.detail.changedOnlyValidationSave ? event.detail.changedOnlyValidationSave : false";

    private T formItem;
    private ClassMetadata<T> formItemMetadata;
    private UIBuilderForm<?> parentForm;
    private FormValidator validator;
    private List<UIBuilderCustomFormValidatorRegistration> customValidatorRegistrations;
    private Set<UIBuilderForm<?>> childForms;
    private Set<FormFieldBinding> bindings;
    private StateNodeManager stateNodeManager;
    private StateNode formItemStateNode;

    private Set<UIBuilderFormResettableField<?>> resettableFields;
    private Set<String> unreachableFields;
    private Map<HasValue, Registration> valueChangeRegistrations;
    private Map<HasItems, Registration> componentStateChangeRegistrations;

    private Predicate<PropertyValidityDescriptor> propertyValidityDescriptorPredicate;
    private Class<T> itemType;

    private final FormFieldValidityTracker fieldValidityTracker;

    public UIBuilderForm() {
        validator = new FormValidator();
        customValidatorRegistrations = new ArrayList<>();
        childForms = new HashSet<>();
        resettableFields = new HashSet<>();
        unreachableFields = new HashSet<>();
        valueChangeRegistrations = new HashMap<>();
        componentStateChangeRegistrations = new HashMap<>();
        fieldValidityTracker = new FormFieldValidityTracker();
        UIBuilderFormRegistry.register(this);
    }

    @Override
    public void onAttached() {
        addListener(FormFieldValueChangeEvent.class, this::handleFormFieldValueChange);
        addListener(FormResetConfirmedEvent.class, e -> handleFormResetConfirmed());
        addListener(FormSaveConfirmedEvent.class, e -> handleFormSaveConfirmed());
        addListener(FormItemAssignedEvent.class, e -> handleFormItemAssigned());
        addListener(FormFieldChangeEvent.class, this::handleFormFieldChange);
        addListener(FormResetEvent.class, this::handleFormReset);
        addListener(FormSaveEvent.class, this::handleFormSave);
        getElement()
            .addEventListener("validate", this::validate)
            .addEventData(CHANGED_ONLY_VALIDATION_SAVE);

        super.onAttached();
    }

    @Override
    public Class<T> getItemType() {
        return itemType;
    }

    @SuppressWarnings("unchecked")
    public void setItemType(Class<?> itemType) {
        this.itemType = (Class<T>) itemType;
    }

    @Override
    public Predicate<PropertyValidityDescriptor> getPropertyValidityDescriptorPredicate() {
        return propertyValidityDescriptorPredicate;
    }

    @Override
    public void setPropertyValidityDescriptorPredicate(Predicate<PropertyValidityDescriptor> propertyValidityDescriptorPredicate) {
        this.propertyValidityDescriptorPredicate = propertyValidityDescriptorPredicate;
    }

    public CustomFormValidatorRegistration addCustomValidator(CustomFormValidator validator) {
        return new UIBuilderCustomFormValidatorRegistration(validator);
    }

    private class UIBuilderCustomFormValidatorRegistration implements CustomFormValidatorRegistration {
        private Consumer<CustomFormValidator.Result> resultConsumer;
        private final CustomFormValidator customFormValidator;

        private UIBuilderCustomFormValidatorRegistration(CustomFormValidator customFormValidator) {
            this.customFormValidator = customFormValidator;
            customValidatorRegistrations.add(this);
        }

        private boolean validate() {
            CustomFormValidator.Result result = customFormValidator.get();
            if (resultConsumer != null) {
                resultConsumer.accept(result);
            }
            return result.isValid();
        }

        @Override
        public CustomFormValidatorRegistration apply(HasValidation component) {
            resultConsumer = result -> {
                component.setErrorMessage(result.getErrorMessage());
                component.setInvalid(!result.isValid());
            };
            return this;
        }

        @Override
        public CustomFormValidatorRegistration apply(Consumer<CustomFormValidator.Result> resultConsumer) {
            this.resultConsumer = resultConsumer;
            return this;
        }

        @Override
        public void remove() {
            customValidatorRegistrations.remove(this);
        }
    }

    protected FormValidator getValidator() {
        return validator;
    }

    private void handleFormReset(FormResetEvent event) {
        if (!event.isProceedProcessingPresent()) {
            handleFormResetConfirmed();
        }
    }

    private void handleFormResetConfirmed() {
        resettableFields.forEach(UIBuilderFormResettableField::reset);
    }

    private void handleFormSave(FormSaveEvent event) {
        if (!event.isProceedProcessingPresent()) {
            handleFormSaveConfirmed();
        }
    }

    private void handleFormSaveConfirmed() {
        resettableFields.forEach(UIBuilderFormResettableField::mark);
    }

    private void handleFormFieldValueChange(FormFieldValueChangeEvent event) {
        if (formItemMetadata != null && formItem != null) {
            List<BindingNodeSyncError> bindingNodeSyncErrors = handleCollectionValueChange(event);
            bindingNodeSyncErrors.addAll(stateNodeManager.synchronizeProperties());
            validate(bindingNodeSyncErrors, event != null ? event.getPropertyName() : null, event != null && event.isFormChildValid(), false);
            handleUnreachableProperties();
        }
    }

    private List<BindingNodeSyncError> handleCollectionValueChange(FormFieldValueChangeEvent event) {
        List<BindingNodeSyncError> errors = new ArrayList<>();
        if (event != null) {
            String propertyName = event.getPropertyName();
            JsonValue newValue = event.getNewValue();
            if (propertyName != null && newValue instanceof JsonArray) {
                formItemMetadata.property(propertyName)
                    .ifPresent(property -> {
                        Object value = property.getValue();
                        if (value instanceof Collection) {
                            property.getFirstParameterizedType().ifPresent(collectionType -> {
                                Collection<?> deserializedValues = JsonSerializer.toObjects(collectionType, (JsonArray) newValue);
                                Collection<?> newValues = value instanceof List ? new ArrayList<>(deserializedValues) : new HashSet<>(deserializedValues);
                                property.setValue(newValues);
                            });
                        }
                    });
            }
        }
        return errors;
    }

    private void handleFormFieldChange(FormFieldChangeEvent event) {
        this.bindings = event.getBindings();
        event.getChildFormIds().forEach(
            childFormId -> UIBuilderFormRegistry.getById(childFormId).ifPresent(this::addChildForm)
        );

        if (formItem != null) {
            initFormItem();
        }
    }

    @SuppressWarnings("unchecked")
    private final Consumer<? super UIBuilderForm> initChildForms = childForm -> {
        childForm.updateFormItemByParent();
        if (childForm.hasChildren()) {
            childForm.getChildForms().forEach(this.initChildForms);
        }
    };

    private void handleFormItemAssigned() {
        getChildForms().forEach(initChildForms);
        handleUnreachableProperties();
        getElement().callJsFunction("_onFormReady");
    }

    private void handleUnreachableProperties() {
        if (formItemMetadata == null || formItem == null) {
            resetFieldsReachableState();
        } else {
            if (bindings != null) {
                bindings.forEach(binding -> {
                    String itemBind = binding.getItemBind();
                    formItemMetadata.property(itemBind).ifPresent(propertyMetadata -> setFieldReachable(itemBind, isReachable(propertyMetadata)));
                });
            }
        }
    }

    private boolean isReachable(PropertyMetadata<?> propertyMetadata) {
        Object instance = propertyMetadata.getInstance();
        return instance != null || BeanNode.findBeanNodeInstanceFactory(propertyMetadata.getContainerClass()).isPresent();
    }

    private void resetFieldsReachableState() {
        unreachableFields.clear();
        if (bindings != null) {
            bindings.stream()
                .map(FormFieldBinding::getItemBind)
                .forEach(this::callSetFieldReachable);
        }
    }

    private void callSetFieldReachable(String itemBind) {
        callSetFieldReachable(itemBind, true);
    }

    private void callSetFieldReachable(String itemBind, boolean reachable) {
        getElement().callJsFunction("_setFieldReachable", itemBind, reachable);
    }

    private void setFieldReachable(String itemBind, boolean reachable) {
        if (reachable) {
            if (unreachableFields.remove(itemBind)) {
                callSetFieldReachable(itemBind);
            }
        } else {
            if (unreachableFields.add(itemBind)) {
                callSetFieldReachable(itemBind, false);
            }
        }
    }

    public boolean validate(DomEvent event) {
        boolean saveAfterValidation = event != null &&
            event.getEventData() != null &&
            event.getEventData().hasKey(CHANGED_ONLY_VALIDATION_SAVE) &&
            event.getEventData().getBoolean(CHANGED_ONLY_VALIDATION_SAVE);
        boolean valid = validate(Collections.emptyList(), saveAfterValidation);
        if (valid && saveAfterValidation) {
            getElement().callJsFunction("save", true);
        }
        return valid;
    }

    public boolean validate() {
        return validate((DomEvent) null);
    }

    private boolean validate(List<BindingNodeSyncError> bindingNodeSyncErrors) {
        return validate(bindingNodeSyncErrors, false);
    }

    private boolean validate(List<BindingNodeSyncError> bindingNodeSyncErrors, boolean saveAfterValidation) {
        return validate(bindingNodeSyncErrors, null, false, saveAfterValidation);
    }

    private boolean validate(List<BindingNodeSyncError> bindingNodeSyncErrors, String propertyName, boolean formChildValid, boolean saveAfterValidation) {
        FormValidatorResult result = validator.validate(this.bindings, formItem);
        FormValidatorResult.add(result, bindingNodeSyncErrors.stream().map(PropertyValidityDescriptor::create).collect(Collectors.toList()));
        if (propertyValidityDescriptorPredicate != null) {
            result.filter(propertyValidityDescriptorPredicate);
        }
        if (isChangedOnlyValidation()) {
            if (propertyName != null) {
                result.filter(propertyValidityDescriptor -> propertyName.equals(propertyValidityDescriptor.getPropertyName()));
                fieldValidityTracker.track(result);
            } else if (formChildValid) {
                result.filter(propertyValidityDescriptor -> false);
            }

            if (saveAfterValidation) {
                getChildForms().forEach(childForm -> childForm.validate(bindingNodeSyncErrors, propertyName, formChildValid, saveAfterValidation));
                fieldValidityTracker.track(result);
            }
        }
        getElement().setPropertyJson(PROP_VALIDITY_DESCRIPTORS, result.toJson());

        boolean allCustomValidatorsValid = true;
        for (UIBuilderCustomFormValidatorRegistration customFormValidatorRegistration : customValidatorRegistrations) {
            allCustomValidatorsValid = allCustomValidatorsValid & customFormValidatorRegistration.validate();
        }
        boolean allChildFormsValid = isChangedOnlyValidation() || getChildForms().stream().allMatch(UIBuilderFormBase::isValid);
        boolean allChildFormsTrackedFieldsValid = getChildForms().stream().allMatch(childForm -> childForm.fieldValidityTracker.isAllValid());
        boolean valid;
        if (isChangedOnlyValidation() && propertyName == null && formChildValid) {
            valid = allCustomValidatorsValid;
        } else {
            valid = result.isValid() & allCustomValidatorsValid & allChildFormsValid;
        }

        valid = valid & allChildFormsTrackedFieldsValid & fieldValidityTracker.isAllValid();

        setValid(valid);
        bubbleValidation(valid);
        return valid;
    }

    private void bubbleValidation(boolean valid) {
        UIBuilderForm<?> parentForm = getParentForm();
        if (valid) {
            if (parentForm != null) {
                ComponentUtil.fireEvent(parentForm, FormFieldValueChangeEvent.nullEventWithFormChildValid(this, valid));
            }
        } else {
            while (parentForm != null) {
                parentForm.setValid(false);
                parentForm = parentForm.getParentForm();
            }
        }
    }

    private void updateFormItemByParent() {
        updateFormItemByParent(getItemBindValue(), parentForm.formItemMetadata);
    }

    private void updateFormItemByParent(String itemBindValue, ClassMetadata<?> parentFormItemMetadata) {
        if (itemBindValue != null) {
            if (parentFormItemMetadata != null) {
                parentFormItemMetadata.property(itemBindValue).ifPresent(parentItemPropertyMetadata -> {
                    Object instance = parentItemPropertyMetadata.getInstance();
                    setFormItem(instance != null ? parentItemPropertyMetadata.getValue() : null);
                });
            } else {
                setFormItem(null);
            }
        }
    }

    protected ClassMetadata<T> getFormItemClassMetadata() {
        return formItemMetadata;
    }

    public UIBuilderForm getParentForm() {
        return parentForm;
    }

    public void setParentForm(UIBuilderForm<?> parentForm) {
        if (parentForm != this.parentForm && this.parentForm != null) {
            this.parentForm.removeChildForm(this, false);
        }
        this.parentForm = parentForm;
        if (parentForm != null && !parentForm.getChildForms().contains(this)) {
            parentForm.addChildForm(this, false);
        }
    }

    private void addChildForm(UIBuilderForm<?> childForm, boolean updateParent) {
        childForms.add(childForm);
        if (updateParent) {
            childForm.setParentForm(this);
        }
    }

    public void addChildForm(UIBuilderForm<?> childForm) {
        addChildForm(childForm, true);
    }

    private void removeChildForm(UIBuilderForm<?> childForm, boolean updateParent) {
        childForms.remove(childForm);
        if (updateParent) {
            childForm.setParentForm(null);
        }
    }

    /**
     * Returns the child forms as an <strong>unmodifiable</strong> set. To modify the
     * child forms use the following methods:<br>
     * <ul>
     * <li>{@link #addChildForm(UIBuilderForm)}</li>
     * <li>{@link #removeChildForm(UIBuilderForm)}</li>
     * <li>{@link #setParentForm(UIBuilderForm)}</li>
     * </ul>
     *
     * @return the child forms of the current form
     */
    public Set<UIBuilderForm> getChildForms() {
        return Collections.unmodifiableSet(childForms);
    }

    public void removeChildForm(UIBuilderForm<?> childForm) {
        removeChildForm(childForm, true);
    }

    public boolean hasChildren() {
        return !childForms.isEmpty();
    }

    public T getFormItem() {
        if (formItem == null && parentForm != null) {
            updateFormItemByParent();
        }
        return formItem;
    }

    public void setFormItem(T formItem) {
        this.formItem = formItem;

        if (formItem != null) {
            initFormItem();
        } else {
            clearFormItem();
        }
    }

    private void removeValueChangeRegistrations() {
        valueChangeRegistrations.values().forEach(Registration::remove);
        valueChangeRegistrations.clear();
    }

    private void removeValueChangeRegistration(HasValue<?, ?> component) {
        Registration registration = valueChangeRegistrations.remove(component);
        if (registration != null) {
            registration.remove();
        }
    }

    private void removeComponentStateChangeRegistrations() {
        componentStateChangeRegistrations.values().forEach(Registration::remove);
        componentStateChangeRegistrations.clear();
    }

    private void removeComponentStateChangeRegistration(HasItems<?> hasItems) {
        Registration registration = componentStateChangeRegistrations.remove(hasItems);
        if (registration != null) {
            registration.remove();
        }
    }

    private void clearFormItem() {
        removeValueChangeRegistrations();
        removeComponentStateChangeRegistrations();
        clearSpecial();
        childForms.forEach(UIBuilderForm::clearFormItem);

        resettableFields.clear();
        formItemMetadata = null;
        stateNodeManager = null;
        formItemStateNode = null;
        getElement().getNode().getFeature(ElementPropertyMap.class).setProperty(PROP_FORM_ITEM, formItemStateNode);
        callFormItemAssigned();
    }

    private void initFormItem() {
        resettableFields.clear();
        initFormItemMetadata();
        initStateNodeManager();
    }

    private void callFormItemAssigned() {
        getElement().callJsFunction("_onFormItemAssigned");
    }

    private void initStateNodeManager() {
        if (bindings != null) {
            stateNodeManager = new StateNodeManager(name -> formItemMetadata);
            Set<String> itemBinds = getPropertyBindings();
            itemBinds.forEach(stateNodeManager::addBindingPath);
            formItemStateNode = (StateNode) stateNodeManager.populatePropertyValues().get(PROP_FORM_ITEM);
            getElement().getNode().getFeature(ElementPropertyMap.class).setProperty(PROP_FORM_ITEM, formItemStateNode);
            registerUpdatableProperties(itemBinds);
            withSpecials(this::bindSpecial);
            handleFormFieldValueChange(null);
            callFormItemAssigned();
        }
    }

    private void clearSpecial() {
        if (bindings != null) {
            withSpecials(this::clearSpecial);
        }
    }

    private void withSpecials(Consumer<? super FormFieldBinding> onSpecialBinding) {
        List<String> specials = Arrays.asList(PROP_ITEMS, PROP_SELECTED, PROP_BACKEND);
        bindings.stream()
            .filter(binding -> StringUtils.isNotBlank(binding.getComponentId()))
            .filter(binding -> specials.contains(binding.getValueSourcePropertyName()))
            .forEach(onSpecialBinding);
    }

    private void clearSpecial(FormFieldBinding binding) {
        if (formItemMetadata != null) {
            findComponentById(binding.getComponentId())
                .ifPresent(component -> formItemMetadata.property(binding.getItemBind())
                    .ifPresent(propertyMetadata -> clearSpecialComponent(component, binding.getValueSourcePropertyName())));
        }
    }

    @SuppressWarnings("unchecked")
    private void clearSpecialComponent(Component component, String valueSourcePropertyName) {
        if (PROP_SELECTED.equals(valueSourcePropertyName) || PROP_BACKEND.equals(valueSourcePropertyName)) {
            if (component instanceof HasValue) {
                ((HasValue) component).setValue(null);
            }
        } else if (PROP_ITEMS.equals(valueSourcePropertyName)) {
            if (component instanceof HasItems) {
                ((HasItems) component).setItems(Collections.emptyList());
            }
        }
    }

    protected Optional<Component> findComponentById(String id) {
        List<Component> foundComponents = new ArrayList<>();

        UI.getCurrent().getInternals().getStateTree().getRootNode().visitNodeTree(stateNode -> {
            if (stateNode.hasFeature(ComponentMapping.class)) {
                ComponentMapping componentMapping = stateNode.getFeature(ComponentMapping.class);
                componentMapping.getComponent().ifPresent(foundComponents::add);
            }
        });

        Optional<Component> first = foundComponents.stream()
            .filter(c -> c.getId().isPresent() && c.getId().get().equals(id))
            .findFirst();

        if (first.isPresent()) {
            return first;
        } else {
            Optional<HtmlElementAwareComponent> comp = ElementCollector.getById(id, this);
            return comp.map(HtmlElementAwareComponent::getComponent);
        }
    }

    private void bindSpecial(FormFieldBinding binding) {
        findComponentById(binding.getComponentId()).ifPresent(
            component -> formItemMetadata.property(binding.getItemBind()).ifPresent(
                propertyMetadata -> {
                    if (PROP_SELECTED.equals(binding.getValueSourcePropertyName()) || PROP_BACKEND.equals(binding.getValueSourcePropertyName())) {
                        bindSpecialSelection(component, propertyMetadata, binding.getItemBind());
                    } else if (PROP_ITEMS.equals(binding.getValueSourcePropertyName())) {
                        bindSpecialCollection(component, propertyMetadata, binding.getItemBind());
                    }
                }));
    }

    private void bindSpecialSelection(Component component, PropertyMetadata<?> propertyMetadata, String propertyPath) {
        if (propertyMetadata.getInstance() != null && component instanceof HasValue) {
            makeResetable(component, propertyMetadata, propertyPath);
            updateValueChangeListener((HasValue) component, propertyMetadata, propertyPath);
        }
    }

    private void makeResetable(Component component, PropertyMetadata<?> propertyMetadata, String propertyPath) {
        resettableFields.add(new UIBuilderFormResettableField<>(this, component, propertyMetadata, propertyPath, true));
    }

    private void updateValueChangeListener(HasValue<?, ?> hasValue, PropertyMetadata<?> propertyMetadata, String propertyPath) {
        removeValueChangeRegistration(hasValue);
        hasValue.setValue(propertyMetadata.getValue()); // no need to use the clone, this is a selection field
        addValueChangeRegistration(hasValue, propertyPath);
    }

    private void addValueChangeRegistration(HasValue<?, ?> hasValue, String propertyName) {
        Registration registration = hasValue.addValueChangeListener(
            event -> updateHasValueField(propertyName, event.getValue()));
        valueChangeRegistrations.put(hasValue, registration);
    }

    protected <V> void updateHasValueField(String propertyName, V value) {
        formItemMetadata.setPropertyValue(propertyName, value);
        validate(Collections.emptyList(), propertyName, false, false);
    }

    private <ITEM> void bindSpecialCollection(Component component, PropertyMetadata<?> propertyMetadata, String propertyPath) {
        if (propertyMetadata.getInstance() != null) {
            Class<?> propertyType = propertyMetadata.getType();
            if (Collection.class.isAssignableFrom(propertyType)) {
                Collection<ITEM> collection = ensureCollection(propertyMetadata.getValue(), propertyType);
                if (component instanceof HasItems) {
                    bindToHasItemsCollection(collection, component, propertyMetadata, propertyPath);
                } else {
                    throw new FormSpecialBindException("Invalid component, component must implement HasItems: " + component.getClass().getName());
                }
            } else {
                throw new FormSpecialBindException("Invalid property type, required a collection, found: " + propertyType.getName());
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected static <ITEM> Collection<ITEM> ensureCollection(Object value, Class<?> type) {
        if (value instanceof Collection) {
            if (type.isAssignableFrom(value.getClass())) {
                return (Collection<ITEM>) value;
            } else {
                throw new FormCollectionSpecialBindException("Illegal collection type: " + value.getClass().getName() + " required: " + type.getName());
            }
        } else {
            if (Set.class.isAssignableFrom(type)) {
                return new HashSet<>();
            } else if (List.class.isAssignableFrom(type)) {
                return new ArrayList<>();
            } else {
                throw new FormCollectionSpecialBindException("Unsupported collection type: " + type.getName());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private <ITEM> void bindToHasItemsCollection(Collection<ITEM> collection, Component component, PropertyMetadata<?> propertyMetadata, String propertyPath) {
        requireCollectionProperty(component, propertyMetadata);
        UIBuilderFormResettableField<T> resettableField =
            new UIBuilderFormResettableField<>(this, component, propertyMetadata, propertyPath, (Serializable) collection);
        resettableFields.add(resettableField);
        updateComponentStateChangedListener((HasItems<ITEM>) component, collection, resettableField);
    }

    private <ITEM> void updateComponentStateChangedListener(HasItems<ITEM> hasItems,
                                                            Collection<ITEM> collection,
                                                            UIBuilderFormResettableField<T> resettableField) {
        removeComponentStateChangeRegistration(hasItems);
        hasItems.setItems(collection);
        addComponentStateChangeRegistration(hasItems, resettableField);
    }

    private void addComponentStateChangeRegistration(HasItems<?> hasItems, UIBuilderFormResettableField<T> resettableField) {
        Registration registration = ((Component) hasItems).getElement().addEventListener("component-state-changed", event -> {
            if (resettableField.isValueChanged()) {
                getElement().callJsFunction("markDirty");
            }
        });
        componentStateChangeRegistrations.put(hasItems, registration);
    }

    private void requireCollectionProperty(Component component, PropertyMetadata<?> propertyMetadata) {
        ParameterizedType parameterizedType = propertyMetadata.getParameterizedType();
        if (parameterizedType == null || parameterizedType.getActualTypeArguments() == null || parameterizedType.getActualTypeArguments().length != 1) {
            throw new FormSpecialBindException(
                "Property " + propertyMetadata.getName() + " is not a collection, cannot special bind to " + component.getClass().getName());
        }
    }

    private Set<String> getPropertyBindings() {
        List<String> specials = Arrays.asList(PROP_FORM_ITEM, PROP_ITEMS, PROP_SELECTED, PROP_BACKEND);
        return bindings.stream()
            .filter(b -> !specials.contains(b.getValueSourcePropertyName()))
            .map(b -> PROP_FORM_ITEM + "." + b.getItemBind())
            .collect(Collectors.toSet());
    }

    @SuppressWarnings("unchecked")
    private void initFormItemMetadata() {
        formItemMetadata = new ClassMetadata(formItem.getClass(), true).withInstance(formItem);
    }

    private void registerUpdatableProperties(Collection<String> bindings) {
        Element element = getElement();
        StateNode node = element.getNode();
        JsonArray boundPropertyNames = createPropertyNames(bindings);
        bindings.forEach(binding -> element.getStateProvider().addSynchronizedProperty(node, binding, DisabledUpdateMode.ALWAYS));

        ElementPropertyMap.getModel(node).setUpdateFromClientFilter(bindings::contains);
        node.runWhenAttached(ui -> ui.getInternals().getStateTree().beforeClientResponse(node,
            ctx -> ctx.getUI().getPage().executeJs(
                "this.registerUpdatableModelProperties($0, $1)",
                element, boundPropertyNames)));
    }

    private JsonArray createPropertyNames(Collection<String> bindings) {
        return bindings.stream().map(Json::create).collect(JsonBuilder.jsonArrayCollector());
    }

}
