/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form;

import com.vaadin.flow.server.VaadinSession;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UIBuilderFormSetVaadinSessionProvider implements UIBuilderFormSetProvider {

    private static final String FORMSET = UIBuilderFormSetVaadinSessionProvider.class.getName();

    public Set<UIBuilderForm<?>> getFormSet() {

        VaadinSession session = VaadinSession.getCurrent();
        @SuppressWarnings("unchecked")
        Set<UIBuilderForm<?>> formSet = (Set<UIBuilderForm<?>>) session.getAttribute(FORMSET);

        if (formSet == null) {
            formSet = Collections.synchronizedSet(new HashSet<>());
            session.setAttribute(FORMSET, formSet);
        }

        return formSet;
    }

}
