/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.form.validator;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.devbench.uibuilder.components.form.event.FormFieldBinding;
import io.devbench.uibuilder.components.form.exception.FormInvalidValidatorFactoryClassException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.Serializable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class FormValidator {

    static final String FORM_VALIDATOR_FACTORY = "formValidatorFactory";
    static final Pattern PROPERTY_DETAIL_ARRAY_MATCHER = Pattern.compile("([\\w.]+)(\\[[0-9]+])*(\\..+)*");
    private Validator validator;

    public FormValidator() {
    }

    protected Validator getValidator() {
        if (validator == null) {
            synchronized (this) {
                if (validator == null) {
                    validator = loadConfigValidator()
                        .orElse(Validation.buildDefaultValidatorFactory())
                        .getValidator();
                }
            }
        }
        return validator;
    }

    protected Config getConfig() {
        return ConfigFactory.load();
    }

    private Optional<ValidatorFactory> loadConfigValidator() {
        Config config = getConfig();

        if (config.hasPath(FORM_VALIDATOR_FACTORY)) {
            String formValidatorFactoryClassName = config.getString(FORM_VALIDATOR_FACTORY);
            Class<?> formValidatorFactoryClass;
            try {
                formValidatorFactoryClass = Class.forName(formValidatorFactoryClassName);
            } catch (ClassNotFoundException e) {
                throw new FormInvalidValidatorFactoryClassException(e);
            }
            if (ValidatorFactory.class.isAssignableFrom(formValidatorFactoryClass)) {
                @SuppressWarnings("unchecked")
                Class<ValidatorFactory> validatorFactoryClass = (Class<ValidatorFactory>) formValidatorFactoryClass;
                ValidatorFactory validatorFactory;
                try {
                    validatorFactory = validatorFactoryClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new FormInvalidValidatorFactoryClassException("Could not instantiate validator factory", e);
                }
                return Optional.of(validatorFactory);
            } else {
                throw new FormInvalidValidatorFactoryClassException(formValidatorFactoryClassName);
            }
        }
        return Optional.empty();
    }

    public <T> FormValidatorResult validate(Set<FormFieldBinding> bindings, T instance) {
        if (bindings == null || bindings.isEmpty() || instance == null) {
            return FormValidatorResult.create(Collections.emptySet());
        }

        Map<String, Set<PropertyValidityDescriptor>> pvdMap = bindings.stream()
            .map(FormFieldBinding::getItemBind)
            .collect(Collectors.toMap(binding -> binding, binding -> new HashSet<>()));

        List<String> otherMessages = new ArrayList<>();
        getValidator().validate(instance).forEach(constraintViolation -> {
            PropertyDetail detail = extractPropertyPathWithIndex(constraintViolation.getPropertyPath().toString());
            Set<PropertyValidityDescriptor> descriptors = pvdMap.get(detail.getPropertyPath());
            if (descriptors != null) {
                PropertyValidityDescriptor pvd = PropertyValidityDescriptor.create(detail.getPropertyPath());
                pvd.markInvalid(constraintViolation.getMessage());
                detail.getSubPropertyPath().ifPresent(pvd::setSubPropertyPath);
                detail.getCollectionIndex().ifPresent(pvd::setIndex);
                descriptors.add(pvd);
            } else {
                otherMessages.add(constraintViolation.getMessage());
            }
        });

        pvdMap.forEach((binding, descriptors) -> {
            if (descriptors.isEmpty()) {
                descriptors.add(PropertyValidityDescriptor.create(binding));
            }
        });

        List<PropertyValidityDescriptor> descriptors = pvdMap.values()
            .stream()
            .flatMap(Collection::stream)
            .collect(Collectors.toList());

        if (!otherMessages.isEmpty()) {
            descriptors.add(PropertyValidityDescriptor.create("", String.join("\n", otherMessages)));
        }

        return FormValidatorResult.create(descriptors);
    }

    private PropertyDetail extractPropertyPathWithIndex(String constraintViolationPropertyPath) {
        PropertyDetail detail = new PropertyDetail();
        detail.setPropertyPath(constraintViolationPropertyPath);

        Matcher matcher = PROPERTY_DETAIL_ARRAY_MATCHER.matcher(constraintViolationPropertyPath);
        if (matcher.matches()) {
            String propertyPath = matcher.group(1);
            if (propertyPath != null) {
                detail.setPropertyPath(propertyPath);
            }

            String indexWithBraces = matcher.group(2);
            if (indexWithBraces != null) {
                try {
                    detail.setCollectionIndex(Integer.parseInt(indexWithBraces.substring(1, indexWithBraces.length() - 1)));
                } catch (NumberFormatException e) {
                    log.warn("Invalid index format: {}", indexWithBraces);
                }
            }

            String subPropertyPath = matcher.group(3);
            if (subPropertyPath != null) {
                if (subPropertyPath.startsWith(".")) {
                    subPropertyPath = subPropertyPath.substring(1);
                }
                if (!subPropertyPath.startsWith("<") || !subPropertyPath.endsWith(">")) {
                    detail.setSubPropertyPath(subPropertyPath);
                }
            }
        }
        return detail;
    }

    @Setter
    public static class PropertyDetail implements Serializable {
        @Getter
        private String propertyPath;
        private Integer collectionIndex;
        private String subPropertyPath;

        public Optional<Integer> getCollectionIndex() {
            return Optional.ofNullable(collectionIndex);
        }

        public Optional<String> getSubPropertyPath() {
            return Optional.ofNullable(subPropertyPath);
        }
    }

}
