/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';


export class ErrorHandlerDialog extends ThemableMixin(PolymerElement) {

    static get template() {
        return html`
            <style>
                [part="content"] {
                    margin: 1em;
                }
            </style>
            <uibuilder-window id="window" header-text="[[headerText]]" opened$="[[opened]]" theme="error dialog">
                <div slot="layout" part="content">
                    <iron-icon icon="vaadin:warning"></iron-icon>
                    [[errorMessage]]
                </div>
                <vaadin-button id="okButton" part="okButton" theme="primary" on-click="_onClose">
                    Ok
                </vaadin-button>
            </uibuilder-window>
        `;
    }

    static get is() {
        return 'error-handler-dialog'
    }

    static get properties() {
        return {
            headerText: {
                type: String,
                value: 'Error'
            },
            opened: Boolean,
            errorMessage: String
        };
    }

    handleError(error, onCloseCallback) {
        this.errorMessage = error;
        this.opened = true;
        this.onCloseCallback = onCloseCallback;
    }

    _onClose() {
        this.errorMessage = null;
        this.opened = false;
        if (this.onCloseCallback)
            this.onCloseCallback();
    }
}

customElements.define(ErrorHandlerDialog.is, ErrorHandlerDialog);
