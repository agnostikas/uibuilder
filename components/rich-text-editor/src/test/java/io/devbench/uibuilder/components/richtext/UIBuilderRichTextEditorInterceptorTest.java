/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.richtext;

import io.devbench.uibuilder.components.richtext.exception.UIBuilderRichTextEditorException;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class UIBuilderRichTextEditorInterceptorTest {

    private UIBuilderRichTextEditorInterceptor testObj;
    private UIBuilderRichTextEditor testTarget;

    @BeforeEach
    void setUp() {
        testTarget = new UIBuilderRichTextEditor();
        testObj = new UIBuilderRichTextEditorInterceptor();
    }

    @Test
    @DisplayName("should be applicable only for the correct component tag")
    void test_should_be_applicable_only_for_the_correct_component_tag() {
        assertFalse(testObj.isApplicable(new Element("wrong-comp")));
        assertTrue(testObj.isApplicable(new Element("uibuilder-rich-text-editor")));
        assertFalse(testObj.isApplicable(new Element("uibuilder-rich-text-field")));
        assertFalse(testObj.isApplicable(new Element("rich-text-field")));
        assertFalse(testObj.isApplicable(new Element("rich-text-editor")));
        assertFalse(testObj.isApplicable(new Element("uibuilder-richtext-editor")));
        assertFalse(testObj.isApplicable(new Element("uibuilder-richtext-field")));
    }

    @Test
    @DisplayName("Should set value mode")
    void test_should_set_value_mode() {
        assertEquals(ValueMode.HTML, testTarget.getValueMode());

        Element targetElement = new Element("uibuilder-rich-text-editor");
        targetElement.attr("value-mode", "plain");

        testObj.intercept(testTarget, targetElement);

        assertEquals(ValueMode.PLAIN, testTarget.getValueMode());
    }

    @Test
    @DisplayName("Should set html render mode")
    void test_should_set_html_render_mode() {
        assertEquals(HtmlRenderMode.FRONTEND, testTarget.getHtmlRenderMode());

        Element targetElement = new Element("uibuilder-rich-text-editor");
        targetElement.attr("html-render-mode", "backend");

        testObj.intercept(testTarget, targetElement);

        assertEquals(HtmlRenderMode.BACKEND, testTarget.getHtmlRenderMode());
    }

    @Test
    @DisplayName("Should set formatter only if formatter is registered")
    void test_should_set_formatter_only_if_formatter_is_registered() {
        assertEquals("HTML", testTarget.getFormatterName());

        Element targetElement = new Element("uibuilder-rich-text-editor");
        targetElement.attr("formatter", "invalid-format");

        assertThrows(UIBuilderRichTextEditorException.class, () -> testObj.intercept(testTarget, targetElement));

        targetElement.attr("formatter", "Markdown");
        testObj.intercept(testTarget, targetElement);

        assertEquals("Markdown", testTarget.getFormatterName());
    }

}
