/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.richtext;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import org.jsoup.nodes.Element;
import static io.devbench.uibuilder.components.richtext.UIBuilderRichTextEditor.*;

public class UIBuilderRichTextEditorInterceptor implements ParseInterceptor {

    @Override
    public void intercept(Component component, Element element) {
        UIBuilderRichTextEditor richTextEditor = (UIBuilderRichTextEditor) component;
        if (element.hasAttr(ATTR_HTML_RENDER_MODE)) {
            richTextEditor.setHtmlRenderMode(HtmlRenderMode.valueOf(element.attr(ATTR_HTML_RENDER_MODE).toUpperCase()));
        }
        if (element.hasAttr(ATTR_VALUE_MODE)) {
            richTextEditor.setValueMode(ValueMode.valueOf(element.attr(ATTR_VALUE_MODE).toUpperCase()));
        }
        if (element.hasAttr(ATTR_FORMATTER)) {
            richTextEditor.setFormatterName(element.attr(ATTR_FORMATTER));
        }
    }

    @Override
    public boolean isApplicable(Element element) {
        return "uibuilder-rich-text-editor".equalsIgnoreCase(element.tagName());
    }
}
