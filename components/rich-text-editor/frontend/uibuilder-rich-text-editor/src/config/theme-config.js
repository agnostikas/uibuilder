/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UibuilderRichTextEditorConfig } from "../uibuilder-rich-text-editor.js";

export class UibuilderRichTextEditorThemeConfig extends UibuilderRichTextEditorConfig {

    static get is() {
        return "theme-config"
    }

    static get properties() {
        return {

            /**
             * possible themes: "snow", "bubble"
             */
            theme: {
                type: String,
                value: "snow",
                notify: true
            }
        }
    }

    preConfig(config) {
        super.preConfig(config);

        Object.assign(config, {
            theme: this.theme
        });
    }

    postConfig(quill) {
        super.postConfig(quill);
    }
}

customElements.define(UibuilderRichTextEditorThemeConfig.is, UibuilderRichTextEditorThemeConfig);
