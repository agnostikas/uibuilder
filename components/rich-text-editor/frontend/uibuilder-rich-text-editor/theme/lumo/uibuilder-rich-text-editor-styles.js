/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

const $_documentContainer = html`
<dom-module id="lumo-uibuilder-rich-text-editor" theme-for="uibuilder-rich-text-editor">
    <template>
        <style>
            :host {
                font-family: var(--lumo-font-family);
                padding: var(--lumo-space-xs) 0;
            }

            [part="error-message"] {
                margin-top: 4px;
                margin-left: calc(var(--lumo-border-radius-m) / 4);
                font-size: var(--lumo-font-size-xs);
                line-height: var(--lumo-line-height-xs);
                color: var(--lumo-error-text-color);
                will-change: max-height;
                transition: 0.4s max-height;
                max-height: 5em;
            }

            [part="label"] {
                align-self: flex-start;
                color: var(--lumo-secondary-text-color);
                font-weight: 500;
                font-size: var(--lumo-font-size-s);
                margin-left: calc(var(--lumo-border-radius-m) / 4);
                transition: color 0.2s;
                line-height: 1;
                padding-bottom: 0.5em;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
                position: relative;
                max-width: 100%;
                box-sizing: border-box;
            }

            [part="label"]:empty {
                display: none;
            }

            :host([label]) {
                padding-top: var(--lumo-space-m);
            }
        </style>
    </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
