/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import '@polymer/polymer/polymer-legacy.js';

import { GridElement } from '@vaadin/vaadin-grid/src/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column.js'
import '@vaadin/vaadin-grid/vaadin-grid-selection-column.js'
import '@vaadin/vaadin-grid/vaadin-grid-tree-column.js'
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@vaadin/flow-frontend/uibuilder/data/data-source-mixin.js';
import '@vaadin/flow-frontend/uibuilder-core/uibuilder-ready-listener-mixin.js';
import './uibuilder-item-editable.js';
import '../uibuilder-grid-doubleclick.js';
import { UIBuilderItemEditable } from "./uibuilder-item-editable";


export class UibuilderGrid extends Uibuilder.DataSourceMixin(Uibuilder.ReadyListenerMixin(GridElement)) {

        static get is() {
            return 'vaadin-uibuilder-grid'
        }

        static get properties() {
            return Object.assign({}, GridElement.properties, {
                selectionMode: {
                    type: String,
                    value: "single",
                    notify: true,
                    reflectToAttribute: true,
                    observer: "_onSelectionModeChange"
                },
                activeItem: {
                    type: Object,
                    notify: true,
                    value: null,
                    observer: "_onActiveItemChange"
                },
                selectedItems: {
                    type: Object,
                    notify: true,
                    value: () => [],
                    observer: "_onSelectItemsChange"
                },
                disabled: {
                    type: Boolean,
                    notify: true,
                    observer: "_onGridDisabledChange"
                },
                itemPredicate: {
                    type: String
                },
                crudPanel: {
                    type: String
                }
            });
        }

        constructor() {
            super();
            this.validSelectionMode = ['none', 'single', 'multi'];
        }

        _uibuilderReady() {
            this._processDataSourceTag();
        }

        _onSelectionModeChange(newValue, oldValue) {
            if (this._validateSelectionMode(newValue, oldValue)) {
                if (newValue !== oldValue) {
                    this.activeItem = null;
                }
            }
        }

        _validateSelectionMode(newValue, oldValue) {
            if (this.validSelectionMode.indexOf(newValue) === -1) {
                this.selectionMode = oldValue;
                return false;
            }
            return true;
        }

        _onActiveItemChange(item, oldItem) {
            if (!this.disabled) {
                this.selectedItems = this._calculateNewSelectedItems(this.selectedItems, item, oldItem);
            }
        }

        _calculateNewSelectedItems(selectedItems, item, oldItem) {
            switch (this.selectionMode) {
                case "single":
                    return item ? [item] : [];
                case "multi":
                    if (item) {
                        return this._toggleItemSelection(selectedItems, item);
                    }
                    if (oldItem) {
                        return this._toggleItemSelection(selectedItems, oldItem);
                    }
                    return this.selectedItems;
                default:
                    return [];
            }
        }

        _getKeyId(item) {
            return item && item[this.itemIdPath];
        }

        _toggleItemSelection(selectedItems, item) {
            const itemId = this._getKeyId(item);
            if (itemId) {
                let toRemove = null;
                selectedItems.forEach(selectedItem => {
                    const selectedItemId = this._getKeyId(selectedItem);
                    if (itemId === selectedItemId) {
                        toRemove = selectedItem;
                    }
                });
                if (toRemove) {
                    const index = selectedItems.indexOf(toRemove);
                    if (index !== -1) {
                        return selectedItems.filter((_, i) => i !== index);
                    }
                } else {
                    return [...selectedItems, item];
                }
            } else {
                console.error("Trying to toggle selection on item without a key ID", item);
            }
        }

        _onSelectItemsChange(newValue) {
            this.dispatchEvent(new CustomEvent("selection-ids-changed", {detail: {newValue}}))
        }

        _onItemsSelected(items) {
            this.selectedItems = items ? Array.isArray(items) ? items : [items] : [];
            if (this.selectionMode === 'single' && this.selectedItems.length === 0) {
                this.activeItem = null;
            }
        }

        _refresh() {
            this.size = undefined;
            this.clearCache();
        }

        _refreshItem(item) {
            if (item) {
                Array.from(this.$.items.children).forEach(row => {
                    let instance = row.children[0]._instance;
                    if (instance) {
                        let rowItem = instance._getProperty('item');
                        if (rowItem && item[this.itemIdPath] === rowItem[this.itemIdPath]) {
                            instance._setPendingProperty('item', {}, false);
                            this._loadPage(this._getPageForIndex(instance.index), this._cache);
                        }
                    }
                });
            }
        }

        _inlineItemSaved(item) {
            this.dispatchEvent(new CustomEvent("inline-item-saved", {
                detail: {
                    item: item,
                    crudPanel: this.crudPanel
                },
            }));
        }

        _onGridDisabledChange(newValue) {
            this.querySelectorAll(UIBuilderItemEditable.is)
                .forEach(itemEditableContainer => itemEditableContainer.disabled = newValue);
        }

        _onDoubleClickTimeout(modelElement) {
            if (modelElement && modelElement.item) {
                this._onActiveItemChange(modelElement.item, this.activeItem);
            }
        }


    }

customElements.define(UibuilderGrid.is, UibuilderGrid);
