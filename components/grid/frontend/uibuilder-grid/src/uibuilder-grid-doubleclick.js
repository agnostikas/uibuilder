/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';

export class UibuilderGridDoubleclick extends ThemableMixin(Uibuilder.I18NBaseMixin('uibuilder-grid-doubleclick', PolymerElement)) {

    static get template() {
        return html`
            <div part="content" on-click="_onClick">
                <slot></slot>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-grid-doubleclick';
    }

    static get properties() {
        return {
            timeout: {
                type: Number,
                value: 300
            },
            preventSingleClick: {
                type: Boolean,
                value: false
            }
        };
    }

    ready() {
        super.ready();
    }

    _handleSingleClick() {
        return !this.preventSingleClick;
    }

    _onClick(e) {
        e.preventDefault();

        if (this._handleSingleClick() && this.doubleClickTimeout) {
            clearTimeout(this.doubleClickTimeout);
            delete this.doubleClickTimeout;
        }

        let isDoubleClick = false;
        if (this.previousMouseEvent) {
            isDoubleClick = this.previousMouseEvent.button === e.button
                && this.previousMouseEvent.x === e.x
                && this.previousMouseEvent.y === e.y
                && (Date.now() - this.previousMouseEventDate <= this.timeout);
        }
        this.previousMouseEvent = e;
        this.previousMouseEventDate = Date.now();

        let currentModelElement = this._getModelElementByMousePosition(e);
        if (isDoubleClick) {
            this.dispatchEvent(new UibuilderGridDoubleclick.ItemHolderEvent(
                "double-click", currentModelElement ? currentModelElement.item : null, {bubbles: true}));
        } else if (this._handleSingleClick()) {
            this.doubleClickTimeout = setTimeout(() => {
                this._findGrid()._onDoubleClickTimeout(currentModelElement);
                delete this.doubleClickTimeout;
            }, this.timeout);
        }
    }

    _findGrid() {
        let el = this;
        while (el && !/^vaadin-uibuilder-grid$/.test(el.localName)) {
            el = el.parentNode;
        }
        return el || undefined;
    }

    _getModelElementByMousePosition(e) {
        const path = e.composedPath();
        const table = path.filter(p => p.tagName === "TABLE" && p.id === "table")[0];
        const cell = path[path.indexOf(table) - 3];
        if (!cell || cell.getAttribute('part').indexOf('details-cell') > -1) {
            return null;
        }
        return this._findGrid().__getRowModel(cell.parentElement);
    }
}

UibuilderGridDoubleclick.ItemHolderEvent = class extends Event {
    constructor(type, item, eventInitDict = {}) {
        super(type, eventInitDict);
        this.model = {item};
    }
};

customElements.define(UibuilderGridDoubleclick.is, UibuilderGridDoubleclick);
