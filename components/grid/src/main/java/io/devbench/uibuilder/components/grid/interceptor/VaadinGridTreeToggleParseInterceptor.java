/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import org.jsoup.nodes.Element;

public class VaadinGridTreeToggleParseInterceptor implements ParseInterceptor {

    public static final String TAG_NAME = "vaadin-grid-tree-toggle";
    public static final String LEAF = "leaf";
    public static final String LEAF_VALUE = "[[item.leaf]]";
    public static final String EXPANDED = "expanded";
    public static final String EXPANDED_VALUE = "{{expanded}}";
    public static final String LEVEL = "level";
    public static final String LEVEL_VALUE = "[[level]]";

    @Override
    public void intercept(Component component, Element element) {
        element.getElementsByTag(TAG_NAME).forEach(child -> {
            if (!child.hasAttr(LEAF)) {
                child.attr(LEAF, LEAF_VALUE);
            }
            if (!child.hasAttr(EXPANDED)) {
                child.attr(EXPANDED, EXPANDED_VALUE);
            }
            if (!child.hasAttr(LEVEL)) {
                child.attr(LEVEL, LEVEL_VALUE);
            }
        });
    }

    @Override
    public boolean isApplicable(Element element) {
        return UIBuilderGrid.TAG_NAME.equals(element.tagName());
    }
}
