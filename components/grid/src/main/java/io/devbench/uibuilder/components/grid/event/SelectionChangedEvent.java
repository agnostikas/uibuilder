/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.event;

import com.vaadin.flow.component.ComponentEvent;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public class SelectionChangedEvent<T> extends ComponentEvent<UIBuilderGrid<T>> {

    private final List<T> selectedItems;

    public SelectionChangedEvent(UIBuilderGrid<T> source, boolean fromClient, @NotNull List<T> selectedItems) {
        super(source, fromClient);
        this.selectedItems = selectedItems;
    }

    public Collection<T> getSelectedItems() {
        return selectedItems;
    }

    public T getSelectedItem() {
        return selectedItems.size() != 1 ? null : selectedItems.get(0);
    }

}
