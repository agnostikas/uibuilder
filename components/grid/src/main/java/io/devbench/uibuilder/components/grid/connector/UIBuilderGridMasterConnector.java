/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.connector;

import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.grid.SelectionMode;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.components.grid.exception.GridNoneSelectionModeException;
import io.devbench.uibuilder.data.collectionds.mdc.AbstractDataSourceCapableMasterConnector;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public class UIBuilderGridMasterConnector<T> extends AbstractDataSourceCapableMasterConnector<UIBuilderGrid<T>, T> {

    private Registration gridSelectionListenerRegistration;
    private Collection<T> selectedItems = Collections.emptyList();

    @SuppressWarnings("unchecked")
    public UIBuilderGridMasterConnector() {
        super((Class) UIBuilderGrid.class);
    }

    @Override
    public void onConnect(@NotNull UIBuilderGrid<T> masterComponent) {
        super.onConnect(masterComponent);

        gridSelectionListenerRegistration = getMasterComponent().addSelectionChangedListener(event -> {
            Collection<T> previouslySelectedItems = getSelectedItems();
            Collection<T> newlySelectedItems = event.getSelectedItems();
            this.selectedItems = newlySelectedItems;
            fireSelectionChangedEvent(
                new MasterSelectionChangedEvent<>(event.getSource(), event.isFromClient(), previouslySelectedItems, newlySelectedItems));
        });
    }

    @Override
    public void disconnect() {
        if (gridSelectionListenerRegistration != null) {
            gridSelectionListenerRegistration.remove();
            gridSelectionListenerRegistration = null;
        }
    }

    @Override
    public Collection<T> getSelectedItems() {
        return selectedItems;
    }

    @Override
    public void setSelectedItems(@NotNull Collection<T> items) {
        Objects.requireNonNull(items, "Selected items collection cannot be null");

        UIBuilderGrid<T> grid = getMasterComponent();
        String selectionMode = grid.getSelectionMode();
        if (SelectionMode.MULTI.is(selectionMode)) {
            grid.setSelectedItems(items);
        } else if (SelectionMode.SINGLE.is(selectionMode)) {
            grid.setSelectedItem(items.isEmpty() ? null : items.iterator().next());
        } else {
            throw new GridNoneSelectionModeException("Cannot set selection on " + selectionMode + " selection model");
        }

        this.selectedItems = Collections.unmodifiableCollection(items);
    }

}
