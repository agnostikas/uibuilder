/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.dom.DisabledUpdateMode;
import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.shared.Registration;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.crud.GenericCrudGridInlineEditorControllerBean;
import io.devbench.uibuilder.api.crud.Refreshable;
import io.devbench.uibuilder.api.exceptions.ComponentInternalException;
import io.devbench.uibuilder.components.grid.event.SelectionChangedEvent;
import io.devbench.uibuilder.components.grid.event.SelectionIDsChangedEvent;
import io.devbench.uibuilder.components.grid.exception.GridMultiSelectModeException;
import io.devbench.uibuilder.components.grid.exception.GridNoneSelectionModeException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.component.ItemPredicateProvider;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.InlineEditHandler;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;

@Slf4j
@Tag(UIBuilderGrid.TAG_NAME)
@JsModule("./uibuilder-grid/src/uibuilder-grid.js")
public class UIBuilderGrid<T> extends AbstractDataSourceComponent<T> implements Refreshable, ItemPredicateProvider<T> {

    public static final String TAG_NAME = "vaadin-uibuilder-grid";
    private static final String SELECTION_MODE = "selectionMode";
    private static final PropertyDescriptor<String, String> SELECTION_MODE_PD = PropertyDescriptors.propertyWithDefault(SELECTION_MODE, "single");
    private static final PropertyDescriptor<String, String> PROP_ITEM_PREDICATE = PropertyDescriptors.propertyWithDefault("itemPredicate", "");

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static final PropertyConverter<Supplier, String> SUPPLIER_CONVERTER =
        (PropertyConverter<Supplier, String>) PropertyConverters.getConverterByType(Supplier.class);

    @Getter
    private final List<T> selectedItems = new LinkedList<>();

    @Inject
    private Provider<GenericCrudGridInlineEditorControllerBean<?>> genericCrudGridInlineEditorControllerBean;


    @Override
    public void onAttached() {
        super.onAttached();
        addListener(SelectionIDsChangedEvent.class, event -> {
            List<T> previouslySelectedItems = new ArrayList<>(selectedItems);
            selectItemsOnBackend(event.getNewValue());
            if (!previouslySelectedItems.equals(selectedItems)) {
                ComponentUtil.fireEvent(this, new SelectionChangedEvent<>(this, event.isFromClient(), selectedItems));
            }
        });
        attachDisabledPropertyChangeHandler();
    }

    private void attachDisabledPropertyChangeHandler() {
        getElement()
            .addPropertyChangeListener(
                "disabled",
                "disabled-changed",
                event -> getElement().getNode().setEnabled(!(boolean) event.getValue()))
            .setDisabledUpdateMode(DisabledUpdateMode.ALWAYS);
    }

    protected ControllerBeanManager getControllerBeanManager() {
        return ControllerBeanManager.getInstance();
    }

    @NotNull
    public InlineEditHandler getInlineEditHandler() {
        return mapDataSource(dataSource -> dataSource.getDataProcessor().getInlineEditHandler())
            .orElseThrow(() -> new ComponentInternalException("DataSource inline edit handler not found in component: " + this));
    }

    @Override
    protected void setSelectedItemsIfItemWasSetPreviously() {
        if (!selectedItems.isEmpty()) {
            refreshSelectedItems();
        }
    }

    public void registerDataSource(@NotNull String datasourceName) {
        getId().ifPresent(gridId -> genericCrudGridInlineEditorControllerBean.get().registerDataSourceName(gridId, datasourceName));
    }

    private void selectItemsOnBackend(JsonArray frontendSelectedItems) {
        withDataSource(dataSource -> {
            selectedItems.clear();
            selectedItems.addAll(dataSource.findItemsByJson(frontendSelectedItems));
        });
    }

    public T getSelectedItem() {
        String selectionMode = getSelectionMode();
        if (SelectionMode.NONE.is(selectionMode)) {
            throw new GridNoneSelectionModeException();
        }
        if (selectedItems.isEmpty()) {
            return null;
        }
        if (selectedItems.size() > 1 && SelectionMode.MULTI.is(selectionMode)) {
            throw new GridMultiSelectModeException();
        }
        return selectedItems.get(0);
    }

    public void setSelectedItem(T item) {
        String selectionMode = getSelectionMode();
        if (SelectionMode.MULTI.is(selectionMode)) {
            throw new GridMultiSelectModeException();
        }
        if (SelectionMode.NONE.is(selectionMode)) {
            throw new GridNoneSelectionModeException();
        }
        selectedItems.clear();
        if (item != null) {
            selectedItems.add(item);
        }
        refreshSelectedItems();
    }

    @SafeVarargs
    @SuppressWarnings("varargs")
    public final void setSelectedItems(T... items) {
        setSelectedItems(Arrays.asList(items));
    }

    public void setSelectedItems(Collection<T> items) {
        if (!SelectionMode.MULTI.is(getSelectionMode())) {
            throw new GridMultiSelectModeException();
        }
        selectedItems.clear();
        selectedItems.addAll(items);
        refreshSelectedItems();
    }

    public void deselect() {
        selectedItems.clear();
        refreshSelectedItems();
    }

    @Override
    public void onItemsSet() {
        super.onItemsSet();
        deselect();
    }

    /**
     * Select the given items in the grid.
     *
     * @param items the items to select
     * @deprecated use {@link #setSelectedItems(Collection)} instead of this
     */
    @Deprecated
    public void selectAll(Collection<T> items) {
        setSelectedItems(items);
    }

    private void refreshSelectedItems() {
        if (getDataSource() != null) {
            getElement().callJsFunction("_onItemsSelected", getDataSource().convertToKeysArray(selectedItems));
        }
    }

    public String getSelectionMode() {
        return get(SELECTION_MODE_PD);
    }

    public void setSelectionMode(SelectionMode selectionMode) {
        set(SELECTION_MODE_PD, selectionMode.getPropertyName());
    }

    @Override
    @Nullable
    @SuppressWarnings("unchecked")
    @Synchronize(property = "itemPredicate", value = "data-provider-changed", allowUpdates = DisabledUpdateMode.ALWAYS)
    public Predicate<T> getItemPredicate() {
        String supplierId = get(PROP_ITEM_PREDICATE);
        SerializableSupplier<Predicate<T>> supplier = StringUtils.isNotBlank(supplierId) ?
            (SerializableSupplier<Predicate<T>>) SUPPLIER_CONVERTER.convertFrom(supplierId) : null;
        if (supplier != null) {
            return supplier.get();
        }
        return null;
    }

    public void setItemPredicate(@Nullable Predicate<T> predicate) {
        SerializableSupplier<Predicate<T>> supplier = predicate != null ? () -> predicate : () -> item -> true;
        set(PROP_ITEM_PREDICATE, SUPPLIER_CONVERTER.convertTo(supplier));
    }

    @SuppressWarnings({"unchecked", "unused", "rawtypes"})
    public Registration addSelectionChangedListener(ComponentEventListener<SelectionChangedEvent<T>> listener) {
        ComponentEventListener componentEventListener = event -> listener.onComponentEvent((SelectionChangedEvent<T>) event);
        return getEventBus().addListener(SelectionChangedEvent.class, componentEventListener);
    }

    @Override
    public void refresh() {
        getElement().callJsFunction("_refresh");
        refreshSelectedItems();
    }

    public void refreshItem(@NotNull T item) {
        withDataSource(dataSource -> refreshItem(dataSource.convertToKey(item)));
    }

    public void setEditMode(@NotNull T item, boolean editMode) {
        withDataSource(dataSource -> setEditMode(dataSource.convertToKey(item), editMode));
    }

    public void setEditMode(@NotNull Collection<T> items, boolean editMode) {
        withDataSource(dataSource -> {
            JsonArray jsonItems = dataSource.convertToKeysArray(items);
            InlineEditHandler inlineEditHandler = dataSource.getDataProcessor().getInlineEditHandler();
            for (int i = 0; i < jsonItems.length(); i++) {
                JsonObject jsonItem = jsonItems.getObject(i);
                inlineEditHandler.setEditMode(jsonItem, editMode);
            }
            refreshSelectedItems();
        });
    }

    private void refreshItem(JsonObject jsonItem) {
        getElement().callJsFunction("_refreshItem", jsonItem);
        refreshSelectedItems();
    }

    private void setEditMode(JsonObject jsonItem, boolean editMode) {
        withDataSource(dataSource -> dataSource.getDataProcessor().getInlineEditHandler().setEditMode(jsonItem, editMode));
        refreshItem(jsonItem);
    }

}

