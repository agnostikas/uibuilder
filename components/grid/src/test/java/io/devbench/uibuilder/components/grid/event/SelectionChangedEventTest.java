/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.event;

import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class SelectionChangedEventTest {

    @Test
    @DisplayName("should return single item")
    void test_should_return_single_item() {
        SelectionChangedEvent<String> event = new SelectionChangedEvent<>(new UIBuilderGrid<>(), false, Collections.singletonList("item"));
        String selectedItem = event.getSelectedItem();

        assertNotNull(selectedItem, "Selected item cannot be null");
        assertEquals("item", selectedItem);
    }

    @Test
    @DisplayName("Should return null if the selected items is empty")
    void test_should_return_null_if_the_selected_items_is_empty() {
        SelectionChangedEvent<String> event = new SelectionChangedEvent<>(new UIBuilderGrid<>(), false, Collections.emptyList());
        String selectedItem = event.getSelectedItem();

        assertNull(selectedItem, "Selected item should be null");
    }

    @Test
    @DisplayName("Should return null if the selected items is more than one items")
    void test_should_return_null_if_the_selected_items_is_more_than_one_items() {
        SelectionChangedEvent<String> event = new SelectionChangedEvent<>(new UIBuilderGrid<>(), false, Arrays.asList("item1", "item2"));
        String selectedItem = event.getSelectedItem();

        assertNull(selectedItem, "Selected item should be null");
    }

}
