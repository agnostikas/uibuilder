/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.interceptor;

import io.devbench.uibuilder.components.grid.exception.ItemEditableUnsupportedComponentException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static io.devbench.uibuilder.api.crud.GenericCrudGridInlineEditorControllerBean.*;
import static org.junit.jupiter.api.Assertions.*;

class UIBuilderItemEditableTransformerTest {

    private UIBuilderItemEditableTransformer testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderItemEditableTransformer();
    }

    @Test
    @DisplayName("Should be applicatble only on uibuilder-grid")
    void test_should_be_applicatble_only_on_uibuilder_grid() {
        Element element = new Element("uibuilder-item-editable");
        Element wrongElement1 = new Element("item-editable");
        Element wrongElement2 = new Element("uibuilder-editable");
        assertTrue(testObj.isApplicable(element));
        assertFalse(testObj.isApplicable(wrongElement1));
        assertFalse(testObj.isApplicable(wrongElement2));
    }

    @Test
    @DisplayName("Should transform uibuilder editable item with generic controller bean and default editor")
    void test_should_transform_uibuilder_editable_item_with_generic_controller_bean_and_default_editor() {
        Document document = Jsoup.parse("");
        Element editableItemElement = document.createElement("uibuilder-item-editable");
        document.appendChild(editableItemElement);

        editableItemElement.attr("path", "name");
        editableItemElement.attr("default-editor", true);

        testObj.transform(editableItemElement);

        assertTrue(editableItemElement.hasAttr("item"));
        assertEquals("[[item]]", editableItemElement.attr("item"));
        assertEquals("{{item.name}}", editableItemElement.ownText());
        Element textEditorFieldElement = editableItemElement.children().first();
        assertNotNull(textEditorFieldElement);

        assertEquals("vaadin-text-field", textEditorFieldElement.tagName());
        assertEquals("edit", textEditorFieldElement.attr("slot"));
        assertEquals("{{item.name}}", textEditorFieldElement.attr("value"));
        assertEquals(BUILT_IN_GENERIC_CRUD_GRID_INLINE_EDITOR_CONTROLLER_BEAN_NAME + "::inlineItemValueChange",
            textEditorFieldElement.attr("on-value-changed"));
        assertEquals("[[item._state.errors.name]]", textEditorFieldElement.attr("error-message"));
        assertEquals("[[item._state.errors.name]]", textEditorFieldElement.attr("invalid"));
    }

    @Test
    @DisplayName("Should transform uibuilder editable item with custom controller bean and custom editor")
    void test_should_transform_uibuilder_editable_item_with_custom_controller_bean_and_custom_editor() {
        Document document = Jsoup.parse("");
        Element editableItemElement = document.createElement("uibuilder-item-editable");
        document.appendChild(editableItemElement);

        editableItemElement.attr("path", "expireDate");
        editableItemElement.attr("default-editor", true);
        editableItemElement.attr("controller-bean", "myCtrlBean");

        Element datetimePickerElement = document.createElement("vaadin-datetime-picker");
        datetimePickerElement.attr("slot", "edit");
        datetimePickerElement.attr("value", "{{item.expireDate}}");
        datetimePickerElement.attr("default-event-handler", true);
        datetimePickerElement.attr("default-validator", true);

        editableItemElement.appendChild(datetimePickerElement);

        testObj.transform(editableItemElement);

        assertTrue(editableItemElement.hasAttr("item"));
        assertEquals("[[item]]", editableItemElement.attr("item"));
        assertEquals("{{item.expireDate}}", editableItemElement.ownText());
        Element customEditorFieldElement = editableItemElement.children().first();
        assertNotNull(customEditorFieldElement);

        assertEquals("vaadin-datetime-picker", customEditorFieldElement.tagName());
        assertEquals("edit", customEditorFieldElement.attr("slot"));
        assertEquals("{{item.expireDate}}", customEditorFieldElement.attr("value"));
        assertEquals("myCtrlBean::inlineItemValueChange", customEditorFieldElement.attr("on-value-changed"));
        assertEquals("[[item._state.errors.expireDate]]", customEditorFieldElement.attr("error-message"));
        assertEquals("[[item._state.errors.expireDate]]", customEditorFieldElement.attr("invalid"));
    }

    @Test
    @DisplayName("Should transform uibuilder editable item with custom controller bean with default buttons")
    void test_should_transform_uibuilder_editable_item_with_custom_controller_bean_with_default_buttons() {
        Document document = Jsoup.parse("");
        Element editableItemElement = document.createElement("uibuilder-item-editable");
        document.appendChild(editableItemElement);

        editableItemElement.attr("default-buttons", true);
        editableItemElement.attr("controller-bean", "myCtrlBean");

        testObj.transform(editableItemElement);

        assertTrue(editableItemElement.hasAttr("item"));
        assertEquals("[[item]]", editableItemElement.attr("item"));

        Elements buttons = editableItemElement.children();

        AtomicBoolean editButtonCreated = new AtomicBoolean(false);
        AtomicBoolean saveButtonCreated = new AtomicBoolean(false);
        AtomicBoolean cancelButtonCreated = new AtomicBoolean(false);
        for (Element button : buttons) {
            if (button.hasAttr("on-click")) {
                if ("myCtrlBean::inlineItemEdit".equals(button.attr("on-click"))) {
                    if (!button.hasAttr("slot")) {
                        editButtonCreated.set(true);
                    } else {
                        fail("Edit button should not contain slot attribute");
                    }
                } else if ("myCtrlBean::inlineItemSave".equals(button.attr("on-click"))) {
                    if (button.hasAttr("slot") &&
                        "edit".equals(button.attr("slot"))) {
                        if (button.hasAttr("disabled") && "[[!item._state.saveAllowed]]".equals(button.attr("disabled"))) {
                            saveButtonCreated.set(true);
                        } else {
                            fail("Save button should have a saveAllowed binding on disabled attribute");
                        }
                    } else {
                        fail("Save button should contain slot attribute with value \"edit\"");
                    }

                } else if ("myCtrlBean::inlineItemCancel".equals(button.attr("on-click"))) {
                    if (button.hasAttr("slot") &&
                        "edit".equals(button.attr("slot"))) {
                        cancelButtonCreated.set(true);
                    } else {
                        fail("Cancel button should contain slot attribute with value \"edit\"");
                    }
                }
            }
        }

        assertTrue(editButtonCreated.get());
        assertTrue(saveButtonCreated.get());
        assertTrue(cancelButtonCreated.get());
    }

    @Test
    @DisplayName("Transform should fail if contains a datasource dependent component")
    void test_transform_should_fail_if_contains_a_datasource_dependent_component() {
        Document document = Jsoup.parse("");
        Element editableItemElement = document.createElement("uibuilder-item-editable");
        document.appendChild(editableItemElement);

        Element unsupportedElement = document.createElement("vaadin-uibuilder-grid");
        unsupportedElement.attr("slot", "edit");
        editableItemElement.appendChild(unsupportedElement);

        ItemEditableUnsupportedComponentException exception = assertThrows(
            ItemEditableUnsupportedComponentException.class, () -> testObj.transform(editableItemElement));

        assertNotNull(exception);
        String exceptionMessage = exception.getMessage();
        assertNotNull(exceptionMessage);
        assertTrue(exceptionMessage.contains("not supported"));
        assertTrue(exceptionMessage.contains("vaadin-uibuilder-grid"));
    }

    @Test
    @DisplayName("Should add default binding")
    void test_should_add_default_binding() {
        Document document = Jsoup.parse("");
        Element editableItemElement = document.createElement("uibuilder-item-editable");
        editableItemElement.attr("path", "todo");
        document.appendChild(editableItemElement);

        testObj.transform(editableItemElement);

        assertEquals("{{item.todo}}", editableItemElement.ownText());

        editableItemElement = document.createElement("uibuilder-item-editable");
        editableItemElement.attr("path", "todo");
        editableItemElement.appendChild(new TextNode(" is the current item"));
        document.appendChild(editableItemElement);

        testObj.transform(editableItemElement);

        assertEquals("{{item.todo}} is the current item", editableItemElement.ownText());

        editableItemElement = document.createElement("uibuilder-item-editable");
        editableItemElement.attr("path", "todo");
        editableItemElement.appendChild(new TextNode(" and {{item.another}} binding"));
        document.appendChild(editableItemElement);

        testObj.transform(editableItemElement);

        assertEquals("{{item.todo}} and {{item.another}} binding", editableItemElement.ownText());

        editableItemElement = document.createElement("uibuilder-item-editable");
        editableItemElement.attr("path", "todo");
        Element component = document.createElement("vaadin-text-field");
        component.attr("slot", "edit");
        component.attr("value", "{{item.todo}}");
        editableItemElement.appendChild(component);
        document.appendChild(editableItemElement);

        testObj.transform(editableItemElement);

        assertEquals("{{item.todo}}", editableItemElement.ownText());
    }

    @Test
    @DisplayName("Should NOT add default binding")
    void test_should_not_add_default_binding() {
        Document document = Jsoup.parse("");
        Element editableItemElement = document.createElement("uibuilder-item-editable");
        editableItemElement.attr("path", "todo");
        editableItemElement.appendChild(new TextNode("The {{item.todo}} is already present"));
        document.appendChild(editableItemElement);

        testObj.transform(editableItemElement);

        assertEquals("The {{item.todo}} is already present", editableItemElement.ownText());

        editableItemElement = document.createElement("uibuilder-item-editable");
        editableItemElement.attr("path", "todo");
        Element textField = document.createElement("vaadin-text-field");
        textField.attr("value", "_ {{item.todo}}_ ");
        Element div = document.createElement("div");
        div.appendChild(new TextNode("start "));
        div.appendChild(textField);
        div.appendChild(new TextNode(" end"));
        editableItemElement.appendChild(new TextNode("Test "));
        editableItemElement.appendChild(div);
        editableItemElement.appendChild(new TextNode(" done."));
        document.appendChild(editableItemElement);

        testObj.transform(editableItemElement);

        assertEquals("Test done.", editableItemElement.ownText());
    }

}
