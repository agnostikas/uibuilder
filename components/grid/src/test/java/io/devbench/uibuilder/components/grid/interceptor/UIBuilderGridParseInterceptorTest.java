/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.interceptor;

import io.devbench.uibuilder.components.grid.SelectionMode;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.List;
import java.util.Optional;

import static io.devbench.uibuilder.core.utils.ElementCollector.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderGridParseInterceptorTest {

    @Mock
    private Element element;

    @Mock
    private UIBuilderGrid<?> grid;

    private UIBuilderGridParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderGridParseInterceptor();
    }

    @Test
    @DisplayName("Should be applicable only for uibuilder grid")
    void test_should_be_applicable_only_for_uibuilder_grid() {
        doReturn("other-tag").when(element).tagName();
        assertFalse(testObj.isApplicable(element), "Element with other tag name should not be applicable");

        doReturn(UIBuilderGrid.TAG_NAME).when(element).tagName();
        assertTrue(testObj.isApplicable(element), "Should be applicable element with tag name: " + UIBuilderGrid.TAG_NAME);
    }

    @Test
    @DisplayName("Should always be an instantiator")
    void test_should_always_be_an_instantiator() {
        assertTrue(testObj.isInstantiator(element), "Should be an instantiator");
    }

    @Test
    @DisplayName("Should set ID on intercept")
    void test_should_set_id_on_intercept() {
        doReturn(Optional.empty()).when(grid).getId();
        doReturn(true).when(element).hasAttr(ID);
        doReturn("test-id").when(element).attr(ID);

        testObj.intercept(grid, element);

        verify(grid).setId("test-id");
    }

    @Test
    @DisplayName("Should not set existing ID on intercept")
    void test_should_not_set_existing_id_on_intercept() {
        doReturn(Optional.of("test-id")).when(grid).getId();
        doReturn(true).when(element).hasAttr(ID);
        doReturn("test-id").when(element).attr(ID);

        testObj.intercept(grid, element);

        verify(grid, never()).setId("test-id");
    }

    @Test
    @DisplayName("Should generate ID if not found")
    void test_should_generate_id_if_not_found() {
        doReturn(Optional.empty()).when(grid).getId();
        doReturn(false).when(element).hasAttr(ID);

        testObj.intercept(grid, element);

        ArgumentCaptor<String> idCaptor = ArgumentCaptor.forClass(String.class);
        verify(element).attr(eq(ID), idCaptor.capture());
        String generatedId = idCaptor.getValue();
        assertNotNull(generatedId);
        verify(grid).setId(generatedId);
    }

    @Test
    @DisplayName("Should set selection mode if attribute is present")
    void test_should_set_selection_mode_if_attribute_is_present() {
        doReturn(Optional.of("test-id")).when(grid).getId();
        doReturn(true).when(element).hasAttr(UIBuilderGridParseInterceptor.SELECTION_MODE_ATTR);


        when(element.attr(UIBuilderGridParseInterceptor.SELECTION_MODE_ATTR))
            .thenReturn("single")
            .thenReturn("multi")
            .thenReturn("none")
            .thenReturn("non-existent-mode");

        testObj.intercept(grid, element);
        testObj.intercept(grid, element);
        testObj.intercept(grid, element);
        testObj.intercept(grid, element);

        ArgumentCaptor<SelectionMode> selectionModeCaptor = ArgumentCaptor.forClass(SelectionMode.class);

        verify(grid, times(4)).setSelectionMode(selectionModeCaptor.capture());

        List<SelectionMode> selectionModes = selectionModeCaptor.getAllValues();

        assertEquals(4, selectionModes.size());
        assertEquals(SelectionMode.SINGLE, selectionModes.get(0));
        assertEquals(SelectionMode.MULTI, selectionModes.get(1));
        assertEquals(SelectionMode.NONE, selectionModes.get(2));
        assertNull(selectionModes.get(3));
    }

    @Test
    @DisplayName("Should not set selection mode if attribute is missing")
    void test_should_not_set_selection_mode_if_attribute_is_missing() {
        doReturn(Optional.of("test-id")).when(grid).getId();
        doReturn(false).when(element).hasAttr(UIBuilderGridParseInterceptor.SELECTION_MODE_ATTR);

        testObj.intercept(grid, element);

        verify(grid, never()).setSelectionMode(any());
    }

    @Test
    @DisplayName("Should set raw element")
    void test_should_set_raw_element() {
        doReturn(Optional.of("test-id")).when(grid).getId();
        doReturn(false).when(element).hasAttr(UIBuilderGridParseInterceptor.SELECTION_MODE_ATTR);

        testObj.intercept(grid, element);

        verify(grid).setRawElement(element);
    }

    @Test
    @DisplayName("Should register datasource if generic datasource attribute is present")
    void test_should_register_datasource_if_generic_datasource_attribute_is_present() {
        doReturn(Optional.of("test-id")).when(grid).getId();
        doReturn(false).when(element).hasAttr(UIBuilderGridParseInterceptor.SELECTION_MODE_ATTR);
        doReturn(true).when(element).hasAttr("generic-datasource-name");
        doReturn("testDS").when(element).attr("generic-datasource-name");

        testObj.intercept(grid, element);

        verify(grid).registerDataSource("testDS");
    }

}
