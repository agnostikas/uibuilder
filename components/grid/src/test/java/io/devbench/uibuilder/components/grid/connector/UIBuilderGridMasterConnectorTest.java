/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.grid.connector;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.TextField;
import io.devbench.uibuilder.api.components.masterconnector.UIBuilderMasterConnector;
import io.devbench.uibuilder.api.exceptions.MasterConnectorNotDirectlyModifiableException;
import io.devbench.uibuilder.api.exceptions.MasterConnectorUnsupportedOperationException;
import io.devbench.uibuilder.components.grid.SelectionMode;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.components.grid.event.SelectionChangedEvent;
import io.devbench.uibuilder.components.grid.exception.GridNoneSelectionModeException;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class UIBuilderGridMasterConnectorTest {

    private UIBuilderMasterConnector<?, ?> testObj;

    @SuppressWarnings("unchecked")
    private UIBuilderGridMasterConnector<String> getStringGridConnector() {
        return (UIBuilderGridMasterConnector<String>) testObj;
    }

    private <T> CommonDataSource<T, ?, ?, ?> mockDataSource(UIBuilderGrid<T> grid) {
        @SuppressWarnings("unchecked")
        CommonDataSource<T, ?, ?, ?> commonDataSource = mock(CommonDataSource.class);
        try {
            Field dataSource = AbstractDataSourceComponent.class.getDeclaredField("dataSource");
            dataSource.setAccessible(true);
            dataSource.set(grid, commonDataSource);
        } catch (Exception e) {
            fail("Could not set data source in UIBuilderGrid");
            return null;
        }
        return commonDataSource;
    }

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderGridMasterConnector<String>();
    }

    @Test
    @DisplayName("should be applicable for Grid")
    void test_should_be_applicable_for_grid() {
        assertFalse(testObj.isApplicable(Grid.class));
        assertTrue(testObj.isApplicable(UIBuilderGrid.class));
        assertFalse(testObj.isApplicable(TextField.class));
    }

    @Test
    @DisplayName("should create connection with selection change listener")
    void test_should_create_connection_with_selection_change_listener() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        UIBuilderGrid<String> grid = new UIBuilderGrid<>();
        CommonDataSource<String, ?, ?, ?> testDataSource = mockDataSource(grid);

        connector.connect(grid);
        AtomicInteger callCounter = new AtomicInteger(0);

        connector.addSelectionChangedListener(event -> {
            int currentRun = callCounter.incrementAndGet();
            assertFalse(event.isFromClient());
            if (currentRun == 1) {
                assertIterableEquals(Collections.emptyList(), event.getPreviouslySelectedItems());
                assertIterableEquals(Collections.singleton("Selected"), event.getNewlySelectedItems());
            } else if (currentRun == 2) {
                assertIterableEquals(Collections.singleton("Selected"), event.getPreviouslySelectedItems());
                assertIterableEquals(Collections.singleton("Another"), event.getNewlySelectedItems());
            } else {
                fail("Should not be called more that two times");
            }
        });

        grid.setSelectedItem("Selected");
        ComponentUtil.fireEvent(grid, new SelectionChangedEvent<>(grid, false, Collections.singletonList("Selected")));

        assertEquals(1, callCounter.get());

        grid.setSelectedItem("Another");
        ComponentUtil.fireEvent(grid, new SelectionChangedEvent<>(grid, false, Collections.singletonList("Another")));

        assertEquals(2, callCounter.get());

        connector.disconnect();
        grid.setSelectedItem("Ignored");
        ComponentUtil.fireEvent(grid, new SelectionChangedEvent<>(grid, false, Collections.singletonList("Ignored")));

        verify(testDataSource, times(3)).convertToKeysArray(any());
        assertEquals(2, callCounter.get());
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @DisplayName("should get master component or throw npe if not specified")
    void test_should_get_master_component_or_throw_npe_if_not_specified() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();

        assertThrows(NullPointerException.class, connector::getMasterComponent, "Should throw NPE");

        UIBuilderGrid<String> grid = new UIBuilderGrid<>();
        connector.connect(grid);

        assertSame(grid, connector.getMasterComponent());
    }

    @Test
    @DisplayName("should set selected items")
    void test_should_set_selected_items() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        @SuppressWarnings("unchecked")
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        Mockito.doReturn(SelectionMode.SINGLE.getPropertyName()).when(grid).getSelectionMode();

        connector.connect(grid);
        assertIterableEquals(Collections.emptyList(), connector.getSelectedItems());

        @SuppressWarnings("ConstantConditions")
        String message = assertThrows(NullPointerException.class, () -> connector.setSelectedItems(null),
            "Should throw NPE when callig set selected items with null").getMessage();
        assertEquals("Selected items collection cannot be null", message);

        connector.setSelectedItem("One");

        verify(grid).setSelectedItem(eq("One"));
        assertIterableEquals(Collections.singletonList("One"), connector.getSelectedItems());
    }

    @Test
    @DisplayName("should set selected items when multiselect")
    void test_should_set_selected_items_when_multiselect() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        @SuppressWarnings("unchecked")
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        doReturn(SelectionMode.MULTI.getPropertyName()).when(grid).getSelectionMode();

        connector.connect(grid);
        assertIterableEquals(Collections.emptyList(), connector.getSelectedItems());

        @SuppressWarnings("ConstantConditions")
        String message = assertThrows(NullPointerException.class, () -> connector.setSelectedItems(null),
            "Should throw NPE when callig set selected items with null").getMessage();
        assertEquals("Selected items collection cannot be null", message);

        connector.setSelectedItems(Arrays.asList("One", "Two"));

        verify(grid).setSelectedItems(eq(Arrays.asList("One", "Two")));
        verify(grid, never()).setSelectedItem(any());
        assertIterableEquals(Arrays.asList("One", "Two"), connector.getSelectedItems());
    }

    @Test
    @DisplayName("should throw exception if trying to set items in invalid selection mode")
    void test_should_throw_exception_if_trying_to_set_items_in_invalid_selection_mode() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        @SuppressWarnings("unchecked")
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        doReturn("invalid").when(grid).getSelectionMode();

        connector.connect(grid);


        GridNoneSelectionModeException ex = assertThrows(GridNoneSelectionModeException.class, () -> connector.setSelectedItem("asd"));
        assertNotNull(ex);
        String exMessage = ex.getMessage();
        assertNotNull(exMessage);
        assertEquals("Cannot set selection on invalid selection model", exMessage);
    }

    @Test
    @DisplayName("should call refresh on grid")
    void test_should_call_refresh_on_grid() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        @SuppressWarnings("unchecked")
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);

        connector.connect(grid);
        connector.refresh();

        verify(grid).refresh();
        assertThrows(MasterConnectorUnsupportedOperationException.class, () -> connector.refresh("Item"));
    }

    @Test
    @DisplayName("should set and get propert enabled status")
    void test_should_set_and_get_propert_enabled_status() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(new UIBuilderGrid<>());

        assertTrue(connector.isEnabled(), "Enabled should be true");

        connector.setEnabled(false);

        assertFalse(connector.isEnabled(), "Enabled should be false");

        connector.setEnabled(true);

        assertTrue(connector.isEnabled(), "Enabled should be true again");
    }

    @Test
    @DisplayName("should return direct modifiable true in all cases at the moment")
    void test_should_return_direct_modifiable_true_in_all_cases_at_the_moment() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(new UIBuilderGrid<>());
        assertFalse(connector.isDirectModifiable());
    }

    @Test
    @DisplayName("should throw exception when trying to add item with non direct modifiable data provider")
    void test_should_throw_exception_when_trying_to_add_item_with_non_direct_modifiable_data_provider() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(new UIBuilderGrid<>());

        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> connector.addItem("New Item"));
    }

    @Test
    @DisplayName("should return proper priority")
    void test_should_return_proper_priority() {
        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        assertEquals(500, connector.getPriority());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should return is modifiable true on collection data source")
    void should_return_is_modifiable_true_on_collection_data_source() {
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);

        when(grid.getDataSource()).thenReturn(mock(CollectionDataSource.class));

        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(grid);

        assertTrue(connector.isDirectModifiable());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should add items to collection data source correctly")
    void should_add_items_to_collection_data_source_if_possible() {
        ArrayList<String> items = new ArrayList<>();
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        CollectionDataSource collectionDataSource = mock(CollectionDataSource.class);

        when(grid.getDataSource()).thenReturn(collectionDataSource);
        when(collectionDataSource.getItems()).thenReturn(items);

        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(grid);
        connector.addItem("Foo");
        connector.addItem("Bar");

        assertEquals(Arrays.asList("Foo", "Bar"), items);
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should remove item from collection data source correctly")
    void should_remove_item_from_collection_data_source_if_possible() {
        ArrayList<String> items = new ArrayList<>(Arrays.asList("One", "Two", "Three"));
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        CollectionDataSource collectionDataSource = mock(CollectionDataSource.class);

        when(grid.getDataSource()).thenReturn(collectionDataSource);
        when(collectionDataSource.getItems()).thenReturn(items);

        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(grid);
        connector.removeItem("Two");

        assertIterableEquals(Arrays.asList("One", "Three"), items);
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should throw exception when adding item to non-collection datasource")
    void should_throw_exception_on_non_collection_datasource() {
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        when(grid.getDataSource()).thenReturn(mock(CommonDataSource.class));

        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(grid);
        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> connector.addItem("Foo"));
    }


    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should throw exception when removing item from non-collection datasource")
    void should_throw_exception_when_removing_item_from_non_collection_datasource() {
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);
        when(grid.getDataSource()).thenReturn(mock(CommonDataSource.class));

        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(grid);
        assertThrows(MasterConnectorNotDirectlyModifiableException.class, () -> connector.removeItem("Foo"));
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should return is modifiable false on non-collection data source")
    void should_return_is_modifiable_false_on_non_collection_data_source() {
        UIBuilderGrid<String> grid = mock(UIBuilderGrid.class);

        when(grid.getDataSource()).thenReturn(mock(CommonDataSource.class));

        UIBuilderGridMasterConnector<String> connector = getStringGridConnector();
        connector.connect(grid);

        assertFalse(connector.isDirectModifiable());
    }

}
