/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.combobox.interceptor;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Optional;

import static io.devbench.uibuilder.core.utils.ElementCollector.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderComboBoxParseInterceptorTest {


    @Mock
    private Element element;

    @Mock
    private UIBuilderComboBox<?> comboBox;

    private UIBuilderComboBoxParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new UIBuilderComboBoxParseInterceptor();
    }

    @Test
    @DisplayName("Should be applicable only for uibuilder combobox")
    void test_should_be_applicable_only_for_uibuilder_combobox() {
        doReturn("other-tag").when(element).tagName();
        assertFalse(testObj.isApplicable(element), "Element with other tag name should not be applicable");

        doReturn(UIBuilderComboBox.TAG_NAME).when(element).tagName();
        assertTrue(testObj.isApplicable(element), "Should be applicable element with tag name: " + UIBuilderComboBox.TAG_NAME);
    }

    @Test
    @DisplayName("Should always be an instantiator")
    void test_should_always_be_an_instantiator() {
        assertTrue(testObj.isInstantiator(element), "Should be an instantiator");
    }

    @Test
    @DisplayName("Should instantiate an UIBuilderComboBox")
    void test_should_instantiate_an_ui_builder_combobox() {
        Component component = testObj.instantiateComponent();

        assertNotNull(component);
        assertTrue(component instanceof UIBuilderComboBox, "Instantiated component should be a UIBuilderComboBox");
    }

    @Test
    @DisplayName("Should set ID on intercept")
    void test_should_set_id_on_intercept() {
        doReturn(Optional.empty()).when(comboBox).getId();
        doReturn(true).when(element).hasAttr(ID);
        doReturn("test-id").when(element).attr(ID);

        testObj.intercept(comboBox, element);

        verify(comboBox).setId("test-id");
    }

    @Test
    @DisplayName("Should not set existing ID on intercept")
    void test_should_not_set_existing_id_on_intercept() {
        doReturn(Optional.of("test-id")).when(comboBox).getId();
        doReturn(true).when(element).hasAttr(ID);
        doReturn("test-id").when(element).attr(ID);

        testObj.intercept(comboBox, element);

        verify(comboBox, never()).setId("test-id");
    }

    @Test
    @DisplayName("Should generate ID if not found")
    void test_should_generate_id_if_not_found() {
        doReturn(Optional.empty()).when(comboBox).getId();
        doReturn(false).when(element).hasAttr(ID);

        testObj.intercept(comboBox, element);

        ArgumentCaptor<String> idCaptor = ArgumentCaptor.forClass(String.class);
        verify(element).attr(eq(ID), idCaptor.capture());
        String generatedId = idCaptor.getValue();
        assertNotNull(generatedId);
        verify(comboBox).setId(generatedId);
    }

    @Test
    @DisplayName("Should set raw element")
    void test_should_set_raw_element() {
        doReturn(Optional.of("test-id")).when(comboBox).getId();
        testObj.intercept(comboBox, element);
        verify(comboBox).setRawElement(element);
    }

}
