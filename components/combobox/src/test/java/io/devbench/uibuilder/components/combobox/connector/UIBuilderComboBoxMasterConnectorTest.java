/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.combobox.connector;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import io.devbench.uibuilder.components.combobox.event.SelectionChangedEvent;
import io.devbench.uibuilder.components.combobox.exceptions.MultiSelectNotSupportedException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UIBuilderComboBoxMasterConnectorTest {

    @Test
    public void should_clear_selected_item_on_disconnect() {
        UIBuilderComboBox mockComponent = mock(UIBuilderComboBox.class);
        UIBuilderComboBoxMasterConnector testObj = new UIBuilderComboBoxMasterConnector();
        HasValue.ValueChangeListener[] valueChangeListener = new HasValue.ValueChangeListener[1];
        Registration registration = mock(Registration.class);
        Object newValue = new Object();

        when(mockComponent.addValueChangeListener(any())).thenAnswer(i -> {
            valueChangeListener[0] = i.getArgument(0);
            return registration;
        });

        testObj.connect(mockComponent);

        valueChangeListener[0].valueChanged(new SelectionChangedEvent<Object>(
            mockComponent, false, null, newValue
        ));

        assertEquals(new ArrayList<>(Collections.singleton(newValue)), new ArrayList<Object>(testObj.getSelectedItems()));

        testObj.disconnect();

        assertEquals(Collections.emptyList(), testObj.getSelectedItems());

        verify(registration).remove();
    }

    @Test
    public void on_set_items_should_call_mastercomponents_method_if_theres_only_one_element_in_the_collection() {
        UIBuilderComboBox mockComponent = mock(UIBuilderComboBox.class, RETURNS_DEEP_STUBS);
        UIBuilderComboBoxMasterConnector testObj = new UIBuilderComboBoxMasterConnector();
        testObj.connect(mockComponent);

        Object item = new Object();
        testObj.setSelectedItems(Collections.singletonList(item));
        verify(mockComponent).setValue(item);
    }

    @Test
    public void on_set_items_should_call_mastercomponents_method_with_null_if_theres_zero_element_in_the_collection() {
        UIBuilderComboBox mockComponent = mock(UIBuilderComboBox.class, RETURNS_DEEP_STUBS);
        UIBuilderComboBoxMasterConnector testObj = new UIBuilderComboBoxMasterConnector();
        testObj.connect(mockComponent);

        Object item = new Object();
        testObj.setSelectedItems(Collections.emptyList());
        verify(mockComponent).setValue(null);
    }

    @Test
    public void on_set_items_should_throw_exception_if_theres_multiple_element_in_the_collection() {
        UIBuilderComboBox mockComponent = mock(UIBuilderComboBox.class, RETURNS_DEEP_STUBS);
        UIBuilderComboBoxMasterConnector testObj = new UIBuilderComboBoxMasterConnector();
        testObj.connect(mockComponent);

        assertThrows(MultiSelectNotSupportedException.class, () -> testObj.setSelectedItems(Arrays.asList(new Object(), new Object())));

        verify(mockComponent, never()).setValue(any());
    }
}
