/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.combobox;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.combobox.event.ComboBoxComponentValueChangeEvent;
import io.devbench.uibuilder.components.combobox.event.SelectionChangedEvent;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderEndpointManager;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({SingletonProviderForTestsExtension.class, MockitoExtension.class})
class UIBuilderComboBoxTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(CommonDataSourceContext.class)
    private CommonDataSourceContext commonDataSourceContext;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataProviderEndpointManager.class)
    private DataProviderEndpointManager dataProviderEndpointManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(UIContext.class)
    private UIContext uiContext;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    @Mock
    private VaadinSession vaadinSession;

    @BeforeEach
    public void setup() {
        reset(commonDataSourceContext, dataProviderEndpointManager, uiContext, dataSourceManager);
        final VaadinSession vaadinSession = VaadinSession.getCurrent();
        doReturn(uiContext).when(vaadinSession).getAttribute("UIBuilderSessionContext");
        doReturn(dataProviderEndpointManager).when(uiContext).computeIfAbsent(eq(DataProviderEndpointManager.class), any());
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
    }

    @Test
    public void should_define_a_value_changed_listener_and_correctly_remap_the_values_from_it() {
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);

        Object oldValue = new Object();
        Object newValue = new Object();

        CollectionDataSource<Object> dataSource = mock(CollectionDataSource.class);
        UIBuilderComboBox<Object> testObj = new UIBuilderComboBox<>();

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("ds-name", null))
            .thenReturn(mock(ItemDataSourceBindingContext.class));
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        when(commonDataSourceContext.replaceDataSource(any(), any(), any())).thenReturn(dataSource);

        testObj.connectItemDataSource("ds-name");
        testObj.setDataSource(dataSource);
        testObj.setValue(oldValue);

        when(dataSource.findItemByIdValue("new-object-id")).thenReturn(newValue);

        AtomicBoolean listenerCalled = new AtomicBoolean(false);
        testObj.onAttached();
        Registration registration = testObj.addValueChangeListener(event -> {
            listenerCalled.set(true);
            assertTrue(event.isFromClient());
            assertEquals(oldValue, event.getOldValue());
            assertEquals(newValue, event.getValue());
        });
        ComponentUtil.fireEvent(testObj, new ComboBoxComponentValueChangeEvent(testObj, true, "new-object-id"));

        assertTrue(listenerCalled.get(), "listener should be called");

        registration.remove();

        listenerCalled.set(false);
        testObj.addValueChangeListener(event -> {
            listenerCalled.set(true);
            assertFalse(event.isFromClient(), "from client should be false when calling from the backend by setValue");
        });
        testObj.setValue("new-object-id");
        assertTrue(listenerCalled.get(), "listener should be called when setValue has been called");
    }

    @Test
    @DisplayName("should skip key generator if value is null to be set")
    void test_should_skip_key_generator_if_value_is_null_to_be_set() throws Exception {
        Element element = mock(Element.class);
        AtomicBoolean eventFired = new AtomicBoolean(false);
        CommonDataSource<String, ?, ?, ?> dataSource = mock(CommonDataSource.class);

        UIBuilderComboBox<String> testObj = new UIBuilderComboBox<String>() {
            @Override
            protected void fireEvent(ComponentEvent<?> componentEvent) {
                eventFired.set(true);

                assertTrue(componentEvent instanceof SelectionChangedEvent, "Component event should be " + SelectionChangedEvent.class.getSimpleName());
                SelectionChangedEvent event = (SelectionChangedEvent) componentEvent;
                assertNotNull(event.getOldValue());
                assertEquals("previous value", event.getOldValue());
                assertNull(event.getValue());
            }

            @Override
            public @Nullable CommonDataSource<String, ?, ?, ?> getDataSource() {
                return dataSource;
            }

            @Override
            public Element getElement() {
                return element;
            }
        };

        Field selectedValue = testObj.getClass().getSuperclass().getDeclaredField("selectedValue");
        selectedValue.setAccessible(true);
        selectedValue.set(testObj, "previous value");

        testObj.setValue(null);

        assertTrue(eventFired.get(), "Event should be fired");
        verify(element).callJsFunction(eq("_onItemSelected"), eq(null));
    }

    @Test
    @DisplayName("should not call frontend if datasource is null")
    void should_not_call_frontend_if_datasource_is_null() throws Exception {
        Element element = mock(Element.class);
        AtomicBoolean eventFired = new AtomicBoolean(false);

        UIBuilderComboBox<String> testObj = new UIBuilderComboBox<String>() {
            @Override
            public @Nullable CommonDataSource<String, ?, ?, ?> getDataSource() {
                return null;
            }

            @Override
            public Element getElement() {
                return element;
            }
        };

        testObj.setValue(null);

        verify(element, never()).callJsFunction(eq("_onItemSelected"), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("should remove selection and call refresh on items")
    void test_should_remove_selection_and_call_refresh_on_items() {
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);

        CollectionDataSource<Object> dataSource = mock(CollectionDataSource.class);

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("ds-name", null))
            .thenReturn(mock(ItemDataSourceBindingContext.class));
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        when(commonDataSourceContext.replaceDataSource(any(), any(), any())).thenReturn(dataSource);

        Element testObjElement = mock(Element.class);

        UIBuilderComboBox<Object> testObj = spy(new UIBuilderComboBox<>());
        when(testObj.getElement()).thenReturn(testObjElement);

        testObj.connectItemDataSource("ds-name");
        testObj.setDataSource(dataSource);

        // assert

        Object selected = new Object();
        testObj.setValue(selected);

        assertSame(selected, testObj.getValue());

        testObj.setItems("One", "Two");

        assertNull(testObj.getValue());
        verify(testObjElement, atLeastOnce()).callJsFunction("_refresh");
    }


    @Test
    @DisplayName("should not accept null value when null values disabled")
    @SuppressWarnings("unchecked")
    void test_should_not_accept_null_value_when_null_values_disabled() {
        UIBuilderComboBox<String> testObj = spy(new UIBuilderComboBox<>());
        CollectionDataSource<String> dataSource = mock(CollectionDataSource.class, RETURNS_DEEP_STUBS);

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("ds-name", null))
            .thenReturn(mock(ItemDataSourceBindingContext.class));
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        when(commonDataSourceContext.replaceDataSource(any(), any(), any())).thenReturn(dataSource);
        when(testObj.getNullValuesDisabled()).thenReturn("true");
        when(testObj.getElement()).thenReturn(mock(Element.class));

        testObj.connectItemDataSource("ds-name");
        testObj.setDataSource(dataSource);
        testObj.setItems("One", "Two");
        testObj.setValue("Two");
        testObj.setValue(null);

        assertEquals(testObj.getValue(), "Two");

        verify(testObj.getElement(), never()).callJsFunction(eq("_onItemSelected"), eq(null));
    }

    @Test
    @DisplayName("should accept null value when null values not disabled")
    @SuppressWarnings("unchecked")
    void test_should_accept_null_value_when_null_values_not_disabled() {
        UIBuilderComboBox<String> testObj = spy(new UIBuilderComboBox<>());
        CollectionDataSource<String> dataSource = mock(CollectionDataSource.class);

        when(dataSourceManager.getDataSourceProvider().getBindingContextForName("ds-name", null))
            .thenReturn(mock(ItemDataSourceBindingContext.class));
        doReturn(commonDataSourceContext).when(uiContext).computeIfAbsent(eq(CommonDataSourceContext.class), any());
        when(commonDataSourceContext.replaceDataSource(any(), any(), any())).thenReturn(dataSource);
        when(testObj.getNullValuesDisabled()).thenReturn("false");
        when(testObj.getElement()).thenReturn(mock(Element.class));

        testObj.connectItemDataSource("ds-name");
        testObj.setDataSource(dataSource);
        testObj.setItems("One", "Two");
        testObj.setValue("Two");
        testObj.setValue(null);

        assertNull(testObj.getValue());

        verify(testObj.getElement(), atLeast(2)).callJsFunction(eq("_onItemSelected"), any());
    }

    @Test
    @DisplayName("should set invalid")
    void test_should_set_invalid() {
        UIBuilderComboBox<String> testObj = spy(new UIBuilderComboBox<>());

        AtomicBoolean internalInvalid = new AtomicBoolean(false);

        Element element = mock(Element.class);
        when(testObj.getElement()).thenReturn(element);
        when(element.getProperty(eq("invalid"), anyBoolean())).thenAnswer(invocationOnMock -> internalInvalid.get());
        when(element.setProperty(eq("invalid"), anyBoolean())).thenAnswer(invocationOnMock -> {
            boolean arg = invocationOnMock.getArgument(1, Boolean.class);
            internalInvalid.set(arg);
            return null;
        });

        assertFalse(testObj.isInvalid(), "Combobox should NOT be invalid");

        testObj.setInvalid(true);

        assertTrue(testObj.isInvalid(), "Combobox should be invalid");
    }

    @Test
    @DisplayName("should set error message")
    void test_should_set_error_message() {
        UIBuilderComboBox<String> testObj = spy(new UIBuilderComboBox<>());

        AtomicReference<String> internalErrorMessage = new AtomicReference<>("");

        Element element = mock(Element.class);
        when(testObj.getElement()).thenReturn(element);
        when(element.getProperty(eq("errorMessage"))).thenAnswer(invocationOnMock -> internalErrorMessage.get());
        when(element.setProperty(eq("errorMessage"), anyString())).thenAnswer(invocationOnMock -> {
            String arg = invocationOnMock.getArgument(1, String.class);
            internalErrorMessage.set(arg);
            return null;
        });

        assertEquals("", testObj.getErrorMessage());

        testObj.setErrorMessage("Something's wrong");

        assertEquals("Something's wrong", testObj.getErrorMessage());
    }
}
