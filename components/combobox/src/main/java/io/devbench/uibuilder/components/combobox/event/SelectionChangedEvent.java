/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.combobox.event;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.HasValue;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import lombok.Getter;

@Getter
public class SelectionChangedEvent<T> extends ComponentEvent<UIBuilderComboBox<T>> implements HasValue.ValueChangeEvent<T> {

    private final T oldValue;
    private final T value;

    public SelectionChangedEvent(
        UIBuilderComboBox<T> source,
        boolean fromClient,
        T oldValue,
        T newValue
    ) {
        super(source, fromClient);
        this.oldValue = oldValue;
        this.value = newValue;
    }

    @Override
    public HasValue<?, T> getHasValue() {
        return getSource();
    }
}
