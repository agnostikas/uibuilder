/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.combobox.connector;

import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import io.devbench.uibuilder.components.combobox.exceptions.MultiSelectNotSupportedException;
import io.devbench.uibuilder.data.collectionds.mdc.AbstractDataSourceCapableMasterConnector;

import java.util.Collection;
import java.util.Collections;

public class UIBuilderComboBoxMasterConnector<T> extends AbstractDataSourceCapableMasterConnector<UIBuilderComboBox<T>, T> {

    private T selectedItem;
    private Registration registration;

    @SuppressWarnings("unchecked")
    public UIBuilderComboBoxMasterConnector() {
        super((Class) UIBuilderComboBox.class);
    }

    @Override
    protected void onConnect(UIBuilderComboBox<T> masterComponent) {
        super.onConnect(masterComponent);
        registration = masterComponent.addValueChangeListener(event -> {
            this.selectedItem = event.getValue();
            fireSelectionChangedEvent(
                new MasterSelectionChangedEvent<>(
                    event.getSource(),
                    event.isFromClient(),
                    Collections.singleton(event.getOldValue()),
                    Collections.singleton(event.getValue())
                )
            );
        });
    }

    @Override
    public void disconnect() {
        selectedItem = null;
        registration.remove();
    }

    @Override
    public Collection<T> getSelectedItems() {
        if (selectedItem != null) {
            return Collections.unmodifiableCollection(Collections.singleton(selectedItem));
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public void setSelectedItems(Collection<T> items) {
        if (items.size() == 1) {
            getMasterComponent().setValue(items.iterator().next());
        } else if (items.size() == 0) {
            getMasterComponent().setValue(null);
        } else {
            throw new MultiSelectNotSupportedException("The ComboBox can only select one item.");
        }
    }

}
