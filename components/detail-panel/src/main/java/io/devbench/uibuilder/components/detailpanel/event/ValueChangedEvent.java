/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.detailpanel.event;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;

@DomEvent("value-changed")
public class ValueChangedEvent extends ComponentEvent<UIBuilderDetailPanel> {

    private String propertyName;
    private String oldValue;
    private String newValue;

    public ValueChangedEvent(UIBuilderDetailPanel source, boolean fromClient,
                             @EventData("event.detail.propertyName") String propertyName,
                             @EventData("event.detail.oldValue") String oldValue,
                             @EventData("event.detail.newValue") String newValue) {
        super(source, fromClient);
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getNewValue() {
        return newValue;
    }
}
