/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.detailpanel;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.internal.nodefeature.NodeProperties;
import com.vaadin.flow.internal.nodefeature.VirtualChildrenList;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.api.components.HasItemType;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import io.devbench.uibuilder.api.components.form.UIBuilderDetailCapable;
import io.devbench.uibuilder.api.listeners.BackendAttachListener;
import io.devbench.uibuilder.components.detailpanel.event.*;
import io.devbench.uibuilder.components.form.PropertyValidityDescriptorFilterable;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.form.validator.PropertyValidityDescriptor;
import java.io.Serializable;
import java.util.Objects;
import java.util.function.Predicate;
import static com.vaadin.flow.dom.DisabledUpdateMode.*;
import static io.devbench.uibuilder.core.utils.ElementCollector.*;

@Tag(UIBuilderDetailPanel.TAG_NAME)
@JsModule("./uibuilder-detail-panel/src/uibuilder-detail-panel.js")
public class UIBuilderDetailPanel<T extends Serializable>
    extends HasRawElementComponent
    implements HasComponents, HasElement, BackendAttachListener, UIBuilderDetailCapable, PropertyValidityDescriptorFilterable, HasItemType {

    public static final String TAG_NAME = "detail-panel";

    static final String PROP_LEGEND = "legend";
    static final String PROP_HIDE_BORDER = "hideBorder";
    static final String PROP_HIDE_FORM_CONTROLS = "hideFormControls";
    static final String PROP_HIDE_DEFAULT_RESET = "hideDefaultReset";
    static final String PROP_HIDE_DEFAULT_CANCEL = "hideDefaultCancel";
    static final String PROP_HIDE_DEFAULT_SAVE = "hideDefaultSave";
    static final String DISABLED = "disabled";
    static final String PROP_READONLY = "readonly";

    private static final String INTERNAL_FORM_ID = "__internalForm";

    private UIBuilderForm<T> internalForm;
    private Class<T> itemType;

    public UIBuilderDetailPanel() {
        internalForm = instantiateInternalForm();
    }

    private org.jsoup.nodes.Element createVirtualFormRawElement() {
        org.jsoup.nodes.Element element = new org.jsoup.nodes.Element(UIBuilderForm.TAG_NAME);
        element.attr(ID, INTERNAL_FORM_ID);
        element.append(getRawElement().html());
        return element;
    }

    UIBuilderForm<T> instantiateInternalForm() {
        return new UIBuilderForm<>();
    }

    private void initInternalForm() {
        internalForm.setRawElement(createVirtualFormRawElement());
    }

    private void setupInternalFormInjection() {
        Element formElement = internalForm.getElement();
        formElement.setAttribute(ID, INTERNAL_FORM_ID);
        VirtualChildrenList virtualChildrenList = getElement().getNode().getFeature(VirtualChildrenList.class);
        virtualChildrenList.append(formElement.getNode(), NodeProperties.INJECT_BY_ID, INTERNAL_FORM_ID);
    }

    @Override
    public Class<T> getItemType() {
        return itemType;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setItemType(Class<?> itemType) {
        this.itemType = (Class<T>) itemType;
    }

    @Override
    public Predicate<PropertyValidityDescriptor> getPropertyValidityDescriptorPredicate() {
        Objects.requireNonNull(internalForm, "Could not get internal form before attach");
        return internalForm.getPropertyValidityDescriptorPredicate();
    }

    @Override
    public void setPropertyValidityDescriptorPredicate(Predicate<PropertyValidityDescriptor> propertyValidityDescriptorPredicate) {
        Objects.requireNonNull(internalForm, "Could not get internal form before attach");
        internalForm.setPropertyValidityDescriptorPredicate(propertyValidityDescriptorPredicate);
    }

    @Override
    public void onAttached() {
        initInternalForm();
        setupInternalFormInjection();
        internalForm.onAttached();
        internalForm.addFormReadyListener(event -> getElement().callJsFunction("_onDetailReady"));
    }

    public void setItem(T item) {
        if (null == item) {
            setDisabled(true);
        } else {
            setDisabled(false);
        }
        internalForm.setFormItem(item);
    }

    public T getItem() {
        return internalForm.getFormItem();
    }

    public void setInternalFormDirty() {
        internalForm.getElement().callJsFunction("markDirty");
    }

    @Synchronize(value = "hide-border-changed", property = "hideBorder", allowUpdates = ALWAYS)
    public boolean isBorderHidden() {
        return getElement().getProperty(PROP_HIDE_BORDER, true);
    }

    public void setBorderHidden(boolean borderVisible) {
        getElement().setProperty(PROP_HIDE_BORDER, borderVisible);
    }

    @Synchronize(value = "disabled-changed", allowUpdates = ALWAYS)
    public boolean isDisabled() {
        return getElement().getProperty(DISABLED, true);
    }

    public void setDisabled(boolean borderVisible) {
        getElement().setProperty(DISABLED, borderVisible);
    }

    @Synchronize(value = "legend-changed", allowUpdates = ALWAYS)
    public String getLegend() {
        return getElement().getProperty(PROP_LEGEND, "");
    }

    public void setLegend(String legend) {
        getElement().setProperty(PROP_LEGEND, legend);
    }

    @Synchronize(value = "hide-form-controls-changed", property = "hideFormControls", allowUpdates = ALWAYS)
    public boolean isFormControlsHidden() {
        return getElement().getProperty(PROP_HIDE_FORM_CONTROLS, false);
    }

    public void setFormControlsHidden(boolean formControlsHidden) {
        getElement().setProperty(PROP_HIDE_FORM_CONTROLS, formControlsHidden);
    }

    @Synchronize(value = "hide-default-reset-changed", property = "hideDefaultReset", allowUpdates = ALWAYS)
    public boolean isDefaultResetHidden() {
        return getElement().getProperty(PROP_HIDE_DEFAULT_RESET, false);
    }

    public void setDefaultResetHidden(boolean defaultResetHidden) {
        getElement().setProperty(PROP_HIDE_DEFAULT_RESET, defaultResetHidden);
    }

    @Synchronize(value = "hide-default-cancel-changed", property = "hideDefaultCancel", allowUpdates = ALWAYS)
    public boolean isDefaultCancelHidden() {
        return getElement().getProperty(PROP_HIDE_DEFAULT_CANCEL, false);
    }

    public void setDefaultancelHidden(boolean defaultCancelHidden) {
        getElement().setProperty(PROP_HIDE_DEFAULT_CANCEL, defaultCancelHidden);
    }

    @Synchronize(value = "hide-default-save-changed", property = "hideDefaultSave", allowUpdates = ALWAYS)
    public boolean isDefaultSaveHidden() {
        return getElement().getProperty(PROP_HIDE_DEFAULT_SAVE, false);
    }

    public void setDefaultSaveHidden(boolean defaultSaveHidden) {
        getElement().setProperty(PROP_HIDE_DEFAULT_SAVE, defaultSaveHidden);
    }

    @Synchronize(value = {"readonly-changed", "detail-ready"}, allowUpdates = ALWAYS)
    public boolean isReadonly() {
        return getElement().getProperty(PROP_READONLY, false);
    }

    public void setReadonly(Boolean readonly) {
        getElement().setProperty(PROP_READONLY, readonly);
    }

    public Registration addValueChangedListener(ComponentEventListener<ValueChangedEvent> valueChangedListener) {
        return addListener(ValueChangedEvent.class, valueChangedListener);
    }

    public Registration addSaveListener(ComponentEventListener<DetailSaveEvent> detailSaveListener) {
        return addListener(DetailSaveEvent.class, detailSaveListener);
    }

    public Registration addResetListener(ComponentEventListener<DetailResetEvent> detailResetListener) {
        return addListener(DetailResetEvent.class, detailResetListener);
    }

    public Registration addCancelListener(ComponentEventListener<DetailCancelEvent> detailCancelListener) {
        return addListener(DetailCancelEvent.class, detailCancelListener);
    }

    public Registration addReadyListener(ComponentEventListener<DetailReadyEvent> detailReadyListener) {
        return addListener(DetailReadyEvent.class, detailReadyListener);
    }
}
