/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/uibuilder-i18n/i18n-base-mixin.js';
import '@vaadin/flow-frontend/uibuilder/util/z-index-manager/popup-mixin.js';


export class UibuilderDetailPanel extends ThemableMixin(Uibuilder.I18NBaseMixin('detail-panel', PolymerElement)) {

    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                }

                .hide-before:before {
                    display: none !important;
                }

                uibuilder-form {
                    height: 100%;
                }

                [part="fields"] {
                    position: relative;
                    padding-bottom: 4px;
                    margin-bottom: 8px;
                    flex: 1;
                }

                [part="formControl"]::before {
                    content: '';
                    position: absolute;
                    width: calc(100% + 20px);
                    height: 1px;
                    top: -10px;
                    left: -10px;
                    margin-top: 4px;
                }

                [part="formControl"] {
                    position: relative;
                    text-align: right;
                    margin-top: 12px;
                }

                :host([disabled]) [part="modal"] {
                    background-color: rgba(0, 0, 0, 0.4);
                    border-radius: 10px;
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    overflow: auto;
                }

                [part="content"] {
                    position: relative;
                    width: 100%;
                    height: 100%;
                }
            </style>

            <div id="panelContent" part="content">
                <uibuilder-form
                    id="__internalForm"
                    legend="[[legend]]"
                    disabled$="[[disabled]]"
                    hide-border="[[hideBorder]]"
                    type-timeout="[[typeTimeout]]"
                    interruptible-reset="[[interruptibleReset]]"
                    interruptible-cancel="[[interruptibleCancel]]"
                    interruptible-save="[[interruptibleSave]]"
                    save-ack-required="[[saveAckRequired]]"
                    on-form-field-value-change="_formFieldValueChanged"
                    on-save="_onSave"
                    on-reset="_onReset"
                    on-cancel="_onCancel"
                    readonly="[[readonly]]">

                    <div part="fields">
                        <slot></slot>
                    </div>

                    <div id="formControlContainer" hidden$="[[hideFormControls]]" part="formControl">
                        <vaadin-button id="cancelBtn" hidden$="[[hideDefaultCancel]]" form-control="cancel" disabled$="[[disabled]]">
                            Cancel
                        </vaadin-button>
                        <vaadin-button id="resetBtn" hidden$="[[hideDefaultReset]]" form-control="reset" disabled$="[[disabled]]">
                            Reset
                        </vaadin-button>
                        <vaadin-button id="saveBtn" hidden$="[[hideDefaultSave]]" form-control="save" disabled$="[[disabled]]" theme="primary">
                            Save
                        </vaadin-button>
                        <slot name="form-control"></slot>
                    </div>

                </uibuilder-form>
                <div id="modal" part="modal"></div>
            </div>
        `;
    }

    static get is() {
        return 'detail-panel'
    }

    static get properties() {
        return {
            legend: {
                type: String,
                notify: true,
                value: ''
            },
            hideBorder: {
                type: Boolean,
                notify: true,
                value: false
            },
            typeTimeout: {
                type: Number,
                value: 300
            },
            disabled: {
                type: Boolean,
                value: false,
                notify: true,
                observe: '_disabledObserver'
            },
            hideDefaultCancel: {
                type: Boolean,
                notify: true,
                value: false
            },
            hideDefaultReset: {
                type: Boolean,
                notify: true,
                value: false
            },
            hideDefaultSave: {
                type: Boolean,
                notify: true,
                value: false
            },
            hideFormControls: {
                type: Boolean,
                notify: true,
                value: false
            },
            interruptibleReset: {
                type: Boolean,
                value: false
            },
            interruptibleCancel: {
                type: Boolean,
                value: false
            },
            interruptibleSave: {
                type: Boolean,
                value: false
            },
            saveAckRequired: {
                type: Boolean,
                value: false
            },
            readonly: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_onReadonlyChange'
            }
        };
    }

    constructor() {
        super();
    }

    ready() {
        super.ready();
    }

    connectedCallback() {
        super.connectedCallback();
        this.shadowRoot.querySelector("#cancelBtn").textContent = this.tr('Cancel');
        this.shadowRoot.querySelector("#resetBtn").textContent = this.tr('Reset');
        this.shadowRoot.querySelector("#saveBtn").textContent = this.tr('Save');
    }

    markDirty() {
        this.$.__internalForm.markDirty();
    }

    markNewItem() {
        this.$.__internalForm.markNewItem();
    }

    forceResetForm() {
        this.$.__internalForm._doReset(false, false);
        this.dispatchEvent(new CustomEvent("cancel", {detail: {internal: true}}));
    }

    _initInternalForm() {
        this.$.__internalForm._fireFormFieldChangeEvent();
    }

    _formFieldValueChanged(e) {
        if (!e.detail.resetInProgress) {
            this.dispatchEvent(new CustomEvent("value-changed", {
                detail: {
                    propertyName: e.detail.propertyName,
                    oldValue: e.detail.oldPropertyValue,
                    newValue: e.detail.newPropertyValue
                }
            }));
        }
    }

    _onSave(event) {
        this.dispatchEvent(new CustomEvent("save", event));
    }

    _onReset(event) {
        this.dispatchEvent(new CustomEvent("reset", event));
    }

    _onCancel(event) {
        this.dispatchEvent(new CustomEvent("cancel", event));
    }

    _onDetailReady() {
        this.dispatchEvent(new CustomEvent("detail-ready"));
    }

    _onReadonlyChange() {
        if (this.readonly) {
            this.$.formControlContainer.classList.add('hide-before');
        } else {
            this.$.formControlContainer.classList.remove('hide-before');
        }
    }
}

customElements.define(UibuilderDetailPanel.is, UibuilderDetailPanel);

window.Uibuilder = window.Uibuilder || {};
window.Uibuilder.formContainerTags = window.Uibuilder.formContainerTags || new Set();
window.Uibuilder.formContainerTags.add(UibuilderDetailPanel.is);
