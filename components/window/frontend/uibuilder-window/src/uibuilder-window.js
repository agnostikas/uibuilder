/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { GestureEventListeners } from '@polymer/polymer/lib/mixins/gesture-event-listeners.js';
import { FlattenedNodesObserver } from '@polymer/polymer/lib/utils/flattened-nodes-observer.js';
import { IronResizableBehavior } from '@polymer/iron-resizable-behavior/iron-resizable-behavior.js';
import * as PolymerLegacy from '@polymer/polymer/lib/legacy/class.js'
import { ElementMixin } from '@vaadin/vaadin-element-mixin/vaadin-element-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import '@vaadin/flow-frontend/uibuilder/util/z-index-manager/popup-mixin.js';

/**
 * @property {boolean} opened
 * @property {string} headerText
 * @property {'none'|'vertical'|'horizontal'|'both'} resize
 * @property {boolean} closeable
 * @property {boolean} keepOpenOnCloseButton
 * @property {{closeButton: HTMLElement, windowDiv: HTMLElement, windowDivHeader: HTMLElement}} $
 */
export class UIBuilderWindowElement extends Uibuilder.PopupMixin(
    ElementMixin(ThemableMixin(GestureEventListeners(PolymerLegacy.mixinBehaviors([IronResizableBehavior], PolymerElement))))) {

    static get template() {
        return html`
            <style>
                :host {
                    --header-height: 28px;
                    --header-padding: 6px;

                    display: none;
                    position: absolute;
                }

                [part="header"] {
                    position: relative;
                    padding: var(--header-padding);
                    cursor: move;
                    height: var(--header-height);
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }

                [part="header"] label {
                    cursor: inherit;
                }

                [part="body"] {
                    border: lightgrey;
                    padding: 0.5em;
                    box-sizing: border-box;
                    width: 100%;
                    height: calc(100% - var(--header-height) - 2 * var(--header-padding) - 10px);
                    overflow: auto;
                }

                [part="modal"] {
                    background-color: rgba(0, 0, 0, 0.4);
                    position: fixed;
                    z-index: inherit;
                    padding-top: 100px;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    overflow: auto;
                }

                [part="modalContent"] {
                    position: fixed;
                    background-color: #F1F1F1;
                    margin: auto;
                    resize: vertical;
                    min-width: 150px;
                    min-height: 150px;
                    overflow: hidden;
                    text-align: center;
                    user-select: none;
                    z-index: inherit;
                    transition: 0.4s;
                    transition-property: opacity, transform;
                }

                #windowDiv {
                    width: inherit;
                    height: inherit;
                }

                [part="closeButton"] {
                    cursor: pointer;
                }

                .closeButtonWrapper {
                    position: absolute;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    width: calc(var(--header-height) + 2 * var(--header-padding));
                    top: 0;
                    right: 0;
                    bottom: 0;
                }
            </style>

            <div id="modalDiv" part="modal"></div>
            <div id="windowDiv" part="modalContent">
                <div id="windowDivHeader" part="header">
                    <label id="headerLabel">[[headerText]]</label>
                    <div class="closeButtonWrapper">
                        <i id="closeButton" part="closeButton" on-click="_onCloseButtonPressed"></i>
                    </div>
                </div>
                <div part="body">
                    <slot id="layout" name="layout"></slot>
                </div>
            </div>
        `;
    }

    static get is() {
        return 'uibuilder-window';
    }

    static get version() {
        return '0.1';
    }

    static get properties() {
        return {
            opened: {
                type: Boolean,
                value: false,
                observer: '_onOpenedChanged',
                notify: true,
                reflectToAttribute: true,
            },
            headerText: {
                type: String,
                notify: true,
            },
            resize: {
                type: String,
                value: "vertical",
                notify: true,
                observer: '_changeResizeValue'
            },
            closeable: {
                type: Boolean,
                value: false,
                notify: true,
                observer: '_changeCloseableEvent'
            },
            keepOpenOnCloseButton: {
                type: Boolean,
                value: false
            }
        }
    }

    ready() {
        super.ready();
        new FlattenedNodesObserver(this, this._processChildren);

        window.addEventListener('resize', this._windowResizeListener = () => this.opened && this.center());

        document.addEventListener('keydown', this._documentKeydownListener = event => {
            if (event.key === "Escape") {
                this._onCloseButtonPressed()
            }
        });

        this._initSizeObserver();

        dragElement(this.$.windowDiv, this.$.windowDivHeader);

        function dragElement(element, header) {
            let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
            if (header) {
                /* if present, the header is where you move the DIV from:*/
                header.onmousedown = dragMouseDown;
            } else {
                /* otherwise, move the DIV from anywhere inside the DIV:*/
                element.onmousedown = dragMouseDown;
            }

            function dragMouseDown(e) {
                e = e || window.event;
                // get the mouse cursor position at startup:
                pos3 = e.clientX;
                pos4 = e.clientY;
                document.onmouseup = closeDragElement;
                // call a function whenever the cursor moves:
                document.onmousemove = elementDrag;
            }

            function elementDrag(e) {
                e = e || window.event;
                // calculate the new cursor position:
                pos1 = pos3 - e.clientX;
                pos2 = pos4 - e.clientY;
                pos3 = e.clientX;
                pos4 = e.clientY;
                // set the element's new position:

                const newTop = element.offsetTop - pos2;
                const newLeft = element.offsetLeft - pos1;

                if (newLeft > 0 && newTop > 0
                    && newLeft + element.offsetWidth < window.innerWidth
                    && newTop + element.offsetHeight < window.innerHeight) {
                    element.style.top = `${newTop}px`;
                    element.style.left = `${newLeft}px`;
                }
            }

            function closeDragElement() {
                /* stop moving when mouse button is released:*/
                document.onmouseup = null;
                document.onmousemove = null;
            }
        }
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        document.removeEventListener('keydown', this._documentKeydownListener);
        window.removeEventListener('resize', this._windowResizeListener);
    }

    _initSizeObserver() {
        const {windowDiv} = this.$;
        const {width, height} = windowDiv.style;
        new MutationObserver(() => {
            if (windowDiv.style.width !== width || windowDiv.style.height !== height) {
                if (windowDiv.offsetLeft + windowDiv.offsetWidth > window.innerWidth)
                    windowDiv.style.width = `${window.innerWidth - windowDiv.offsetLeft}px`;
                if (windowDiv.offsetTop + windowDiv.offsetHeight > window.innerHeight)
                    windowDiv.style.height = `${window.innerHeight - windowDiv.offsetTop}px`;
                this.notifyResize();
            }
        })
            .observe(windowDiv, {attributes: true, attributeFilter: ['style']});
    }

    _onCloseButtonPressed() {
        if (!this.closeable) {
            return;
        }

        if (this.keepOpenOnCloseButton)
            this.dispatchEvent(new CustomEvent('close-button-pressed'));
        else
            this._closePopup();
    }

    _closePopup() {
        this.opened = false;
        this.dispatchEvent(new CustomEvent('value-changed', {opened: false}));
        this._onClose();
    }

    _onOpenedChanged() {
        if (this.opened) {
            this.style.display = "inline-flex";
            this.$.windowDiv.style.opacity = '0';
            this.$.windowDiv.style.transform = 'translateY(-20px)';
            this.center();
            setTimeout(() => {
                this.$.windowDiv.style.opacity = '1';
                this.$.windowDiv.style.transform = '';
            });
        } else {
            this.style.display = "none";
        }
    }

    center() {
        this.$.windowDiv.style.left = `${window.innerWidth / 2 - this.$.windowDiv.offsetWidth / 2}px`;
        this.$.windowDiv.style.top = `${window.innerHeight / 2 - this.$.windowDiv.offsetHeight / 2}px`;
    }

    _changeCloseableEvent() {
        this.$.closeButton.style.display = this.closeable ? 'inline-flex' : 'none';
    }

    _changeResizeValue() {
        this.$.windowDiv.style.resize = this.resize;
    }

    _processChildren() {
        this.getEffectiveChildren().forEach(child => {
            child.setAttribute('slot', 'layout');
        });
    }

    _onClose() {
        this.dispatchEvent(new CustomEvent('close'));
    }

}

customElements.define(UIBuilderWindowElement.is, UIBuilderWindowElement);
