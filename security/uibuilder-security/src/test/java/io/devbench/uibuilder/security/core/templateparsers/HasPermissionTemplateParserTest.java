/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.templateparsers;

import io.devbench.uibuilder.security.core.templateparsers.permission.HasPermissionTemplateParser;
import io.devbench.uibuilder.test.annotations.LoadElement;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class HasPermissionTemplateParserTest extends TemplateParserTestBase {
    @LoadElement(value = "/security-template-tests.html", id = "hasPermission")
    private Element testElement;

    @LoadElement(value = "/security-template-tests.html", id = "authenticatedTest")
    private Element anotherElement;

    private HasPermissionTemplateParser testObj = new HasPermissionTemplateParser();

    @Test
    void should_remove_has_permission_nodes_if_the_user_do_not_has_role() {
        when(securityService.isPermitted(anyString())).thenReturn(false);

        walkElementWithTemplateParser(testElement, testObj);

        assertEquals(normalizeHtml("<uibuilder-page id=\"hasPermission\">\n" +
                "    <div>Other content</div>\n" +
                "</uibuilder-page>"),
            normalizeHtml(testElement.outerHtml()));
    }

    @Test
    public void should_replace_has_any_role_nodes_with_a_div_containing_the_same_elements_as_the_has_role_mode_if_the_user_has_role() {
        when(securityService.isPermitted(anyString())).thenReturn(true);

        walkElementWithTemplateParser(testElement, testObj);

        assertEquals(
            normalizeHtml("<uibuilder-page id=\"hasPermission\">\n" +
                "    <div>Hello logged in user!</div>\n" +
                "    <div>Other content</div>\n" +
                "</uibuilder-page>"),
            normalizeHtml(testElement.outerHtml())
        );
    }

    @Test
    public void should_not_replace_anything() {
        when(securityService.isAuthenticated()).thenReturn(true);

        walkElementWithTemplateParser(anotherElement, testObj);

        assertEquals(
            normalizeHtml("<uibuilder-page id=\"authenticatedTest\">\n" +
                "    <authenticated>\n" +
                "        <div>Hello logged in user!</div>\n" +
                "    </authenticated>\n" +
                "    <div>Other content</div>\n" +
                "</uibuilder-page>\n"),
            normalizeHtml(anotherElement.outerHtml())
        );
    }


}
