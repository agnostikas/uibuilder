/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.flow.xml;

import io.devbench.uibuilder.core.flow.exceptions.FlowUnauthorized;
import io.devbench.uibuilder.security.api.SecurityService;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class HasPermissionFlowElementTest {

    @Mock
    @SingletonInstance(SecurityService.class)
    private SecurityService securityService;

    @Test
    public void should_return_empty_optional_when_user_has_the_permission() {
        HasPermissionFlowElement testObj = new HasPermissionFlowElement("testPermission", "fallBackId", Collections.emptyList());

        when(securityService.isPermitted("testPermission")).thenReturn(true);

        assertEquals(Optional.empty(), testObj.convertToParsedElement().calcAlternativeFlowId());

        verify(securityService).isPermitted("testPermission");
    }

    @Test
    public void should_return_alternative_flow_id_when_user_not_have_the_permission() {
        HasPermissionFlowElement testObj = new HasPermissionFlowElement("testPermission", "fallBackId", Collections.emptyList());

        when(securityService.isPermitted("testPermission")).thenReturn(false);

        assertEquals(Optional.of("fallBackId"), testObj.convertToParsedElement().calcAlternativeFlowId());

        verify(securityService).isPermitted("testPermission");
    }

    @Test
    public void should_split_the_permissions_with_comma_separator() {
        HasPermissionFlowElement testObj = new HasPermissionFlowElement("testPermission1,testPermission2", "fallBackId", Collections.emptyList());

        when(securityService.isPermitted("testPermission1")).thenReturn(false);
        when(securityService.isPermitted("testPermission2")).thenReturn(false);

        assertEquals(Optional.of("fallBackId"), testObj.convertToParsedElement().calcAlternativeFlowId());

        verify(securityService).isPermitted("testPermission1");
        verify(securityService).isPermitted("testPermission2");
    }

    @Test
    public void should_split_the_permissions_with_comma_separator_and_handle_whitespaces_correctly() {
        HasPermissionFlowElement testObj = new HasPermissionFlowElement("  testPermission1 , testPermission2 ", "fallBackId", Collections.emptyList());

        when(securityService.isPermitted("testPermission1")).thenReturn(false);
        when(securityService.isPermitted("testPermission2")).thenReturn(false);

        assertEquals(Optional.of("fallBackId"), testObj.convertToParsedElement().calcAlternativeFlowId());

        verify(securityService).isPermitted("testPermission1");
        verify(securityService).isPermitted("testPermission2");
    }

    @Test
    public void should_throw_flow_unauthorized_exception_when_the_user_is_not_authenticated_and_there_is_no_fallback_id() {
        HasPermissionFlowElement testObj = new HasPermissionFlowElement("testPermission1", null, Collections.emptyList());

        when(securityService.isPermitted("testPermission1")).thenReturn(false);

        assertThrows(FlowUnauthorized.class, () -> testObj.convertToParsedElement().calcAlternativeFlowId());

        verify(securityService).isPermitted("testPermission1");
    }
}
