/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.templateparsers;

import io.devbench.uibuilder.test.annotations.LoadElement;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PrincipalTemplateParserTest extends TemplateParserTestBase {
    @LoadElement(value = "/security-template-tests.html", id = "principal")
    private Element testElement;

    @LoadElement(value = "/security-template-tests.html", id = "authenticatedTest")
    private Element anotherElement;

    private PrincipalTemplateParser testObj = new PrincipalTemplateParser();


    @Test
    void should_replace_principal_nodes() {
        when(securityService.getPrincipal()).thenReturn("Zsombor");

        walkElementWithTemplateParser(testElement, testObj);

        assertEquals(normalizeHtml("<uibuilder-page id=\"principal\">" +
                "        <div> Hello <div>Zsombor</div> user! </div>" +
                "</uibuilder-page>"),
            normalizeHtml(testElement.outerHtml()));
    }

    @Test
    public void should_not_replace_anything() {
        when(securityService.isAuthenticated()).thenReturn(true);

        walkElementWithTemplateParser(anotherElement, testObj);

        assertEquals(
            normalizeHtml("<uibuilder-page id=\"authenticatedTest\">\n" +
                "    <authenticated>\n" +
                "        <div>Hello logged in user!</div>\n" +
                "    </authenticated>\n" +
                "    <div>Other content</div>\n" +
                "</uibuilder-page>"),
            normalizeHtml(anotherElement.outerHtml())
        );
    }
}
