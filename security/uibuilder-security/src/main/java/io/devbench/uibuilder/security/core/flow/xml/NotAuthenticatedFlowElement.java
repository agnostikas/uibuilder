/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.flow.xml;

import io.devbench.uibuilder.api.singleton.SingletonManager;
import io.devbench.uibuilder.core.flow.exceptions.FlowUnauthorized;
import io.devbench.uibuilder.core.flow.parsed.ParsedFlowElement;
import io.devbench.uibuilder.core.flow.xml.FlowElement;
import io.devbench.uibuilder.core.flow.xml.FlowTargetInherited;
import io.devbench.uibuilder.core.flow.xml.HasSubFlow;
import io.devbench.uibuilder.security.api.SecurityService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@XmlRootElement(name = "notAuthenticated")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class NotAuthenticatedFlowElement extends FlowElement implements HasSubFlow, FlowTargetInherited {

    @XmlAttribute
    private String fallbackId;

    @XmlElementRef
    private List<FlowElement> subFlows = new ArrayList<>();

    private Optional<String> getAlternativeNavigationId() {
        if (!SingletonManager.getInstanceOf(SecurityService.class).isAuthenticated()) {
            return Optional.empty();
        } else {
            if (fallbackId != null) {
                return Optional.of(fallbackId);
            } else {
                throw new FlowUnauthorized();
            }
        }
    }

    @Override
    public ParsedFlowElement convertToParsedElement() {
        return new ParsedFlowElement(UUID.randomUUID().toString(), this::getAlternativeNavigationId);
    }
}

