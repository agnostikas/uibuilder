/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.templateparsers.permission;

import io.devbench.uibuilder.api.singleton.SingletonManager;
import io.devbench.uibuilder.security.api.SecurityService;
import io.devbench.uibuilder.security.core.templateparsers.TagNameMarkedTemplateParser;
import io.devbench.uibuilder.security.core.templateparsers.TagReplaceTemplateParser;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;

public class HasPermissionTemplateParser implements TagNameMarkedTemplateParser, TagReplaceTemplateParser {
    @Getter
    private final String tagName = "has-permission";
    private final String attribute = "name";

    @Override
    public boolean test(Element element) {
        final String permission = element.attr(attribute);
        return StringUtils.isNotBlank(permission) && SingletonManager.getInstanceOf(SecurityService.class).isPermitted(permission);
    }
}
