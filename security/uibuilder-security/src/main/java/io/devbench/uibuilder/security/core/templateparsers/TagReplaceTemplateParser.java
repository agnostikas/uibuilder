/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.templateparsers;

import io.devbench.uibuilder.api.parse.backendtemplate.TemplateParser;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public interface TagReplaceTemplateParser extends TemplateParser {

    boolean test(Element element);

    @Override
    default Elements processTemplate(Element element) {
        if (test(element)) {
            return element.children().size() == 1 ?
                new Elements(element.child(0)) :
                new Elements(element.children());
        } else {
            return new Elements();
        }
    }

}
