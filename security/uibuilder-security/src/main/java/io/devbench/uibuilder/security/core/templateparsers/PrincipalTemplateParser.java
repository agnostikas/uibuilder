/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.security.core.templateparsers;

import io.devbench.uibuilder.api.singleton.SingletonManager;
import io.devbench.uibuilder.security.api.SecurityService;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.reflect.Method;

import static org.apache.commons.lang3.StringUtils.*;

public class PrincipalTemplateParser implements TagNameMarkedTemplateParser {
    @Getter
    private final String tagName = "principal";
    private final String attribute = "property";

    @Override
    public Elements processTemplate(Element element) {
        final String methodName = element.attr(attribute);
        final Element div = new Element("div");
        Object principal = SingletonManager.getInstanceOf(SecurityService.class).getPrincipal();
        if (null != principal) {
            div.text(getValue(principal, methodName));
        }
        return new Elements(div);
    }

    private String principalMethodName(String role) {
        return "get" + StringUtils.capitalize(role);
    }

    private String getValue(Object principal, String name) {
        if (isBlank(name)) {
            return principal.toString();
        } else {
            try {
                final Class<?> principalClass = principal.getClass();
                final Method method = principalClass.getMethod(principalMethodName(name));
                return method.invoke(principal).toString();
            } catch (Throwable e) {
                return principal.toString();
            }
        }
    }
}
