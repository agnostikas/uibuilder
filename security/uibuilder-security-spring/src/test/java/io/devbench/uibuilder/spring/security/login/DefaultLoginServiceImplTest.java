/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.security.login;

import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Mockito.eq;

@ExtendWith(MockitoExtension.class)
class DefaultLoginServiceImplTest extends AbstractShiroTest {

    private MockService testObj;

    private FlowManager flowManager;

    @BeforeEach
    void setUp() {
        testObj = new MockService();
        DefaultSecurityManager securityManager = new DefaultSecurityManager(getSimpleAccountRealm());
        setSecurityManager(securityManager);
        Subject subject = new Subject.Builder(getSecurityManager()).buildSubject();
        setSubject(subject);
        flowManager = new FlowManager("/flow.xml");
    }

    @Test
    @DisplayName("should login successfully")
    void test_should_login_successfully() {
        assertAll(
            () -> assertTrue(testObj.login("admin", "admin")),
            () -> assertTrue(testObj.login("user", "user"))
        );
    }

    @Test
    @DisplayName("should get authentication failed exception on login")
    void test_should_get_authentication_failed_exception_on_login() {
        assertFalse(testObj.login(or(not(eq("admin")), not(eq("user"))), or(not(eq("admin")), not(eq("user")))));
    }

    @AfterEach
    void clear() {
        tearDownShiro();
    }

    private SimpleAccountRealm getSimpleAccountRealm() {
        SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();
        simpleAccountRealm.addAccount("admin", "admin");
        simpleAccountRealm.addAccount("user", "user");
        simpleAccountRealm.setRolePermissionResolver(roleString -> Collections.singleton(new WildcardPermission(roleString)));
        return simpleAccountRealm;
    }

    class MockService extends DefaultLoginServiceImpl {
        public Boolean login(String username, String password) {
            Subject subject = getSubject();
            try {
                subject.login(createTokenWithUsernameAndPassword(username, password));
            } catch (AuthenticationException e) {
                return false;
            }
            return true;
        }
    }
}
