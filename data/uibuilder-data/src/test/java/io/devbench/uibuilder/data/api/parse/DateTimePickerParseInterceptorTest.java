/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse;

import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(BaseUIBuilderTestExtension.class)
class DateTimePickerParseInterceptorTest {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE_TIME;

    private DateTimePickerParseInterceptor testObj;
    private DateTimePicker picker;
    private Element element;

    @BeforeEach
    void setUp() {
        testObj = new DateTimePickerParseInterceptor();
        picker = new DateTimePicker();
        element = new Element("vaadin-date-time-picker");
    }

    @Test
    @DisplayName("Should set step attribute")
    void test_should_set_step_attribute() {
        assertEquals(3600, picker.getStep().getSeconds());

        element.attr("step", "900");
        testObj.intercept(picker, element);

        assertEquals(900, picker.getStep().getSeconds());
    }

    @Test
    @DisplayName("Should set min attribute")
    void test_should_set_min_attribute() {
        assertNull(picker.getMin());

        element.attr("min", "1999-01-01T00:00:00");
        testObj.intercept(picker, element);

        assertEquals(toDateTime("1999-01-01T00:00:00"), picker.getMin());
    }

    @Test
    @DisplayName("Should set max attribute")
    void test_should_set_max_attribute() {
        assertNull(picker.getMax());

        element.attr("max", "2049-01-01T00:00:00");
        testObj.intercept(picker, element);

        assertEquals(toDateTime("2049-01-01T00:00:00"), picker.getMax());
    }

    @Test
    @DisplayName("Should set placeholders")
    void test_should_set_placeholders() {
        assertNull(picker.getDatePlaceholder());
        assertNull(picker.getTimePlaceholder());

        element.attr("date-placeholder", "Here goes the date");
        element.attr("time-placeholder", "Here goes the time");
        testObj.intercept(picker, element);

        assertEquals("Here goes the date", picker.getDatePlaceholder());
        assertEquals("Here goes the time", picker.getTimePlaceholder());
    }

    private LocalDateTime toDateTime(String dateTimeString) {
        return LocalDateTime.from(FORMATTER.parse(dateTimeString));
    }

}
