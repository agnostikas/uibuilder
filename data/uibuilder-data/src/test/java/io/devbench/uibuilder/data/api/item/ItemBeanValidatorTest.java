/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.item;

import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.Builder;
import lombok.Data;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Validator;
import javax.validation.constraints.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ItemBeanValidatorTest {

    @InjectMocks
    private ItemBeanValidator testObj;

    @Mock
    private Validator validator;

    @Test
    @DisplayName("Should validate")
    void test_should_validate() {
        TestItem item = TestItem.builder().name("John Doe").age(22).other("Foo").build();
        Set<String> propertyPaths = new HashSet<>(Arrays.asList("name", "age"));

        doReturn(Collections.emptySet()).when(validator).validate(item);

        Map<String, String> resultMap = testObj.validate(propertyPaths, item);

        assertNotNull(resultMap);
        assertTrue(resultMap.isEmpty());

        Map<String, String> emptyResult = testObj.validate(Collections.emptySet(), item);
        assertNotNull(emptyResult);
        assertNotSame(resultMap, emptyResult);
        assertTrue(emptyResult.isEmpty());
    }

    @Test
    @DisplayName("Should validate with errors")
    void test_should_validate_with_errors() {
        TestItem item = TestItem.builder().name("John Doe").age(22).other("Foo").build();
        Set<String> propertyPaths = new HashSet<>(Collections.singletonList("age"));

        doReturn(new HashSet<>(Arrays.asList(
            createMockValidatorConstraint("name", "Name should not be empty"),
            createMockValidatorConstraint("age", "Age cannot be greater than 30"),
            createMockValidatorConstraint("other", "Other should start with character \"A\"")
        ))).when(validator).validate(item);

        Map<String, String> resultMap = testObj.validate(propertyPaths, item);

        assertNotNull(resultMap);
        assertFalse(resultMap.isEmpty());
        assertEquals("Age cannot be greater than 30", resultMap.get("age"));
        String errorMessagesWithoutPath = resultMap.get("");
        assertTrue(errorMessagesWithoutPath.contains("Name should not be empty"));
        assertTrue(errorMessagesWithoutPath.contains("Other should start with character \"A\""));
    }

    private ConstraintViolation<?> createMockValidatorConstraint(String path, String message) {
        ConstraintViolation<?> constraintViolation = mock(ConstraintViolation.class);
        Path propertyPath = mock(Path.class);
        when(constraintViolation.getPropertyPath()).thenReturn(propertyPath);
        when(propertyPath.toString()).thenReturn(path);
        when(constraintViolation.getMessage()).thenReturn(message);
        return constraintViolation;
    }

    @Data
    @Builder
    static class TestItem {

        @NotNull(message = "Name should not be null")
        @NotEmpty(message = "Name should not be empty")
        private String name;

        @Min(value = 10, message = "Age cannot be less than 10")
        @Max(value = 30, message = "Age cannot be greater than 30")
        private Integer age;

        @Pattern(regexp = "^A", message = "Other should start with character \"A\"")
        private String other;

    }

}
