/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.filter.validation;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionDescriptor;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.io.Serializable;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class FilterValidationExecutorTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Mock
    private FilterExpression filterExpression;

    @Mock
    private FilterExpressionDescriptor descriptor;

    private FilterValidationExecutor testObj;

    @BeforeEach
    private void setup() {
        FilterValidationExecutor.registerToActiveContext();
        testObj = FilterValidationExecutor.getInstance();
        when(memberScanner.findClassesByAnnotation(CustomFilterValidator.class)).thenReturn(Collections.singleton(TestFilterValidator.class));
    }

    @Test
    @DisplayName("Should find the validator by its name, and validate the expression against it and return the report from it")
    public void should_find_the_validator_by_its_name_and_validate_the_expression_against_it_and_return_the_report_from_it() {
        when(filterExpression.getImmutableDescriptor()).thenReturn(descriptor);

        FilterValidationReport report = testObj.validate("testValidator", filterExpression);

        assertAll(
            () -> verify(memberScanner).findClassesByAnnotation(CustomFilterValidator.class),
            () -> assertNotNull(report),
            () -> assertTrue(report.isInvalid()),
            () -> assertTrue(report.hasErrors()),
            () -> assertTrue(report.hasWarnings()),
            () -> assertEquals(1, report.getErrors().size()),
            () -> assertEquals(1, report.getWarnings().size()),
            () -> assertEquals("test error message", report.getErrors().get(0)),
            () -> assertEquals("test warn message", report.getWarnings().get(0))
        );
    }

    @Test
    @DisplayName("Should throw exception when the filter validator cannot be found with the given name")
    public void should_throw_exception_when_the_filter_validator_cannot_be_found_with_the_given_name() {
        FilterValidatorNotFoundException exception = assertThrows(FilterValidatorNotFoundException.class,
            () -> testObj.validate("INVALID NAME", filterExpression));
        assertEquals("Filter validator not found with name: INVALID NAME", exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception when the named filter validator doesn't implement the FilterValidator interface")
    public void should_throw_exception_when_the_named_filter_validator_doesn_t_implement_the_filter_validator_interface() {
        when(memberScanner.findClassesByAnnotation(CustomFilterValidator.class)).thenReturn(Collections.singleton(InvalidTypeFilterValidator.class));

        FilterValidatorNotFoundException exception = assertThrows(FilterValidatorNotFoundException.class,
            () -> testObj.validate("invalidValidator", filterExpression));
        assertEquals("Filter validator not found with name: invalidValidator", exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception when the validator name or the expression is null")
    public void should_throw_exception_when_the_validator_name_or_the_expression_is_null() {
        assertAll(
            () -> assertThrows(NullPointerException.class, () -> testObj.validate(null, null)),
            () -> assertThrows(NullPointerException.class, () -> testObj.validate(null, filterExpression)),
            () -> assertThrows(NullPointerException.class, () -> testObj.validate("nonnullname", null))
        );
    }


    @CustomFilterValidator("testValidator")
    public static class TestFilterValidator implements Serializable, FilterValidator<FilterExpressionDescriptor> {

        @Override
        public void validate(FilterExpressionDescriptor expression, FilterValidationContext context) {
            context.error("test error message");
            context.warn("test warn message");
        }
    }

    @CustomFilterValidator("invalidValidator")
    public static class InvalidTypeFilterValidator {

    }

}
