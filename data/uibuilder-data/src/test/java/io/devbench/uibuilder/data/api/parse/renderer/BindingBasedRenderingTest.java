/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse.renderer;

import elemental.json.Json;
import elemental.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class BindingBasedRenderingTest {

    private BindingBasedRendering testObj;

    @Test
    public void should_mapValueToKeyInJsonObjectGraph_item_id() {
        testObj = new BindingBasedRendering(null, null);
        JsonObject expected = Json.instance().parse("{\"id\":\"value\"}");
        JsonObject jsonObject = Json.createObject();
        testObj.mapValueToKeyInJsonObjectGraph(jsonObject, "item.id", "value");
        assertEquals(expected.toString(), jsonObject.toString());
    }

    @Test
    public void should_mapValueToKeyInJsonObjectGraph_item_entity_id() {
        testObj = new BindingBasedRendering(null, null);
        JsonObject expected = Json.instance().parse("{\"entity\":{\"id\":\"value\"}}");
        JsonObject jsonObject = Json.createObject();
        testObj.mapValueToKeyInJsonObjectGraph(jsonObject, "item.entity.id", "value");
        assertEquals(expected.toString(), jsonObject.toString());
    }

}
