/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.DataGenerator;
import com.vaadin.flow.data.renderer.Rendering;
import com.vaadin.flow.dom.Element;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.parse.renderer.ItemsTemplateRenderer;
import lombok.Data;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


class ItemsTemplateRendererTest {

    private ItemsTemplateRenderer<TestEntity> testObj = ItemsTemplateRenderer.of("<div>{{item.id}}</div>", () -> ClassMetadata.ofClass(TestEntity.class));

    @Test
    @DisplayName("Should render property value with the passed template")
    public void should_render_property_value_with_the_passed_template() {
        TestEntity testEntity = new TestEntity();
        testEntity.id = "testIdValue";
        testObj.withProperty("item.id");
        Grid<TestEntity> testGrid = new Grid<>();
        Grid.Column<TestEntity> testColumn = testGrid.addColumn(testObj);
        Element templateElement = new Element("template");
        Rendering<TestEntity> templateRendering = testObj.render(testColumn.getElement(), testGrid.getDataCommunicator().getKeyMapper(), templateElement);
        Optional<DataGenerator<TestEntity>> optionalDataGenerator = templateRendering.getDataGenerator();

        assertAll(
            () -> assertEquals("<div>{{item.id}}</div>", templateElement.getProperty("innerHTML")),
            () -> assertTrue(optionalDataGenerator.isPresent()),
            () -> {
                JsonObject jsonObject = Json.createObject();
                optionalDataGenerator.get().generateData(testEntity, jsonObject);
                assertEquals("testIdValue", jsonObject.getString("id"));
            }
        );

    }


    @Test
    @DisplayName("Should render property value from master entity")
    public void should_render_property_value_with_the_passed_template_TEntity_id() {
        ItemsTemplateRenderer<TEntity> testObj = ItemsTemplateRenderer.of("<div>{{item.id}}</div>", () -> ClassMetadata.ofClass(TEntity.class));
        TEntity masterEntity = new TEntity();
        TestEntity detailEntity = new TestEntity();
        detailEntity.id = "testIdValue";
        masterEntity.setDetailEntity(detailEntity);
        masterEntity.id = "tIdValue";

        testObj.withProperty("item.id");
        Grid<TEntity> testGrid = new Grid<>();
        Grid.Column<TEntity> testColumn = testGrid.addColumn(testObj);
        Element templateElement = new Element("template");
        Rendering<TEntity> templateRendering = testObj.render(testColumn.getElement(), testGrid.getDataCommunicator().getKeyMapper(), templateElement);
        Optional<DataGenerator<TEntity>> optionalDataGenerator = templateRendering.getDataGenerator();

        assertAll(
            () -> assertEquals("<div>{{item.id}}</div>", templateElement.getProperty("innerHTML")),
            () -> assertTrue(optionalDataGenerator.isPresent()),
            () -> {
                JsonObject jsonObject = Json.createObject();
                optionalDataGenerator.get().generateData(masterEntity, jsonObject);
                assertEquals("tIdValue", jsonObject.getString("id"));

            }
        );

    }

    @Test
    @DisplayName("Should render property value from detail entity")
    public void should_render_property_value_with_the_passed_template_TEntity_TestEntity_id() {
        ItemsTemplateRenderer<TEntity> testObj = ItemsTemplateRenderer.of(
            "<div>{{item.detailEntity.id}}</div>",
            () -> ClassMetadata.ofClass(TEntity.class));
        TEntity masterEntity = new TEntity();
        TestEntity detailEntity = new TestEntity();
        detailEntity.id = "testIdValue";
        masterEntity.setDetailEntity(detailEntity);
        masterEntity.id = "tIdValue";

        testObj.withProperty("item.detailEntity.id");
        Grid<TEntity> testGrid = new Grid<>();
        Grid.Column<TEntity> testColumn = testGrid.addColumn(testObj);
        Element templateElement = new Element("template");
        Rendering<TEntity> templateRendering = testObj.render(testColumn.getElement(), testGrid.getDataCommunicator().getKeyMapper(), templateElement);
        Optional<DataGenerator<TEntity>> optionalDataGenerator = templateRendering.getDataGenerator();

        assertAll(
            () -> assertEquals("<div>{{item.detailEntity.id}}</div>", templateElement.getProperty("innerHTML")),
            () -> assertTrue(optionalDataGenerator.isPresent()),
            () -> {
                JsonObject jsonObject = Json.createObject();
                optionalDataGenerator.get().generateData(masterEntity, jsonObject);
                assertEquals(Json.parse("{\"id\":\"testIdValue\"}").toString(), jsonObject.get("detailEntity").toString());

            }
        );
    }


    @Data
    public static class TestEntity {
        private String id;
    }

    @Data
    public static class TEntity {
        private String id;
        private TestEntity detailEntity;
    }


}
