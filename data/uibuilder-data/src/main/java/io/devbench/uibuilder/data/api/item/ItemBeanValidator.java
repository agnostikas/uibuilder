/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.item;

import org.jetbrains.annotations.NotNull;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

public class ItemBeanValidator {

    private Validator validator;

    protected Validator getValidator() {
        if (validator == null) {
            synchronized (this) {
                if (validator == null) {
                    ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
                    validator = validatorFactory.getValidator();
                }
            }
        }
        return validator;
    }

    /**
     * Validates the item
     *
     * @param propertyPaths the set of the propertyPaths to validate on the provided item
     * @param instance      the item on which the validation has to be run
     * @param <T>           the type of the item
     * @return a map with key as the propertyPath and value as the error message.
     * The empty propertyPath contains the complex validation result, an empty map means the item instance is valid
     */
    public <T> Map<String, String> validate(@NotNull Set<String> propertyPaths, @NotNull T instance) {
        if (propertyPaths.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, String> result = new HashMap<>();
        List<String> otherMessages = new ArrayList<>();
        getValidator().validate(instance).forEach(constraintViolation -> {
            String propertyPath = constraintViolation.getPropertyPath().toString();
            if (!propertyPath.trim().isEmpty() && propertyPaths.contains(propertyPath)) {
                result.put(propertyPath, constraintViolation.getMessage());
            } else {
                otherMessages.add(constraintViolation.getMessage());
            }
        });

        if (!otherMessages.isEmpty()) {
            result.put("", String.join("\n", otherMessages));
        }

        return result;
    }

    public static ItemBeanValidator getInstance() {
        return new ItemBeanValidator();
    }
}
