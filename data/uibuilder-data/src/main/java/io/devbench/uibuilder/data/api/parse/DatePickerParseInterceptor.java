/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datepicker.DatePicker.DatePickerI18n;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.i18n.core.I;
import org.jsoup.nodes.Element;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

import static java.util.Arrays.*;

@SuppressWarnings("unused")
public class DatePickerParseInterceptor implements ParseInterceptor {

    @Override
    public void intercept(Component component, Element element) {
    }

    @Override
    public boolean isInstantiator(Element element) {
        return true;
    }

    @Override
    public Component instantiateComponent() {
        Locale locale = UI.getCurrent().getLocale();

        DatePicker datePicker = new DatePickerForceBackend();
        datePicker.setI18n(createI18n(locale));
        return datePicker;
    }

    static DatePickerI18n createI18n(Locale locale) {
        DateFormatSymbols dfs = new DateFormatSymbols(locale);
        DatePickerI18n i18n = new DatePickerI18n();
        i18n.setMonthNames(asList(dfs.getMonths()));
        i18n.setWeekdays(asList(dfs.getWeekdays()).subList(1, 8));
        i18n.setWeekdaysShort(asList(dfs.getShortWeekdays()).subList(1, 8));
        i18n.setFirstDayOfWeek(getFirstDayOfWeek(locale));
        i18n.setWeek(I.tr("Week"));
        i18n.setCalendar(I.tr("Calendar"));
        i18n.setClear(I.tr("Clear"));
        i18n.setToday(I.tr("Today"));
        i18n.setCancel(I.tr("Cancel"));
        return i18n;
    }

    private static int getFirstDayOfWeek(Locale locale) {
        return Calendar.getInstance(locale).getFirstDayOfWeek() - 1;
    }

    @Override
    public boolean isApplicable(Element element) {
        return "vaadin-date-picker".equals(element.tagName());
    }

}
