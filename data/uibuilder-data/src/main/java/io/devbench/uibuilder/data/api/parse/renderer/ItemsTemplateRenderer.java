/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse.renderer;

import com.vaadin.flow.data.provider.DataKeyMapper;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.renderer.Rendering;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.SerializableConsumer;
import io.devbench.uibuilder.api.exceptions.InvalidPropertyBindingException;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import io.devbench.uibuilder.data.api.parse.ClassMetadataSupplier;
import lombok.Getter;

import java.util.Objects;
import java.util.Optional;


public class ItemsTemplateRenderer<SOURCE> extends Renderer<SOURCE> {

    private static final String ITEM_PREFIX = "item.";

    @Getter
    private ClassMetadataSupplier<SOURCE> classMetaSupplier;

    private ClassMetadata<SOURCE> classMeta;

    public static <SOURCE> ItemsTemplateRenderer<SOURCE> of(String template, ClassMetadataSupplier<SOURCE> classMetaSupplier) {
        Objects.requireNonNull(template);
        return new ItemsTemplateRenderer<>(template, classMetaSupplier);
    }

    private ItemsTemplateRenderer(String template, ClassMetadataSupplier<SOURCE> classMetaSupplier) {
        super(template);
        this.classMetaSupplier = classMetaSupplier;
    }

    public ClassMetadata<SOURCE> getClassMeta() {
        return classMeta == null ? (classMeta = classMetaSupplier.get()) : classMeta;
    }

    public ItemsTemplateRenderer<SOURCE> withProperty(String binding) {
        if (binding.startsWith(ITEM_PREFIX)) {
            String propertyPath = binding.substring(ITEM_PREFIX.length());
            setProperty(binding, item -> {
                classMeta = getClassMeta().withInstance(item);
                Optional<PropertyMetadata<?>> optionalProperty = getClassMeta().property(propertyPath);
                if (optionalProperty.isPresent()) {
                    PropertyMetadata<?> property = optionalProperty.get();
                    if (property.getInstance() != null) {
                        return PropertyConverters.getConverterFor(property).convertTo(property.getValue());
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            });
            return this;
        }

        throw new InvalidPropertyBindingException("Property bindings in column templates should start with 'item.' prefix");
    }

    public ItemsTemplateRenderer<SOURCE> withEventHandler(String handlerName, SerializableConsumer<SOURCE> handler) {
        setEventHandler(handlerName, handler);
        return this;
    }

    @Override
    public Rendering<SOURCE> render(Element container, DataKeyMapper<SOURCE> keyMapper, Element contentTemplate) {
        Rendering<SOURCE> baseRendering = super.render(container, keyMapper, contentTemplate);
        return new BindingBasedRendering<>(this, baseRendering);
    }

}
