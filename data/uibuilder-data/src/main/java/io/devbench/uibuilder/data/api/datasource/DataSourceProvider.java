/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.datasource;

import io.devbench.uibuilder.api.parse.BindingContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface DataSourceProvider<DATA_SOURCE_TYPE extends DataSource, DATA_SOURCE_SELECTOR extends DataSourceSelector> {

    DATA_SOURCE_TYPE getDataSource(@NotNull String dataSourceId, @NotNull DATA_SOURCE_SELECTOR dataSourceSelector);

    void requestRefresh(String dataSourceName, @Nullable DATA_SOURCE_SELECTOR dataSourceSelector);

    void registerBindingContextForDataSource(BindingContext bindingContext, @Nullable DATA_SOURCE_SELECTOR dataSourceSelector);

    @Nullable
    BindingContext getBindingContextForName(String dataSourceId, @Nullable DATA_SOURCE_SELECTOR dataSourceSelector);
}
