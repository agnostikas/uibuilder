/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.datasource;

import io.devbench.uibuilder.api.singleton.SingletonManager;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;


public class DataSourceManager {

    @Getter
    private final DataSourceProvider<? extends DataSource<?, ?, ?, ?>, ? super DataSourceSelector> dataSourceProvider;

    private DataSourceManager(DataSourceProvider<DataSource<?, ?, ?, ?>, ? super DataSourceSelector> dataSourceProvider) {
        this.dataSourceProvider = dataSourceProvider;
    }

    public DataSource<?, ?, ?, ?> getDataSource(@NotNull String dataSourceId, @NotNull DataSourceSelector dataSourceSelector) {
        return dataSourceProvider.getDataSource(dataSourceId, dataSourceSelector);
    }

    public void requestRefresh(String dataSourceName, DataSourceSelector dataSourceSelector) {
        dataSourceProvider.requestRefresh(dataSourceName, dataSourceSelector);
    }


    public static DataSourceManager getInstance() {
        return SingletonManager.getInstanceOf(DataSourceManager.class);
    }

    public static DataSourceManager registerToActiveContext(DataSourceProvider<DataSource<?, ?, ?, ?>, ? super DataSourceSelector> dataSourceProvider) {
        DataSourceManager instance = new DataSourceManager(dataSourceProvider);
        SingletonManager.registerSingleton(instance);
        return instance;
    }


}
