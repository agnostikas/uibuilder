/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.parse.renderer;

import com.vaadin.flow.data.provider.CompositeDataGenerator;
import com.vaadin.flow.data.provider.DataGenerator;
import com.vaadin.flow.data.renderer.Rendering;
import com.vaadin.flow.dom.Element;
import elemental.json.Json;
import elemental.json.JsonObject;

import java.util.Optional;

final class BindingBasedRendering<SOURCE> implements Rendering<SOURCE> {

    private ItemsTemplateRenderer<SOURCE> itemsTemplateRenderer;
    private Rendering<SOURCE> baseRendering;

    BindingBasedRendering(ItemsTemplateRenderer<SOURCE> itemsTemplateRenderer, Rendering<SOURCE> baseRendering) {
        this.itemsTemplateRenderer = itemsTemplateRenderer;
        this.baseRendering = baseRendering;
    }

    @Override
    public Optional<DataGenerator<SOURCE>> getDataGenerator() {
        if (itemsTemplateRenderer.getValueProviders() == null || itemsTemplateRenderer.getValueProviders().isEmpty()) {
            return Optional.empty();
        }
        CompositeDataGenerator<SOURCE> composite = new CompositeDataGenerator<>();
        itemsTemplateRenderer.getValueProviders().forEach((key, provider) -> composite
            .addDataGenerator((item, jsonObject) -> mapValueToKeyInJsonObjectGraph(jsonObject, key, provider.apply(item))));
        return Optional.of(composite);
    }

    protected void mapValueToKeyInJsonObjectGraph(JsonObject root, String key, Object value) { //TODO check if converters should support JsonValue still???
        String[] keyArray = key.split("\\.");
        JsonObject object = root;
        for (int i = 1; i < keyArray.length; i++) {
            if (i != keyArray.length - 1) {
                if (object.hasKey(keyArray[i])) {
                    object = object.getObject(keyArray[i]);
                } else {
                    JsonObject innerObject = Json.createObject();
                    object.put(keyArray[i], innerObject);
                    object = innerObject;
                }
            } else {
                object.put(keyArray[i], Json.create(String.valueOf(Optional.ofNullable(value).orElse(""))));
            }
        }
    }

    @Override
    public Element getTemplateElement() {
        return baseRendering.getTemplateElement();
    }
}
