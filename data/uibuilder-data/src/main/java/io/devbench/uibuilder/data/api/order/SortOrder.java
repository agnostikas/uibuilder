/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.api.order;

import com.google.gson.annotations.SerializedName;
import com.vaadin.flow.data.provider.SortDirection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortOrder {

    private String path;

    @Nullable
    private Direction direction;

    public enum Direction {
        @SerializedName("asc")
        ASCENDING,
        @SerializedName("desc")
        DESCENDING;

        public SortDirection toVaadinDirection() {
            return this == ASCENDING ? SortDirection.ASCENDING : SortDirection.DESCENDING;
        }
    }

}
