/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.data.jpa;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.components.util.datasource.BaseOrmDataSource;
import io.devbench.uibuilder.components.util.datasource.DataSourceBindingContext;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.order.OrderExpressionFactory;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import io.devbench.uibuilder.data.jpa.JpaFilterExpressionFactory;
import io.devbench.uibuilder.data.jpa.JpaOrderExpressionFactory;
import io.devbench.uibuilder.data.jpa.filterexpressions.JpaAndFilterExpression;
import io.devbench.uibuilder.data.jpa.filterexpressions.JpaFilterExpression;
import io.devbench.uibuilder.data.jpa.orderexpressions.JpaOrderExpression;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.Supplier;

public class SpringJpaDataSource<T> extends BaseOrmDataSource<T, JpaOrderExpression, JpaFilterExpression<?>> {

    private final Class<T> entityType;

    private final FilterExpressionFactory<JpaFilterExpression<?>> expressionFactory;

    private final OrderExpressionFactory<JpaOrderExpression> orderExpressionFactory;

    private final Supplier<JPAQuery<?>> querySupplier;
    private final Supplier<JPAQuery<?>> querySupplierBase;


    private JpaAndFilterExpression currentFilter;
    private JpaFilterExpression<?> currentHierarchical;

    private final PathBuilder<?> queryPath;


    private JpaOrderExpression currentOrder;

    protected SpringJpaDataSource(DataSourceBindingContext bindings,
                                  PathBuilder<?> queryPath,
                                  List<String> keyPaths,
                                  Class<T> entityType,
                                  Supplier<JPAQuery<?>> jpaQuery, Supplier<JPAQuery<?>> jpaQueryBase) {
        super(bindings, keyPaths);
        this.entityType = entityType;
        this.queryPath = queryPath;
        this.querySupplier = jpaQuery;
        this.querySupplierBase = jpaQueryBase;
        this.expressionFactory = new JpaFilterExpressionFactory();
        this.orderExpressionFactory = new JpaOrderExpressionFactory();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T findElementByKeyFilter(@Nonnull JpaFilterExpression<?> keyFilter) {
        JPAQuery<?> query = querySupplierBase.get();
        keyFilter.setQueryPath(queryPath);
        query.where(keyFilter.toPredicate());
        return (T) query.fetchOne();
    }

    @Override
    public Class<T> getElementType() {
        return entityType;
    }

    @Override
    public FilterExpressionFactory<JpaFilterExpression<?>> getFilterExpressionFactory() {
        return expressionFactory;
    }

    @Override
    public OrderExpressionFactory<JpaOrderExpression> getOrderExpressionFactory() {
        return orderExpressionFactory;
    }

    @Override
    public void registerFilter(JpaFilterExpression<?> expression) {
        this.currentFilter = (JpaAndFilterExpression) expression;
    }

    @Override
    public void registerHierarchical(JpaFilterExpression<?> expression) {
        currentHierarchical = expression;
    }

    @Override
    public void registerOrder(JpaOrderExpression expression) {
        this.currentOrder = expression;
    }

    @Override
    public long fetchSize(@Nullable PagingFetchRequest request) {
        return prepareCopyOfQuery(request).fetchCount();
    }

    @Override
    public boolean hasChildren(JpaFilterExpression<?> request) {
        JPAQuery<?> query = getQuery();
        request.setQueryPath(queryPath);
        setUpWhere(query, request);
        return query.fetchCount() != 0L;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> fetchData(@Nullable PagingFetchRequest request) {
        return (List<T>) prepareCopyOfQuery(request).fetch();
    }

    private JPQLQuery<?> prepareCopyOfQuery(@Nullable PagingFetchRequest request) {
        JPAQuery<?> query = getQuery();
        setUpCurrentWherePredicate(query);
        setUpOrder(query);
        setUpSize(request, query);
        return query;
    }

    private void setUpSize(@Nullable PagingFetchRequest request, JPAQuery<?> query) {
        if (isPagingFetchRequestPresent(request)) {
            query.offset((long) request.getPage() * request.getPageSize()).limit(request.getPageSize());
        }
    }

    private void setUpOrder(JPAQuery<?> query) {
        if (currentOrder != null) {
            currentOrder.setQueryPath(queryPath);
            query.orderBy(currentOrder.toOrder().toArray(OrderSpecifier[]::new));
        }
    }

    private void setUpCurrentWherePredicate(JPAQuery<?> query) {
        if (isDualFiltered()) {
            setUpDualFilter(query);
        } else if (isHierarchicalFilterReady()) {
            setUpHierarchical(query);
        } else if (isCurrentFilterReady()) {
            setUpFilter(query);
        }
    }

    private void setUpDualFilter(JPAQuery<?> query) {
        final JpaFilterExpression<?> predicate = wrapFilters(currentFilter, currentHierarchical);
        predicate.setQueryPath(queryPath);
        setUpWhere(query, predicate);
    }

    private boolean isDualFiltered() {
        return isHierarchicalFilterReady() && isCurrentFilterReady();
    }

    private boolean isHierarchicalFilterReady() {
        return currentHierarchical != null;
    }

    private boolean isCurrentFilterReady() {
        return currentFilter != null && !currentFilter.getExpressions().isEmpty();
    }


    private void setUpHierarchical(JPAQuery<?> query) {
        currentHierarchical.setQueryPath(queryPath);
        setUpWhere(query, currentHierarchical);
    }

    private void setUpFilter(JPAQuery<?> query) {
        currentFilter.setQueryPath(queryPath);
        setUpWhere(query, currentFilter);
    }

    private void setUpWhere(JPAQuery<?> query, JpaFilterExpression<?> predicate) {
        if (predicate != null) {
            query.where(predicate.toPredicate());
        }
    }

    private JPAQuery<?> getQuery() {
        return this.querySupplier.get();
    }
}
