/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.data.ebean;

import io.devbench.uibuilder.components.util.datasource.BaseOrmDataSource;
import io.devbench.uibuilder.components.util.datasource.DataSourceBindingContext;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.order.OrderExpressionFactory;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import io.devbench.uibuilder.data.ebean.EbeanFilterExpressionFactory;
import io.devbench.uibuilder.data.ebean.EbeanOrderExpressionFactory;
import io.devbench.uibuilder.data.ebean.filterexpressions.EbeanAndFilterExpression;
import io.devbench.uibuilder.data.ebean.filterexpressions.EbeanFilterExpression;
import io.devbench.uibuilder.data.ebean.orderexpressions.EbeanOrderExpression;
import io.ebean.ExpressionList;
import io.ebean.Query;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.Supplier;

public class SpringEbeanDataSource<T> extends BaseOrmDataSource<T, EbeanOrderExpression, EbeanFilterExpression<?>> {

    private final Class<T> entityType;

    private final FilterExpressionFactory<EbeanFilterExpression<?>> expressionFactory;
    private final OrderExpressionFactory<EbeanOrderExpression> orderExpressionFactory;

    private final Supplier<Query<?>> querySupplier;
    private final Supplier<Query<?>> querySupplierBase;

    private EbeanAndFilterExpression currentFilter;
    private EbeanFilterExpression<? extends ExpressionList<?>> currentHierarchical;
    private EbeanOrderExpression currentOrder;

    protected SpringEbeanDataSource(DataSourceBindingContext bindings,
                                    List<String> keyPaths, Class<T> entityType,
                                    Supplier<Query<?>> querySupplier,
                                    Supplier<Query<?>> querySupplierBase) {
        super(bindings, keyPaths);
        this.entityType = entityType;
        this.querySupplier = querySupplier;
        this.querySupplierBase = querySupplierBase;
        this.expressionFactory = new EbeanFilterExpressionFactory();
        this.orderExpressionFactory = new EbeanOrderExpressionFactory();
    }

    @Override
    public Class<T> getElementType() {
        return entityType;
    }

    @Override
    public FilterExpressionFactory<EbeanFilterExpression<?>> getFilterExpressionFactory() {
        return expressionFactory;
    }

    @Override
    public OrderExpressionFactory<EbeanOrderExpression> getOrderExpressionFactory() {
        return orderExpressionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T findElementByKeyFilter(@Nonnull @NotNull EbeanFilterExpression<?> keyFilter) {
        Query<?> query = this.querySupplierBase.get();

        keyFilter.setContainerExpressionList(query.where());
        return (T) keyFilter.toPredicate().findOne();
    }

    @Override
    public void registerFilter(EbeanFilterExpression<?> expression) {
        this.currentFilter = (EbeanAndFilterExpression) expression;
    }

    @Override
    public void registerHierarchical(EbeanFilterExpression<?> expression) {
        currentHierarchical = expression;
    }

    @Override
    public void registerOrder(EbeanOrderExpression expression) {
        this.currentOrder = expression;
    }

    @Override
    public long fetchSize(@Nullable PagingFetchRequest request) {
        ExpressionList<?> where = prepareCopyOfQuery(request);
        return where.findCount();
    }

    @Override
    public boolean hasChildren(EbeanFilterExpression<? extends ExpressionList<?>> childrenIdentifier) {
        Query<?> query = this.querySupplier.get();
        ExpressionList<?> where = query.where();
        setupWhere(where, childrenIdentifier);
        return where.exists();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> fetchData(@Nullable PagingFetchRequest request) {
        ExpressionList<?> where = prepareCopyOfQuery(request);
        return (List<T>) where.findList();
    }

    private ExpressionList<?> prepareCopyOfQuery(@Nullable PagingFetchRequest request) {
        Query<?> query = this.querySupplier.get();
        ExpressionList<?> where = query.where();
        setUpCurrentWherePredicate(where);
        setUpOrder(where);
        setUpSize(request, where);
        return where;
    }

    private void setUpSize(@Nullable PagingFetchRequest request, ExpressionList<?> where) {
        if (request != null) {
            where.setFirstRow(request.getPage() * request.getPageSize())
                .setMaxRows(request.getPageSize());
        }
    }

    private void setUpCurrentWherePredicate(ExpressionList<?> where) {
        if (isDualFiltered()) {
            setupWhere(where, wrapFilters(currentFilter, currentHierarchical));
        } else if (isHierarchicalFilterReady()) {
            setUpHierarchical(where);
        } else if (isCurrentFilterReady()) {
            setUpFilter(where);
        }
    }

    private boolean isDualFiltered() {
        return isHierarchicalFilterReady() && isCurrentFilterReady();
    }

    private boolean isHierarchicalFilterReady() {
        return currentHierarchical != null;
    }

    private boolean isCurrentFilterReady() {
        return currentFilter != null && !currentFilter.getExpressions().isEmpty();
    }

    private void setUpHierarchical(ExpressionList<?> where) {
        setupWhere(where, currentHierarchical);
    }

    private void setUpFilter(ExpressionList<?> where) {
        setupWhere(where, currentFilter);
    }

    private void setupWhere(ExpressionList<?> where, EbeanFilterExpression<? extends ExpressionList<?>> expression) {
        expression.setContainerExpressionList(where);
        expression.toPredicate();
    }

    private void setUpOrder(ExpressionList<?> where) {
        if (currentOrder != null) {
            currentOrder.setContainerExpressionList(where);
            currentOrder.toOrder();
        }
    }
}
