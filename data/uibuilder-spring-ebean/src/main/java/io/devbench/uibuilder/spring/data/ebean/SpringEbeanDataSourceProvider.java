/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.data.ebean;

import io.devbench.uibuilder.components.util.datasource.DataSourceBindingContext;
import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.data.api.exceptions.DataSourceNotFoundException;
import io.devbench.uibuilder.spring.data.IdHandler;
import io.devbench.uibuilder.spring.data.SpringCommonDataSourceProvider;
import io.ebean.Query;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.ebean.repository.EbeanRepository;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SpringEbeanDataSourceProvider<T> extends SpringCommonDataSourceProvider<SpringEbeanDataSource<T>, EbeanRepository<T, ?>> implements IdHandler {

    @Autowired
    public SpringEbeanDataSourceProvider(ApplicationContext applicationContext) {
        super(applicationContext);
    }

    public Set<Class<? extends Annotation>> getInterestedAnnotationTypes() {
        return Collections.singleton(TargetDataSource.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void lateInit() {
        dataSourceRepositories = Collections.unmodifiableMap(
            getAnnotatedClasses().getOrDefault(TargetDataSource.class, Collections.emptySet()).stream()
                .filter(EbeanRepository.class::isAssignableFrom)
                .<Pair<TargetDataSource, Class<? extends EbeanRepository<T, ?>>>>map(repoClass ->
                    Pair.of(repoClass.getAnnotation(TargetDataSource.class), (Class<? extends EbeanRepository<T, ?>>) repoClass))
                .collect(Collectors.toMap(pair -> pair.getKey().name(), Function.identity()))
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    public SpringEbeanDataSource createNewDataSource(String dataSourceId, @Nullable String optionalDefaultQueryName) {
        final Optional<Pair<TargetDataSource, Class<? extends EbeanRepository<T, ?>>>> targetDSRepo = findDataSourceNameById(dataSourceId);
        return targetDSRepo.map(repo -> {
            final Class<?> entityType = tryToFindEntityClassByRepositoryClass(repo.getValue())
                .orElseThrow(IllegalStateException::new);
            final Supplier<Query<?>> query = buildQuerySupplier(optionalDefaultQueryName, repo, entityType);
            final Supplier<Query<?>> queryBase = buildQuerySupplier(null, repo, entityType);
            final List<String> keyPaths = getKeyPaths(repo.getKey(), entityType);
            final DataSourceBindingContext bindings = (DataSourceBindingContext)
                registeredBindingContexts.get(createBindingsKey(dataSourceId, optionalDefaultQueryName));
            return new SpringEbeanDataSource(bindings, keyPaths, entityType, query, queryBase);
        }).orElseThrow(() -> new DataSourceNotFoundException("Couldn't find data source with id:" + dataSourceId
            + " name: " + idToDataSourceName.get(dataSourceId)));
    }


    private Supplier<Query<?>> buildQuerySupplier(@Nullable String optionalDefaultQueryName,
                                                  Pair<TargetDataSource, Class<? extends EbeanRepository<T, ?>>> targetDSRepo,
                                                  Class<?> entityType) {
        final EbeanRepository<T, ?> repository = applicationContext.getBean(targetDSRepo.getValue());
        return () -> Optional
            .ofNullable(optionalDefaultQueryName)
            .flatMap(defaultQuery -> this.tryToFindQueryByMethodNameInRepository(repository, defaultQuery))
            .orElse(repository.db().createQuery(entityType));
    }

    private Optional<Query<?>> tryToFindQueryByMethodNameInRepository(EbeanRepository<T, ?> repository, String methodName) {
        //TODO add an abstraction to the default query by using an annotation to name the method, to hide method names from frontend
        try {
            Object methodResult = repository.getClass().getDeclaredMethod(methodName).invoke(repository);
            if (methodResult instanceof io.ebean.Query) {
                io.ebean.Query<?> query = (io.ebean.Query<?>) methodResult;
                return Optional.of(query);
            } else {
                log.error("Query type not supported:" + methodResult.getClass());
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.error(e.getMessage(), e); //TODO implement better error handling
        }
        return Optional.empty();
    }
}
