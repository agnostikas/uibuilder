/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.jpa.filterexpressions.JpaEqualsFilterExpression;
import io.devbench.uibuilder.data.jpa.filterexpressions.JpaFilterExpression;
import io.devbench.uibuilder.data.jpa.filterexpressions.JpaOrFilterExpression;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class JpaFilterExpressionFactoryTest {

    private final JpaFilterExpressionFactory testObj = new JpaFilterExpressionFactory();

    @Test
    public void shouldCreateAnExpression() {
        Stream
            .of(JpaOrFilterExpression.class, JpaEqualsFilterExpression.class)
            .map(testObj::create)
            .forEach(Assertions::assertNotNull);
    }

    @Test
    public void shouldThrowAnExceptionWhenFilterClassIsUnknown() {
        assertThrows(FilterException.class, () -> testObj.create(UnknownFilterExpression.class));
    }

    public abstract static class UnknownFilterExpression implements JpaFilterExpression {

    }

}
