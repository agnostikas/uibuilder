/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.filterexpressions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class JpaExpressionPopulateTest extends JpaFilterTestBase {

    private JpaAndFilterExpression testObject;

    @BeforeEach
    public void setUp() {
        testObject = new JpaAndFilterExpression();
        testObject.setQueryPath(queryPath);
    }

    @Test
    public void should_create_a_ComparableExpression() {
        JpaEqualsFilterExpression jpaEqualsFilterExpression1 = getJpaEqualsFilterExpression("path1", "value1");
        testObject.add(jpaEqualsFilterExpression1);
        assertNotNull(testObject.toPredicate());
        assertEquals("Object.path1 = value1", testObject.toPredicate().toString());
    }

    @Test
    public void should_create_a_composite_and_or_expression() {
        JpaOrFilterExpression jpaOrFilterExpression = new JpaOrFilterExpression();
        JpaEqualsFilterExpression jpaEqualsFilterExpression = getJpaEqualsFilterExpression("path", "value");
        JpaEqualsFilterExpression jpaEqualsFilterExpression0 = getJpaEqualsFilterExpression("path0", "value0");
        jpaOrFilterExpression.add(jpaEqualsFilterExpression);
        jpaOrFilterExpression.add(jpaEqualsFilterExpression0);
        testObject.setQueryPath(queryPath);
        JpaEqualsFilterExpression jpaEqualsFilterExpression1 = getJpaEqualsFilterExpression("path1", "value1");
        testObject.add(jpaEqualsFilterExpression1);
        testObject.add(jpaOrFilterExpression);
        assertNotNull(testObject.toPredicate());
        assertEquals("Object.path1 = value1 && (Object.path = value || Object.path0 = value0)", testObject.toPredicate().toString());
    }

}
