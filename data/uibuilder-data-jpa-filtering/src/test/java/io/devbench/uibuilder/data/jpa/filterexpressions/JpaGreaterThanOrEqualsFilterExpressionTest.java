/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.filterexpressions;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JpaGreaterThanOrEqualsFilterExpressionTest {
    protected PathBuilder<Object> queryPath = new PathBuilder<>(Object.class, Object.class.getSimpleName());
    private JpaGreaterThanOrEqualsFilterExpression testObject;

    @BeforeEach
    public void setUp() {
        testObject = new JpaGreaterThanOrEqualsFilterExpression();
        testObject.setQueryPath(queryPath);
    }

    @Test
    public void should_throw_exception_when_values_are_not_comparable() {
        testObject.setPath("asd");
        testObject.setValue(new Object());
        assertThrows(ClassCastException.class, () -> testObject.toPredicate());
    }

    @Test
    void should_create_booleanExpression() {
        testObject.setPath("asd");
        testObject.setValue("asd");
        final BooleanExpression booleanExpression = testObject.toPredicate();
        assertNotNull(booleanExpression);
        assertEquals("Object.asd >= asd", testObject.toPredicate().toString());
    }
}
