/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.filterexpressions;

import com.querydsl.core.types.Ops;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import lombok.Setter;


public class JpaAndFilterExpression extends
    AndFilterExpression<JpaFilterExpression<BooleanExpression>, BooleanExpression> implements
    JpaFilterExpression<BooleanExpression>, ExpressionPopulate {

    @Setter
    private PathBuilder<?> queryPath;

    @Override
    public BooleanExpression toPredicate() {
        return populateExpression(this.getExpressions(), queryPath,  Ops.AND);
    }
}

