/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.filterexpressions;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import lombok.Setter;

public class JpaGreaterThanFilterExpression extends ExpressionTypes.GreaterThan<BooleanExpression> implements JpaFilterExpression<BooleanExpression> {

    @Setter
    private PathBuilder<?> queryPath;

    @Override
    @SuppressWarnings("unchecked")
    public BooleanExpression toPredicate() {
        final Comparable value = (Comparable) getValue();
        return ((PathBuilder) queryPath).getComparable(getPath(), value.getClass()).gt(value);
    }
}


