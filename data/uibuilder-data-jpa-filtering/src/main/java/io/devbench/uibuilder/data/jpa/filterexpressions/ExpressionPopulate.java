/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.filterexpressions;

import com.querydsl.core.types.Ops;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;

import java.util.List;
import java.util.stream.Collectors;

public interface ExpressionPopulate {

    default BooleanExpression populateExpression(List<JpaFilterExpression<BooleanExpression>> block,
                                         PathBuilder<?> queryPath,
                                         Ops operator) {
        List<BooleanExpression> predicateList = getPredicateList(block, queryPath);
        if (predicateList.isEmpty()) {
            return null;
        }
        if (predicateList.size() == 1
            && (operator.equals(Ops.AND) || operator.equals(Ops.OR))) {
            return predicateList.get(0);
        }
        if (operator.equals(Ops.AND)) {
            return Expressions.allOf(predicateList.toArray(new BooleanExpression[0]));
        } else if (operator.equals(Ops.OR)) {
            return Expressions.anyOf(predicateList.toArray(new BooleanExpression[0]));
        }
        return null;
    }

    default List<BooleanExpression> getPredicateList(List<JpaFilterExpression<BooleanExpression>> block, PathBuilder<?> queryPath) {
        return block.stream()
            .map(expression -> {
                expression.setQueryPath(queryPath);
                return expression.toPredicate();
            })
            .collect(Collectors.toList());
    }

}
