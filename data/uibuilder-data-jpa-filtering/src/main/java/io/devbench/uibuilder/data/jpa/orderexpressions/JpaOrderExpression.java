/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.jpa.orderexpressions;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathBuilder;
import io.devbench.uibuilder.data.api.order.OrderExpression;
import io.devbench.uibuilder.data.api.order.SortOrder;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JpaOrderExpression implements OrderExpression {

    @Setter
    private PathBuilder<?> queryPath;

    private List<SortOrder> sortOrderList = new ArrayList<>();

    @Override
    public void addOrder(@NotNull SortOrder sortOrder) {
        sortOrderList.add(sortOrder);
    }

    public Stream<OrderSpecifier> toOrder() {
        return sortOrderList.stream().map(this::getDirection).filter(Objects::nonNull);
    }

    @SuppressWarnings("unchecked")
    private OrderSpecifier getDirection(@NotNull SortOrder order) {
        Path<Object> fieldPath = queryPath.get(order.getPath());
        if (order.getDirection() == null) {
            return null;
        }
        return order.getDirection() == SortOrder.Direction.ASCENDING ? new OrderSpecifier(Order.ASC, fieldPath) : new OrderSpecifier(Order.DESC, fieldPath);
    }

}
