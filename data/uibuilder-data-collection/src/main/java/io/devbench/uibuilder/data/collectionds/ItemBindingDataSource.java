/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds;

import io.devbench.uibuilder.api.exceptions.InvalidPropertyBindingException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ItemBindingDataSource<T> extends CollectionDataSource<T> {


    @Getter
    private final String bindingPath;

    private static final Pattern BINDING_PATTERN = Pattern.compile("\\{\\{(\\w+)\\.((\\w+)(\\.(\\w+))*)}}");

    public ItemBindingDataSource(ItemDataSourceBindingContext bindings, List<String> keyPaths, @NotNull String bindingPath) {
        super(bindings, keyPaths, getItemsBasedOnBindingPath(bindingPath));
        this.bindingPath = bindingPath;
    }

    @SuppressWarnings("unchecked")
    private static <T> Collection<T> getItemsBasedOnBindingPath(String bindingPath) {
        Matcher matcher = BINDING_PATTERN.matcher(bindingPath);
        if (matcher.find()) {
            String beanName = matcher.group(1);
            String propertyName = matcher.group(2);
            ClassMetadata<T> instance = ControllerBeanManager.getInstance().getBeanMetadata(beanName);
            return (Collection<T>) instance
                .getPropertyValue(propertyName)
                .filter(Collection.class::isInstance)
                .orElseThrow(() -> new InvalidPropertyBindingException("Binding expression `" + bindingPath + "` is not a valid Collection."));
        } else {
            throw new InvalidPropertyBindingException("Binding expression `" + bindingPath + "` is not valid.");
        }
    }

    @Override
    protected Collection<T> getRootItems() {
        return getItemsBasedOnBindingPath(bindingPath);
    }
}
