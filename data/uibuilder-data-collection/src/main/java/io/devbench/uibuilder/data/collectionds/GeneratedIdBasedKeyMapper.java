/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds;

import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.common.dataprovidersupport.KeyMapper;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class GeneratedIdBasedKeyMapper<T> implements KeyMapper<T> {

    private static final String ID_FIELD = "id";
    private final Map<String, T> idToItemsMap;

    public GeneratedIdBasedKeyMapper(Collection<? extends T> items) {
        this.idToItemsMap = items
            .stream()
            .map(it -> Pair.of(UUID.randomUUID().toString(), it))
            .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    @Override
    public String getKey(ClassMetadata<T> itemClassMetadata) {
        JsonObject jsonObject = Json.createObject();
        T instance = itemClassMetadata.getInstance();
        Optional<String> id = idToItemsMap.entrySet()
            .stream()
            .filter(it -> it.getValue().equals(instance))
            .map(Map.Entry::getKey)
            .findFirst();
        if (id.isPresent()) {
            jsonObject.put(ID_FIELD, id.get());
        } else {
            String newId = UUID.randomUUID().toString();
            idToItemsMap.put(newId, instance);
            jsonObject.put(ID_FIELD, newId);
        }
        return Base64.getEncoder().encodeToString(jsonObject.toJson().getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public T getItem(String key) {
        String decodedKey = new String(Base64.getDecoder().decode(key), StandardCharsets.UTF_8);
        JsonObject keyObject = Json.parse(decodedKey);
        return idToItemsMap.get(keyObject.getString(ID_FIELD));
    }
}
