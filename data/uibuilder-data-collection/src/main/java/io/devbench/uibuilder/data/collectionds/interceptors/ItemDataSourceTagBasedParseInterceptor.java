/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.interceptors;

import com.vaadin.flow.component.Component;
import elemental.json.Json;
import elemental.json.JsonArray;
import io.devbench.uibuilder.components.util.datasource.BaseTagBasedParseInterceptor;
import io.devbench.uibuilder.components.util.datasource.DataSourceUtils;
import io.devbench.uibuilder.data.api.datasource.DataSourceDeposit;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.devbench.uibuilder.data.collectionds.ItemBindingDataSource;
import io.devbench.uibuilder.data.collectionds.ItemDataSourceCapable;
import org.jsoup.nodes.Element;
import java.util.List;
import java.util.Optional;

public class ItemDataSourceTagBasedParseInterceptor extends BaseTagBasedParseInterceptor<ItemDataSourceBindingContext> {

    static final String DATA_SOURCE_TAG_NAME = "item-data-source";
    static final String KEY_PATHS_ATTRIBUTE_NAME = "keys";
    static final String ITEMS_ATTRIBUTE_NAME = "items";
    static final String HIERARCHY_PROVIDER = "hierarchy-provider";
    static final String SORT_PATH = "sort-path";
    static final String SORT_DIRECTION = "sort-direction";

    public boolean appliesToSubElements() {
        return true;
    }

    @Override
    public ItemDataSourceBindingContext createBindingContext(Element element) {
        ItemDataSourceBindingContext context = new ItemDataSourceBindingContext();

        Element dataSourceElement = DataSourceUtils.findOneChildren(element, DATA_SOURCE_TAG_NAME);
        Optional<ItemDataSourceBindingContext> foundBindingContext = tryBindingContextFromDeposit(dataSourceElement);

        if (foundBindingContext.isPresent()) {

            foundBindingContext.get().copy(context);
            context.setDataSourceId(DataSourceUtils.replaceDataSourceId(dataSourceElement, true));

        } else {

            parseCommonBindingContextElements(dataSourceElement, element, context);

            if (dataSourceElement.hasAttr(KEY_PATHS_ATTRIBUTE_NAME)) {
                JsonArray keyPaths = Json.instance().parse(dataSourceElement.attr(KEY_PATHS_ATTRIBUTE_NAME));
                for (int i = 0; i < keyPaths.length(); i++) {
                    context.getKeyPaths().add(keyPaths.getString(i));
                }
            }

            if (dataSourceElement.hasAttr(ITEMS_ATTRIBUTE_NAME)) {
                context.setItemsAttribute(dataSourceElement.attr(ITEMS_ATTRIBUTE_NAME));
                dataSourceElement.attr(ITEMS_ATTRIBUTE_NAME, "");
            }

            if (dataSourceElement.hasAttr(HIERARCHY_PROVIDER)) {
                context.setHierarchyProvider(dataSourceElement.attr(HIERARCHY_PROVIDER));
            }

            if (dataSourceElement.hasAttr(SORT_PATH)) {
                context.setSortPath(dataSourceElement.attr(SORT_PATH));
            }

            context.setSortDirection(
                dataSourceElement.hasAttr(SORT_DIRECTION) ?
                    (dataSourceElement.attr(SORT_DIRECTION).trim().toLowerCase().startsWith("asc")
                        ? SortOrder.Direction.ASCENDING
                        : SortOrder.Direction.DESCENDING) :
                    SortOrder.Direction.ASCENDING
            );

            context.setDataSourceId(DataSourceUtils.replaceDataSourceId(dataSourceElement));
        }

        return context;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void commitBindingParse(ItemDataSourceBindingContext bindingContext, Component component) {
        bindingContext.commit();
        DataSourceManager
            .getInstance()
            .getDataSourceProvider()
            .registerBindingContextForDataSource(bindingContext, null);

        if (component == null) {
            DataSourceDeposit.getInstance().add(bindingContext.getDataSourceName(), bindingContext);
        } else {
            ((ItemDataSourceCapable<?>) component).connectItemDataSource(bindingContext.getDataSourceId());
            String itemsPath = bindingContext.getItemsAttribute();
            if (itemsPath != null) {
                List<String> keyPaths = bindingContext.getKeyPaths();
                ((ItemDataSourceCapable<?>) component).setDataSource(new ItemBindingDataSource(bindingContext, keyPaths, itemsPath));
            }
        }
    }

    @Override
    public boolean isApplicable(Element element) {
        return DataSourceUtils.isChildrenExist(element, DATA_SOURCE_TAG_NAME);
    }

    @Override
    public void intercept(Component component, Element element) {
        //ignore
    }
}
