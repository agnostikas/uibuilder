/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter.factories;

import com.google.common.collect.Sets;
import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.collectionds.filter.*;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

public class CollectionFilterExpressionFactory<T> implements FilterExpressionFactory<CollectionFilterExpression<T>> {
    private static final Set<Class<? extends CollectionFilterExpression>> COLLECTION_FILTER_EXPRESSION_TYPES;

    @Override
    @NotNull
    public <FILTER_EXPRESSION extends CollectionFilterExpression<T>> FILTER_EXPRESSION create(Class<FILTER_EXPRESSION> expressionType) {
        return COLLECTION_FILTER_EXPRESSION_TYPES.stream()
            .filter(expressionType::isAssignableFrom)
            .findAny()
            .flatMap(this::tryToCreateInstance)
            .map(expressionType::cast)
            .orElseThrow(() -> new FilterException("Couldn't find FilterExpression implementation for " + expressionType.getSimpleName()));
    }

    private <EXPRESSION> Optional<EXPRESSION> tryToCreateInstance(Class<EXPRESSION> expressionClass) {
        try {
            return Optional.of(expressionClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            return Optional.empty();
        }
    }

    static {
        COLLECTION_FILTER_EXPRESSION_TYPES = Collections.unmodifiableSet(Sets.newHashSet(
            CollectionAndFilterExpression.class,
            CollectionBetweenExpression.class,
            CollectionEqualsFilterExpression.class,
            CollectionGreaterThanFilterExpression.class,
            CollectionGreaterThanOrEqualsFilterExpression.class,
            CollectionInFilterExpression.class,
            CollectionNotInFilterExpression.class,
            CollectionLessThanFilterExpression.class,
            CollectionLessThanOrEqualsFilterExpression.class,
            CollectionLikeExpression.class,
            CollectionIgnoreCaseLikeExpression.class,
            CollectionNotEqualsFilterExpression.class,
            CollectionNotFilterExpression.class,
            CollectionOrFilterExpression.class,
            CollectionIsNotNullFilterExpression.class,
            CollectionIsNullFilterExpression.class
        ));
    }
}
