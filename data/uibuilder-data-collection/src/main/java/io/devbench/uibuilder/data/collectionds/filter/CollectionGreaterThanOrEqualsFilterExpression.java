/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;

import java.util.function.Predicate;

public class CollectionGreaterThanOrEqualsFilterExpression<T>
    extends ExpressionTypes.GreaterThanOrEquals<Predicate<T>>
    implements CollectionPathFilterExpression<T> {

    @Override
    @SuppressWarnings("unchecked")
    public Predicate<T> toPredicate() {
        return entity -> {
            Comparable propertyValue = (Comparable) getPropertyValue(entity);
            return propertyValue != null && propertyValue.compareTo(getValue()) >= 0;
        };
    }
}
