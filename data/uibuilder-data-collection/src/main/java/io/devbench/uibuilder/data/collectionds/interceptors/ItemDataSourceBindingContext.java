/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.interceptors;

import io.devbench.uibuilder.core.controllerbean.uiproperty.converters.FunctionPropertyConverter;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.devbench.uibuilder.data.common.datasource.BaseDataSourceBindingContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class ItemDataSourceBindingContext extends BaseDataSourceBindingContext {

    private static final FunctionPropertyConverter FUNCTION_PROPERTY_CONVERTER = new FunctionPropertyConverter();

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private List<String> keyPaths = new ArrayList<>();

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String itemsAttribute;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String hierarchyProvider;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String sortPath;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private SortOrder.Direction sortDirection;

    public void commit() {
        super.commit();
        this.keyPaths = Collections.unmodifiableList(keyPaths);
    }

    @Override
    public void setDataSourceName(String dataSourceName) {
        super.setDataSourceName(dataSourceName);
    }

    @SuppressWarnings("unchecked")
    public Collection<?> provideChildren(Object parent) {
        return (Collection<?>) ((Function) FUNCTION_PROPERTY_CONVERTER.convertFrom(hierarchyProvider)).apply(parent);
    }

    @Override
    protected <T extends BaseDataSourceBindingContext> void copy(T into) {
        super.copy(into);
        ((ItemDataSourceBindingContext) into).setKeyPaths(keyPaths);
        ((ItemDataSourceBindingContext) into).setItemsAttribute(itemsAttribute);
        ((ItemDataSourceBindingContext) into).setHierarchyProvider(hierarchyProvider);
        ((ItemDataSourceBindingContext) into).setSortPath(sortPath);
        ((ItemDataSourceBindingContext) into).setSortDirection(sortDirection);
    }
}
