/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.mdc;

import io.devbench.uibuilder.api.components.masterconnector.AbstractUIBuilderMasterConnector;
import io.devbench.uibuilder.api.exceptions.MasterConnectorNotDirectlyModifiableException;
import io.devbench.uibuilder.api.exceptions.MasterConnectorUnsupportedOperationException;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;

public abstract class AbstractDataSourceCapableMasterConnector<COMP extends AbstractDataSourceComponent<ITEM_VALUE>, ITEM_VALUE>
    extends AbstractUIBuilderMasterConnector<COMP, ITEM_VALUE> {

    protected AbstractDataSourceCapableMasterConnector(Class<COMP> componentClass) {
        super(componentClass);
    }

    @Override
    public final void refresh() {
        getMasterComponent().refresh();
    }

    @Override
    public final void refresh(ITEM_VALUE item) {
        throw new MasterConnectorUnsupportedOperationException();
    }

    @Override
    public final void setEnabled(boolean enabled) {
        getMasterComponent().setEnabled(enabled);
    }

    @Override
    public final boolean isEnabled() {
        return getMasterComponent().isEnabled();
    }

    @Override
    public final boolean isDirectModifiable() {
        return getMasterComponent().getDataSource() instanceof CollectionDataSource;
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void addItem(ITEM_VALUE item) {
        CommonDataSource<ITEM_VALUE, ?, ?, ?> dataSource = getMasterComponent().getDataSource();
        if (dataSource instanceof CollectionDataSource) {
            ((CollectionDataSource<ITEM_VALUE>) dataSource).getItems().add(item);
        } else {
            throw new MasterConnectorNotDirectlyModifiableException();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void removeItem(ITEM_VALUE item) {
        CommonDataSource<ITEM_VALUE, ?, ?, ?> dataSource = getMasterComponent().getDataSource();
        if (dataSource instanceof CollectionDataSource) {
            ((CollectionDataSource<ITEM_VALUE>) dataSource).getItems().remove(item);
        } else {
            throw new MasterConnectorNotDirectlyModifiableException();
        }
    }

    @Override
    public final int getPriority() {
        return DEFAULT_PRIORITY - 500;
    }
}
