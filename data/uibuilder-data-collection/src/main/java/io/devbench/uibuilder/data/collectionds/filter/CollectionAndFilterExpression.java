/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;

import java.util.function.Predicate;

public class CollectionAndFilterExpression<T>
    extends AndFilterExpression<CollectionFilterExpression<T>, Predicate<T>>
    implements CollectionFilterExpression<T> {

    @Override
    public Predicate<T> toPredicate() {
        Predicate<T> retValue = t -> true;
        for (CollectionFilterExpression<T> expression : getExpressions()) {
            retValue = retValue.and(expression.toPredicate());
        }
        return retValue;
    }
}
