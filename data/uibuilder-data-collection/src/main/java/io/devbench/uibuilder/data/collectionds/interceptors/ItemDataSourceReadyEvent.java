/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.interceptors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.vaadin.flow.dom.DisabledUpdateMode;

@DomEvent(value = "itemDataSourceReady", allowUpdates = DisabledUpdateMode.ALWAYS)
public class ItemDataSourceReadyEvent extends ComponentEvent<Component> {

    private final String dataSourceName;

    public ItemDataSourceReadyEvent(Component source, boolean fromClient,
                                    @EventData("event.detail.dataSource.name") String dataSourceName) {
        super(source, fromClient);
        this.dataSourceName = dataSourceName;
    }

    public String getDataSourceName() {
        return dataSourceName;
    }

}
