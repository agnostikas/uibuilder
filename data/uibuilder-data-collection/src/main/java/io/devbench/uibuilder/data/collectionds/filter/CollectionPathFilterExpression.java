/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.collectionds.exceptions.FilterPathInvalidException;

public interface CollectionPathFilterExpression<T> extends CollectionFilterExpression<T> {

    String getPath();

    default Object getPropertyValue(T entity) {
        return ClassMetadata
            .ofValue(entity)
            .property(getPath())
            .orElseThrow(() -> new FilterPathInvalidException("The filter path `" + getPath() + "` is invalid."))
            .getValue();
    }
}
