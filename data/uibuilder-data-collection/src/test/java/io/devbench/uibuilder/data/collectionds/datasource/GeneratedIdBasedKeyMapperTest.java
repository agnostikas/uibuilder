/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.datasource;

import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.collectionds.GeneratedIdBasedKeyMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class GeneratedIdBasedKeyMapperTest {

    @Test
    public void should_give_me_the_same_object_with_the_generated_id() {
        ClassMetadata<Object> classMetadata = mock(ClassMetadata.class);

        Object expected = new Object();
        List<Object> objects = Arrays.asList(expected, new Object(), new Object(), new Object(), new Object(), new Object(), new Object());

        GeneratedIdBasedKeyMapper<Object> testObj = new GeneratedIdBasedKeyMapper<>(objects);

        when(classMetadata.getInstance()).thenReturn(expected);

        String key = testObj.getKey(classMetadata);
        Object actual = testObj.getItem(key);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_give_me_the_same_object_with_the_generated_id_in_case_of_an_object_which_was_not_in_the_collection_originally() {
        ClassMetadata<Object> classMetadata = mock(ClassMetadata.class);

        Object expected = new Object();
        List<Object> objects = Arrays.asList(new Object(), new Object(), new Object(), new Object(), new Object(), new Object());

        GeneratedIdBasedKeyMapper<Object> testObj = new GeneratedIdBasedKeyMapper<>(objects);

        when(classMetadata.getInstance()).thenReturn(expected);

        String key = testObj.getKey(classMetadata);
        Object actual = testObj.getItem(key);

        Assertions.assertEquals(expected, actual);
    }

}
