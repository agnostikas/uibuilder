/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LikeExpressionToRegexpTest {

    @Test
    void likeShouldReturnFalseWhenStrOrExpIsNull() {
        assertFalse(LikeExpressionToRegexp.like(null, ""));
        assertFalse(LikeExpressionToRegexp.like("", null));
        assertFalse(LikeExpressionToRegexp.like(null, null));
    }

    @Test
    void ilikeShouldReturnFalseWhenStrOrExpIsNull() {
        assertFalse(LikeExpressionToRegexp.ilike(null, ""));
        assertFalse(LikeExpressionToRegexp.ilike("", null));
        assertFalse(LikeExpressionToRegexp.ilike(null, null));
    }

    @Test
    void like() {
        assertTrue(LikeExpressionToRegexp.like("asd", "asd"));
        assertFalse(LikeExpressionToRegexp.like("asd", "aSd"));
        assertFalse(LikeExpressionToRegexp.like("asd", "ASD"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "asd"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "AsD"));
        assertTrue(LikeExpressionToRegexp.like("asd", "a_d"));
        assertTrue(LikeExpressionToRegexp.like("asd", "_sd"));
        assertTrue(LikeExpressionToRegexp.like("asd", "as_"));
        assertTrue(LikeExpressionToRegexp.like("asd", "___"));
        assertTrue(LikeExpressionToRegexp.like("asd", "_s_"));
        assertTrue(LikeExpressionToRegexp.like("ASD", "A_D"));
        assertTrue(LikeExpressionToRegexp.like("ASD", "_SD"));
        assertTrue(LikeExpressionToRegexp.like("ASD", "AS_"));
        assertTrue(LikeExpressionToRegexp.like("ASD", "___"));
        assertTrue(LikeExpressionToRegexp.like("ASD", "_S_"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "a_d"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "_sd"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "as_"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "_s_"));
        assertTrue(LikeExpressionToRegexp.like("[](){}.*+?$^|#\\", "[](){}.*+?$^|#\\"));
        assertTrue(LikeExpressionToRegexp.like("[", "["));
        assertTrue(LikeExpressionToRegexp.like("]", "]"));
        assertTrue(LikeExpressionToRegexp.like(".", "."));
        assertTrue(LikeExpressionToRegexp.like("*", "*"));
        assertTrue(LikeExpressionToRegexp.like("?", "?"));
        assertTrue(LikeExpressionToRegexp.like("$", "$"));
        assertTrue(LikeExpressionToRegexp.like("|", "|"));
        assertTrue(LikeExpressionToRegexp.like("[]", "[]"));
        assertTrue(LikeExpressionToRegexp.like("[]", "[_"));
        assertTrue(LikeExpressionToRegexp.like("[]", "_]"));
        assertTrue(LikeExpressionToRegexp.like("[]", "__"));
        assertTrue(LikeExpressionToRegexp.like("\\w", "\\w"));
        assertFalse(LikeExpressionToRegexp.like("asd", "\\w"));

        assertTrue(LikeExpressionToRegexp.like("asd", "%"));
        assertTrue(LikeExpressionToRegexp.like("", "%"));
        assertFalse(LikeExpressionToRegexp.like(null, "%"));
        assertTrue(LikeExpressionToRegexp.like("asd", "a%"));
        assertTrue(LikeExpressionToRegexp.like("asd", "%d"));

        assertTrue(LikeExpressionToRegexp.like("ASD", "%"));
        assertFalse(LikeExpressionToRegexp.like(null, "%"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "a%"));
        assertFalse(LikeExpressionToRegexp.like("ASD", "%d"));

    }

    @Test
    void ilike() {
        assertTrue(LikeExpressionToRegexp.ilike("asd", "asd"));
        assertTrue(LikeExpressionToRegexp.ilike("asd", "aSd"));
        assertTrue(LikeExpressionToRegexp.ilike("asd", "ASD"));
        assertTrue(LikeExpressionToRegexp.ilike("ASD", "asd"));
        assertTrue(LikeExpressionToRegexp.ilike("ASD", "AsD"));
        assertTrue(LikeExpressionToRegexp.ilike("[](){}.*+?$^|#\\", "[](){}.*+?$^|#\\"));
        assertTrue(LikeExpressionToRegexp.ilike("[", "["));
        assertTrue(LikeExpressionToRegexp.ilike("]", "]"));
        assertTrue(LikeExpressionToRegexp.ilike(".", "."));
        assertTrue(LikeExpressionToRegexp.ilike("*", "*"));
        assertTrue(LikeExpressionToRegexp.ilike("?", "?"));
        assertTrue(LikeExpressionToRegexp.ilike("$", "$"));
        assertTrue(LikeExpressionToRegexp.ilike("|", "|"));
        assertTrue(LikeExpressionToRegexp.ilike("[]", "[]"));
        assertTrue(LikeExpressionToRegexp.ilike("[]", "[_"));
        assertTrue(LikeExpressionToRegexp.ilike("[]", "_]"));
        assertTrue(LikeExpressionToRegexp.ilike("[]", "__"));
    }

    @Test
    void replaceSpecial() {
        assertEquals("", LikeExpressionToRegexp.translateString(""));
        assertEquals(" ", LikeExpressionToRegexp.translateString(" "));
        assertEquals("asd ", LikeExpressionToRegexp.translateString("asd "));
        assertEquals("asd \\? ", LikeExpressionToRegexp.translateString("asd ? "));
        assertEquals("asd .*?", LikeExpressionToRegexp.translateString("asd %"));
        assertEquals(".*? asd", LikeExpressionToRegexp.translateString("% asd"));
        assertEquals(".*? asd .*?", LikeExpressionToRegexp.translateString("% asd %"));
        assertEquals(".*? \\.\\*\\? .*?", LikeExpressionToRegexp.translateString("% .*? %"));
    }

    @Test
    void charIn() {
        assertFalse(LikeExpressionToRegexp.charIn('a', "bcdefghijklmnopqrstuvwxyz"), "The alphabet is not contained the searched letter");
        assertFalse(LikeExpressionToRegexp.charIn('a', "_"));
        assertFalse(LikeExpressionToRegexp.charIn('a', "32"));
        assertFalse(LikeExpressionToRegexp.charIn('a', "32f"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "3a2"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "a32"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "32a"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "a32a"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "vaa32a"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "aa  sada"));
        assertTrue(LikeExpressionToRegexp.charIn('a', " a "));
        assertTrue(LikeExpressionToRegexp.charIn('a', " \\a "));
        assertTrue(LikeExpressionToRegexp.charIn('a', " \\a dg"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "* \\a %"));
        assertTrue(LikeExpressionToRegexp.charIn('a', "1234215uregkerjzoitrnmgélmkncvknxcvpoibjifdhjpjhfdjhdfhj9zjhtr %      a"));
        assertTrue(LikeExpressionToRegexp.charIn('[', "[](){}.*+?$^|#\\"));
        assertTrue(LikeExpressionToRegexp.charIn(']', "[](){}.*+?$^|#\\"));
        assertTrue(LikeExpressionToRegexp.charIn('?', "[](){}.*+?$^|#\\"));
        assertTrue(LikeExpressionToRegexp.charIn('#', "[](){}.*+?$^|#\\"));
        assertTrue(LikeExpressionToRegexp.charIn('\\', "[](){}.*+?$^|#\\"));
    }
}
