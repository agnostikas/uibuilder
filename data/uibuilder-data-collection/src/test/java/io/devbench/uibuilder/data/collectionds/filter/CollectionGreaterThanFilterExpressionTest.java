/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class CollectionGreaterThanFilterExpressionTest {

    @Data
    @AllArgsConstructor
    public static class TestPojo {
        private final Integer integer;
    }

    @Test
    public void should_return_only_entities_with_null_properties() {
        List<TestPojo> pojos = Arrays.asList(
            new TestPojo(5),
            new TestPojo(6),
            new TestPojo(7),
            new TestPojo(null)
        );

        CollectionGreaterThanFilterExpression<TestPojo> testObj = new CollectionGreaterThanFilterExpression<>();
        testObj.setPath("integer");
        testObj.setValue(5);

        List<TestPojo> actual = pojos.stream().filter(testObj.toPredicate()).collect(Collectors.toList());
        List<TestPojo> expected = pojos
            .stream()
            .filter(i -> i.getInteger() != null)
            .filter(i -> i.getInteger() == 6 || i.getInteger() == 7)
            .collect(Collectors.toList());
        assertEquals(expected, actual);
    }

}
