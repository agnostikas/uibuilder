/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.interceptors;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.components.util.datasource.DataSourceUtils;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.devbench.uibuilder.data.collectionds.ItemBindingDataSource;
import io.devbench.uibuilder.data.collectionds.ItemDataSourceCapable;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, BaseUIBuilderTestExtension.class})
class ItemDataSourceTagBasedParseInterceptorTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    @InjectMocks
    private ItemDataSourceTagBasedParseInterceptor testObj;

    @Test
    public void should_apply_to_elements_with_item_data_source_children() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);

        assertTrue(testObj.isApplicable(parentElement));
    }

    @Test
    public void should_not_apply_to_elements_without_item_data_source_children() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Collections.singleton(dataSourceElement)))
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));

        when(dataSourceElement.tagName()).thenReturn("foo-bar");

        assertFalse(testObj.isApplicable(parentElement));
    }

    @Test
    public void should_return_binding_context_with_correct_name() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertEquals("testDs", bindingContext.getDataSourceName());
    }

    @Test
    public void should_return_binding_context_with_item_binding() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.hasAttr(ItemDataSourceTagBasedParseInterceptor.ITEMS_ATTRIBUTE_NAME)).thenReturn(true);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.ITEMS_ATTRIBUTE_NAME)).thenReturn("{{foo.bar}}");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.attr(DataSourceUtils.DATASOURCE_NAME)).thenReturn("");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertEquals("{{foo.bar}}", bindingContext.getItemsAttribute());
    }

    @Test
    public void should_return_binding_context_with_correct_name_and_key_paths() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr(ItemDataSourceTagBasedParseInterceptor.KEY_PATHS_ATTRIBUTE_NAME)).thenReturn(true);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.KEY_PATHS_ATTRIBUTE_NAME)).thenReturn("['foo', 'bar']");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertEquals("testDs", bindingContext.getDataSourceName());
        assertEquals(Arrays.asList("foo", "bar"), bindingContext.getKeyPaths());
    }

    @Test
    public void should_handle_bindings_correctly() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children())
            .thenReturn(new Elements(Arrays.asList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);
        testObj.handleBinding("item.foo", bindingContext);
        testObj.handleBinding("bar.xyz", bindingContext);

        assertEquals("testDs", bindingContext.getDataSourceName());
        assertEquals(Sets.newHashSet("item.foo"), bindingContext.getBindings());
    }

    @Test
    public void should_register_binding_context_to_datasource() {
        ItemDataSourceBindingContext context = mock(ItemDataSourceBindingContext.class);
        Component component = mock(Component.class, withSettings().extraInterfaces(ItemDataSourceCapable.class));

        testObj.commitBindingParse(context, component);

        verify(dataSourceManager.getDataSourceProvider()).registerBindingContextForDataSource(context, null);
    }

    @Test
    public void should_set_datasource_name_on_component_on_commit() {
        ItemDataSourceBindingContext context = mock(ItemDataSourceBindingContext.class);
        Component component = mock(Component.class, withSettings().extraInterfaces(ItemDataSourceCapable.class));

        when(context.getDataSourceId()).thenReturn("FooBarDataSource");

        testObj.commitBindingParse(context, component);

        verify((ItemDataSourceCapable) component).connectItemDataSource("FooBarDataSource");
    }

    @Test
    public void should_set_key_paths_on_component_on_commit() {
        class Foo {
            private Collection<String> bar = Collections.singleton("a");
        }
        ItemDataSourceBindingContext context = mock(ItemDataSourceBindingContext.class);
        Component component = mock(Component.class, withSettings().extraInterfaces(ItemDataSourceCapable.class));

        when(context.getItemsAttribute()).thenReturn("{{foo.bar}}");
        when(controllerBeanManager.getBeanMetadata("foo")).thenReturn(ClassMetadata.ofValue(new Foo()));
        doAnswer(i -> {
            Assertions.assertEquals("{{foo.bar}}", ((ItemBindingDataSource) i.getArgument(0)).getBindingPath());
            return null;
        }).when(((ItemDataSourceCapable) component)).setDataSource(any());

        testObj.commitBindingParse(context, component);

        verify((ItemDataSourceCapable) component, times(1)).setDataSource(any());
    }

    @Test
    @DisplayName("Should read sortPath and sortDirection attributes")
    void test_should_read_sort_path_and_sort_direction_attributes() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Collections.singletonList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        mockAttribute(dataSourceElement, "id", "id");
        mockAttribute(dataSourceElement, "sort-path", "text");
        mockAttribute(dataSourceElement, "sort-direction", "desc");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertEquals("text", bindingContext.getSortPath());
        assertEquals(SortOrder.Direction.DESCENDING, bindingContext.getSortDirection());
    }

    @Test
    @DisplayName("Should set ascending when sortDirection is absent")
    void test_should_set_ascending_when_sort_direction_is_absent() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Collections.singletonList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());
        mockAttribute(dataSourceElement, "id", "id");
        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);
        assertEquals(SortOrder.Direction.ASCENDING, bindingContext.getSortDirection());
    }

    private void mockAttribute(Element mockElement, String attributeName, String attributeValue) {
        when(mockElement.hasAttr(attributeName)).thenReturn(true);
        when(mockElement.attr(attributeName)).thenReturn(attributeValue);
    }

    @Test
    @DisplayName("commit should store binding context into deposit and when the same datasource has been requested to be created")
    void test_commit_should_store_binding_context_into_deposit_and_when_the_same_datasource_has_been_requested_to_be_created() {
        Element parentElement = mock(Element.class);
        Element dataSourceElement = mock(Element.class);

        when(parentElement.children()).thenReturn(new Elements(Collections.singletonList(dataSourceElement)));
        when(dataSourceElement.tagName()).thenReturn(ItemDataSourceTagBasedParseInterceptor.DATA_SOURCE_TAG_NAME);
        when(dataSourceElement.attr(ItemDataSourceTagBasedParseInterceptor.NAME_ATTRIBUTE_NAME)).thenReturn("testDs");
        when(dataSourceElement.hasAttr("id")).thenReturn(true);
        when(dataSourceElement.attr("id")).thenReturn("id");
        when(dataSourceElement.getElementsByTag(anyString())).thenReturn(new Elements());

        ItemDataSourceBindingContext bindingContext = testObj.createBindingContext(parentElement);

        assertNotNull(bindingContext);

        String datasourceId = bindingContext.getDataSourceId();
        assertTrue(StringUtils.isNotBlank(datasourceId));

        testObj.commitBindingParse(bindingContext, null);

        ItemDataSourceBindingContext anotherBindingContext = testObj.createBindingContext(parentElement);

        assertNotSame(bindingContext, anotherBindingContext);

        String anotherDatasourceId = anotherBindingContext.getDataSourceId();
        assertTrue(StringUtils.isNotBlank(anotherDatasourceId));

        assertNotEquals(anotherDatasourceId, datasourceId);
        assertEquals("testDs", anotherBindingContext.getDataSourceName());
    }
}
