/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.datasource;

import com.google.common.collect.Sets;
import io.devbench.uibuilder.api.exceptions.InvalidPropertyBindingException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.collectionds.ItemBindingDataSource;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class ItemBindingDataSourceTest {


    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    @Test
    public void should_return_given_collection_on_get_collection_when_binding_is_correct() {
        class Foo {
            private Collection<String> bar = new ArrayList<>(Arrays.asList("a", "b"));
        }

        Foo value = new Foo();
        when(controllerBeanManager.getBeanMetadata("foo")).thenReturn(ClassMetadata.ofValue(value));

        ItemBindingDataSource<String> testObj = new ItemBindingDataSource<>(new TestBindingContext(), Collections.emptyList(), "{{foo.bar}}");
        assertEquals(Arrays.asList("a", "b"), testObj.getItems());
        testObj.getItems().add("fff");
        assertEquals(Arrays.asList("a", "b", "fff"), value.bar);
    }

    @Test
    public void should_throw_exception_when_binding_path_is_nonsense() {
        assertThrows(
            InvalidPropertyBindingException.class,
            () -> new ItemBindingDataSource<>(new TestBindingContext(), Arrays.asList(), "pattern-must-not-match")
        );
    }

    @Test
    public void should_throw_exception_when_collection_is_not_available() {
        assertThrows(
            InvalidPropertyBindingException.class,
            () -> new ItemBindingDataSource<>(new TestBindingContext(), Arrays.asList(), "{{randomBean.nonExistingCollection}}")
        );
    }

    class TestBindingContext extends ItemDataSourceBindingContext {

        @Override
        public @NotNull Set<String> getBindings() {
            return Sets.newHashSet();
        }

        @Override
        public @Nullable String getDataSourceName() {
            return null;
        }

        @Override
        public @NotNull String getDataSourceId() {
            return null;
        }
    }
}
