/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds;

import com.google.common.collect.Sets;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.devbench.uibuilder.data.collectionds.filter.CollectionIsNullFilterExpression;
import io.devbench.uibuilder.data.collectionds.filter.CollectionLikeExpression;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import io.devbench.uibuilder.data.common.dataprovidersupport.OrmKeyMapper;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataFilter;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataProviderRequest;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataRequestBody;
import io.devbench.uibuilder.data.common.datasource.DefaultFilterDescriptor;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class CollectionDataSourceTest {

    @Data
    public static class TestInnerPojo {
        private final String foo;
    }

    @Data
    public static class TestPojo {
        private final String id;
        private final String foo;
        private final String bar;
        private final Integer xyz;
        private final TestInnerPojo inner;
    }

    @Test
    public void should_return_correct_element_type_when_theres_element_in_the_collection() {
        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.singletonList("id"),
            Collections.singletonList(new TestPojo("3", "foo-third", "bar-3", 5, null))
        );
        assertEquals(TestPojo.class, testObj.getElementType());
    }

    @Test
    public void should_return_id_based_key_mapper_when_theres_id_path() {
        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.singletonList("id"),
            Collections.emptyList()
        );
        assertEquals(OrmKeyMapper.class, testObj.createKeyMapper().getClass());
    }

    @Test
    public void should_return_uuid_based_key_mapper_when_theres_no_id_path() {
        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.emptyList(),
            Collections.emptyList()
        );
        assertEquals(GeneratedIdBasedKeyMapper.class, testObj.createKeyMapper().getClass());
    }

    @Test
    public void should_return_object_element_type_when_theres_element_in_the_collection() {
        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.singletonList("id"),
            Collections.emptyList()
        );
        assertEquals(Object.class, testObj.getElementType());
    }

    @Test
    public void should_return_collection_as_is_when_no_ordering_no_filtering_present() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1", 10, null),
            new TestPojo("2", "foo-second", "bar-2", 15, null),
            new TestPojo("3", "foo-third", "bar-3", 5, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        assertEquals(testPojos, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_return_collection_size_when_fetching_size() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1", 10, null),
            new TestPojo("2", "foo-second", "bar-2", 15, null),
            new TestPojo("3", "foo-third", "bar-3", 5, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        assertEquals(3, testObj.fetchSize((PagingFetchRequest) null));
    }

    @Test
    public void should_throw_sort_path_invalid_exception_on_invalid_path() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1", 10, null),
            new TestPojo("1", "foo-first", "bar-1", 10, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionOrderExpression orderExpression = new CollectionOrderExpression();
        orderExpression.addOrder(new SortOrder("invalid-path", SortOrder.Direction.ASCENDING));
        testObj.registerOrder(orderExpression);

        assertThrows(SortPathInvalidException.class, () -> testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_order_nulls_first_when_ordering_on_nullable_property() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", null, 10, null),
            new TestPojo("1", "foo-first", "bar-1", 10, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionOrderExpression orderExpression = new CollectionOrderExpression();
        orderExpression.addOrder(new SortOrder("bar", SortOrder.Direction.ASCENDING));
        testObj.registerOrder(orderExpression);

        List<TestPojo> expected = testPojos
            .stream()
            .sorted(Comparator.comparing(TestPojo::getBar, Comparator.nullsLast(Comparable::compareTo)))
            .collect(Collectors.toList());

        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_return_ordered_collection_correctly_when_simple_ordering_is_present() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-99", 10, null),
            new TestPojo("2", "foo-second", "bar-98", 15, null),
            new TestPojo("3", "foo-third", "bar-97", 5, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionOrderExpression orderExpression = new CollectionOrderExpression();
        orderExpression.addOrder(new SortOrder("bar", SortOrder.Direction.ASCENDING));
        testObj.registerOrder(orderExpression);

        List<TestPojo> expected = testPojos.stream().sorted(Comparator.comparing(TestPojo::getBar)).collect(Collectors.toList());
        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_return_ordered_collection_correctly_when_multiple_ordering_is_present() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-99", 10, null),
            new TestPojo("2", "foo-second", "bar-99", 5, null),
            new TestPojo("3", "foo-third", "bar-97", 10, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionOrderExpression orderExpression = new CollectionOrderExpression();
        orderExpression.addOrder(new SortOrder("bar", SortOrder.Direction.ASCENDING));
        orderExpression.addOrder(new SortOrder("xyz", SortOrder.Direction.ASCENDING));
        testObj.registerOrder(orderExpression);

        List<TestPojo> expected = testPojos
            .stream()
            .sorted(Comparator.comparing(TestPojo::getBar).thenComparing(TestPojo::getXyz))
            .collect(Collectors.toList());

        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_return_ordered_collection_correctly_when_multiple_ordering_is_present_descending() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-99", 10, null),
            new TestPojo("2", "foo-second", "bar-99", 5, null),
            new TestPojo("3", "foo-third", "bar-97", 10, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionOrderExpression orderExpression = new CollectionOrderExpression();
        orderExpression.addOrder(new SortOrder("bar", SortOrder.Direction.ASCENDING));
        orderExpression.addOrder(new SortOrder("xyz", SortOrder.Direction.DESCENDING));
        testObj.registerOrder(orderExpression);

        List<TestPojo> expected = testPojos
            .stream()
            .sorted(Comparator.comparing(TestPojo::getBar).thenComparing(Comparator.comparing(TestPojo::getXyz).reversed()))
            .collect(Collectors.toList());

        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_return_filtered_collection_when_simple_filtering_present() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1", 10, null),
            new TestPojo("2", "foo-second", "bar-2", null, null),
            new TestPojo("3", "foo-third", "bar-3", 5, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionIsNullFilterExpression<TestPojo> expression = new CollectionIsNullFilterExpression<>();
        expression.setPath("xyz");
        testObj.registerFilter(expression);

        List<TestPojo> expected = testPojos.stream().filter(t -> t.getXyz() == null).collect(Collectors.toList());

        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    public void should_return_element_when_filtering_on_key() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1", 10, null),
            new TestPojo("2", "foo-second", "bar-2", null, null),
            new TestPojo("3", "foo-third", "bar-3", 5, null)
        );

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Arrays.asList("id"),
            testPojos
        );

        CollectionIsNullFilterExpression<TestPojo> expression = new CollectionIsNullFilterExpression<>();
        expression.setPath("xyz");

        TestPojo expected = testPojos.stream().filter(t -> t.getXyz() == null).findFirst().get();

        assertEquals(expected, testObj.findElementByKeyFilter(expression));
    }

    @Test
    @DisplayName("Should return filtered collection with one matching item")
    public void should_return_filtered_collection_with_one_matching_item() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1", 10, null),
            new TestPojo("2", "foo-second", "bar-2", null, null),
            new TestPojo("3", "foo-third", "bar-3", 5, null)
        );

        final String filterExpression = "first";
        final String filterPath = "foo";

        ItemDataSourceBindingContext mockBindingContext = Mockito.mock(ItemDataSourceBindingContext.class);
        Mockito.when(mockBindingContext.getDefaultFilterDescriptor()).thenReturn(new DefaultFilterDescriptor());
        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(mockBindingContext, Collections.singletonList("id"), testPojos);

        DataProviderRequest request = createBaseRequest();
        DataRequestBody body = new DataRequestBody();

        DataFilter dataFilter = new DataFilter();
        dataFilter.setPath(filterPath);
        dataFilter.setValue(filterExpression);

        body.setFilters(Arrays.asList(dataFilter));
        request.setBody(body);

        TestPojo expected = testPojos.stream().filter(t -> t.getFoo().contains(filterExpression)).findFirst().get();

        assertEquals(1, testObj.fetchSize(request).intValue());
        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null).stream().findFirst().get());
    }

    @Test
    @DisplayName("Should return filtered collection with more matching items")
    public void should_return_filtered_collection_with_two_matching_item() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-first", "bar-1 common", 10, null),
            new TestPojo("2", "foo-second", "bar-2 common", null, null),
            new TestPojo("3", "foo-third", "bar-3", 5, null)
        );

        final String filterExpression = "common";
        final String filterPath = "bar";

        ItemDataSourceBindingContext mockBindingContext = Mockito.mock(ItemDataSourceBindingContext.class);
        Mockito.when(mockBindingContext.getDefaultFilterDescriptor()).thenReturn(new DefaultFilterDescriptor());
        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(mockBindingContext, Collections.singletonList("id"), testPojos);

        DataProviderRequest request = createBaseRequest();
        DataRequestBody body = new DataRequestBody();

        DataFilter dataFilter = new DataFilter();
        dataFilter.setPath(filterPath);
        dataFilter.setValue(filterExpression);

        body.setFilters(Arrays.asList(dataFilter));
        request.setBody(body);

        List<TestPojo> expected = testPojos.stream().filter(t -> t.getBar().contains(filterExpression)).collect(Collectors.toList());

        assertEquals(2, testObj.fetchSize(request).intValue());
        assertEquals(expected, testObj.fetchData((PagingFetchRequest) null));
    }

    @Test
    @DisplayName("Should return slice of the items when the source is List and the page and pageSize are defined")
    void test_should_return_slice_of_the_items_when_the_source_is_list_and_the_page_and_page_size_are_defined() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-1", "bar-1", 10, null),
            new TestPojo("2", "foo-2", "bar-2", null, null),
            new TestPojo("3", "foo-3", "bar-3", 5, null),
            new TestPojo("4", "foo-4", "bar-4-baz", 5, null),
            new TestPojo("5", "foo-5", "bar-5-baz", 5, null),
            new TestPojo("6", "foo-6", "bar-6-baz", 5, null),
            new TestPojo("7", "foo-7", "bar-7-baz", 5, null),
            new TestPojo("8", "foo-8", "bar-8", 5, null));

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.singletonList("id"), testPojos);

        @SuppressWarnings("unchecked")
        CollectionLikeExpression<TestPojo> collectionLikeExpression = testObj.getFilterExpressionFactory().create(CollectionLikeExpression.class);
        collectionLikeExpression.setPath("bar");
        collectionLikeExpression.setValue("%baz");
        testObj.registerFilter(collectionLikeExpression);

        PagingFetchRequest pagingFetchRequest = PagingFetchRequest.builder()
            .page(1)
            .pageSize(2)
            .build();

        List<TestPojo> expected = Arrays.asList(testPojos.get(5), testPojos.get(6));

        assertEquals(4, testObj.fetchSize(pagingFetchRequest));
        assertEquals(expected, testObj.fetchData(pagingFetchRequest));
    }

    @Test
    @DisplayName("Should return slice of the items when the datasource is ordered and the page and pageSize are defined")
    void test_should_return_slice_of_the_items_when_the_datasource_is_ordered_and_the_page_and_page_size_are_defined() {
        List<TestPojo> testPojoList = Arrays.asList(
            new TestPojo("1", "foo-1", "bar-1", 10, null),
            new TestPojo("4", "foo-4", "bar-4-baz", 5, null),
            new TestPojo("3", "foo-3", "bar-3", 5, null),
            new TestPojo("7", "foo-7", "bar-7-baz", 5, null),
            new TestPojo("2", "foo-2", "bar-2", null, null),
            new TestPojo("6", "foo-6", "bar-6-baz", 5, null),
            new TestPojo("5", "foo-5", "bar-5-baz", 5, null),
            new TestPojo("8", "foo-8", "bar-8", 5, null));
        Set<TestPojo> testPojos = new HashSet<>(testPojoList);

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.singletonList("id"), testPojos);

        @SuppressWarnings("unchecked")
        CollectionLikeExpression<TestPojo> collectionLikeExpression = testObj.getFilterExpressionFactory().create(CollectionLikeExpression.class);
        collectionLikeExpression.setPath("bar");
        collectionLikeExpression.setValue("%baz");
        testObj.registerFilter(collectionLikeExpression);

        CollectionOrderExpression orderExpression = testObj.getOrderExpressionFactory().create();
        orderExpression.addOrder(new SortOrder("foo", SortOrder.Direction.ASCENDING));
        testObj.registerOrder(orderExpression);

        PagingFetchRequest pagingFetchRequest = PagingFetchRequest.builder()
            .page(1)
            .pageSize(2)
            .build();

        List<TestPojo> expected = Arrays.asList(testPojoList.get(5), testPojoList.get(3));

        assertEquals(4, testObj.fetchSize(pagingFetchRequest));
        assertEquals(expected, testObj.fetchData(pagingFetchRequest));
    }

    @Test
    @DisplayName("Should get index of the item when the datasource is ordered")
    void test_should_get_index_of_the_item_when_the_datasource_is_ordered() {
        List<TestPojo> testPojos = Arrays.asList(
            new TestPojo("1", "foo-1", "bar-1", 10, null),
            new TestPojo("2", "foo-2", "bar-2", null, null),
            new TestPojo("3", "foo-3", "bar-3", 5, null),
            new TestPojo("4", "foo-4", "bar-4-baz", 5, null),
            new TestPojo("5", "foo-5", "bar-5-baz", 5, null),
            new TestPojo("6", "foo-6", "bar-6-baz", 5, null),
            new TestPojo("7", "foo-7", "bar-7-baz", 5, null),
            new TestPojo("8", "foo-8", "bar-8", 5, null));

        CollectionDataSource<TestPojo> testObj = new CollectionDataSource<>(
            new TestBindingContext(Sets.newHashSet("foo", "bar")),
            Collections.singletonList("id"), testPojos);

        Optional<Integer> indexOfItem = testObj.getIndexOfItem(testPojos.get(5));
        assertTrue(indexOfItem.isPresent());
        assertEquals(5, indexOfItem.get());

        @SuppressWarnings("unchecked")
        CollectionLikeExpression<TestPojo> collectionLikeExpression = testObj.getFilterExpressionFactory().create(CollectionLikeExpression.class);
        collectionLikeExpression.setPath("bar");
        collectionLikeExpression.setValue("%baz");
        testObj.registerFilter(collectionLikeExpression);

        indexOfItem = testObj.getIndexOfItem(testPojos.get(5));
        assertTrue(indexOfItem.isPresent());
        assertEquals(2, indexOfItem.get());

        Optional<Map<? extends TestPojo, Integer>> indexOfItems = testObj.getIndexOfItems();
        assertTrue(indexOfItems.isPresent());
        Map<? extends TestPojo, Integer> map = indexOfItems.get();
        assertEquals(2, map.get(testPojos.get(5)));
        assertEquals(0, map.get(testPojos.get(3)));
        assertNull(map.get(testPojos.get(2)));
    }


    private DataProviderRequest createBaseRequest() {
        DataProviderRequest request = new DataProviderRequest();
        request.setEndpointId("test-id");
        request.setRequestType(DataProviderRequest.RequestType.FETCH_DATA);
        return request;
    }

    @Getter
    @RequiredArgsConstructor
    static class TestBindingContext extends ItemDataSourceBindingContext {

        private final Set<String> bindings;

        @Override
        public @Nullable String getDataSourceName() {
            return "testDS";
        }

        @Override
        public @NotNull String getDataSourceId() {
            return "test-ds-is";
        }
    }
}
