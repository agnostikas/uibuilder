/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class CollectionOrFilterExpressionTest {

    @Data
    @AllArgsConstructor
    public static class TestPojo {
        private final String string;
    }

    @Test
    public void should_return_only_entities_with_null_properties() {
        List<TestPojo> pojos = Arrays.asList(
            new TestPojo(null),
            new TestPojo("a"),
            new TestPojo(null),
            new TestPojo("b")
        );

        CollectionEqualsFilterExpression<TestPojo> isEqA = new CollectionEqualsFilterExpression<>();
        isEqA.setPath("string");
        isEqA.setValue("b");

        CollectionIsNullFilterExpression<TestPojo> isNull = new CollectionIsNullFilterExpression<>();
        isNull.setPath("string");

        CollectionOrFilterExpression<TestPojo> testObj = new CollectionOrFilterExpression<>();
        testObj.getExpressions().add(isEqA);
        testObj.getExpressions().add(isNull);

        List<TestPojo> actual = pojos.stream().filter(testObj.toPredicate()).collect(Collectors.toList());
        List<TestPojo> expected = pojos.stream().filter(t -> !Objects.equals(t.getString(), "a")).collect(Collectors.toList());
        assertEquals(expected, actual);
    }

}
