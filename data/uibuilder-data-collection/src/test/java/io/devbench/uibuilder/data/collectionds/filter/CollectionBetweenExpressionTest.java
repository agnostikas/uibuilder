/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.collectionds.filter;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.collectionds.exceptions.FilterPathInvalidException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CollectionBetweenExpressionTest {

    @Data
    @AllArgsConstructor
    public static class TestIntPojo {
        private final Integer integer;
    }

    @Test
    public void should_return_only_entities_with_null_properties() {
        List<TestIntPojo> pojos = Arrays.asList(
            new TestIntPojo(1),
            new TestIntPojo(2),
            new TestIntPojo(3),
            new TestIntPojo(4),
            new TestIntPojo(5),
            new TestIntPojo(6)
        );

        CollectionBetweenExpression<TestIntPojo> testObj = new CollectionBetweenExpression<>();
        testObj.setPath("integer");
        testObj.setValues(Arrays.asList(2, 5));

        List<TestIntPojo> expected = pojos.stream().filter(t -> t.getInteger() >= 2).filter(t -> t.getInteger() <= 5).collect(Collectors.toList());
        List<TestIntPojo> actual = pojos.stream().filter(testObj.toPredicate()).collect(Collectors.toList());
        assertEquals(expected, actual);
    }

    @Test
    public void should_throw_invalid_filter_path_exception_on_invalid_path() {
        CollectionBetweenExpression<TestIntPojo> testObj = new CollectionBetweenExpression<>();
        testObj.setPath("foobar");
        testObj.setValues(Arrays.asList(2, 5));
        assertThrows(
            FilterPathInvalidException.class,
            () -> Stream.of(new TestIntPojo(5)).filter(testObj.toPredicate()).forEach(System.out::println)
        );
    }

    @Test
    public void should_throw_invalid_filter_exception_on_multiple_values() {
        CollectionBetweenExpression<TestIntPojo> testObj = new CollectionBetweenExpression<>();
        testObj.setPath("string");
        testObj.setValues(Arrays.asList(2, 5, 7));
        assertThrows(
            FilterException.class,
            () -> Stream.of(new TestIntPojo(5)).filter(testObj.toPredicate()).forEach(System.out::println)
        );
    }

}
