/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.filterdescriptors;

import io.devbench.uibuilder.data.common.filter.comperingfilters.AnyOperandFilterExpression;
import lombok.Getter;

import java.util.Collection;
import java.util.Collections;

@Getter
public class AnyOperandFilterDescriptor extends ComperingFilterDescriptor {

    private final String path;
    private final Class<? extends AnyOperandFilterExpression> operator;
    private final Collection<Object> values;

    public AnyOperandFilterDescriptor(String path, Class<? extends AnyOperandFilterExpression> operator, Collection<Object> values) {
        this.path = path;
        this.operator = operator;
        this.values = Collections.unmodifiableCollection(values);
    }

}
