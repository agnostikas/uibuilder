/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.metadata;

import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.core.controllerbean.uiproperty.StringPropertyConverter;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import java.util.Optional;
import java.util.function.Supplier;

public class CommonBindingMetadataProvider<ELEMENT> implements BindingMetadataProvider {

    private final Supplier<Class<ELEMENT>> elementTypeProvider;

    public CommonBindingMetadataProvider(Supplier<Class<ELEMENT>> elementTypeProvider) {
        this.elementTypeProvider = elementTypeProvider;
    }

    @Override
    public BindingMetadata getMetadataForPath(String path) {
        BindingMetadata bindingMetadata = new BindingMetadata();
        Optional<PropertyMetadata<?>> property = ClassMetadata.ofClass(elementTypeProvider.get()).property(path);
        if (property.isPresent()) {
            bindingMetadata.setConverter(PropertyConverters.getConverterFor(property.get()));
            bindingMetadata.setPropertyType(property.get().getType());
        }
        bindingMetadata.setPath(path);
        return bindingMetadata;
    }
}
