/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.Component;
import lombok.Getter;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

class DataProviderEndpointWeakReference extends WeakReference<Component> {

    @Getter
    private DataProviderEndpointRegistration endpoint;

    DataProviderEndpointWeakReference(Component component, DataProviderEndpointRegistration endpoint, ReferenceQueue<Component> referenceQueue) {
        super(component, referenceQueue);
        this.endpoint = endpoint;
    }
}
