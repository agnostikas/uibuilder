/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.comperingfilters;

import io.devbench.uibuilder.data.common.filter.filterdescriptors.BinaryOperandFilterDescriptor;
import lombok.Getter;
import lombok.Setter;

public abstract class BinaryOperandFilterExpression<PREDICATE> extends ComperingFilterExpression<PREDICATE> {

    @Setter
    @Getter
    private Object value;

    protected abstract Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass();

    @Override
    @SuppressWarnings("unchecked")
    public BinaryOperandFilterDescriptor getImmutableDescriptor() {
        return new BinaryOperandFilterDescriptor(getPath(), getOperatorDescribingClass(), value);
    }
}
