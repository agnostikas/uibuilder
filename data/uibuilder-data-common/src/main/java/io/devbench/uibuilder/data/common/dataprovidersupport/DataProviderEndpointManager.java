/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.shared.ApplicationConstants;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.controllerbean.injection.ItemResolver;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.core.session.context.UIContextStored;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.api.exceptions.DataProviderEndpointException;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataProviderRequest;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataResponse;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.lang.ref.ReferenceQueue;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@UIContextStored
public class DataProviderEndpointManager implements ItemResolver {

    private ReferenceQueue<Component> referenceQueue = new ReferenceQueue<>();
    private Map<String, DataProviderEndpointWeakReference> endpointMap;

    private DataProviderEndpointManager() {
        endpointMap = new ConcurrentHashMap<>();
        new DataProviderEndpointRegistrationCleanUpDaemon(referenceQueue, endpointMap).start();
        UIContext.getContext().addToActiveUIContextAs(ItemResolver.class, this);
    }

    private DataProviderEndpointRegistration getEndpointById(String id) {
        DataProviderEndpointWeakReference reference = endpointMap.get(id);
        if (reference != null && reference.get() != null) {
            return reference.getEndpoint();
        } else {
            endpointMap.remove(id);
            throw new DataProviderEndpointException(String.format("Invalid or expired endpoint (%s)", id));
        }
    }

    private CommonDataSource<?, ?, ?, ?> createDataSource(String dataSourceId, String defaultQuery, Component component) {
        return (CommonDataSource<?, ?, ?, ?>) DataSourceManager
            .getInstance()
            .getDataSourceProvider()
            .getDataSource(dataSourceId, new CommonDataSourceSelector(defaultQuery, component));
    }

    private DataProviderEndpointRegistration register(Component component, DataProviderEndpointRegistration datasourceEndpointRegistration) {
        endpointMap.put(
            datasourceEndpointRegistration.getEndpointId(),
            new DataProviderEndpointWeakReference(component, datasourceEndpointRegistration, referenceQueue));
        return datasourceEndpointRegistration;
    }

    public DataProviderEndpointRegistration register(Component component, String dataSourceId, String dataSourceName, String defaultQuery) {
        CommonDataSource<?, ?, ?, ?> datasource = createDataSource(dataSourceId, defaultQuery, component);
        return register(component, datasource);
    }

    public DataProviderEndpointRegistration register(Component component, CommonDataSource<?, ?, ?, ?> dataSource) {
        final String endpointId = dataSource.getDataSourceId();
        final String url = "/datasource-api/" + endpointId + "?" + ApplicationConstants.UI_ID_PARAMETER + "=" + UI.getCurrent().getUIId();
        final DataProviderEndpointRegistration datasourceEndpointRegistration = DataProviderEndpointRegistration.of(endpointId, dataSource, url);
        return register(component, datasourceEndpointRegistration);
    }

    public Optional<Component> getComponent(String endpointId) {
        DataProviderEndpointWeakReference reference = endpointMap.get(endpointId);
        if (reference == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(reference.get());
    }

    public DataResponse processFetchDataRequest(DataProviderRequest request) {
        DataProviderEndpointRegistration endpoint = getEndpointById(request.getEndpointId());
        return endpoint.getDataSource().fetchData(request);
    }

    public Long processFetchSizeRequest(DataProviderRequest request) {
        return getEndpointById(request.getEndpointId()).getDataSource().fetchSize(request);
    }

    public static DataProviderEndpointManager getInstance() {
        return UIContext.getContext().computeIfAbsent(DataProviderEndpointManager.class, DataProviderEndpointManager::new);
    }

    @Override
    public Optional<Object> getByItemKey(@NotNull JsonObject rawItem) {
        if (rawItem.hasKey(DataProcessor.ITEM_ENDPOINT_ID) && rawItem.hasKey(DataProcessor.ITEM_KEY)) {
            final String endpointId = rawItem.getString(DataProcessor.ITEM_ENDPOINT_ID);
            final String itemKey = rawItem.getString(DataProcessor.ITEM_KEY);
            return Optional.ofNullable(getEndpointById(endpointId).getDataSource().findItemByIdValue(itemKey));
        }
        return Optional.empty();
    }

    public Optional<CommonDataSource<?, ?, ?, ?>> getDataSourceByItemKey(@NotNull JsonObject rawItem) {
        if (rawItem.hasKey(DataProcessor.ITEM_ENDPOINT_ID)) {
            final String endpointId = rawItem.getString(DataProcessor.ITEM_ENDPOINT_ID);
            return Optional.ofNullable(getEndpointById(endpointId).getDataSource());
        }
        return Optional.empty();
    }

}
