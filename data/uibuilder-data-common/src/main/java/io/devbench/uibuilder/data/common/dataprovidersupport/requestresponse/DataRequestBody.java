/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse;

import com.google.gson.annotations.JsonAdapter;
import elemental.json.JsonValue;
import io.devbench.uibuilder.data.api.order.SortOrder;
import lombok.Data;

import java.util.List;

@Data
public class DataRequestBody {

    /**
     * Requested page index
     */
    private Integer page;

    /**
     * Current page size
     */
    private Integer pageSize;

    /**
     * Currently applied filters
     */
    private List<DataFilter> filters;

    /**
     * Currently applied sorting orders
     */
    private List<SortOrder> sortOrders;

    /**
     * A flag to reset the current filters
     */
    private Boolean resetFilters;

    /**
     * Basically a json object representing a whole row in the tree-grid, but in a simple grid it will be
     * absent always.
     * <p>
     * When tree is used, and sublevel items are requested, reference to parent item of the requested
     * sublevel. Otherwise undefined.
     */
    @JsonAdapter(JsonElementToJsonValueConverter.class)
    private JsonValue parentItem;
}
