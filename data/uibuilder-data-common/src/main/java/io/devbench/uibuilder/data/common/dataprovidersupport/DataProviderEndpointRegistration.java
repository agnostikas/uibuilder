/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import lombok.Getter;

import java.io.Closeable;
import java.io.Serializable;

@Getter
public class DataProviderEndpointRegistration implements Serializable, Closeable {

    private String endpointId;
    private String url;
    private CommonDataSource<?, ?, ?, ?> dataSource;

    private DataProviderEndpointRegistration() {
    }

    public void close() {
        // TODO: Close datasource and endpoint
    }

    @SuppressWarnings("unchecked")
    public <ELEMENT> CommonDataSource<ELEMENT, ?, ?, ?> getDataSource() {
        return (CommonDataSource<ELEMENT, ?, ?, ?>) dataSource;
    }

    public static DataProviderEndpointRegistration of(String endpointId, CommonDataSource<?, ?, ?, ?> dataSource, String url) {
        DataProviderEndpointRegistration datasourceEndpointRegistration = new DataProviderEndpointRegistration();
        datasourceEndpointRegistration.endpointId = endpointId;
        datasourceEndpointRegistration.url = url;
        datasourceEndpointRegistration.dataSource = dataSource;
        return datasourceEndpointRegistration;
    }
}
