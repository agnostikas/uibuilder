/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.HasEnabled;
import elemental.json.*;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.common.component.ItemPredicateProvider;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.InlineEditHandler;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.SimpleInlineEditHandler;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataResponse;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.exceptions.DataSourceItemKeyNotFoundException;
import io.devbench.uibuilder.data.common.item.ItemState;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DataProcessor<ELEMENT> {

    public static final String ITEM_KEY = "___ITEM_KEY";
    public static final String ITEM_ENDPOINT_ID = "__ITEM_ENDPOINT_ID";
    public static final String PRIMITIVE_VALUE_KEY = "___PRIMITIVE_VALUE";
    public static final String ITEM = "item";
    public static final String ITEM_WITH_DOT = "item.";
    public static final String LEAF = "item.leaf";

    @Getter
    private final CommonDataSource<ELEMENT, ?, ?, ?> dataSource;

    @Getter
    private final KeyMapper<ELEMENT> keyMapper;
    private final BindingMetadataProvider metadataProvider;
    private final Map<String, BindingMetadata> metadataCache;
    @Getter
    private InlineEditHandler inlineEditHandler;

    public DataProcessor(
        CommonDataSource<ELEMENT, ?, ?, ?> dataSource,
        BindingMetadataProvider metadataProvider,
        Supplier<KeyMapper<ELEMENT>> keyMapperSupplier
    ) {
        this.dataSource = dataSource;
        this.metadataProvider = metadataProvider;
        this.keyMapper = keyMapperSupplier.get();
        this.metadataCache = new HashMap<>();
        this.inlineEditHandler = new SimpleInlineEditHandler();
    }

    @NotNull
    public static String getItemKey(@NotNull JsonObject jsonItem) {
        JsonValue jsonValue = jsonItem.get(ITEM_KEY);
        if (jsonValue instanceof JsonString) {
            return jsonValue.asString();
        }
        throw new DataSourceItemKeyNotFoundException("Could not find datasource item key in json object: " + jsonItem);
    }

    public JsonArray convertToKeysArray(@NotNull Collection<? extends ELEMENT> items) {
        return getJsonArray(items, (ignore) -> Json.createObject());
    }

    public DataResponse convertToDataResponse(@NotNull Collection<? extends ELEMENT> items) {
        Objects.requireNonNull(items);
        DataResponse dataResponse = new DataResponse();
        JsonArray jsonArray = getJsonArray(items,
            (finalClassMetadata) -> Stream.concat(mapBingingToValue(finalClassMetadata), mapHierarchicBinding(finalClassMetadata))
                .collect(Json::createObject, this::addPathValueToJsonObject, this::combineJsonObjects));
        applyInlineEditablePredicate(jsonArray);
        handleInlineEditPropertyChanges(jsonArray);
        dataResponse.setResponse(jsonArray);
        return dataResponse;
    }

    private void applyInlineEditablePredicate(@NotNull JsonArray jsonArray) {
        DataProviderEndpointManager.getInstance()
            .getComponent(dataSource.getDataSourceId())
            .filter(ItemPredicateProvider.class::isInstance)
            .map(ItemPredicateProvider.class::cast)
            .ifPresent(itemPredicateProvider -> {
                onJsonItem(jsonArray, jsonItem -> true, jsonItem -> {
                    ELEMENT item = keyMapper.getItem(getItemKey(jsonItem));
                    @SuppressWarnings("unchecked")
                    Predicate<ELEMENT> itemPredicate = itemPredicateProvider.getItemPredicate();
                    if (itemPredicate != null) {
                        boolean editAllowed = itemPredicate.test(item);
                        ItemState itemState = inlineEditHandler.getItemState(jsonItem);
                        itemState.setEditAllowed(editAllowed);
                        if (!editAllowed) {
                            itemState.setEditMode(false);
                        }
                        itemState.applyOnJsonItem(jsonItem);
                    }
                });
            });
    }

    private void handleInlineEditPropertyChanges(@NotNull JsonArray jsonArray) {
        DataProviderEndpointManager.getInstance()
            .getComponent(dataSource.getDataSourceId())
            .filter(HasEnabled.class::isInstance)
            .map(HasEnabled.class::cast)
            .ifPresent(component -> {
                if (component.isEnabled()) {
                    onJsonItem(jsonArray, jsonItem -> inlineEditHandler.isEditMode(jsonItem), jsonItem -> {
                        for (String itemKey : jsonItem.keys()) {
                            if (!itemKey.startsWith("_") && inlineEditHandler.hasChangedPropertyValue(jsonItem, itemKey)) {
                                jsonItem.put(itemKey, inlineEditHandler.getChangedPropertyValue(jsonItem, itemKey));
                            }
                        }
                    });
                } else {
                    onJsonItem(jsonArray, jsonItem -> true, jsonItem -> {
                        ItemState itemState = inlineEditHandler.getItemState(jsonItem);
                        itemState.setEditAllowed(false);
                        itemState.setSaveAllowed(false);
                        itemState.applyOnJsonItem(jsonItem);
                    });
                }
            });
    }

    private void onJsonItem(@NotNull JsonArray array, Predicate<JsonObject> jsonItemPredicate, Consumer<JsonObject> itemConsumer) {
        for (int i = 0; i < array.length(); i++) {
            JsonValue jsonValue = array.get(i);
            if (jsonValue instanceof JsonObject) {
                JsonObject item = (JsonObject) jsonValue;
                if (jsonItemPredicate.test(item)) {
                    itemConsumer.accept(item);
                }
            }
        }
    }

    @NotNull
    private Stream<Pair<String, JsonBoolean>> mapHierarchicBinding(ClassMetadata<ELEMENT> finalClassMetadata) {
        if (dataSource.isHierarchical()) {
            return Stream.of(Pair.of(LEAF, Json.create(!dataSource.hasChildren(finalClassMetadata))));
        } else {
            return Stream.empty();
        }
    }

    @NotNull
    public Stream<Pair<String, JsonValue>> mapBingingToValue(ClassMetadata<ELEMENT> finalClassMetadata) {
        return dataSource.getBindings().stream()
            .filter(binding -> binding.startsWith(ITEM))
            .map(binding -> Pair.of(binding, getConvertedValueForPath(binding, finalClassMetadata)));
    }

    private JsonArray getJsonArray(@NotNull Collection<? extends ELEMENT> items,
                                   Function<ClassMetadata<ELEMENT>, JsonObject> createJsonObjectFromClassMetadata) {
        JsonArray jsonArray = Json.createArray();
        ClassMetadata<ELEMENT> classMetadata = null;
        int index = 0;
        Optional<Map<? extends ELEMENT, Integer>> elementIndexMap = dataSource.getIndexOfItems();
        for (ELEMENT item : nonNullItems(items)) {
            if (classMetadata == null) {
                @SuppressWarnings("unchecked")
                Class<ELEMENT> itemClass = (Class<ELEMENT>) item.getClass();
                classMetadata = ClassMetadata.ofClass(itemClass).withInstance(item);
            } else {
                classMetadata = classMetadata.withInstance(item);
            }
            final JsonObject jsonObject = createJsonObjectFromClassMetadata.apply(classMetadata);
            addKeyToJsonItem(jsonObject, classMetadata, elementIndexMap.map(map -> map.get(item)).orElse(null));
            jsonArray.set(index++, jsonObject);
        }
        return jsonArray;
    }

    @NotNull
    private List<? extends ELEMENT> nonNullItems(@NotNull Collection<? extends ELEMENT> items) {
        return items.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    private void addKeyToJsonItem(JsonObject jsonItem, ClassMetadata<ELEMENT> classMetadata, Integer itemIndex) {
        String key = keyMapper.getKey(classMetadata);
        jsonItem.put(ITEM_KEY, key);
        if (itemIndex != null) {
            jsonItem.put("___ITEM_IDX", itemIndex);
        }
        jsonItem.put(ITEM_ENDPOINT_ID, dataSource.getDataSourceId());
        inlineEditHandler.getItemState(key).applyOnJsonItem(jsonItem);
    }

    private void combineJsonObjects(JsonObject result, JsonObject otherObject) {
        Arrays.stream(otherObject.keys()).forEach(key -> {
            if ((otherObject.get(key) instanceof JsonObject) && result.hasKey(key)) {
                combineJsonObjects(result.getObject(key), otherObject.getObject(key));
            } else {
                result.put(key, (JsonValue) otherObject.get(key));
            }
        });
    }

    private void addPathValueToJsonObject(JsonObject root, Pair<String, ? extends JsonValue> pathValuePair) {
        JsonObject objectRef = root;
        String key = pathValuePair.getKey();
        if (Objects.equals(ITEM, key)) {
            key = PRIMITIVE_VALUE_KEY;
        } else {
            key = key.substring(ITEM_WITH_DOT.length());
        }
        String[] splitKey = key.split("\\.");
        for (int i = 0; i < splitKey.length; i++) {
            if (i != splitKey.length - 1) {
                if (objectRef.hasKey(splitKey[i])) {
                    objectRef = objectRef.getObject(splitKey[i]);
                } else {
                    JsonObject innerObject = Json.createObject();
                    objectRef.put(splitKey[i], innerObject);
                    objectRef = innerObject;
                }
            } else {
                objectRef.put(splitKey[i], pathValuePair.getValue());
            }
        }
    }

    private BindingMetadata getMetadataForBinding(String binding) {
        return metadataCache.computeIfAbsent(binding, metadataProvider::getMetadataForPath);
    }

    private JsonValue getConvertedValueForPath(String path, ClassMetadata<ELEMENT> classMetadata) {
        if (Objects.equals(ITEM, path)) {
            return Json.create(classMetadata.getInstance().toString());
        } else if (path.startsWith(ITEM_WITH_DOT)) {
            String reflectionPath = path.substring(ITEM_WITH_DOT.length());
            return Json.create(tryGetPropertyValue(classMetadata, reflectionPath)
                .map(value -> getMetadataForBinding(reflectionPath).getConverter().convertTo(value))
                .orElse(""));
        } else {
            throw new IllegalStateException();
        }
    }

    private Optional<Object> tryGetPropertyValue(ClassMetadata<ELEMENT> classMetadata, String reflectionPath) {
        try {
            return classMetadata.getPropertyValue(reflectionPath);
        } catch (InternalResolverException ignore) {
            return Optional.empty();
        }
    }

    public List<ELEMENT> getItems(JsonArray jsonItems) {
        return IntStream.range(0, jsonItems.length())
            .mapToObj(jsonItems::get)
            .filter(JsonObject.class::isInstance)
            .map(JsonObject.class::cast)
            .filter(jsonItem -> jsonItem.hasKey(ITEM_KEY))
            .map(jsonItem -> jsonItem.getString(ITEM_KEY))
            .map(keyMapper::getItem)
            .collect(Collectors.toList());
    }

    public JsonObject convertToKey(ELEMENT item) {
        return getJsonArray(Collections.singletonList(item), ignore -> Json.createObject()).get(0);
    }
}
