/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.api.datasource.DataSourceProvider;
import io.devbench.uibuilder.data.common.exceptions.DataSourceNotActiveException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface CommonDataSourceProvider<DATA_SOURCE extends CommonDataSource>
    extends DataSourceProvider<DATA_SOURCE, CommonDataSourceSelector> {

    @Override
    @SuppressWarnings("unchecked")
    default DATA_SOURCE getDataSource(@NotNull String dataSourceId, @NotNull CommonDataSourceSelector dataSourceSelector) {
        CommonDataSourceContext<DATA_SOURCE> commonDataSourceContext = (CommonDataSourceContext<DATA_SOURCE>) CommonDataSourceContext.getInstance();
        return commonDataSourceContext.findDataSource(
            dataSourceId,
            dataSourceSelector,
            () -> createNewDataSource(dataSourceId, dataSourceSelector.getDefaultQuery())
        );
    }

    DATA_SOURCE createNewDataSource(String dataSourceId, @Nullable String optionalDefaultQueryName);

    @Nullable
    default String getOptionalDefaultQueryName(@Nullable CommonDataSourceSelector dataSourceSelector) {
        return Optional
            .ofNullable(dataSourceSelector)
            .map(CommonDataSourceSelector::getDefaultQuery)
            .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    default void requestRefresh(String dataSourceName, @Nullable CommonDataSourceSelector dataSourceSelector) {
        CommonDataSourceContext<DATA_SOURCE> commonDataSourceContext = UIContext.getContext().get(CommonDataSourceContext.class);
        if (commonDataSourceContext != null) {
            commonDataSourceContext.refreshRequestedForDataSource(dataSourceName, dataSourceSelector);
        } else {
            throw new DataSourceNotActiveException(dataSourceName, dataSourceSelector);
        }
    }

    default String createBindingsKey(String dataSourceName, @Nullable String optionalDefaultQueryName) {
        return dataSourceName + (optionalDefaultQueryName != null ? optionalDefaultQueryName : "");
    }
}
