/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;

@Data
public class DataProviderRequest implements EndpointIdAware, Serializable {

    public enum RequestType {
        @SerializedName("fetchData")
        FETCH_DATA,
        @SerializedName("fetchSize")
        FETCH_SIZE
    }

    private String endpointId;

    @SerializedName("event")
    private RequestType requestType;

    private String csrfId;

    @SerializedName("detail")
    private DataRequestBody body;

}
