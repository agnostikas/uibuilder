/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.parse.annotation;

import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;

import java.util.Collection;
import java.util.stream.Collectors;

public class IllegalIdPropertyPathRecursionException extends RuntimeException {
    public IllegalIdPropertyPathRecursionException(Collection<PropertyMetadata<?>> propertyMetadataList) {
        super("The @Id definition of the following classes contains a loop: " + formatPropertyMetaDataList(propertyMetadataList));
    }

    private static Object formatPropertyMetaDataList(Collection<PropertyMetadata<?>> propertyMetadataList) {
        return propertyMetadataList
            .stream()
            .map(PropertyMetadata::getContainerClass)
            .map(Class::getName)
            .collect(Collectors.joining(", "));
    }
}
