/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.comperingfilters;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;

/**
 * Just a convenience class which collects the common Expression types
 */
public final class ExpressionTypes {

    public abstract static class Equals<PREDICATE> extends
        BinaryOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return Equals.class;
        }
    }

    public abstract static class NotEquals<PREDICATE> extends
        BinaryOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return NotEquals.class;
        }
    }

    public abstract static class LessThan<PREDICATE> extends
        BinaryOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return LessThan.class;
        }
    }

    public abstract static class LessThanOrEquals<PREDICATE> extends
        BinaryOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return LessThanOrEquals.class;
        }
    }

    public abstract static class GreaterThan<PREDICATE> extends
        BinaryOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return GreaterThan.class;
        }
    }

    public abstract static class GreaterThanOrEquals<PREDICATE> extends
        BinaryOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return GreaterThanOrEquals.class;
        }
    }

    public abstract static class BaseLike<PREDICATE> extends BinaryOperandFilterExpression<PREDICATE> {

        @Getter
        @Setter
        @NotNull
        private LikeExpressionType likeExpressionType = LikeExpressionType.NONE;

        public enum LikeExpressionType {
            STARTS_WITH("starts-with"),
            CONTAINS("contains"),
            ENDS_WITH("ends-with"),
            NONE("none");

            private final String mode;

            LikeExpressionType(String mode) {
                this.mode = mode;
            }

            public String getMode() {
                return mode;
            }

            public static LikeExpressionType fromMode(String mode) {
                return Arrays.stream(LikeExpressionType.values())
                    .filter(likeExpressionType -> likeExpressionType.mode.equals(mode))
                    .findFirst()
                    .orElse(null);
            }
        }

        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return BaseLike.class;
        }

        protected String getExpressionValue() {
            switch (getLikeExpressionType()) {
                case STARTS_WITH:
                    return getValue() + "%";
                case ENDS_WITH:
                    return "%" + getValue();
                case NONE:
                    return String.valueOf(getValue());
                default:
                    return "%" + getValue() + "%";
            }
        }
    }

    public abstract static class Like<PREDICATE> extends BaseLike<PREDICATE> {

        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return Like.class;
        }

    }

    public abstract static class IgnoreCaseLike<PREDICATE> extends BaseLike<PREDICATE> {

        @Override
        protected Class<? extends BinaryOperandFilterExpression> getOperatorDescribingClass() {
            return IgnoreCaseLike.class;
        }

    }

    public abstract static class Between<PREDICATE> extends AnyOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends AnyOperandFilterExpression> getOperatorDescribingClass() {
            return Between.class;
        }
    }

    public abstract static class In<PREDICATE> extends AnyOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends AnyOperandFilterExpression> getOperatorDescribingClass() {
            return In.class;
        }
    }

    public abstract static class NotIn<PREDICATE> extends AnyOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends AnyOperandFilterExpression> getOperatorDescribingClass() {
            return NotIn.class;
        }
    }

    public abstract static class IsNull<PREDICATE> extends EmptyOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends EmptyOperandFilterExpression> getOperatorDescribingClass() {
            return IsNull.class;
        }
    }

    public abstract static class IsNotNull<PREDICATE> extends EmptyOperandFilterExpression<PREDICATE> {
        @Override
        protected Class<? extends EmptyOperandFilterExpression> getOperatorDescribingClass() {
            return IsNotNull.class;
        }
    }

}
