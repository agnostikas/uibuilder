/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

/**
 * Components supporting data-source sharing with same type should implement this interface.
 *
 * Data-sources with same {@code datasource-id} are implicitly sharable between different
 * types of components, for example a data-source can be shared between a filter and a grid
 * component by default, but two grid instance cannot share the same data-source, even with
 * the same {@code datasource-id}. This is because the grid component can manipulate the
 * order, filtering, and other aspects of the data-source, which may cause problems if shared
 * between instances.
 * However, there can be components that only visualizing data and require the option, to
 * share data-source instances with the same type. Those components can implement this
 * interface providing a {@code contextId} to separate instances with the same type.
 */
public interface SupportsDataSourceReuse {

    /**
     * ContextId to separate data-souce context holding components with the same type.
     *
     * @return contextId string unique to the context holding component instance.
     * (eg.: the component's id property, or other unique string identifying the component
     * instance)
     */
    String getContextId();

}
