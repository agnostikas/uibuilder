/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.operator;

import io.devbench.uibuilder.data.common.filter.comperingfilters.ComperingFilterExpression;
import io.devbench.uibuilder.i18n.core.I;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import static io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes.*;
import static io.devbench.uibuilder.data.common.filter.operator.OperatorValueType.*;

public enum OperatorType {

    LIKE("like", () -> I.tr("like"), Like.class, SINGLE),
    I_LIKE("ilike", () -> I.tr("ilike"), IgnoreCaseLike.class, SINGLE),
    EQUALS("=", Equals.class, SINGLE),
    NOT_EQUALS("!=", NotEquals.class, SINGLE),
    GT(">", GreaterThan.class, SINGLE),
    GTE(">=", GreaterThanOrEquals.class, SINGLE),
    LT("<", LessThan.class, SINGLE),
    LTE("<=", LessThanOrEquals.class, SINGLE),
    BETWEEN("between", () -> I.tr("between"), Between.class, TWO),
    IN("in", () -> I.tr("in"), In.class, ANY),
    NOT_IN("not in", () -> I.tr("not in"), NotIn.class, ANY),
    IS_NULL("is null", () -> I.tr("is null"), IsNull.class, NONE),
    IS_NOT_NULL("is not null", () -> I.tr("is not null"), IsNotNull.class, NONE);

    private static final Map<String, OperatorType> NAME_MAP = new HashMap<>();

    @Getter
    private String name;

    private Supplier<String> displayNameSupplier;

    @Getter
    private Class<? extends ComperingFilterExpression> expressionType;

    @Getter
    private OperatorValueType valueType;

    OperatorType(@NotNull String name, Class<? extends ComperingFilterExpression> expressionType, OperatorValueType valueType) {
        this(name, () -> name, expressionType, valueType);
    }

    OperatorType(@NotNull String name, Supplier<String> displayNameSupplier,
                 Class<? extends ComperingFilterExpression> expressionType, OperatorValueType valueType) {
        this.name = name;
        this.displayNameSupplier = displayNameSupplier;
        this.expressionType = expressionType;
        this.valueType = valueType;
    }

    public static OperatorType fromName(String name) {
        return NAME_MAP.computeIfAbsent(name, n -> Arrays
            .stream(OperatorType.values())
            .filter(value -> value.name.equals(name))
            .findFirst().orElse(null));
    }

    @Override
    public String toString() {
        return name;
    }

    public String getDisplayName() {
        return displayNameSupplier.get();
    }
}
