/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.item;

import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.data.api.item.ItemBeanValidator;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor
public class ItemState implements Serializable {

    public static final String STATE_PROPERTY = "_state";
    private boolean editMode = false;
    private boolean saveAllowed = true;
    private boolean editAllowed = true;
    private Map<String, String> propertyErrors = new HashMap<>();

    public static ItemState withEditMode(boolean editMode) {
        ItemState state = new ItemState();
        state.setEditMode(editMode);
        return state;
    }

    public static ItemState fromJsonItem(@NotNull JsonObject jsonItem) {
        ItemState itemState = new ItemState();
        if (jsonItem.hasKey(STATE_PROPERTY)) {
            JsonObject stateObject = jsonItem.getObject(STATE_PROPERTY);
            itemState.setEditMode(stateObject.getBoolean("editMode"));
            itemState.setEditAllowed(stateObject.getBoolean("editAllowed"));
            itemState.setSaveAllowed(stateObject.getBoolean("saveAllowed"));
            if (stateObject.hasKey("errors")) {
                JsonObject errors = stateObject.getObject("errors");
                for (String propertyPath : errors.keys()) {
                    itemState.getPropertyErrors().put(propertyPath, errors.getString(propertyPath));
                }
            }
        }
        return itemState;
    }

    public JsonObject toJson() {
        JsonObject stateObject = Json.createObject();
        stateObject.put("editMode", editMode);
        stateObject.put("saveAllowed", saveAllowed);
        stateObject.put("editAllowed", editAllowed);
        if (!propertyErrors.isEmpty()) {
            JsonObject errors = Json.createObject();
            propertyErrors.forEach(errors::put);
            stateObject.put("errors", errors);
        }
        return stateObject;
    }

    public <T extends Serializable> ItemState validate(@NotNull Set<String> propertyPaths, @NotNull T instance) {
        propertyErrors.clear();
        ItemBeanValidator.getInstance().validate(propertyPaths, instance).forEach(propertyErrors::put);
        return this;
    }

    public void applyOnJsonItem(@NotNull JsonObject jsonItem) {
        jsonItem.put(STATE_PROPERTY, toJson());
    }

    public boolean isPropertyValid(@NotNull String propertyPath) {
        return propertyErrors.get(propertyPath) == null;
    }

    public String getPropertyErrorMessage(@NotNull String propertyPath) {
        return propertyErrors.get(propertyPath);
    }

    public ItemState clearPropertyErrorMessage(@NotNull String propertyPath) {
        propertyErrors.remove(propertyPath);
        return this;
    }

    public ItemState addPropertyError(@NotNull String propertyPath, @NotNull String errorMessage) {
        propertyErrors.put(propertyPath, errorMessage);
        return this;
    }

    public ItemState setEditMode(boolean editMode) {
        this.editMode = editMode;
        return this;
    }

    public ItemState setSaveAllowed(boolean saveAllowed) {
        this.saveAllowed = saveAllowed;
        return this;
    }

    public ItemState setEditAllowed(boolean editAllowed) {
        this.editAllowed = editAllowed;
        return this;
    }

    public boolean isValid() {
        return propertyErrors.isEmpty();
    }

}
