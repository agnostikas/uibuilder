/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit;

import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import io.devbench.uibuilder.data.common.exceptions.InlineApplyChangesPropertyNotFoundException;
import io.devbench.uibuilder.data.common.item.ItemState;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public interface InlineEditHandler {

    static boolean isNotAnInternalProperty(String key) {
        return !key.startsWith("_");
    }

    @NotNull Boolean getEditModeDefault();

    void setEditModeDefault(boolean editModeDefault);

    void clearEditModes();

    void setEditMode(@NotNull String key, boolean editMode);

    default void setEditMode(@NotNull JsonObject item, boolean editMode) {
        setEditMode(getItemKey(item), editMode);
    }

    void clearEditMode(@NotNull String key);

    default void clearEditMode(@NotNull JsonObject item) {
        clearEditMode(getItemKey(item));
    }

    boolean isEditMode(@NotNull String key);

    default boolean isEditMode(@NotNull JsonObject item) {
        return isEditMode(getItemKey(item));
    }

    void registerPropertyChange(@NotNull JsonObject item, @NotNull String propertyPath, @Nullable JsonValue propertyValue);

    default void registerPropertyChange(@NotNull JsonObject item) {
        Arrays.stream(item.keys())
            .filter(InlineEditHandler::isNotAnInternalProperty)
            .forEach(key -> registerPropertyChange(item, key, item.get(key)));
    }

    boolean hasChangedPropertyValue(@NotNull JsonObject item, @NotNull String propertyPath);

    JsonValue getChangedPropertyValue(@NotNull JsonObject item, @NotNull String propertyPath);

    void removePropertyChanges(@NotNull JsonObject item);

    void removePropertyChanges(@NotNull String itemKey);

    void removeChangedProperty(@NotNull JsonObject item, @NotNull String propertyPath);

    void forChangedProperty(@NotNull JsonObject item, @NotNull BiConsumer<String, JsonValue> consumer);

    Stream<JsonObject> editedItems();

    String getItemKey(@NotNull JsonObject item);

    default <T> void applyChanges(@NotNull T item, @NotNull JsonObject jsonItem) {
        ClassMetadata<T> itemMetadata = ClassMetadata.ofValue(item);
        forChangedProperty(jsonItem, (key, jsonValue) -> {
            PropertyMetadata<?> keyPropertyMetadata = itemMetadata.property(key)
                .orElseThrow(() -> new InlineApplyChangesPropertyNotFoundException("Could not find property: " + key + ", in class: " + item.getClass()));
            @SuppressWarnings("unchecked")
            PropertyConverter<?, String> propertyConverter = (PropertyConverter<?, String>) PropertyConverters.getConverterFor(keyPropertyMetadata);
            keyPropertyMetadata.setValue(propertyConverter.convertFrom(jsonValue.asString()));
        });
    }

    ItemState getItemState(@NotNull String key);

    default ItemState getItemState(@NotNull JsonObject item) {
        return getItemState(getItemKey(item));
    }

    void setItemState(@NotNull String key, @Nullable ItemState itemState);

    default void setItemState(@NotNull JsonObject item, @Nullable ItemState itemState) {
        setItemState(getItemKey(item), itemState);
    }

    default void removeItemState(@NotNull JsonObject item) {
        setItemState(item, null);
    }

    default void removeItemState(@NotNull String key) {
        setItemState(key, null);
    }

    Set<String> getChangedProperties(@NotNull JsonObject jsonObject);
}
