/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.logicaloperators;

import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionDescriptor;
import io.devbench.uibuilder.data.common.filter.filterdescriptors.LogicalOperatorFilterDescriptor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AndFilterExpression<EXPRESSION_REALIZED extends FilterExpression<?>, PREDICATE> implements FilterExpression<PREDICATE> {

    @Getter
    private final List<EXPRESSION_REALIZED> expressions;

    public AndFilterExpression() {
        expressions = new ArrayList<>();
    }

    public void add(EXPRESSION_REALIZED expression) {
        expressions.add(expression);
    }

    @Override
    @SuppressWarnings("unchecked")
    public LogicalOperatorFilterDescriptor getImmutableDescriptor() {
        List<? extends FilterExpressionDescriptor> children = expressions.stream()
            .map(FilterExpression.class::cast)
            .<FilterExpressionDescriptor>map(FilterExpression::getImmutableDescriptor)
            .collect(Collectors.toList());
        return new LogicalOperatorFilterDescriptor(children);
    }
}
