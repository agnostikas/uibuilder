/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.component;

import com.vaadin.flow.component.Component;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.api.crud.GenericGridInlineEditorControllerBean;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProcessor;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderEndpointManager;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.InlineEditHandler;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.exceptions.DataSourceNotFoundByEndpointException;
import io.devbench.uibuilder.data.common.exceptions.InlineSaveFailedException;
import io.devbench.uibuilder.data.common.item.ItemState;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Optional;

@Slf4j
public abstract class AbstractGenericGridInlineEditorControllerBean<T> implements GenericGridInlineEditorControllerBean<T> {

    @UIEventHandler("inlineItemSave")
    public void onInlineItemSave(@CallOnNonNull @Item T item, @CallOnNonNull @Value(DataProcessor.ITEM) JsonObject jsonItem) {
        getInlineEditHandler(jsonItem).applyChanges(item, jsonItem);
        try {
            onInlineItemSave(item, jsonItem, findMemberComponent(jsonItem).orElse(null));
            setEditMode(jsonItem, false);
        } catch (Exception e) {
            throw new InlineSaveFailedException(e);
        }
    }

    public abstract void onInlineItemSave(@NotNull T modifiedItem, @NotNull JsonObject modifiedJsonItem, @Nullable Component component);

    @UIEventHandler(value = "inlineItemValueChange", debounce = 800)
    public void onInlineItemValueChange(@CallOnNonNull @Item T item, @CallOnNonNull @Value(DataProcessor.ITEM) JsonObject jsonItem) {
        InlineEditHandler inlineEditHandler = getInlineEditHandler(jsonItem);
        inlineEditHandler.registerPropertyChange(jsonItem);

        Serializable clonedItem = SerializationUtils.clone((Serializable) item);
        inlineEditHandler.applyChanges(clonedItem, jsonItem);

        ItemState itemState = inlineEditHandler.getItemState(jsonItem);
        itemState
            .validate(inlineEditHandler.getChangedProperties(jsonItem), clonedItem)
            .setSaveAllowed(itemState.isValid());

        refreshItem(jsonItem);
    }

    @Override
    public void setEditMode(@NotNull JsonObject jsonItem, boolean editMode) {
        getInlineEditHandler(jsonItem).setEditMode(jsonItem, editMode);
        refreshItem(jsonItem);
    }

    protected void refreshItem(@NotNull JsonObject jsonItem) {
        findMemberComponent(jsonItem)
            .map(Component::getElement)
            .ifPresent(element -> element.callJsFunction("_refreshItem", jsonItem));
    }

    protected InlineEditHandler getInlineEditHandler(@NotNull JsonObject jsonItem) {
        return DataProviderEndpointManager.getInstance()
            .getDataSourceByItemKey(jsonItem)
            .map(CommonDataSource::getDataProcessor)
            .orElseThrow(DataSourceNotFoundByEndpointException::new)
            .getInlineEditHandler();
    }

    protected Optional<Component> findMemberComponent(@NotNull JsonObject jsonItem) {
        return DataProviderEndpointManager.getInstance()
            .getComponent(jsonItem.getString(DataProcessor.ITEM_ENDPOINT_ID))
            .filter(Component.class::isInstance)
            .map(Component.class::cast);
    }
}
