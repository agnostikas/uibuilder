/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit;

import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProcessor;
import io.devbench.uibuilder.data.common.item.ItemState;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class SimpleInlineEditHandler implements InlineEditHandler {

    private final Map<String, JsonObject> itemMap;
    private final Map<String, ItemState> itemStateMap;
    private final Map<String, Map<String, JsonValue>> changedPropertyValues;
    private boolean editModeDefault;

    public SimpleInlineEditHandler() {
        editModeDefault = false;
        changedPropertyValues = new HashMap<>();
        itemStateMap = new HashMap<>();
        itemMap = new HashMap<>();
    }

    @NotNull
    @Override
    public Boolean getEditModeDefault() {
        return editModeDefault;
    }

    @Override
    public void setEditModeDefault(boolean editModeDefault) {
        this.editModeDefault = editModeDefault;
    }

    @Override
    public void clearEditModes() {
        itemStateMap.clear();
        changedPropertyValues.clear();
    }

    @Override
    public void setEditMode(@NotNull String key, boolean editMode) {
        getItemState(key).setEditMode(editMode);
        if (!editMode) {
            removePropertyChanges(key);
        }
    }

    @Override
    public void clearEditMode(@NotNull String key) {
        itemStateMap.remove(key);
        removePropertyChanges(key);
    }

    @Override
    public boolean isEditMode(@NotNull String key) {
        return getItemState(key).isEditMode();
    }

    @Override
    public void registerPropertyChange(@Nonnull JsonObject item, @Nonnull String propertyPath, JsonValue propertyValue) {
        getPropertyMap(item).put(propertyPath, propertyValue);
    }

    @Override
    public boolean hasChangedPropertyValue(@Nonnull JsonObject item, @Nonnull String propertyPath) {
        return getPropertyMap(item).containsKey(propertyPath);
    }

    @Override
    public JsonValue getChangedPropertyValue(@Nonnull JsonObject item, @Nonnull String propertyPath) {
        return getPropertyMap(item).get(propertyPath);
    }

    @Override
    public void removePropertyChanges(@NotNull JsonObject item) {
        changedPropertyValues.remove(getItemKey(item));
    }

    @Override
    public void removePropertyChanges(@NotNull String itemKey) {
        changedPropertyValues.remove(itemKey);
    }

    @Override
    public void removeChangedProperty(@NotNull JsonObject item, @NotNull String propertyPath) {
        getPropertyMap(item).remove(propertyPath);
    }

    @Override
    public void forChangedProperty(@NotNull JsonObject item, @NotNull BiConsumer<String, JsonValue> consumer) {
        getPropertyMap(item).forEach(consumer);
    }

    @Override
    public Stream<JsonObject> editedItems() {
        return new HashSet<>(changedPropertyValues.keySet())
            .stream()
            .map(itemMap::get)
            .filter(Objects::nonNull);
    }

    @Override
    public String getItemKey(@NotNull JsonObject item) {
        String itemKey = DataProcessor.getItemKey(item);
        itemMap.put(itemKey, item);
        return itemKey;
    }

    @Override
    public ItemState getItemState(@NotNull String key) {
        return itemStateMap.computeIfAbsent(key, keyWithoutState -> ItemState.withEditMode(editModeDefault));
    }

    /**
     * To remove an item state for the specified key, then provide null as the itemState
     *
     * @param key       the key for which the item state should be set (or removed if itemState is null)
     * @param itemState which will be set for the provided key
     */
    @Override
    public void setItemState(@NotNull String key, @Nullable ItemState itemState) {
        if (itemState == null) {
            itemStateMap.remove(key);
        } else {
            itemStateMap.put(key, itemState);
        }
    }

    @Override
    public Set<String> getChangedProperties(@NotNull JsonObject jsonObject) {
        return getPropertyMap(jsonObject).keySet();
    }

    private Map<String, JsonValue> getPropertyMap(@NotNull JsonObject item) {
        return changedPropertyValues.computeIfAbsent(getItemKey(item), itemWithoutMap -> new HashMap<>());
    }

}
