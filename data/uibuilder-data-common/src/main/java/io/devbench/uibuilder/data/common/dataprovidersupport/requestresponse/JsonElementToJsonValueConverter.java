/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse;

import com.google.gson.*;
import elemental.json.JsonValue;
import elemental.json.impl.JreJsonFactory;

import java.lang.reflect.Type;

public class JsonElementToJsonValueConverter implements JsonDeserializer<JsonValue>, JsonSerializer<JsonValue> {

    @Override
    public JsonValue deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new JreJsonFactory().parse(json.toString());
    }

    @Override
    public JsonElement serialize(JsonValue src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonParser().parse(src.toJson());
    }
}
