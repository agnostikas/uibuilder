/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.parse.annotation;

import org.junit.jupiter.api.Test;

import javax.persistence.Id;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class IdAnnotationParserTest {

    @Test
    public void shouldFindKeyOnSimpleTestClass() {
        final Set<String> filedNames = IdAnnotationParser.getIdPropertyPathsFromClass(SimpleTestClass.class);
        assertFalse(filedNames.isEmpty());
        assertEquals(1, filedNames.size());
        assertEquals("id", filedNames.iterator().next());
    }

    @Test
    public void shouldFindKeyOnSimpleTestClassWithComplexKey() {
        final Set<String> filedNames = IdAnnotationParser.getIdPropertyPathsFromClass(SimpleTestClassWithComplexKey.class);
        assertFalse(filedNames.isEmpty());
        assertEquals(2, filedNames.size());
        assertTrue(filedNames.contains("complexKey.id"));
        assertTrue(filedNames.contains("complexKey.id2"));
    }

    @Test
    public void shouldFindKeyOnSimpleTestClassWithComplexKeySecond() {
        final Set<String> filedNames = IdAnnotationParser.getIdPropertyPathsFromClass(SimpleTestClassWithComplexKeySecond.class);
        assertFalse(filedNames.isEmpty());
        assertEquals(6, filedNames.size());
        assertTrue(filedNames.contains("id"));
        assertTrue(filedNames.contains("complexKeySecond.id"));
        assertTrue(filedNames.contains("complexKeySecond.complexKey.id"));
        assertTrue(filedNames.contains("complexKeySecond.complexKey.id2"));
        assertTrue(filedNames.contains("complexKey.id"));
        assertTrue(filedNames.contains("complexKey.id2"));
    }

    @Test
    public void shouldThrowExceptionWhenTheClassesAreWronglyAnnotated() {
        assertThrows(IllegalIdPropertyPathRecursionException.class, () -> IdAnnotationParser.getIdPropertyPathsFromClass(WronglyAnnotatedClass0.class));
        assertThrows(IllegalIdPropertyPathRecursionException.class, () -> IdAnnotationParser.getIdPropertyPathsFromClass(WronglyAnnotatedClass1.class));
        assertThrows(IllegalIdPropertyPathRecursionException.class, () -> IdAnnotationParser.getIdPropertyPathsFromClass(WronglyAnnotatedClass2.class));
        assertThrows(IllegalIdPropertyPathRecursionException.class, () -> IdAnnotationParser.getIdPropertyPathsFromClass(WronglyAnnotatedClass3.class));
        assertThrows(IllegalIdPropertyPathRecursionException.class, () -> IdAnnotationParser.getIdPropertyPathsFromClass(WronglyAnnotatedClass4.class));
    }

    @Test
    public void shouldFindKeyOnSimpleTestClassWithInheritance() {
        class BaseClass {
            @Id
            private String id;
        }

        class FooBarClass extends BaseClass {

        }


        final Set<String> filedNames = IdAnnotationParser.getIdPropertyPathsFromClass(FooBarClass.class);
        assertFalse(filedNames.isEmpty());
        assertEquals(1, filedNames.size());
        assertEquals("id", filedNames.iterator().next());
    }

    @Test
    public void shouldFindKeyOnSimpleTestClassWhenThePropertiesAreGetterBased() {
        class FooBarClass {

            @Id
            public String getId() {
                return null;
            }
        }


        final Set<String> filedNames = IdAnnotationParser.getIdPropertyPathsFromClass(FooBarClass.class);
        assertFalse(filedNames.isEmpty());
        assertEquals(1, filedNames.size());
        assertEquals("id", filedNames.iterator().next());
    }

    @Test
    public void should_throw_exception_when_theres_no_field_annotated_with_id_annotation() {
        class FooBarClass {
            private String id;
        }


        assertThrows(EntityIdNotFoundException.class, () -> IdAnnotationParser.getIdPropertyPathsFromClass(FooBarClass.class));
    }


    class SimpleTestClass {
        @Id
        private Long id;
    }

    class ComplexKey {
        @Id
        private Long id;

        @Id
        private Long id2;
    }

    class ComplexKeySecond {
        @Id
        private ComplexKey complexKey;

        @Id
        private Long id;
    }

    class SimpleTestClassWithComplexKeySecond {
        @Id
        private ComplexKeySecond complexKeySecond;
        @Id
        private ComplexKey complexKey;
        @Id
        private Long id;
    }

    class SimpleTestClassWithComplexKey {
        @Id
        private ComplexKey complexKey;
    }

    class WronglyAnnotatedClass0 {
        @Id
        private WronglyAnnotatedClass1 wronglyAnnotatedClass1;
    }

    class WronglyAnnotatedClass1 {
        @Id
        private WronglyAnnotatedClass0 wronglyAnnotatedClass1;
    }

    class WronglyAnnotatedClass2 {
        @Id
        private WronglyAnnotatedClass0 wronglyAnnotatedClass1;
        @Id
        private Long id;
    }

    class WronglyAnnotatedClass3 {
        @Id
        private WronglyAnnotatedClass0 wronglyAnnotatedClass1;
        @Id
        private ComplexKey id;
    }

    class WronglyAnnotatedClass4 {
        @Id
        private WronglyAnnotatedClass0 wronglyAnnotatedClass1;
        @Id
        private ComplexKeySecond complexKeySecond;
        @Id
        private ComplexKey complexKey;
        @Id
        private Long id;
    }
}
