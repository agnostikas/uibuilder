/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.item;

import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ItemStateTest {

    private ItemState testObj;

    @Test
    @DisplayName("Should be created with edit mode set")
    void test_should_be_created_with_edit_mode_set() {
        ItemState itemStateWithEditMode = ItemState.withEditMode(true);
        ItemState itemStateWithoutEditMode = ItemState.withEditMode(false);

        assertNotNull(itemStateWithEditMode);
        assertNotNull(itemStateWithoutEditMode);
        assertTrue(itemStateWithEditMode.isEditMode());
        assertFalse(itemStateWithoutEditMode.isEditMode());
    }

    @Test
    @DisplayName("Should be create by json object")
    void test_should_be_create_by_json_object() {
        JsonObject stateObject = Json.createObject();
        stateObject.put("editMode", true);
        stateObject.put("editAllowed", true);
        stateObject.put("saveAllowed", true);
        JsonObject errors = Json.createObject();
        errors.put("name", "Name error message");
        errors.put("age", "Age error message");
        stateObject.put("errors", errors);
        JsonObject jsonItem = Json.createObject();
        jsonItem.put("_state", stateObject);

        ItemState itemState = ItemState.fromJsonItem(jsonItem);

        assertNotNull(itemState);

        assertTrue(itemState.isEditMode());
        assertTrue(itemState.isSaveAllowed());
        assertFalse(itemState.isValid());
        assertEquals("Name error message", itemState.getPropertyErrorMessage("name"));
        Map<String, String> propertyErrors = itemState.getPropertyErrors();
        assertNotNull(propertyErrors);
        assertEquals(2, propertyErrors.size());
        assertEquals("Name error message", propertyErrors.get("name"));
        assertEquals("Age error message", propertyErrors.get("age"));
    }

    @Test
    @DisplayName("Should create json object from state")
    void test_should_create_json_object_from_state() {
        ItemState itemState = new ItemState();
        itemState.setEditMode(true);
        itemState.setSaveAllowed(true);
        itemState.addPropertyError("name", "Name error");
        itemState.addPropertyError("other", "Other error");

        JsonObject jsonStateObject = itemState.toJson();

        assertNotNull(jsonStateObject);
        assertTrue(jsonStateObject.getBoolean("editMode"));
        assertTrue(jsonStateObject.getBoolean("saveAllowed"));
        assertTrue(jsonStateObject.getBoolean("editAllowed"));
        assertTrue(jsonStateObject.hasKey("errors"));
        JsonObject errors = jsonStateObject.getObject("errors");
        assertNotNull(errors);
        assertEquals(2, errors.keys().length);
        assertEquals("Name error", errors.getString("name"));
        assertEquals("Other error", errors.getString("other"));
    }

    @Test
    @DisplayName("Should apply state on json item")
    void test_should_apply_state_on_json_item() {
        JsonObject stateObject = Json.createObject();
        stateObject.put("editMode", false);
        stateObject.put("saveAllowed", false);
        stateObject.put("editAllowed", true);
        JsonObject jsonItem = Json.createObject();
        jsonItem.put("_state", stateObject);

        ItemState itemState = new ItemState();
        itemState.setEditMode(true);
        itemState.setSaveAllowed(true);
        itemState.addPropertyError("name", "Name error");
        itemState.addPropertyError("other", "Other error");

        itemState.applyOnJsonItem(jsonItem);

        JsonObject jsonStateObject = jsonItem.getObject("_state");

        assertNotNull(jsonStateObject);
        assertTrue(jsonStateObject.getBoolean("editMode"));
        assertTrue(jsonStateObject.getBoolean("saveAllowed"));
        assertTrue(jsonStateObject.hasKey("errors"));
        JsonObject errors = jsonStateObject.getObject("errors");
        assertNotNull(errors);
        assertEquals(2, errors.keys().length);
        assertEquals("Name error", errors.getString("name"));
        assertEquals("Other error", errors.getString("other"));
    }

    @Test
    @DisplayName("Property validity should be able to test")
    void test_property_validity_should_be_able_to_test() {
        ItemState itemState = new ItemState();
        itemState.addPropertyError("name", "Name error");

        assertFalse(itemState.isValid());
        assertFalse(itemState.isPropertyValid("name"));
        assertTrue(itemState.isPropertyValid("other"));
        assertTrue(itemState.isPropertyValid("another"));

        itemState.clearPropertyErrorMessage("name");

        assertTrue(itemState.isValid());
        assertTrue(itemState.isPropertyValid("name"));
    }

}
