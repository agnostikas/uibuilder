/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.core.controllerbean.uiproperty.StringPropertyConverter;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.datasource.FetchRequest;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.api.order.OrderExpression;
import io.devbench.uibuilder.data.api.order.OrderExpressionFactory;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProcessor;
import io.devbench.uibuilder.data.common.dataprovidersupport.OrmKeyMapper;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataFilter;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataProviderRequest;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataRequestBody;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataResponse;
import io.devbench.uibuilder.data.common.filter.comperingfilters.BinaryOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommonDataSourceTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private DataProcessor<Object> dataProcessor;

    @Mock
    private FilterExpressionFactory expressionFactory;

    @Mock
    private OrderExpressionFactory<OrderExpression> orderExpressionFactory;

    @Mock
    private BindingMetadataProvider metadataProvider;

    private CommonDataSource<Object, OrderExpression, FilterExpression<?>, ?> testObj;

    @BeforeEach
    private void setup() {
        testObj = spy(new TestCommonDataSource());
    }

    @Test
    @DisplayName("Should map page info from data provider request to fetch request and call the generic fetch data")
    public void should_map_page_info_from_data_provider_request_to_fetch_request_and_call_the_generic_fetch_data() {
        DataResponse mockResponse = mockDataProcessorConvert();

        DataResponse dataResponse = testObj.fetchData(buildDataProviderRequestWithPageInfo());

        ArgumentCaptor<PagingFetchRequest> requestCaptor = ArgumentCaptor.forClass(PagingFetchRequest.class);
        verify(testObj).fetchData(requestCaptor.capture());
        FetchRequest request = requestCaptor.getValue();

        assertAll(
            () -> assertTrue(request instanceof PagingFetchRequest),
            () -> assertEquals((Integer) 2, ((PagingFetchRequest) request).getPage()),
            () -> assertEquals((Integer) 100, ((PagingFetchRequest) request).getPageSize()),
            () -> assertNotNull(dataResponse),
            () -> assertSame(mockResponse, dataResponse)
        );
    }


    @Test
    @DisplayName("Should map data provider filter information to filter expressions before fetching the data")
    public void should_map_data_provider_filter_information_to_filter_expressions_before_fetching_the_data() {
        DataResponse mockResponse = mockDataProcessorConvert();
        prepareMetadataForFilter();
        prepareExpressionFactoryForFilter();

        DataResponse dataResponse = testObj.fetchData(buildDataProviderRequestWithFilterInfo());

        ArgumentCaptor<FilterExpression> filterCaptor = ArgumentCaptor.forClass(FilterExpression.class);
        verify(testObj).registerFilter(filterCaptor.capture());
        FilterExpression filterExpression = filterCaptor.getValue();

        assertAll(
            () -> assertNotNull(filterExpression),
            () -> assertTrue(filterExpression instanceof AndFilterExpression),
            () -> verify(metadataProvider, times(2)).getMetadataForPath(matches("test\\.path(1|2)")),
            () -> assertEquals(2, ((AndFilterExpression) filterExpression).getExpressions().size()),
            () -> assertTrue(((AndFilterExpression) filterExpression).getExpressions().get(0) instanceof ExpressionTypes.Equals),
            () -> assertTrue(((AndFilterExpression) filterExpression).getExpressions().get(1) instanceof ExpressionTypes.Like),
            () -> assertEquals("test.path1", ((ExpressionTypes.Equals) ((AndFilterExpression) filterExpression).getExpressions().get(0)).getPath()),
            () -> assertEquals("test.path2", ((ExpressionTypes.Like) ((AndFilterExpression) filterExpression).getExpressions().get(1)).getPath()),
            () -> assertEquals(new BigDecimal("1"), ((ExpressionTypes.Equals) ((AndFilterExpression) filterExpression).getExpressions().get(0)).getValue()),
            () -> assertEquals("testValue2", ((ExpressionTypes.Like) ((AndFilterExpression) filterExpression).getExpressions().get(1)).getValue()),
            () -> assertSame(mockResponse, dataResponse)
        );
    }

    @Test
    @SuppressWarnings({"rawtypes", "unchecked"})
    @DisplayName("Should create proper expression according to the default filter descriptor")
    void test_should_create_proper_expression_according_to_the_default_filter_descriptor() {
        DefaultFilterDescriptor defaultFilterDescriptor = ((BaseDataSourceBindingContext) testObj.getBindingContext()).getDefaultFilterDescriptor();
        defaultFilterDescriptor.addFilterType("deadline", ">=", null);
        defaultFilterDescriptor.addFilterType("other", "invalid", null);

        ExpressionTypes.Like likeExpression = mock(ExpressionTypes.Like.class);
        ExpressionTypes.IgnoreCaseLike ignoreCaseLikeExpression = mock(ExpressionTypes.IgnoreCaseLike.class);
        ExpressionTypes.GreaterThanOrEquals greaterThanOrEqualsExpression = mock(ExpressionTypes.GreaterThanOrEquals.class);
        ExpressionTypes.Equals equalsExpression = mock(ExpressionTypes.Equals.class);

        when(expressionFactory.create(ExpressionTypes.Like.class)).thenReturn(likeExpression);
        when(expressionFactory.create(ExpressionTypes.IgnoreCaseLike.class)).thenReturn(ignoreCaseLikeExpression);
        when(expressionFactory.create(ExpressionTypes.GreaterThanOrEquals.class)).thenReturn(greaterThanOrEqualsExpression);
        when(expressionFactory.create(ExpressionTypes.Equals.class)).thenReturn(equalsExpression);

        ArgumentCaptor<ExpressionTypes.BaseLike.LikeExpressionType> likeExpressionTypeArgumentCaptor =
            ArgumentCaptor.forClass(ExpressionTypes.BaseLike.LikeExpressionType.class);

        BinaryOperandFilterExpression<?> filterExpression;
        DataFilter filter;

        filter = new DataFilter();
        filter.setPath("todo");
        filter.setValue("anything");
        filterExpression = testObj.createFilterExpression(filter, false);
        assertSame(equalsExpression, filterExpression);

        filter = new DataFilter();
        filter.setPath("todo");
        filter.setValue("anything");
        filterExpression = testObj.createFilterExpression(filter, true);
        assertSame(likeExpression, filterExpression);
        verify(likeExpression).setLikeExpressionType(likeExpressionTypeArgumentCaptor.capture());
        assertEquals(ExpressionTypes.BaseLike.LikeExpressionType.CONTAINS, likeExpressionTypeArgumentCaptor.getValue());

        defaultFilterDescriptor.setDefaultFilterType("ilike", "ends-with");

        filter = new DataFilter();
        filter.setPath("todo");
        filter.setValue("anything");
        filterExpression = testObj.createFilterExpression(filter, true);
        assertSame(ignoreCaseLikeExpression, filterExpression);
        verify(ignoreCaseLikeExpression).setLikeExpressionType(likeExpressionTypeArgumentCaptor.capture());
        assertEquals(ExpressionTypes.BaseLike.LikeExpressionType.ENDS_WITH, likeExpressionTypeArgumentCaptor.getValue());

        filter = new DataFilter();
        filter.setPath("deadline");
        filter.setValue("anything");
        filterExpression = testObj.createFilterExpression(filter, true);
        assertSame(greaterThanOrEqualsExpression, filterExpression);

        filter = new DataFilter();
        filter.setPath("deadline");
        filter.setValue("anything");
        filterExpression = testObj.createFilterExpression(filter, true);
        assertSame(greaterThanOrEqualsExpression, filterExpression);

        filter = new DataFilter();
        filter.setPath("other");
        filter.setValue("anything");
        filterExpression = testObj.createFilterExpression(filter, true);
        assertSame(equalsExpression, filterExpression);
    }

    @Test
    @DisplayName("Should map data provider order information to order expression before fetching the data")
    void test_should_map_data_provider_order_information_to_order_expression_before_fetching_the_data() {
        DataResponse mockResponse = mockDataProcessorConvert();
        prepareMetadataForFilter();
        prepareExpressionFactoryForFilter();
        prepareOrderExpressionFactoryForFilter();

        DataResponse dataResponse = testObj.fetchData(buildDataProviderRequestWithFilterAndOrderInfo());

        ArgumentCaptor<FilterExpression> filterCaptor = ArgumentCaptor.forClass(FilterExpression.class);
        ArgumentCaptor<OrderExpression> orderCaptor = ArgumentCaptor.forClass(OrderExpression.class);
        verify(testObj).registerFilter(filterCaptor.capture());
        verify(testObj).registerOrder(orderCaptor.capture());
        FilterExpression filterExpression = filterCaptor.getValue();
        OrderExpression orderExpression = orderCaptor.getValue();

        assertAll(
            () -> assertNotNull(filterExpression),
            () -> assertTrue(filterExpression instanceof AndFilterExpression),
            () -> assertNotNull(orderExpression),
            () -> assertTrue(orderExpression instanceof MockOrder),
            () -> verify(metadataProvider, times(2)).getMetadataForPath(matches("test\\.path(1|2)")),
            () -> assertEquals(2, ((AndFilterExpression) filterExpression).getExpressions().size()),
            () -> assertTrue(((AndFilterExpression) filterExpression).getExpressions().get(0) instanceof ExpressionTypes.Equals),
            () -> assertTrue(((AndFilterExpression) filterExpression).getExpressions().get(1) instanceof ExpressionTypes.Like),
            () -> assertEquals("test.path1", ((ExpressionTypes.Equals) ((AndFilterExpression) filterExpression).getExpressions().get(0)).getPath()),
            () -> assertEquals("test.path2", ((ExpressionTypes.Like) ((AndFilterExpression) filterExpression).getExpressions().get(1)).getPath()),
            () -> assertEquals(new BigDecimal("1"), ((ExpressionTypes.Equals) ((AndFilterExpression) filterExpression).getExpressions().get(0)).getValue()),
            () -> assertEquals("testValue2", ((ExpressionTypes.Like) ((AndFilterExpression) filterExpression).getExpressions().get(1)).getValue()),
            () -> assertSame(mockResponse, dataResponse)
        );
    }

    @Test
    @DisplayName("Should handle paging and filter information from data provider request in fetchSize before delegating the call")
    public void should_handle_paging_and_filter_information_from_data_provider_request_in_fetch_size_before_delegating_the_call() {
        when(testObj.fetchSize(any(PagingFetchRequest.class))).thenReturn(100L);
        prepareMetadataForFilter();
        prepareExpressionFactoryForFilter();

        Long size = testObj.fetchSize(buildComplexDataProviderRequest());

        ArgumentCaptor<FilterExpression> filterCaptor = ArgumentCaptor.forClass(FilterExpression.class);
        verify(testObj).registerFilter(filterCaptor.capture());
        FilterExpression filterExpression = filterCaptor.getValue();

        ArgumentCaptor<PagingFetchRequest> requestCaptor = ArgumentCaptor.forClass(PagingFetchRequest.class);
        verify(testObj).fetchSize(requestCaptor.capture());
        FetchRequest request = requestCaptor.getValue();

        assertAll(
            () -> assertTrue(request instanceof PagingFetchRequest),
            () -> assertEquals((Integer) 2, ((PagingFetchRequest) request).getPage()),
            () -> assertEquals((Integer) 100, ((PagingFetchRequest) request).getPageSize()),
            () -> assertNotNull(filterExpression),
            () -> assertTrue(filterExpression instanceof AndFilterExpression),
            () -> verify(metadataProvider, times(2)).getMetadataForPath(matches("test\\.path(1|2)")),
            () -> assertEquals(2, ((AndFilterExpression) filterExpression).getExpressions().size()),
            () -> assertTrue(((AndFilterExpression) filterExpression).getExpressions().get(0) instanceof ExpressionTypes.Equals),
            () -> assertTrue(((AndFilterExpression) filterExpression).getExpressions().get(1) instanceof ExpressionTypes.Like),
            () -> assertEquals("test.path1", ((ExpressionTypes.Equals) ((AndFilterExpression) filterExpression).getExpressions().get(0)).getPath()),
            () -> assertEquals("test.path2", ((ExpressionTypes.Like) ((AndFilterExpression) filterExpression).getExpressions().get(1)).getPath()),
            () -> assertEquals(new BigDecimal("1"), ((ExpressionTypes.Equals) ((AndFilterExpression) filterExpression).getExpressions().get(0)).getValue()),
            () -> assertEquals("testValue2", ((ExpressionTypes.Like) ((AndFilterExpression) filterExpression).getExpressions().get(1)).getValue()),
            () -> assertEquals(((Long) 100L), size)
        );
    }

    @Test
    @DisplayName("Should find items by json items by calling the data processor")
    public void should_find_items_by_json_items_by_calling_the_data_processor() {
        JsonArray mockArray = mock(JsonArray.class);
        List mockResult = mock(List.class);
        when(dataProcessor.getItems(mockArray)).thenReturn(mockResult);

        List<Object> result = testObj.findItemsByJson(mockArray);

        verify(dataProcessor).getItems(mockArray);
        assertSame(mockResult, result);
    }

    @Test
    @DisplayName("Should proxy the converttokey method")
    public void should_proxy_the_convert_to_key_method() {
        JsonObject mockJsonObj = mock(JsonObject.class);
        Object item = new Object();
        when(dataProcessor.convertToKey(item)).thenReturn(mockJsonObj);

        assertEquals(mockJsonObj, testObj.convertToKey(item));

        verify(dataProcessor).convertToKey(item);
    }

    @Test
    @DisplayName("Should proxy the findItemByIdValue method")
    public void should_proxy_the_find_item_by_id_value_method() {
        Object item = new Object();
        when(dataProcessor.getKeyMapper().getItem("foobar")).thenReturn(item);

        assertEquals(item, testObj.findItemByIdValue("foobar"));

        verify(dataProcessor.getKeyMapper()).getItem("foobar");
    }

    @Test
    @DisplayName("Should find item by json item")
    void test_should_find_item_by_json_item() {
        Object item = new Object();
        when(dataProcessor.getKeyMapper().getItem("foobar")).thenReturn(item);
        JsonObject jsonItem = Json.createObject();
        jsonItem.put(DataProcessor.ITEM_KEY, "foobar");

        Optional<Object> foundItem = testObj.findItem(jsonItem);

        assertNotNull(foundItem);
        assertTrue(foundItem.isPresent());
        assertSame(item, foundItem.get());
    }

    private DataResponse mockDataProcessorConvert() {
        DataResponse mockResponse = mock(DataResponse.class);
        when(dataProcessor.convertToDataResponse(any())).thenReturn(mockResponse);
        return mockResponse;
    }

    private void prepareExpressionFactoryForFilter() {
        when(expressionFactory.create(AndFilterExpression.class)).thenReturn((FilterExpression) new MockAndFE());

        when(expressionFactory.create(ExpressionTypes.Equals.class)).thenReturn((FilterExpression) new MockEquals());
        when(expressionFactory.create(ExpressionTypes.Like.class)).thenReturn((FilterExpression) new MockLike());
    }

    private void prepareOrderExpressionFactoryForFilter() {
        when(orderExpressionFactory.create()).thenReturn(new MockOrder());
    }

    private void prepareMetadataForFilter() {
        BindingMetadata path1Metadata = new BindingMetadata();
        StringPropertyConverter<BigDecimal> path1Converter = mock(StringPropertyConverter.class);
        when(path1Converter.convertFrom("testValue1")).thenReturn(new BigDecimal("1"));
        path1Metadata.setConverter(path1Converter);
        path1Metadata.setPropertyType(BigDecimal.class);
        when(metadataProvider.getMetadataForPath("test.path1")).thenReturn(path1Metadata);
        BindingMetadata path2Metadata = new BindingMetadata();
        StringPropertyConverter<String> path2Converter = mock(StringPropertyConverter.class);
        when(path2Converter.convertFrom("testValue2")).thenReturn("testValue2");
        path2Metadata.setConverter(path2Converter);
        path2Metadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("test.path2")).thenReturn(path2Metadata);
    }

    private DataProviderRequest buildDataProviderRequestWithPageInfo() {
        DataProviderRequest request = createBaseRequest();
        DataRequestBody body = new DataRequestBody();
        setupPageInfo(body);
        request.setBody(body);
        return request;
    }

    private DataProviderRequest buildDataProviderRequestWithFilterInfo() {
        DataProviderRequest request = createBaseRequest();
        DataRequestBody body = new DataRequestBody();
        setupFilters(body);
        request.setBody(body);
        return request;
    }

    private DataProviderRequest buildDataProviderRequestWithFilterAndOrderInfo() {
        DataProviderRequest request = createBaseRequest();
        DataRequestBody body = new DataRequestBody();
        setupFilters(body);
        setupOrders(body);
        request.setBody(body);
        return request;
    }

    private DataProviderRequest buildComplexDataProviderRequest() {
        DataProviderRequest request = createBaseRequest();

        DataRequestBody body = new DataRequestBody();
        setupPageInfo(body);
        setupFilters(body);
        setupSortOrders(body);
        request.setBody(body);
        return request;
    }

    private DataProviderRequest createBaseRequest() {
        DataProviderRequest request = new DataProviderRequest();
        request.setEndpointId("test-id");
        request.setRequestType(DataProviderRequest.RequestType.FETCH_DATA);
        return request;
    }

    private void setupSortOrders(DataRequestBody body) {
        SortOrder sortOrder1 = new SortOrder();
        sortOrder1.setPath("test.path1");
        sortOrder1.setDirection(SortOrder.Direction.ASCENDING);
        SortOrder sortOrder2 = new SortOrder();
        sortOrder2.setPath("test.path2");
        sortOrder2.setDirection(SortOrder.Direction.DESCENDING);

        body.setSortOrders(Arrays.asList(sortOrder1, sortOrder2));
    }

    private void setupFilters(DataRequestBody body) {
        DataFilter filter1 = new DataFilter();
        filter1.setPath("test.path1");
        filter1.setValue("testValue1");
        DataFilter filter2 = new DataFilter();
        filter2.setPath("test.path2");
        filter2.setValue("testValue2");

        body.setFilters(Arrays.asList(filter1, filter2));
    }

    private void setupOrders(DataRequestBody body) {
        SortOrder order1 = new SortOrder();
        order1.setPath("test.asd");
        order1.setDirection(SortOrder.Direction.DESCENDING);
        SortOrder order2 = new SortOrder();
        order2.setPath("test.qwe");
        order2.setDirection(SortOrder.Direction.ASCENDING);

        body.setSortOrders(Arrays.asList(order1, order2));
    }

    private void setupPageInfo(DataRequestBody body) {
        body.setPage(2);
        body.setPageSize(100);
    }


    private static class MockLike extends ExpressionTypes.Like<Object> {

        @Override
        public Object toPredicate() {
            return null;
        }
    }

    private static class MockIgnoreCaseLike extends ExpressionTypes.IgnoreCaseLike<Object> {

        @Override
        public Object toPredicate() {
            return null;
        }
    }

    private class TestCommonDataSource extends CommonDataSource<Object, OrderExpression, FilterExpression<?>, BindingContext> {

        private TestCommonDataSource() {
            super(new BaseDataSourceBindingContext() {
                @Override
                public @NotNull Set<String> getBindings() {
                    return Collections.emptySet();
                }

                @Override
                public @Nullable String getDataSourceName() {
                    return null;
                }

                @Override
                public @NotNull String getDataSourceId() {
                    return null;
                }

                @Override
                public @NotNull Element getParsedElement() {
                    return null;
                }
            });
        }

        @Override
        protected DataProcessor<Object> createDataProcessor() {
            return dataProcessor;
        }

        @Override
        protected void handleHierarchicalRequest(DataProviderRequest request) {

        }

        @Override
        public boolean hasChildren(ClassMetadata<Object> element) {
            return false;
        }

        @Override
        protected Collection<String> getKeyPaths() {
            return null;
        }

        @Override
        public boolean isHierarchical() {
            return false;
        }

        @Override
        public Object findElementByKeyFilter(@Nonnull FilterExpression<?> keyFilter) {
            return null;
        }

        @Override
        public Class<Object> getElementType() {
            return null;
        }

        @Override
        public OrderExpressionFactory<OrderExpression> getOrderExpressionFactory() {
            return orderExpressionFactory;
        }

        @Override
        public void registerOrder(OrderExpression expression) {

        }

        @Override
        @SuppressWarnings("unchecked")
        public FilterExpressionFactory<FilterExpression<?>> getFilterExpressionFactory() {
            return expressionFactory;
        }

        @Override
        public BindingMetadataProvider getMetadataProvider() {
            return metadataProvider;
        }

        @Override
        public void registerFilter(FilterExpression expression) {

        }

        @Override
        public long fetchSize(@Nullable PagingFetchRequest request) {
            return -1L;
        }

        @Override
        public boolean hasChildren(FilterExpression<?> request) {
            return false;
        }

        @Override
        public List<Object> fetchData(@Nullable PagingFetchRequest request) {
            return null;
        }

        @Override
        protected OrmKeyMapper<Object, ?> createKeyMapper() {
            throw new UnsupportedOperationException();
        }
    }

}
