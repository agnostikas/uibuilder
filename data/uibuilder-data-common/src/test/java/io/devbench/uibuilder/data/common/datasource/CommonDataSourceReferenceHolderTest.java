/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.textfield.TextField;
import io.devbench.uibuilder.data.api.datasource.interfaces.DataSourceRefreshNotifiable;
import io.devbench.uibuilder.data.api.exceptions.DataSourceReferenceException;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.lang.ref.WeakReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommonDataSourceReferenceHolderTest {

    @Mock
    private Button reference1;

    @Mock
    private TextField reference2;

    @Mock
    private Button reference3;

    @Mock
    private TextField reference4;

    @Mock
    private DataSourceReusingButton reference5;

    @Mock
    private DataSourceReusingButton reference6;

    @Mock
    private WeakReference<Component> weakReference1;

    @Mock
    private WeakReference<Component> weakReference2;

    @Mock
    private CommonDataSource<?, ?, ?, ?> dataSource;

    private CommonDataSourceReferenceHolder<CommonDataSource<?, ?, ?, ?>> testObj;


    @BeforeEach
    private void setup() {
        when(weakReference1.get()).thenReturn(reference1);
        testObj = new TestCommonDataSourceReferenceHolder(reference1, dataSource);
    }

    @Test
    @DisplayName("Should be able to hold reference, only if the given component is a different type of component, than any already stored references' type")
    public void should_be_able_to_hold_reference_if_the_given_component_is_a_different_type_of_component_than_any_already_stored_references_type() {
        boolean shouldBeTrue = testObj.canHoldReference(reference2);
        boolean shouldBeFalse = testObj.canHoldReference(reference3);

        assertAll(
            () -> assertTrue(shouldBeTrue),
            () -> assertFalse(shouldBeFalse)
        );
    }

    @Test
    @DisplayName("Should be able to hold reference with the same type, if component supports data-source reuse")
    public void should_be_able_to_hold_reference_with_the_same_type_if_component_supports_data_source_reuse() {
        when(reference5.getContextId()).thenReturn("1");
        when(reference6.getContextId()).thenReturn("2");

        testObj.registerReference(reference5);

        assertTrue(testObj.canHoldReference(reference6));
    }

    @Test
    @DisplayName("Should not be able to hold reference with same type and context id, when component supports data-source reuse")
    public void should_not_be_able_to_hold_reference_with_same_type_and_context_id_when_component_supports_data_source_reuse() {
        when(reference5.getContextId()).thenReturn("1");
        when(reference6.getContextId()).thenReturn("1");

        testObj.registerReference(reference5);

        assertFalse(testObj.canHoldReference(reference6));
    }

    @Test
    @DisplayName("Should return the data source set in the constructor")
    public void should_return_the_data_source_set_in_the_constructor() {
        assertSame(dataSource, testObj.getDataSource());
    }

    @Test
    @DisplayName("Should register reference to the specified component if it can hold this reference, otherwise should throw exception")
    public void should_register_reference_to_the_specified_component_if_it_can_hold_this_reference_otherwise_should_throw_exception() {
        assertDoesNotThrow(() -> testObj.registerReference(reference2));

        DataSourceReferenceException exception = assertThrows(DataSourceReferenceException.class, () -> testObj.registerReference(reference4));

        assertEquals("Reference cannot be registered, because the holder already contains a reference with this class.", exception.getMessage());
    }

    @Test
    @DisplayName("Should be able to hold the reference for component, only if the component with the same type is the same as the component being asked for")
    public void should_be_able_to_hold_the_reference_for_component_only_if_the_component_with_the_same_type_is_the_same_as_the_component_being_asked_for() {
        assertTrue(testObj.canHoldReference(reference1));
        when(reference1.equals(reference3)).thenReturn(true);
        assertFalse(testObj.canHoldReference(reference3));
    }

    @Test
    @DisplayName("Should not create new weak reference, if the same component passed for registration, that is already referenced")
    public void should_not_create_new_weak_reference_if_the_same_component_passed_for_registration_that_is_already_referenced() {
        CommonDataSourceReferenceHolder<CommonDataSource<?, ?, ?, ?>> spy = Mockito.spy(testObj);
        spy.registerReference(reference1);
        verify(spy, times(0)).createWeakReferenceFor(reference1);
    }

    @Test
    @DisplayName("Should return true for contains reference, until all weak references registered are lost the reference to the components")
    public void should_return_true_for_contains_reference_until_all_weak_references_registered_are_lost_the_reference_to_the_components() {
        when(weakReference2.get()).thenReturn(reference2);
        ((TestCommonDataSourceReferenceHolder) testObj).activeRef = weakReference2;
        testObj.registerReference(reference2);

        boolean shouldBeTrue = testObj.containsReferences();
        when(weakReference1.get()).thenReturn(null);
        boolean shouldBeStillTrue = testObj.containsReferences();
        when(weakReference2.get()).thenReturn(null);
        boolean shouldBeFalse = testObj.containsReferences();

        assertAll(
            () -> assertTrue(shouldBeTrue),
            () -> assertTrue(shouldBeStillTrue),
            () -> assertFalse(shouldBeFalse)
        );
    }

    @Test
    @DisplayName("Should cleanup references, when the components in them are detached")
    public void should_cleanup_references_when_the_components_in_them_are_detached() {
        Answer clearAnswer = invocation -> {
            when(((WeakReference) invocation.getMock()).get()).thenReturn(null);
            return null;
        };
        doAnswer(clearAnswer).when(weakReference1).clear();
        doAnswer(clearAnswer).when(weakReference2).clear();

        when(weakReference2.get()).thenReturn(reference2);
        ((TestCommonDataSourceReferenceHolder) testObj).activeRef = weakReference2;

        testObj.registerReference(reference2);

        ArgumentCaptor<ComponentEventListener<DetachEvent>> detachListenerCaptor = ArgumentCaptor.forClass(ComponentEventListener.class);
        verify(reference1).addDetachListener(detachListenerCaptor.capture());
        verify(reference2).addDetachListener(detachListenerCaptor.capture());

        DetachEvent detachEvent = mock(DetachEvent.class);
        when(detachEvent.getSource())
            .thenReturn(reference1)
            .thenReturn(reference2);

        detachListenerCaptor.getAllValues().forEach(listener -> listener.onComponentEvent(detachEvent));

        assertAll(
            () -> verify(weakReference1).clear(),
            () -> verify(weakReference2).clear(),
            () -> assertFalse(testObj.containsReferences())
        );
    }

    @Test
    @DisplayName("Should answer whether it holds reference to a component or not")
    public void should_answer_whether_it_holds_reference_to_a_component_or_not() {
        when(weakReference2.get()).thenReturn(null);
        ((TestCommonDataSourceReferenceHolder) testObj).activeRef = weakReference2;
        testObj.registerReference(reference2);

        boolean shouldBeTrue = testObj.hasReferenceTo(reference1);
        boolean shouldBeFalse = testObj.hasReferenceTo(reference2);

        assertAll(
            () -> assertTrue(shouldBeTrue),
            () -> assertFalse(shouldBeFalse)
        );
    }

    @Test
    @DisplayName("Should answer if it holds reference to data-source reusing component")
    public void should_answer_if_it_holds_reference_to_data_source_reusing_component() {
        when(reference5.getContextId()).thenReturn("1");
        when(reference6.getContextId()).thenReturn("2");

        when(weakReference1.get()).thenReturn(reference5);
        testObj.registerReference(reference5);

        assertAll(
            () -> assertTrue(testObj.hasReferenceTo(reference5)),
            () -> assertFalse(testObj.hasReferenceTo(reference6))
        );

        when(reference6.getContextId()).thenReturn("1");

        assertFalse(testObj.hasReferenceTo(reference6));
    }

    @Test
    @DisplayName("Should notify refresh notifiable references when refresh requested")
    public void should_notify_refresh_notifiable_references_when_refresh_requested() {
        NotifiableButton notifiableReference = mock(NotifiableButton.class);
        when(weakReference2.get()).thenReturn(notifiableReference);
        ((TestCommonDataSourceReferenceHolder) testObj).activeRef = weakReference2;
        testObj.registerReference(notifiableReference);
        reset(reference1);

        testObj.notifyReferencesAboutRefresh();

        assertAll(
            () -> verify(notifiableReference).refresh(),
            () -> verifyNoMoreInteractions(reference1)
        );
    }

    private class TestCommonDataSourceReferenceHolder extends CommonDataSourceReferenceHolder<CommonDataSource<?, ?, ?, ?>> {

        private WeakReference<Component> activeRef;

        TestCommonDataSourceReferenceHolder(Component initialReference, CommonDataSource dataSource) {
            super(initialReference, dataSource);
        }

        @Override
        WeakReference<Component> createWeakReferenceFor(Component component) {
            return activeRef == null ? weakReference1 : activeRef;
        }
    }

    private static class NotifiableButton extends Button implements DataSourceRefreshNotifiable {

        @Override
        public void refresh() {

        }
    }

    private static class DataSourceReusingButton extends Button implements SupportsDataSourceReuse {

        @Override
        public String getContextId() {
            return null;
        }
    }

}
