/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.filter.validators;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.data.api.filter.validation.CustomFilterValidator;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationExecutor;
import io.devbench.uibuilder.data.api.filter.validation.FilterValidationReport;
import io.devbench.uibuilder.data.common.filter.comperingfilters.AnyOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.BinaryOperandFilterExpression;
import io.devbench.uibuilder.data.common.filter.comperingfilters.EmptyOperandFilterExpression;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class ValueNotNullFilterValidatorTest {

    @InjectMocks
    @SuppressWarnings("unused")
    @SingletonInstance(FilterValidationExecutor.class)
    private FilterValidationExecutor validator;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @BeforeEach
    void setUp() {
        Set<Class<?>> customFilterValidatorClasses = new HashSet<>(Collections.singletonList(ValueNotNullFilterValidator.class));
        doReturn(customFilterValidatorClasses).when(memberScanner).findClassesByAnnotation(CustomFilterValidator.class);
    }

    @Test
    @DisplayName("Should contain error when BinaryOperandFilterExpression value is null")
    void test_should_contain_error_when_binary_operand_filter_expression_value_is_null() {
        BinaryOperandFilterExpression<Object> expression = new BinaryOperandFilterExpression<Object>() {
            @Override
            protected Class<? extends BinaryOperandFilterExpression<Object>> getOperatorDescribingClass() {
                return null;
            }

            @Override
            public Object toPredicate() {
                return null;
            }
        };
        expression.setPath("");
        expression.setValue("");

        FilterValidationReport report = FilterValidationExecutor.getInstance().validate(ValueNotNullFilterValidator.NAME, expression);

        assertNotNull(report);
        assertFalse(report.hasErrors());

        expression.setValue(null);

        report = FilterValidationExecutor.getInstance().validate(ValueNotNullFilterValidator.NAME, expression);
        assertNotNull(report);
        assertTrue(report.hasErrors());
    }

    @Test
    @DisplayName("Should contain error when AnyOperandFilterExpression values contain at least one item with value null")
    void test_should_contain_error_when_any_operand_filter_expression_values_contain_at_least_one_item_with_value_null() {
        AnyOperandFilterExpression<Object> expression = new AnyOperandFilterExpression<Object>() {
            @Override
            protected Class<? extends AnyOperandFilterExpression<Object>> getOperatorDescribingClass() {
                return null;
            }

            @Override
            public Object toPredicate() {
                return null;
            }
        };
        expression.setPath("");
        expression.setValues(Arrays.asList("one", "two", "three", "four"));

        FilterValidationReport report = FilterValidationExecutor.getInstance().validate(ValueNotNullFilterValidator.NAME, expression);

        assertNotNull(report);
        assertFalse(report.hasErrors());

        expression.setValues(Arrays.asList("one", "two", null, "four"));

        report = FilterValidationExecutor.getInstance().validate(ValueNotNullFilterValidator.NAME, expression);
        assertNotNull(report);
        assertTrue(report.hasErrors());
    }

    @Test
    @DisplayName("Should not get any error when validating an EmptyOperandFilterExpression")
    void test_should_not_get_any_error_when_validating_an_empty_operand_filter_expression() {
        EmptyOperandFilterExpression<Object> expression = new EmptyOperandFilterExpression<Object>() {
            @Override
            protected Class<? extends EmptyOperandFilterExpression<Object>> getOperatorDescribingClass() {
                return null;
            }

            @Override
            public Object toPredicate() {
                return null;
            }
        };
        expression.setPath("");

        FilterValidationReport report = FilterValidationExecutor.getInstance().validate(ValueNotNullFilterValidator.NAME, expression);

        assertNotNull(report);
        assertFalse(report.hasErrors());
        assertFalse(report.isInvalid());
    }

}
