/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.data.api.datasource.DataSourceManager;
import io.devbench.uibuilder.data.api.datasource.DataSourceProvider;
import io.devbench.uibuilder.data.api.exceptions.DataProviderEndpointException;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataProviderRequest;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataResponse;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class DataProviderEndpointManagerTest {

    @Mock
    private Component mockComponent;

    @Mock
    private DataSourceProvider<CommonDataSource<?, ?, ?, ?>, CommonDataSourceSelector> dataSourceProvider;

    @Mock
    private CommonDataSource dataSource;

    @Mock
    @SingletonInstance(DataSourceManager.class)
    private DataSourceManager dataSourceManager;

    private DataProviderEndpointManager testObj;

    @BeforeEach
    private void setup() {
        when(dataSourceManager.getDataSourceProvider()).thenReturn((DataSourceProvider) dataSourceProvider);
        testObj = DataProviderEndpointManager.getInstance();
        when(dataSource.getDataSourceId()).thenReturn("testDataSourceId");
        when(dataSourceProvider.getDataSource(eq("testDataSourceId"), eq(new CommonDataSourceSelector("testQuery", mockComponent)))).thenReturn(dataSource);
    }

    @Test
    @DisplayName("Should register component to data source name and default query, and return a data source endpoint registration instance")
    public void should_register_component_to_data_source_name_and_default_query_and_return_a_data_source_endpoint_registration_instance() {
        DataProviderEndpointRegistration registration = registerMockComponent();

        verify(dataSourceProvider).getDataSource(eq("testDataSourceId"), eq(new CommonDataSourceSelector("testQuery", mockComponent)));

        assertAll(
            () -> assertEquals("testDataSourceId", registration.getEndpointId()),
            () -> assertEquals("/datasource-api/testDataSourceId?v-uiId=" + UI.getCurrent().getUIId(), registration.getUrl()),
            () -> assertSame(dataSource, registration.getDataSource())
        );
    }

    @Test
    @DisplayName("Should process fetch data requests by passing them to the data source")
    public void should_process_fetch_data_requests_by_passing_them_to_the_data_source() {
        DataProviderRequest mockRequest = createDataSourceRequest(registerMockComponent());
        DataResponse mockResponse = mock(DataResponse.class);
        when(dataSource.fetchData(mockRequest)).thenReturn(mockResponse);

        DataResponse response = testObj.processFetchDataRequest(mockRequest);

        verify(dataSource).fetchData(mockRequest);
        assertSame(mockResponse, response);
    }

    @Test
    @DisplayName("Should process fetch size requests, by passing them to the datasource")
    public void should_process_fetch_size_requests_by_passing_them_to_the_datasource() {
        DataProviderRequest mockRequest = createDataSourceRequest(registerMockComponent());
        when(dataSource.fetchSize(mockRequest)).thenReturn(100L);

        Long result = testObj.processFetchSizeRequest(mockRequest);

        verify(dataSource).fetchSize(mockRequest);
        assertEquals(((Long) 100L), result);
    }

    @Test
    @DisplayName("Should return bound component by endpointId")
    void should_return_bound_component_by_endpoint_id() {
        DataProviderEndpointRegistration registration = registerMockComponent();
        String endpointId = registration.getEndpointId();

        Optional<Component> component = testObj.getComponent(endpointId);

        assertAll(
            () -> assertNotNull(component),
            () -> assertTrue(component.isPresent()),
            () -> assertSame(mockComponent, component.get())
        );
    }

    @Test
    @DisplayName("should return empty if no component has been bound to the endpoint")
    void test_should_return_empty_if_no_component_has_been_bound_to_the_endpoint() {
        String endpointId = UUID.randomUUID().toString();

        Optional<Component> component = testObj.getComponent(endpointId);

        assertAll(
            () -> assertNotNull(component),
            () -> assertFalse(component.isPresent())
        );
    }

    @Test
    void should_return_empty_item_if_datasource_is_not_exist_or_cannot_be_resolve_the_item() {
        assertEquals(Optional.empty(), testObj.getByItemKey(Json.createObject()));

        final JsonObject testJsonObjectWithoutEnpointID = Json.createObject();
        testJsonObjectWithoutEnpointID.put(DataProcessor.ITEM_ENDPOINT_ID, "1_endpoint_id");
        assertEquals(Optional.empty(), testObj.getByItemKey(testJsonObjectWithoutEnpointID));

        final JsonObject testJsonObjectWithoutItemKey = Json.createObject();
        testJsonObjectWithoutItemKey.put(DataProcessor.ITEM_KEY, "1_key");
        assertEquals(Optional.empty(), testObj.getByItemKey(testJsonObjectWithoutItemKey));

        final JsonObject testJsonObjectWithWrongParameters = Json.createObject();
        testJsonObjectWithWrongParameters.put(DataProcessor.ITEM_KEY, "wrong_key");
        testJsonObjectWithWrongParameters.put(DataProcessor.ITEM_ENDPOINT_ID, "wrong_endpoint_id");
        assertThrows(DataProviderEndpointException.class, () -> testObj.getByItemKey(testJsonObjectWithWrongParameters));
    }

    @Test
    void should_return_the_correct_object_if_the_raw_item_is_resolvable() {
        final DataProviderEndpointRegistration registration = registerMockComponent();
        final String endpointId = registration.getEndpointId();
        final JsonObject testJsonObjectWithCorrectParameters = Json.createObject();
        testJsonObjectWithCorrectParameters.put(DataProcessor.ITEM_KEY, "correct_key");
        testJsonObjectWithCorrectParameters.put(DataProcessor.ITEM_ENDPOINT_ID, endpointId);
        when(dataSource.findItemByIdValue(any())).thenReturn("THIS_IS_CORRECT");
        assertEquals("THIS_IS_CORRECT", testObj.getByItemKey(testJsonObjectWithCorrectParameters).get());
    }

    private DataProviderEndpointRegistration registerMockComponent() {
        return testObj.register(mockComponent, "testDataSourceId", "testDataSource", "testQuery");
    }

    private DataProviderRequest createDataSourceRequest(DataProviderEndpointRegistration registration) {
        DataProviderRequest request = mock(DataProviderRequest.class);
        when(request.getEndpointId()).thenReturn(registration.getEndpointId());
        return request;
    }

}
