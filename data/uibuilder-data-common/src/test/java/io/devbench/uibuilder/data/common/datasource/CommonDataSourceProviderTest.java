/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.common.exceptions.DataSourceNotActiveException;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class CommonDataSourceProviderTest {

    @Mock
    private CommonDataSourceContext<?> context;

    private CommonDataSourceProvider<?> testObj;

    @BeforeEach
    private void setup() {
        when(UI.getCurrent().getUIId()).thenReturn(1);
        UIContext.getContext().addToActiveUIContextAs(CommonDataSourceContext.class, context);
        testObj = new TestCommonDataSourceProvider();
    }

    @Test
    @DisplayName("Should find data source using the common data source context")
    public void should_find_data_source_using_the_common_data_source_context() {
        CommonDataSourceSelector selector = new CommonDataSourceSelector(null, mock(Component.class));
        testObj.getDataSource("test", selector);
        verify(context).findDataSource(eq("test"), same(selector), any(Supplier.class));
    }

    @Test
    @DisplayName("Should notify the common data source context about the refresh request")
    public void should_notify_the_common_data_source_context_about_the_refresh_request() {
        CommonDataSourceSelector selector = new CommonDataSourceSelector(null, mock(Component.class));
        testObj.requestRefresh("test", selector);
        verify(context).refreshRequestedForDataSource(eq("test"), same(selector));
    }

    @Test
    @DisplayName("Should throw exception, when the context is not registered")
    public void should_throw_exception_when_the_context_is_not_registered() {
        when(UI.getCurrent().getUIId()).thenReturn(2);

        assertThrows(DataSourceNotActiveException.class, () -> testObj.requestRefresh("test", null));
    }


    private static class TestCommonDataSourceProvider implements CommonDataSourceProvider<CommonDataSource> {
        @Override
        public CommonDataSource createNewDataSource(String dataSourceName, String optionalDefaultQueryName) {
            return null;
        }

        @Override
        public void registerBindingContextForDataSource(BindingContext bindingContext, @Nullable CommonDataSourceSelector dataSourceSelector) {

        }

        @Nullable
        @Override
        public BindingContext getBindingContextForName(String dataSourceName, @Nullable CommonDataSourceSelector dataSourceSelector) {
            return null;
        }
    }
}
