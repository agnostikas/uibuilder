/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.Tag;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import elemental.json.impl.JreJsonFactory;
import elemental.json.impl.JreJsonString;
import io.devbench.uibuilder.core.controllerbean.uiproperty.StringPropertyConverter;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadata;
import io.devbench.uibuilder.data.api.filter.metadata.BindingMetadataProvider;
import io.devbench.uibuilder.data.common.component.ItemPredicateProvider;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataResponse;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

import static io.devbench.uibuilder.test.JsonAssert.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({ResourceReaderExtension.class, MockitoExtension.class})
class DataProcessorTest {

    @Mock
    private CommonDataSource<TestElement, ?, ?, ?> dataSource;

    @Mock
    private OrmKeyMapper<TestElement, ?> keyMapper;

    @Mock
    private BindingMetadataProvider metadataProvider;

    @Mock
    private DataProviderEndpointManager endpointManager;

    private DataProcessor<TestElement> testObj;

    @BeforeEach
    private void setup() {
        doReturn("e1wia2V5XCI6XCJ0ZXN0S2V5XCJ9").when(keyMapper).getKey(any(ClassMetadata.class));
        when(dataSource.getDataSourceId()).thenReturn("test-endpoint");
        testObj = new DataProcessor<>(dataSource, metadataProvider, () -> keyMapper);
        UIContext.getContext().addToActiveUIContextAs(DataProviderEndpointManager.class, endpointManager);
    }

    @Test
    @DisplayName("Should map elements to data response using the state node manager and the key mapper")
    public void should_map_elements_to_data_response_using_the_state_node_manager_and_the_key_mapper(
        @ReadResource("/data-processor-expected-response.json") String expectedResponseString) {
        JsonArray testExpectedResponse = new JreJsonFactory().parse(expectedResponseString);

        when(dataSource.getBindings()).thenReturn(Sets.newSet("item.id", "item.name", "item.subElement.name"));

        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        StringPropertyConverter idPropertyConverter = mock(StringPropertyConverter.class);
        when(idPropertyConverter.convertTo(any(Integer.class))).then(invocation -> invocation.getArgument(0).toString());
        idMetadata.setConverter(idPropertyConverter);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);

        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        when(namePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        BindingMetadata subElementNameMetadata = new BindingMetadata();
        subElementNameMetadata.setPath("subElement.name");
        StringPropertyConverter subNamePropertyConverter = mock(StringPropertyConverter.class);
        when(subNamePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        subElementNameMetadata.setConverter(subNamePropertyConverter);
        subElementNameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("subElement.name")).thenReturn(subElementNameMetadata);

        DataResponse dataResponse = testObj.convertToDataResponse(buildTestElements());

        verify(metadataProvider, times(3)).getMetadataForPath(matches("((id|name)|subElement\\.name)"));

        verify(keyMapper, times(2)).getKey(any(ClassMetadata.class));

        assertAll(
            () -> assertNotNull(dataResponse),
            () -> assertNotNull(dataResponse.getResponse()),
            () -> assertEquals(2, dataResponse.getResponse().length()),
            () -> assertJsonEquals(testExpectedResponse, dataResponse.getResponse())
        );
    }

    @Test
    @DisplayName("Should override edit mode property values by inline edit handler")
    void test_should_override_edit_mode_property_values_by_inline_edit_handler() {
        List<TestElement> testElements = new ArrayList<>();
        TestElement element1 = TestElement.builder().id(1).name("Element 1").build();
        TestElement element2 = TestElement.builder().id(2).name("Element 2").build();
        testElements.add(element1);
        testElements.add(element2);

        when(dataSource.getBindings()).thenReturn(Sets.newSet("item.name"));

        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(namePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        doReturn("element1__KEY").when(keyMapper).getKey(argThat(argument -> argument.getInstance() == element1));
        doReturn("element2__KEY").when(keyMapper).getKey(argThat(argument -> argument.getInstance() == element2));
        JsonObject element2jsonItem = Json.createObject();
        element2jsonItem.put(DataProcessor.ITEM_KEY, "element2__KEY");

        doReturn(Optional.of(new TestHasEnabledComponent(true)))
            .when(endpointManager)
            .getComponent("test-endpoint");

        testObj.getInlineEditHandler().setEditMode(element2jsonItem, true);
        testObj.getInlineEditHandler().registerPropertyChange(element2jsonItem, "name", new JreJsonString("Modified 2"));

        DataResponse dataResponse = testObj.convertToDataResponse(testElements);

        assertNotNull(dataResponse);
        JsonArray response = dataResponse.getResponse();

        assertNotNull(response);
        assertEquals(2, response.length());

        AtomicBoolean element1Unmodified = new AtomicBoolean(false);
        AtomicBoolean element2Modified = new AtomicBoolean(false);
        for (int i = 0; i < response.length(); i++) {
            JsonValue jsonValue = response.get(i);
            if (jsonValue instanceof JsonObject) {
                if ("element1__KEY".equals(((JsonObject) jsonValue).getString(DataProcessor.ITEM_KEY))) {
                    if ("Element 1".equals(((JsonObject) jsonValue).getString("name"))) {
                        element1Unmodified.set(true);
                    }
                }
                if ("element2__KEY".equals(((JsonObject) jsonValue).getString(DataProcessor.ITEM_KEY))) {
                    if ("Modified 2".equals(((JsonObject) jsonValue).getString("name"))) {
                        element2Modified.set(true);
                    }
                }
            }
        }

        assertTrue(element1Unmodified.get(), "Element 1 should not have been modified by inline edit handler");
        assertTrue(element2Modified.get(), "Element 2 should HAVE been modified by the inline edit handler");
    }

    @Test
    @DisplayName("Should override edit allowed property by provided predicate")
    void test_should_override_edit_allowed_property_by_provided_predicate() {
        List<TestElement> testElements = new ArrayList<>();
        TestElement element1 = TestElement.builder().id(1).name("Element 1").build();
        TestElement element2 = TestElement.builder().id(2).name("Element 2").build();
        testElements.add(element1);
        testElements.add(element2);

        when(dataSource.getBindings()).thenReturn(Sets.newSet("item.name"));

        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(namePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        doReturn("element1__KEY").when(keyMapper).getKey(argThat(argument -> argument.getInstance() == element1));
        doReturn("element2__KEY").when(keyMapper).getKey(argThat(argument -> argument.getInstance() == element2));
        doReturn(element1).when(keyMapper).getItem("element1__KEY");
        doReturn(element2).when(keyMapper).getItem("element2__KEY");

        ItemPredicateProvider<TestElement> itemPredicateProvider =
            new TestItemPredicateProviderComponent<>(item -> item == null || !item.getName().endsWith("2"));

        doReturn(Optional.of(itemPredicateProvider)).when(endpointManager).getComponent("test-endpoint");

        DataResponse dataResponse = testObj.convertToDataResponse(testElements);

        assertNotNull(dataResponse);
        JsonArray response = dataResponse.getResponse();

        assertNotNull(response);
        assertEquals(2, response.length());

        AtomicBoolean element1EditAllowed = new AtomicBoolean(false);
        AtomicBoolean element2EditDenied = new AtomicBoolean(false);
        for (int i = 0; i < response.length(); i++) {
            JsonValue jsonValue = response.get(i);
            if (jsonValue instanceof JsonObject) {
                if ("element1__KEY".equals(((JsonObject) jsonValue).getString(DataProcessor.ITEM_KEY))) {
                    if (((JsonObject) jsonValue).getObject("_state").getBoolean("editAllowed")) {
                        element1EditAllowed.set(true);
                    }
                }
                if ("element2__KEY".equals(((JsonObject) jsonValue).getString(DataProcessor.ITEM_KEY))) {
                    if (!((JsonObject) jsonValue).getObject("_state").getBoolean("editAllowed")) {
                        element2EditDenied.set(true);
                    }
                }
            }
        }

        assertTrue(element1EditAllowed.get(), "Element 1 should have been allowed to be edited inline");
        assertTrue(element2EditDenied.get(), "Element 2 should HAVE NOT been allowed to be edited inline");
    }

    @Test
    @DisplayName("Should override edit allowed and save allowed property by containers disabled state")
    void test_should_override_edit_allowed_and_save_allowed_property_by_containers_disabled_state() {
        List<TestElement> testElements = new ArrayList<>();
        TestElement element1 = TestElement.builder().id(1).name("Element 1").build();
        TestElement element2 = TestElement.builder().id(2).name("Element 2").build();
        testElements.add(element1);
        testElements.add(element2);

        when(dataSource.getBindings()).thenReturn(Sets.newSet("item.name"));

        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(namePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        doReturn("element1__KEY").when(keyMapper).getKey(argThat(argument -> argument.getInstance() == element1));
        doReturn("element2__KEY").when(keyMapper).getKey(argThat(argument -> argument.getInstance() == element2));
        doReturn(element1).when(keyMapper).getItem("element1__KEY");
        doReturn(element2).when(keyMapper).getItem("element2__KEY");

        doReturn(Optional.of(new TestHasEnabledComponent(false)))
            .when(endpointManager)
            .getComponent("test-endpoint");

        DataResponse dataResponse = testObj.convertToDataResponse(testElements);

        assertNotNull(dataResponse);
        JsonArray response = dataResponse.getResponse();

        assertNotNull(response);
        assertEquals(2, response.length());

        AtomicBoolean element1EditDenied = new AtomicBoolean(false);
        AtomicBoolean element2EditDenied = new AtomicBoolean(false);
        AtomicBoolean element1SaveDenied = new AtomicBoolean(false);
        AtomicBoolean element2SaveDenied = new AtomicBoolean(false);
        for (int i = 0; i < response.length(); i++) {
            JsonValue jsonValue = response.get(i);
            if (jsonValue instanceof JsonObject) {
                if ("element1__KEY".equals(((JsonObject) jsonValue).getString(DataProcessor.ITEM_KEY))) {
                    if (!((JsonObject) jsonValue).getObject("_state").getBoolean("editAllowed")) {
                        element1EditDenied.set(true);
                    }
                    if (!((JsonObject) jsonValue).getObject("_state").getBoolean("saveAllowed")) {
                        element1SaveDenied.set(true);
                    }
                }
                if ("element2__KEY".equals(((JsonObject) jsonValue).getString(DataProcessor.ITEM_KEY))) {
                    if (!((JsonObject) jsonValue).getObject("_state").getBoolean("editAllowed")) {
                        element2EditDenied.set(true);
                    }
                    if (!((JsonObject) jsonValue).getObject("_state").getBoolean("saveAllowed")) {
                        element2SaveDenied.set(true);
                    }
                }
            }
        }

        assertTrue(element1EditDenied.get(), "Element 1 should HAVE NOT been allowed to be edited inline");
        assertTrue(element2EditDenied.get(), "Element 2 should HAVE NOT been allowed to be edited inline");
        assertTrue(element1SaveDenied.get(), "Element 1 should HAVE NOT been allowed to be saved");
        assertTrue(element2SaveDenied.get(), "Element 2 should HAVE NOT been allowed to be saved");
    }


    @Test
    @DisplayName("Should fetch the items represented by the jsonItems, by extracting the keys from them, and passing it to the key mapper")
    public void should_fetch_the_items_represented_by_the_json_items_by_extracting_the_keys_from_them_and_passing_it_to_the_key_mapper(
        @ReadResource("/data-processor-expected-response.json") String exampleItemsString) {
        JsonArray jsonItems = new JreJsonFactory().parse(exampleItemsString);
        when(keyMapper.getItem(any())).thenReturn(mock(TestElement.class));

        List<TestElement> items = testObj.getItems(jsonItems);

        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        verify(keyMapper, times(2)).getItem(keyCaptor.capture());

        assertAll(
            () -> assertEquals(2, items.size()),
            () -> assertEquals(2, keyCaptor.getAllValues().size()),
            () -> assertEquals("e1wia2V5XCI6XCJ0ZXN0S2V5XCJ9", keyCaptor.getAllValues().get(0)),
            () -> assertEquals("e1wia2V5XCI6XCJ0ZXN0S2V5XCJ9", keyCaptor.getAllValues().get(1))
        );
    }


    private List<TestElement> buildTestElements() {
        TestElement element1 = TestElement.builder()
            .id(1)
            .name("element1")
            .subElement(TestElement.builder().name("subname1").build())
            .build();
        TestElement element2 = TestElement.builder()
            .id(2)
            .name("element2")
            .subElement(TestElement.builder().name("subname2").build())
            .build();
        return Arrays.asList(element1, null, element2);
    }

    @Test
    @DisplayName("Should map element to json array which only contains the keys of the elements")
    void should_map_element_to_json_array_which_only_contains_the_keys_of_the_elements(
        @ReadResource("/data-processor-expected-keys-response.json") String expectedResponseString) {
        JsonArray testExpectedResponse = new JreJsonFactory().parse(expectedResponseString);
        when(dataSource.getBindings())
            .thenReturn(Sets.newSet("item.id", "item.name", "item.subElement.name"));
        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        StringPropertyConverter idPropertyConverter = mock(StringPropertyConverter.class);
        when(idPropertyConverter.convertTo(any(Integer.class)))
            .then(invocation -> invocation.getArgument(0).toString());
        idMetadata.setConverter(idPropertyConverter);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);

        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        when(namePropertyConverter.convertTo(any(String.class)))
            .then(invocation -> invocation.getArgument(0));
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        BindingMetadata subElementNameMetadata = new BindingMetadata();
        subElementNameMetadata.setPath("subElement.name");
        StringPropertyConverter subNamePropertyConverter = mock(StringPropertyConverter.class);
        when(subNamePropertyConverter.convertTo(any(String.class)))
            .then(invocation -> invocation.getArgument(0));
        subElementNameMetadata.setConverter(subNamePropertyConverter);
        subElementNameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("subElement.name"))
            .thenReturn(subElementNameMetadata);

        JsonArray dataResponse = testObj.convertToKeysArray(buildTestElements());

        verify(keyMapper, times(2)).getKey(any(ClassMetadata.class));

        assertAll(
            () -> assertNotNull(dataResponse),
            () -> assertEquals(2, dataResponse.length()),
            () -> assertJsonEquals(testExpectedResponse, dataResponse)
        );
    }

    @Test
    @DisplayName("Should map single element to json object which only contains the keys of the elements")
    void should_map_single_element_to_json_object_which_only_contains_the_keys_of_the_elements() {
        when(dataSource.getBindings()).thenReturn(Sets.newSet("item.id", "item.name", "item.subElement.name"));
        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        StringPropertyConverter idPropertyConverter = mock(StringPropertyConverter.class);
        when(idPropertyConverter.convertTo(any(Integer.class))).then(invocation -> invocation.getArgument(0).toString());
        idMetadata.setConverter(idPropertyConverter);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);

        BindingMetadata nameMetadata = new BindingMetadata();
        nameMetadata.setPath("name");
        StringPropertyConverter namePropertyConverter = mock(StringPropertyConverter.class);
        when(namePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        nameMetadata.setConverter(namePropertyConverter);
        nameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("name")).thenReturn(nameMetadata);

        BindingMetadata subElementNameMetadata = new BindingMetadata();
        subElementNameMetadata.setPath("subElement.name");
        StringPropertyConverter subNamePropertyConverter = mock(StringPropertyConverter.class);
        when(subNamePropertyConverter.convertTo(any(String.class))).then(invocation -> invocation.getArgument(0));
        subElementNameMetadata.setConverter(subNamePropertyConverter);
        subElementNameMetadata.setPropertyType(String.class);
        when(metadataProvider.getMetadataForPath("subElement.name")).thenReturn(subElementNameMetadata);

        JsonObject dataResponse = testObj.convertToKey(buildTestElements().get(0));

        verify(keyMapper, times(1)).getKey(any(ClassMetadata.class));

        assertAll(
            () -> assertNotNull(dataResponse),
            () -> assertEquals("e1wia2V5XCI6XCJ0ZXN0S2V5XCJ9", dataResponse.getString(DataProcessor.ITEM_KEY))
        );
    }

    @Test
    void should_not_throw_exception_when_the_binding_path_deeper_then_thee() {
        when(dataSource.getBindings()).thenReturn(Sets.newSet("item.subElement.subElement.name", "item.id"));
        BindingMetadata idMetadata = new BindingMetadata();
        idMetadata.setPath("id");
        StringPropertyConverter idPropertyConverter = mock(StringPropertyConverter.class);
        when(idPropertyConverter.convertTo(any(Integer.class))).then(invocation -> invocation.getArgument(0).toString());
        idMetadata.setConverter(idPropertyConverter);
        idMetadata.setPropertyType(Integer.class);
        when(metadataProvider.getMetadataForPath("id")).thenReturn(idMetadata);

        DataResponse dataResponse = assertDoesNotThrow(() -> testObj.convertToDataResponse(Collections.singletonList(TestElement.builder()
            .id(2)
            .name("element2")
            .subElement(null).build())));

        assertNotNull(dataResponse);
        assertAll(
            () -> assertNotNull(dataResponse),
            () -> assertNotNull(dataResponse.getResponse())
        );
    }

    @Test
    @DisplayName("Should add item index if the datasource is ordered")
    void test_should_add_item_index_if_the_datasource_is_ordered() {
        List<TestElement> testElements = Arrays.asList(
            TestElement.builder().id(0).name("Element 1").build(),
            TestElement.builder().id(1).name("Element 2").build(),
            TestElement.builder().id(2).name("Element 3").build(),
            TestElement.builder().id(3).name("Element 4").build(),
            TestElement.builder().id(4).name("Element 5").build(),
            TestElement.builder().id(5).name("Element 6").build());

        Map<TestElement, Integer> indexMap = new HashMap<>();
        for (int i = 0; i < testElements.size(); i++) {
            indexMap.put(testElements.get(i), i);
        }
        when(dataSource.getIndexOfItems()).thenReturn(Optional.of(indexMap));

        TestElement testElement = testElements.get(2);
        JsonObject jsonObject = testObj.convertToKey(testElement);

        assertNotNull(jsonObject);
        assertEquals(2, (int) jsonObject.getNumber("___ITEM_IDX"));
    }

    @Tag("test-has-enabled-component")
    public static class TestHasEnabledComponent extends Component implements HasEnabled {

        private boolean internalEnabled;

        TestHasEnabledComponent(boolean internalEnabled) {
            this.internalEnabled = internalEnabled;
        }

        @Override
        public void setEnabled(boolean enabled) {
            internalEnabled = enabled;
        }

        @Override
        public boolean isEnabled() {
            return internalEnabled;
        }
    }

    @Tag("test-item-predicate-provider-component")
    public static class TestItemPredicateProviderComponent<T> extends Component implements ItemPredicateProvider<T> {

        private final Predicate<T> predicate;

        TestItemPredicateProviderComponent(Predicate<T> predicate) {
            this.predicate = predicate;
        }

        @Override
        public Predicate<T> getItemPredicate() {
            return predicate;
        }
    }
}
