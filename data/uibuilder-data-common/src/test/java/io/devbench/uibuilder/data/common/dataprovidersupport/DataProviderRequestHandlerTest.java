/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.*;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataProviderRequest;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataRequestBody;
import io.devbench.uibuilder.data.common.dataprovidersupport.requestresponse.DataResponse;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.slf4j.Logger;

import java.io.*;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, ResourceReaderExtension.class})
class DataProviderRequestHandlerTest {

    public static final String MINIMAL_RESPONSE_STRING = "[{'testpath':1}]";
    public static final String TEST_CSRF_ID = "test-csrf-id";

    @Mock
    private DataProviderEndpointManager dataProviderEndpointManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private VaadinSession session;

    @Mock
    private VaadinRequest request;

    @Mock
    private VaadinResponse response;

    @Mock
    private DataResponse dataResponse;

    @Mock
    private OutputStream mockOutputStream;

    @Mock
    private Logger mockLog;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private UIContext uiContext;

    private DataProviderRequestHandler testObj;

    @BeforeEach
    private void setup() throws IOException {
        UI currentUI = UI.getCurrent();
        testObj = spy(new DataProviderRequestHandler());
        when(testObj.getLog()).thenReturn(mockLog);
        when(response.getOutputStream()).thenReturn(mockOutputStream);
        when(VaadinSession.getCurrent().getService().getDeploymentConfiguration().isXsrfProtectionEnabled()).thenReturn(true);
        when(UI.getCurrent().getCsrfToken()).thenReturn(TEST_CSRF_ID);
        when(VaadinSession.getCurrent().getService().findUI(request)).thenReturn(currentUI);
        when(session.getAttribute("UIBuilderSessionContext")).thenReturn(uiContext);
        doReturn(dataProviderEndpointManager).when(uiContext).computeIfAbsent(eq(DataProviderEndpointManager.class), any());
    }

    @Test
    @DisplayName("Should handle fetch data requests, sent to datasource endpoints, and propagate the parsed versions of them to the data source endpoint manager")
    public void should_handle_fetch_data_requests_sent_to_datasource_endpoints_and_propagate_the_parsed_versions_of_them_to_the_data_source_endpoint_manager(
        @ReadResource("/example-fetch-data-request.json") final String exampleDataRequestString
    ) throws IOException {
        setupRequestToValidDataSourceEndpoint();
        setupRequestToValidFetchDataRequest();
        setupMinimalDataResponse();

        boolean handled = testObj.handleRequest(VaadinSession.getCurrent(), request, response);

        ArgumentCaptor<DataProviderRequest> dataRequestCaptor = ArgumentCaptor.forClass(DataProviderRequest.class);
        verify(dataProviderEndpointManager).processFetchDataRequest(dataRequestCaptor.capture());

        verify(response).setContentType("application/json; charset=UTF-8");
        verify(response).setContentLength(MINIMAL_RESPONSE_STRING.getBytes().length);
        verify(mockOutputStream).write(MINIMAL_RESPONSE_STRING.getBytes());
        verify(mockOutputStream).flush();

        DataProviderRequest dataProviderRequest = dataRequestCaptor.getValue();

        DataRequestBody requestBody = dataProviderRequest.getBody();

        assertAll(
            () -> assertTrue(handled),
            () -> assertEquals("test-ds-endpoint", dataProviderRequest.getEndpointId()),
            () -> assertEquals(DataProviderRequest.RequestType.FETCH_DATA, dataProviderRequest.getRequestType()),
            () -> assertNotNull(requestBody),
            () -> assertIntToIntegerEquals(1, requestBody.getPage()),
            () -> assertIntToIntegerEquals(30, requestBody.getPageSize()),
            () -> assertNotNull(requestBody.getFilters()),
            () -> assertEquals(2, requestBody.getFilters().size()),
            () -> assertEquals("testFilterPath", requestBody.getFilters().get(0).getPath()),
            () -> assertEquals("testValue", requestBody.getFilters().get(0).getValue()),
            () -> assertEquals("testFilterPath2", requestBody.getFilters().get(1).getPath()),
            () -> assertEquals("testValue", requestBody.getFilters().get(1).getValue()),
            () -> assertNotNull(requestBody.getSortOrders()),
            () -> assertEquals(2, requestBody.getSortOrders().size()),
            () -> assertEquals("testSortPath", requestBody.getSortOrders().get(0).getPath()),
            () -> assertEquals(SortOrder.Direction.ASCENDING, requestBody.getSortOrders().get(0).getDirection()),
            () -> assertEquals("testSortPath2", requestBody.getSortOrders().get(1).getPath()),
            () -> assertEquals(SortOrder.Direction.DESCENDING, requestBody.getSortOrders().get(1).getDirection()),
            () -> assertTrue(requestBody.getParentItem() instanceof JsonObject),
            () -> assertTrue(((JsonObject) requestBody.getParentItem()).hasKey("testItemPath")),
            () -> assertEquals("testItemPathValue", ((JsonObject) requestBody.getParentItem()).get("testItemPath").asString())
        );
    }

    @Test
    @DisplayName("Should handle fetch size requests and propaget them to the DataSourceEndpointManager")
    public void should_handle_fetch_size_requests_and_propaget_them_to_the_data_source_endpoint_manager() throws IOException {
        setupRequestToValidDataSourceEndpoint();
        setupRequestToValidFetchSizeRequest();

        when(dataProviderEndpointManager.processFetchSizeRequest(ArgumentMatchers.any(DataProviderRequest.class))).thenReturn(10L);

        boolean handled = testObj.handleRequest(VaadinSession.getCurrent(), request, response);

        verify(dataProviderEndpointManager).processFetchSizeRequest(ArgumentMatchers.any(DataProviderRequest.class));
        verify(response).setContentType("text/plain");
        verify(response).setContentLength("10".getBytes().length);
        verify(mockOutputStream).write("10".getBytes());
        verify(mockOutputStream).flush();

        assertTrue(handled);
    }

    @Test
    @DisplayName("Should reject requests if the csrfId is invalid")
    public void should_reject_requests_if_the_csrf_id_is_invalid() throws IOException {
        setupRequestToValidDataSourceEndpoint();
        setupRequestToInvalidCsrfFetchDataRequest();

        when(request.getRemoteHost()).thenReturn("test_host");

        boolean handled = testObj.handleRequest(VaadinSession.getCurrent(), request, response);

        verify(VaadinSession.getCurrent(), atLeastOnce()).lock();
        verify(mockLog).warn("Invalid security key received from {}, on endpoint: {}", "test_host", "/datasource-api/test-ds-endpoint");
        verify(VaadinSession.getCurrent(), atLeastOnce()).unlock();

        verify(response).setContentType("text/plain");
        verify(response).setContentLength("WRONG CSRF TOKEN".getBytes().length);
        verify(mockOutputStream).write("WRONG CSRF TOKEN".getBytes());
        verify(mockOutputStream).flush();

        assertTrue(handled);
    }

    @Test
    @DisplayName("Should handle error")
    public void should_handle_error() throws Exception {
        doAnswer(invocationOnMock -> {
            Command command = invocationOnMock.getArgument(0);
            command.execute();
            return mock(Future.class);
        }).when(UI.getCurrent()).access(Mockito.any());

        AtomicBoolean errorHandlerCalled = new AtomicBoolean(false);
        ErrorHandler errorHandler = event -> {
            errorHandlerCalled.set(true);
            assertNotNull(event.getThrowable());
            assertTrue(event.getThrowable() instanceof IllegalStateException);
            assertEquals("Test exception", event.getThrowable().getMessage());
        };

        when(VaadinSession.getCurrent().getErrorHandler()).thenReturn(errorHandler);

        IllegalStateException ex = new IllegalStateException("Test exception");
        setupRequestToValidDataSourceEndpoint();
        setupRequestToValidFetchDataRequest();
        setupException(ex);

        when(request.getRemoteHost()).thenReturn("test_host");

        boolean handled = testObj.handleRequest(VaadinSession.getCurrent(), request, response);

        assertTrue(errorHandlerCalled.get(), "Session error handler should be called");
        assertTrue(handled);
    }

    private void setupException(Throwable throwable) {
        when(dataProviderEndpointManager.processFetchDataRequest(ArgumentMatchers.any())).thenThrow(throwable);
    }

    private void setupMinimalDataResponse() {
        when(dataProviderEndpointManager.processFetchDataRequest(ArgumentMatchers.any())).thenReturn(dataResponse);
        when(dataResponse.asString()).thenReturn(MINIMAL_RESPONSE_STRING);
    }

    private void setupRequestToValidDataSourceEndpoint() {
        when(request.getPathInfo()).thenReturn("/datasource-api/test-ds-endpoint");
    }

    private void setupRequestToValidFetchDataRequest() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/example-fetch-data-request.json");
        when(request.getInputStream()).thenReturn(inputStream);
        when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(inputStream)));
    }

    private void setupRequestToInvalidCsrfFetchDataRequest() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/example-invalid-csrf-fetch-data-request.json");
        when(request.getInputStream()).thenReturn(inputStream);
        when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(inputStream)));
    }

    private void setupRequestToValidFetchSizeRequest() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/example-fetch-size-request.json");
        when(request.getInputStream()).thenReturn(inputStream);
        when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(inputStream)));
    }

    private void assertIntToIntegerEquals(int expected, Integer actual) {
        assertEquals(Integer.valueOf(expected), actual);
    }

}
