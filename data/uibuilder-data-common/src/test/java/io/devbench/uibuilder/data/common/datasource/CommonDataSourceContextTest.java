/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.datasource;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.data.common.exceptions.DataSourceNotActiveException;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collections;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommonDataSourceContextTest {

    @Mock
    private CommonDataSource<?, ?, ?, ?> dataSource1;

    @Mock
    private CommonDataSource<?, ?, ?, ?> dataSource2;

    @Mock
    private Supplier<CommonDataSource> dataSourceSupplier;

    @Mock
    private Component component;

    @Mock
    private CommonDataSourceReferenceHolder<CommonDataSource> referenceHolder1;

    @Mock
    private CommonDataSourceReferenceHolder<CommonDataSource> referenceHolder2;

    private CommonDataSourceContext<CommonDataSource> testObj;

    @BeforeEach
    private void setup() {
        when(referenceHolder1.getDataSource()).thenReturn(dataSource1);
        when(referenceHolder2.getDataSource()).thenReturn(dataSource2);
        when(referenceHolder1.containsReferences()).thenReturn(true);
        when(referenceHolder2.containsReferences()).thenReturn(true);
        when(dataSourceSupplier.get())
            .thenReturn(dataSource1)
            .thenReturn(dataSource2);
        testObj = spy(new TestCommonDataSourceContext());
    }

    @Test
    @DisplayName("Should call the supplier, and create a reference holder with the data source from the supplier to store it")
    public void should_call_the_supplier_and_create_a_reference_holder_with_the_data_source_from_the_supplier_to_store_it() {
        CommonDataSource dataSource = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        verify(testObj).createReferenceHolder(any(Component.class), any(CommonDataSource.class));
        verify(dataSourceSupplier).get();

        assertSame(dataSource1, dataSource);
    }

    @Test
    @DisplayName("Should return different data sources, if the data source name changes")
    public void should_return_different_data_sources_if_the_data_source_name_changes() {
        CommonDataSource result = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);
        CommonDataSource result2 = testObj.findDataSource("testDS2", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        assertNotSame(result, result2);
    }

    @Test
    @DisplayName("Should return different data sources, if the query name changes")
    public void should_return_different_data_sources_if_the_query_name_changes() {
        CommonDataSource result = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);
        CommonDataSource result2 = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery2", component), dataSourceSupplier);

        assertNotSame(result, result2);
    }

    @Test
    @DisplayName("Should return the same data source, if the data source name, and query name is the same, and the reference holder says it can hold the reference of the component")
    public void should_return_the_same_data_source_if_the_data_source_name_and_query_name_is_the_same_and_the_reference_holder_says_it_can_hold_the_reference_of_the_component() {
        when(referenceHolder1.canHoldReference(component)).thenReturn(true);

        CommonDataSource result = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);
        CommonDataSource result2 = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        verify(referenceHolder1).canHoldReference(component);
        verify(referenceHolder1, times(2)).registerReference(component);

        assertSame(result, result2);
    }

    @Test
    @DisplayName("Should return different data source, when the reference holder says it cannot hold the reference for the component")
    public void should_return_different_data_source_when_the_reference_holder_says_it_cannot_hold_the_reference_for_the_component() {
        when(referenceHolder1.canHoldReference(component)).thenReturn(false);

        CommonDataSource result = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);
        CommonDataSource result2 = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        verify(referenceHolder1).canHoldReference(component);
        verify(referenceHolder1).registerReference(component);
        verify(referenceHolder2).registerReference(component);

        assertNotSame(result, result2);
    }

    @RepeatedTest(10)
    @DisplayName("Should return different data sources, when the reference holder says it cannot hold the reference, but is should get the same data source when asked again")
    public void should_return_different_data_sources_when_the_reference_holder_says_it_cannot_hold_the_reference_but_is_should_get_the_same_data_source_when_asked_again() {
        when(referenceHolder1.canHoldReference(component)).thenReturn(false);
        when(referenceHolder2.canHoldReference(component)).thenReturn(true);

        CommonDataSource result = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);
        CommonDataSource result2 = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        CommonDataSource result3 = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        verify(referenceHolder1, times(2)).canHoldReference(component);
        verify(referenceHolder1).registerReference(component);
        verify(referenceHolder2, times(2)).registerReference(component);
        verify(referenceHolder2).canHoldReference(component);

        assertAll(
            () -> assertNotSame(result, result2),
            () -> assertSame(result2, result3)
        );
    }

    @Test
    @DisplayName("Should cleanup reference holders, when they do not hold real references anymore. (in a thread)")
    public void should_cleanup_reference_holders_when_they_do_not_hold_real_references_anymore() throws InterruptedException {
        when(referenceHolder1.canHoldReference(component)).thenReturn(false);

        testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        assertAll(
            () -> assertEquals(1, testObj.getReferenceHoldersMap().size()),
            () -> assertEquals(1, testObj.getReferenceHoldersMap().get("testDStestQuery").size())
        );

        when(referenceHolder1.containsReferences()).thenReturn(false);
        testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        waitForCleanupDeamon();

        assertAll(
            () -> assertEquals(1, testObj.getReferenceHoldersMap().size()),
            () -> assertEquals(1, testObj.getReferenceHoldersMap().get("testDStestQuery").size())
        );

        when(referenceHolder2.containsReferences()).thenReturn(false);
        testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), dataSourceSupplier);

        waitForCleanupDeamon();

        assertEquals(0, testObj.getReferenceHoldersMap().size());
    }

    @Test
    @DisplayName("Should notify all notifiable components registered to the data source, when refresh requested")
    public void should_notify_all_notifiable_components_registered_to_the_data_source_when_refresh_requested() {
        testObj.getReferenceHoldersMap().put("testDStestQuery", Collections.singletonList(referenceHolder1));

        when(referenceHolder1.containsReferences()).thenReturn(true);
        when(referenceHolder1.hasReferenceTo(component)).thenReturn(true);

        testObj.refreshRequestedForDataSource("testDS", new CommonDataSourceSelector("testQuery", component));

        verify(referenceHolder1).hasReferenceTo(component);
        verify(referenceHolder1).notifyReferencesAboutRefresh();
    }

    @Test
    @DisplayName("Should throw exception when refresh requested to a data source, that is nor in the refeerence holder map")
    public void should_throw_exception_when_refresh_requested_to_a_data_source_that_is_nor_in_the_refeerence_holder_map() {
        assertThrows(
            DataSourceNotActiveException.class,
            () -> testObj.refreshRequestedForDataSource("testDS", new CommonDataSourceSelector("testQuery", component))
        );
    }

    @Test
    @DisplayName("Should return different data sources for the same name, if the datasource was changed in the meantime")
    public void should_return_different_data_sources_if_the_it_was_replaced() {
        testObj = new CommonDataSourceContext<>();

        CommonDataSource dataSource1 = mock(CommonDataSource.class);
        CommonDataSource dataSource2 = mock(CommonDataSource.class);

        CommonDataSource result1 = testObj.findDataSource("testDS", new CommonDataSourceSelector("testQuery", component), () -> dataSource1);
        CommonDataSource result2 = testObj.replaceDataSource("testDS", new CommonDataSourceSelector("testQuery", component), () -> dataSource2);

        assertNotSame(result1, result2);
    }

    private void waitForCleanupDeamon() throws InterruptedException {
        do {
            Thread.sleep(5L);
        } while (CommonDataSourceContext.CLEANUP_THREAD_POOL_EXECUTOR.getActiveCount() != 0);
    }


    private class TestCommonDataSourceContext extends CommonDataSourceContext<CommonDataSource> {

        @Override
        CommonDataSourceReferenceHolder<CommonDataSource> createReferenceHolder(Component component, CommonDataSource dataSource) {
            if (dataSource == dataSource1) {
                return referenceHolder1;
            } else {
                return referenceHolder2;
            }
        }
    }

}
