/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.dom.Element;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProcessor;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderEndpointManager;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.InlineEditHandler;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.data.common.exceptions.DataSourceNotFoundByEndpointException;
import io.devbench.uibuilder.data.common.exceptions.InlineSaveFailedException;
import io.devbench.uibuilder.data.common.item.ItemState;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.io.Serializable;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, BaseUIBuilderTestExtension.class})
class AbstractGenericGridInlineEditorControllerBeanTest {

    private AbstractGenericGridInlineEditorControllerBean<TestEntity> testObj;

    @Mock
    private DataProviderEndpointManager dataProviderEndpointManager;

    @Mock
    private InlineEditHandler inlineEditHandler;

    @Mock
    private CommonDataSource<TestEntity, ?, ?, ?> commonDataSource;

    @Mock
    private DataProcessor<TestEntity> dataProcessor;

    @BeforeEach
    void setUp() {
        UIContext.getContext().addToActiveUIContextAs(DataProviderEndpointManager.class, dataProviderEndpointManager);
        when(commonDataSource.getDataProcessor()).thenReturn(dataProcessor);
        when(dataProcessor.getInlineEditHandler()).thenReturn(inlineEditHandler);

        testObj = new TestGenericGridInlineEditorControllerBean();
    }

    private void mockCommonDatasourceForItem(@NotNull JsonObject item) {
        doReturn(Optional.of(commonDataSource)).when(dataProviderEndpointManager).getDataSourceByItemKey(item);
    }

    private void mockComponentForItem(@NotNull JsonObject item, @Nullable Component component) {
        doReturn(Optional.ofNullable(component)).when(dataProviderEndpointManager).getComponent("test-endpoint-id");
    }

    @NotNull
    private JsonObject createJsonItem(@NotNull String key) {
        JsonObject item = Json.createObject();
        item.put(DataProcessor.ITEM_KEY, key);
        item.put(DataProcessor.ITEM_ENDPOINT_ID, "test-endpoint-id");
        return item;
    }

    @NotNull
    private Component mockComponentWithElement() {
        Component component = mock(Component.class);
        Element componentElement = mock(Element.class);
        doReturn(componentElement).when(component).getElement();
        return component;
    }

    private void verifyItemRefresh(@NotNull Component component, @NotNull JsonObject item) {
        Element componentElement = component.getElement();
        verify(componentElement).callJsFunction(eq("_refreshItem"), same(item));
    }

    @Test
    @DisplayName("Should throw exception is datasource cannot be found")
    void test_should_throw_exception_is_datasource_cannot_be_found() {
        assertThrows(DataSourceNotFoundByEndpointException.class,
            () -> testObj.getInlineEditHandler(createJsonItem("key")));
    }

    @Test
    @DisplayName("Should set edit mode")
    void test_should_set_edit_mode() {
        JsonObject item = createJsonItem("key");
        Component component = mockComponentWithElement();
        mockCommonDatasourceForItem(item);
        mockComponentForItem(item, component);

        testObj.setEditMode(item, true);

        verifyItemRefresh(component, item);
        verify(inlineEditHandler).setEditMode(same(item), eq(true));
    }

    @Test
    @DisplayName("Should handle property change")
    void test_should_handle_property_change() {
        TestEntity item = new TestEntity();
        item.setName("Original name");

        JsonObject jsonItem = createJsonItem("key");
        jsonItem.put("name", "Modified name");

        Component component = mockComponentWithElement();
        mockCommonDatasourceForItem(jsonItem);
        mockComponentForItem(jsonItem, component);

        ItemState itemState = mock(ItemState.class);
        doReturn(itemState).when(itemState).validate(any(Set.class), any(Serializable.class));
        doReturn(itemState).when(inlineEditHandler).getItemState(jsonItem);

        testObj.onInlineItemValueChange(item, jsonItem);

        verify(inlineEditHandler).registerPropertyChange(jsonItem);
        verify(inlineEditHandler).applyChanges(argThat(argument -> argument instanceof TestEntity && argument != item), same(jsonItem));
        verify(inlineEditHandler).getChangedProperties(jsonItem);
        verify(itemState).validate(anySet(), argThat(argument -> argument instanceof TestEntity && argument != item));
        verify(itemState).setSaveAllowed(anyBoolean());
        verifyItemRefresh(component, jsonItem);
    }

    @Test
    @DisplayName("Should handle save")
    void test_should_handle_save() {
        TestEntity item = new TestEntity();
        JsonObject jsonItem = createJsonItem("key");
        Component component = mockComponentWithElement();
        mockComponentForItem(jsonItem, component);
        mockCommonDatasourceForItem(jsonItem);

        testObj.onInlineItemSave(item, jsonItem);

        verify(inlineEditHandler).applyChanges(same(item), same(jsonItem));
        verify(inlineEditHandler).setEditMode(same(jsonItem), eq(false));

        TestEntity inlineSaveItem = ((TestGenericGridInlineEditorControllerBean) testObj).getInlineSaveItem();
        Component inlineSaveComponent = ((TestGenericGridInlineEditorControllerBean) testObj).getInlineSaveComponent();

        assertNotNull(inlineSaveItem);
        assertNotNull(inlineSaveComponent);
        assertSame(item, inlineSaveItem);
        assertSame(component, inlineSaveComponent);
    }

    @Test
    @DisplayName("Should throw exception if the save process failed")
    void test_should_throw_exception_if_the_save_process_failed() {
        TestEntity item = new TestEntity();
        JsonObject jsonItem = createJsonItem("key");
        mockCommonDatasourceForItem(jsonItem);

        RuntimeException testException = new RuntimeException();
        ((TestGenericGridInlineEditorControllerBean) testObj).setExceptionToThrowWhenSave(testException);

        InlineSaveFailedException exception = assertThrows(InlineSaveFailedException.class, () -> testObj.onInlineItemSave(item, jsonItem));
        assertSame(testException, exception.getCause());
    }

    @Data
    @NoArgsConstructor
    static class TestEntity implements Serializable {
        private String name;
    }

    static class TestGenericGridInlineEditorControllerBean extends AbstractGenericGridInlineEditorControllerBean<TestEntity> {

        @Setter
        private RuntimeException exceptionToThrowWhenSave = null;

        @Getter
        private TestEntity inlineSaveItem = null;

        @Getter
        private Component inlineSaveComponent = null;

        @Override
        public void onInlineItemSave(@NotNull TestEntity modifiedItem, @NotNull JsonObject modifiedJsonItem, @Nullable Component component) {
            if (exceptionToThrowWhenSave != null) {
                throw exceptionToThrowWhenSave;
            }
            inlineSaveItem = modifiedItem;
            inlineSaveComponent = component;
        }

        @Override
        public void onInlineItemCancel(JsonObject jsonItem) {
            super.onInlineItemCancel(jsonItem);
        }

        @Override
        public void onInlineItemEdit(JsonObject jsonItem) {
            super.onInlineItemEdit(jsonItem);
        }
    }

}
