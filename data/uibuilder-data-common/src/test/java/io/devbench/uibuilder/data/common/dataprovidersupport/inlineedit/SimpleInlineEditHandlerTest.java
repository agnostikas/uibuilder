/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit;

import elemental.json.Json;
import elemental.json.JsonObject;
import elemental.json.JsonString;
import elemental.json.JsonValue;
import elemental.json.impl.JreJsonNumber;
import elemental.json.impl.JreJsonString;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProcessor;
import io.devbench.uibuilder.data.common.item.ItemState;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class SimpleInlineEditHandlerTest {

    private SimpleInlineEditHandler testObj;

    @BeforeEach
    void setUp() {
        testObj = new SimpleInlineEditHandler();
    }

    @Test
    @DisplayName("New item state should be created with default edit mode")
    void test_new_item_state_should_be_created_with_default_edit_mode() {
        Boolean editModeDefault = testObj.getEditModeDefault();

        ItemState itemStateNotInEditMode = testObj.getItemState("key1");

        testObj.setEditModeDefault(true);
        ItemState itemStateInEditMode = testObj.getItemState("key2");

        assertTrue(testObj.getEditModeDefault());
        assertFalse(editModeDefault);
        assertFalse(itemStateNotInEditMode.isEditMode());
        assertTrue(itemStateInEditMode.isEditMode());
    }

    @Test
    @DisplayName("Should set item state for the key, and remove it if the state is null")
    void test_should_set_item_state_for_the_key_and_remove_it_if_the_state_is_null() {
        ItemState itemState = new ItemState();

        testObj.setItemState("key", itemState);
        ItemState setItemState = testObj.getItemState("key");
        testObj.setItemState("key", null);
        ItemState newlyCreatedItemState = testObj.getItemState("key");

        assertNotNull(setItemState);
        assertSame(itemState, setItemState);
        assertNotNull(newlyCreatedItemState);
        assertNotSame(itemState, newlyCreatedItemState);
    }

    @Test
    @DisplayName("Should register changed properties for a key")
    void test_should_register_changed_properties_for_a_key() {
        JsonObject jsonItem = createJsonItem("key");

        Set<String> emptyChangedProperties = testObj.getChangedProperties(jsonItem);
        JsonValue nullValue = testObj.getChangedPropertyValue(jsonItem, "name");

        assertNotNull(emptyChangedProperties);
        assertTrue(emptyChangedProperties.isEmpty());
        assertNull(nullValue);

        testObj.registerPropertyChange(jsonItem, "name", new JreJsonString("changed value"));

        Set<String> changedProperties = testObj.getChangedProperties(jsonItem);
        JsonValue changedValue = testObj.getChangedPropertyValue(jsonItem, "name");

        assertNotNull(changedProperties);
        assertEquals(1, changedProperties.size());
        assertIterableEquals(Collections.singleton("name"), changedProperties);
        assertTrue(testObj.hasChangedPropertyValue(jsonItem, "name"));
        assertTrue(changedValue instanceof JsonString);
        assertEquals("changed value", changedValue.asString());
    }

    @Test
    @DisplayName("Should set and clear edit mode for the specified key")
    void test_should_set_and_clear_edit_mode_for_the_specified_key() {
        JsonObject keyItem = createJsonItem("key");

        assertFalse(testObj.isEditMode("key"));

        testObj.setEditMode(keyItem, true);
        assertTrue(testObj.isEditMode("key"));

        testObj.clearEditMode(keyItem);
        assertFalse(testObj.isEditMode(keyItem));

        testObj.clearEditMode(keyItem); // to remove the auto-created state
        testObj.setEditModeDefault(true);
        assertTrue(testObj.isEditMode(keyItem));

        testObj.setEditMode("key", false);
        assertFalse(testObj.isEditMode(keyItem));

        testObj.clearEditMode("key");
        assertTrue(testObj.isEditMode(keyItem));
    }

    @Test
    @DisplayName("Should clear all edit modes in the current instance")
    void test_should_clear_all_edit_modes_in_the_current_instance() {
        testObj.setEditMode("key1", false);
        testObj.setEditMode("key2", true);
        testObj.setEditMode("key3", true);

        assertAll(
            () -> assertFalse(testObj.isEditMode("key1")),
            () -> assertTrue(testObj.isEditMode("key2")),
            () -> assertTrue(testObj.isEditMode("key3"))
        );

        testObj.clearEditModes();

        assertAll(
            () -> assertFalse(testObj.isEditMode("key1")),
            () -> assertFalse(testObj.isEditMode("key2")),
            () -> assertFalse(testObj.isEditMode("key3"))
        );
    }

    @Test
    @DisplayName("Settings edit mode to false should remove all registered property changes")
    void test_settings_edit_mode_to_false_should_remove_all_registered_property_changes() {
        JsonObject item = createJsonItem("key");
        testObj.setEditMode(item, true);
        testObj.registerPropertyChange(item, "name", new JreJsonString("changed"));

        assertTrue(testObj.hasChangedPropertyValue(item, "name"));

        testObj.setEditMode(item, false);

        assertFalse(testObj.hasChangedPropertyValue(item, "name"));
        assertTrue(testObj.getChangedProperties(item).isEmpty());
    }

    @Test
    @DisplayName("Should remove registered changed property")
    void test_should_remove_registered_changed_property() {
        JsonObject item = createJsonItem("key");
        testObj.setEditMode(item, true);
        testObj.registerPropertyChange(item, "name", new JreJsonString("changed"));
        testObj.registerPropertyChange(item, "age", new JreJsonNumber(20));

        assertTrue(testObj.hasChangedPropertyValue(item, "name"));
        assertTrue(testObj.hasChangedPropertyValue(item, "age"));

        testObj.removeChangedProperty(item, "age");

        assertTrue(testObj.hasChangedPropertyValue(item, "name"));
        assertFalse(testObj.hasChangedPropertyValue(item, "age"));

        testObj.removePropertyChanges(item);

        assertFalse(testObj.hasChangedPropertyValue(item, "name"));
        assertFalse(testObj.hasChangedPropertyValue(item, "age"));
    }

    @Test
    @DisplayName("Should iterate all edited items")
    void test_should_iterate_all_edited_items() {
        JsonObject item1 = createJsonItem("key1");
        JsonObject item2 = createJsonItem("key2");
        JsonObject item3 = createJsonItem("key3");
        JsonObject item4 = createJsonItem("key4");

        testObj.setEditMode(item1, false);
        testObj.setEditMode(item2, true);
        testObj.setEditMode(item3, true);
        testObj.setEditMode(item4, true);

        testObj.registerPropertyChange(item3, "name", new JreJsonString("item 3 modified name"));
        testObj.registerPropertyChange(item4, "age", new JreJsonString("21"));

        List<JsonObject> editedItems = testObj.editedItems().collect(Collectors.toList());
        assertEquals(2, editedItems.size());
        assertFalse(editedItems.contains(item1));
        assertFalse(editedItems.contains(item2));
        assertTrue(editedItems.contains(item3));
        assertTrue(editedItems.contains(item4));
    }

    @Test
    @DisplayName("Should iterate all property changed for specified item")
    void test_should_iterate_all_property_changed_for_specified_item() {
        JsonObject item = createJsonItem("key");
        testObj.setEditMode(item, true);
        testObj.registerPropertyChange(item, "name", new JreJsonString("item name modified"));
        testObj.registerPropertyChange(item, "age", new JreJsonString("22"));

        Map<String, String> modifiedValues = new HashMap<>();
        testObj.forChangedProperty(item, (s, jsonValue) -> modifiedValues.put(s, jsonValue.asString()));

        assertEquals(2, modifiedValues.size());
        assertEquals("item name modified", modifiedValues.get("name"));
        assertEquals("22", modifiedValues.get("age"));
    }

    @NotNull
    private JsonObject createJsonItem(@NotNull String key) {
        JsonObject jsonItem = Json.createObject();
        jsonItem.put(DataProcessor.ITEM_KEY, key);
        return jsonItem;
    }

}
