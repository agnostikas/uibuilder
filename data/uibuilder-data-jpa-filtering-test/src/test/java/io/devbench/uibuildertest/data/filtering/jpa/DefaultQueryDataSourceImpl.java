/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuildertest.data.filtering.jpa;

import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuildertest.data.model.QTestEntity;
import io.devbench.uibuildertest.data.model.TestEntity;

public class DefaultQueryDataSourceImpl implements DefaultQueryDataSource {
    @Override
    public JPAQuery<TestEntity> testDefaultQuery() {
        final JPAQuery<TestEntity> query = new JPAQuery<>();
        return query.from(QTestEntity.testEntity).where(QTestEntity.testEntity.text.eq("test"));
    }
}
