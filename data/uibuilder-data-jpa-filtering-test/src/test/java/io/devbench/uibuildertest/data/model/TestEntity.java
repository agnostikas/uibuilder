/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuildertest.data.model;

import lombok.*;
import org.springframework.data.domain.Persistable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Table
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class TestEntity implements Persistable<String> {

    @Id
    private String id;

    private String text;

    private LocalDate localDate;

    private Instant instant;

    private LocalDateTime localDateTime;

    private Character character;

    private Long longValue;

    private Float floatValue;

    private Double doubleValue;

    private BigInteger bigInteger;

    private BigDecimal bigDecimal;

    private Integer integer;

    private Byte byteValue;

    private Short shortValue;

    @Override
    public boolean isNew() {
        return getId() == null;
    }

    @PrePersist
    public void prePersist() {
        id = UUID.randomUUID().toString();
    }
}
