/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuildertest.data.filtering.jpa;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.Constants;
import io.devbench.uibuilder.annotations.EnableUIBuilder;
import io.devbench.uibuilder.components.util.datasource.DataSourceBindingContext;
import io.devbench.uibuilder.data.api.datasource.DataSource;
import io.devbench.uibuilder.data.api.filter.FilterExpression;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.devbench.uibuilder.data.common.filter.logicaloperators.AndFilterExpression;
import io.devbench.uibuilder.data.common.filter.logicaloperators.NotFilterExpression;
import io.devbench.uibuilder.data.common.filter.logicaloperators.OrFilterExpression;
import io.devbench.uibuilder.spring.data.jpa.SpringJpaDataSourceProvider;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuildertest.data.filtering.config.JpaConfig;
import io.devbench.uibuildertest.data.model.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes.BaseLike.LikeExpressionType.*;
import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ComponentScan(value = "io.devbench")
@EnableUIBuilder("io.devbench")
@EnableJpaRepositories(
    value = "io.devbench",
    transactionManagerRef = "jpaTransactionManager"
)
@ContextConfiguration(classes = JpaConfig.class)
@ExtendWith({SpringExtension.class, BaseUIBuilderTestExtension.class})
// This property is necessary while the UIBuilder components are not entirely NPM based
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = "vaadin." + Constants.SERVLET_PARAMETER_COMPATIBILITY_MODE + "=true")
@EnableAutoConfiguration
public class JpaFilterIT {
    private static final int ENTITY_COUNT = 5;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private PagingAndSortingRepository<TestEntity, String> testRepository;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private SpringJpaDataSourceProvider springDataSourceProvider;

    private DataSource<List<TestEntity>, ?, FilterExpression<?>, PagingFetchRequest> dataSource;
    private FilterExpressionFactory<FilterExpression<?>> filterExpressionFactory;

    @BeforeEach
    void setup() {
        Component component = mock(Component.class);
        testRepository.deleteAll();
        initDb();
        initSession(component);
        dataSource = springDataSourceProvider.getDataSource("testDataSource", new CommonDataSourceSelector(null, component));
        filterExpressionFactory = dataSource.getFilterExpressionFactory();
    }

    private void initSession(Component component) {
        final Set<String> bindings = stream(TestEntity.class.getFields()).map(Field::getName).collect(Collectors.toSet());
        DataSourceBindingContext bindingContext = new DataSourceBindingContext();
        bindingContext.getBindings().addAll(bindings);
        bindingContext.setDataSourceId("testDataSource");
        bindingContext.setDataSourceName("testDataSource");

        springDataSourceProvider.registerBindingContextForDataSource(bindingContext, new CommonDataSourceSelector(null, component));
    }

    @Test
    void equals_filter_should_find_only_matching_entities() {
        ExpressionTypes.Equals filter = filterExpressionFactory.create(ExpressionTypes.Equals.class);
        filter.setPath("text");
        filter.setValue("Test 2");
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals(2L, data.get(0).getLongValue().longValue());
    }

    @Test
    void not_equals_filter_should_find_only_matching_entities() {
        ExpressionTypes.NotEquals filter = filterExpressionFactory.create(ExpressionTypes.NotEquals.class);
        filter.setPath("text");
        filter.setValue("Test 2");
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(ENTITY_COUNT - 1, data.size());
    }

    @Test
    void between_filter_should_find_only_matching_entities() {
        ExpressionTypes.Between filter = filterExpressionFactory.create(ExpressionTypes.Between.class);
        filter.setPath("integer");
        filter.setValues(asList(2, 4));
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(3, data.size());
        String text = data.stream()
            .map(TestEntity::getText)
            .sorted()
            .collect(Collectors.joining(";"));
        assertEquals("Test 2;Test 3;Test 4", text);
    }

    @Test
    void in_filter_should_find_only_matching_entities() {
        ExpressionTypes.In filter = filterExpressionFactory.create(ExpressionTypes.In.class);
        filter.setPath("integer");
        filter.setValues(asList(2, 4, -1));
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(2, data.size());
        String text = data.stream()
            .map(TestEntity::getText)
            .sorted()
            .collect(Collectors.joining(";"));
        assertEquals("Test 2;Test 4", text);
    }

    @Test
    void not_in_filter_should_find_only_matching_entities() {
        ExpressionTypes.NotIn filter = filterExpressionFactory.create(ExpressionTypes.NotIn.class);
        filter.setPath("integer");
        filter.setValues(asList(2, 4, -1));
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(3, data.size());
        String text = data.stream()
            .map(TestEntity::getText)
            .sorted()
            .collect(Collectors.joining(";"));
        assertEquals("Test 0;Test 1;Test 3", text);
    }

    @Test
    void greater_than_filter_should_find_only_matching_entities() {
        ExpressionTypes.GreaterThan filter = filterExpressionFactory.create(ExpressionTypes.GreaterThan.class);
        filter.setPath("integer");
        filter.setValue(3);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals("Test 4", data.get(0).getText());
    }

    @Test
    void greater_than_or_equals_filter_should_find_only_matching_entities() {
        ExpressionTypes.GreaterThanOrEquals filter = filterExpressionFactory.create(ExpressionTypes.GreaterThanOrEquals.class);
        filter.setPath("integer");
        filter.setValue(3);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(2, data.size());
        String text = data.stream()
            .map(TestEntity::getText)
            .sorted()
            .collect(Collectors.joining(";"));
        assertEquals("Test 3;Test 4", text);
    }

    @Test
    void less_than_filter_should_find_only_matching_entities() {
        ExpressionTypes.LessThan filter = filterExpressionFactory.create(ExpressionTypes.LessThan.class);
        filter.setPath("integer");
        filter.setValue(3);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(3, data.size());
        String text = data.stream()
            .map(TestEntity::getText)
            .sorted()
            .collect(Collectors.joining(";"));
        assertEquals("Test 0;Test 1;Test 2", text);
    }

    @Test
    void less_than_or_equals_filter_should_find_only_matching_entities() {
        ExpressionTypes.LessThanOrEquals filter = filterExpressionFactory.create(ExpressionTypes.LessThanOrEquals.class);
        filter.setPath("integer");
        filter.setValue(3);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(4, data.size());
        String text = data.stream()
            .map(TestEntity::getText)
            .sorted()
            .collect(Collectors.joining(";"));
        assertEquals("Test 0;Test 1;Test 2;Test 3", text);
    }

    @Test
    void starts_with_like_filter_should_find_only_matching_entities() {
        createEntity("Cat Dog Fish", 100);
        createEntity("Dog Fish Cat", 101);
        createEntity("Fish Cat Dog", 102);
        ExpressionTypes.Like filter = filterExpressionFactory.create(ExpressionTypes.Like.class);
        filter.setPath("text");
        filter.setValue("Dog");
        filter.setLikeExpressionType(STARTS_WITH);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals(101, data.get(0).getInteger().intValue());
    }

    @Test
    void ends_with_like_filter_should_find_only_matching_entities() {
        createEntity("Cat Dog Fish", 100);
        createEntity("Dog Fish Cat", 101);
        createEntity("Fish Cat Dog", 102);
        ExpressionTypes.Like filter = filterExpressionFactory.create(ExpressionTypes.Like.class);
        filter.setPath("text");
        filter.setValue("Dog");
        filter.setLikeExpressionType(ENDS_WITH);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals(102, data.get(0).getInteger().intValue());
    }

    @Test
    void contains_like_filter_should_find_only_matching_entities() {
        createEntity("Cat Dog Fish", 100);
        createEntity("Dog Fish Cat", 101);
        createEntity("Fish Cat Dog", 102);
        ExpressionTypes.Like filter = filterExpressionFactory.create(ExpressionTypes.Like.class);
        filter.setPath("text");
        filter.setValue("Dog");
        filter.setLikeExpressionType(CONTAINS);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(3, data.size());
    }

    @Test
    void none_like_filter_should_find_only_matching_entities() {
        createEntity("Cat Dog Fish", 100);
        createEntity("Dog Fish Cat", 101);
        createEntity("Fish Cat Dog", 102);
        createEntity("Dog", 103);
        ExpressionTypes.Like filter = filterExpressionFactory.create(ExpressionTypes.Like.class);
        filter.setPath("text");
        filter.setValue("Dog");
        filter.setLikeExpressionType(NONE);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals(103, data.get(0).getInteger().intValue());
    }

    @Test
    void not_filter_should_find_only_matching_entities() {
        NotFilterExpression filter = filterExpressionFactory.create(NotFilterExpression.class);
        ExpressionTypes.Equals equalsFilter = filterExpressionFactory.create(ExpressionTypes.Equals.class);
        equalsFilter.setPath("text");
        equalsFilter.setValue("Test 2");
        filter.setNegatedFilterExpression(equalsFilter);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(ENTITY_COUNT - 1, data.size());
    }

    @Test
    void and_filter_should_find_only_matching_entities() {
        createEntity("Test 2", 3);
        AndFilterExpression filter = filterExpressionFactory.create(AndFilterExpression.class);
        ExpressionTypes.Equals equals1 = filterExpressionFactory.create(ExpressionTypes.Equals.class);
        equals1.setPath("text");
        equals1.setValue("Test 2");
        ExpressionTypes.Equals equals2 = filterExpressionFactory.create(ExpressionTypes.Equals.class);
        equals2.setPath("integer");
        equals2.setValue(3);
        filter.add(equals1);
        filter.add(equals2);
        dataSource.registerFilter(filter);

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
    }

    @Test
    void or_filter_should_find_only_matching_entities() {
        createEntity("Test 2", 3);
        OrFilterExpression filter = filterExpressionFactory.create(OrFilterExpression.class);
        ExpressionTypes.Equals equals1 = filterExpressionFactory.create(ExpressionTypes.Equals.class);
        equals1.setPath("text");
        equals1.setValue("Test 2");
        ExpressionTypes.Equals equals2 = filterExpressionFactory.create(ExpressionTypes.Equals.class);
        equals2.setPath("integer");
        equals2.setValue(3);
        filter.add(equals1);
        filter.add(equals2);
        dataSource.registerFilter(wrapInAndFilter(filterExpressionFactory, filter));

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(3, data.size());
    }


    private PagingFetchRequest createFetchRequest() {
        return PagingFetchRequest.builder().page(0).pageSize(Integer.MAX_VALUE).build();
    }

    private void initDb() {
        IntStream.range(0, ENTITY_COUNT)
            .forEach(e -> createEntity("Test " + e, e));
    }

    private TestEntity createEntity(String text, int e) {
        return testRepository.save(TestEntity.builder()
            .text(text)
            .floatValue((float) e)
            .integer(e)
            .bigDecimal(BigDecimal.valueOf(e))
            .bigInteger(BigInteger.valueOf(e))
            .byteValue((byte) e)
            .longValue((long) e)
            .shortValue((short) e)
            .doubleValue((double) e)
            .build());
    }

    private AndFilterExpression wrapInAndFilter(FilterExpressionFactory<FilterExpression<?>> filterExpressionFactory, FilterExpression<?> filterExpression) {
        AndFilterExpression andFilterExpression = filterExpressionFactory.create(AndFilterExpression.class);
        andFilterExpression.add(filterExpression);
        return andFilterExpression;
    }
}
