/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuildertest.data.filtering.jpa;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.Constants;
import io.devbench.uibuilder.annotations.EnableUIBuilder;
import io.devbench.uibuilder.components.util.datasource.DataSourceBindingContext;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.common.datasource.BaseDataSourceBindingContext;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import io.devbench.uibuilder.data.jpa.filterexpressions.JpaFilterExpression;
import io.devbench.uibuilder.spring.data.jpa.SpringJpaDataSource;
import io.devbench.uibuilder.spring.data.jpa.SpringJpaDataSourceProvider;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuildertest.data.filtering.config.JpaConfig;
import io.devbench.uibuildertest.data.model.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.internal.util.reflection.FieldSetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ComponentScan(value = "io.devbench")
@EnableUIBuilder("io.devbench")
@EnableJpaRepositories(
    value = "io.devbench",
    transactionManagerRef = "jpaTransactionManager"
)
@ContextConfiguration(classes = JpaConfig.class)
@ExtendWith({SpringExtension.class, BaseUIBuilderTestExtension.class})
// This property is necessary while the UIBuilder components are not entirely NPM based
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = "vaadin." + Constants.SERVLET_PARAMETER_COMPATIBILITY_MODE + "=true")
@EnableAutoConfiguration
public class JpaBaseDefaultQueryFilterIT {
    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private PagingAndSortingRepository<TestEntity, String> testRepository;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private SpringJpaDataSourceProvider<TestEntity> springDataSourceProvider;

    private SpringJpaDataSource<TestEntity> dataSource;
    private FilterExpressionFactory<JpaFilterExpression<?>> filterExpressionFactory;
    private Set<String> bindings;

    @BeforeEach
    void setup() throws NoSuchFieldException {
        testRepository.deleteAll();
        initSession();
        dataSource = springDataSourceProvider
            .getDataSource("testDataSource", new CommonDataSourceSelector("testDefaultQuery", mock(Component.class)));
        filterExpressionFactory = dataSource.getFilterExpressionFactory();
    }

    private void initSession() throws NoSuchFieldException {
        bindings = stream(TestEntity.class.getDeclaredFields()).map(Field::getName).collect(Collectors.toSet());
        DataSourceBindingContext bindingContext = new DataSourceBindingContext();
        bindingContext.getBindings().addAll(bindings);
        bindingContext.setDataSourceId("testDataSource");
        FieldSetter.setField(bindingContext, BaseDataSourceBindingContext.class.getDeclaredField("dataSourceName"), "testDataSource");

        springDataSourceProvider
            .registerBindingContextForDataSource(bindingContext,
                new CommonDataSourceSelector("testDefaultQuery", mock(Component.class)));
    }


    @Test
    void shouldCreateOnlyOneDataSourceByRepositoryAndDefaultQuery() {
        assertNotNull(dataSource);
        assertNotNull(dataSource.getFilterExpressionFactory());
        assertEquals(bindings, dataSource.getBindings());
    }

    @Test
    void equals_filter_should_find_only_matching_entities() {
        final TestEntity test = TestEntity.builder()
            .text("test")
            .floatValue((float) 1)
            .integer(1)
            .bigDecimal(BigDecimal.valueOf(1))
            .bigInteger(BigInteger.valueOf(1))
            .byteValue((byte) 1)
            .longValue((long) 1)
            .shortValue((short) 1)
            .doubleValue((double) 1)
            .build();
        testRepository.save(test);

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals(test, data.get(0));
    }

    private PagingFetchRequest createFetchRequest() {
        return PagingFetchRequest.builder().page(0).pageSize(Integer.MAX_VALUE).build();
    }

}
