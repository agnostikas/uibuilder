/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuildertest.data;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.Constants;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.annotations.EnableUIBuilder;
import io.devbench.uibuilder.components.util.datasource.DataSourceBindingContext;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import io.devbench.uibuilder.data.common.datasource.PagingFetchRequest;
import io.devbench.uibuilder.data.ebean.filterexpressions.EbeanFilterExpression;
import io.devbench.uibuilder.spring.data.ebean.SpringEbeanDataSource;
import io.devbench.uibuilder.spring.data.ebean.SpringEbeanDataSourceProvider;
import io.devbench.uibuildertest.data.config.EbeanConfig;
import io.devbench.uibuildertest.data.model.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.ebean.repository.config.EnableEbeanRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ComponentScan(
    value = "io.devbench",
    excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = {
        "io\\.devbench\\.uibuilder\\.data\\.spring.ebean\\..+",
        "io\\.devbench\\.uibuilder\\.spring\\.ebean\\..+"
    })
)
@EnableUIBuilder("io.devbench.uibuildertest.data.ebean")
@EnableEbeanRepositories("io.devbench.uibuildertest.data.ebean")
@ContextConfiguration(classes = EbeanConfig.class)
@ExtendWith(SpringExtension.class)
// This property is necessary while the UIBuilder components are not entirely NPM based
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = "vaadin." + Constants.SERVLET_PARAMETER_COMPATIBILITY_MODE + "=true")
@EnableAutoConfiguration
public class EbeanBaseDefaultQueryFilterIT {
    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private PagingAndSortingRepository<TestEntity, String> testRepository;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private SpringEbeanDataSourceProvider<TestEntity> springDataSourceProvider;

    private SpringEbeanDataSource<TestEntity> dataSource;
    private FilterExpressionFactory<EbeanFilterExpression<?>> filterExpressionFactory;
    private Set<String> bindings;

    @BeforeEach
    void setup() {
        testRepository.deleteAll();
        Component component = mock(Component.class);
        initSession(component);
        dataSource = springDataSourceProvider
            .getDataSource("testDataSource", new CommonDataSourceSelector("testEbeanDefaultQuery", component));
        filterExpressionFactory = dataSource.getFilterExpressionFactory();
    }

    private void initSession(Component component) {
        final Map<String, Object> stringObjectHashMap = new HashMap<>();
        final VaadinSession vaadinSession = new VaadinSession(null) {
            @Override
            public Object getAttribute(String name) {
                return stringObjectHashMap.get(name);
            }

            @Override
            public void setAttribute(String type, Object value) {
                stringObjectHashMap.put(type, value);
            }
        };
        VaadinSession.setCurrent(vaadinSession);
        UI.setCurrent(mock(UI.class));
        bindings = stream(TestEntity.class.getDeclaredFields()).map(Field::getName).collect(Collectors.toSet());
        DataSourceBindingContext bindingContext = new DataSourceBindingContext();
        bindingContext.getBindings().addAll(bindings);
        bindingContext.setDataSourceId("testDataSource");
        bindingContext.setDataSourceName("testDataSource");
        springDataSourceProvider.registerBindingContextForDataSource(bindingContext, new CommonDataSourceSelector("testEbeanDefaultQuery", component));
    }


    @Test
    void shouldCreateOnlyOneDataSourceByRepositoryAndDefaultQuery() {
        assertNotNull(dataSource);
        assertNotNull(dataSource.getFilterExpressionFactory());
        assertEquals(bindings, dataSource.getBindings());
    }

    @Test
    void equals_filter_should_find_only_matching_entities() {
        final TestEntity test = TestEntity.builder()
            .text("test")
            .floatValue((float) 1)
            .integer(1)
            .bigDecimal(BigDecimal.valueOf(1))
            .bigInteger(BigInteger.valueOf(1))
            .byteValue((byte) 1)
            .longValue((long) 1)
            .shortValue((short) 1)
            .doubleValue((double) 1)
            .build();
        testRepository.save(test);

        List<TestEntity> data = dataSource.fetchData(createFetchRequest());

        assertEquals(1, data.size());
        assertEquals(test, data.get(0));
    }

    private PagingFetchRequest createFetchRequest() {
        return PagingFetchRequest.builder().page(0).pageSize(Integer.MAX_VALUE).build();
    }

}
