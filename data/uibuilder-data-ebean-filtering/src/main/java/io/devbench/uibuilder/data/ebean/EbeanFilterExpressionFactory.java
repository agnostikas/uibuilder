/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.ebean;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.api.filter.FilterExpressionFactory;
import io.devbench.uibuilder.data.ebean.filterexpressions.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class EbeanFilterExpressionFactory implements FilterExpressionFactory<EbeanFilterExpression<?>> {

    private static final Set<Class<? extends EbeanFilterExpression>> EBEAN_FILTER_EXPRESSION_TYPES;

    @Override
    @NotNull
    public <FILTER_EXPRESSION extends EbeanFilterExpression<?>> FILTER_EXPRESSION create(Class<FILTER_EXPRESSION> expressionType) {
        return EBEAN_FILTER_EXPRESSION_TYPES.stream()
            .filter(expressionType::isAssignableFrom)
            .findAny()
            .flatMap(this::tryToCreateInstance)
            .map(expressionType::cast)
            .orElseThrow(() -> new FilterException("Couldn't find FilterExpression implementation for " + expressionType.getSimpleName()));
    }

    private <EXPRESSION> Optional<EXPRESSION> tryToCreateInstance(Class<EXPRESSION> expressionClass) {
        try {
            return Optional.of(expressionClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            return Optional.empty();
        }
    }

    static {
        EBEAN_FILTER_EXPRESSION_TYPES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
            EbeanAndFilterExpression.class,
            EbeanBetweenExpression.class,
            EbeanEqualsFilterExpression.class,
            EbeanGreaterThanFilterExpression.class,
            EbeanGreaterThanOrEqualsFilterExpression.class,
            EbeanInFilterExpression.class,
            EbeanNotInFilterExpression.class,
            EbeanLessThanFilterExpression.class,
            EbeanLessThanOrEqualsFilterExpression.class,
            EbeanIgnoreCaseLikeFilterExpression.class,
            EbeanLikeFilterExpression.class,
            EbeanNotEqualsFilterExpression.class,
            EbeanNotFilterExpression.class,
            EbeanOrFilterExpression.class,
            EbeanIsNullFilterExpression.class,
            EbeanIsNotNullFilterExpression.class
        )));
    }
}
