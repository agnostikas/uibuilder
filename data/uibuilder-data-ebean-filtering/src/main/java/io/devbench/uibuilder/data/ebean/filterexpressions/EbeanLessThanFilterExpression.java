/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.ebean.filterexpressions;

import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.ebean.ExpressionList;
import lombok.Setter;

public class EbeanLessThanFilterExpression extends ExpressionTypes.LessThan<ExpressionList<?>> implements EbeanFilterExpression<ExpressionList<?>> {
    @Setter
    private ExpressionList<?> containerExpressionList;

    @Override
    public ExpressionList<?> toPredicate() {
        return containerExpressionList.lt(getPath(), getValue());
    }
}




