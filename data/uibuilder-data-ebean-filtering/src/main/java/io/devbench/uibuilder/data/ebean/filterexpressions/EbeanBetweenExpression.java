/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.ebean.filterexpressions;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.common.filter.comperingfilters.ExpressionTypes;
import io.ebean.ExpressionList;
import lombok.Setter;

import java.util.Iterator;

public class EbeanBetweenExpression extends ExpressionTypes.Between<ExpressionList<?>> implements
    EbeanFilterExpression<ExpressionList<?>> {

    @Setter
    private ExpressionList<?> containerExpressionList;

    @Override
    public ExpressionList<?> toPredicate() {
        final int size = getValues().size();
        if (size == 2) {
            final Iterator<Object> iterator = getValues().iterator();
            return containerExpressionList.between(getPath(), iterator.next(), iterator.next());
        } else {
            throw new FilterException(
                "The Between FilterExpression requires exactly 2 parameters, but got " + size
                    + ", the exact values were: " + getValues());
        }
    }
}
