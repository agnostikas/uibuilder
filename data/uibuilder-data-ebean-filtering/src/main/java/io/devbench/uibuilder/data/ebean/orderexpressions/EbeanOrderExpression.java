/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.ebean.orderexpressions;

import io.devbench.uibuilder.data.api.order.OrderExpression;
import io.devbench.uibuilder.data.api.order.SortOrder;
import io.ebean.ExpressionList;
import io.ebean.OrderBy;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class EbeanOrderExpression implements OrderExpression {

    @Setter
    private ExpressionList<?> containerExpressionList;

    private List<SortOrder> sortOrderList = new ArrayList<>();

    @Override
    public void addOrder(SortOrder sortOrder) {
        sortOrderList.add(sortOrder);
    }

    public void toOrder() {
        OrderBy<?> orderBy = containerExpressionList.orderBy();
        orderBy.clear();
        for (SortOrder sortOrder : sortOrderList) {
            orderBy.add(createOrderByClause(sortOrder));
        }
    }

    private String createOrderByClause(SortOrder sortOrder) {
        return sortOrder.getPath() + " " + (sortOrder.getDirection() == SortOrder.Direction.ASCENDING ? "ASC" : "DESC");
    }

}
