/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.data.ebean;

import io.devbench.uibuilder.data.api.exceptions.FilterException;
import io.devbench.uibuilder.data.ebean.filterexpressions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class EbeanFilterExpressionFactoryTest {

    private final EbeanFilterExpressionFactory ebeanFilterExpressionFactory = new EbeanFilterExpressionFactory();

    @Test
    public void shouldCreateAnExpression() {
        Stream
            .of(EbeanAndFilterExpression.class, EbeanBetweenExpression.class,
                EbeanEqualsFilterExpression.class, EbeanGreaterThanFilterExpression.class,
                EbeanGreaterThanOrEqualsFilterExpression.class, EbeanInFilterExpression.class,
                EbeanNotInFilterExpression.class, EbeanIgnoreCaseLikeFilterExpression.class,
                EbeanLessThanFilterExpression.class, EbeanLessThanOrEqualsFilterExpression.class,
                EbeanLikeFilterExpression.class, EbeanNotEqualsFilterExpression.class,
                EbeanNotFilterExpression.class, EbeanOrFilterExpression.class)
            .map(ebeanFilterExpressionFactory::create)
            .forEach(Assertions::assertNotNull);
    }

    @Test
    public void shouldThrowAnExceptionWhenFilterClassIsUnknown() {
        assertThrows(FilterException.class,
            () -> ebeanFilterExpressionFactory.create(UnknownFilterExpression.class));
    }

    public abstract static class UnknownFilterExpression implements EbeanFilterExpression {
    }
}
