/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.utils.copycode.services;

import io.devbench.uibuilder.examples.utils.copycode.pojos.CodeBlock;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CodeBlockExtractorTest {

    private CodeBlockExtractor testObj = new CodeBlockExtractor();

    @Test
    public void should_read_code_block_from_java_file_correctly() throws Exception {
        Path path = Paths.get(getClass().getResource("/JavaCodeBlocks/extract_single_block").toURI());
        String code = TestUtil.readResource("/JavaCodeBlocks/extract_single_block/TodoItemExpected");
        CodeBlock expected = new CodeBlock("TodoItem class", code);

        Collection<CodeBlock> actual = testObj.extractCodeBlocksFromPath(path);
        assertEquals(Collections.singletonList(expected), new ArrayList<>(actual));
    }

    @Test
    public void should_read_code_block_from_hmtl_file_correctly() throws Exception {
        Path path = Paths.get(getClass().getResource("/HtmlCodeBlocks/extract_single_block").toURI());
        String code = TestUtil.readResource("/HtmlCodeBlocks/extract_single_block/vaadin-uibuilder-grid-expected");
        CodeBlock expected = new CodeBlock("collection based grid", code);

        Collection<CodeBlock> actual = testObj.extractCodeBlocksFromPath(path);
        assertEquals(Collections.singletonList(expected), new ArrayList<>(actual));
    }

    @Test
    public void should_fail_on_unfinished_code_block_correctly_1() throws Exception {
        Path path = Paths.get(getClass().getResource("/JavaCodeBlocks/fail_on_unfinished_block").toURI());

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> testObj.extractCodeBlocksFromPath(path));
        assertEquals("Code block (TodoItem class) is not correctly formatted.", exception.getMessage());
    }

    @Test
    public void should_fail_on_unfinished_code_block_correctly_2() throws Exception {
        Path path = Paths.get(getClass().getResource("/JavaCodeBlocks/fail_on_unfinished_block_2").toURI());

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> testObj.extractCodeBlocksFromPath(path));
        assertEquals("Code block (TodoItem class) is not correctly formatted.", exception.getMessage());
    }

}
