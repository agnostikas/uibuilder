/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.utils.copycode.services;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import io.devbench.uibuilder.examples.utils.copycode.pojos.CodeBlock;
import io.devbench.uibuilder.examples.utils.copycode.testutils.InMemoryCopyOfDirectory;
import io.devbench.uibuilder.examples.utils.copycode.testutils.InMemoryCopyOfDirectoryExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(InMemoryCopyOfDirectoryExtension.class)
class CodeBlockWriterTest {

    private final CodeBlockWriter testObj = new CodeBlockWriter();

    @Test
    public void should_write_code_block_into_xml_file_correctly(
        @InMemoryCopyOfDirectory("/resourcesDirWithPlaceholders") Path inMemoryDir
    ) throws Exception {
        String codeBlockJavaContent = TestUtil.readResource("/JavaCodeBlocks/extract_single_block/TodoItemExpected");
        String codeBlockHtmlContent = TestUtil.readResource("/HtmlCodeBlocks/extract_single_block/vaadin-uibuilder-grid-expected");
        CodeBlock codeBlock01 = new CodeBlock("existing-code-block", codeBlockJavaContent);
        CodeBlock codeBlock02 = new CodeBlock("new-code-block", codeBlockHtmlContent);
        testObj.writeCodeBlocksToFiles(inMemoryDir, Arrays.asList(codeBlock01, codeBlock02));

        String existingCodeBlockContent = TestUtil.readResource(inMemoryDir.resolve("existing-code-block"));
        String newCodeBlockContent = TestUtil.readResource(inMemoryDir.resolve("new-code-block"));

        assertEquals(codeBlockJavaContent, existingCodeBlockContent);
        assertEquals(codeBlockHtmlContent, newCodeBlockContent);
    }

    @Test
    public void should_create_the_folder_even_if_it_does_not_exist() throws Exception {
        FileSystem fileSystem = Jimfs.newFileSystem(Configuration.unix());
        Path inMemoryDir = fileSystem.getPath("/some-random-dir");

        String codeBlockJavaContent = TestUtil.readResource("/JavaCodeBlocks/extract_single_block/TodoItemExpected");
        String codeBlockHtmlContent = TestUtil.readResource("/HtmlCodeBlocks/extract_single_block/vaadin-uibuilder-grid-expected");
        CodeBlock codeBlock01 = new CodeBlock("existing-code-block", codeBlockJavaContent);
        CodeBlock codeBlock02 = new CodeBlock("new-code-block", codeBlockHtmlContent);
        testObj.writeCodeBlocksToFiles(inMemoryDir, Arrays.asList(codeBlock01, codeBlock02));

        String existingCodeBlockContent = TestUtil.readResource(inMemoryDir.resolve("existing-code-block"));
        String newCodeBlockContent = TestUtil.readResource(inMemoryDir.resolve("new-code-block"));

        assertEquals(codeBlockJavaContent, existingCodeBlockContent);
        assertEquals(codeBlockHtmlContent, newCodeBlockContent);
    }

}
