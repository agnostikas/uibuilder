/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.utils.copycode;

import io.devbench.uibuilder.examples.utils.copycode.pojos.CodeBlock;
import io.devbench.uibuilder.examples.utils.copycode.services.CodeBlockExtractor;
import io.devbench.uibuilder.examples.utils.copycode.services.CodeBlockWriter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

@Mojo(name = "copy-code-blocks", threadSafe = true)
public class CopyCodeBlockMojo extends AbstractMojo {

    @Parameter(property = "sourcesDir", defaultValue = "${basedir}/src")
    private File sourcesDir;

    @Parameter(property = "codeBlocksDir", defaultValue = "${project.build.directory}/code-blocks")
    private File codeBlocksDir;

    private final CodeBlockExtractor codeBlockExtractor;
    private final CodeBlockWriter codeBlockWriter;

    public CopyCodeBlockMojo() {
        this.codeBlockExtractor = new CodeBlockExtractor();
        this.codeBlockWriter = new CodeBlockWriter();
    }

    @Override
    public void execute() throws MojoExecutionException {
        try {
            Collection<CodeBlock> codeBlocks = codeBlockExtractor.extractCodeBlocksFromPath(sourcesDir.toPath());
            codeBlockWriter.writeCodeBlocksToFiles(codeBlocksDir.toPath(), codeBlocks);
        } catch (IOException e) {
            throw new MojoExecutionException("", e);
        }
    }

}
