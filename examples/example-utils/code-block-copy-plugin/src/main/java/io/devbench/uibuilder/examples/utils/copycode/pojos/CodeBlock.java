/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.utils.copycode.pojos;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public final class CodeBlock {

    @NotNull
    private final String identifier;
    @NotNull
    private final String code;

    public CodeBlock(String identifier, String code) {
        this.identifier = Objects.requireNonNull(identifier, "`identifier` must not be null");
        this.code = Objects.requireNonNull(code, "`code` must not be null");
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeBlock codeBlock = (CodeBlock) o;
        return Objects.equals(getIdentifier(), codeBlock.getIdentifier()) &&
            Objects.equals(getCode(), codeBlock.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdentifier(), getCode());
    }

    @Override
    public String toString() {
        return "CodeBlock{" +
            "identifier='" + identifier + '\'' +
            ", code='" + code + '\'' +
            '}';
    }
}
