/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.utils.copycode.services;

import io.devbench.uibuilder.examples.utils.copycode.pojos.CodeBlock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class CodeBlockWriter {

    public void writeCodeBlocksToFiles(Path basePath, Collection<CodeBlock> codeBlocks) throws IOException {
        if (!Files.exists(basePath)) {
            Files.createDirectories(basePath);
        }

        mergeCodeBlocks(codeBlocks).forEach(codeBlock -> writeCodeBlockToFile(basePath, codeBlock));
    }

    private Collection<CodeBlock> mergeCodeBlocks(Collection<CodeBlock> codeBlocks) {
        return codeBlocks.stream()
            .collect(Collectors.toMap(CodeBlock::getIdentifier, CodeBlock::getCode, CodeBlockWriter::concatCodeBlocks))
            .entrySet()
            .stream()
            .map(e -> new CodeBlock(e.getKey(), e.getValue()))
            .collect(Collectors.toList());
    }

    private static String concatCodeBlocks(String existingCode, String newCode) {
        return existingCode + "\n" + newCode;
    }

    private void writeCodeBlockToFile(Path basePath, CodeBlock codeBlock) {
        try (BufferedReader br = new BufferedReader(new StringReader(codeBlock.getCode()))) {
            Path path = basePath.resolve(codeBlock.getIdentifier());
            List<String> lines = br.lines().collect(Collectors.toList());
            Files.write(path, lines, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
