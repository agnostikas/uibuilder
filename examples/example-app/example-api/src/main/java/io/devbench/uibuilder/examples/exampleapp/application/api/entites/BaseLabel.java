/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

// CODE BLOCK DEFINITION - BaseLabel.java - START
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "labels")
@Inheritance
@DiscriminatorColumn(name = "label_type")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BaseLabel extends BaseEntity {

    @Column(name = "base_name")
    private String baseName = "uibuilder";

}
// CODE BLOCK DEFINITION END
