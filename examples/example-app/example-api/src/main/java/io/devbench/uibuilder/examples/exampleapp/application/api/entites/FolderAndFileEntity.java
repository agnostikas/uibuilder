/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

// CODE BLOCK DEFINITION - FolderAndFileEntity.java - START
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@ToString(exclude = "parent")
@EqualsAndHashCode(callSuper = true, exclude = {"parent", "children"})
public class FolderAndFileEntity extends BaseEntity {

    @ManyToOne
    @JoinTable(name = "folderToFolder",
        joinColumns = @JoinColumn(name = "parent_id"),
        inverseJoinColumns = @JoinColumn(name = "folder_id")
    )
    private FolderAndFileEntity parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<FolderAndFileEntity> children = new HashSet<>();

    private long childrenCount;
    private Long size;
    private String name;
    private FolderOrFile folderOrFile;

    public void calculateSize() {
        if (folderOrFile == FolderOrFile.FOLDER) {
            size = children.stream()
                .peek(FolderAndFileEntity::calculateSize)
                .filter(child -> Objects.nonNull(child.getSize()))
                .mapToLong(FolderAndFileEntity::getSize)
                .sum();
        }
    }

    public void addChild(FolderAndFileEntity... entity) {
        children.addAll(Arrays.asList(entity));
    }

    public void setUpParent() {
        children.forEach(child -> {
                child.setParent(this);
                child.setUpParent();
            }
        );
        calculateSize();
        childrenCount = (long) children.size();
    }
}
// CODE BLOCK DEFINITION END
