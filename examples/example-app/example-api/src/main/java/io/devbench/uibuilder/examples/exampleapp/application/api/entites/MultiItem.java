/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Max;
import java.io.Serializable;
import java.time.LocalDateTime;

// CODE BLOCK DEFINITION - MultiItem.java - START
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MultiItem implements Serializable {

    private String name;

    @Max(value = 100, message = "The maximum priority is 100")
    private Integer priority;

    private LocalDateTime expire;

    private MultiItemLabel label;

    public MultiItem(String name, Integer priority, LocalDateTime expire) {
        this.name = name;
        this.priority = priority;
        this.expire = expire;
        this.label = null;
    }
}
// CODE BLOCK DEFINITION END
