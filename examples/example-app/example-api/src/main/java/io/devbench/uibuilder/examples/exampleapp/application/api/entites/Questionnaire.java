/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;
// CODE BLOCK DEFINITION - Questionnaire.java - START

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class Questionnaire implements Serializable {

    @NotNull(message = "Full name must be set")
    @Size(min = 3, message = "At least 3 characters required")
    private String fullName;

    @NotNull(message = "Age must be set")
    @Min(value = 18, message = "Must be at least 18 years old")
    private Integer age;

    @NotNull(message = "Answer must be set")
    @Size(min = 3, message = "Answer mut be at least 3 character long")
    private String answer1;

    @NotNull
    @Size(min = 10, message = "Answer mut be at least 10 character long")
    private String answer2;

    @NotNull(message = "Label must be selected")
    private Label label;

    @Valid
    private ExtraAttribute extra = new ExtraAttribute();

    @AssertTrue(message = "The conditions must be accepted")
    private Boolean accepted = false;

}
// CODE BLOCK DEFINITION END
