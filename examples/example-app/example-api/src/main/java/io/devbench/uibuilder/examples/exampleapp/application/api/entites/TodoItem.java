/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

// CODE BLOCK DEFINITION - TodoItem.java - START

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "todo_items")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class TodoItem extends BaseEntity {

    @Column(name = "todo")
    @Size(min = 3, message = "Todo text must be at least 3 characters long")
    private String todo;

    @Column(name = "deadline")
    private LocalDate deadline;

    @Column(name = "alert_time")
    private LocalDateTime alertTime;

    @Valid
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "extra_attr_id")
    private ExtraAttribute extraAttribute;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "label_id")
    private BaseLabel label;

    @OneToMany(mappedBy = "todoItem", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<ReminderItem> reminders = new HashSet<>();

    public TodoItem(String todo, LocalDate deadline) {
        this.todo = todo;
        this.deadline = deadline;
    }

    public TodoItem(String todo, LocalDate deadline, ExtraAttribute extraAttribute) {
        this(todo, deadline);
        this.extraAttribute = extraAttribute;
    }

    public TodoItem(String todo, LocalDate deadline, ExtraAttribute extraAttribute, Set<ReminderItem> reminderItems, Label label) {
        this(todo, deadline, extraAttribute);
        this.reminders = reminderItems == null ? new HashSet<>() : reminderItems;
        this.reminders.forEach(reminderItem -> reminderItem.setTodoItem(this));
        this.label = label;
    }

    public TodoItem(String todo, LocalDate deadline, LocalDateTime alertTime, ExtraAttribute extraAttribute, Set<ReminderItem> reminderItems, Label label) {
        this(todo, deadline, extraAttribute, reminderItems, label);
        this.alertTime = alertTime;
    }
}
// CODE BLOCK DEFINITION END
