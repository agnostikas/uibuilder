/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.api.entites;

// CODE BLOCK DEFINITION - ReminderItem.java - START
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "reminders")
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"todoItem"})
@ToString(callSuper = true, exclude = {"todoItem"})
@AllArgsConstructor
@NoArgsConstructor
public class ReminderItem extends BaseEntity {

    public static final int DEFAULT_PRIORITY = 50;

    @ManyToOne
    @JoinColumn(name = "todo_item_id")
    private TodoItem todoItem;

    @Column(name = "text", nullable = false)
    @NotNull(message = "Reminder text cannot be null")
    @NotEmpty(message = "Reminder text cannot be empty")
    private String text;

    @Column(name = "alert_on_day")
    private LocalDate alertOnDay;

    @Column(name = "priority", nullable = false)
    @Min(value = 0, message = "Minimum priority is zero")
    @Max(value = 100, message = "Maximum priority is 100")
    private Integer priority = DEFAULT_PRIORITY;

    public ReminderItem(String text, LocalDate alertOnDay, Integer priority) {
        this.text = text;
        this.alertOnDay = alertOnDay;
        this.priority = priority;
    }

    public ReminderItem(String text, LocalDate alertOnDay) {
        this.text = text;
        this.alertOnDay = alertOnDay;
    }
}
// CODE BLOCK DEFINITION END
