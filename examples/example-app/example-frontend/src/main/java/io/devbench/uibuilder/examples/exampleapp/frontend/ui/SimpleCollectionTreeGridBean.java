/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - SimpleCollectionTreeGridBean.java - START
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.FolderAndFileEntity;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.FolderOrFile;
import lombok.Getter;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@ControllerBean("simpleCollectionTreeGridBean")
public class SimpleCollectionTreeGridBean {

    @Getter
    private List<FolderAndFileEntity> rootItems;

    @PostConstruct
    public void setup() throws IOException {
        this.rootItems = new ObjectMapper().readValue(
            this.getClass().getResourceAsStream("/test_data/folderAndFileEntity.json"),
            new TypeReference<List<FolderAndFileEntity>>() {}
        );
    }

    public Set<FolderAndFileEntity> provideChildren(FolderAndFileEntity parent) {
        if (parent.getFolderOrFile() == FolderOrFile.FOLDER) {
            return parent.getChildren();
        } else {
            return Collections.emptySet();
        }
    }
}
// CODE BLOCK DEFINITION END
