/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.form;
// CODE BLOCK DEFINITION - ChangedOnlyValidationFormControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.Label;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.Questionnaire;
import javax.inject.Provider;
import java.util.Arrays;
import java.util.List;

@ControllerBean("changedOnlyValidation")
public class ChangedOnlyValidationFormControllerBean {

    @UIComponent("form")
    private Provider<UIBuilderForm<Questionnaire>> form;
    private List<Label> labels;

    public ChangedOnlyValidationFormControllerBean() {
        labels = Arrays.asList(new Label("ONE"), new Label("TWO"));
    }

    @UIEventHandler("resetQuestionnaire")
    public void init() {
        form.get().setFormItem(new Questionnaire());
    }

    @UIEventHandler("onSave")
    public void onSave() {
        Questionnaire questionnaire = form.get().getFormItem();
        Notification.show("Questionnaire: " + questionnaire, 5000, Notification.Position.TOP_CENTER);
    }

}
// CODE BLOCK DEFINITION END
