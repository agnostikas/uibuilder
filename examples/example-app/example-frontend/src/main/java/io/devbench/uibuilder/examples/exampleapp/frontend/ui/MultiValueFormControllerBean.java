/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - MultiValueFormControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.function.SerializableSupplier;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.multivalue.UIBuilderMultiValue;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.MultiItem;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@ControllerBean("multiValueFormCb")
public class MultiValueFormControllerBean {

    @SuppressWarnings("unused")
    private final SerializableSupplier<MultiItem> multiItemSupplier =
        () -> new MultiItem("New Item", 10, LocalDateTime.now());


    private List<MultiItem> createMultiItems() {
        List<MultiItem> multiItems = new ArrayList<>();
        multiItems.add(new MultiItem("Name 1", 50, LocalDateTime.now().plusHours(1)));
        multiItems.add(new MultiItem("Name 2", 60, LocalDateTime.now().plusHours(2)));
        multiItems.add(new MultiItem("Name 3", 70, LocalDateTime.now().plusHours(3)));
        return multiItems;
    }

    private void showMultiItems(List<MultiItem> multiItems) {
        Notification.show(
            "Items:\n" + multiItems.stream()
                .map(MultiItem::toString)
                .collect(Collectors.joining("\n"))
                .replaceAll(" ", "\u00A0")
                .replaceAll("-", "\u2011"),
            5000, Notification.Position.TOP_CENTER);
    }

    private void showPriorities(List<Integer> priorities) {
        Notification.show(
            "Priorities: " + priorities.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", ")),
            5000,
            Notification.Position.TOP_CENTER);
    }

    @UIEventHandler("getValuesOnBackend")
    public void onGetValuesOnBacked(@UIComponent("testMultiValue") UIBuilderMultiValue<MultiItem, Integer> multiValue) {
        List<MultiItem> items = multiValue.getItems();
        showMultiItems(items);

        List<Integer> values = multiValue.getValue();
        showPriorities(values);
    }

    @UIEventHandler("setValuesOnBackend")
    public void onSetValuesOnBackend(@UIComponent("testMultiValue") UIBuilderMultiValue<MultiItem, Integer> multiValue) {
        List<Integer> values = multiValue.getValue();
        int count = values != null ? values.size() : 0;

        Random random = new SecureRandom();
        values = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            values.add(random.nextInt(100));
        }
        multiValue.setValue(values);
    }

    @UIEventHandler("setItemsOnBackend")
    public void onSetItemsOnBackend(@UIComponent("testMultiValue") UIBuilderMultiValue<MultiItem, ?> multiValue) {
        multiValue.setItems(createMultiItems());
    }

}
// CODE BLOCK DEFINITION END
