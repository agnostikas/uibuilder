/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.calendar;

// CODE BLOCK DEFINITION - EntryWithDescriptionBuilder.java - START
import java.time.Instant;

public class EntryWithDescriptionBuilder {
    private String id;
    private String title;
    private Instant start;
    private Instant end;
    private boolean allDay;
    private boolean editable;
    private String color;
    private String description;

    public EntryWithDescriptionBuilder id(String id) {
        this.id = id;
        return this;
    }

    public EntryWithDescriptionBuilder title(String title) {
        this.title = title;
        return this;
    }

    public EntryWithDescriptionBuilder start(Instant start) {
        this.start = start;
        return this;
    }

    public EntryWithDescriptionBuilder end(Instant end) {
        this.end = end;
        return this;
    }

    public EntryWithDescriptionBuilder allDay(boolean allDay) {
        this.allDay = allDay;
        return this;
    }

    public EntryWithDescriptionBuilder editable(boolean editable) {
        this.editable = editable;
        return this;
    }

    public EntryWithDescriptionBuilder color(String color) {
        this.color = color;
        return this;
    }

    public EntryWithDescriptionBuilder description(String description) {
        this.description = description;
        return this;
    }

    public EntryWithDescription build() {
        return new EntryWithDescription(id, title, start, end, allDay, editable, color, description);
    }
}
// CODE BLOCK DEFINITION END
