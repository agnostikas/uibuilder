/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.event;

// CODE BLOCK DEFINITION - EventHandlerControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import org.springframework.context.annotation.Scope;

@Scope("session")
@ControllerBean("eventHandler")
public class EventHandlerControllerBean {

    @CallOnNonNull
    @UIEventHandler("doNotPopUp")
    public void doNotPopUp(@UIComponent("notification1") Notification notification) {
        throw new UnsupportedOperationException("This cannot be called");
    }

    @UIEventHandler("popUp")
    public void popUp(@CallOnNonNull @UIComponent("notification2") Notification notification) {
        notification.setText("Congratulation, you have opened the notification2 from the backend");
        notification.setDuration(1000);
        notification.open();
    }
}
// CODE BLOCK DEFINITION END
