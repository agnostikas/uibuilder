/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.event;

// CODE BLOCK DEFINITION - UnloadEventHandlerControllerBean.java - START
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
// CODE BLOCK DEFINITION END

// CODE BLOCK DEFINITION - UnloadEventHandlerControllerBean.java - START
@UIScope
@ControllerBean("unloadEventHandler")
public class UnloadEventHandlerControllerBean {

    @UIEventHandler("unload")
    public void unload() {
        Notification notification = new Notification(
            "Notification from the page's unload event.",
            5000,
            Notification.Position.MIDDLE);
        notification.open();
    }
}
// CODE BLOCK DEFINITION END
