/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;
// CODE BLOCK DEFINITION - SimpleTodoItemCollectionControllerBean.java - START

import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ExtraAttribute;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.Label;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import javax.annotation.PostConstruct;
import javax.inject.Provider;
import java.time.LocalDate;
import java.util.*;

@UIScope
@ControllerBean("simpleTodoItemCb")
public class SimpleTodoItemCollectionControllerBean {

    @UIComponent("itemsCombobox")
    private Provider<UIBuilderComboBox<TodoItem>> itemsComboboxProvider;

    private List<TodoItem> todoItems = new ArrayList<>();
    private String lastAddedItemName;

    @PostConstruct
    public void popuplateTodoItems() {
        Set<ReminderItem> reminderItems = new HashSet<>();

        TodoItem todo1 = new TodoItem("Item 1", LocalDate.now(), new ExtraAttribute("Extra 1", 100), reminderItems, new Label("Label 1"));
        TodoItem todo2 = new TodoItem("Item 2", LocalDate.now(), new ExtraAttribute("Extra 2", 200), null, null);
        TodoItem todo3 = new TodoItem("Item 3", LocalDate.now(), null, null, new Label("Label 3"));

        reminderItems.add(new ReminderItem(todo1, "Reminder 1", LocalDate.now().minusDays(1), 50));

        todoItems.add(todo1);
        todoItems.add(todo2);
        todoItems.add(todo3);
    }

    public List<TodoItem> getTodoItems() {
        return todoItems;
    }

    public void setTodoItems(List<TodoItem> todoItems) {
        this.todoItems = todoItems;
    }

    public String getLastAddedItemName() {
        return lastAddedItemName;
    }

    public void setLastAddedItemName(String lastAddedItemName) {
        this.lastAddedItemName = lastAddedItemName;
    }

    @UIEventHandler("refresh")
    public void onRefresh() {
        itemsComboboxProvider.get().refresh();
    }

    @UIEventHandler("modify")
    public void onModify() {
        lastAddedItemName = "Newly added item " + (todoItems.size() + 1);
        todoItems.add(new TodoItem(lastAddedItemName, LocalDate.now()));
        itemsComboboxProvider.get().refresh();
    }

    @UIEventHandler("toggleInvalid")
    public void onToggleInvalid() {
        UIBuilderComboBox<TodoItem> comboBox = itemsComboboxProvider.get();
        comboBox.setInvalid(!comboBox.isInvalid());
        if (comboBox.isInvalid()) {
            comboBox.setErrorMessage("Error #" + new Random().nextInt());
        }
    }
}
// CODE BLOCK DEFINITION END
