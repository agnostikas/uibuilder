/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - MultiValueFormPrimitivesControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.function.SerializableSupplier;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.MultiItemHolder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerBean("multiValueFormPrimitivesCb")
public class MultiValueFormPrimitivesControllerBean {

    private final SerializableSupplier<Integer> prioSupplier = () -> 123;

    private void showPriorities(List<Integer> priorities) {
        Notification.show(
            "Priorities: " + priorities.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", ")),
            5000,
            Notification.Position.TOP_CENTER);
    }

    @UIEventHandler("setupFormItem")
    public void onSetupFormItem(@UIComponent("testMultiForm") UIBuilderForm<MultiItemHolder> multiItemHolderForm) {
        MultiItemHolder holder = new MultiItemHolder();
        holder.setHolderName("Initial name");
        holder.setPriorities(new ArrayList<>(Arrays.asList(5, 6)));
        multiItemHolderForm.setFormItem(holder);
    }

    @UIEventHandler("save")
    public void onSave(@UIComponent("testMultiForm") UIBuilderForm<MultiItemHolder> multiItemHolderForm) {
        MultiItemHolder formItem = multiItemHolderForm.getFormItem();
        showPriorities(formItem.getPriorities());
    }

    @UIEventHandler("initForms")
    public void initForms(@UIComponent("testMultiForm") UIBuilderForm<MultiItemHolder> multiItemHolderForm) {
        MultiItemHolder holder1 = new MultiItemHolder();
        holder1.setHolderName("Initial name");
        holder1.setPriorities(new ArrayList<>(Arrays.asList(5, 6)));
        multiItemHolderForm.setFormItem(holder1);
    }

}
// CODE BLOCK DEFINITION END
