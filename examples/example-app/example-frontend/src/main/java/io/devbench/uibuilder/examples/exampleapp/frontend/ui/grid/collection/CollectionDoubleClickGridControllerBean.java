/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.grid.collection;

// CODE BLOCK DEFINITION - CollectionDoubleClickGridControllerBean.java - START

import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Data;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@ControllerBean("collectionDoubleClickCB")
public class CollectionDoubleClickGridControllerBean {

    @UIEventHandler("initializeGrid")
    public void initializeGrid(@UIComponent("grid") UIBuilderGrid<TodoItem> grid) {
        grid.setItems(
            new TodoItem("Buy some food", LocalDate.now().plus(3, ChronoUnit.DAYS)),
            new TodoItem("Feed the cat", LocalDate.now()),
            new TodoItem("Write a unit test", LocalDate.now().plus(5, ChronoUnit.DAYS)),
            new TodoItem("Fix the car", LocalDate.now().plus(10, ChronoUnit.DAYS))
        );
    }

    @UIEventHandler("doubleClick")
    public void onDoubleClick(@Item TodoItem item, @Data("path") String path) {
        Notification.show("Double-click! Path: " + path + ", Item name: " + item.getTodo(), 5000, Notification.Position.TOP_CENTER);
    }
}
// CODE BLOCK DEFINITION END
