/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.urlparameters;

// CODE BLOCK DEFINITION - FlowParameterControllerBean.java - START
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.core.flow.FlowParameter;
import io.devbench.uibuilder.core.flow.NavigationBuilder;
import io.devbench.uibuilder.core.flow.QueryParameter;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
// CODE BLOCK DEFINITION END

// CODE BLOCK DEFINITION - FlowParameterControllerBean.java - START
@ControllerBean("flowParameterControllerBean")
public class FlowParameterControllerBean {

    @Getter
    private TodoItem todoItem;

    @Autowired
    @Qualifier("todoItemRepository")
    private CrudRepository<TodoItem, String> todoItemRepository;

    @UIEventHandler("seeIdOfSelectedItem")
    public void seeIdOfSelectedItem(@UIComponent("todo-grid") UIBuilderGrid<TodoItem> grid) {
        TodoItem selectedItem = grid.getSelectedItem();
        if (selectedItem != null) {
            new Dialog(new Label(selectedItem.getId())).open();
        }
    }

    @UIEventHandler("seeDetailsOfSelectedItem")
    public void seeDetailsOfSelectedItem(@UIComponent("todo-grid") UIBuilderGrid<TodoItem> grid) {
        TodoItem selectedItem = grid.getSelectedItem();
        if (selectedItem != null) {
            String id = grid.getSelectedItem().getId();
            NavigationBuilder
                .to("todoDetails")
                .urlParameter("item-id", id)
                .navigate();
        }
    }

    @UIEventHandler("seeNextTodoDetail")
    public void seeNextTodoDetail(@FlowParameter("item-id") String itemId, @QueryParameter("previousId") String previousId) {
        TodoItem todoItem = StreamSupport.stream(todoItemRepository.findAll().spliterator(), false)
            .filter(it -> !Objects.equals(it.getId(), itemId))
            .filter(it -> previousId == null || !Objects.equals(it.getId(), previousId))
            .findAny()
            .orElse(null);

        if (todoItem != null) {
            String id = todoItem.getId();
            NavigationBuilder
                .to("todoDetails")
                .urlParameter("item-id", id)
                .queryParameter("previousId", itemId)
                .navigate();
        }
    }

    @UIEventHandler("seePreviousTodoDetail")
    public void seePreviousTodoDetail(@QueryParameter("previousId") String previousId, @FlowParameter("item-id") String itemId) {
        Optional<TodoItem> todoItem = previousId != null ? todoItemRepository.findById(previousId) : Optional.empty();
        if (todoItem.isPresent()) {
            String id = todoItem.get().getId();
            NavigationBuilder
                .to("todoDetails")
                .urlParameter("item-id", id)
                .queryParameter("previousId", itemId)
                .navigate();
        } else {
            Notification.show("No previous item to load, use the Next button first", 5000, Notification.Position.MIDDLE);
        }
    }

    @UIEventHandler("beforeLoad")
    public void beforeLoad(@FlowParameter("item-id") String itemId) {
        todoItem = todoItemRepository.findById(itemId).get();
    }
}
// CODE BLOCK DEFINITION END
