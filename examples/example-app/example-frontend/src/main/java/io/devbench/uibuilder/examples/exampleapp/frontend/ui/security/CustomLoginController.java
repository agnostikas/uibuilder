/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.security;

// CODE BLOCK DEFINITION - CustomLoginController.java - START
import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.examples.exampleapp.frontend.application.RealmConfiguration;
import io.devbench.uibuilder.spring.page.PageScope;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;

import java.util.Optional;

@PageScope("login-page")
@ControllerBean("customLoginController")
public class CustomLoginController {

    @UIEventHandler("login")
    public void login() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            if (subject.hasRole(RealmConfiguration.ADMIN_ROLE)) {
                navigate("adminScreen");
            } else {
                navigate("userScreen");
            }
            FlowManager.getCurrent().reload();
        }
    }

    private void navigate(String path) {
        FlowManager.getCurrent().navigate(path);
    }

    public String getUserMenuName() {
        return Optional
            .ofNullable(SecurityUtils.getSubject())
            .map(Subject::getPrincipal)
            .map(Object::toString)
            .orElse("");
    }

    @UIEventHandler("logout")
    public void logout() {
        SecurityUtils.getSubject().logout();
        FlowManager.getCurrent().reload();
    }

    @UIEventHandler("showAdminNotification")
    @RequiresPermissions("ADMIN")
    public void showAdminNotification() {
        Notification.show("This notification is only visible for the admin user");
    }
}
// CODE BLOCK DEFINITION END
