/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.calendar;

// CODE BLOCK DEFINITION - EntryWithDescription.java - START
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.utils.elemental.json.JsonBuilder;
import io.devbench.uibuilder.components.calendar.data.Entry;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;

@Data
@EqualsAndHashCode(callSuper = true)
public class EntryWithDescription extends Entry {
    private String description;

    public EntryWithDescription(String id, String title, Instant start, Instant end, boolean allDay, boolean editable, String color, String description) {
        super(id, title, start, end, allDay, editable, color);
        this.description = description;
    }

    @Override
    public JsonObject toJson() {
        JsonObject jsonObject = super.toJson();
        jsonObject.put("extendedProps", JsonBuilder.jsonObject().put("description", description).build());
        return jsonObject;
    }
}
// CODE BLOCK DEFINITION END
