/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.dynamic;

// CODE BLOCK DEFINITION - DynamicControllerBeanImpl.java - START
import com.vaadin.flow.component.datepicker.DatePicker;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import io.devbench.uibuilder.examples.exampleapp.application.repository.dynamic.DynamicControllerBean;

import java.time.LocalDate;
import java.util.Optional;

@ControllerBean("dynamic")
public class DynamicControllerBeanImpl implements DynamicControllerBean {

    private LocalDate deadline;

    @UIEventHandler("deadlineChanged")
    public void deadlineChanged(@UIComponent("deadline") DatePicker datePicker, @UIComponent("todo_grid") UIBuilderGrid<TodoItem> todoItemGrid) {
        if (datePicker != null) {
            deadline = datePicker.getValue();
            todoItemGrid.refresh();
        }
    }

    @Override
    public Optional<LocalDate> getDeadLine() {
        return Optional.ofNullable(deadline);
    }

}
// CODE BLOCK DEFINITION END
