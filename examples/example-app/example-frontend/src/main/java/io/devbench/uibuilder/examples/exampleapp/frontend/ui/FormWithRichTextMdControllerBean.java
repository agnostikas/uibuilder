/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - FormWithRichTextMdControllerBean.java - START

import com.vaadin.flow.component.html.Pre;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;

import javax.inject.Provider;
import java.time.LocalDate;

@ControllerBean("formRichTextMdCb")
public class FormWithRichTextMdControllerBean {

    @UIComponent("form")
    private Provider<UIBuilderForm<TodoItem>> formProvider;

    @UIComponent("lastSavedMd")
    private Provider<Pre> lastSavedMd;

    @UIEventHandler("initForm")
    public void initForm() {
        String initialTodoValue = "Initial **text** with markdown";
        TodoItem todoItem = new TodoItem(initialTodoValue, LocalDate.now());
        formProvider.get().setFormItem(todoItem);
    }

    @UIEventHandler("onSave")
    public void onSave() {
        String formattedTodo = formProvider.get().getFormItem().getTodo();
        Pre lastSavedMdPre = lastSavedMd.get();
        lastSavedMdPre.removeAll();
        lastSavedMdPre.setText(formattedTodo);
    }

}
// CODE BLOCK DEFINITION END
