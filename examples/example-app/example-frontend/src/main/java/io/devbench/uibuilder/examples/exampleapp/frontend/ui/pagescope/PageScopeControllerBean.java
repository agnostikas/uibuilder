/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.pagescope;

// CODE BLOCK DEFINITION - PageScopeControllerBean.java - START

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.spring.page.PageScope;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@PageScope("page-scope-example")
@ControllerBean("pageScopeControllerBean")
public class PageScopeControllerBean {

    @Getter
    @Setter
    private String someValue;

    @UIEventHandler("reload")
    public void reload() {
        FlowManager.getCurrent().reload();
    }

    @UIEventHandler("addLabel")
    public void addLabel(@UIComponent("contentDiv") VerticalLayout contentDiv) {
        contentDiv.add(new Label("Some label at: " + Instant.now()));
    }
}
// CODE BLOCK DEFINITION END
