/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - FormControllerBean.java - START
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.CustomFormValidatorRegistration;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.form.validator.CustomFormValidator;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ExtraAttribute;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import java.time.LocalDate;

@ControllerBean("formController")
public class FormControllerBean {

    @UIEventHandler("activateTodoItem1")
    public void activateTodoItem1(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        ExtraAttribute extraAttribute = new ExtraAttribute("extra 1", 1000);
        TodoItem todoItem = new TodoItem("First todo item", LocalDate.now(), extraAttribute);
        simpleForm.setFormItem(todoItem);
    }

    @UIEventHandler("activateTodoItem2")
    public void activateTodoItem2(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        TodoItem todoItem = new TodoItem("Second todo item", LocalDate.now().minusDays(2), null);
        simpleForm.setFormItem(todoItem);
    }

    @UIEventHandler("clearFormItem")
    public void clearFormItem(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        simpleForm.setFormItem(null);
    }

    @UIEventHandler("onSave")
    public void onSave(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        TodoItem item = simpleForm.getFormItem();
        Notification.show("Saving " + item, 10000, Notification.Position.BOTTOM_CENTER);
    }

    @UIEventHandler("setFormReadonly")
    public void setFormReadonly(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        simpleForm.setReadonly(!simpleForm.isReadonly());
    }

    @UIEventHandler("formReady")
    public void formReady(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm,
                          @UIComponent("customValidTextField") TextField customValidTextField) {

        customValidTextField.setValueChangeMode(ValueChangeMode.EAGER);

        CustomFormValidatorRegistration registration = simpleForm.addCustomValidator(() -> {
            String value = customValidTextField.getValue();
            boolean valid = "agree".equals(value);
            return CustomFormValidator.Result.builder().valid(valid).errorMessage("You must agree to save the form").build();
        }).apply(customValidTextField);

    }

    @UIEventHandler("revalidate")
    public void revalidateForm(@UIComponent("form") UIBuilderForm<TodoItem> simpleForm) {
        simpleForm.validate();
    }
}
// CODE BLOCK DEFINITION END
