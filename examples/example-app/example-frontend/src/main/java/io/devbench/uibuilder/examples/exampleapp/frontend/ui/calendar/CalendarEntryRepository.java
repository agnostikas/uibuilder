/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.calendar;

// CODE BLOCK DEFINITION - CalendarEntryRepository.java - START

import io.devbench.uibuilder.components.calendar.data.Entry;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.beans.factory.config.BeanDefinition.*;

@Component
@Scope(SCOPE_PROTOTYPE)
public class CalendarEntryRepository {
    private final List<Entry> calendarEntries = new ArrayList<>();

    public CalendarEntryRepository() {
        Instant midnight = Instant.now().truncatedTo(ChronoUnit.DAYS);
        create(Entry.builder()
            .id(UUID.randomUUID().toString())
            .start(midnight.plus(7, ChronoUnit.HOURS))
            .end(midnight.plus(8, ChronoUnit.HOURS))
            .color("green")
            .title("Green event")
            .build());
    }

    public Collection<Entry> findByColor(Instant start, Instant end, boolean green, boolean blue) {
        return calendarEntries.stream()
            .filter(entry -> !start.isAfter(entry.getStart()))
            .filter(entry -> !end.isBefore(entry.getEnd()))
            .filter(entry -> green && "green".equals(entry.getColor()) || blue && entry.getColor() == null)
            .collect(Collectors.toList());
    }

    public void create(Entry entry) {
        calendarEntries.add(entry);
    }

    public void update(Entry entry) {
        calendarEntries.stream()
            .filter(e -> e.getId().equals(entry.getId()))
            .findAny()
            .ifPresent(e -> {
                e.setStart(entry.getStart());
                e.setEnd(entry.getEnd());
            });
    }
}
// CODE BLOCK DEFINITION END
