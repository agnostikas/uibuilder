/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.application;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
@Slf4j
public class StartupInitializer {

    @Autowired
    @Qualifier("todoItemRepository")
    private CrudRepository<TodoItem, String> repository;

    @Autowired
    @Qualifier("folderAndFileRepository")
    private CrudRepository<FolderAndFileEntity, String> folderAndFileRepository;

    @PostConstruct
    public void setup() {
        todoSetup();
        folderAndFileSetup();
    }

    public void todoSetup() {
        ExtraAttribute extraAttribute = new ExtraAttribute("Extra attribute text", 123);

        Set<ReminderItem> reminderItems1 = new HashSet<>();
        reminderItems1.add(new ReminderItem("Reminder 1", LocalDate.now().minusDays(3)));
        reminderItems1.add(new ReminderItem("Reminder 2", LocalDate.now().minusDays(2)));
        reminderItems1.add(new ReminderItem("Reminder 3", LocalDate.now().minusDays(2), 40));

        Set<ReminderItem> reminderItems2 = new HashSet<>();
        reminderItems2.add(new ReminderItem("Another reminder item", LocalDate.now()));
        reminderItems2.add(new ReminderItem("Last reminder", LocalDate.now(), 75));

        Label label1 = new Label();
        label1.setName("Custom label");

        Label label2 = new Label();
        label2.setName("");

        LocalDateTime currentLocalDateTime = LocalDateTime.now();
        repository.save(new TodoItem("Buy some chocolate",
            LocalDate.now().plus(1, ChronoUnit.YEARS),
            currentLocalDateTime.plus(1, ChronoUnit.HOURS),
            null, null, null));
        repository.save(new TodoItem("Write more tests",
            LocalDate.now().plus(2, ChronoUnit.MONTHS),
            currentLocalDateTime.plus(2, ChronoUnit.MINUTES),
            extraAttribute, reminderItems1, label1));
        repository.save(new TodoItem("Brew a cup of coffee",
            LocalDate.now().plus(3, ChronoUnit.DAYS),
            currentLocalDateTime.plus(3, ChronoUnit.SECONDS),
            null, reminderItems2, label2));
    }

    private void folderAndFileSetup() {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            saveTestEntities(mapper);
        } catch (IOException e) {
            log.warn("Db Folder and File table init is corrupted, because", e);
        }
    }

    private void saveTestEntities(ObjectMapper mapper) throws IOException {
        final List<FolderAndFileEntity> testEntities = mapper
            .readValue(this.getClass().getResourceAsStream("/test_data/folderAndFileEntity.json"), new TypeReference<List<FolderAndFileEntity>>() {
            });
        testEntities.forEach(FolderAndFileEntity::setUpParent);
        folderAndFileRepository.saveAll(testEntities);
    }
}
