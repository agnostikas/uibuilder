/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.combobox;

// CODE BLOCK DEFINITION - ComboboxWithDefaultSelectionControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.combobox.UIBuilderComboBox;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;

@ControllerBean("comboboxWithDefaultSelectionControllerBean")
public class ComboboxWithDefaultSelectionControllerBean {

    @Autowired
    @Qualifier("todoItemRepository")
    private CrudRepository<TodoItem, String> todoItemRepository;

    @UIEventHandler("afterLoad")
    public void afterLoad(@UIComponent("comboboxWithDefaultValue")UIBuilderComboBox<TodoItem> comboBox) {
        comboBox.setValue(todoItemRepository.findAll().iterator().next());
    }

}
// CODE BLOCK DEFINITION END
