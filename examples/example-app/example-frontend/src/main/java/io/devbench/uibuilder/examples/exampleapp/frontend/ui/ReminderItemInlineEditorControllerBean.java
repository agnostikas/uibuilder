/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui;

// CODE BLOCK DEFINITION - ReminderItemInlineEditorControllerBean.java - START

import com.vaadin.flow.function.SerializableSupplier;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;

import java.util.function.Predicate;

@ControllerBean("reminderInlineEditor")
public class ReminderItemInlineEditorControllerBean {

    private SerializableSupplier<Predicate<ReminderItem>> reminderItemPredicateSupplier =
        () -> reminderItem -> reminderItem.getText() == null || !reminderItem.getText().endsWith("2");

}
// CODE BLOCK DEFINITION END
