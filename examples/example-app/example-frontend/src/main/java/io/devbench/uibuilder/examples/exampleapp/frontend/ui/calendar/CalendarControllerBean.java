/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.frontend.ui.calendar;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.UIScope;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.calendar.UIBuilderCalendar;
import io.devbench.uibuilder.components.calendar.data.DropData;
import io.devbench.uibuilder.components.calendar.data.Entry;
import io.devbench.uibuilder.components.calendar.data.ResizeData;
import io.devbench.uibuilder.components.calendar.data.TimeSlot;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.util.Random;
import java.util.UUID;

// CODE BLOCK DEFINITION - CalendarControllerBean.java - START
@UIScope
@RequiredArgsConstructor
@ControllerBean(value = "calendarControllerBean")
public class CalendarControllerBean {

    private final CalendarEntryRepository calendarEntryRepository;
    private int eventTitleSequence;

    @UIEventHandler("init")
    public void init(
        @UIComponent("green") Checkbox green,
        @UIComponent("blue") Checkbox blue,
        @UIComponent("calendar") UIBuilderCalendar calendar
    ) {
        green.setValue(true);
        blue.setValue(true);

        calendar.setEntryRepository((start, end) -> calendarEntryRepository.findByColor(start, end, green.getValue(), blue.getValue()));
    }

    @UIEventHandler("filterChanged")
    public void filterChanged(@UIComponent("calendar") UIBuilderCalendar calendar) {
        calendar.refetchEvents();
    }

    @UIEventHandler("onTimeSlotSelect")
    public void onTimeSlotSelect(@UIComponent("calendar") UIBuilderCalendar calendar, @Item TimeSlot timeSlot) {
        calendarEntryRepository.create(new EntryWithDescriptionBuilder()
            .id(UUID.randomUUID().toString())
            .start(timeSlot.getStart())
            .end(timeSlot.getEnd())
            .allDay(timeSlot.isAllDay())
            .title("Event " + (++eventTitleSequence))
            .description("Event " + eventTitleSequence + " extra property " + Integer.toString(new Random().nextInt(), 36))
            .editable(true)
            .build());
        calendar.refetchEvents();
    }

    @UIEventHandler("onEntryClick")
    public void onEntryClick(@Item Entry entry) {
        Notification.show(
            "ID: " + entry.getId() +
                "  Duration: " + Duration.between(entry.getStart(), entry.getEnd())
        );
    }

    @UIEventHandler("onResize")
    public void onResize(@Item ResizeData resizeData) {
        calendarEntryRepository.update(resizeData.getEntry());
        Notification.show(
            "[Resize] ID: " + resizeData.getEntry().getId() +
                " Start Delta: " +  resizeData.getStartDelta().toPeriod() + " " + resizeData.getStartDelta().toDuration() +
                " End Delta: " +  resizeData.getEndDelta().toPeriod() + " " + resizeData.getEndDelta().toDuration()
        );
    }

    @UIEventHandler("onDrop")
    public void onDrop(@Item DropData dropData) {
        calendarEntryRepository.update(dropData.getEntry());
        Notification.show(
            "[Drop] ID: " + dropData.getEntry().getId() +
                " Delta: " + dropData.getDelta().toPeriod() + " " + dropData.getDelta().toDuration()
        );
    }
}
// CODE BLOCK DEFINITION END
