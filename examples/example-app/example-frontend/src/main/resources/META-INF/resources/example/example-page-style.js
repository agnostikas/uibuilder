
const $_styleContainer = document.createElement('template');
$_styleContainer.innerHTML = `
<custom-style>
    <style>
        .content {
            margin: 0;
            padding: 16px;
            background-color: var(--uibuilder-lumo-menu-bg-color);
        }

        .description {
            margin: 16px 0;
            padding: 8px;
            background-color: #FEFDF8;
            border: 1px solid #F0E9EA;
            border-radius: 5px;
            color: #505040;
            font-family: sans-serif;
        }

        code, .code {
            font-family: monospace;
            font-weight: bold;
            font-size: 1.1rem;
            padding: 3px;
            background-color: rgba(0, 0, 0, 0.05);
            border-radius: 3px;
        }

        .component {
            margin: 16px 0;
            padding: 16px;
            border: 1px solid #CBCBCB;
            border-radius: 5px;
            background-color: #FFFFFF;
            color: #101010;
        }

        page {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            position: relative;
        }

        h2[part="header"] {
            margin-top: 4px;
        }

        uibuilder-code-block {
            font-size: 14px;
        }
    </style>
</custom-style>
`;

document.head.appendChild($_styleContainer.content);
