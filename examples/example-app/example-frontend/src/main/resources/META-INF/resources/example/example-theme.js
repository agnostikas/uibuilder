
const $_styleContainer = document.createElement('template');
$_styleContainer.innerHTML = `
<custom-style>
    <style>
        html {
            --lumo-size-xs: 20px;
            --lumo-size-s: 24px;
            --lumo-size-m: 28px;
            --lumo-size-l: 32px;
            --lumo-size-xl: 40px;
            /* Font sizes (will affect spacing also) */
            --lumo-font-size-xxs: 10px;
            --lumo-font-size-xs: 11px;
            --lumo-font-size-s: 12px;
            --lumo-font-size-m: 14px;
            --lumo-font-size-l: 16px;
            --lumo-font-size-xl: 20px;
            --lumo-font-size-xxl: 24px;
            --lumo-font-size-xxxl: 32px;
            /* Icon size */
            --lumo-icon-size-s: 1em;
            --lumo-icon-size-m: 1.25em;
            --lumo-icon-size-l: 1.5em;
            /* Line height */
            --lumo-line-height-xs: 1.1;
            --lumo-line-height-s: 1.3;
            --lumo-line-height-m: 1.5;

            --lumo-base-color: #fcfdff;
            --lumo-primary-color: hsl(211, 100%, 30%);
            --lumo-primary-color-50pct: hsla(214, 80%, 41%, 0.5);
            --lumo-primary-color-10pct: hsla(214, 80%, 42%, 0.1);

            --lumo-error-color: hsl(0, 60%, 42%);
            --lumo-error-color-50pct: hsla(3, 65%, 43%, 0.5);
            --lumo-error-color-10pct: hsla(3, 62%, 43%, 0.1);
            --lumo-error-text-color: hsl(3, 78%, 27%);

            --lumo-success-color: hsl(146, 79%, 24%);
            --lumo-success-color-50pct: hsla(145, 73%, 31%, 0.55);
            --lumo-success-color-10pct: hsla(145, 73%, 24%, 0.12);
            --lumo-success-text-color: hsl(144, 100%, 17%);

            font-family: sans-serif;
        }
    </style>
</custom-style>
`;

document.head.appendChild($_styleContainer.content);
