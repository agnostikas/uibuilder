// CODE BLOCK DEFINITION - example-filter-theme.js - START
const $_documentContainer = document.createElement('dom-module');
$_documentContainer.setAttribute('id', 'example-filter-theme');
$_documentContainer.setAttribute('theme-for', 'uibuilder-common-filter');

const $_styleContainer = document.createElement('template');
$_styleContainer.innerHTML = `
<style>
    :host{
        --lumo-primary-text-color: green;
    }

    [part="search-button"] {
        float: left;
        margin-left: 10px;
        color: green;
    }

    [part="add-button"]{
        float: left;
        color: green;
    }

    #search [part="search-button-label"]::before {
        font-family: "lumo-icons";
        content: var(--lumo-icons-search);
        font-size: var(--lumo-icon-size-s);
    }

    #add [part="add-button-label"]::before {
        font-family: "lumo-icons";
        content: var(--lumo-icons-plus);
        font-size: var(--lumo-icon-size-s);
    }
</style>
`;

$_documentContainer.appendChild($_styleContainer);
document.head.appendChild($_documentContainer);
// CODE BLOCK DEFINITION END
