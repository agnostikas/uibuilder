<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>io.devbench.uibuilder.examples</groupId>
        <artifactId>example-app</artifactId>
        <version>2.1.3-SNAPSHOT</version>
    </parent>

    <groupId>io.devbench.uibuilder.examples.example-app</groupId>
    <artifactId>example-frontend</artifactId>

    <name>UIBuilder examples - Example App - Frontend</name>
    <description>Page definitions and controller beans of the UIBuilder Example App</description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.8</java.version>
        <uibuilder.version>2.1.3-SNAPSHOT</uibuilder.version>
    </properties>

    <profiles>
        <profile>
            <id>default</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <dependencies>
                <dependency>
                    <groupId>io.devbench.uibuilder.examples.example-app</groupId>
                    <artifactId>example-repo-jpa</artifactId>
                    <version>${project.version}</version>
                </dependency>
            </dependencies>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-maven-plugin</artifactId>
                        <version>${spring-boot.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>repackage</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>ebean-example-app</id>
            <dependencies>
                <dependency>
                    <groupId>io.devbench.uibuilder.examples.example-app</groupId>
                    <artifactId>example-repo-ebean</artifactId>
                    <version>${project.version}</version>
                </dependency>
            </dependencies>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-maven-plugin</artifactId>
                        <version>${spring-boot.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>jpa-example-app</id>
            <dependencies>
                <dependency>
                    <groupId>io.devbench.uibuilder.examples.example-app</groupId>
                    <artifactId>example-repo-jpa</artifactId>
                    <version>${project.version}</version>
                </dependency>
            </dependencies>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-maven-plugin</artifactId>
                        <version>${spring-boot.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>production</id>

            <dependencies>
                <dependency>
                    <groupId>com.vaadin</groupId>
                    <artifactId>flow-server-production-mode</artifactId>
                </dependency>

                <dependency>
                    <groupId>io.devbench.uibuilder.examples.example-app</groupId>
                    <artifactId>example-repo-jpa</artifactId>
                    <version>${project.version}</version>
                </dependency>
            </dependencies>

            <build>
                <plugins>
                    <plugin>
                        <groupId>com.vaadin</groupId>
                        <artifactId>vaadin-maven-plugin</artifactId>
                        <version>${vaadin.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>prepare-frontend</goal>
                                    <goal>build-frontend</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-maven-plugin</artifactId>
                        <version>${spring-boot.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>repackage</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>cicd</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>com.github.eirslett</groupId>
                        <artifactId>frontend-maven-plugin</artifactId>
                        <version>1.9.1</version>
                        <configuration>
                            <nodeVersion>v12.15.0</nodeVersion>
                        </configuration>
                        <executions>
                            <execution>
                                <phase>generate-resources</phase>
                                <goals>
                                    <goal>install-node-and-npm</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>com.vaadin</groupId>
                        <artifactId>vaadin-maven-plugin</artifactId>
                        <version>${vaadin.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>prepare-frontend</goal>
                                    <goal>build-frontend</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <dependencies>
        <dependency>
            <groupId>io.devbench.uibuilder.examples.example-app</groupId>
            <artifactId>example-api</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- spring deps -->

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-autoconfigure</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-log4j2</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-commons</artifactId>
            <version>${spring-boot.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
        </dependency>

        <!-- UIBuilder dependencies -->

        <dependency>
            <groupId>io.devbench.uibuilder.components</groupId>
            <artifactId>components-all</artifactId>
        </dependency>

        <dependency>
            <groupId>io.devbench.uibuilder.core</groupId>
            <artifactId>uibuilder-core</artifactId>
        </dependency>

        <dependency>
            <groupId>io.devbench.uibuilder.context-management</groupId>
            <artifactId>uibuilder-springboot</artifactId>
        </dependency>

        <dependency>
            <groupId>io.devbench.uibuilder.security</groupId>
            <artifactId>uibuilder-security-spring</artifactId>
        </dependency>

        <dependency>
            <groupId>io.devbench.uibuilder.core.theme</groupId>
            <artifactId>uibuilder-lumo</artifactId>
        </dependency>

        <!-- 3rd party deps -->

        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
        </dependency>

        <dependency>
            <groupId>com.vaadin</groupId>
            <artifactId>vaadin-ordered-layout-flow</artifactId>
        </dependency>

        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator-annotation-processor</artifactId>
        </dependency>

    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.devbench</groupId>
                <artifactId>uibuilder-bom</artifactId>
                <version>${project.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <finalName>example-frontend</finalName>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.vaadin</groupId>
                    <artifactId>vaadin-maven-plugin</artifactId>
                    <version>${vaadin.version}</version>
                    <configuration>
                        <skipEs5>true</skipEs5>
                        <minify>true</minify>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>io.devbench.uibuilder.i18n.plugin</groupId>
                <artifactId>uibuilder-i18n-maven-plugin</artifactId>
                <version>${project.version}</version>
                <executions>
                    <execution>
                        <configuration>
                            <baseTranslationFile>${basedir}/src/main/resources/translations/base.pot</baseTranslationFile>
                            <supportedLanguages>
                                <language>hu</language>
                                <language>en-GB</language>
                                <language>de</language>
                            </supportedLanguages>
                        </configuration>
                        <id>compile-translations</id>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>compile-messages</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>io.devbench.uibuilder.examples.utils</groupId>
                <artifactId>code-block-copy-plugin</artifactId>
                <version>${project.version}</version>
                <executions>
                    <execution>
                        <id>replace-code-blocks</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>copy-code-blocks</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>com.vaadin</groupId>
                <artifactId>vaadin-maven-plugin</artifactId>
                <version>${vaadin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-frontend</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
            </resource>
            <resource>
                <directory>${project.build.directory}/code-blocks</directory>
                <targetPath>static/code-blocks</targetPath>
            </resource>
        </resources>
    </build>

</project>
