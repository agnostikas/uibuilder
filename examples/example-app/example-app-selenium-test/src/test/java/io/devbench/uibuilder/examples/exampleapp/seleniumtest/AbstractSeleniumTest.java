/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.seleniumtest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import static java.nio.file.StandardOpenOption.*;
import static java.time.format.DateTimeFormatter.*;

public abstract class AbstractSeleniumTest {
    protected RemoteWebDriver driver;
    protected WebDriverWait wait;

    @BeforeEach
    void setUp() throws Exception {
        driver = createDriver();
        wait = new WebDriverWait(driver, getTimeOutInSeconds());
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    protected String getBaseUrl() {
        return System.getProperty("test.selenium.baseUrl", "http://example-app/");
    }

    protected RemoteWebDriver createDriver() throws Exception {
        String driverHost = System.getProperty("test.selenium.webDriverHost", "localhost");
        switch (System.getProperty("test.selenium.browserName", "")) {
            case "firefox":
                return new RemoteWebDriver(new URL("http://" + driverHost + ":4445/wd/hub"), new FirefoxOptions());
            default:
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-dev-shm-usage");
                return new RemoteWebDriver(new URL("http://" + driverHost + ":4444/"), options);
        }
    }

    protected long getTimeOutInSeconds() {
        return 5;
    }

    protected void captureScreenshot() {
        try {
            byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
            Path path = Paths.get("target/screenshots/" + ISO_DATE_TIME.format(LocalDateTime.now()) + ".png");
            Files.createDirectories(path.getParent());
            Files.write(path, screenshot, CREATE);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
