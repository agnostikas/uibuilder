/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories;

// CODE BLOCK DEFINITION - CustomTodoItemRepository.java - START

import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import org.springframework.data.ebean.repository.EbeanRepository;
import org.springframework.stereotype.Repository;

@Repository("customTodoItemRepository")
@TargetDataSource(name = "customTodoDS")
public interface CustomTodoItemRepository extends EbeanRepository<TodoItem, String>, TodoItemCustomQuery {

}
// CODE BLOCK DEFINITION END
