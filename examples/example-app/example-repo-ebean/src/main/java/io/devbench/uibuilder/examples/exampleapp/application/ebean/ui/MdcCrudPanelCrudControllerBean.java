/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.ui;

// CODE BLOCK DEFINITION - MdcCrudPanelCrudControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.crudpanel.UIBuilderCrudPanel;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories.TodoItemRepository;
import org.springframework.context.annotation.Scope;

@Scope("session")
@ControllerBean("mdcCrudPanelCrudBean")
public class MdcCrudPanelCrudControllerBean extends MdcNestedCrudControllerBean {

    public MdcCrudPanelCrudControllerBean(TodoItemRepository repository) {
        super(repository);
    }

    @UIEventHandler("onSaveReminderItemByCrudPanel")
    public void onSaveReminderItemByCrudPanel(@Item ReminderItem reminderItem,
                                   @UIComponent("cp") UIBuilderCrudPanel<TodoItem> cp,
                                   @UIComponent("reminder-cp") UIBuilderCrudPanel<ReminderItem> reminderCp) {
        UIBuilderMasterDetailController<TodoItem> mdc = cp.getMasterDetailController();
        UIBuilderMasterDetailController<ReminderItem> reminderMdc = reminderCp.getMasterDetailController();

        if (reminderMdc.isSelectedItemCreated()) {
            reminderItem.setTodoItem(mdc.getSelectedItem());
            reminderMdc.getMasterConnector().addItem(reminderItem);
        }
        reminderMdc.getMasterConnector().refresh();
    }

}
// CODE BLOCK DEFINITION END
