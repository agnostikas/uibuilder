/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.ui;

// CODE BLOCK DEFINITION - MdcReminderCrudControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.ReminderItem;
import io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories.ReminderItemRepository;
import io.devbench.uibuilder.spring.crud.AbstractSpringControllerBean;
import org.springframework.context.annotation.Scope;

import java.time.LocalDate;

@Scope("session")
@ControllerBean("mdcReminderCrudBean")
public class MdcReminderCrudControllerBean extends AbstractSpringControllerBean<ReminderItem, ReminderItemRepository> {

    public MdcReminderCrudControllerBean(ReminderItemRepository repository) {
        super(() -> new ReminderItem("created reminder item", LocalDate.now().plusDays(10)), repository);
    }

}
// CODE BLOCK DEFINITION END
