/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories;

// CODE BLOCK DEFINITION - TodoItemCustomQueryImpl.java - START
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import io.ebean.EbeanServer;
import io.ebean.Expr;
import io.ebean.Query;
import org.springframework.beans.factory.annotation.Autowired;

public class TodoItemCustomQueryImpl implements TodoItemCustomQuery {

    @Autowired
    private EbeanServer ebeanServer;

    @Override
    public Query<TodoItem> findTodoByPrefix() {
        return ebeanServer.find(TodoItem.class).where(Expr.startsWith("todo", "B"));
    }

}
// CODE BLOCK DEFINITION END
