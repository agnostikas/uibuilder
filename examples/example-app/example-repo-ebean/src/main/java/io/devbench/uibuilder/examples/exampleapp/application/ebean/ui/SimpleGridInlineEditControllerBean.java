/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.jpa.ui;

// CODE BLOCK DEFINITION - MdcSimpleCrudControllerBean.java - START

import com.vaadin.flow.component.button.Button;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.crud.GenericCrudGridInlineEditorControllerBean;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.data.common.dataprovidersupport.inlineedit.InlineEditHandler;
import io.devbench.uibuilder.data.common.exceptions.DataSourceItemNotFoundException;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.TodoItem;
import io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

@Scope("session")
@ControllerBean("simpleGridInlineEdit")
public class SimpleGridInlineEditControllerBean {

    @Autowired
    private TodoItemRepository todoItemRepository;

    @Autowired
    private GenericCrudGridInlineEditorControllerBean<TodoItem> genericCrudGridInlineEditorControllerBean;

    @UIEventHandler("registerDataSource")
    public void registerDataSource(@UIComponent("editableGrid") UIBuilderGrid<TodoItem> editableGrid) {
        genericCrudGridInlineEditorControllerBean.registerDataSourceName(editableGrid.getId().get(), "todoDS");
    }

    @UIEventHandler("allEditMode")
    public void onAllEditMode(@UIComponent("editableGrid") UIBuilderGrid<TodoItem> editableGrid) {
        InlineEditHandler inlineEditHandler = editableGrid.getInlineEditHandler();
        inlineEditHandler.clearEditModes();
        inlineEditHandler.setEditModeDefault(true);
        editableGrid.refresh();
    }

    @Transactional
    @UIEventHandler("saveAllEdited")
    public void onSaveAllEdited(@UIComponent("editableGrid") UIBuilderGrid<TodoItem> editableGrid) {
        editableGrid.withDataSource(dataSource -> {
            InlineEditHandler inlineEditHandler = editableGrid.getInlineEditHandler();
            inlineEditHandler
                .editedItems()
                .forEach(jsonItem -> {
                    TodoItem todoItem = dataSource.findItem(jsonItem)
                        .orElseThrow(() -> new DataSourceItemNotFoundException("Could not find item by json: " + jsonItem));
                    inlineEditHandler.applyChanges(todoItem, jsonItem);
                    inlineEditHandler.setEditMode(jsonItem, false);
                    todoItemRepository.save(todoItem);
                });
            editableGrid.refresh();
        });
    }

    @UIEventHandler("toggleDisabled")
    public void onToggleDisabled(@UIComponent("editableGrid") UIBuilderGrid<TodoItem> editableGrid,
                                 @UIComponent("toggleBtn") Button toggleBtn) {

        if ("Enable grid".equals(toggleBtn.getText())) {
            toggleBtn.setText("Disable grid");
            editableGrid.setEnabled(true);
        } else {
            toggleBtn.setText("Enable grid");
            editableGrid.setEnabled(false);
        }
    }

}
// CODE BLOCK DEFINITION END
