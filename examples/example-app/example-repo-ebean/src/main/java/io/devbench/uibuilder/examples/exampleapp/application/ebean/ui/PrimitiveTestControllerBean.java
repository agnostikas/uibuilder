/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.examples.exampleapp.application.ebean.ui;

// CODE BLOCK DEFINITION - PrimitiveTestControllerBean.java - START
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.examples.exampleapp.application.api.entites.PrimitiveTest;
import io.devbench.uibuilder.examples.exampleapp.application.ebean.repositories.PrimitiveTestRepository;
import io.devbench.uibuilder.spring.crud.AbstractSpringControllerBean;

import java.util.Arrays;
import java.util.List;

@ControllerBean("primitiveTestCb")
public class PrimitiveTestControllerBean extends AbstractSpringControllerBean<PrimitiveTest, PrimitiveTestRepository> {

    public PrimitiveTestControllerBean(PrimitiveTestRepository repository) {
        super(PrimitiveTest::new, repository);
    }

    public List<String> getDataOneItems() {
        return Arrays.asList("Item 1", "Item 2", "Item 3");
    }

    public List<String> getDataTwoItems() {
        return Arrays.asList("Item One", "Item Two", "Item Three");
    }

    public List<String> getDataThreeItems() {
        return Arrays.asList("Item I", "Item II", "Item III");
    }

}
// CODE BLOCK DEFINITION END
