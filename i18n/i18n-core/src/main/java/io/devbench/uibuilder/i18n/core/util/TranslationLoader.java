/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core.util;

import io.devbench.uibuilder.i18n.core.dto.TranslationCatalogId;
import org.apache.commons.lang3.tuple.Pair;
import org.avaje.classpath.scanner.FilterResource;
import org.avaje.classpath.scanner.Resource;
import org.avaje.classpath.scanner.core.Scanner;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.PoParser;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TranslationLoader {

    private static final Pattern LANGUAGE_HEADER_PATTERN = Pattern.compile("Language: (.+)\\n");
    private static final Logger logger = LoggerFactory.getLogger(TranslationLoader.class);

    public static Map<TranslationCatalogId, Catalog> loadTranslations(String pathToScan) {
        return new Scanner(Thread.currentThread().getContextClassLoader())
            .scanForResources(pathToScan, FilterResource.bySuffix(".po"))
            .stream()
            .flatMap(TranslationLoader::getResourcesWithName)
            .map(TranslationLoader::loadTranslation)
            .filter(pair -> pair.getKey() != null)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, TranslationLoader::merge));
    }

    private static Stream<Pair<Resource, URL>> getResourcesWithName(Resource resource) {
        try {
            List<URL> resources = new ArrayList<>();
            Enumeration<URL> systemResources = Thread.currentThread().getContextClassLoader().getResources(resource.getLocation());
            while (systemResources.hasMoreElements()) {
                resources.add(systemResources.nextElement());
            }
            Collections.reverse(resources);
            return resources
                .stream()
                .map(url -> Pair.of(resource, url));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Catalog merge(Catalog catalog1, Catalog catalog2) {
        for (Message message : catalog2) {
            catalog1.addMessage(message);
        }
        return catalog1;
    }

    private static Pair<TranslationCatalogId, Catalog> loadTranslation(Pair<Resource, URL> resource) {
        String componentId = getComponentIdForTranslation(resource.getLeft());
        try (
            InputStreamReader is = new InputStreamReader(resource.getRight().openStream(), StandardCharsets.UTF_8)
        ) {
            Map.Entry<Locale, Catalog> entry = loadTranslation(is, false);
            return Pair.of(
                new TranslationCatalogId(componentId, entry.getKey()),
                entry.getValue()
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getComponentIdForTranslation(Resource resource) {
        Path translationPath = Paths.get(resource.getLocation());
        if (translationPath.getNameCount() > 2) {
            return translationPath.getName(translationPath.getNameCount() - 2).toString();
        } else {
            return "";
        }
    }

    public static Map.Entry<Locale, Catalog> loadTranslation(Reader reader, boolean isPot) {
        Catalog catalog = parseFileToCatalog(reader, isPot);
        Locale locale = extractLocaleFromHeader(catalog);
        return new HashMap.SimpleEntry<>(locale, catalog);
    }

    private static Catalog parseFileToCatalog(Reader reader, boolean isPot) {
        PoParser poParser = new PoParser();
        return poParser.parseCatalog(reader, isPot);
    }

    @Nullable
    public static Locale extractLocaleFromHeader(Catalog messages) {
        return Optional.ofNullable(messages.locateHeader())
            .map(Message::getMsgstr)
            .map(LANGUAGE_HEADER_PATTERN::matcher)
            .filter(Matcher::find)
            .map(matcher -> matcher.group(1))
            .map(Locale::forLanguageTag)
            .orElse(null);
    }
}
