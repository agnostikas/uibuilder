/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core.wrappers;

import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.MessageHashKey;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

public class UnmodifiableCatalog {

    @NotNull
    private final Catalog catalog;

    public UnmodifiableCatalog(@NotNull Catalog catalog) {
        this.catalog = Objects.requireNonNull(catalog);
    }


    public int size() {
        return catalog.size();
    }

    public UnmodifiableMessage locateMessage(MessageHashKey key) {
        return new UnmodifiableMessage(catalog.locateMessage(key));
    }

    public UnmodifiableMessage locateMessage(String msgctx, String msgid) {
        return new UnmodifiableMessage(catalog.locateMessage(msgctx, msgid));
    }

    public void forEach(Consumer<? super UnmodifiableMessage> action) {
        catalog.processMessages(
            message -> action.accept(new UnmodifiableMessage(message))
        );
    }

    public Optional<UnmodifiableMessage> locateHeader() {
        return Optional.ofNullable(catalog.locateHeader()).map(UnmodifiableMessage::new);
    }
}
