/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core.wrappers;

import org.fedorahosted.tennera.jgettext.Message;

import java.util.List;

public class UnmodifiableMessage {

    private final Message message;

    public UnmodifiableMessage(Message message) {
        this.message = message;
    }


    public String getMsgstr() {
        return message.getMsgstr();
    }

    public boolean isHeader() {
        return message.isHeader();
    }

    public String getMsgid() {
        return message.getMsgid();
    }

    public List<String> getMsgstrPlural() {
        return message.getMsgstrPlural();
    }

    public String getMsgctxt() {
        return message.getMsgctxt();
    }
}
