/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core.text;

import io.devbench.uibuilder.i18n.core.I;

import java.sql.Date;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.time.*;
import java.util.TimeZone;

public class Java8DateFormatWrapper extends Format {
    private final DateFormat format;

    public Java8DateFormatWrapper(DateFormat format) {
        this.format = format;
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        format.setTimeZone(TimeZone.getTimeZone(I.getActiveTimeZoneId()));
        return format.format(convertParameterToOldDate(obj), toAppendTo, pos);
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        return format.parseObject(source, pos);
    }

    private Object convertParameterToOldDate(final Object parameter) {
        if (parameter instanceof Instant) {
            return Date.from((Instant) parameter);
        } else if (parameter instanceof LocalDate) {
            LocalDate localDate = (LocalDate) parameter;
            return Date.from(localDate.atStartOfDay(I.getActiveTimeZoneId()).toInstant());
        } else if (parameter instanceof LocalDateTime) {
            LocalDateTime localDateTime = (LocalDateTime) parameter;
            return Date.from(localDateTime.atZone(I.getActiveTimeZoneId()).toInstant());
        } else if (parameter instanceof ZonedDateTime) {
            ZonedDateTime zonedDateTime = (ZonedDateTime) parameter;
            return Date.from(zonedDateTime.toInstant());
        } else if (parameter instanceof LocalTime) {
            LocalTime localTime = (LocalTime) parameter;
            return Date.from(localTime.atDate(LocalDate.now()).atZone(I.getActiveTimeZoneId()).toInstant());
        }else {
            return parameter;
        }
    }
}
