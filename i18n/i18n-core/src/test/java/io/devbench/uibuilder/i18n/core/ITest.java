/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core;

import io.devbench.uibuilder.i18n.core.wrappers.UnmodifiableCatalog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnJre;
import org.junit.jupiter.api.condition.JRE;

import java.time.*;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ITest {

    @Nested
    @DisplayName("Should Read Localizations From File Correctly")
    class ShouldReadLocalizationsFromFileCorrectly {
        @BeforeEach
        public void setup() {
            I.setLanguageProvider(() -> new Locale("hu"));
            I.loadTranslations("/BasicTranslationTests");
        }

        @Test
        public void should_translate_existing_simple_string_correctly() {
            String actual = I.tr("Hello world!");
            assertEquals("Sziasztok emberek!", actual);
        }

        @Test
        public void should_return_original_string_on_untranslated_messages() {
            String actual = I.tr("Untranslated string");
            assertEquals("Untranslated string", actual);
        }

        @Test
        public void should_return_original_string_on_untranslated_formatted_messages() {
            String actual = I.trf("Untranslated string");
            assertEquals("Untranslated string", actual);
        }

        @Test
        public void should_not_fail_on_not_existing_translation() {
            String actual = I.tr("Not existing translation");
            assertEquals("Not existing translation", actual);
        }

        @Test
        public void should_not_fail_on_not_existing_language() {
            I.setLanguageProvider(() -> Locale.TRADITIONAL_CHINESE);
            String actual = I.tr("Hello world!");
            assertEquals("Hello world!", actual);
        }

        @Test
        public void should_translate_existing_string_with_string_parameter_correctly() {
            String actual = I.trf("Hello {0}!", "Vilmos");
            assertEquals("Szia Vilmos!", actual);
        }

        @Test
        public void should_read_utf8_strings_correctly_from_translation_file() {
            String actual = I.tr("some utf8 string");
            assertEquals("árvíztűrő tükörfúrógép", actual);
        }

        @Test
        public void should_read_plural_forms_correctly() {
            String plural = I.trfp(
                "There is {0,number} orange in the {1}.",
                "There are {0,number} oranges in the {1}.",
                3, 3, "kosár"
            );
            String singular = I.trfp(
                "There is {0,number} orange in the {1}.",
                "There are {0,number} oranges in the {1}.",
                1, 1, "kosár"
            );

            assertEquals("Egy narancs van a kosárban.", singular);
            assertEquals("3 narancs van a kosárban.", plural);
        }

        @Test
        public void should_read_plural_forms_correctly_when_the_item_count_is_negative() {
            String plural = I.trfp(
                "There is {0,number} orange in the {1}.",
                "There are {0,number} oranges in the {1}.",
                -5, -5, "kosár"
            );

            assertEquals("-5 narancs van a kosárban.", plural);
        }
    }

    @Nested
    @DisplayName("Should handle Java8 DateTime API correctly")
    class ShouldHandleJava8DateTimeAPICorrectly {
        @BeforeEach
        public void setup() {
            I.setLanguageProvider(() -> Locale.ENGLISH);
            I.setTimeZoneProvider(() -> ZoneId.of("Europe/Budapest"));
        }

        @Test
        public void should_format_instant_correctly_if_the_format_contains_parameter_as_general() {
            Instant instant = Instant.ofEpochSecond(739120868L);
            String actual = I.trf("I was born at {0}.", instant);
            assertEquals("I was born at 1993-06-03T15:21:08Z.", actual);
        }

        @Test
        public void should_format_instant_correctly_if_the_format_contains_parameter_as_time() {
            Instant instant = Instant.ofEpochSecond(739120868L);
            String actual = I.trf("I was born at {0, date, full} {0, time, short}.", instant);
            assertEquals("I was born at Thursday, June 3, 1993 5:21 PM.", actual);
        }

        @Test
        public void should_format_instant_correctly_if_the_format_contains_parameter_as_time_and_as_general() {
            Instant instant = Instant.ofEpochSecond(739120868L);
            String actual;

            actual = I.trf("I was born at {0} or {0, date, full}.", instant);
            assertEquals("I was born at 1993-06-03T15:21:08Z or Thursday, June 3, 1993.", actual);

            actual = I.trf("I was born at {0, date, full} or {0}.", instant);
            assertEquals("I was born at Thursday, June 3, 1993 or 1993-06-03T15:21:08Z.", actual);
        }

        @Test
        public void should_format_localdate_correctly_if_the_format_contains_parameter_as_general() {
            LocalDate localDate = LocalDate.of(1993, 6, 3);
            String actual = I.trf("I was born at {0}.", localDate);
            assertEquals("I was born at 1993-06-03.", actual);
        }

        @Test
        public void should_format_localdate_correctly_if_the_format_contains_parameter_as_date() {
            LocalDate localDate = LocalDate.of(1993, 6, 3);
            String actual = I.trf("I was born at {0, date, full}.", localDate);
            assertEquals("I was born at Thursday, June 3, 1993.", actual);
        }

        @Test
        public void should_format_localtime_correctly_if_the_format_contains_parameter_as_general() {
            LocalTime localTime = LocalTime.of(12, 0, 0);
            String actual = I.trf("The current time is {0}.", localTime);
            assertEquals("The current time is 12:00.", actual);
        }

        @Test
        @DisabledOnJre(JRE.JAVA_11)
        public void should_format_localtime_correctly_if_the_format_contains_parameter_as_date() {
            LocalTime localTime = LocalTime.of(12, 0, 0);
            String actual = I.trf("The current time is {0, time, full}.", localTime);

            LocalDateTime now = LocalDateTime.now();
            ZonedDateTime zdt = ZonedDateTime.of(now, ZoneId.systemDefault());
            if (zdt.getZone().getRules().isDaylightSavings(now.toInstant(zdt.getOffset()))) {
                assertEquals("The current time is 12:00:00 PM CEST.", actual);
            } else {
                assertEquals("The current time is 12:00:00 PM CET.", actual);
            }
        }

        @Test
        public void should_format_localdatetime_correctly_if_the_format_contains_parameter_as_general() {
            LocalDateTime localDate = LocalDateTime.of(1993, 6, 3, 1, 1);
            String actual = I.trf("I was born at {0}.", localDate);
            assertEquals("I was born at 1993-06-03T01:01.", actual);
        }

        @Test
        @DisabledOnJre(JRE.JAVA_11)
        public void should_format_localdatetime_correctly_if_the_format_contains_parameter_as_date() {
            LocalDateTime localDate = LocalDateTime.of(1993, 6, 3, 1, 1);
            String actual = I.trf("I was born at {0, date, full} {0, time, full}.", localDate);
            assertEquals("I was born at Thursday, June 3, 1993 1:01:00 AM CEST.", actual);
        }

        @Test
        public void should_format_zoneddatetime_correctly_if_the_format_contains_parameter_as_general() {
            ZonedDateTime zonedDateTime = ZonedDateTime.of(1993, 6, 3, 1, 1, 0, 0, ZoneId.of("Asia/Bangkok"));
            String actual = I.trf("I was born at {0}.", zonedDateTime);
            assertEquals("I was born at 1993-06-03T01:01+07:00[Asia/Bangkok].", actual);
        }

        @Test
        @DisabledOnJre(JRE.JAVA_11)
        public void should_format_zoneddatetime_correctly_if_the_format_contains_parameter_as_date() {
            ZonedDateTime zonedDateTime = ZonedDateTime.of(2018, 7, 6, 4, 0, 0, 0, ZoneId.of("Asia/Bangkok"));
            String actual = I.trf("I was born at {0, date, full} {0, time, full}.", zonedDateTime);
            assertEquals("I was born at Thursday, July 5, 2018 11:00:00 PM CEST.", actual);
        }
    }

    @Nested
    @DisplayName("Should handle conflicting module translations correctly")
    class ShouldHandleConflictingModuleTranslationsCorrectly {

        @BeforeEach
        public void setup() {
            I.setLanguageProvider(() -> new Locale("hu"));
        }

        @Test
        public void should_return_correct_message_for_non_conflicting_text() {
            I.loadTranslations("/ConflictingModuleTests/non-conflicting");
            String actual = I.tr("Hello world!");
            assertEquals("Sziasztok emberek!", actual);
        }

        @Test
        public void should_read_translations_from_multiple_file_correctly_and_merge_them_if_conflicting() {
            I.loadTranslations("/ConflictingModuleTests/conflicting");

            assertEquals("Sziasztok emberek!", I.tr("Hello world!"));
            assertFalse(I.trf("The time is {0, time, short}.", LocalDateTime.now()).startsWith("The time is"));
        }
    }

    @Nested
    @DisplayName("Should provide all translation for a given component correctly")
    class ShouldProvideAllTranslationForAGivenComponentCorrectly {

        @BeforeEach
        public void setup() {
            I.setLanguageProvider(() -> new Locale("hu"));
        }

        @Test
        public void should_return_unmodifiable_catalog_when_language_is_matched_exactly() {
            I.loadTranslations("/ConflictingModuleTests/non-conflicting");
            Map<String, UnmodifiableCatalog> bestCatalogsForComponentIds = I.findBestCatalogsForComponentIds(Collections.singleton("module-a"));

            UnmodifiableCatalog actualCatalog = bestCatalogsForComponentIds.get("module-a");
            assertNotNull(actualCatalog);
            assertEquals(2, actualCatalog.size());
        }
    }

    @Nested
    @DisplayName("Should lookup translations based on RFC-4647")
    class ShouldLookUpTranslationsBasedOnRfc {

        @Test
        public void should_find_hu_hu_translation_for_hungarian_locale() {
            I.setLanguageProvider(() -> new Locale("hu", "HU"));
            I.loadTranslations("/LookupBasedOnRFC");

            assertEquals("Sziasztok emberek!", I.tr("Hello world!"));
            assertEquals("Szervusz magyar polgár!", I.trc("The country of the Locale is unknown.", "Hello unknown citizen."));
        }

        @Test
        public void when_finding_catalog_based_on_component_id_should_return_best_matching_one() {
            I.setLanguageProvider(() -> new Locale("hu", "HU"));
            I.loadTranslations("/LookupBasedOnRFC");
            Map<String, UnmodifiableCatalog> bestCatalogsForComponentIds = I.findBestCatalogsForComponentIds(Collections.singleton("component-a"));

            UnmodifiableCatalog actualCatalog = bestCatalogsForComponentIds.get("component-a");
            assertNotNull(actualCatalog);
            assertEquals(3, actualCatalog.size());
        }

        @Test
        public void should_not_add_component_to_retlist_when_no_translation_for_component() {
            I.setLanguageProvider(() -> new Locale("hu", "HU"));
            I.loadTranslations("/LookupBasedOnRFC");
            Map<String, UnmodifiableCatalog> bestCatalogsForComponentIds = I.findBestCatalogsForComponentIds(Collections.singleton("non-existing-component"));

            assertTrue(bestCatalogsForComponentIds.isEmpty());
        }

    }
}
