/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.core.util;

import io.devbench.uibuilder.i18n.core.dto.TranslationCatalogId;
import io.devbench.uibuilder.i18n.testtools.MessageUtil;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class TranslationLoaderTest {

    @Test
    public void should_not_fail_on_empty_directory() {
        Map<TranslationCatalogId, Catalog> localeCatalogMap = TranslationLoader.loadTranslations("/not-existing-directory");
        assertEquals(Collections.emptyMap(), localeCatalogMap);
    }

    @Test
    public void should_read_translations_from_single_file_correctly() {
        Map<TranslationCatalogId, Catalog> localeCatalogMap = TranslationLoader.loadTranslations("/ConflictingModuleTests/non-conflicting");
        Catalog moduleACatalog = localeCatalogMap.get(new TranslationCatalogId("module-a", new Locale("hu")));

        Message actual = moduleACatalog.locateMessage(new MessageHashKey(null, "Hello world!"));
        Message expected = new Message();
        expected.setMsgid("Hello world!");
        expected.setMsgstr("Sziasztok emberek!");
        Assertions.assertTrue(MessageUtil.messageEquals(expected, actual));

        assertEquals(2, moduleACatalog.size());
    }

    @Test
    public void should_read_translations_from_multiple_file_correctly_and_merge_them_if_conflicting() {
        Map<TranslationCatalogId, Catalog> localeCatalogMap = TranslationLoader.loadTranslations("/ConflictingModuleTests/conflicting");
        Catalog moduleBCatalog = localeCatalogMap.get(new TranslationCatalogId("module-b", new Locale("hu")));

        Message moduleBActual = moduleBCatalog.locateMessage(new MessageHashKey(null, "The time is {0, time, short}."));
        Message moduleBExpected = new Message();
        moduleBExpected.setMsgid("The time is {0, time, short}.");
        moduleBExpected.setMsgstr("A pontos idő {0, time, short}.");
        assertTrue(MessageUtil.messageEquals(moduleBExpected, moduleBActual));

        Catalog moduleCCatalog = localeCatalogMap.get(new TranslationCatalogId("module-c", new Locale("hu")));

        Message moduleCActual = moduleCCatalog.locateMessage(new MessageHashKey(null, "The time is {0, time, short}."));
        Message moduleCExpected = new Message();
        moduleCExpected.setMsgid("The time is {0, time, short}.");
        moduleCExpected.setMsgstr("Az idő {0, time, short}.");
        assertTrue(MessageUtil.messageEquals(moduleCExpected, moduleCActual));
    }
}
