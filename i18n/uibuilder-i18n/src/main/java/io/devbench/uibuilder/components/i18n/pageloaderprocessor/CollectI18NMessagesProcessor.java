/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.i18n.pageloaderprocessor;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.page.pageloaderprocessors.PageLoaderProcessor;
import io.devbench.uibuilder.i18n.core.I;
import io.devbench.uibuilder.i18n.core.wrappers.UnmodifiableCatalog;
import io.devbench.uibuilder.i18n.core.wrappers.UnmodifiableMessage;
import org.jsoup.nodes.Element;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class CollectI18NMessagesProcessor extends PageLoaderProcessor {

    private static final String NO_CONTEXT_STRING = "_NO_CONTEXT_";

    private final Function<Set<String>, Map<String, UnmodifiableCatalog>> catalogProvider;
    private final Supplier<Page> pageProvider;

    public CollectI18NMessagesProcessor() {
        this.catalogProvider = I::findBestCatalogsForComponentIds;
        this.pageProvider = () -> UI.getCurrent().getPage();
    }

    CollectI18NMessagesProcessor(Function<Set<String>, Map<String, UnmodifiableCatalog>> catalogProvider, Supplier<Page> pageProvider) {
        this.catalogProvider = catalogProvider;
        this.pageProvider = pageProvider;
    }

    @Override
    public void process(PageLoaderContext context) {
        Set<String> elementIds = collectElementIds(context);
        Map<String, UnmodifiableCatalog> catalogsForEachElement = catalogProvider.apply(elementIds);
        pageProvider
            .get()
            .executeJs(
                "window.Uibuilder && window.Uibuilder.i18n && window.Uibuilder.i18n.mergeMessages && window.Uibuilder.i18n.mergeMessages($0)",
                createAppropriateJsonObjectForTranslations(catalogsForEachElement)
            );
    }

    private JsonObject createAppropriateJsonObjectForTranslations(Map<String, UnmodifiableCatalog> catalogsForEachElement) {
        JsonObject translations = Json.createObject();
        catalogsForEachElement.forEach((componentId, catalog) -> {
            if (translations.getObject(componentId) == null) {
                translations.put(componentId, Json.createObject());
            }
            catalog.forEach(message -> {
                if (!message.isHeader()) {
                    String context = Optional.ofNullable(message.getMsgctxt()).orElse(NO_CONTEXT_STRING);
                    if (translations.getObject(componentId).getObject(context) == null) {
                        translations.getObject(componentId).put(context, Json.createObject());
                    }
                    translations.getObject(componentId).getObject(context).put(message.getMsgid(), createTranslatedJsonObject(message));
                }
            });
        });
        return translations;
    }

    private JsonValue createTranslatedJsonObject(UnmodifiableMessage message) {
        if (message.getMsgstrPlural() != null && !message.getMsgstrPlural().isEmpty()) {
            JsonArray retVal = Json.createArray();
            message.getMsgstrPlural().forEach(plural -> retVal.set(retVal.length(), plural));
            return retVal;
        } else {
            return Json.create(message.getMsgstr());
        }
    }

    private Set<String> collectElementIds(PageLoaderContext context) {
        return getAllChild(context.getPageElement())
            .map(Element::tagName)
            .collect(Collectors.toSet());
    }

    private Stream<Element> getAllChild(Element element) {
        return element != null ?
            Stream.concat(
                Stream.of(element),
                element.children().stream()
                    .flatMap(this::getAllChild)
            ) :
            Stream.empty();
    }

}
