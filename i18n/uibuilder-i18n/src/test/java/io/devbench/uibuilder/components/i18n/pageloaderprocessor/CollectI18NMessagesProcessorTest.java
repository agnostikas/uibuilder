/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.components.i18n.pageloaderprocessor;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.page.Page;
import elemental.json.Json;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.i18n.core.wrappers.UnmodifiableCatalog;
import io.devbench.uibuilder.test.JsonAssert;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.PoParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;

import java.io.Serializable;
import java.io.StringReader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class CollectI18NMessagesProcessorTest {

    public static final String EXPECTED_FUNCTION_CALL =
        "window.Uibuilder && window.Uibuilder.i18n && window.Uibuilder.i18n.mergeMessages && window.Uibuilder.i18n.mergeMessages($0)";

    private CollectI18NMessagesProcessor testObj;

    @Test
    @ExtendWith(ResourceReaderExtension.class)
    public void should_push_messages_to_frontend_without_context_correctly(
        @ReadResource("/CollectI18NMessageProcessorTest/should_push_messages_to_frontend_without_context_correctly.po") String poFile,
        @ReadResource("/CollectI18NMessageProcessorTest/should_push_messages_to_frontend_without_context_correctly_expected.json") String expectedJson
    ) {
        Page page = mock(Page.class);
        PageLoaderContext pageLoaderContext = mock(PageLoaderContext.class);
        Element element = mock(Element.class);
        Catalog catalog = new PoParser().parseCatalog(
            new StringReader(poFile),
            false
        );
        ArgumentCaptor<Serializable> jsArguments = ArgumentCaptor.forClass(Serializable.class);


        when(pageLoaderContext.getPageElement()).thenReturn(element);
        when(element.tagName()).thenReturn("mock-element");
        when(element.children()).thenReturn(new Elements(Collections.emptyList()));

        testObj = new CollectI18NMessagesProcessor(
            componentIds -> Collections.singletonMap(
                "mock-element",
                new UnmodifiableCatalog(catalog)
            ),
            () -> page
        );

        testObj.process(pageLoaderContext);

        verify(page).executeJs(eq(EXPECTED_FUNCTION_CALL), jsArguments.capture());

        JsonObject expected = Json.parse(expectedJson);
        JsonAssert.assertJsonEquals(expected, (JsonValue) jsArguments.getAllValues().get(0));
    }

    @Test
    @ExtendWith(ResourceReaderExtension.class)
    public void should_push_messages_to_frontend_with_context_correctly(
        @ReadResource("/CollectI18NMessageProcessorTest/should_push_messages_to_frontend_with_context_correctly.po") String poFile,
        @ReadResource("/CollectI18NMessageProcessorTest/should_push_messages_to_frontend_with_context_correctly_expected.json") String expectedJson
    ) {
        Page page = mock(Page.class);
        PageLoaderContext pageLoaderContext = mock(PageLoaderContext.class);
        Element element = mock(Element.class);
        Catalog catalog = new PoParser().parseCatalog(
            new StringReader(poFile),
            false
        );
        ArgumentCaptor<Serializable> jsArguments = ArgumentCaptor.forClass(Serializable.class);


        when(pageLoaderContext.getPageElement()).thenReturn(element);
        when(element.tagName()).thenReturn("mock-element");
        when(element.children()).thenReturn(new Elements(Collections.emptyList()));

        testObj = new CollectI18NMessagesProcessor(
            componentIds -> Collections.singletonMap(
                "mock-element",
                new UnmodifiableCatalog(catalog)
            ),
            () -> page
        );

        testObj.process(pageLoaderContext);

        verify(page).executeJs(eq(EXPECTED_FUNCTION_CALL), jsArguments.capture());

        JsonObject expected = Json.parse(expectedJson);
        JsonAssert.assertJsonEquals(expected, (JsonValue) jsArguments.getAllValues().get(0));
    }

    @Test
    @ExtendWith(ResourceReaderExtension.class)
    public void should_push_plural_messages_to_frontend_without_context_correctly(
        @ReadResource("/CollectI18NMessageProcessorTest/should_push_plural_messages_to_frontend_correctly.po") String poFile,
        @ReadResource("/CollectI18NMessageProcessorTest/should_push_plural_messages_to_frontend_correctly_expected.json") String expectedJson
    ) {
        Page page = mock(Page.class);
        PageLoaderContext pageLoaderContext = mock(PageLoaderContext.class);
        Element element = mock(Element.class);
        Catalog catalog = new PoParser().parseCatalog(
            new StringReader(poFile),
            false
        );
        ArgumentCaptor<Serializable> jsArguments = ArgumentCaptor.forClass(Serializable.class);


        when(pageLoaderContext.getPageElement()).thenReturn(element);
        when(element.tagName()).thenReturn("mock-element");
        when(element.children()).thenReturn(new Elements(Collections.emptyList()));

        testObj = new CollectI18NMessagesProcessor(
            componentIds -> Collections.singletonMap(
                "mock-element",
                new UnmodifiableCatalog(catalog)
            ),
            () -> page
        );

        testObj.process(pageLoaderContext);

        verify(page).executeJs(eq(EXPECTED_FUNCTION_CALL), jsArguments.capture());

        JsonObject expected = Json.parse(expectedJson);
        JsonAssert.assertJsonEquals(expected, (JsonValue) jsArguments.getAllValues().get(0));
    }

    @Test
    @ExtendWith(ResourceReaderExtension.class)
    public void should_extract_all_component_ids_correctly(
        @ReadResource("/CollectI18NMessageProcessorTest/should_extract_all_component_ids_correctly/example.html") String examplePage
    ) {
        Page page = mock(Page.class);
        PageLoaderContext pageLoaderContext = mock(PageLoaderContext.class);
        Collection<String> componentIds = new HashSet<>();

        when(pageLoaderContext.getPageElement()).thenReturn(Jsoup.parse(examplePage).getElementById("page"));

        testObj = new CollectI18NMessagesProcessor(
            ids -> {
                componentIds.addAll(ids);
                return Collections.emptyMap();
            },
            () -> page
        );

        testObj.process(pageLoaderContext);

        assertTrue(componentIds.containsAll(Sets.newHashSet("uibuilder-editor-window", "uibuilder-page")));
    }
}
