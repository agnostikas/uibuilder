/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core;

import io.devbench.uibuilder.i18n.plugin.core.services.PoFileService;
import io.devbench.uibuilder.i18n.testtools.MessageUtil;
import io.devbench.uibuilder.test.CollectionAssert;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MessageMergerTest {

    @Mock
    private PoFileService poFileService;

    private Path baseMessagePath;
    private MessageMerger testObj;

    @BeforeEach
    public void setup() throws IOException {
        MockitoAnnotations.initMocks(this);
        baseMessagePath = Files.createTempFile("baseMessagePath", "");
        testObj = new MessageMerger(poFileService, baseMessagePath);
    }


    @Test
    public void should_add_new_messages_to_translated_ones_correctly() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslation("Hello world!", ""),
            createMessageWithTranslation("Apply", "")
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!")
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Arrays.asList(
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!"),
            createMessageWithTranslation("Apply", "")
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), false);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    @Test
    public void should_keep_the_header_unmodified_in_the_translated_message() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslation("Hello world!", "")
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslation("", "Some random header"),
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!")
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Arrays.asList(
            createMessageWithTranslation("", "Some random header"),
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!")
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), false);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    @Test
    public void should_not_remove_translated_lines_when_its_removed_from_template() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslation("Hello world!", ""),
            createMessageWithTranslation("Apply", "")
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!"),
            createMessageWithTranslation("Cancel", "Mégse")
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Arrays.asList(
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!"),
            createMessageWithTranslation("Apply", ""),
            createMessageWithTranslation("Cancel", "Mégse")
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), true);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    @Test
    public void should_remove_translated_lines_when_its_removed_from_template() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslation("Hello world!", ""),
            createMessageWithTranslation("Apply", "")
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!"),
            createMessageWithTranslation("Cancel", "Mégse")
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Arrays.asList(
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!"),
            createMessageWithTranslation("Apply", "")
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), false);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    @Test
    public void should_add_comments_to_translated_message_when_new_comment_appears_in_the_template() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslationAndComments("Hello world!", "", Collections.singletonList("new comment"), Collections.emptyList())
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslation("Hello world!", "Sziasztok emberek!")
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Collections.singletonList(
            createMessageWithTranslationAndComments("Hello world!", "Sziasztok emberek!", Collections.singletonList("new comment"), Collections.emptyList())
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), false);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    @Test
    public void should_not_delete_existing_comments_from_translated_message_when_comment_disappears_in_the_template() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslation("Hello world!", "")
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslationAndComments("Hello world!", "Sziasztok emberek!", Collections.singletonList("new comment"), Collections.emptyList())
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Collections.singletonList(
            createMessageWithTranslationAndComments("Hello world!", "Sziasztok emberek!", Collections.singletonList("new comment"), Collections.emptyList())
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), false);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    @Test
    public void should_change_source_ref_comments_to_translated_message_when_new_source_ref_comment_appears_in_the_template() {
        Catalog baseTemplate = createCatalogWithMessages(true,
            createMessageWithTranslationAndComments("Hello world!", "", Collections.singletonList("new comment"), Collections.singletonList("fooFile:90"))
        );

        Catalog hungarianCatalog = createCatalogWithMessages(false,
            createMessageWithTranslationAndComments("Hello world!", "Sziasztok emberek!", Collections.emptyList(), Collections.singletonList("fooFile:88"))
        );

        when(poFileService.getMessageTemplate()).thenReturn(baseTemplate);
        when(poFileService.readPoFileForLanguage(new Locale("hu"))).thenReturn(hungarianCatalog);

        verifyThatCatalogContains(Collections.singletonList(
            createMessageWithTranslationAndComments("Hello world!", "Sziasztok emberek!",
                Collections.singletonList("new comment"), Collections.singletonList("fooFile:90"))
        ));

        testObj.mergeMessages(Collections.singletonList("hu"), false);

        verify(poFileService).writeCatalogTo(any(Catalog.class));
    }

    private Catalog createCatalogWithMessages(boolean template, Message... messages) {
        Catalog baseTemplate = new Catalog(template);
        for (Message message : messages) {
            baseTemplate.addMessage(message);
        }
        return baseTemplate;
    }

    private void verifyThatCatalogContains(List<Message> expectedMessages) {
        doAnswer(i -> {
            Catalog actualCatalog = (Catalog) i.getArguments()[0];
            Set<Message> actualMessages = StreamSupport
                .stream(Spliterators.spliteratorUnknownSize(actualCatalog.iterator(), Spliterator.IMMUTABLE), false)
                .collect(Collectors.toSet());
            CollectionAssert.assertContainsSame(
                expectedMessages,
                actualMessages,
                MessageUtil::messageEquals
            );
            return null;
        }).when(poFileService).writeCatalogTo(any());
    }

    private Message createMessageWithTranslation(String msgid, String msgstr) {
        Message message = new Message();
        message.setMsgid(msgid);
        message.setMsgstr(msgstr);
        return message;
    }

    private Message createMessageWithTranslationAndComments(
        String msgid,
        String msgstr,
        List<String> comments,
        List<String> sourceComments
    ) {
        Message message = createMessageWithTranslation(msgid, msgstr);
        comments.forEach(message::addComment);
        sourceComments.forEach(message::addSourceReference);
        return message;
    }

}
