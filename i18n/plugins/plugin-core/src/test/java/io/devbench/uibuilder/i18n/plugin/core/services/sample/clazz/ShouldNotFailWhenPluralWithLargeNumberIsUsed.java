/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services.sample.clazz;

import io.devbench.uibuilder.i18n.core.I;

public class ShouldNotFailWhenPluralWithLargeNumberIsUsed {

    public static void main(String[] args) {
        // this constant has to be larger than Short.MAX_VALUE
        // because I'd like to use the LDC bytecode in the compiled class file

        String testSingleString = I.trfp(
            "One apple",
            "{0, number} apples",
            32800, 32800);
    }

}
