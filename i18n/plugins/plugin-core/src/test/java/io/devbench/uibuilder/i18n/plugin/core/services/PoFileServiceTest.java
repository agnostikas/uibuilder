/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import io.devbench.uibuilder.i18n.core.util.TranslationLoader;
import io.devbench.uibuilder.test.annotations.ReadResource;
import io.devbench.uibuilder.test.extensions.ResourceReaderExtension;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.PoWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(ResourceReaderExtension.class)
public class PoFileServiceTest {

    private PoWriter poWriter;
    private Path baseMessagePath;
    private PoFileService testObj;

    @BeforeEach
    public void setup() throws IOException {
        poWriter = new PoWriter();
        Path tempDir = Files.createTempDirectory("poTests");
        baseMessagePath = Files.createTempFile(tempDir, "baseMessagePath", ".po");
    }

    @Test
    public void should_return_empty_catalog_with_header_for_new_locale() {
        testObj = new PoFileService(poWriter, baseMessagePath);

        Catalog catalog = testObj.readPoFileForLanguage(Locale.CHINA);

        assertNotNull(catalog.locateHeader());
        Assertions.assertEquals(Locale.CHINA, TranslationLoader.extractLocaleFromHeader(catalog));
        assertEquals(1, catalog.size());
    }

    @Test
    public void should_read_correct_catalog_for_existing_locale(
        @ReadResource("/PoFileServiceTest/should_read_correct_catalog_for_existing_locale.po") String hungarianTranslation
    ) throws IOException {
        Path huPo = baseMessagePath.resolveSibling("hu.po");
        Files.write(huPo, hungarianTranslation.getBytes(StandardCharsets.UTF_8));

        testObj = new PoFileService(poWriter, baseMessagePath);

        Catalog catalog = testObj.readPoFileForLanguage(new Locale("hu"));

        assertNotNull(catalog.locateHeader());
        assertEquals(new Locale("hu"), TranslationLoader.extractLocaleFromHeader(catalog));
        assertEquals("Sziasztok!", catalog.locateMessage(null, "Hello world!").getMsgstr());
        assertEquals(2, catalog.size());
    }

    @Test
    public void should_write_catalog_to_correct_path_based_on_locale(
        @ReadResource("/PoFileServiceTest/should_write_catalog_to_correct_path_based_on_locale.po") String expectedContent
    ) throws IOException {
        testObj = new PoFileService(poWriter, baseMessagePath);

        Message testMessage01 = new Message();
        testMessage01.setMsgid("test");
        testMessage01.setMsgstr("test");
        testMessage01.addSourceReference("a", 1);

        Message testMessage02 = new Message();
        testMessage02.setMsgid("foo");
        testMessage02.setMsgstr("bar");
        testMessage02.addSourceReference("c", 4);
        testMessage02.addSourceReference("b", 1);

        Catalog catalog = testObj.readPoFileForLanguage(new Locale("hu"));

        catalog.addMessage(testMessage01);
        catalog.addMessage(testMessage02);

        testObj.writeCatalogTo(catalog);

        String actualContent = String.join("\n", Files.readAllLines(baseMessagePath.resolveSibling("hu.po")));
        assertEquals(expectedContent.trim(), actualContent);
    }

}
