/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services.sample.clazz;

import io.devbench.uibuilder.i18n.core.I;

import java.util.stream.Stream;

public class ShouldExtractMessagesAfterInvokeDynamic {

    //indy returns one parameter for the `Stream.iterate` call, so...
    public static void main(String[] args) {
        Stream.iterate(1L, n -> n + 1);
        String sample01 = I.tr("One apple");
    }


}
