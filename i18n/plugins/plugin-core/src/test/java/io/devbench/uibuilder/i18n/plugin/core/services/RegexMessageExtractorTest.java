/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import io.devbench.uibuilder.i18n.testtools.MessageUtil;
import io.devbench.uibuilder.test.CollectionAssert;
import org.fedorahosted.tennera.jgettext.Message;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.*;
import static org.junit.jupiter.api.Assertions.*;

class RegexMessageExtractorTest {

    RegexMessageExtractor testObj = new RegexMessageExtractor();

    @Test
    public void should_extract_simple_string_from_elements_inner_text() throws URISyntaxException, IOException {
        Path p = Paths.get(getClass()
            .getResource("/extractor/html/should_extract_simple_string_from_elements_inner_text.html").toURI());

        Collection<Message> actualMessages = testObj.extractMessages(p, "");
        CollectionAssert.assertContainsSame(
            Arrays.asList(
                MessageUtil.createMessage("Hello world!", emptyList())
            ),
            actualMessages,
            (m1, m2) -> Objects.equals(m1.getMsgid(), m1.getMsgid())
        );
    }

    @Test
    public void should_extract_strings_with_bindings_from_elements_inner_text() throws URISyntaxException, IOException {
        Path p = Paths.get(StaticFileMessageExtractor
            .class
            .getResource("/extractor/html/should_extract_strings_with_bindings_from_elements_inner_text.html").toURI());

        Collection<Message> actualMessages = testObj.extractMessages(p, "");
        CollectionAssert.assertContainsSame(
            Arrays.asList(
                MessageUtil.createMessage("Hello {{name}}!", "")
            ),
            actualMessages,
            (m1, m2) -> Objects.equals(m1.getMsgid(), m1.getMsgid())
        );
    }

    @Test
    public void should_extract_messages_when_formatting_is_in_the_message_correctly() throws URISyntaxException, IOException {
        Path p = Paths.get(StaticFileMessageExtractor
            .class
            .getResource("/extractor/html/should_extract_messages_when_formatting_is_in_the_message_correctly.html").toURI());

        Collection<Message> actualMessages = testObj.extractMessages(p, "");
        CollectionAssert.assertContainsSame(
            Arrays.asList(
                MessageUtil.createMessage("this text is <strong>bold</strong>.", ""),
                MessageUtil.createMessage("this text is <em>italic</em>.", "")
            ),
            actualMessages,
            (m1, m2) -> Objects.equals(m1.getMsgid(), m1.getMsgid())
        );
    }

    @Test
    public void should_extract_messages_from_attributes_correctly() throws URISyntaxException, IOException {
        Path p = Paths.get(StaticFileMessageExtractor
            .class
            .getResource("/extractor/html/should_extract_messages_from_attributes_correctly.html").toURI());

        Collection<Message> actualMessages = testObj.extractMessages(p, "");
        CollectionAssert.assertContainsSame(
            Arrays.asList(
                MessageUtil.createMessage("You sure?", ""),
                MessageUtil.createMessage("with multiple   spaces", "")
            ),
            actualMessages,
            (m1, m2) -> Objects.equals(m1.getMsgid(), m1.getMsgid())
        );
    }

    @Test
    public void should_extract_messages_from_js_correctly() throws URISyntaxException, IOException {
        Path p = Paths.get(StaticFileMessageExtractor
            .class
            .getResource("/extractor/html/should_extract_messages_from_component.js").toURI());

        Collection<Message> actualMessages = testObj.extractMessages(p, "");
        CollectionAssert.assertContainsSame(
            Arrays.asList(
                MessageUtil.createMessage("Cancel", ""),
                MessageUtil.createMessage("Reset", ""),
                MessageUtil.createMessage("Save", "")
            ),
            actualMessages,
            (m1, m2) -> Objects.equals(m1.getMsgid(), m1.getMsgid())
        );
    }

    @Test
    public void should_handle_parenthesis_and_comma_in_string_literals() {
        List<String> args = testObj.parseArgs("     'asd',     'don\\'t'  ,   'a,b,c',           '(a + b) / 2'   ");
        assertEquals(Arrays.asList("asd", "don't", "a,b,c", "(a + b) / 2"), args);
    }
}
