import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {ThemableMixin} from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

export class TestComponent extends Uibuilder.I18NBaseMixin('test-component', ThemableMixin(PolymerElement)) {

    static get template() {
        return html`
            <vaadin-button id="cancelBtn" disabled$="[[disabled]]">
                Cancel
            </vaadin-button>
            <vaadin-button id="resetBtn" disabled$="[[disabled]]">
                Reset
            </vaadin-button>
            <vaadin-button id="saveBtn" disabled$="[[disabled]]"
                           theme="primary">
                Save
            </vaadin-button>
        `;
    }

    static get is() {
        return 'test-component'
    }

    static get properties() {
        return {
            disabled: {
                type: Boolean,
            },
        }
    }

    ready() {
        super.ready();
        this.shadowRoot.querySelector("#cancelBtn").textContent = this.tr('Cancel');
        this.shadowRoot.querySelector("#resetBtn").textContent = this.tr('Reset');
        this.shadowRoot.querySelector("#saveBtn").textContent = this.tr('Save');
    }

}

customElements.define(TestComponent.is, TestComponent);
