/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core;

import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;

public abstract class MessageCollectionUtils {

    private MessageCollectionUtils() {

    }

    public static Collector<Message, ?, Map<MessageHashKey, Message>> messageCollector() {
        return Collector.of(
            HashMap::new,
            (map, newValue) -> map.compute(new MessageHashKey(newValue), (key, oldValue) -> mapOldValueAndNewValueIntoTheSameElement(newValue, oldValue)),
            (map1, map2) -> {
                map2.values().forEach(
                    newValue ->
                        map1.compute(new MessageHashKey(newValue), (key, oldValue) -> mapOldValueAndNewValueIntoTheSameElement(newValue, oldValue))
                );
                return map1;
            }
        );
    }

    public static Message mapOldValueAndNewValueIntoTheSameElement(Message newValue, Message oldValue) {
        if (oldValue == null) {
            return newValue;
        } else {
            newValue.getSourceReferences().forEach(oldValue::addSourceReference);
            return oldValue;
        }
    }
}
