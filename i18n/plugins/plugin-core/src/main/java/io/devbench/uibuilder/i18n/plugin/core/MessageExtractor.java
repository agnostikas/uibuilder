/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core;

import io.devbench.uibuilder.i18n.plugin.core.services.JavaMessageExtractor;
import io.devbench.uibuilder.i18n.plugin.core.services.PoFileService;
import io.devbench.uibuilder.i18n.plugin.core.services.StaticFileMessageExtractor;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static io.devbench.uibuilder.i18n.plugin.core.MessageCollectionUtils.*;

public class MessageExtractor {

    private final JavaMessageExtractor javaMessageExtractor;
    private final StaticFileMessageExtractor staticFileMessageExtractor;
    private final PoFileService poFileWriter;
    private final Path baseMessagePath;

    @Inject
    public MessageExtractor(
        JavaMessageExtractor javaMessageExtractor,
        StaticFileMessageExtractor staticFileMessageExtractor,
        PoFileService poFileWriter,
        @Named("baseMessagePath") Path baseMessagePath
    ) {
        this.javaMessageExtractor = javaMessageExtractor;
        this.staticFileMessageExtractor = staticFileMessageExtractor;
        this.poFileWriter = poFileWriter;
        this.baseMessagePath = baseMessagePath;
    }


    public void extractMessages(final Path outputDir) throws IOException {
        createOutputDirIfNotExists(outputDir);
        Catalog catalog = new Catalog(true);

        Map<MessageHashKey, Message> javaMessages = javaMessageExtractor.extractMessagesFromJavaFiles(outputDir);
        Map<MessageHashKey, Message> htmlMessages = staticFileMessageExtractor.extractMessagesFromHtmlFiles(outputDir);

        Map<MessageHashKey, Message> mergedMessages = new HashMap<>();
        javaMessages
            .forEach((key, message) -> mergedMessages.compute(key, (oldKey, oldMessage) -> mapOldValueAndNewValueIntoTheSameElement(message, oldMessage)));
        htmlMessages
            .forEach((key, message) -> mergedMessages.compute(key, (oldKey, oldMessage) -> mapOldValueAndNewValueIntoTheSameElement(message, oldMessage)));

        mergedMessages.values().forEach(catalog::addMessage);

        poFileWriter.writeCatalogTo(catalog, baseMessagePath);
    }

    private void createOutputDirIfNotExists(Path outputDir) throws IOException {
        if (Files.notExists(outputDir)) {
            Files.createDirectories(outputDir);
        }
    }

}
