/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import io.devbench.uibuilder.i18n.plugin.core.MessageCollectionUtils;
import io.devbench.uibuilder.i18n.plugin.core.exceptions.RuntimeIOException;
import io.devbench.uibuilder.i18n.plugin.core.util.PathUtil;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.List;
import java.util.Map;

public class StaticFileMessageExtractor {

    private static final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.{html,js}");
    private final RegexMessageExtractor regexMessageExtractor = new RegexMessageExtractor();

    @Inject
    public StaticFileMessageExtractor() {
    }

    public Map<MessageHashKey, Message> extractMessagesFromHtmlFiles(Path outputDir) throws IOException {
        return Files.walk(outputDir)
            .filter(matcher::matches)
            .flatMap(path -> extractMessagesFromHtmlFile(path, outputDir).stream())
            .collect(MessageCollectionUtils.messageCollector());
    }

    private List<Message> extractMessagesFromHtmlFile(Path path, Path outputDir) {
        try {
            String pathString = PathUtil.formatPathAsUnixPath(outputDir.relativize(path));
            return regexMessageExtractor.extractMessages(path, pathString);
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

}
