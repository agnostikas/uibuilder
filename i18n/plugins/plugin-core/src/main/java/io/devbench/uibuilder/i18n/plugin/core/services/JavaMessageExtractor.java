/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.services;

import io.devbench.uibuilder.i18n.plugin.core.MessageCollectionUtils;
import io.devbench.uibuilder.i18n.plugin.core.asm.TranslationClassVisitor;
import io.devbench.uibuilder.i18n.plugin.core.exceptions.RuntimeIOException;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;
import org.objectweb.asm.ClassReader;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Map;
import java.util.stream.Stream;

import static io.devbench.uibuilder.i18n.plugin.core.util.PathUtil.*;

public class JavaMessageExtractor {

    private static final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.{class}");

    @Inject
    public JavaMessageExtractor() {
    }


    private Stream<Message> extractMessagesFromClassFile(final Path file, final Path basedir) {
        try {
            InputStream fileAsStream = Files.newInputStream(file);
            ClassReader classReader = new ClassReader(fileAsStream);
            TranslationClassVisitor visitor = new TranslationClassVisitor(formatPathAsUnixPath(basedir.relativize(file)));
            classReader.accept(visitor, 0);
            return visitor.getMessages().stream();
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    public Map<MessageHashKey, Message> extractMessagesFromJavaFiles(final Path outputDir) throws IOException {
        return Files.walk(outputDir)
            .filter(matcher::matches)
            .flatMap(path -> extractMessagesFromClassFile(path, outputDir))
            .collect(MessageCollectionUtils.messageCollector());
    }
}
