/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core;

import io.devbench.uibuilder.i18n.plugin.core.services.PoFileService;
import org.fedorahosted.tennera.jgettext.Catalog;
import org.fedorahosted.tennera.jgettext.Message;
import org.fedorahosted.tennera.jgettext.MessageHashKey;

import javax.inject.Inject;
import javax.inject.Named;
import java.nio.file.Path;
import java.util.*;

public class MessageMerger {

    private final PoFileService poFileService;
    private final Path baseMessagePath;

    @Inject
    public MessageMerger(
        PoFileService poFileService,
        @Named("baseMessagePath") Path baseMessagePath
    ) {
        this.poFileService = poFileService;
        this.baseMessagePath = baseMessagePath;
    }

    public void mergeMessages(List<String> supportedLanguages, boolean keepUnusedTranslations) {
        Catalog baseMessageTemplate = poFileService.getMessageTemplate();
        supportedLanguages
            .stream()
            .map(this::mapLanguageCodeToLocale)
            .map(poFileService::readPoFileForLanguage)
            .map(poFile -> mergeTemplateToFile(poFile, baseMessageTemplate, keepUnusedTranslations))
            .forEach(poFileService::writeCatalogTo);
    }

    private Catalog mergeTemplateToFile(Catalog originalTranslatedCatalog, Catalog baseMessageTemplate, boolean keepUnusedTranslations) {
        Map<MessageHashKey, Message> originalTranslatedMessages = new HashMap<>();
        for (Message message : originalTranslatedCatalog) {
            originalTranslatedMessages.put(new MessageHashKey(message), message);
        }

        Catalog mergedTranslatedCatalog = mergeMessagesWithTheTemplate(baseMessageTemplate, originalTranslatedMessages, keepUnusedTranslations);
        copyHeaderToMergedCatalogIfNecessary(originalTranslatedCatalog, mergedTranslatedCatalog);

        return mergedTranslatedCatalog;
    }

    private Catalog mergeMessagesWithTheTemplate(Catalog baseMessageTemplate,
                                                 Map<MessageHashKey, Message> originalTranslatedMessages,
                                                 boolean keepUnusedTranslations) {
        Catalog mergedTranslatedCatalog = new Catalog(false);
        for (Message baseMessage : baseMessageTemplate) {
            MessageHashKey key = new MessageHashKey(baseMessage);
            Message messageToAdd = Optional
                .ofNullable(originalTranslatedMessages.remove(key))
                .orElse(baseMessage);

            baseMessage.getComments().stream().filter(comment -> !messageToAdd.getComments().contains(comment)).forEach(messageToAdd::addComment);
            List<String> newSourceRefs = new ArrayList<>(baseMessage.getSourceReferences());
            messageToAdd.getSourceReferences().clear();
            messageToAdd.getSourceReferences().addAll(newSourceRefs);

            mergedTranslatedCatalog.addMessage(messageToAdd);
        }
        if (keepUnusedTranslations) {
            originalTranslatedMessages
                .values()
                .stream()
                .filter(message -> !message.isHeader())
                .forEach(mergedTranslatedCatalog::addMessage);
        }
        return mergedTranslatedCatalog;
    }

    private void copyHeaderToMergedCatalogIfNecessary(Catalog originalTranslatedCatalog, Catalog mergedTranslatedCatalog) {
        if (mergedTranslatedCatalog.locateHeader() == null) {
            Message originalHeader = originalTranslatedCatalog.locateHeader();
            if (originalHeader != null) {
                mergedTranslatedCatalog.addMessage(originalHeader);
            }
        }
    }

    private Locale mapLanguageCodeToLocale(String languageCode) {
        Locale locale = Locale.forLanguageTag(languageCode);
        if (Objects.equals(new Locale(""), locale)) {
            throw new UnsupportedOperationException("Unsupported Language Tag: " + languageCode);
        }
        return locale;
    }
}
