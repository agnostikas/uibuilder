/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.asm;

import io.devbench.uibuilder.i18n.core.I;
import io.devbench.uibuilder.i18n.plugin.core.enums.TranslationMethod;
import org.fedorahosted.tennera.jgettext.Message;
import org.objectweb.asm.*;

import java.util.*;
import java.util.stream.Stream;

public class TranslationMethodVisitor extends MethodVisitor {

    private static final String targetClass = I.class.getCanonicalName().replace('.', '/');

    private int lineNumber;
    private Deque<Object> stack = new ArrayDeque<>();
    private Set<Message> messages = new HashSet<>();

    public TranslationMethodVisitor() {
        super(Opcodes.ASM6);
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments) {
        super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
        modifyStackBasedOnInvoke(Opcodes.INVOKEDYNAMIC, descriptor);
    }

    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean isInterface) {
        super.visitMethodInsn(opcode, owner, name, desc, isInterface);

        if (!Objects.equals(targetClass, owner)) {
            modifyStackBasedOnInvoke(opcode, desc);
            return;
        }

        Stream
            .of(TranslationMethod.values())
            .filter(method -> method.matches(name))
            .findAny()
            .map(translationMethod -> translationMethod.toMessage(stack))
            .ifPresent(message -> {
                message.addComment(Integer.toString(lineNumber));
                messages.add(message);
            });

        stack.add(new ReturnValue());
    }

    private void modifyStackBasedOnInvoke(int opcode, String desc) {
        int argumentSizes = Type.getArgumentTypes(desc).length;
        if (opcode == Opcodes.INVOKEVIRTUAL || opcode == Opcodes.INVOKESPECIAL) {
            stack.removeLast(); // remove the this from the stack;
        }
        for (int i = 0; i < argumentSizes; i++) {
            stack.removeLast();
        }
        if (Type.getReturnType(desc) != Type.VOID_TYPE) {
            stack.add(new ReturnValue());
        }
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
        super.visitTypeInsn(opcode, type);
        modifyStackBasedOnOpcode(opcode);
    }

    @Override
    public void visitInsn(int opcode) {
        super.visitInsn(opcode);
        modifyStackBasedOnOpcode(opcode);
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        super.visitIntInsn(opcode, operand);
        modifyStackBasedOnOpcode(opcode);
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
        super.visitVarInsn(opcode, var);
        modifyStackBasedOnOpcode(opcode);
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
        super.visitFieldInsn(opcode, owner, name, descriptor);
        modifyStackBasedOnOpcode(opcode);
    }

    private void modifyStackBasedOnOpcode(int opcode) {
        switch (opcode) {
            case Opcodes.ANEWARRAY:
                stack.removeLast();
                stack.add(new OpcodeLoad(opcode));
                break;
            case Opcodes.AASTORE:
                stack.removeLast();
                stack.removeLast();
                stack.removeLast();
                break;
            case Opcodes.DMUL:
            case Opcodes.IMUL:
            case Opcodes.LMUL:
            case Opcodes.FMUL:
            case Opcodes.DADD:
            case Opcodes.IADD:
            case Opcodes.LADD:
            case Opcodes.FADD:
                stack.removeLast(); // remove one multiplicand
                stack.removeLast(); // remove the other multiplicand
                stack.add(new OpcodeLoad(opcode));
                break;
            case Opcodes.ICONST_0:
            case Opcodes.ICONST_1:
            case Opcodes.ICONST_2:
            case Opcodes.ICONST_3:
            case Opcodes.ICONST_4:
            case Opcodes.ICONST_5:
            case Opcodes.ICONST_M1:
            case Opcodes.DCONST_0:
            case Opcodes.DCONST_1:
            case Opcodes.FCONST_0:
            case Opcodes.FCONST_1:
            case Opcodes.FCONST_2:
            case Opcodes.LCONST_0:
            case Opcodes.LCONST_1:
            case Opcodes.DUP:
            case Opcodes.BIPUSH:
            case Opcodes.SIPUSH:
            case Opcodes.ALOAD:
            case Opcodes.NEW:
            case Opcodes.GETSTATIC:
            case Opcodes.ACONST_NULL:
            case Opcodes.ILOAD:
            case Opcodes.LLOAD:
            case Opcodes.FLOAD:
            case Opcodes.DLOAD:
                stack.add(new OpcodeLoad(opcode));
                break;
        }
    }

    @Override
    public void visitLdcInsn(Object value) {
        super.visitLdcInsn(value);
        stack.add(value);
    }

    @Override
    public void visitCode() {
        super.visitCode();
        stack.clear();
    }

    public void visitLineNumber(int line, Label start) {
        this.lineNumber = line;
    }

    public Collection<Message> getMessages() {
        return messages;
    }

    private static class OpcodeLoad {
        private final int opcode;

        private OpcodeLoad(int opcode) {
            this.opcode = opcode;
        }
    }

    private static class ReturnValue {}
}
