/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugin.core.asm;

import org.fedorahosted.tennera.jgettext.Message;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.Collection;
import java.util.stream.Collectors;

public class TranslationClassVisitor extends ClassVisitor {

    private final String relativePathToFile;
    private TranslationMethodVisitor mv = new TranslationMethodVisitor();

    public TranslationClassVisitor(String relativePathToFile) {
        super(Opcodes.ASM7);
        this.relativePathToFile = relativePathToFile;
    }

    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        return mv;
    }

    public Collection<Message> getMessages() {
        return mv
            .getMessages()
            .stream()
            .map(this::addRelativePathToAllComments)
            .collect(Collectors.toSet());
    }

    private Message addRelativePathToAllComments(Message message) {
        String originalComment = message.getComments().iterator().next();
        message.getComments().clear();
        message.addSourceReference(relativePathToFile, Integer.valueOf(originalComment));
        return message;
    }
}
