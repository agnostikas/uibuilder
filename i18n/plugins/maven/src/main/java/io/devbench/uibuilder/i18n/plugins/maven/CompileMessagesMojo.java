/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugins.maven;

import io.devbench.uibuilder.i18n.plugin.core.PluginContext;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


@Mojo(name = "compile-messages", threadSafe = true)
public class CompileMessagesMojo extends AbstractMojo {

    @Parameter(property = "baseTranslationFile", defaultValue = "${basedir}/src/main/resources/translations/base.pot")
    private File baseTranslationFile;

    @Parameter(property = "supportedLanguages", required = true)
    private List<String> supportedLanguages;

    @Parameter(property = "project.build.outputDirectory", required = true, readonly = true)
    private String outputDirectory;


    public void execute() throws MojoExecutionException {
        try {
            Path outputDir = Paths.get(outputDirectory);
            Path baseMessagePath = baseTranslationFile.toPath();
            PluginContext pluginContext = PluginContext.createContext(baseMessagePath);

            pluginContext.getMessageExtractor().extractMessages(outputDir);
            pluginContext.getMessageMerger().mergeMessages(supportedLanguages, true);
        } catch (IOException e) {
            throw new MojoExecutionException("", e);
        }
    }
}
