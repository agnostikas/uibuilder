/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.plugins.maven;

import io.devbench.uibuilder.i18n.plugin.core.PluginContext;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.nio.file.Path;
import java.util.List;


@Mojo(name = "clean-messages", threadSafe = true)
public class CleanMessagesMojo extends AbstractMojo {

    @Parameter(property = "baseTranslationFile", defaultValue = "${basedir}/src/main/resources/translations/base.pot")
    private File baseTranslationFile;

    @Parameter(property = "supportedLanguages", required = true)
    private List<String> supportedLanguages;

    public void execute() {
        Path baseMessagePath = baseTranslationFile.toPath();
        PluginContext pluginContext = PluginContext.createContext(baseMessagePath);
        pluginContext.getMessageMerger().mergeMessages(supportedLanguages, false);
    }
}
