/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.i18n.testtools;

import org.fedorahosted.tennera.jgettext.Message;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class MessageUtil {

    private MessageUtil() {
    }

    public static Message createMessage(
        String msgId,
        String sourceRef
    ) {
        Message message = new Message();
        message.setMsgid(msgId);
        message.addSourceReference(sourceRef);
        return message;
    }

    public static Message createMessage(
        String msgId,
        List<String> sourceRefs
    ) {
        Message message = new Message();
        message.setMsgid(msgId);
        sourceRefs.forEach(message::addSourceReference);
        return message;
    }

    public static Message createMessage(String msgId, String plural, String sourceRef) {
        Message message = createMessage(msgId, sourceRef);
        message.setMsgidPlural(plural);
        return message;
    }

    public static Message createMessage(String msgId, String plural, List<String> sourceRefs) {
        Message message = createMessage(msgId, sourceRefs);
        message.setMsgidPlural(plural);
        return message;
    }

    public static Message createMessageWithContext(String msgctxt, String msgid, String sourceRef) {
        Message message = createMessage(msgid, sourceRef);
        message.setMsgctxt(msgctxt);
        return message;
    }

    public static Message createMessageWithContext(String msgctxt, String msgid, String plural, String sourceRef) {
        Message message = createMessage(msgid, plural, sourceRef);
        message.setMsgctxt(msgctxt);
        return message;
    }

    public static boolean messageEquals(Message m1, Message m2) {
        if (m1 == m2) {
            return true;
        }
        if (m2 == null || m2.getClass() != m1.getClass()) {
            return false;
        }
        return m1.isObsolete() == m2.isObsolete() &&
            Objects.equals(m1.getDomain(), m2.getDomain()) &&
            Objects.equals(m1.getMsgctxt(), m2.getMsgctxt()) &&
            Objects.equals(m1.getMsgid(), m2.getMsgid()) &&
            Objects.equals(m1.getMsgidPlural(), m2.getMsgidPlural()) &&
            Objects.equals(m1.getMsgstr(), m2.getMsgstr()) &&
            Objects.equals(m1.getMsgstrPlural(), m2.getMsgstrPlural()) &&
            Objects.equals(m1.getPrevMsgctx(), m2.getPrevMsgctx()) &&
            Objects.equals(m1.getPrevMsgid(), m2.getPrevMsgid()) &&
            Objects.equals(m1.getPrevMsgidPlural(), m2.getPrevMsgidPlural()) &&
            Objects.equals(m1.getComments(), m2.getComments()) &&
            Objects.equals(m1.getExtractedComments(), m2.getExtractedComments()) &&
            Objects.equals(m1.getFormats(), m2.getFormats()) &&
            Objects.equals(m1.getAllowWrap(), m2.getAllowWrap())
            && sourceRefsEquals(m1, m2);
    }

    private static boolean sourceRefsEquals(Message m1, Message m2) {
        if (m1 == null || m2 == null) {
            return m2 == m1;
        }

        return Objects.equals(new HashSet<>(m1.getSourceReferences()), new HashSet<>(m2.getSourceReferences()));
    }
}
