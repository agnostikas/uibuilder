/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.scanner;

import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MemberComponentProviderTest {

    @Mock
    private Environment environment;

    private MemberComponentProvider testObject;

    @BeforeEach
    private void setup() {

    }

    @Test
    @DisplayName("Should include interfaces when checks the candidates, only if the considerInterfaces attribute true")
    public void should_include_interfaces_when_checks_the_candidates_only_if_the_consider_interfaces_attribute_true() {
        testObject = new MemberComponentProvider(true, environment);

        AnnotatedBeanDefinition beanDefinition = mock(AnnotatedBeanDefinition.class);
        AnnotationMetadata metadata = mock(AnnotationMetadata.class);
        when(beanDefinition.getMetadata()).thenReturn(metadata);
        when(metadata.isIndependent()).thenReturn(true);
        when(metadata.isInterface()).thenReturn(true);
        when(metadata.isConcrete()).thenReturn(false);
        when(metadata.isAbstract()).thenReturn(false);

        boolean shouldBeTrue = testObject.isCandidateComponent(beanDefinition);

        testObject = new MemberComponentProvider(false, environment);
        boolean shouldBeFalse = testObject.isCandidateComponent(beanDefinition);

        assertAll(
            () -> assertTrue(shouldBeTrue),
            () -> assertFalse(shouldBeFalse)
        );
    }


}
