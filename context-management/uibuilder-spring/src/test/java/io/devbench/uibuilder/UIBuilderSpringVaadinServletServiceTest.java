/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder;

import com.vaadin.flow.function.DeploymentConfiguration;
import com.vaadin.flow.server.VaadinServlet;
import io.devbench.uibuilder.spring.UIBuilderSpringVaadinServletService;
import io.devbench.uibuilder.spring.configuration.staticcontent.StaticContentDescription;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import java.net.URL;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UIBuilderSpringVaadinServletServiceTest {

    @Nested
    @ExtendWith(MockitoExtension.class)
    public class ShouldHandleStaticResourcesWhenStaticContentDescriptorIsAvailable {

        @Mock
        private StaticContentDescription staticContentDescription;

        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        private VaadinServlet servlet;

        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        private DeploymentConfiguration deploymentConfiguration;

        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        private ApplicationContext context;

        @InjectMocks
        private UIBuilderSpringVaadinServletService testObj;

        @Test
        public void should_return_static_resource_path_if_prefix_matches() {
            when(staticContentDescription.getUrl()).thenReturn("/fooBarStatic");
            when(staticContentDescription.getPaths()).thenReturn(Collections.singletonList("/test-static-files"));

            URL staticResource = testObj.getStaticResource("/fooBarStatic/static.txt");
            URL expectedUrl = ShouldHandleStaticResourcesWhenStaticContentDescriptorIsAvailable.class.getResource("/test-static-files/static.txt");

            assertEquals(expectedUrl, staticResource);
        }

        @Test
        public void should_return_static_resource_path_if_prefix_matches_with_frontend() {
            when(staticContentDescription.getUrl()).thenReturn("/fooBarStatic");
            when(staticContentDescription.getPaths()).thenReturn(Collections.singletonList("/test-static-files"));

            URL staticResource = testObj.getStaticResource("/frontend/fooBarStatic/static.txt");
            URL expectedUrl = ShouldHandleStaticResourcesWhenStaticContentDescriptorIsAvailable.class.getResource("/test-static-files/static.txt");

            assertEquals(expectedUrl, staticResource);
        }

        @Test
        public void should_return_static_resource_path_if_prefix_matches_without_slash() {
            when(staticContentDescription.getUrl()).thenReturn("/fooBarStatic");
            when(staticContentDescription.getPaths()).thenReturn(Collections.singletonList("/test-static-files"));

            URL staticResource = testObj.getStaticResource("fooBarStatic/static.txt");
            URL expectedUrl = ShouldHandleStaticResourcesWhenStaticContentDescriptorIsAvailable.class.getResource("/test-static-files/static.txt");

            assertEquals(expectedUrl, staticResource);
        }

        @Test
        public void should_call_super_when_static_prefix_does_not_match() throws Exception {
            when(staticContentDescription.getUrl()).thenReturn("/fooBarStatic");
            when(staticContentDescription.getPaths()).thenReturn(Collections.singletonList("/test-static-files"));
            when(servlet.getServletContext().getResource("/someotherprefix/static.txt")).thenReturn(null);

            URL staticResource = testObj.getStaticResource("/someotherprefix/static.txt");
            assertNull(staticResource);
        }
    }

    @Nested
    @ExtendWith(MockitoExtension.class)
    public class ShouldHandleStaticResourcesWhenStaticContentDescriptorIsNotAvailable {

        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        private VaadinServlet servlet;

        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        private DeploymentConfiguration deploymentConfiguration;

        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        private ApplicationContext context;

        @InjectMocks
        private UIBuilderSpringVaadinServletService testObj;

        @Test
        public void should_call_super_when_static_descriptor_is_not_available() throws Exception {
            when(servlet.getServletContext().getResource("/someotherprefix/static.txt")).thenReturn(null);

            URL staticResource = testObj.getStaticResource("/someotherprefix/static.txt");
            assertNull(staticResource);
        }
    }

}
