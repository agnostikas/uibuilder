/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.function.SerializableConsumer;
import com.vaadin.flow.server.ErrorEvent;
import com.vaadin.flow.server.ErrorHandler;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.ObjectProvider;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class ErrorHandlerRegistratorTest {

    // Used by junit default BaseUIBuilderTestExtension extension
    @SuppressWarnings("unused")
    private VaadinSession vaadinSession = new VaadinSession(mock(VaadinService.class));

    @Mock
    private UI mockUI;

    @Mock
    private ObjectProvider<ErrorHandler> errorHandlerProvider;

    @InjectMocks
    private ErrorHandlerRegistrator testObj;

    @Test
    void should_call_custom_error_handler_if_one_is_defined() {
        ErrorHandler errorHandler = mock(ErrorHandler.class);
        when(errorHandlerProvider.getIfAvailable()).thenReturn(errorHandler);
        ErrorEvent errorEvent = mock(ErrorEvent.class);
        VaadinSession vaadinSession = VaadinSession.getCurrent();

        testObj.handleRequest(vaadinSession, null, null);

        VaadinSession.getCurrent().lock();
        vaadinSession.getErrorHandler().error(errorEvent);

        verify(errorHandler).error(errorEvent);
    }

    @Test
    void should_use_default_error_dialog_when_user_has_none() {
        ErrorEvent errorEvent = mock(ErrorEvent.class);
        VaadinSession vaadinSession = VaadinSession.getCurrent();

        testObj.handleRequest(vaadinSession, null, null);

        VaadinSession.getCurrent().lock();

        doAnswer(invocation -> {
            SerializableConsumer<?> consumer = invocation.getArgument(1);
            consumer.accept(null);
            return null;
        }).when(mockUI).beforeClientResponse(any(), any());

        vaadinSession.getErrorHandler().error(errorEvent);

        verify(mockUI).add(any(Dialog.class));
    }
}
