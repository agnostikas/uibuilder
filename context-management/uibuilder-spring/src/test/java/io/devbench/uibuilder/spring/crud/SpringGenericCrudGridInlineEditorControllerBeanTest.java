/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.crud;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.dom.Element;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.spring.data.SpringCommonDataSourceProvider;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SpringGenericCrudGridInlineEditorControllerBeanTest {

    private SpringGenericCrudGridInlineEditorControllerBean<TestEntity> testObj;

    @Mock
    @SuppressWarnings("rawtypes")
    private SpringCommonDataSourceProvider commonDataSourceProvider;

    @Mock
    private ApplicationContext applicationContext;

    @BeforeEach
    void setUp() {
        testObj = new SpringGenericCrudGridInlineEditorControllerBean<>(commonDataSourceProvider, applicationContext);
    }

    @Test
    @DisplayName("Should save item")
    void test_should_save_item() {
        TestEntity item = new TestEntity();
        Component component = mock(Component.class);
        Element element = mock(Element.class);
        doReturn(element).when(component).getElement();
        doReturn(Optional.of("grid-id")).when(component).getId();

        Pair<TargetDataSource, Class<? extends PagingAndSortingRepository<TestEntity, String>>> datasourceRepoPair = Pair.of(null, TestRepository.class);
        Map<String, Pair<TargetDataSource, Class<? extends PagingAndSortingRepository<TestEntity, String>>>> dsMap = new HashMap<>();
        dsMap.put("testDS", datasourceRepoPair);
        when(commonDataSourceProvider.getDataSourceRepositories()).thenReturn(dsMap);

        TestRepository repository = mock(TestRepository.class);
        ObjectProvider<?> repositoryProvider = mock(ObjectProvider.class);
        doReturn(repository).when(repositoryProvider).getIfAvailable();
        doReturn(repositoryProvider).when(applicationContext).getBeanProvider(TestRepository.class);

        testObj.registerDataSourceName("grid-id", "testDS");
        JsonObject jsonItem = Json.createObject();
        testObj.onInlineItemSave(item, jsonItem, component);

        verify(repository).save(same(item));
        verify(element).callJsFunction(eq("_inlineItemSaved"), same(jsonItem));
    }

    @Test
    @SuppressWarnings({"unchecked", "rawtypes"})
    @DisplayName("Should not save into any repository if the component has a collection datasource")
    void test_should_not_save_into_any_repository_if_the_component_has_a_collection_datasource() {
        TestEntity item = new TestEntity();
        AbstractDataSourceComponent<?> component = mock(AbstractDataSourceComponent.class);
        Element element = mock(Element.class);
        CommonDataSource collectionDataSource = mock(CollectionDataSource.class);

        when(component.getDataSource()).thenReturn(collectionDataSource);
        doReturn(element).when(component).getElement();

        JsonObject jsonItem = Json.createObject();
        testObj.onInlineItemSave(item, jsonItem, component);

        verify(element).callJsFunction(eq("_inlineItemSaved"), same(jsonItem));
    }

    interface TestRepository extends PagingAndSortingRepository<TestEntity, String> {

    }

    @Data
    @NoArgsConstructor
    static class TestEntity implements Serializable {
        private String id;
        private String name;
    }

}
