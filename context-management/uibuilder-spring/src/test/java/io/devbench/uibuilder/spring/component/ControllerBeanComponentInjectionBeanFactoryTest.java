/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPointComponentStore;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.test.extensions.BaseUIBuilderTestExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import lombok.Getter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.inject.Provider;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, BaseUIBuilderTestExtension.class})
class ControllerBeanComponentInjectionBeanFactoryTest {

    @InjectMocks
    private ControllerBeanComponentInjectionBeanFactory testObj;

    @Mock
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    @Mock
    private UI ui;

    @Mock
    private VaadinSession session;

    @Test
    @DisplayName("Should inject component provider into the controller bean")
    void test_should_inject_component_provider_into_the_controller_bean() throws Exception {
        doReturn(session).when(ui).getSession();
        WrappedSession wrappedSessionMock = Mockito.mock(WrappedSession.class);
        doReturn(wrappedSessionMock).when(session).getSession();
        doReturn("12").when(wrappedSessionMock).getId();
        doReturn(112).when(ui).getUIId();

        ComponentInjectionPoint injectionPoint = new ComponentInjectionPoint("testComponent", Collections.singleton(Button.class));
        Field componentStoreField = ComponentInjectionPoint.class.getDeclaredField("componentStore");
        componentStoreField.setAccessible(true);
        componentStoreField.set(injectionPoint, new TestComponentInjectionPointComponentStore());

        Button testButton = new Button();
        injectionPoint.setValue(testButton);

        when(controllerBeanManager.getAllCachedComponentInjectionPoint()).thenReturn(Collections.singletonList(injectionPoint));

        TestControllerBean testControllerBean = new TestControllerBean();
        assertNull(testControllerBean.getButtonProvider());

        testObj.postProcessBeforeInitialization(testControllerBean, "test-controller-bean");

        Provider<Button> buttonProvider = testControllerBean.getButtonProvider();
        assertNotNull(buttonProvider);
        Button button = buttonProvider.get();
        assertNotNull(button);
    }

    public static class TestControllerBean {

        @Getter
        @UIComponent("testComponent")
        private Provider<Button> buttonProvider;

    }

    public static class TestComponentInjectionPointComponentStore implements ComponentInjectionPointComponentStore {

        private Map<String, Component> valueMap = new HashMap<>();

        @Override
        public void put(String key, Component component) {
            valueMap.put(key, component);
        }

        @Override
        public void remove(String key) {
            valueMap.remove(key);
        }

        @Override
        public Component get(String key) {
            return valueMap.get(key);
        }
    }
}
