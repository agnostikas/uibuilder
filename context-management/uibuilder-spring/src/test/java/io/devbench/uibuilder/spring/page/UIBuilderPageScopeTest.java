/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.page;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.page.PageLoader;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UIBuilderPageScopeTest {

    private UIBuilderPageScope testObj = new UIBuilderPageScope();

    @Mock
    private FlowManager flowManager;

    @Mock
    private UIContext uiContext;

    @Mock
    private VaadinSession vaadinSession;

    @Test
    public void should_register_page_scope_to_the_beanfactory() {
        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class);
        testObj.postProcessBeanFactory(beanFactory);
        verify(beanFactory).registerScope(UIBuilderPageScope.UIBUILDER_PAGE_SCOPE, testObj);
    }

    @Test
    public void should_call_beanstore_with_correct_bean_id() {

        @PageScope("foobar-page")
        class FooBar {}

        final PageLoader pageLoader = mock(PageLoader.class);
        doReturn(uiContext).when(vaadinSession).getAttribute("UIBuilderSessionContext");
        doReturn(flowManager).when(uiContext).get(FlowManager.class);

        BeanStoreWrapper beanStoreWrapper = mock(BeanStoreWrapper.class, Answers.RETURNS_DEEP_STUBS);
        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class, Answers.RETURNS_DEEP_STUBS);
        BeanStore beanStore = mock(BeanStore.class);
        ObjectFactory<FooBar> objectFactory = mock(ObjectFactory.class);

        when(beanFactory.getBeanDefinition("foobar").getBeanClassName()).thenReturn(FooBar.class.getName());
        when(vaadinSession.getAttribute(BeanStoreWrapper.class)).thenReturn(beanStoreWrapper);
        when(beanStoreWrapper.getBeanStore(any(), any())).thenReturn(beanStore);
        when(flowManager.getPageLoaders()).thenReturn(Arrays.asList(pageLoader));
        when(pageLoader.getPageContextIds()).thenReturn(Arrays.asList("foobar-page"));
        when(beanStore.get(any(), any())).thenReturn(new FooBar());

        testObj.postProcessBeanFactory(beanFactory);
        testObj.get("foobar", objectFactory);
        verify(beanStore).get(PageScopeBeanId.of("foobar", "foobar-page"), objectFactory);
    }

    @Test
    public void should_create_new_beanstore_to_session_when_currently_there_is_not_any() {

        @PageScope("foobar-page")
        class FooBar {}

        final PageLoader pageLoader = mock(PageLoader.class);
        doReturn(uiContext).when(vaadinSession).getAttribute("UIBuilderSessionContext");
        doReturn(flowManager).when(uiContext).get(FlowManager.class);

        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class, Answers.RETURNS_DEEP_STUBS);
        ObjectFactory<FooBar> objectFactory = mock(ObjectFactory.class);

        when(beanFactory.getBeanDefinition("foobar").getBeanClassName()).thenReturn(FooBar.class.getName());
        when(vaadinSession.getAttribute(BeanStoreWrapper.class)).thenReturn(null);
        when(flowManager.getPageLoaders()).thenReturn(Arrays.asList(pageLoader));
        when(pageLoader.getPageContextIds()).thenReturn(Arrays.asList("foobar-page"));
        when(objectFactory.getObject()).thenReturn(new FooBar());

        testObj.postProcessBeanFactory(beanFactory);
        testObj.get("foobar", objectFactory);
        verify(vaadinSession).setAttribute(eq(BeanStoreWrapper.class), any(BeanStoreWrapper.class));
    }

    @Test
    public void should_throw_correct_exception_when_class_is_not_correct() {

        @PageScope("foobar-page")
        class FooBar {}

        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class, Answers.RETURNS_DEEP_STUBS);
        ObjectFactory<?> objectFactory = mock(ObjectFactory.class);

        when(beanFactory.getBeanDefinition("foobar").getBeanClassName()).thenReturn("not.existing.Class");
        when(vaadinSession.getAttribute(BeanStoreWrapper.class)).thenReturn(mock(BeanStoreWrapper.class));

        testObj.postProcessBeanFactory(beanFactory);
        assertThrows(BeanClassNotFoundException.class, () -> testObj.get("foobar", objectFactory));
    }

    @Test
    public void should_register_correct_destruction_callback_to_beanstore() {

        @PageScope("foobar-page")
        class FooBar {}

        BeanStoreWrapper beanStoreWrapper = mock(BeanStoreWrapper.class, Answers.RETURNS_DEEP_STUBS);
        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class, Answers.RETURNS_DEEP_STUBS);
        BeanStore beanStore = mock(BeanStore.class);
        Runnable destructionCallback = mock(Runnable.class);

        when(beanFactory.getBeanDefinition("foobar").getBeanClassName()).thenReturn(FooBar.class.getName());
        when(vaadinSession.getAttribute(BeanStoreWrapper.class)).thenReturn(beanStoreWrapper);
        when(beanStoreWrapper.getBeanStore(any(), any())).thenReturn(beanStore);

        testObj.postProcessBeanFactory(beanFactory);
        testObj.registerDestructionCallback("foobar", destructionCallback);
        verify(beanStore).registerDestructionCallback(PageScopeBeanId.of("foobar", "foobar-page"), destructionCallback);
    }

    @Test
    public void unload_page_lambda_should_test_the_page_id() {

        @PageScope("foobar-page")
        class FooBar {}

        BeanStoreWrapper beanStoreWrapper = mock(BeanStoreWrapper.class, Answers.RETURNS_DEEP_STUBS);
        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class, Answers.RETURNS_DEEP_STUBS);
        BeanStore beanStore = mock(BeanStore.class);
        ArgumentCaptor<Predicate<PageScopeBeanId>> unloadPredicateCaptor = ArgumentCaptor.forClass(Predicate.class);

        when(beanFactory.getBeanDefinition("foobar").getBeanClassName()).thenReturn(FooBar.class.getName());
        when(vaadinSession.getAttribute(BeanStoreWrapper.class)).thenReturn(beanStoreWrapper);
        when(beanStoreWrapper.getBeanStore(any(), any())).thenReturn(beanStore);

        testObj.postProcessBeanFactory(beanFactory);
        testObj.unloadPage("foobar-page");
        verify(beanStore).destroyBeans(unloadPredicateCaptor.capture());

        assertTrue(unloadPredicateCaptor.getValue().test(PageScopeBeanId.of("randomBean", "foobar-page")));
        assertFalse(unloadPredicateCaptor.getValue().test(PageScopeBeanId.of("randomBean", "some-other-page-id")));
    }

    @Test
    public void should_throw_illegal_state_exception_when_the_page_scope_is_not_active() {

        @PageScope("foobar-page")
        class FooBar {}

        final PageLoader pageLoader = mock(PageLoader.class);
        ConfigurableListableBeanFactory beanFactory = mock(ConfigurableListableBeanFactory.class, Answers.RETURNS_DEEP_STUBS);

        doReturn(uiContext).when(vaadinSession).getAttribute("UIBuilderSessionContext");
        doReturn(flowManager).when(uiContext).get(FlowManager.class);
        when(flowManager.getPageLoaders()).thenReturn(Arrays.asList(pageLoader));
        when(pageLoader.getPageContextIds()).thenReturn(Collections.emptyList());
        when(beanFactory.getBeanDefinition("foobar").getBeanClassName()).thenReturn(FooBar.class.getName());

        testObj.postProcessBeanFactory(beanFactory);
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> testObj.get("foobar", mock(ObjectFactory.class)));
        assertEquals("PageScope for contextId (`foobar-page`) is not active, because the page is not loaded.", exception.getMessage());
    }

}
