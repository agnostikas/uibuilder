/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.configuration.staticcontent;

import io.devbench.uibuilder.annotations.StaticContent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.core.type.AnnotationMetadata;

import java.util.HashMap;

import static org.mockito.Mockito.*;

class StaticContentSpringConfigurationTest {

    private StaticContentSpringConfiguration testObj = new StaticContentSpringConfiguration();

    @Test
    public void should_not_fail_when_theres_no_annotation_in_the_context() {
        BeanDefinitionRegistry beanDefinitionRegistry = mock(BeanDefinitionRegistry.class);
        AnnotationMetadata annotationMetadata = mock(AnnotationMetadata.class);

        when(annotationMetadata.hasAnnotation(StaticContent.class.getName())).thenReturn(false);

        testObj.registerBeanDefinitions(annotationMetadata, beanDefinitionRegistry);

        verifyZeroInteractions(beanDefinitionRegistry);
    }

    @Test
    public void should_register_bean_when_there_is_annotation_in_the_context() {
        BeanDefinitionRegistry beanDefinitionRegistry = mock(BeanDefinitionRegistry.class);
        AnnotationMetadata annotationMetadata = mock(AnnotationMetadata.class);
        String[] pathArray = {"/static/path"};

        when(annotationMetadata.hasAnnotation(StaticContent.class.getName())).thenReturn(true);
        when(annotationMetadata.getAnnotationAttributes(StaticContent.class.getName())).thenReturn(new HashMap<String, Object>() {
            {
                put("lookupPaths", pathArray);
                put("url", "/staticUrl");
            }
        });

        testObj.registerBeanDefinitions(annotationMetadata, beanDefinitionRegistry);

        GenericBeanDefinition expectedBeanDefinition = new GenericBeanDefinition();
        expectedBeanDefinition.setBeanClass(StaticContentDescription.class);
        expectedBeanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(0, "/staticUrl");
        expectedBeanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(1, pathArray);
        expectedBeanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);

        verify(beanDefinitionRegistry).registerBeanDefinition(StaticContent.class.getName(), expectedBeanDefinition);
    }

}
