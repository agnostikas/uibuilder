/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.page;

import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BeanStoreWrapperTest {

    @Mock
    private VaadinSession vaadinSession;

    @InjectMocks
    private BeanStoreWrapper testObj;

    @Test
    public void should_return_the_same_beanstore_for_the_same_class_and_ui() {
        UI ui = mock(UI.class);
        BeanStore<BeanStoreWrapperTest> beanStore = testObj.getBeanStore(ui, BeanStoreWrapperTest.class);
        assertEquals(beanStore, testObj.getBeanStore(ui, BeanStoreWrapperTest.class));
        assertEquals(beanStore, testObj.getBeanStore(ui, BeanStoreWrapperTest.class));
        assertNotEquals(beanStore, testObj.getBeanStore(mock(UI.class), BeanStoreWrapperTest.class));
    }

    @Test
    public void should_not_return_the_same_beanstore_after_ui_destroyed() {
        UI ui = mock(UI.class);
        DetachEvent detachEvent = mock(DetachEvent.class);

        when(detachEvent.getUI()).thenReturn(ui);

        BeanStore<BeanStoreWrapperTest> beanStore = testObj.getBeanStore(ui, BeanStoreWrapperTest.class);
        testObj.onComponentEvent(detachEvent);
        BeanStore<BeanStoreWrapperTest> beanStoreAgain = testObj.getBeanStore(ui, BeanStoreWrapperTest.class);
        assertNotEquals(beanStore, beanStoreAgain);
    }
}
