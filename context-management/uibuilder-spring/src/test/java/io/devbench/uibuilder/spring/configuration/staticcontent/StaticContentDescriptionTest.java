/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.configuration.staticcontent;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StaticContentDescriptionTest {

    @Test
    public void the_static_content_url_prefix_should_start_with_slash_when_originally_was_not_prefixed() {
        StaticContentDescription testObj = new StaticContentDescription("url", new String[0]);
        assertEquals("/url", testObj.getUrl());
    }

    @Test
    public void the_static_content_url_prefix_should_start_with_slash_when_originally_was_prefixed() {
        StaticContentDescription testObj = new StaticContentDescription("/url", new String[0]);
        assertEquals("/url", testObj.getUrl());
    }

}
