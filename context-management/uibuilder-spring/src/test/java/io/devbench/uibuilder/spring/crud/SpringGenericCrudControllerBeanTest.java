/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.crud;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.crud.CanCreate;
import io.devbench.uibuilder.api.crud.Refreshable;
import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.spring.data.SpringCommonDataSourceProvider;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SpringGenericCrudControllerBeanTest {

    @InjectMocks
    private SpringGenericCrudControllerBean testObj;

    @Mock
    private SpringCommonDataSourceProvider commonDataSourceProvider;

    @Mock
    private ApplicationContext applicationContext;

    @Test
    @DisplayName("Should call refresh on onRefresh if refreshable is not null")
    void test_should_call_refresh_on_on_refresh_if_refreshable_is_not_null() {
        Refreshable refreshable = mock(Refreshable.class);
        testObj.refresh(refreshable);
        verify(refreshable).refresh();
    }

    @Test
    @DisplayName("Should find and register crud data")
    void test_should_find_and_register_crud_data() {
        PagingAndSortingRepository mockRepository = registerCrudData("testDS");

        testObj.registerGenericCrudData("test-mdc-id", "testDS");

        GenericCrudData<?> crudData = (GenericCrudData<?>) testObj.getGenericCrudDataMdcIdMap().get("test-mdc-id");
        assertNotNull(crudData);
        assertEquals(TestItem.class, crudData.getItemClass());
        assertSame(mockRepository, crudData.getRepository());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should call save on repository when on save")
    void test_should_call_save_on_repository_when_on_save() {
        TestItem item = new TestItem();
        MockMdc component = mock(MockMdc.class);
        doReturn(Optional.of("test-mdc-id")).when(component).getId();
        PagingAndSortingRepository mockRepository = registerCrudData("testDS");
        testObj.registerGenericCrudData("test-mdc-id", "testDS");

        testObj.onSave(item, component);

        verify(mockRepository).save(item);
        verify(component).refresh();
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should call delete on repository when on delete")
    void test_should_call_delete_on_repository_when_on_delete() {
        TestItem item = new TestItem();
        MockMdc component = mock(MockMdc.class);
        doReturn(Optional.of("test-mdc-id")).when(component).getId();
        PagingAndSortingRepository mockRepository = registerCrudData("anotherDS");
        testObj.registerGenericCrudData("test-mdc-id", "anotherDS");

        testObj.onDelete(item, component);

        verify(mockRepository).delete(item);
        verify(component).refresh();
    }

    @Test
    @DisplayName("Should call createItem on canCreate component when on create")
    void test_should_call_create_item_on_can_create_component_when_on_create() {
        MockMdc component = mock(MockMdc.class);
        doReturn(Optional.of("test-mdc-id")).when(component).getId();
        registerCrudData("testDS");
        testObj.registerGenericCrudData("test-mdc-id", "testDS");

        testObj.onCreate(component);

        verify(component).createItem(any(TestItem.class));
        verify(component).refresh();
    }

    @SuppressWarnings("unchecked")
    private PagingAndSortingRepository registerCrudData(String datasourceName) {
        ObjectProvider<TestRepository> repositoryObjectProvider = mock(ObjectProvider.class);
        TestRepository repository = mock(TestRepository.class);
        doReturn(repository).when(repositoryObjectProvider).getIfAvailable();

        Map<String, Pair<TargetDataSource, Class>> dataSourceRepositories = new HashMap<>();
        dataSourceRepositories.put(datasourceName, Pair.of(null, TestRepository.class));
        when(commonDataSourceProvider.getDataSourceRepositories()).thenReturn(dataSourceRepositories);
        commonDataSourceProvider.getDataSourceRepositories().get(datasourceName);

        doReturn(repositoryObjectProvider).when(applicationContext).getBeanProvider(TestRepository.class);
        doReturn(Optional.of(TestItem.class)).when(commonDataSourceProvider).tryToFindEntityClassByRepositoryClass(TestRepository.class);

        return repository;
    }

    static class MockMdc extends Component implements Refreshable, CanCreate {

        @Override
        public boolean isSelectedItemCreated() {
            return false;
        }

        @Override
        public void createItem(Object item) {

        }

        @Override
        public void refresh() {

        }
    }

    @SuppressWarnings("WeakerAccess")
    public static class TestItem {

    }

    @SuppressWarnings("NullableProblems")
    static class TestRepository implements PagingAndSortingRepository {
        @Override
        public Iterable findAll(Sort sort) {
            return null;
        }

        @Override
        public Page findAll(Pageable pageable) {
            return null;
        }

        @Override
        public Object save(Object entity) {
            return null;
        }

        @Override
        public Iterable saveAll(Iterable entities) {
            return null;
        }

        @Override
        public Optional findById(Object o) {
            return Optional.empty();
        }

        @Override
        public boolean existsById(Object o) {
            return false;
        }

        @Override
        public Iterable findAll() {
            return null;
        }

        @Override
        public Iterable findAllById(Iterable iterable) {
            return null;
        }

        @Override
        public long count() {
            return 0;
        }

        @Override
        public void deleteById(Object o) {

        }

        @Override
        public void delete(Object entity) {

        }

        @Override
        public void deleteAll(Iterable entities) {

        }

        @Override
        public void deleteAll() {

        }
    }

}
