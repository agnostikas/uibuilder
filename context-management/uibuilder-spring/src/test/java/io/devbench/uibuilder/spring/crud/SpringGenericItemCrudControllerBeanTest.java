/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.crud;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import io.devbench.uibuilder.api.components.form.UIBuilderDetailCapable;
import io.devbench.uibuilder.api.components.masterconnector.UIBuilderMasterConnector;
import io.devbench.uibuilder.api.crud.CanCreate;
import io.devbench.uibuilder.api.crud.MasterConnectorProvider;
import io.devbench.uibuilder.api.crud.Refreshable;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import lombok.Data;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import javax.persistence.OneToMany;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, JsoupExtension.class, SingletonProviderForTestsExtension.class})
class SpringGenericItemCrudControllerBeanTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @InjectMocks
    private SpringGenericItemCrudControllerBean<TestItem, ?> testObj;

    @BeforeEach
    void setup() {
        when(memberScanner.findClassesBySuperType(UIBuilderDetailCapable.class)).thenReturn(new HashSet<>(Arrays.asList(
            TestDetailPanel.class, TestEditorWindow.class
        )));
    }

    @Test
    @DisplayName("Should call refresh")
    void test_should_call_refresh() {
        @SuppressWarnings("unchecked")
        TestMdc<TestItem> refreshable = mock(TestMdc.class);

        testObj.refresh(refreshable);

        verify(refreshable).refresh();
    }

    @Test
    @DisplayName("Should add item on save")
    void test_should_add_item_on_save() {
        TestItem item = new TestItem();

        @SuppressWarnings("unchecked")
        TestMdc<TestItem> refreshable = mock(TestMdc.class);

        @SuppressWarnings("unchecked")
        UIBuilderMasterConnector<Component, TestItem> connector = mock(UIBuilderMasterConnector.class);

        when(refreshable.isSelectedItemCreated()).thenReturn(true);
        when(refreshable.getMasterConnector()).thenReturn(connector);

        testObj.onSave(item, refreshable);

        verify(refreshable).refresh();
        verify(connector).addItem(same(item));
    }

    @Test
    @DisplayName("Should handle one to many relation")
    void test_should_handle_one_to_many_relation(@LoadElement(value = "/generic-item-crud-cb-test-1.html", id = "test-grid") Element testGridElement) {
        assertOneToManyRelationIsHandled(testGridElement);
    }

    @Test
    @DisplayName("Should handle one to many relation when item-bind is item-data-source")
    void test_should_handle_one_to_many_relation_when_item_bind_is_on_item_data_source(
        @LoadElement(value = "/generic-item-crud-cb-test-2.html", id = "test-grid") Element testGridElement
    ) {
        assertOneToManyRelationIsHandled(testGridElement);
    }

    @Test
    @DisplayName("Should handle one to many relation, when the detail component is editor window")
    void should_handle_one_to_many_relation_when_the_detail_component_is_editor_window(
        @LoadElement(value = "/generic-item-crud-cb-test-3.html", id = "test-grid") Element testGridElement
    ) {
        assertOneToManyRelationIsHandled(testGridElement);
    }

    private void assertOneToManyRelationIsHandled(
        @LoadElement(value = "/generic-item-crud-cb-test-1.html", id = "test-grid") Element testGridElement) {
        TestParentItem parentItem = new TestParentItem();
        testObj.registerJoinItemSupplier("mdc", () -> parentItem);

        @SuppressWarnings("unchecked")
        TestMdc<TestItem> refreshable = mock(TestMdc.class);

        TestGrid testGrid = mock(TestGrid.class);

        @SuppressWarnings("unchecked")
        UIBuilderMasterConnector<Component, TestItem> connector = mock(UIBuilderMasterConnector.class);
        when(refreshable.getMasterConnector()).thenReturn(connector);
        when(connector.getMasterComponent()).thenReturn(testGrid);
        when(testGrid.getRawElement()).thenReturn(testGridElement);

        testObj.onCreate(refreshable);

        ArgumentCaptor<TestItem> testItemArgumentCaptor = ArgumentCaptor.forClass(TestItem.class);
        verify(refreshable).createItem(testItemArgumentCaptor.capture());

        TestItem createdTestItem = testItemArgumentCaptor.getValue();

        assertNotNull(createdTestItem);
        assertSame(parentItem, createdTestItem.getParent());
    }


    static class TestMdc<T> extends HasRawElementComponent implements CanCreate<T>, MasterConnectorProvider<T>, Refreshable {
        @Override
        public boolean isSelectedItemCreated() {
            return false;
        }

        @Override
        public void createItem(T item) {

        }

        @Override
        public UIBuilderMasterConnector<Component, T> getMasterConnector() {
            return null;
        }

        @Override
        public void refresh() {

        }
    }

    static class TestGrid extends HasRawElementComponent {

    }

    @Data
    static class TestParentItem {

        @OneToMany(mappedBy = "parent")
        private Set<TestItem> tests = new HashSet<>();

    }

    @Data
    public static class TestItem {

        private TestParentItem parent;

    }

    @Tag("detail-panel")
    public static class TestDetailPanel implements UIBuilderDetailCapable {}

    @Tag("uibuilder-editor-window")
    public static class TestEditorWindow implements UIBuilderDetailCapable {}

}
