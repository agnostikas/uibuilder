/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import io.devbench.uibuilder.annotations.EnableUIBuilder;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ContextConfiguration
@ComponentScan("io.devbench")
@EnableUIBuilder("io.devbench.uibuilder.core.controllerbean")
@ExtendWith(SpringExtension.class)
public class ControllerBeanManagerIntegrationTest {

    @Autowired
    @Qualifier("testBean")
    private TestControllerBean testControllerBean;

    @Test
    @DisplayName("Should find controller bean by name")
    public void should_find_controller_bean_by_name() {
        Object testBean = ControllerBeanManager.getInstance().getControllerBean("testBean");
        assertAll(
            () -> assertNotNull(testBean),
            () -> assertEquals(TestControllerBean.class, testBean.getClass())
        );
    }

    @Test
    @DisplayName("Should find extended controller bean by name")
    public void should_find_extended_controller_bean_by_name() {
        Object testBean = ControllerBeanManager.getInstance().getControllerBean("extTestBean");
        assertAll(
            () -> assertNotNull(testBean),
            () -> assertEquals(ExtendedTestControllerBean.class, testBean.getClass())
        );
    }

    @Test
    @DisplayName("Should find controller bean by type")
    public void should_find_controller_bean_by_type() {
        Object testBean = ControllerBeanManager.getInstance().getControllerBean(ExtendedTestControllerBean.class);
        assertAll(
            () -> assertNotNull(testBean),
            () -> assertEquals(ExtendedTestControllerBean.class, testBean.getClass())
        );
    }

    @Test
    @DisplayName("Should find event handler context by qualified name")
    public void should_find_event_handler_context_by_qualified_name() {
        Optional<UIEventHandlerContext> optionalEventHandlerContext = ControllerBeanManager.getInstance().getEventHandlerContext("testBean::testEventHandler");
        assertAll(
            () -> assertTrue(optionalEventHandlerContext.isPresent()),
            () -> assertEquals(TestControllerBean.class.getDeclaredMethod("testEventHandler", Button.class), optionalEventHandlerContext.get().getMethod())
        );
    }

    @Test
    @DisplayName("Should call correct event handler when found")
    public void should_call_correct_event_handler_when_found() {
        Optional<UIEventHandlerContext> optionalEventHandlerContext = ControllerBeanManager.getInstance()
            .getEventHandlerContext("testBean::testEventHandlerWithItem");
        assertTrue(optionalEventHandlerContext.isPresent());
        Button button = new Button();
        optionalEventHandlerContext.ifPresent(eventHandler -> eventHandler.callEventHandlerWithItem(button));
        assertEquals(button, testControllerBean.getItem());
    }

    @Test
    @DisplayName("Should get field injected button")
    void test_should_get_field_injected_button() {
        UI currentUI = UI.getCurrent();
        VaadinSession vaadinSessionMock = Mockito.mock(VaadinSession.class);
        WrappedSession wrappedSessionMock = Mockito.mock(WrappedSession.class);
        doReturn(vaadinSessionMock).when(currentUI).getSession();
        doReturn(wrappedSessionMock).when(vaadinSessionMock).getSession();
        doReturn("12").when(wrappedSessionMock).getId();
        doReturn(112).when(currentUI).getUIId();

        ComponentInjectionPoint testComponentInjectionPoint = ControllerBeanManager.getInstance().findInjectionPointsById().get("testComponent");
        Button button = new Button();
        testComponentInjectionPoint.setValue(button);

        Button testButton = testControllerBean.getButton();

        assertNotNull(testButton);
        assertSame(button, testButton);
    }

}
