/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.data;

import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.interceptors.ItemDataSourceBindingContext;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceProvider;
import io.devbench.uibuilder.data.common.datasource.CommonDataSourceSelector;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component(BasicSpringDataSourceProvider.BASIC_SPRING_DATA_SOURCE_PROVIDER_BEAN_NAME)
public class BasicSpringDataSourceProvider<T>
    extends SpringDataSourceProvider<CollectionDataSource<T>, CommonDataSourceSelector>
    implements CommonDataSourceProvider<CollectionDataSource<T>> {

    public static final String BASIC_SPRING_DATA_SOURCE_PROVIDER_BEAN_NAME = "uibuilderBasicSpringDataSourceProvider";

    protected final ApplicationContext applicationContext;
    protected Map<String, BindingContext> registeredBindingContexts;
    protected Map<String, String> idToDataSourceName;

    public BasicSpringDataSourceProvider(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.registeredBindingContexts = new HashMap<>();
        this.idToDataSourceName = new HashMap<>();
    }

    @Override
    public CollectionDataSource<T> createNewDataSource(String dataSourceId, @Nullable String optionalDefaultQueryName) {
        BindingContext bindingContext = registeredBindingContexts.get(createBindingsKey(dataSourceId, optionalDefaultQueryName));
        ItemDataSourceBindingContext itemDataSourceBindingContext = (ItemDataSourceBindingContext) bindingContext;
        return new CollectionDataSource<>(itemDataSourceBindingContext, itemDataSourceBindingContext.getKeyPaths(), Collections.emptyList());
    }

    @Override
    public Set<Class<? extends Annotation>> getInterestedAnnotationTypes() {
        return Collections.emptySet();
    }

    @Override
    protected void lateInit() {

    }

    @Override
    public void registerBindingContextForDataSource(BindingContext bindingContext, @Nullable CommonDataSourceSelector dataSourceSelector) {
        idToDataSourceName.put(bindingContext.getDataSourceId(), bindingContext.getDataSourceName());
        if (!bindingContext.getBindings().isEmpty()) {
            final String bindingsKey = createBindingsKey(bindingContext.getDataSourceId(), getOptionalDefaultQueryName(dataSourceSelector));
            registeredBindingContexts.put(bindingsKey, bindingContext);
        }
    }

    @Nullable
    @Override
    public final BindingContext getBindingContextForName(String dataSourceName, @Nullable CommonDataSourceSelector dataSourceSelector) {
        return registeredBindingContexts.get(createBindingsKey(dataSourceName, getOptionalDefaultQueryName(dataSourceSelector)));
    }
}
