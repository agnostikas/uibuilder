/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.crud;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.annotation.UIScope;
import elemental.json.JsonObject;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.crud.GenericCrudGridInlineEditorControllerBean;
import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.data.collectionds.CollectionDataSource;
import io.devbench.uibuilder.data.collectionds.datasource.component.AbstractDataSourceComponent;
import io.devbench.uibuilder.data.common.component.AbstractGenericGridInlineEditorControllerBean;
import io.devbench.uibuilder.data.common.datasource.CommonDataSource;
import io.devbench.uibuilder.spring.crud.exception.GenericInlineEditorSaveItemException;
import io.devbench.uibuilder.spring.data.SpringCommonDataSourceProvider;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@UIScope
@ControllerBean(GenericCrudGridInlineEditorControllerBean.BUILT_IN_GENERIC_CRUD_GRID_INLINE_EDITOR_CONTROLLER_BEAN_NAME)
public class SpringGenericCrudGridInlineEditorControllerBean<TYPE>
    extends AbstractGenericGridInlineEditorControllerBean<TYPE>
    implements GenericCrudGridInlineEditorControllerBean<TYPE> {

    @Getter(AccessLevel.PACKAGE)
    private final Map<String, String> datasourceByGridIdMap;
    private final SpringCommonDataSourceProvider commonDataSourceProvider;
    private final ApplicationContext applicationContext;

    public SpringGenericCrudGridInlineEditorControllerBean(SpringCommonDataSourceProvider commonDataSourceProvider,
                                                           ApplicationContext applicationContext) {

        this.commonDataSourceProvider = commonDataSourceProvider;
        this.applicationContext = applicationContext;
        this.datasourceByGridIdMap = new HashMap<>();
    }

    @Override
    public void registerDataSourceName(@NotNull String gridId, @NotNull String datasourceName) {
        datasourceByGridIdMap.put(gridId, datasourceName);
    }

    @Override
    public void onInlineItemSave(@NotNull TYPE modifiedItem, @NotNull JsonObject modifiedJsonItem, @Nullable Component component) {
        if (component != null) {
            component.getElement().callJsFunction("_inlineItemSaved", modifiedJsonItem);
            if (!isItemDatasourceDriven(component)) {
                String id = component.getId()
                    .orElseThrow(() -> new GenericInlineEditorSaveItemException("Cannot save item, component ID not found"));

                String dataSourceName = datasourceByGridIdMap.get(id);
                if (dataSourceName != null) {
                    Optional<Class<? extends PagingAndSortingRepository<TYPE, ?>>> foundRepositoryClass = findRepositoryClass(dataSourceName);
                    if (foundRepositoryClass.isPresent()) {
                        Class<? extends PagingAndSortingRepository<TYPE, ?>> repositoryClass = foundRepositoryClass.get();
                        ObjectProvider<? extends PagingAndSortingRepository<TYPE, ?>> repositoryProvider = applicationContext.getBeanProvider(repositoryClass);
                        PagingAndSortingRepository<TYPE, ?> repository = repositoryProvider.getIfAvailable();
                        if (repository != null) {

                            repository.save(modifiedItem);

                        } else {
                            throw new GenericInlineEditorSaveItemException("Could not save item, repository not found");
                        }
                    } else {
                        throw new GenericInlineEditorSaveItemException("Could not save item, target datasource not found");
                    }
                } else {
                    throw new GenericInlineEditorSaveItemException("Could not save item, data-source name not found");
                }
            }
        } else {
            throw new GenericInlineEditorSaveItemException("Could not save item, component not found");
        }
    }

    private Optional<Class<? extends PagingAndSortingRepository<TYPE, ?>>> findRepositoryClass(String dataSourceName) {
        @SuppressWarnings("unchecked")
        Pair<TargetDataSource, Class<? extends PagingAndSortingRepository<TYPE, ?>>> datasourceRepoPair =
            (Pair<TargetDataSource, Class<? extends PagingAndSortingRepository<TYPE, ?>>>)
                commonDataSourceProvider.getDataSourceRepositories().get(dataSourceName);

        if (datasourceRepoPair != null) {
            return Optional.ofNullable(datasourceRepoPair.getValue());
        }

        return Optional.empty();
    }

    private boolean isItemDatasourceDriven(Component component) {
        if (component instanceof AbstractDataSourceComponent) {
            CommonDataSource<?, ?, ?, ?> dataSource = ((AbstractDataSourceComponent<?>) component).getDataSource();
            return dataSource instanceof CollectionDataSource;
        }
        return false;
    }

}
