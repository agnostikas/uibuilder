/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.singleton;

import io.devbench.uibuilder.api.singleton.ContextAwareSingletonHolder;
import io.devbench.uibuilder.api.singleton.SingletonProvider;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;

import java.util.Collection;
import java.util.Collections;

final class SpringSingletonManagerContextHolder {

    private Collection<SingletonProvider> singletonProviders;
    private Collection<ContextAwareSingletonHolder> contextAwareSingletonHolders;

    SpringSingletonManagerContextHolder(@NotNull ApplicationContext applicationContext) {
        singletonProviders = applicationContext.getBeansOfType(SingletonProvider.class).values();
        contextAwareSingletonHolders = applicationContext.getBeansOfType(ContextAwareSingletonHolder.class).values();
    }

    Collection<SingletonProvider> getSingletonProviders() {
        return Collections.unmodifiableCollection(singletonProviders);
    }

    Collection<ContextAwareSingletonHolder> getContextAwareSingletonHolders() {
        return Collections.unmodifiableCollection(contextAwareSingletonHolders);
    }

}
