/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.page;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

class PageScopeBeanId {

    private final String beanName;
    private final String pageName;

    private PageScopeBeanId(String beanName, String pageName) {
        this.beanName = Objects.requireNonNull(beanName);
        this.pageName = Objects.requireNonNull(pageName);
    }

    public static PageScopeBeanId of(@NotNull String beanName, @NotNull String pageName) {
        return new PageScopeBeanId(beanName, pageName);
    }

    public String getBeanName() {
        return beanName;
    }

    public String getPageName() {
        return pageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PageScopeBeanId that = (PageScopeBeanId) o;
        return getBeanName().equals(that.getBeanName()) &&
            getPageName().equals(that.getPageName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBeanName(), getPageName());
    }
}
