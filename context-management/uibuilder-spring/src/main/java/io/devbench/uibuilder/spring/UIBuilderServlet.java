/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring;

import com.vaadin.flow.function.DeploymentConfiguration;
import com.vaadin.flow.server.ServiceException;
import com.vaadin.flow.server.VaadinServletService;
import com.vaadin.flow.spring.SpringServlet;
import com.vaadin.flow.spring.SpringVaadinServletService;
import io.devbench.uibuilder.spring.configuration.staticcontent.StaticContentDescription;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;

public class UIBuilderServlet extends SpringServlet {

    private final ApplicationContext context;

    @Nullable
    private final StaticContentDescription staticContentDescription;
    private final ErrorHandlerRegistrator errorHandlerRegistrator;

    /**
     * Creates a new Vaadin servlet instance with the application
     * {@code context} provided.
     *
     * @param context the Spring application context
     * @param staticContentDescription
     */
    public UIBuilderServlet(ApplicationContext context, @Nullable StaticContentDescription staticContentDescription) {
        super(context, true);
        this.context = context;
        this.staticContentDescription = staticContentDescription;
        this.errorHandlerRegistrator = context.getBean(ErrorHandlerRegistrator.class);
    }

    @Override
    protected VaadinServletService createServletService(DeploymentConfiguration deploymentConfiguration) throws ServiceException {
        SpringVaadinServletService service = new UIBuilderSpringVaadinServletService(
            this, deploymentConfiguration, context, staticContentDescription, errorHandlerRegistrator);
        service.init();
        return service;
    }
}
