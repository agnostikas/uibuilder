/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.singleton;

import io.devbench.uibuilder.api.singleton.ContextAwareSingletonHolder;
import io.devbench.uibuilder.api.singleton.ContextUnawareSingletonHolder;
import io.devbench.uibuilder.api.singleton.SingletonManagerProvider;
import io.devbench.uibuilder.api.singleton.SingletonProvider;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Service
public class SpringSingletonManagerProvider implements SingletonManagerProvider, ApplicationContextAware {

    private static SpringSingletonManagerContextHolder contextHolder;

    private Optional<SpringSingletonManagerContextHolder> getContextHolder() {
        return Optional.ofNullable(contextHolder);
    }

    private void setContextHolder(SpringSingletonManagerContextHolder contextHolder) {
        SpringSingletonManagerProvider.contextHolder = contextHolder;
    }

    @Override
    public Collection<SingletonProvider> getSingletonProviders() {
        return getContextHolder().orElseThrow(SpringSingletonManagerContextHolderNotFoundException::new).getSingletonProviders();
    }

    @Override
    public Collection<ContextAwareSingletonHolder> getContextAwareSingletonHolders() {
        return getContextHolder().orElseThrow(SpringSingletonManagerContextHolderNotFoundException::new).getContextAwareSingletonHolders();
    }

    @Override
    public Collection<ContextUnawareSingletonHolder> getContextUnawareSingletonHolders() {
        return Collections.emptyList();
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        setContextHolder(new SpringSingletonManagerContextHolder(applicationContext));
    }

}
