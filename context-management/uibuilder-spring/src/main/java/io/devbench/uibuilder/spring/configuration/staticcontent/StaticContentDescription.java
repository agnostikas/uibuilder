/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.configuration.staticcontent;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StaticContentDescription {

    private final String url;
    private final List<String> paths;

    public StaticContentDescription(String url, String[] paths) {
        this.url = createUrlPath(url);
        this.paths = Collections.unmodifiableList(Arrays.asList(paths));
    }

    private String createUrlPath(String url) {
        if (url.startsWith("/")) {
            return url;
        } else {
            return "/" + url;
        }
    }

    /**
     * @return The url prefix of static files. Starts with the `/` prefix.
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return An unmodifiable list where the app should look up the static files.
     */
    public List<String> getPaths() {
        return paths;
    }
}
