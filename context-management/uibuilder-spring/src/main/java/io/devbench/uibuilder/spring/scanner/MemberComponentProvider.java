/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.scanner;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.env.Environment;

public class MemberComponentProvider extends ClassPathScanningCandidateComponentProvider {

    private final boolean considerInterfaces;

    public MemberComponentProvider(boolean considerInterfaces, Environment environment) {
        super(false, environment);
        this.considerInterfaces = considerInterfaces;
    }


    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        if (considerInterfaces) {
            return (beanDefinition.getMetadata().isIndependent() && beanDefinition.getMetadata().isInterface())
                || super.isCandidateComponent(beanDefinition);
        }
        return super.isCandidateComponent(beanDefinition);
    }
}
