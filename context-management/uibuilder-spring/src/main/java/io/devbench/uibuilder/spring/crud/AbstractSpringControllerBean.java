/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.crud;

import io.devbench.uibuilder.api.crud.AbstractControllerBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.function.Supplier;

public abstract class AbstractSpringControllerBean<TYPE, REPOSITORY extends PagingAndSortingRepository<TYPE, ?>> extends AbstractControllerBean<TYPE> {
    protected final REPOSITORY repository;

    public AbstractSpringControllerBean(Supplier<TYPE> baseConstructor, REPOSITORY repository) {
        super(baseConstructor, repository::save, repository::delete);
        this.repository = repository;
    }
}
