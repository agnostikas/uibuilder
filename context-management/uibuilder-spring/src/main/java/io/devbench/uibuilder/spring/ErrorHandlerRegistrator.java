/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
class ErrorHandlerRegistrator implements RequestHandler {

    @Autowired
    private ObjectProvider<ErrorHandler> errorHandlerProvider;

    @Override
    public boolean handleRequest(VaadinSession session, VaadinRequest request, VaadinResponse response) {
        try {
            if (!session.hasLock()) {
                session.lock();
            }
            session.setErrorHandler(this::handleError);
        } finally {
            session.unlock();
        }
        return false;
    }

    private void handleError(ErrorEvent event) {
        ErrorHandler errorHandler = errorHandlerProvider.getIfAvailable();
        if (errorHandler != null) {
            errorHandler.error(event);
        } else {
            Dialog dialog = new Dialog();
            VerticalLayout verticalLayout = new VerticalLayout(
                new H3("Internal error."),
                new Label("Please notify the Administrator."),
                new Button("Ok", ignore -> {
                    dialog.close();
                })
            );
            verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
            dialog.add(verticalLayout);
            dialog.open();
            log.error("Internal error", event.getThrowable());
        }
    }


}
