/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring.configuration;

import io.devbench.uibuilder.spring.configuration.basepackages.UIBuilderScanPackages;
import io.devbench.uibuilder.spring.startup.RegistryInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.function.Consumer;

@Configuration
@PropertySource("classpath:/vaadin-uibuilder-defaults.properties")
public class SpringConfiguration {

    @Bean
    public RegistryInitializer getRegistryInitializer(@Autowired ApplicationContext applicationContext,
                                                      @Autowired UIBuilderScanPackages scanPackages,
                                                      @Autowired(required = false) @Qualifier("beforeRegistryInitializer")
                                                          Consumer<ApplicationContext> beforeRegistryInitializer) throws Exception {
        if (beforeRegistryInitializer != null) {
            beforeRegistryInitializer.accept(applicationContext);
        }
        return new RegistryInitializer(applicationContext, scanPackages);
    }

}
