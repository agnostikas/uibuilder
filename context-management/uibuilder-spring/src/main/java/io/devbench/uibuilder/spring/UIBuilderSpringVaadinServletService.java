/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.spring;

import com.vaadin.flow.function.DeploymentConfiguration;
import com.vaadin.flow.server.RequestHandler;
import com.vaadin.flow.server.ServiceException;
import com.vaadin.flow.server.VaadinServlet;
import com.vaadin.flow.spring.SpringVaadinServletService;
import io.devbench.uibuilder.data.common.dataprovidersupport.DataProviderRequestHandler;
import io.devbench.uibuilder.spring.configuration.staticcontent.StaticContentDescription;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;

import java.net.URL;
import java.util.List;
import java.util.Objects;

public final class UIBuilderSpringVaadinServletService extends SpringVaadinServletService {

    @Nullable
    private final StaticContentDescription staticContentDescription;
    private final ErrorHandlerRegistrator errorHandlerRegistrator;

    /**
     * Creates an instance connected to the given servlet and using the given
     * configuration with provided application {@code context}.
     *
     * @param servlet                  the servlet which receives requests
     * @param deploymentConfiguration  the configuration to use
     * @param context
     * @param staticContentDescription
     */
    public UIBuilderSpringVaadinServletService(
        VaadinServlet servlet,
        DeploymentConfiguration deploymentConfiguration,
        ApplicationContext context,
        @Nullable StaticContentDescription staticContentDescription,
        ErrorHandlerRegistrator errorHandlerRegistrator
    ) {
        super(servlet, deploymentConfiguration, context);
        this.staticContentDescription = staticContentDescription;
        this.errorHandlerRegistrator = errorHandlerRegistrator;
    }

    @Override
    protected List<RequestHandler> createRequestHandlers() throws ServiceException {
        List<RequestHandler> requestHandlers = super.createRequestHandlers();
        requestHandlers.add(errorHandlerRegistrator);
        requestHandlers.add(new DataProviderRequestHandler());
        return requestHandlers;
    }

    @Override
    public URL getStaticResource(String path) {
        if (staticContentDescription != null) {
            String requestedPath = prefixPathIfNecessary(path);
            if (requestedPath.indexOf(staticContentDescription.getUrl()) == 0) {
                String withoutStaticPrefix = requestedPath.substring(staticContentDescription.getUrl().length());
                return staticContentDescription
                    .getPaths()
                    .stream()
                    .map(baseDir -> baseDir + withoutStaticPrefix)
                    .map(UIBuilderSpringVaadinServletService.class::getResource)
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElseGet(() -> super.getStaticResource(path));
            }
        }
        return super.getStaticResource(path);
    }

    private String prefixPathIfNecessary(String path) {
        if (path.indexOf("/frontend") == 0) {
            path = path.substring("/frontend".length());
        }

        if (path.startsWith("/")) {
            return path;
        } else {
            return "/" + path;
        }
    }
}
