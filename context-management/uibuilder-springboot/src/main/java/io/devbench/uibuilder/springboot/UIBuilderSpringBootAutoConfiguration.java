/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.springboot;

import com.vaadin.flow.server.Constants;
import com.vaadin.flow.spring.RootMappedCondition;
import com.vaadin.flow.spring.SpringServlet;
import com.vaadin.flow.spring.VaadinConfigurationProperties;
import io.devbench.uibuilder.spring.UIBuilderServlet;
import io.devbench.uibuilder.spring.configuration.staticcontent.StaticContentDescription;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

@Configuration
@AutoConfigureBefore(WebMvcAutoConfiguration.class)
@ConditionalOnClass(ServletContextInitializer.class)
@EnableConfigurationProperties(VaadinConfigurationProperties.class)
public class UIBuilderSpringBootAutoConfiguration {

    private static final String VAADIN_SERVLET_MAPPING = "/vaadinServlet/*";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private VaadinConfigurationProperties configurationProperties;

    @Nullable
    @Autowired(required = false)
    private StaticContentDescription staticContentDescription;

    @Bean
    @Primary
    public ServletRegistrationBean<SpringServlet> uibuilderSpringBootServlet() {
        String mapping = configurationProperties.getUrlMapping();
        Map<String, String> initParameters = new HashMap<>();
        boolean rootMapping = RootMappedCondition.isRootMapping(mapping);
        if (rootMapping) {
            mapping = VAADIN_SERVLET_MAPPING;
            initParameters.put(Constants.SERVLET_PARAMETER_PUSH_URL,
                makeContextRelative(mapping.replace("*", "")));
        }
        UIBuilderServlet uiBuilderServlet = new UIBuilderServlet(context, staticContentDescription);
        ServletRegistrationBean<SpringServlet> registration = new ServletRegistrationBean<>(uiBuilderServlet, mapping);
        registration.setInitParameters(initParameters);
        registration.setOrder(1);
        registration.setAsyncSupported(configurationProperties.isAsyncSupported());
        registration.setName(ClassUtils.getShortNameAsProperty(SpringServlet.class));
        return registration;
    }

    private static String makeContextRelative(String url) {
        // / -> context://
        // foo -> context://foo
        // /foo -> context://foo
        if (url.startsWith("/")) {
            url = url.substring(1);
        }
        return "context://" + url;
    }
}
