### Feature description <!-- (required) -->

###### Feature proposal issue number: 
<!-- The issue number of the feature proposal -->

###### Short description of feature:
<!-- Describe the feature in a few sentences -->

### Checklist

- [ ] Create wiki page for plan description
- [ ] Attach related images
- [ ] Create skeleton implementation issue


/label ~Design 
