/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

class CollectionUtilTest {

    @Test
    @DisplayName("Collections should be equal when both collections contains the same elements ignoring the order")
    void test_collections_should_be_equal_when_both_collections_contains_the_same_elements_ignoring_the_order() {
        Set<Integer> collectionOne = new HashSet<>(Arrays.asList(15, 25, 35));
        List<Integer> collectionTwo = new ArrayList<>(Arrays.asList(15, 25, 35));
        List<Integer> collectionThree = new ArrayList<>(Arrays.asList(25, 15, 35));

        assertTrue(CollectionUtil.isCollectionsEqual(collectionOne, collectionTwo));
        assertTrue(CollectionUtil.isCollectionsEqual(collectionOne, collectionThree));
        assertTrue(CollectionUtil.isCollectionsEqual(collectionTwo, collectionThree));

        collectionOne.remove(25);

        assertFalse(CollectionUtil.isCollectionsEqual(collectionOne, collectionTwo));
        assertFalse(CollectionUtil.isCollectionsEqual(collectionOne, collectionThree));
    }

}
