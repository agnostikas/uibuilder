/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.controllerbean.injectionpoint;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class ComponentInjectionPointTest {

    private ComponentInjectionPoint testObj;

    private Map<Method, Parameter> argumentInjectionPoints;

    @BeforeEach
    void setup() {
        argumentInjectionPoints = new HashMap<>();
        testObj = new ComponentInjectionPoint("testId", argumentInjectionPoints);
        UI currentUI = mock(UI.class);
        UI.setCurrent(currentUI);
        VaadinSession vaadinSessionMock = mock(VaadinSession.class);
        WrappedSession wrappedSessionMock = mock(WrappedSession.class);
        doReturn(vaadinSessionMock).when(currentUI).getSession();
        doReturn(wrappedSessionMock).when(vaadinSessionMock).getSession();
        doReturn("12").when(wrappedSessionMock).getId();
        doReturn(112).when(currentUI).getUIId();
    }

    @Test
    @DisplayName("Should decide if a injection point is ambigous or not")
    public void should_decide_if_a_injection_point_is_ambigous_or_not() throws NoSuchMethodException {
        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod2, parameter2);
        assertTrue(testObj.isAmbiguous());
    }

    @Test
    @DisplayName("Should get the unambiguous type of the injection point, even if multiple argument references the component")
    public void should_get_the_unambiguous_type_of_the_injection_point_even_if_multiple_argument_references_the_component() throws NoSuchMethodException {
        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod3 = TestBean.class.getDeclaredMethod("testMethod3", Button.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod3.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod3, parameter2);
        Class<?> type = testObj.getUnambiguousType();
        assertAll(
            () -> assertNotNull(type),
            () -> assertEquals(Button.class, type)
        );
    }

    @Test
    @DisplayName("Should decide if a method and its parameter matching to the injection point")
    public void should_decide_if_a_method_and_its_parameter_matching_to_the_injection_point() throws NoSuchMethodException {
        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod3 = TestBean.class.getDeclaredMethod("testMethod3", Button.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod3.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod3, parameter2);

        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter3 = testMethod2.getParameters()[0];

        assertAll(
            () -> assertTrue(testObj.isParameterMatching(testMethod1, parameter1)),
            () -> assertTrue(testObj.isParameterMatching(testMethod3, parameter2)),
            () -> assertFalse(testObj.isParameterMatching(testMethod2, parameter3))
        );
    }

    @Test
    @DisplayName("Should get the value for the specified parameter, from the previously set value, when the injection point is unambiguous")
    public void should_get_the_value_for_the_specified_parameter_from_the_previously_set_value_when_the_injection_point_is_unambiguous() throws NoSuchMethodException {
        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod3 = TestBean.class.getDeclaredMethod("testMethod3", Button.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod3.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod3, parameter2);

        Button button = new Button();
        testObj.setValue(button);

        assertSame(button, testObj.getValueForParameter(parameter2));
    }

    @Test
    @DisplayName("Should get the value for the specified parameter, by creating the component from the specified element if the injection point is ambiguous")
    public void should_get_the_value_for_the_specified_parameter_by_creating_the_component_from_the_specified_element_if_the_injection_point_is_ambiguous() throws NoSuchMethodException {
        UI ui = mock(UI.class);
        VaadinSession session = mock(VaadinSession.class);
        when(ui.getSession()).thenReturn(session);
        VaadinService service = mock(VaadinService.class);
        Instantiator instantiator = mock(Instantiator.class);
        when(instantiator.createComponent(Button.class)).thenReturn(new Button());
        when(service.getInstantiator()).thenReturn(instantiator);
        when(session.getService()).thenReturn(service);
        UI.setCurrent(ui);

        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod2, parameter2);

        Element element = new Element("vaadin-button");
        testObj.setElement(element);

        Component value = testObj.getValueForParameter(parameter1);

        assertAll(
            () -> assertNotNull(value),
            () -> assertEquals(Button.class, value.getClass()),
            () -> assertEquals(element.toString(), value.getElement().toString())
        );
    }

    @Test
    @DisplayName("Should decide if any of the injection points referenced parameters type is sublclass of the provided type")
    public void should_decide_if_any_of_the_injection_points_referenced_parameters_type_is_sublclass_of_the_provided_type() throws NoSuchMethodException {
        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod2, parameter2);

        assertAll(
            () -> assertTrue(testObj.isAnyComponentSubclassOf(Button.class)),
            () -> assertTrue(testObj.isAnyComponentSubclassOf(Checkbox.class)),
            () -> assertTrue(testObj.isAnyComponentSubclassOf(Component.class)),
            () -> assertFalse(testObj.isAnyComponentSubclassOf(TextField.class))
        );
    }

    @Test
    @DisplayName("Should throw exception when trying to get the unambiguous type of the injection point and the injection point in fact ambiguous")
    public void should_throw_exception_when_trying_to_get_the_unambiguous_type_of_the_injection_point_and_the_injection_point_in_fact_ambiguous() throws NoSuchMethodException {
        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod2, parameter2);

        InternalResolverException exception = assertThrows(InternalResolverException.class, () -> testObj.getUnambiguousType());
        assertEquals("This injection point is ambiguous, it's type not accessible this way", exception.getMessage());
    }

    @Test
    @DisplayName("Should get the component for parameter, if the injection point is ambiguous and the component already set for the previously set element")
    public void should_get_the_component_for_parameter_if_the_injection_point_is_ambiguous_and_the_component_already_set_for_the_previously_set_element() throws NoSuchMethodException {
        UI ui = mock(UI.class);
        VaadinSession session = mock(VaadinSession.class);
        when(ui.getSession()).thenReturn(session);
        VaadinService service = mock(VaadinService.class);
        Instantiator instantiator = mock(Instantiator.class);
        Button button = new Button();
        when(instantiator.createComponent(Button.class)).thenReturn(button);
        when(service.getInstantiator()).thenReturn(instantiator);
        when(session.getService()).thenReturn(service);
        UI.setCurrent(ui);

        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod2, parameter2);

        Element element = spy(new Element("vaadin-button"));
        when(element.getComponent()).thenReturn(Optional.of(button));
        testObj.setElement(element);

        Component value = testObj.getValueForParameter(parameter1);

        assertAll(
            () -> assertNotNull(value),
            () -> assertEquals(Button.class, value.getClass())
        );
    }

    @Test
    @DisplayName("Should throw exception, if trying get component for parameter if the injection point is ambiguous and the previously set element contains a different type of component")
    public void should_throw_exception_if_trying_get_component_for_parameter_if_the_injection_point_is_ambiguous_and_the_previously_set_element_contains_a_different_type_of_component() throws NoSuchMethodException {
        UI ui = mock(UI.class);
        VaadinSession session = mock(VaadinSession.class);
        when(ui.getSession()).thenReturn(session);
        VaadinService service = mock(VaadinService.class);
        Instantiator instantiator = mock(Instantiator.class);
        when(instantiator.createComponent(Checkbox.class)).thenReturn(new Checkbox());
        when(service.getInstantiator()).thenReturn(instantiator);
        when(session.getService()).thenReturn(service);
        UI.setCurrent(ui);

        Method testMethod1 = TestBean.class.getDeclaredMethod("testMethod1", Button.class, TextField.class);
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod1.getParameters()[0];
        Parameter parameter2 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod1, parameter1);
        argumentInjectionPoints.put(testMethod2, parameter2);

        Element element = spy(new Element("vaadin-button"));
        when(element.getComponent()).thenReturn(Optional.of(new Button()));
        testObj.setElement(element);

        InternalResolverException illegalStateException = assertThrows(InternalResolverException.class, () -> testObj.getValueForParameter(parameter2));

        assertEquals("Component (id: <UNKNOWN>) cannot be injected to parameter (with requested id: testId), " +
                "because the element already has a component registered to it with different type. " +
                "Component type: Button, inject point type: Checkbox",
            illegalStateException.getMessage());
    }

    @Test
    @DisplayName("Should get null when the value is null for the specified parameter")
    public void test_should_get_null_when_the_value_is_null_for_the_specified_parameter() throws Exception {
        Method testMethod2 = TestBean.class.getDeclaredMethod("testMethod2", Checkbox.class);
        Parameter parameter1 = testMethod2.getParameters()[0];
        argumentInjectionPoints.put(testMethod2, parameter1);

        testObj.setValue(null);

        Component valueForParameter = testObj.getValueForParameter(parameter1);
        assertNull(valueForParameter);
    }

    @Test
    @DisplayName("Should get component for type")
    void test_should_get_component_for_type() {
        TestComponent component = new TestComponent();
        Element componentElement = mock(Element.class);
        doReturn(Optional.of(component)).when(componentElement).getComponent();

        testObj = new ComponentInjectionPoint("componentId", new HashSet<>(Arrays.asList(AnotherTestComponent.class, TestComponent.class)));
        testObj.setElement(componentElement);

        Component valueForType = testObj.getValueForType(TestComponent.class);

        assertTrue(valueForType instanceof TestComponent);
    }

    @Test
    @DisplayName("Should throw exception, if trying get component for field if the injection point is ambiguous and the previously set element contains a different type of component")
    public void should_throw_exception_if_trying_get_component_for_field_if_the_injection_point_is_ambiguous_and_the_previously_set_element_contains_a_different_type_of_component() throws NoSuchMethodException {
        UI ui = mock(UI.class);
        VaadinSession session = mock(VaadinSession.class);
        when(ui.getSession()).thenReturn(session);
        VaadinService service = mock(VaadinService.class);
        Instantiator instantiator = mock(Instantiator.class);
        when(instantiator.createComponent(Checkbox.class)).thenReturn(new Checkbox());
        when(service.getInstantiator()).thenReturn(instantiator);
        when(session.getService()).thenReturn(service);
        UI.setCurrent(ui);

        Method testMethod3 = TestBean.class.getDeclaredMethod("testMethod3", Button.class);
        Parameter parameter1 = testMethod3.getParameters()[0];
        argumentInjectionPoints.put(testMethod3, parameter1);
        testObj.setTargetClasses(Collections.singleton(TextField.class));

        Element element = spy(new Element("vaadin-button"));
        when(element.getComponent()).thenReturn(Optional.of(new Button()));
        testObj.setElement(element);

        InternalResolverException illegalStateException = assertThrows(InternalResolverException.class, () -> testObj.getValueForType(TextField.class));

        assertEquals("Component (id: <UNKNOWN>) cannot be injected to parameter (with requested id: testId), " +
                "because the element already has a component registered to it with different type. " +
                "Component type: Button, inject point type: TextField",
            illegalStateException.getMessage());
    }

    @Test
    @DisplayName("Should return parameterized class if it is the same in every injection point")
    void test_should_return_parameterized_class_if_it_is_the_same_in_every_injection_point() throws NoSuchMethodException {
        Method testMethod1 = TestUnambiguousParameterizedBean.class.getDeclaredMethod("testMethod1", Map.class);
        Method testMethod2 = TestUnambiguousParameterizedBean.class.getDeclaredMethod("testMethod2", Map.class);
        argumentInjectionPoints.put(testMethod1, testMethod1.getParameters()[0]);
        argumentInjectionPoints.put(testMethod2, testMethod2.getParameters()[0]);

        Optional<Class<?>> unambiguousParameterizedType = testObj.getUnambiguousParameterizedType();
        assertNotNull(unambiguousParameterizedType);
        assertTrue(unambiguousParameterizedType.isPresent());
        assertEquals(String.class, unambiguousParameterizedType.get());
    }

    @Test
    @DisplayName("Should not return parameterized class if it differs in at least one injection point")
    void test_should_not_return_parameterized_class_if_it_differs_in_at_least_one_injection_point() throws NoSuchMethodException {
        Method testMethod1 = TestAmbiguousParameterizedBean.class.getDeclaredMethod("testMethod1", Map.class);
        Method testMethod2 = TestAmbiguousParameterizedBean.class.getDeclaredMethod("testMethod2", Map.class);
        Method testMethod3 = TestAmbiguousParameterizedBean.class.getDeclaredMethod("testMethod3", Map.class);
        argumentInjectionPoints.put(testMethod1, testMethod1.getParameters()[0]);
        argumentInjectionPoints.put(testMethod2, testMethod2.getParameters()[0]);
        argumentInjectionPoints.put(testMethod3, testMethod3.getParameters()[0]);

        Optional<Class<?>> unambiguousParameterizedType = testObj.getUnambiguousParameterizedType();
        assertNotNull(unambiguousParameterizedType);
        assertFalse(unambiguousParameterizedType.isPresent());
    }

    @Tag("test-component")
    public static class TestComponent extends Component {

    }

    @Tag("another-test-component")
    public static class AnotherTestComponent extends Component {

    }

    public static class TestBean {

        public void testMethod1(Button arg1, TextField arg2) {
        }

        public void testMethod2(Checkbox arg3) {
        }

        public void testMethod3(Button arg4) {
        }

    }

    public static class TestUnambiguousParameterizedBean {

        public void testMethod1(Map<String, Integer> stringIntegerMap) {

        }

        public void testMethod2(Map<String, String> stringStringMap) {

        }

    }

    public static class TestAmbiguousParameterizedBean {

        public void testMethod1(Map<String, Integer> stringIntegerMap) {

        }

        public void testMethod2(Map<Integer, String> integerStringMap) {

        }

        public void testMethod3(Map<String, Double> stringDoubleMap) {

        }

    }

}
