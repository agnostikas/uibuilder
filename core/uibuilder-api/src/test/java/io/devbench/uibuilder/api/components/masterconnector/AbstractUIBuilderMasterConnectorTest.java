/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.components.masterconnector;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.api.exceptions.SuperNotCalledException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AbstractUIBuilderMasterConnectorTest {

    @Test
    @DisplayName("should_register_test_and_remove_listener")
    void test_should_register_test_and_remove_listener() {
        TextField testComponent = new TextField();
        Collection<String> testPrevItems = Collections.emptyList();
        Collection<String> testNewlyItems = Collections.emptyList();

        TestMasterConnector<String> connector = new TestMasterConnector<>();

        AtomicInteger callCounter = new AtomicInteger(0);

        Registration registration = connector.addSelectionChangedListener(event -> {
            int i = callCounter.incrementAndGet();
            assertSame(testComponent, event.getSource());
            if (i == 1) {
                assertFalse(event.isFromClient());
                assertSame(testPrevItems, event.getPreviouslySelectedItems());
                assertSame(testNewlyItems, event.getNewlySelectedItems());
            } else {
                assertTrue(event.isFromClient());
                assertNull(event.getPreviouslySelectedItems());
                assertNull(event.getNewlySelectedItems());
            }
        });

        assertNotNull(registration);

        connector.fireSelectionChangedEvent(new UIBuilderMasterConnector.MasterSelectionChangedEvent<>(
            testComponent, false, testPrevItems, testNewlyItems));
        connector.fireSelectionChangedEvent(new UIBuilderMasterConnector.MasterSelectionChangedEvent<>(
            testComponent, true, null, null));

        registration.remove();

        assertEquals(2, callCounter.get());

        connector.fireSelectionChangedEvent(new UIBuilderMasterConnector.MasterSelectionChangedEvent<>(
            testComponent, true, null, null));

        assertEquals(2, callCounter.get(), "Last call should not be received, listener has been removed");
    }

    @Test
    public void should_throw_exception_when_the_on_connect_does_not_call_the_super_method() {
        class NotCallingSuper extends TestMasterConnector<Component> {
            @Override
            protected void onConnect(Component masterComponent) {
                // do not call super!
            }
        }

        SuperNotCalledException exception = assertThrows(SuperNotCalledException.class, () -> new NotCallingSuper().connect(mock(Component.class)));
        assertEquals("The `onConnect` method of [" + NotCallingSuper.class.getName() + "] should call super.onConnect();", exception.getMessage());
    }

    static class TestMasterConnector<T> extends AbstractUIBuilderMasterConnector<Component, T> {
        TestMasterConnector() {
            super(Component.class);
        }

        @Override
        public boolean isApplicable(Class componentClass) {
            return false;
        }

        @Override
        public void disconnect() {

        }

        @Override
        public Collection<T> getSelectedItems() {
            return null;
        }

        @Override
        public void setSelectedItems(Collection<T> items) {

        }

        @Override
        public void refresh() {

        }

        @Override
        public void refresh(T item) {

        }

        @Override
        public void setEnabled(boolean enabled) {

        }

        @Override
        public boolean isEnabled() {
            return false;
        }

        @Override
        public boolean isDirectModifiable() {
            return false;
        }

        @Override
        public void addItem(T item) {

        }

        @Override
        public void removeItem(T item) {

        }
    }

}
