/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.parse;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.exceptions.ParserException;
import org.jsoup.nodes.Element;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;


public interface ParseInterceptor {

    default <V> void processAttribute(Element element,
                                      String attribute,
                                      Function<String, V> attributeValueConverter,
                                      Consumer<V> valueConsumer) {
        processAttribute(element, attribute, attributeValueConverter, valueConsumer,
            (stringValue, exception) ->
                new ParserException("Illegal value in the " + attribute + " attribute of the " + element.tagName() + " component: " + stringValue, exception));
    }

    default <V> void processAttribute(Element element,
                                      String attribute,
                                      Function<String, V> attributeValueConverter,
                                      Consumer<V> valueConsumer,
                                      BiFunction<String, Exception, RuntimeException> converterException) {

        if (element.hasAttr(attribute)) {
            String stringValue = element.attr(attribute);
            try {
                V value = attributeValueConverter.apply(stringValue);
                valueConsumer.accept(value);
            } catch (Exception e) {
                throw converterException.apply(stringValue, e);
            }
        }
    }

    default boolean isInstantiator(Element element) {
        return false;
    }

    default Component instantiateComponent() {
        return null;
    }

    void intercept(Component component, Element element);

    default void setParentComponent(Component parentComponent) {}

    default void setHtmlPath(String htmlPath) {
        //ignore
    }

    default String getHtmlPath() {
        //ignore
        return "";
    }

    boolean isApplicable(Element element);

}
