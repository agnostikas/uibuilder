/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.components.masterconnector;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.shared.Registration;
import io.devbench.uibuilder.api.exceptions.SuperNotCalledException;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractUIBuilderMasterConnector<COMP extends Component, ITEM_VALUE> implements UIBuilderMasterConnector<COMP, ITEM_VALUE> {

    private List<MasterSelectionChangedListener<COMP, ITEM_VALUE>> selectionChangedListeners = new ArrayList<>();

    private COMP masterComponent;
    private boolean onConnectCalled;

    private final Class<COMP> componentClass;

    protected AbstractUIBuilderMasterConnector(Class<COMP> componentClass) {
        this.componentClass = componentClass;
    }

    @Override
    public boolean isApplicable(Class componentClass) {
        return this.componentClass.isAssignableFrom(componentClass);
    }

    @Override
    public Registration addSelectionChangedListener(MasterSelectionChangedListener<COMP, ITEM_VALUE> selectionChangedListener) {
        selectionChangedListeners.add(selectionChangedListener);
        return (Registration) () -> selectionChangedListeners.remove(selectionChangedListener);
    }

    @Override
    @NotNull
    public final COMP getMasterComponent() {
        return Objects.requireNonNull(masterComponent);
    }

    @Override
    public final void connect(COMP masterComponent) {
        this.onConnectCalled = false;
        onConnect(masterComponent);
        if (!this.onConnectCalled) {
            throw new SuperNotCalledException("The `onConnect` method of [" + this.getClass().getName() + "] should call super.onConnect();");
        }
    }

    protected void onConnect(COMP masterComponent) {
        this.masterComponent = Objects.requireNonNull(masterComponent);
        this.onConnectCalled = true;
    }

    protected void fireSelectionChangedEvent(MasterSelectionChangedEvent<COMP, ITEM_VALUE> event) {
        selectionChangedListeners.forEach(listener -> listener.onComponentEvent(event));
    }

}
