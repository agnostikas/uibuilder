/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.components.masterconnector;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;

public interface UIBuilderMasterConnector<COMP extends Component, ITEM_VALUE> {

    int DEFAULT_PRIORITY = 1000;

    @Getter
    class MasterSelectionChangedEvent<COMP extends Component, ITEM_VALUE> extends ComponentEvent<COMP> {
        private Collection<ITEM_VALUE> newlySelectedItems;
        private Collection<ITEM_VALUE> previouslySelectedItems;

        public MasterSelectionChangedEvent(COMP source, boolean fromClient,
                                           Collection<ITEM_VALUE> previouslySelectedItems,
                                           Collection<ITEM_VALUE> newlySelectedItems) {

            super(source, fromClient);
            this.previouslySelectedItems = previouslySelectedItems;
            this.newlySelectedItems = newlySelectedItems;
        }
    }

    interface MasterSelectionChangedListener<COMP extends Component, ITEM_VALUE> extends ComponentEventListener<MasterSelectionChangedEvent<COMP, ITEM_VALUE>> {
    }

    boolean isApplicable(Class componentClass);

    void connect(COMP masterComponent);

    default void reconnect() {
        disconnect();
        connect(getMasterComponent());
    }

    void disconnect();

    COMP getMasterComponent();

    /**
     * @return the selected items stored in the connector
     */
    Collection<ITEM_VALUE> getSelectedItems();

    /**
     * Sets the connector's internal selection collection to the given items, and
     * also sets the components selection
     *
     * @param items the items to be selected
     */
    void setSelectedItems(Collection<ITEM_VALUE> items);

    default void setSelectedItem(ITEM_VALUE item) {
        setSelectedItems(Collections.singleton(item));
    }

    Registration addSelectionChangedListener(MasterSelectionChangedListener<COMP, ITEM_VALUE> selectionChangedListener);

    void refresh();

    void refresh(ITEM_VALUE item);

    void setEnabled(boolean enabled);

    boolean isEnabled();

    boolean isDirectModifiable();

    void addItem(ITEM_VALUE item);

    default void addItems(@NotNull Collection<ITEM_VALUE> items) {
        items.forEach(this::addItem);
    }

    void removeItem(ITEM_VALUE item);

    default void removeItems(@NotNull Collection<ITEM_VALUE> items) {
        items.forEach(this::removeItem);
    }

    /**
     * lower value means greater priority
     *
     * @return the int number indicatiting the priority
     */
    default int getPriority() {
        return DEFAULT_PRIORITY;
    }
}
