/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.utils.elemental.json;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.JsonValue;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.IntStream;

public final class JsonBuilder {

    private JsonBuilder() {
    }

    public static JsonObjectBuilder jsonObject() {
        return new JsonObjectBuilder();
    }

    public static JsonObjectBuilder jsonObjectFrom(JsonObject base) {
        return new JsonObjectBuilder(base);
    }

    public static JsonArrayBuilder jsonArray() {
        return new JsonArrayBuilder();
    }

    public static JsonArray concatJsonArrays(JsonArray base, JsonArray... jsonArrays) {
        JsonArrayBuilder jsonArrayBuilder = new JsonArrayBuilder(base);
        Arrays.stream(jsonArrays).forEach(jsonArrayBuilder::addAll);
        return jsonArrayBuilder.build();
    }

    public static JsonArray jsonArrayFrom(Collection<? extends JsonValue> collection) {
        return new JsonArrayBuilder().addAll(collection).build();
    }

    public static <T extends JsonValue> Collector<T, ?, JsonArray> jsonArrayCollector() {
        return new Collector<T, JsonArrayBuilder, JsonArray>() {
            @Override
            public Supplier<JsonArrayBuilder> supplier() {
                return JsonArrayBuilder::new;
            }

            @Override
            public BiConsumer<JsonArrayBuilder, T> accumulator() {
                return JsonArrayBuilder::add;
            }

            @Override
            public BinaryOperator<JsonArrayBuilder> combiner() {
                return JsonArrayBuilder::addAll;
            }

            @Override
            public Function<JsonArrayBuilder, JsonArray> finisher() {
                return JsonArrayBuilder::build;
            }

            @Override
            public Set<Characteristics> characteristics() {
                return Collections.emptySet();
            }
        };
    }

    public static final class JsonArrayBuilder {
        private final JsonArray jsonArray;

        private JsonArrayBuilder() {
            jsonArray = Json.createArray();
        }

        private JsonArrayBuilder(JsonArray base) {
            jsonArray = base;
        }

        public JsonArrayBuilder add(JsonValue value) {
            jsonArray.set(jsonArray.length(), value);
            return this;
        }

        public JsonArrayBuilder add(String value) {
            jsonArray.set(jsonArray.length(), value);
            return this;
        }

        public JsonArrayBuilder addAll(Collection<? extends JsonValue> collection) {
            collection.forEach(value -> jsonArray.set(jsonArray.length(), value));
            return this;
        }

        public JsonArrayBuilder addAllStrings(Collection<String> strings) {
            strings.forEach(value -> jsonArray.set(jsonArray.length(), value));
            return this;
        }

        public JsonArrayBuilder addAll(JsonArray otherArray) {
            IntStream.range(0, otherArray.length())
                .mapToObj(otherArray::get)
                .filter(Objects::nonNull)
                .map(JsonValue.class::cast)
                .forEach(value -> jsonArray.set(jsonArray.length(), value));
            return this;
        }

        public JsonArrayBuilder addAll(JsonArrayBuilder otherBuilder) {
            return addAll(otherBuilder.jsonArray);
        }

        public JsonArray build() {
            return jsonArray;
        }
    }

    public static final class JsonObjectBuilder {

        private final JsonObject jsonObject;

        private JsonObjectBuilder() {
            jsonObject = Json.createObject();
        }

        private JsonObjectBuilder(JsonObject base) {
            jsonObject = base;
        }

        public JsonObjectBuilder put(String key, boolean value) {
            jsonObject.put(key, value);
            return this;
        }

        public JsonObjectBuilder put(String key, String value) {
            jsonObject.put(key, value);
            return this;
        }

        public JsonObjectBuilder put(String key, double value) {
            jsonObject.put(key, value);
            return this;
        }

        public JsonObjectBuilder put(String key, JsonValue value) {
            jsonObject.put(key, value);
            return this;
        }

        public JsonObjectBuilder putIf(boolean check, String key, JsonValue value) {
            if (check) {
                jsonObject.put(key, value);
            }
            return this;
        }

        public JsonObject build() {
            return jsonObject;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof JsonObjectBuilder) {
                return jsonObject.jsEquals(((JsonObjectBuilder) obj).jsonObject);
            } else {
                return false;
            }
        }

        @Override
        public String toString() {
            return jsonObject.toString();
        }
    }


}
