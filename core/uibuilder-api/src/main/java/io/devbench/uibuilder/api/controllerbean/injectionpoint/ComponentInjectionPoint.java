/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.controllerbean.injectionpoint;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.dom.Element;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ComponentInjectionPoint {

    @Getter
    private final String id;
    private final Map<Method, Parameter> argumentInjectionPoints;

    @Setter
    private Element element;

    @Getter
    @Setter
    private Set<? extends Class<?>> targetClasses;

    @Setter(AccessLevel.PACKAGE)
    private ComponentInjectionPointComponentStore componentStore;
    private Boolean ambiguous;


    public ComponentInjectionPoint(String id, Set<? extends Class<?>> targetClasses) {
        this(id, (Map<Method, Parameter>) null);
        this.targetClasses = targetClasses;
    }

    public ComponentInjectionPoint(String id, Map<Method, Parameter> argumentInjectionPoints) {
        this.id = id;
        this.targetClasses = null;
        this.argumentInjectionPoints = argumentInjectionPoints;
        this.componentStore = new WeakReferenceComponentInjectionPointComponentStore();
    }

    public void setValue(Component value) {
        final String key = getValueKeyBySession();
        componentStore.put(key, value);
        UI.getCurrent().addDetachListener(e -> componentStore.remove(key));
    }

    /**
     * Decides if the injection point is ambiguous, which means in this case that the "id" specifying
     * the injection point appears for more parameters whose types are different.
     * <p>
     * E.g.:
     * <pre><code>
     *     class ExampleBean {
     *         public void exampleEventHandler(@UIComponent("exampleId") Button button) {
     *             //...
     *         }
     *
     *         public void exampleEventHandler2(@UIComponent("exampleId") TextField textField) {
     *             //...
     *         }
     *     }
     * </code></pre>
     * <p>
     * In this case the injection point representing the "exampleId" injection possibilities is
     * ambiguous because it can represent either the {@code Button} in the {@code exampleEventHandler}
     * or the {@code TextField} in {@code exampleEventHandler2}.
     * <p>
     * A injection point being ambiguous doesn't mean the configuration is wrong, because it is possible
     * that the two component having the same id never shown on the same "page", although using the same
     * "id" for different type of components is highly discouraged.
     *
     * @return True if the injection point contain references to colliding type of method arguments.
     */
    public boolean isAmbiguous() {
        if (ambiguous == null) {
            ambiguous = getMergedTargetClasses().distinct().count() > 1;
        }
        return ambiguous;
    }

    private Stream<? extends Class<?>> getMergedTargetClasses() {
        Set<Class<?>> mergedTargetClasses = new HashSet<>();
        if (argumentInjectionPoints != null) {
            argumentInjectionPoints.values().stream().map(Parameter::getType).forEach(mergedTargetClasses::add);
        }
        if (targetClasses != null) {
            mergedTargetClasses.addAll(targetClasses);
        }
        return mergedTargetClasses.stream();
    }

    public Class<?> getUnambiguousType() {
        if (!isAmbiguous()) {
            return getMergedTargetClasses().findFirst().orElseThrow(() -> new InternalResolverException("No target type found"));
        } else {
            throw new InternalResolverException("This injection point is ambiguous, it's type not accessible this way");
        }
    }

    public Optional<Class<?>> getUnambiguousParameterizedType() {
        if (argumentInjectionPoints != null) {

            List<Class<?>> parameterTypes = argumentInjectionPoints.values().stream()
                .map(Parameter::getParameterizedType)
                .filter(Objects::nonNull)
                .map(ParameterizedType.class::cast)
                .map(ParameterizedType::getActualTypeArguments)
                .filter(types -> types.length >= 1)
                .map(types -> types[0])
                .filter(Class.class::isInstance)
                .map(type -> (Class<?>) type)
                .collect(Collectors.toList());

            if (parameterTypes.stream().distinct().count() == 1) {
                return Optional.ofNullable(parameterTypes.get(0));
            }
        }
        return Optional.empty();
    }

    public boolean isParameterMatching(Method method, Parameter parameter) {
        if (argumentInjectionPoints != null) {
            Parameter injectionRequestingParameter = argumentInjectionPoints.get(method);
            return injectionRequestingParameter != null && injectionRequestingParameter.equals(parameter);
        }
        return false;
    }

    private String getValueKeyBySession() {
        return UI.getCurrent().getSession().getSession().getId() + UI.getCurrent().getUIId();
    }

    public Component getValueForType(Class<? extends Component> componentType) {
        if (isAmbiguous()) {
            if (element.getComponent().isPresent()) {
                Component component = element.getComponent().get();
                if (component.getClass().isAssignableFrom(componentType)) {
                    return component;
                } else {
                    throw new InternalResolverException(String.format(
                        "Component (id: %s) cannot be injected to parameter (with requested id: %s), because the element already has a component " +
                            "registered to it with different type. Component type: %s, inject point type: %s",
                        element.getComponent().flatMap(Component::getId).orElse("<UNKNOWN>"),
                        id, component.getClass().getSimpleName(), componentType.getSimpleName()));
                }
            } else {
                return Component.from(element, (Class<? extends Component>) componentType);
            }
        } else {
            String valueKeyBySession = getValueKeyBySession();
            return componentStore.get(valueKeyBySession);
        }
    }

    @SuppressWarnings("unchecked")
    public Component getValueForParameter(Parameter parameter) {
        return getValueForType((Class<? extends Component>) parameter.getType());
    }

    public boolean isAnyComponentSubclassOf(Class<?> superClassCandidate) {
        return getMergedTargetClasses().anyMatch(superClassCandidate::isAssignableFrom);
    }
}
