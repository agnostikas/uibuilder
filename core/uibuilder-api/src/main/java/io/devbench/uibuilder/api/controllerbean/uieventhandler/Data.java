/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.controllerbean.uieventhandler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>The <code>@Data</code> parameter annotation can be used in an <code>@UIEventHandler</code> method
 * to get the value of the following data type attribute on the source component tag: <code>data-param="param-value"</code>.
 * The value of the annotation has to be the parameter name, which is the attribute name part after the <code>"data-"</code> prefix.
 * </p>
 *
 * <h3>Example:</h3>
 * <pre>
 * &lt;vaadin-button on-click="testCb::onClick" data-test="test-value"&gt;&lt;/vaadin-button&gt;
 *
 * &#64;UIEventHandler("onClick")
 * public void onClick(@Data("test") String test) {
 *     assertEquals("test-value", test);
 * }
 * </pre>
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Data {

    String value();

}
