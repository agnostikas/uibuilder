/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.singleton;

import io.devbench.uibuilder.api.exceptions.SingletonNotFoundException;
import io.devbench.uibuilder.api.exceptions.SingletonProviderNotFoundException;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.stream.StreamSupport;


public final class SingletonManager {

    private static SingletonManagerProvider MANAGER_PROVIDER;

    static {
        resetManager();
    }

    private static void resetManager() {
        ServiceLoader<SingletonManagerProvider> singletonManagerProviderServiceLoader = ServiceLoader.load(SingletonManagerProvider.class);
        MANAGER_PROVIDER = StreamSupport
            .stream(singletonManagerProviderServiceLoader.spliterator(), false)
            .max(Comparator.comparing(SingletonManagerProvider::priority))
            .orElseThrow(SingletonProviderNotFoundException::new);
    }

    public static <T> T getInstanceOf(@NotNull Class<T> instanceClass) {
        return getOptionalInstanceOf(instanceClass).orElseThrow(() -> new SingletonNotFoundException(String.format(
            "Singleton of class: %s cannot be found in current context",
            instanceClass.getSimpleName()
        )));
    }

    private static <T> Optional<T> getOptionalInstanceOf(@NotNull Class<T> instanceClass) {
        return MANAGER_PROVIDER.getSingletonProviders().stream()
            .max(Comparator.comparing(SingletonProvider::priority))
            .flatMap(singletonProvider -> singletonProvider.getInstance(instanceClass));
    }

    @SuppressWarnings("unchecked")
    public static <T> void registerSingletonToContext(@NotNull Object context, @NotNull T instance) {
        Optional<ContextUnawareSingletonHolder> optionalUnawareSingletonHolder = MANAGER_PROVIDER.getContextUnawareSingletonHolders().stream()
            .max(Comparator.comparing(ContextUnawareSingletonHolder::priority));
        optionalUnawareSingletonHolder.ifPresent(contextUnawareSingletonHolder ->
            contextUnawareSingletonHolder.storeInstance(context, (Class<T>) instance.getClass(), instance));
    }

    @SuppressWarnings("unchecked")
    public static <T> void registerSingleton(@NotNull T instance) {
        registerSingleton(((Class<T>) instance.getClass()), instance);
    }

    public static <T> void registerSingleton(Class<T> type, @NotNull T instance) {
        Optional<ContextAwareSingletonHolder> optionalContextAwareSingletonHolder = MANAGER_PROVIDER.getContextAwareSingletonHolders().stream()
            .max(Comparator.comparing(ContextAwareSingletonHolder::priority));

        optionalContextAwareSingletonHolder.ifPresent(contextAwareSingletonHolder -> contextAwareSingletonHolder.storeInstance(type, instance));
    }
}
