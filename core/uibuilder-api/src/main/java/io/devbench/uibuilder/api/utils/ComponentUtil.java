/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class ComponentUtil {

    private ComponentUtil() {
    }

    public static <C extends Component> Optional<String> getComponentTagName(@NotNull Class<C> componentClass) {
        Tag tagAnnotation = null;
        Class<?> clz = componentClass;
        while (clz != null && (tagAnnotation = clz.getAnnotation(Tag.class)) == null) {
            clz = clz.getSuperclass();
        }
        return tagAnnotation != null ? Optional.of(tagAnnotation.value()) : Optional.empty();
    }

    public static <C extends Component> boolean isComponentTagEquals(@NotNull Class<C> componentClass, @NotNull String tagName) {
        return getComponentTagName(componentClass)
            .map(tagName::equals)
            .orElse(false);
    }

    public static String attrToProp(String attributeName) {
        if (attributeName == null) {
            return null;
        }
        String[] parts = attributeName.split("-");
        return Arrays.stream(parts)
            .map(String::toLowerCase)
            .map(StringUtils::capitalize)
            .collect(Collectors.joining());
    }

    public static String propToAttr(String propertyName) {
        String[] parts = StringUtils.splitByCharacterTypeCamelCase(propertyName);
        if (parts == null) {
            return null;
        } else {
            return Arrays.stream(parts)
                .map(String::toLowerCase)
                .collect(Collectors.joining("-"));
        }
    }

    public static String attrChangedEventName(String attributeName) {
        return attributeName + "-changed";
    }

    public static String propChangedEventName(String propertyName) {
        return attrChangedEventName(propToAttr(propertyName));
    }

}
