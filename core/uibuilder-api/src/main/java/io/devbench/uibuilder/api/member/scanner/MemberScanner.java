/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.member.scanner;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class MemberScanner {

    private final LoadingCache<Class<? extends Annotation>, Set<Class<?>>> findByAnnotationFunctionInterfacesNotAllowed;
    private final LoadingCache<Class<? extends Annotation>, Set<Class<?>>> findByAnnotationFunctionInterfacesAllowed;
    private final LoadingCache<Class<?>, Set<? extends Class<?>>> findBySuperTypeFunctionInterfaceNotAllowed;
    private final LoadingCache<Class<?>, Set<? extends Class<?>>> findBySuperTypeFunctionInterfaceAllowed;
    private final LoadingCache<Class<?>, Set<?>> findInstancesBySuperTypeFunction;

    private MemberScanner(FindByAnnotationFunction findByAnnotationFunction, FindClassesBySuperTypeFunction findBySuperTypeFunction) {
        findByAnnotationFunctionInterfacesNotAllowed = CacheBuilder.newBuilder()
            .build(CacheLoader.from((annotationType) -> findByAnnotationFunction.apply(annotationType, false)));
        findByAnnotationFunctionInterfacesAllowed = CacheBuilder.newBuilder()
            .build(CacheLoader.from((annotationType) -> findByAnnotationFunction.apply(annotationType, true)));
        findBySuperTypeFunctionInterfaceNotAllowed = CacheBuilder.newBuilder()
            .build(CacheLoader.from((superClass) -> findBySuperTypeFunction.apply(superClass, false)));
        findBySuperTypeFunctionInterfaceAllowed = CacheBuilder.newBuilder()
            .build(CacheLoader.from((superClass) -> findBySuperTypeFunction.apply(superClass, true)));
        findInstancesBySuperTypeFunction = CacheBuilder.newBuilder()
            .build(CacheLoader.from((superClass) -> findBySuperTypeFunction.apply(superClass, false)
                .stream()
                .map(this::tryToInstantiate)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet())));
    }

    private <T> Optional<T> tryToInstantiate(Class<T> clz) {
        try {
            Constructor<T> constructor = clz.getDeclaredConstructor();
            return Optional.of(constructor.newInstance());
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
            log.error("Could not instantiate " + clz.getName(), e);
            return Optional.empty();
        }
    }

    public Set<Class<?>> findClassesByAnnotation(Class<? extends Annotation> annotationType) {
        return findByAnnotationFunctionInterfacesNotAllowed.getUnchecked(annotationType);
    }

    public Set<Class<?>> findClassesOrInterfacesByAnnotation(Class<? extends Annotation> annotationType) {
        return findByAnnotationFunctionInterfacesAllowed.getUnchecked(annotationType);
    }

    @SuppressWarnings("unchecked")
    public <SUPER_TYPE> Set<Class<? extends SUPER_TYPE>> findClassesBySuperType(Class<SUPER_TYPE> superClass) {
        return (Set<Class<? extends SUPER_TYPE>>) findBySuperTypeFunctionInterfaceNotAllowed.getUnchecked(superClass);
    }

    @SuppressWarnings("unchecked")
    public <SUPER_TYPE> Set<Class<? extends SUPER_TYPE>> findClassesOrInterfacesBySuperType(Class<SUPER_TYPE> superClass) {
        return (Set<Class<? extends SUPER_TYPE>>) findBySuperTypeFunctionInterfaceAllowed.getUnchecked(superClass);
    }

    @SuppressWarnings("unchecked")
    public <T> Set<T> findInstancesBySuperType(Class<T> superClass) {
        return (Set<T>) findInstancesBySuperTypeFunction.getUnchecked(superClass);
    }

    public static MemberScanner getInstance() {
        return SingletonManager.getInstanceOf(MemberScanner.class);
    }

    public static MemberScanner registerToActiveContext(FindByAnnotationFunction findByAnnotationFunction,
                                                        FindClassesBySuperTypeFunction findBySuperTypeFunction) {

        MemberScanner memberScanner = new MemberScanner(findByAnnotationFunction, findBySuperTypeFunction);
        SingletonManager.registerSingleton(memberScanner);
        return memberScanner;
    }

    public static MemberScanner registerToContext(Object context, FindByAnnotationFunction findByAnnotationFunction,
                                                  FindClassesBySuperTypeFunction findBySuperTypeFunction) {

        MemberScanner memberScanner = new MemberScanner(findByAnnotationFunction, findBySuperTypeFunction);
        SingletonManager.registerSingletonToContext(context, memberScanner);
        return memberScanner;
    }

}
