/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.controllerbean.injectionpoint;

import com.vaadin.flow.component.Component;
import lombok.extern.slf4j.Slf4j;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class WeakReferenceComponentInjectionPointComponentStore implements ComponentInjectionPointComponentStore {

    private Map<String, WeakReference<Component>> valueMap = new HashMap<>();

    @Override
    public void put(String key, Component component) {
        valueMap.put(key, new WeakReference<>(component));
    }

    @Override
    public void remove(String key) {
        valueMap.remove(key);
    }

    @Override
    public Component get(String key) {
        WeakReference<Component> componentReference = valueMap.get(key);
        if (componentReference == null) {
            log.warn("Could not found component by key: {}", key);
            return null;
        }
        return componentReference.get();
    }

}
