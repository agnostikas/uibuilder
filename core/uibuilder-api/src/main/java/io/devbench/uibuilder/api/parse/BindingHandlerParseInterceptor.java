/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.parse;

import com.vaadin.flow.component.Component;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Element;


public interface BindingHandlerParseInterceptor<BINDING_CONTEXT extends BindingContext> extends ParseInterceptor {

    default boolean appliesToSubElements() {
        return false;
    }

    BINDING_CONTEXT createBindingContext(Element element);

    boolean handleBinding(@NotNull String binding, @NotNull BINDING_CONTEXT bindingContext);

    void commitBindingParse(BINDING_CONTEXT bindingContext, Component component);

}
