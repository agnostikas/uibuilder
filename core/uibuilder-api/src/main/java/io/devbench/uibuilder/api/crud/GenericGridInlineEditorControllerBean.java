/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.crud;

import elemental.json.JsonObject;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import org.jetbrains.annotations.NotNull;

public interface GenericGridInlineEditorControllerBean<T> {

    @UIEventHandler("inlineItemSave")
    void onInlineItemSave(@CallOnNonNull @Item T item, @CallOnNonNull @Value("item") JsonObject jsonItem);

    @UIEventHandler("inlineItemValueChange")
    void onInlineItemValueChange(@CallOnNonNull @Item T item, @CallOnNonNull @Value("item") JsonObject jsonItem);

    @UIEventHandler("inlineItemCancel")
    default void onInlineItemCancel(@CallOnNonNull @Value("item") JsonObject jsonItem) {
        setEditMode(jsonItem, false);
    }

    @UIEventHandler("inlineItemEdit")
    default void onInlineItemEdit(@CallOnNonNull @Value("item") JsonObject jsonItem) {
        setEditMode(jsonItem, true);
    }

    void setEditMode(@NotNull JsonObject jsonItem, boolean editMode);

}
