/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.api.parse.backendtemplate;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Slf4j
public class BackendTemplateManager implements Serializable {

    private final Set<TemplateParser> parsers;

    private BackendTemplateManager() {
        parsers = new HashSet<>();
        initialize();
    }

    public static BackendTemplateManager getInstance() {
        return SingletonManager.getInstanceOf(BackendTemplateManager.class);
    }

    public static BackendTemplateManager registerToContext(Object context) {
        BackendTemplateManager instance = new BackendTemplateManager();
        SingletonManager.registerSingletonToContext(context, instance);
        return instance;
    }

    public static BackendTemplateManager registerToActiveContext() {
        BackendTemplateManager instance = new BackendTemplateManager();
        SingletonManager.registerSingleton(instance);
        return instance;
    }

    private void initialize() {
        MemberScanner memberScanner = MemberScanner.getInstance();
        memberScanner.findClassesBySuperType(TemplateParser.class).stream()
            .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers())
                && !Modifier.isInterface(clazz.getModifiers()))
            .map(this::tryToInstantiateTemplateParser)
            .filter(Objects::nonNull)
            .forEach(this::registerTemplateParser);
    }

    private TemplateParser tryToInstantiateTemplateParser(Class<? extends TemplateParser> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("Cannot instantiate TemplateParser with type: " + clazz.getName(), e);
            return null;
        }
    }

    public void registerTemplateParser(TemplateParser parser) {
        parsers.add(parser);
    }

    public Set<TemplateParser> getRegisteredTemplateParsers() {
        return Collections.unmodifiableSet(parsers);
    }
}
