const $_styleContainer = document.createElement('template');
$_styleContainer.innerHTML = `
    <custom-style>
        <style>
            html {
                --uibuilder-lumo-modal-content-bg-color: #F1F1F1;
                --uibuilder-lumo-modal-color: var(--lumo-shade-40pct);

                --uibuilder-lumo-border: 1px solid #E0E0E0;

                --uibuilder-lumo-form-bg-color: #FAFAFA;
                --uibuilder-lumo-form-legend-color: #505050;
                --uibuilder-lumo-form-control-separator-color: var(--uibuilder-lumo-modal-color);

                --uibuilder-lumo-menu-bg-color: #F0F0F0;
                --uibuilder-lumo-menu-item-bg-color: #303030;
                --uibuilder-lumo-menu-selected-color: #101010;
                --uibuilder-lumo-menu-selected-bg-color: #D0E0F0;
                --uibuilder-lumo-menu-hover-bg-color: #E0E0F0;
            }

            body {
                width: 100%;
            }
        </style>
    </custom-style>
`;

document.head.appendChild($_styleContainer.content);
