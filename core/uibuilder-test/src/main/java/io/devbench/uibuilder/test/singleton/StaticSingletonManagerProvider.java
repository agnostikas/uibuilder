/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.singleton;

import io.devbench.uibuilder.api.singleton.ContextAwareSingletonHolder;
import io.devbench.uibuilder.api.singleton.ContextUnawareSingletonHolder;
import io.devbench.uibuilder.api.singleton.SingletonManagerProvider;
import io.devbench.uibuilder.api.singleton.SingletonProvider;

import java.util.Collection;
import java.util.Collections;

public class StaticSingletonManagerProvider implements SingletonManagerProvider {

    private ContextAwareSingletonHolder contextAwareSingletonHolder;
    private SingletonProvider singletonProvider;

    public StaticSingletonManagerProvider() {
        contextAwareSingletonHolder = new ContextAwareStaticSingletonHolder();
        singletonProvider = new StaticSingletonProvider();
    }

    @Override
    public Collection<SingletonProvider> getSingletonProviders() {
        return Collections.singleton(singletonProvider);
    }

    @Override
    public Collection<ContextAwareSingletonHolder> getContextAwareSingletonHolders() {
        return Collections.singleton(contextAwareSingletonHolder);
    }

    @Override
    public Collection<ContextUnawareSingletonHolder> getContextUnawareSingletonHolders() {
        return Collections.emptyList();
    }

    @Override
    public int priority() {
        return StaticSingletonHolderForTests.useStaticSingletons ? Integer.MAX_VALUE : Integer.MIN_VALUE;
    }

}
