/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.extensions;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.test.annotations.DisableMockedSession;
import io.devbench.uibuilder.test.annotations.DisableMockedUI;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BaseUIBuilderTestExtension implements BeforeEachCallback, AfterEachCallback {

    private UI vaadinUi;
    private VaadinSession vaadinSession;


    @Override
    public void afterEach(ExtensionContext context) {
        verifyVaadinUI();
        verifyVaadinSession();
    }

    private void verifyVaadinSession() {
        assertSame(vaadinSession, VaadinSession.getCurrent());
        VaadinSession.setCurrent(null);
    }

    private void verifyVaadinUI() {
        assertSame(vaadinUi, UI.getCurrent());
        UI.setCurrent(null);
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        mockVaadinSession(context);
        mockVaadinUI(context);
    }

    private void mockVaadinSession(ExtensionContext context) throws NoSuchFieldException, IllegalAccessException {
        Optional<VaadinSession> vaadinSession = getFieldOfType(VaadinSession.class, context);
        Optional<VaadinService> vaadinService = getFieldOfType(VaadinService.class, context);
        if (testMethodHasAnnotation(DisableMockedSession.class, context)) {
            this.vaadinSession = null;
        } else {
            this.vaadinSession = vaadinSession.orElseGet(() -> new VaadinSession(vaadinService.orElse(null)));
            Field lockField = VaadinSession.class.getDeclaredField("lock");
            lockField.setAccessible(true);
            ReentrantLock lock = new ReentrantLock();
            lockField.set(this.vaadinSession, lock);
            lock.lock();
        }
        VaadinSession.setCurrent(this.vaadinSession);
    }

    private <T> Optional<T> getFieldOfType(Class<T> clazz, ExtensionContext context) throws IllegalAccessException {
        Class<?> testClass = context.getTestClass().orElseThrow(NoSuchElementException::new);
        Object testInstance = context.getTestInstance().orElseThrow(NoSuchElementException::new);
        List<Field> fieldList = Stream.of(testClass.getDeclaredFields())
            .filter(field -> clazz.isAssignableFrom(field.getType()))
            .peek(field -> field.setAccessible(true))
            .collect(Collectors.toList());

        if (fieldList.size() == 0) {
            return Optional.empty();
        } else if (fieldList.size() == 1) {
            return Optional.of((T) fieldList.get(0).get(testInstance));
        } else {
            throw new IllegalStateException("");
        }
    }

    private void mockVaadinUI(ExtensionContext context) throws IllegalAccessException {
        if (testMethodHasAnnotation(DisableMockedUI.class, context)) {
            this.vaadinUi = null;
        } else {
            Optional<UI> ui = getFieldOfType(UI.class, context);
            if (ui.isPresent()) {
                this.vaadinUi = ui.get();
            } else {
                this.vaadinUi = spy(new UI());
                when(this.vaadinUi.getSession()).thenReturn(vaadinSession);
            }
        }
        UI.setCurrent(vaadinUi);
    }

    private boolean testMethodHasAnnotation(Class<? extends Annotation> annotationClass, ExtensionContext context) {
        return context
            .getTestMethod()
            .map(method -> method.getAnnotation(annotationClass))
            .isPresent();
    }
}
