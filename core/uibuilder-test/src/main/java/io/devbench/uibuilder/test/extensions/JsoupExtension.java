/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.extensions;

import io.devbench.uibuilder.test.annotations.LoadElement;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.extension.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

public class JsoupExtension implements TestInstancePostProcessor, ParameterResolver {

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.isAnnotated(LoadElement.class)
            && Element.class.isAssignableFrom(parameterContext.getParameter().getType());
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        Optional<LoadElement> loadElementOptional = parameterContext.findAnnotation(LoadElement.class);
        if (loadElementOptional.isPresent()) {
            LoadElement loadElement = loadElementOptional.get();
            return getElementByLoadElementAnnotation(loadElement);
        }
        return null;
    }

    private Element getElementByLoadElementAnnotation(LoadElement loadElement) {
        InputStream elementInputStream = JsoupExtension.class.getResourceAsStream(loadElement.value());
        Document document = null;
        try {
            document = Jsoup.parse(elementInputStream, StandardCharsets.UTF_8.name(), "/");
        } catch (IOException e) {
            throw new ParameterResolutionException("Cannot load html:" + loadElement.value(), e);
        }

        Elements elements;

        if (!loadElement.elemantName().equals(LoadElement.DEFAULT)) {
            elements = document.getElementsByTag(loadElement.elemantName());
        } else if (!loadElement.id().equals(LoadElement.DEFAULT)) {
            return document.getElementById(loadElement.id());
        } else {
            elements = document.getAllElements();
        }

        if (elements.size() > 0) {
            return elements.first();
        } else {
            throw new ParameterResolutionException("Cannot find element");
        }
    }

    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext extensionContext) throws Exception {
        for (Field field : testInstance.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(LoadElement.class) && Element.class.isAssignableFrom(field.getType())) {
                LoadElement loadElement = field.getAnnotation(LoadElement.class);
                Element element = getElementByLoadElementAnnotation(loadElement);

                boolean wasAccesible = field.isAccessible();
                field.setAccessible(true);
                field.set(testInstance, element);
                field.setAccessible(wasAccesible);
            }
        }
    }
}
