/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.BiFunction;

import static org.junit.jupiter.api.Assertions.*;

public final class CollectionAssert {

    private CollectionAssert() {
    }

    public static <T> void assertContainsSame(Collection<T> expected, Collection<T> actual) {
        assertEquals(new HashSet<>(expected), new HashSet<>(actual));
        assertEquals(expected.size(), actual.size());
    }

    public static <T> void assertContainsSame(Collection<T> expected, Collection<T> actual, BiFunction<T, T, Boolean> equalsFunction) {
        for (T exp : expected) {
            boolean match = actual.stream().anyMatch(it -> equalsFunction.apply(it, exp));
            if (!match) {
                throw new AssertionError(exp + " not found in actual. Actual is: " + actual);
            }
        }
        for (T act : actual) {
            boolean match = expected.stream().anyMatch(it -> equalsFunction.apply(it, act));
            if (!match) {
                throw new AssertionError(act + " not found in excepted. Expected is: " + expected);
            }
        }
        assertEquals(expected.size(), actual.size());
    }
}
