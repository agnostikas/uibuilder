/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.baselayout;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.WildcardParameter;
import com.vaadin.flow.theme.Theme;
import io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedTheme;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.FlowTarget;
import io.devbench.uibuilder.core.page.Page;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;


@Slf4j
@Route("")
@Theme(ConfigurationBasedTheme.class)
public class BaseLayout extends Div implements HasUrlParameter<String>, FlowTarget {

    private static final Config CONFIG = ConfigFactory.load();

    private Page page;
    private FlowManager flowManager;

    public BaseLayout() {
        page = new Page();
        flowManager = new FlowManager(CONFIG.getString("flowDefinition"));
        registerSelf();
        add(page);
        useBrowserLocale();
        setId("root-context");
        setWidth("100%");
        setHeight("100vh");
    }

    @Override
    public void setParameter(BeforeEvent event, @WildcardParameter String relativeUrl) {
        try {
            Map<String, List<String>> parameters = event
                .getLocation()
                .getQueryParameters()
                .getParameters();
            flowManager.handleUrlRequest(relativeUrl, parameters);
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage(), e);
            remove(page);
            setText("Internal error");
        } catch (IllegalStateException e) {
            log.error(e.getMessage(), e);
            remove(page);
            setText("Couldn't load any content");
        }
    }

    @Override
    public String getComponentId() {
        return "UI";
    }

    @Override
    public void loadContent(String htmlPath) {
        page.loadPage(htmlPath);
    }

    @Override
    public void unloadContent() {
        page.unloadContent();
    }

    private void useBrowserLocale() {
        UI ui = UI.getCurrent();
        ui.setLocale(
            ui.getInternals()
                .getSession()
                .getBrowser()
                .getLocale()
        );
    }
}
