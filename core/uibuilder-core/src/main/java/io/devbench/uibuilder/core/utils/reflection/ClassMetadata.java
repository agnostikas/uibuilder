/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.devbench.uibuilder.core.utils.reflection.MetadataReflectionUtils.*;


@Slf4j
public class ClassMetadata<T> {

    private static final Config config = ConfigFactory.load();
    private static final LoadingCache<Class<?>, ClassMetadata<?>> classMetaDataCache = CacheBuilder.newBuilder()
        .maximumSize(config.getInt("cache.class-meta-data.max-size"))
        .expireAfterWrite(config.getInt("cache.class-meta-data.expiration"), TimeUnit.SECONDS)
        .build(
            new CacheLoader<Class<?>, ClassMetadata<?>>() {
                public ClassMetadata<?> load(Class<?> clazz) {
                    return new ClassMetadata<>(clazz);
                }
            });

    @Getter
    private final Class<T> targetClass;

    @Getter
    private T instance;

    @Getter(value = AccessLevel.PROTECTED)
    private Map<String, PropertyMetadata<T>> propertyCache = new HashMap<>();
    private Set<MethodMetadata<T>> methodCache = new HashSet<>();

    @NotNull
    @SuppressWarnings("unchecked")
    public static <T> ClassMetadata<T> ofClass(Class<T> clazz) {
        return (ClassMetadata<T>) classMetaDataCache.getUnchecked(clazz);
    }

    @NotNull
    @SuppressWarnings("unchecked")
    public static <T> ClassMetadata<T> ofParametrizedType(ParameterizedType parameterizedType) {
        Class<T> clazz = (Class<T>) parameterizedType.getRawType();
        return (ClassMetadata<T>) classMetaDataCache.getUnchecked(clazz);
    }

    private ClassMetadata(
        Class<T> targetClass,
        T instance,
        Map<String, PropertyMetadata<T>> propertyCache,
        Set<MethodMetadata<T>> methodCache
    ) {
        this.targetClass = targetClass;
        this.instance = instance;
        this.propertyCache = propertyCache
            .entrySet()
            .stream()
            .map(it -> Pair.of(it.getKey(), it.getValue().withInstance(instance)))
            .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        this.methodCache = methodCache
            .stream()
            .map(it -> it.withInstance(instance).withContainerClassMetadata(this))
            .collect(Collectors.toSet());
    }

    public ClassMetadata(Class<T> targetClass) {
        this(targetClass, false);
    }

    public ClassMetadata(Class<T> targetClass, boolean inheritanceEnabled) {
        this.targetClass = targetClass;
        buildPropertyCache(inheritanceEnabled);
        buildMethodCache();
    }

    @SuppressWarnings("unchecked")
    public static <T> ClassMetadata<T> ofValue(T value) {
        Optional<ClassMetadata<T>> classMetadata = Optional.empty();
        if (value instanceof Proxy) {
            Optional<Class<T>> originalClass = ofClass((Class<T>) value.getClass()).withInstance(value).getPropertyValue("decoratedClass");
            classMetadata = originalClass.map(ClassMetadata::ofClass);
        }
        return classMetadata.orElseGet(() -> (ClassMetadata<T>) ofClass(value.getClass())).withInstance(value);
    }

    private void buildMethodCache() {
        methodCache = getAllClassMember(targetClass, Class::getDeclaredMethods).stream()
            .map(method -> new MethodMetadata<>(this, targetClass, instance, method))
            .collect(Collectors.toSet());
        methodCache = Collections.unmodifiableSet(methodCache);
    }

    @SuppressWarnings("unchecked")
    private List<Class<T>> findAllTargetClasses(Class<T> clz) {
        List<Class<T>> classes = new ArrayList<>();
        classes.add(clz);
        MemberScanner.getInstance().findClassesBySuperType(clz).stream()
            .map(c -> (Class<T>) c)
            .forEach(classes::add);
        return classes;
    }

    private void buildPropertyCache(Class<T> clz, boolean inheritanceEnabled) {
        getAllClassMember(clz, Class::getDeclaredFields).forEach(field ->
            propertyCache.put(field.getName(), new PropertyMetadata<T>(instance, clz, field, inheritanceEnabled)));
        getAllClassMember(clz, Class::getDeclaredMethods).stream()
            .filter(method -> method.getName().startsWith("is") && method.getReturnType().equals(boolean.class) || method.getName().startsWith("get"))
            .forEach(method -> {
                PropertyMetadata<T> property = new PropertyMetadata<>(instance, clz, method, inheritanceEnabled);
                propertyCache.put(property.getName(), property);
            });
    }

    private void buildPropertyCache(boolean inheritanceEnabled) {
        if (inheritanceEnabled) {
            findAllTargetClasses(targetClass).forEach(clz -> buildPropertyCache(clz, true));
        } else {
            buildPropertyCache(targetClass, false);
        }
        propertyCache = Collections.unmodifiableMap(propertyCache);
    }

    public <VALUE> Optional<VALUE> getPropertyValue(String propertyPath) {
        return property(propertyPath)
            .map((Function<? super PropertyMetadata<?>, ? extends VALUE>) PropertyMetadata::getValue);
    }

    public <VALUE> void setPropertyValue(String propertyPath, VALUE value) {
        property(propertyPath)
            .ifPresent(propertyMetadata -> propertyMetadata.setValue(value));
    }

    public Optional<PropertyMetadata<?>> property(String propertyPath) {
        if (StringUtils.isNotBlank(propertyPath) && propertyPath.matches("\\w+(\\.\\w+)+")) {
            return Optional.ofNullable(getPropertyByPropertyPath(propertyPath));
        }
        return Optional.ofNullable(propertyCache.get(propertyPath));
    }

    private PropertyMetadata<?> getPropertyByPropertyPath(String path) {
        PropertyMetadata<?> property = null;
        ClassMetadata<?> classMetadata = this;

        for (String pathElement : path.split("\\.")) {
            if (classMetadata != null) {
                property = classMetadata.propertyCache.get(pathElement);
                if (property == null) {
                    property = getPropertyByInstance(classMetadata, pathElement);
                }
                if (property != null) {
                    classMetadata = property.typeMeta();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
        return property;
    }

    @SuppressWarnings("unchecked")
    private PropertyMetadata<?> getPropertyByInstance(ClassMetadata<?> classMetadata, String property) {
        Object instance = classMetadata.getInstance();
        if (instance != null) {
            Class<?> instanceClass = instance.getClass();
            try {
                Field field = instanceClass.getDeclaredField(property);
                return new PropertyMetadata(instance, classMetadata.getTargetClass(), field);
            } catch (NoSuchFieldException e) {
                log.debug("Could not found instance property (" + property + ") of type " + instanceClass.getName(), e);
                return null;
            }
        }
        return null;
    }

    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
        return targetClass.getAnnotation(annotationType);
    }

    public Collection<PropertyMetadata<T>> getProperties() {
        return propertyCache.values();
    }

    public Set<MethodMetadata<T>> getMethods() {
        return this.methodCache;
    }

    public ClassMetadata<T> withInstance(T instance) {
        return this.instance == instance ? this : new ClassMetadata<>(this.targetClass, instance, this.propertyCache, this.methodCache);
    }
}
