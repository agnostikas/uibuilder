/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.xml;

import io.devbench.uibuilder.core.flow.parsed.ParsedFlowElement;
import io.devbench.uibuilder.core.flow.parsed.ParsedUrlSegment;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "flow")
public class FlowDefinition extends FlowElement implements HasSubFlow {

    public static final String UI = "UI";

    @XmlAttribute
    private String id;

    @XmlAttribute
    private String route;

    @XmlAttribute
    private String html;

    @XmlElementRef
    private List<FlowElement> subFlows = new ArrayList<>();

    public String getRoute() {
        final String route = this.route;
        if (route.startsWith("/")) {
            this.route = route.substring(1);
        }
        return this.route;
    }

    @Override
    public ParsedFlowElement convertToParsedElement() {
        if (html == null) {
            return new ParsedFlowElement(id, Optional::empty);
        } else {
            return new ParsedUrlSegment(id, Optional::empty, getTargetWithDefaultValue(), html);
        }
    }

    private String getTargetWithDefaultValue() {
        String target = getTarget();
        if (StringUtils.isBlank(target)) {
            return UI;
        } else {
            return target;
        }
    }

}
