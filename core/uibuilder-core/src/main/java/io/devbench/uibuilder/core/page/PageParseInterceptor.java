/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import org.jsoup.nodes.Element;


public class PageParseInterceptor implements ParseInterceptor {

    @Override
    public boolean isInstantiator(Element element) {
        return true;
    }

    @Override
    public Component instantiateComponent() {
        return new Page();
    }

    @Override
    public boolean isApplicable(Element element) {
        return Page.TAG_NAME.equals(element.tagName());
    }

    @Override
    public void intercept(Component component, Element element) {
        if (element.hasAttr("href")) {
            String htmlPath = element.attr("href");
            ((Page) component).loadPage(htmlPath);
            element.removeAttr("href");
        }
    }
}
