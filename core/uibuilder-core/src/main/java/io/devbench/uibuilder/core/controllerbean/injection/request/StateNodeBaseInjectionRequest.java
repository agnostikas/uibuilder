/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.request;

import com.vaadin.flow.dom.DomEvent;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import org.jetbrains.annotations.Nullable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import static io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils.*;

public final class StateNodeBaseInjectionRequest extends AbstractInjectionRequest {

    private final StateNodeManager stateNodeManager;
    private final DomEvent event;

    public StateNodeBaseInjectionRequest(StateNodeManager stateNodeManager, DomEvent event, Method method) {
        super(method);
        this.stateNodeManager = stateNodeManager;
        this.event = event;
    }

    @Override
    @Nullable
    public Object getValueByParameter(Parameter parameter) {
        return findClientParameterName(parameter)
            .map(parameterName -> getClientParameterValue(parameterName, stateNodeManager, event, parameter.getType()))
            .orElseGet(() -> super.getValueByParameter(parameter));
    }
}
