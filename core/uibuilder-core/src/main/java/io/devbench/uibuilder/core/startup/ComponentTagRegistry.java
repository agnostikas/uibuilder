/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.startup;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ComponentTagRegistry {

    private Map<String, Class<Component>> componentTagMap;

    private ComponentTagRegistry() {
        initialize();
    }

    @SuppressWarnings("unchecked")
    private void initialize() {
        componentTagMap = Collections.unmodifiableMap(
            MemberScanner.getInstance().findClassesByAnnotation(Tag.class).stream()
                .filter(Component.class::isAssignableFrom)
                .filter(clz -> !"div".equals(getTagNameByClass(clz)))
                .collect(Collectors.toMap(
                    this::getTagNameByClass,
                    clz -> (Class<Component>) clz,
                    (clz1, clz2) -> clz1)));
    }

    private String getTagNameByClass(Class clz) {
        return ((Tag) clz.getAnnotation(Tag.class)).value().toLowerCase();
    }

    public Optional<Class<Component>> getComponentClassByTag(@NotNull String tag) {
        return Optional.ofNullable(componentTagMap.get(tag.trim().toLowerCase()));
    }

    public static ComponentTagRegistry getInstance() {
        return SingletonManager.getInstanceOf(ComponentTagRegistry.class);
    }

    public static void registerToActiveContext() {
        ComponentTagRegistry instance = new ComponentTagRegistry();
        SingletonManager.registerSingleton(instance);
    }

}
