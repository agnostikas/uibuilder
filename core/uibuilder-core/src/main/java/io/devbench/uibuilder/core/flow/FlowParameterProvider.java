/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow;

import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.core.session.context.UIContextStored;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiPredicate;

@UIContextStored
public class FlowParameterProvider {

    private final Map<String, String> urlParameters = new ConcurrentHashMap<>();
    private final Map<String, List<String>> queryParameters = new ConcurrentHashMap<>();

    public static FlowParameterProvider getInstance() {
        if (UIContext.getContext().get(FlowParameterProvider.class) == null) {
            synchronized (FlowParameterProvider.class) {
                if (UIContext.getContext().get(FlowParameterProvider.class) == null) {
                    UIContext.getContext().addToActiveUIContextAs(FlowParameterProvider.class, new FlowParameterProvider());
                }
            }
        }

        return UIContext.getContext().get(FlowParameterProvider.class);
    }

    /**
     * @deprecated use {@link FlowParameterProvider#setUrlParameter(String, String)} instead of this
     */
    @Deprecated
    public void setParameter(String name, String value) {
        urlParameters.put(name, value);
    }

    public void setUrlParameter(String name, String value) {
        urlParameters.put(name, value);
    }

    /**
     * @deprecated use {@link FlowParameterProvider#getUrlParameter(String)} instead of this
     */
    @Deprecated
    public Optional<String> getParameter(String name) {
        return Optional.ofNullable(urlParameters.get(name));
    }

    public Optional<String> getUrlParameter(String name) {
        return Optional.ofNullable(urlParameters.get(name));
    }

    /**
     * Clears all parameters (url parameters and query parameters) from the context.
     */
    void clear() {
        urlParameters.clear();
        queryParameters.clear();
    }

    Map<String, String> getAllUrlParameters() {
        return new HashMap<>(urlParameters);
    }

    void removeParameters(BiPredicate<String, String> parameterRemovePredicate) {
        urlParameters.entrySet().removeIf(entry -> parameterRemovePredicate.test(entry.getKey(), entry.getValue()));
    }

    void clearQueryParameters() {
        queryParameters.clear();
    }

    public Map<String, List<String>> getAllQueryParameters() {
        return new HashMap<>(queryParameters);
    }

    public List<String> getQueryParameters(String name) {
        return Optional.ofNullable(queryParameters.get(name)).orElse(Collections.emptyList());
    }

    public void setQueryParameters(String name, List<String> value) {
        queryParameters.put(name, value);
    }
}
