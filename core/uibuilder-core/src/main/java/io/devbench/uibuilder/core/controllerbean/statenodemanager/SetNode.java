/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class SetNode extends CollectionNode {

    private Map<UUID, Object> valueMap;
    private Set<? super Object> referencedSet;
    private Map<UUID, BindingNode> elementIdToCollectionNodeMap;

    SetNode(String name, AbstractPropertyMetadata<? super Object> property, ClassMetadata<?> classMetadata, StateNodeManager manager) {
        super(name, property, classMetadata, manager);
    }

    @Override
    public Serializable populateValues() {
        if (property.getInstance() != null && property.getValue() != null) {
            referencedSet = property.getValue();
            createValueMapFromReferencedSet();
        } else {
            referencedSet = Collections.emptySet();
            valueMap = Collections.emptyMap();
        }

        buildCollectionNodesByValuesMap();

        return createStateNodeFromCollectionNodes();
    }

    private void createValueMapFromReferencedSet() {
        valueMap = referencedSet.stream()
            .collect(Collectors.toMap(value -> UUID.randomUUID(), Function.identity()));
    }

    private void buildCollectionNodesByValuesMap() {
        elementIdToCollectionNodeMap = new HashMap<>();
        collectionNodes = new ArrayList<>();
        valueMap.forEach((key, mappedValue) -> {
            BindingNode actualNode = prototypeNode.cloneWithProperty(
                syntheticPropertyPrototype.clone(
                    () -> valueMap.get(key),
                    value -> {
                        referencedSet.remove(mappedValue);
                        valueMap.put(key, value);
                        referencedSet.add(value);
                    }
                )
            );
            collectionNodes.add(actualNode);
            elementIdToCollectionNodeMap.put(key, actualNode);
        });
    }

    @Override
    public void synchronizeStateNode() {
        Set<? super Object> newValue = null;
        if (property.getInstance() != null) {
            newValue = property.getValue();
        }

        if (newValue != null && !newValue.isEmpty()) {
            if (newValue != referencedSet) {
                referencedSet = newValue;
                createValueMapFromReferencedSet();
                buildCollectionNodesByValuesMap();
            } else {
                if (newValue.size() != valueMap.values().size()
                    || !newValue.containsAll(valueMap.values())
                    || !valueMap.values().containsAll(newValue)) {
                    addNewElementsToCollectionNodes();
                    removeObsolateElementFromCollectionNodes();
                }
            }
        } else {
            collectionNodes.clear();
            elementIdToCollectionNodeMap.clear();
            valueMap.clear();
        }

        setupModelListInStateNode();
    }

    private void removeObsolateElementFromCollectionNodes() {
        Set<UUID> idsToRemove = valueMap.entrySet().stream()
            .filter(entry -> !referencedSet.contains(entry.getValue()))
            .map(Map.Entry::getKey)
            .collect(Collectors.toSet());
        idsToRemove.forEach(valueMap::remove);
        idsToRemove.stream()
            .map(elementIdToCollectionNodeMap::get)
            .forEach(collectionNodes::remove);
    }

    private void addNewElementsToCollectionNodes() {
        referencedSet.stream()
            .filter(element -> !valueMap.values().contains(element))
            .forEach(element -> {
                UUID id = UUID.randomUUID();
                valueMap.put(id, element);
                BindingNode actualNode = prototypeNode.cloneWithProperty(syntheticPropertyPrototype.clone(
                    () -> element,
                    value -> {
                        referencedSet.remove(element);
                        valueMap.put(id, value);
                        referencedSet.add(value);
                    }
                ));
                elementIdToCollectionNodeMap.put(id, actualNode);
                collectionNodes.add(actualNode);
            });
    }

    @Override
    public BindingNode cloneWithProperty(AbstractPropertyMetadata<? super Object> property) {
        CollectionNode result = new SetNode(name, property, classMetadata, manager);
        result.prototypeNode = this.prototypeNode;
        return result;
    }

}
