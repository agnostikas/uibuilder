/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.api.parse.utils.ElementUtil;
import io.devbench.uibuilder.core.page.PageLoaderContext;

import java.util.stream.StreamSupport;

public class MethodReferenceNormalizerProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        ElementUtil.extractAllChildElementOf(context.getPageElement())
            .flatMap(element -> StreamSupport.stream(element.attributes().spliterator(), false))
            .filter(attribute -> attribute.getValue() != null)
            .filter(attribute -> attribute.getValue().matches("^[a-zA-Z][a-z_A-Z0-9]*::[a-zA-Z][a-z_A-Z0-9]*$"))
            .forEach(attribute -> attribute.setValue(attribute.getValue().replaceAll("::", "_")));
    }
}
