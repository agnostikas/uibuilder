/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow;

import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class NavigationBuilder {

    private final String flowTargetId;
    private final Map<String, String> urlParameters;
    private final Map<String, List<String>> queryParameters;

    private NavigationBuilder(String flowTargetId) {
        this.flowTargetId = flowTargetId;
        this.urlParameters = new HashMap<>();
        this.queryParameters = new HashMap<>();
    }

    public static NavigationBuilder to(String flowTargetId) {
        return new NavigationBuilder(flowTargetId);
    }

    /**
     * Adds or removes an urlParameter to the current navigation call.
     *
     * @param parameterName the name of the parameter
     * @param value the value of the parameter. if the value is null we'll unset the url parameter
     * @return this NavigationBuilder instance
     */
    public <T> NavigationBuilder urlParameter(@NotNull String parameterName, T value) {
        if (value == null) {
            urlParameters.remove(parameterName);
        } else {
            PropertyConverter<T, String> propertyConverter = (PropertyConverter<T, String>) PropertyConverters.getConverterByType(value.getClass());
            urlParameters.put(parameterName, propertyConverter.convertTo(value));
        }
        return this;
    }

    /**
     * Adds or removes a queryParameter to the current navigation call.
     *
     * @param parameterName the name of the parameter
     * @param value the value of the parameter. if the value is null we'll unset the query parameter
     * @return this NavigationBuilder instance
     */
    public <T> NavigationBuilder queryParameter(@NotNull String parameterName, T value) {
        if (value != null) {
            PropertyConverter<T, String> propertyConverter = (PropertyConverter<T, String>) PropertyConverters.getConverterByType(value.getClass());
            queryParameters
                .computeIfAbsent(parameterName, s -> new ArrayList<>())
                .add(propertyConverter.convertTo(value));
        }
        return this;
    }

    /**
     * Executes the built navigation request.
     */
    public void navigate() {
        FlowManager.getCurrent().navigate(flowTargetId, urlParameters, queryParameters);
    }


}
