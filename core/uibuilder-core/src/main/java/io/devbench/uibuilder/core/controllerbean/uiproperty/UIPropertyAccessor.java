/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.function.Consumer;
import java.util.function.Supplier;


@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class UIPropertyAccessor<T> {

    @NotNull
    private final PropertyConverter<T, ? super Serializable> converter;

    @NotNull
    private final Supplier<? extends Serializable> rawValueSupplier;

    @NotNull
    private final Consumer<? super Serializable> rawValueConsumer;


    public T getValue() {
        return converter.convertFrom(rawValueSupplier.get());
    }

    public void setValue(T value) {
        rawValueConsumer.accept(converter.convertTo(value));
    }

}
