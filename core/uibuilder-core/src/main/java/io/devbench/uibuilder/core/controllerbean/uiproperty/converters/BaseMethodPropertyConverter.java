/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty.converters;

import com.helger.commons.functional.IThrowingSupplier;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.controllerbean.uiproperty.StringPropertyConverter;
import io.devbench.uibuilder.core.exceptions.PropertyConverterException;
import io.devbench.uibuilder.core.templateparser.UIBuilderMethodReference;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public abstract class BaseMethodPropertyConverter<T> implements StringPropertyConverter<T> {

    private final Class<T> clazz;

    protected BaseMethodPropertyConverter(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public final String convertTo(T value) {
        if (!(value instanceof Serializable)) {
            throw new PropertyConverterException("Not serializable supplier");
        }
        final String id = UUID.randomUUID().toString();
        VaadinSession.getCurrent().setAttribute(id, value);
        return id;
    }

    @Override
    @SuppressWarnings("unchecked")
    public final T apply(@NotNull String value) {
        Object attribute = VaadinSession.getCurrent().getAttribute(value);
        if (clazz.isInstance(attribute)) {
            return (T) attribute;
        } else if (attribute instanceof UIBuilderMethodReference) {
            return createImplementationFromMethodReference((UIBuilderMethodReference) attribute);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    private T createImplementationFromMethodReference(UIBuilderMethodReference methodReference) {
        Map<MethodType, MethodHandle> methodHandles = methodReference
            .getMethodsWithName()
            .entrySet()
            .stream()
            .filter(it -> it.getKey().parameterCount() == getParameterCount())
            .filter(it -> it.getKey().parameterType(0).isAssignableFrom(methodReference.getControllerBeanClass()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return createImplementationFromBestMatchingMethodHandle(methodReference, methodHandles);
    }

    protected abstract T createImplementationFromBestMatchingMethodHandle(
        UIBuilderMethodReference methodReference,
        Map<MethodType, MethodHandle> methodHandles
    );

    protected abstract int getParameterCount();

    final Object tryToInvokeMethodHandle(IThrowingSupplier<Object, Throwable> supplier) {
        try {
            return supplier.get();
        } catch (Throwable throwable) {
            throw new InvocationException(throwable);
        }
    }

    static Collector<MethodHandle, ?, MethodHandle> methodCollector() {
        return Collector.of(
            () -> new MethodHandle[1],
            BaseMethodPropertyConverter::selectOverriddenMethod,
            BaseMethodPropertyConverter::selectOverriddenMethod,
            (accumulator) -> {
                if (accumulator[0] != null) {
                    return accumulator[0];
                } else {
                    throw new NoSuchElementException();
                }
            }
        );
    }

    private static MethodHandle[] selectOverriddenMethod(MethodHandle[] left, MethodHandle[] right) {
        if (left[0] == null) {
            return right;
        } else if (right[0] == null) {
            return left;
        } else {
            MethodHandle[] accumulator = new MethodHandle[1];
            selectOverriddenMethod(accumulator, left[0]);
            selectOverriddenMethod(accumulator, right[0]);
            return accumulator;
        }
    }

    private static void selectOverriddenMethod(MethodHandle[] accumulator, MethodHandle newMethodHandle) {
        if (accumulator[0] == null) {
            accumulator[0] = newMethodHandle;
        } else {
            MethodHandle current = accumulator[0];
            boolean shouldChange = isNewMethodHandleOverridesTheOldOneInBaseClass(current, newMethodHandle)
                || isNewMethodHandleHasMoreSpecificParameterTypesInSameClass(current, newMethodHandle);
            if (shouldChange) {
                accumulator[0] = newMethodHandle;
            }
        }
    }

    private static boolean isNewMethodHandleHasMoreSpecificParameterTypesInSameClass(MethodHandle current, MethodHandle newMethodHandle) {
        return current.type().parameterType(0).equals(newMethodHandle.type().parameterType(0))
            && current.type().parameterType(1).isAssignableFrom(newMethodHandle.type().parameterType(1));
    }

    private static boolean isNewMethodHandleOverridesTheOldOneInBaseClass(MethodHandle current, MethodHandle newMethodHandle) {
        return current.type().parameterType(0).isAssignableFrom(newMethodHandle.type().parameterType(0))
            && !current.type().parameterType(0).equals(newMethodHandle.type().parameterType(0));
    }
}
