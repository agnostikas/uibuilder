/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.validator;

import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Data;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Source;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.core.flow.FlowParameter;
import io.devbench.uibuilder.core.flow.QueryParameter;
import lombok.Getter;

public enum InjectionAnnotation {

    ITEM(Item.class),
    SOURCE(Source.class),
    UICOMPONENT(UIComponent.class),
    URLPARAMETER(FlowParameter.class),
    QUERYPARAMETER(QueryParameter.class),
    VALUE(Value.class),
    DATA(Data .class);

    @Getter
    private final Class<?> annotationClass;

    InjectionAnnotation(Class<?> annotationClass) {
        this.annotationClass = annotationClass;
    }
}
