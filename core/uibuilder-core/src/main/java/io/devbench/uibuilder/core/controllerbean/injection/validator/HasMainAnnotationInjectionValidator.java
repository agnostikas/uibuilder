/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.validator;

import org.apache.commons.lang3.tuple.Pair;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.*;
import static java.util.stream.Collectors.*;

public class HasMainAnnotationInjectionValidator implements InjectionAnnotationValidator {

    public static final Set<InjectionAnnotation> KNOWN_ANNOTATIONS = stream(
        InjectionAnnotation.values())
        .collect(toSet());

    public static boolean isValid(Map<Annotation, Optional<InjectionAnnotation>> injectionAnnotations) {
        final long countOfMainAnnotation = getCountOfMainAnnotation(injectionAnnotations);
        return countOfMainAnnotation == 1;
    }

    private static long getCountOfMainAnnotation(Map<Annotation, Optional<InjectionAnnotation>> injectionAnnotations) {
        return injectionAnnotations
            .values()
            .stream()
            .filter(Optional::isPresent)
            .map(Optional::get)
            .count();
    }

    public Map<Annotation, Optional<InjectionAnnotation>> apply(Annotation[] annotations) {
        return stream(annotations)
            .map(annotation -> Pair.of(annotation, getFirst(annotation)))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Optional<InjectionAnnotation> getFirst(Annotation annotation) {
        return KNOWN_ANNOTATIONS
            .stream().filter(injectionAnnotation -> injectionAnnotation.getAnnotationClass()
                .isAssignableFrom(annotation.getClass())).findFirst();
    }

    @Override
    public boolean test(Annotation[] annotations, Object ignore) {
        return isValid(apply(annotations));
    }
}
