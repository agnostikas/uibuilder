/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Implement this, for simple types, that can be represented as a simple string, and the client component will be able to use that string
 *
 */
public interface StringPropertyConverter<PROPERTY_TYPE> extends PropertyConverter<PROPERTY_TYPE, String> {

    default String convertTo(PROPERTY_TYPE value) {
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }

    default PROPERTY_TYPE convertFrom(String value) {
        if (value != null) {
            if (StringUtils.isNotBlank(value)) {
                return apply(value);
            }
        }
        return null;
    }

    PROPERTY_TYPE apply(@NotNull String value);
}
