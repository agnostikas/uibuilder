/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import io.devbench.uibuilder.core.controllerbean.uiproperty.Converter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.ItemConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverter;
import io.devbench.uibuilder.core.exceptions.StateNodeInvalidReferenceException;
import io.devbench.uibuilder.core.exceptions.StateNodeSyntheticPropertyException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import lombok.Getter;
import lombok.Setter;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.Consumer;
import java.util.function.Supplier;


public class CollectionSyntheticProperty implements AbstractPropertyMetadata<Object> {

    @Getter
    @Setter
    private Supplier<?> valueProvider;

    @Getter
    @Setter
    private Consumer<? super Object> valueConsumer;

    private ClassMetadata<?> typeMetaCache;

    private AbstractPropertyMetadata<? super Object> collectionProperty;

    private Class<?> typeParameterClass;

    public CollectionSyntheticProperty(AbstractPropertyMetadata<? super Object> collectionProperty) {
        this.collectionProperty = collectionProperty;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <VALUE> VALUE getValue() {
        return valueProvider != null ? (VALUE) valueProvider.get() : null;
    }

    @Override
    public <VALUE> void setValue(VALUE value) {
        if (valueConsumer != null) {
            valueConsumer.accept(value);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> ClassMetadata<T> typeMeta() {
        if (typeMetaCache == null) {
            Type typeParameterAsType = getTypeParameterAsType();
            if (typeParameterAsType instanceof Class) {
                typeMetaCache = ClassMetadata.ofClass(getTypeParameterAsClass());
            } else if (typeParameterAsType instanceof ParameterizedType) {
                typeMetaCache = ClassMetadata.ofParametrizedType((ParameterizedType) typeParameterAsType);
            } else {
                throwNotClassOrParameterizedTypeException(typeParameterAsType);
            }
        }
        return (ClassMetadata<T>) typeMetaCache;
    }

    public Class<?> getTypeParameterAsClass() {
        if (typeParameterClass == null) {
            Type typeArgument = getTypeParameterAsType();
            if (typeArgument instanceof Class) {
                typeParameterClass = (Class<?>) typeArgument;
            } else if (typeArgument instanceof ParameterizedType) {
                typeParameterClass = (Class<?>) ((ParameterizedType) typeArgument).getRawType();
            } else {
                return throwNotClassOrParameterizedTypeException(typeArgument);
            }
        }
        return typeParameterClass;
    }

    private Class<?> throwNotClassOrParameterizedTypeException(Type typeArgument) {
        throw new StateNodeInvalidReferenceException(
            "Actual parameter type is not class or parameterized type in referenced collection, type parameters: " + typeArgument);
    }

    private Type getTypeParameterAsType() {
        Type[] actualTypeArguments = collectionProperty.getParameterizedType().getActualTypeArguments();
        if (actualTypeArguments != null && actualTypeArguments.length > 0) {
            return actualTypeArguments[0];
        } else {
            throw new StateNodeInvalidReferenceException("The referred collection hasn't got any actual parameter type");
        }
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return (Converter.class.isAssignableFrom(annotationClass) && collectionProperty.isAnnotationPresent(ItemConverter.class))
            || (ItemConverter.class.isAssignableFrom(annotationClass) && collectionProperty.isAnnotationPresent(ItemConverter.class));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
        if (Converter.class.isAssignableFrom(annotationClass) && collectionProperty.isAnnotationPresent(ItemConverter.class)) {
            ItemConverter itemConverter = collectionProperty.getAnnotation(ItemConverter.class);
            return (A) new Converter() {

                @Override
                public Class<? extends Annotation> annotationType() {
                    return Converter.class;
                }

                @Override
                public Class<? extends PropertyConverter<?, ?>> value() {
                    return itemConverter.value();
                }
            };
        } else if (ItemConverter.class.isAssignableFrom(annotationClass) && collectionProperty.isAnnotationPresent(ItemConverter.class)) {
            return (A) collectionProperty.getAnnotation(ItemConverter.class);
        }
        return null;
    }

    @Override
    public AbstractPropertyMetadata<Object> clone(Object instance) {
        throw new StateNodeSyntheticPropertyException("Cannot access the instance in a CollectionSyntheticProperty");
    }

    public CollectionSyntheticProperty clone(Supplier<?> valueProvider, Consumer<? super Object> valueConsumer) {
        CollectionSyntheticProperty clone = new CollectionSyntheticProperty(collectionProperty);
        clone.typeMetaCache = typeMetaCache;
        clone.typeParameterClass = typeParameterClass;
        clone.valueProvider = valueProvider;
        clone.valueConsumer = valueConsumer;
        return clone;
    }

    @Override
    public Object getInstance() {
        if (valueProvider == null) {
            throw new StateNodeSyntheticPropertyException("Cannot access the instance in a CollectionSyntheticProperty without a value provider set");
        }
        return valueProvider;
    }

    @Override
    public void setInstance(Object instance) {
        throw new StateNodeSyntheticPropertyException("Cannot set the instance in a CollectionSyntheticProperty");
    }

    @Override
    public Class<?> getType() {
        return getTypeParameterAsClass();
    }

    @Override
    public String getName() {
        return CollectionNode.ITEM_REFERENCE_STRING;
    }

    @Override
    public ParameterizedType getParameterizedType() {
        Type typeParameterAsType = getTypeParameterAsType();

        if (typeParameterAsType instanceof ParameterizedType) {
            return (ParameterizedType) typeParameterAsType;
        }

        throw new StateNodeInvalidReferenceException("The referred collection hasn't got any actual parameter type");
    }

    @Override
    public boolean isSynthetic() {
        return true;
    }
}
