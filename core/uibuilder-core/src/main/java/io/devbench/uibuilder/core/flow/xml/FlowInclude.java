/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.xml;

import io.devbench.uibuilder.core.flow.parsed.ParsedFlowElement;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "include")
public class FlowInclude extends FlowElement {

    @XmlAttribute
    private String id;

    @XmlAttribute
    private String route;

    @XmlAttribute
    private String path;

    @Override
    public ParsedFlowElement convertToParsedElement() {
        throw new UnsupportedOperationException("This should be changed to some other FlowElements during the parsing...");
    }
}
