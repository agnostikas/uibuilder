/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.session.context;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.exceptions.BeanNotMarkedAsUiContextStoredException;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

public class UIContext {

    private static final String UI_BUILDER_SESSION_CONTEXT = "UIBuilderSessionContext";

    @Getter(AccessLevel.PACKAGE)
    private Map<Class<?>, Map<Integer, Object>> sessionSeparatedUIContextMap;

    public static UIContext getContext() {
        UIContext uiContext;
        if (VaadinSession.getCurrent().getAttribute(UI_BUILDER_SESSION_CONTEXT) == null) {
            uiContext = new UIContext();
            VaadinSession.getCurrent().setAttribute(UI_BUILDER_SESSION_CONTEXT, uiContext);
        } else {
            uiContext = (UIContext) VaadinSession.getCurrent().getAttribute(UI_BUILDER_SESSION_CONTEXT);
        }
        return uiContext;
    }

    @SuppressWarnings("unchecked")
    public <T> void addToActiveUIContext(T instanceToAdd) {
        addToActiveUIContextAs((Class<? super T>) instanceToAdd.getClass(), instanceToAdd);
    }

    public <S, T extends S> void addToActiveUIContextAs(Class<S> clazz, T instanceToAdd) {
        if (clazz.isAnnotationPresent(UIContextStored.class)) {
            UI.getCurrent().addDetachListener(event -> sessionSeparatedUIContextMap.get(clazz).remove(((UI) event.getSource()).getUIId()));
            sessionSeparatedUIContextMap.computeIfAbsent(clazz, key -> new ConcurrentHashMap<>()).put(UI.getCurrent().getUIId(), instanceToAdd);
        } else {
            throw new BeanNotMarkedAsUiContextStoredException("The class " + clazz + " should have the @UIContextStored annotation", clazz);
        }
    }

    private UIContext() {
        sessionSeparatedUIContextMap = new ConcurrentHashMap<>();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> type) {
        if (type.isAnnotationPresent(UIContextStored.class)) {
            return (T) sessionSeparatedUIContextMap.getOrDefault(type, Collections.emptyMap()).get(UI.getCurrent().getUIId());
        }
        return null;
    }

    public <T> T computeIfAbsent(Class<T> type, Supplier<T> instanceSupplier) {
        T instance = get(type);
        if (instance == null) {
            instance = instanceSupplier.get();
            Objects.requireNonNull(instance);
            addToActiveUIContext(instance);
        }
        return instance;
    }

}
