/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.internal.nodefeature.NodeProperties;
import com.vaadin.flow.shared.Registration;
import elemental.json.Json;
import io.devbench.uibuilder.api.components.HasItemType;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import io.devbench.uibuilder.api.listeners.BackendAttachListener;
import io.devbench.uibuilder.api.parse.BindingContext;
import io.devbench.uibuilder.api.parse.BindingHandlerParseInterceptor;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.BindingNode;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.CollectionNode;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.flow.FlowTarget;
import io.devbench.uibuilder.core.parse.ParseInterceptorsRegistry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import static io.devbench.uibuilder.core.flow.FlowManager.*;


@Slf4j
public class ElementParsingComponentManager {

    public static final String FLOW_FUNCTION_PREFIX = "____flow_navigation_to____";
    public static final String ATTR_AS = "as";
    public static final String DEFAULT_TEMPLATE_BEAN_NAME = "item";

    private Element element;
    private Component parentComponent;
    private Set<ParseInterceptor> interceptors;
    private Map<String, ComponentInjectionPoint> componentInjectionPoints;
    private CollectionNode collectionBindingNode;
    private com.vaadin.flow.dom.Element mainContainerElement;
    private ElementParsingComponentManager parent;
    private Set<String> bindings;
    private StateNodeManager stateNodeManager;
    private Consumer<String> availableFlowTargetEventConsumer;
    private String htmlPath;

    private DomEventListener virtualChildAttacher;
    private com.vaadin.flow.dom.Element vaadinElement;
    private Component vaadinComponent;

    private final String templateBeanName;

    private boolean bindingInterceptorCreator;

    private List<ElementBindingHandlerDescriptor> bindingHandlerDescriptors;

    public ElementParsingComponentManager(ElementParserContext elementParserContext) {
        this.element = elementParserContext.getElement();
        this.parentComponent = elementParserContext.getParentComponent();
        this.componentInjectionPoints = elementParserContext.getComponentInjectionPoints();
        this.stateNodeManager = elementParserContext.getStateNodeManager();
        this.bindings = elementParserContext.getBindings();
        this.mainContainerElement = elementParserContext.getMainContainerElement();
        this.parent = elementParserContext.getParent();
        this.availableFlowTargetEventConsumer = elementParserContext.getAvailableFlowTargetEventConsumer();
        this.htmlPath = elementParserContext.getHtmlPath();
        this.bindingHandlerDescriptors = elementParserContext.getBindingHandlerDescriptors();
        this.interceptors = ParseInterceptorsRegistry.getInstance().getInterceptorsByElement(element);

        this.templateBeanName = element
            .parents()
            .stream()
            .filter(elem -> elem.hasAttr(ATTR_AS))
            .findFirst()
            .map(e -> e.attr(ATTR_AS))
            .orElse(DEFAULT_TEMPLATE_BEAN_NAME);
    }

    public void manageComponent() {
        if (!isInScript() && !isInNoScript()) {
            handleComponent();
        }
        commitBindingHandler();
    }

    private void handleComponent() {
        if (!isInTemplate()) {
            handleNotTemplateComponent();
        }
        handleBindings();
        handleChildren();
    }

    private void handleNotTemplateComponent() {
        extractFlowNavigationLinksAsFunctionReferences();
        vaadinComponent = instantiateComponentByTagNameIfRequired(element);
        String id = element.id();
        findOrCreateVaadinComponent();
        if (StringUtils.isNotBlank(id)) {
            manageInjectionPoint(id);
        }

        if (vaadinComponent != null) {
            additionalVaadinComponentSetup(id);
        }
    }

    private void additionalVaadinComponentSetup(String id) {
        if (StringUtils.isNotBlank(id)) {
            vaadinComponent.setId(id);
        }

        interceptParseOn(vaadinComponent);
        createIdForElementIfNeeded();
        setupVirtualChildAttachListener();
        setupValueTypeCapableComponent();

        if (vaadinComponent instanceof FlowTarget) {
            ((FlowTarget) vaadinComponent).registerSelf();

            if (vaadinComponent instanceof Page) {
                ((Page) vaadinComponent).setParentStateNodeManager(stateNodeManager);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void manageInjectionPoint(String id) {
        ComponentInjectionPoint injectionPoint = componentInjectionPoints.get(id);
        if (injectionPoint != null) {
            injectionPoint.setElement(vaadinElement);
            if (!injectionPoint.isAmbiguous()) {
                Class<?> componentType = injectionPoint.getUnambiguousType();
                if (vaadinComponent == null && Component.class.isAssignableFrom(componentType)) {
                    vaadinComponent = Component.from(vaadinElement, (Class<? extends Component>) componentType);
                }
                injectionPoint.setValue(vaadinComponent);
            }
        }
    }

    private void findOrCreateVaadinComponent() {
        if (vaadinComponent != null) {
            vaadinElement = vaadinComponent.getElement();
        } else {
            vaadinElement = new com.vaadin.flow.dom.Element(element.tagName());
            if (isElementNotHtmlComponent(element)) {
                vaadinElement.attachShadow();
            }
        }
    }

    public boolean isInNoScript() {
        return isInTag("noscript");
    }

    public boolean isInScript() {
        return isInTag("script");
    }

    private boolean isElementNotHtmlComponent(Element element) {
        String id = element.id();
        return StringUtils.isNotBlank(id) && componentInjectionPoints.get(id) != null
            && !componentInjectionPoints.get(id).isAnyComponentSubclassOf(HtmlComponent.class);
    }

    private void extractFlowNavigationLinksAsFunctionReferences() {
        for (Attribute attribute : element.attributes()) {
            if (attribute.getValue() == null) {
                continue;
            }
            Matcher matcher = FLOW_PATTERN.matcher(attribute.getValue());
            while (matcher.find()) {
                String flowId = matcher.group(1);
                String flowNavigationMethodName = FLOW_FUNCTION_PREFIX + flowId;
                availableFlowTargetEventConsumer.accept(flowNavigationMethodName);
            }
            attribute.setValue(attribute.getValue().replaceAll("flow:([A-Za-z][A-Za-z0-9]*)", FLOW_FUNCTION_PREFIX + "$1"));
        }
    }

    private void handleChildren() {
        element.children().stream()
            .map(child -> new ElementParsingComponentManager(
                ElementParserContext.builder()
                    .element(child)
                    .componentInjectionPoints(componentInjectionPoints)
                    .parentComponent(parentComponent)
                    .stateNodeManager(stateNodeManager)
                    .bindings(bindings)
                    .mainContainerElement(mainContainerElement)
                    .parent(this)
                    .availableFlowTargetEventConsumer(availableFlowTargetEventConsumer)
                    .bindingHandlerDescriptors(getBindingHandlerDescriptorsForChildren())
                    .build()
            ))
            .forEach(ElementParsingComponentManager::manageComponent);
    }

    private List<ElementBindingHandlerDescriptor> getBindingHandlerDescriptorsForChildren() {
        if (bindingHandlerDescriptors != null) {
            return bindingHandlerDescriptors.stream()
                .filter(descriptor -> descriptor.getInterceptor().appliesToSubElements())
                .collect(Collectors.toList());
        }
        return null;
    }

    private void createIdForElementIfNeeded() {
        if (StringUtils.isBlank(element.id())) {
            element.attr("id", UUID.randomUUID().toString());
        }
    }

    /**
     * Sets up a listener to handle virtual children addition on the first element attach event on the main container.
     * After the event fired it will add the components element as a virtual child to the parent component or the main container element if there is no parent,
     * then it removes the listener from the main container so no further changes will trigger this code;
     */
    private void setupVirtualChildAttachListener() {
        Registration domListenerRegistration = mainContainerElement.getComponent()
            .filter(DomBind.class::isInstance)
            .map(DomBind.class::cast)
            .map(domBind -> domBind.addInnerHtmlSetEventListener(e -> {
                if (virtualChildAttacher != null) {
                    virtualChildAttacher.handleEvent(new DomEvent(e.getSource().getElement(), "innerHTMLSet", Json.createObject()));
                }
                callUibuilderReadyJsMethodIfExists();
            })).orElse(null);
        virtualChildAttacher = e -> {
            com.vaadin.flow.dom.Element parentContainer = parentComponent == null ? mainContainerElement : parentComponent.getElement();
            parentContainer
                .getStateProvider()
                .appendVirtualChild(parentContainer.getNode(), vaadinElement, NodeProperties.INJECT_BY_ID, element.id());

            virtualChildAttacher = null;

            if (vaadinComponent instanceof BackendAttachListener) {
                ((BackendAttachListener) vaadinComponent).onAttached();
            }

            if (domListenerRegistration != null) {
                domListenerRegistration.remove();
            }
        };
    }

    private void setupValueTypeCapableComponent() {
        if (vaadinComponent.getId().isPresent() && vaadinComponent instanceof HasItemType) {
            HasItemType valueTypeElement = (HasItemType) vaadinComponent;
            String componentId = vaadinComponent.getId().get();
            ComponentInjectionPoint componentInjectionPoint = componentInjectionPoints.get(componentId);
            if (componentInjectionPoint != null && !componentInjectionPoint.isAmbiguous()) {
                componentInjectionPoint
                    .getUnambiguousParameterizedType()
                    .ifPresent(valueTypeElement::setItemType);
            }
        }
    }

    private void callUibuilderReadyJsMethodIfExists() {
        UI ui = UI.getCurrent();
        ui.getInternals().getStateTree().beforeClientResponse(vaadinElement.getNode(),
            context -> ui.getPage().executeJs("$0._uibuilderReady && $0._uibuilderReady()", vaadinElement));
    }

    public boolean isInTemplate() {
        return isInTag("template");
    }

    private boolean isInTag(String tagName) {
        return tagName.equals(element.tagName())
            || element.parents().stream().anyMatch(element -> tagName.equalsIgnoreCase(element.tagName()));
    }

    @SuppressWarnings("unchecked")
    private void prepareBindingHandler() {
        interceptors.stream()
            .filter(BindingHandlerParseInterceptor.class::isInstance)
            .map(interceptor -> ((BindingHandlerParseInterceptor<BindingContext>) interceptor))
            .filter(bindingHandlerInterceptor -> bindingHandlerInterceptor.isApplicable(element))
            .forEach(bindingHandlerInterceptor -> {
                BindingContext bindingContext = bindingHandlerInterceptor.createBindingContext(element);
                if (bindingHandlerDescriptors == null) {
                    bindingHandlerDescriptors = new ArrayList<>();
                }
                bindingHandlerDescriptors.add(new ElementBindingHandlerDescriptor(bindingHandlerInterceptor, bindingContext));
                bindingInterceptorCreator = true;
            });
    }

    private void commitBindingHandler() {
        if (bindingInterceptorCreator && bindingHandlerDescriptors != null && !bindingHandlerDescriptors.isEmpty()) {
            bindingHandlerDescriptors.forEach(descriptor -> {
                descriptor.getInterceptor().commitBindingParse(descriptor.getBindingContext(), vaadinComponent);
            });
        }
    }

    private void handleBindings() {
        prepareBindingHandler();
        element.attributes().forEach(attribute -> {
            if (attribute.getValue() != null) {
                Matcher matcher = StateNodeManager.BINDING_PATTERN.matcher(attribute.getValue());
                while (matcher.find()) {
                    String binding = matcher.group(1);
                    handleBinding(binding);
                }
            }
        });
        if (element.hasText()) {
            Matcher matcher = StateNodeManager.BINDING_PATTERN.matcher(element.ownText());
            while (matcher.find()) {
                String binding = matcher.group(1);
                handleBinding(binding);
            }
        }
    }

    private boolean isTemplateBeanBinding(@NotNull String binding) {
        return binding.startsWith(templateBeanName) || binding.startsWith(DEFAULT_TEMPLATE_BEAN_NAME);
    }

    private void handleBinding(String binding) {
        if (handleWithBindingHandler(binding)) {
            return;
        }
        if (isInTemplate() && isTemplateBeanBinding(binding)) {
            CollectionNode collectionBindingInParents = getClosestCollectionBindingInParents(parent);
            if (collectionBindingInParents == null) {
                // it is safe to ignore this on the backend right now
                return;
            }
            collectionBindingInParents.addCollectionPath(binding);
            BindingNode bindingNode = collectionBindingInParents.getNodeAt(binding.split("\\."));
            if (bindingNode instanceof CollectionNode) {
                collectionBindingNode = (CollectionNode) bindingNode;
            }
        } else {
            stateNodeManager.addBindingPath(binding);
            BindingNode node = stateNodeManager.getNodeForPath(binding);
            if (node instanceof CollectionNode) {
                collectionBindingNode = (CollectionNode) node;
            }
        }
        bindings.add(binding);
    }

    private boolean handleWithBindingHandler(String binding) {
        if (bindingHandlerDescriptors != null) {
            bindingHandlerDescriptors.forEach(descriptor -> {
                BindingContext bindingContext = descriptor.getBindingContext();
                BindingHandlerParseInterceptor<BindingContext> interceptor = descriptor.getInterceptor();
                if (!element.equals(bindingContext.getParsedElement())) {
                    interceptor.handleBinding(binding, bindingContext);
                }
            });
        }
        return false;
    }

    private CollectionNode getClosestCollectionBindingInParents(ElementParsingComponentManager parent) {
        if (parent != null) {
            return parent.collectionBindingNode != null ? parent.collectionBindingNode : getClosestCollectionBindingInParents(parent.parent);
        }
        return null;
    }

    private void interceptParseOn(Component component) {
        if (interceptors != null) {
            interceptors.forEach(interceptor -> {
                if (parent != null) {
                    interceptor.setParentComponent(parent.vaadinComponent);
                }
                interceptor.setHtmlPath(htmlPath);
                interceptor.intercept(component, element);
            });
        }
    }

    private Component instantiateComponentByTagNameIfRequired(Element componentElement) {
        if (interceptors != null) {
            return interceptors.stream()
                .filter(parseInterceptor -> parseInterceptor.isInstantiator(componentElement))
                .findAny()
                .map(ParseInterceptor::instantiateComponent).orElse(null);
        }
        return null;
    }

}
