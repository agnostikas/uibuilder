/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.UnloadPageFromFlowManagerProcessor;
import io.devbench.uibuilder.core.page.pageloaderprocessors.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import java.util.*;


@Slf4j
public class PageLoader {

    protected static final List<PageLoaderProcessor> pageLoaderProcessors;

    protected static final List<PageUnloadProcessor> pageUnloadProcessors;

    @Getter(AccessLevel.PACKAGE)
    private final PageLoaderContext context;

    static {
        pageLoaderProcessors = new ArrayList<>(Arrays.asList(
            new ReadDocumentToContextProcessor(),
            new FindPageElementProcessor(),
            new BeforeLoadEventProcessor(),
            new AddJsModuleImportsProcessor(),
            new TranslateStaticTextsProcessor(),
            new PageTransformProcessor(),
            new MethodReferenceNormalizerProcessor(),
            new BackendTemplatesProcessor(),
            new ComponentsInjectionProcessor(),
            new SetPlainHtmlToDomBindProcessor(),
            new PropertiesManagingProcessor(),
            new CustomEventHandlersRegisteringProcessor(),
            new FlowNavigationMethodsRegisteringProcessor(),
            new AfterLoadEventProcessor()
        ));

        pageUnloadProcessors = new ArrayList<>(Arrays.asList(
            new BeforeUnloadEventProcessor(),
            new UnloadPageFromFlowManagerProcessor()
        ));
    }

    public static void registerProcessor(PageLoaderProcessor processor) {
        pageLoaderProcessors.add(processor);
    }

    public static void registerUnloadProcessor(PageUnloadProcessor processor) {
        pageUnloadProcessors.add(processor);
    }

    public static void registerProcessorAfter(Class<? extends PageLoaderProcessor> after, PageLoaderProcessor processor) {
        addAfterToList(after, pageLoaderProcessors, processor);
    }

    private static void addAfterToList(Class<?> after, List processors, Object processor) {
        int addAfter = -1;
        for (int i = 0; i < processors.size(); i++) {
            if (processors.get(i).getClass().isAssignableFrom(after)) {
                addAfter = i + 1;
            }
        }
        if (addAfter > 0) {
            processors.add(addAfter, processor);
        } else {
            throw new InternalResolverException("No processor found with class name: " + after.getSimpleName());
        }
    }

    public static void registerUnloadProcessorAfter(Class<? extends PageUnloadProcessor> after, PageUnloadProcessor processor) {
        addAfterToList(after, pageUnloadProcessors, processor);
    }

    PageLoader(String htmlPath, DomBind domBind, StateNodeManager parentStateNodeManager) {
        context = new PageLoaderContext(htmlPath, domBind, parentStateNodeManager);
        FlowManager.getCurrent().registerPageLoader(htmlPath, this);
    }


    public void load() {
        pageLoaderProcessors.forEach(processor -> processor.process(context));
    }

    public void loadFragment(String fragmentHtml, Component parentComponent, List<String> additionalBindings) {
        Set<String> mergedBindings = new HashSet<>();
        mergedBindings.addAll(context.getBindings());
        mergedBindings.addAll(additionalBindings);

        Element fragmentElement = Jsoup.parse(fragmentHtml).body();

        fragmentElement.children().stream()
            .map(element -> new ElementParsingComponentManager(
                ElementParserContext.builder()
                    .element(element)
                    .parentComponent(parentComponent)
                    .componentInjectionPoints(ControllerBeanManager.getInstance().findInjectionPointsById())
                    .stateNodeManager(context.getStateNodeManager())
                    .bindings(mergedBindings)
                    .mainContainerElement(context.getDomBind().getElement())
                    .parent(null)
                    .availableFlowTargetEventConsumer(context.getFlowNavigationMethods()::add)
                    .htmlPath(context.getHtmlPath())
                    .build()
            ))
            .forEach(ElementParsingComponentManager::manageComponent);

        String parentId = parentComponent != null ? parentComponent.getId().orElse(null) : null;
        context.getDomBind().dispatchInnerHtmlFragmentSet(fragmentElement.html(), parentId);
    }

    public void unload() {
        context.getStateNodeManager().unload();
        pageUnloadProcessors.forEach(processor -> processor.process(context));
    }

    public Collection<String> getPageContextIds() {
        return Collections.unmodifiableCollection(context.getPageContextIds());
    }

}
