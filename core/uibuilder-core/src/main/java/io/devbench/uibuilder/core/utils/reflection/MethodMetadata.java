/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import lombok.Getter;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.*;


@Slf4j
public class MethodMetadata<CONTAINER_TYPE> {

    private static final MethodHandles.Lookup lookup = MethodHandles.lookup();

    @Getter
    @With
    private final ClassMetadata<CONTAINER_TYPE> containerClassMetadata;

    private final Class<CONTAINER_TYPE> containerClass;

    @Getter
    @With
    private final CONTAINER_TYPE instance;

    private final Method method;


    MethodMetadata(ClassMetadata<CONTAINER_TYPE> containerClassMetadata, Class<CONTAINER_TYPE> containerClass, CONTAINER_TYPE instance, Method method) {
        this.containerClassMetadata = containerClassMetadata;
        this.containerClass = containerClass;
        this.instance = instance;
        this.method = method;
    }

    private void checkInstance() {
        if (instance == null) {
            throw new InternalResolverException("Instance is not set on metadata, to handle request");
        }
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public <T> T invoke(Object... parameters) {
        checkInstance();
        try {
            return (T) method.invoke(instance, parameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("invocation failed", e);
            return null;
        }
    }

    public Parameter[] getParameters() {
        return this.method.getParameters();
    }

    public Class<?> getDeclaringClass() {
        return this.method.getDeclaringClass();
    }

    public Type[] getGenericParameterTypes() {
        return this.method.getGenericParameterTypes();
    }

    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return this.method.isAnnotationPresent(annotationClass);
    }

    public String getName() {
        return this.method.getName();
    }

    public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
        return this.method.getAnnotation(annotationClass);
    }

    public boolean isVarArgs() {
        return this.method.isVarArgs();
    }

    public Class<?>[] getExceptionTypes() {
        return this.method.getExceptionTypes();
    }

    public Annotation[] getAnnotations() {
        return this.method.getAnnotations();
    }

    public boolean isBridge() {
        return this.method.isBridge();
    }

    public AnnotatedType[] getAnnotatedExceptionTypes() {
        return this.method.getAnnotatedExceptionTypes();
    }

    public AnnotatedType getAnnotatedReceiverType() {
        return this.method.getAnnotatedReceiverType();
    }

    public <T extends Annotation> T getDeclaredAnnotation(Class<T> annotationClass) {
        return this.method.getDeclaredAnnotation(annotationClass);
    }

    public AnnotatedType getAnnotatedReturnType() {
        return this.method.getAnnotatedReturnType();
    }

    public Type[] getGenericExceptionTypes() {
        return this.method.getGenericExceptionTypes();
    }

    public Annotation[][] getParameterAnnotations() {
        return this.method.getParameterAnnotations();
    }

    public String toGenericString() {
        return this.method.toGenericString();
    }

    public boolean isSynthetic() {
        return this.method.isSynthetic();
    }

    public int getModifiers() {
        return this.method.getModifiers();
    }

    public <T extends Annotation> T[] getDeclaredAnnotationsByType(Class<T> annotationClass) {
        return this.method.getDeclaredAnnotationsByType(annotationClass);
    }

    public Type getGenericReturnType() {
        return this.method.getGenericReturnType();
    }

    public Object getDefaultValue() {
        return this.method.getDefaultValue();
    }

    public Annotation[] getDeclaredAnnotations() {
        return this.method.getDeclaredAnnotations();
    }

    public <T extends Annotation> T[] getAnnotationsByType(Class<T> annotationClass) {
        return this.method.getAnnotationsByType(annotationClass);
    }

    public int getParameterCount() {
        return this.method.getParameterCount();
    }

    public Class<?> getReturnType() {
        return this.method.getReturnType();
    }

    public void setAccessible(boolean flag) throws SecurityException {
        this.method.setAccessible(flag);
    }

    public TypeVariable<Method>[] getTypeParameters() {
        return this.method.getTypeParameters();
    }

    public Class<?>[] getParameterTypes() {
        return this.method.getParameterTypes();
    }

    public AnnotatedType[] getAnnotatedParameterTypes() {
        return this.method.getAnnotatedParameterTypes();
    }

    public boolean isAccessible() {
        return this.method.isAccessible();
    }

    public boolean isDefault() {
        return this.method.isDefault();
    }

    public MethodHandle unreflect() throws IllegalAccessException {
        return lookup.unreflect(method);
    }
}
