/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.internal.nodefeature.NodeProperties;
import com.vaadin.flow.internal.nodefeature.VirtualChildrenList;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.api.components.HasRawElement;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.api.exceptions.UnsupportedElementException;
import io.devbench.uibuilder.core.startup.ComponentTagRegistry;
import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ElementCollector {

    public static final String ID = "id";
    public static final String ELEMENT_COLLECTOR_COMPONENT_MAP = "ElementCollector_ComponentMap";

    private static final Logger log = LoggerFactory.getLogger(ElementCollector.class);

    static Map<String, HtmlElementAwareComponent> getComponentMap() {
        VaadinSession session = VaadinSession.getCurrent();
        @SuppressWarnings("unchecked")
        Map<String, HtmlElementAwareComponent> map =
            (Map<String, HtmlElementAwareComponent>) session.getAttribute(ELEMENT_COLLECTOR_COMPONENT_MAP);
        if (map == null) {
            map = new HashMap<>();
            session.setAttribute(ELEMENT_COLLECTOR_COMPONENT_MAP, map);
        }
        return map;
    }

    public static void register(Component component, Element element) {
        getComponentId(component, element)
            .ifPresent(componentId -> getComponentMap().put(componentId, new HtmlElementAwareComponent(component, element)));
    }

    private static Optional<String> getComponentId(Component component, Element element) {
        return Optional.ofNullable(component.getId().orElseGet(() -> element.hasAttr(ID) ? element.attr(ID) : null));
    }

    public static void removeId(String id) {
        getComponentMap().remove(id);
    }

    public static Optional<HtmlElementAwareComponent> getById(String id) {
        return Optional.ofNullable(getComponentMap().get(id));
    }

    public static Optional<HtmlElementAwareComponent> getById(String id, HasRawElement rawElementComponent) {
        return getById(id, (Component) rawElementComponent, rawElementComponent.getRawElement());
    }

    public static Optional<HtmlElementAwareComponent> getById(String id, Component relativeComponent, Node relativeNode) {
        HtmlElementAwareComponent htmlElementAwareComponent = getComponentMap().get(id);
        if (htmlElementAwareComponent != null) {
            return Optional.of(htmlElementAwareComponent);
        } else {
            Optional<Element> element = findElementById(id, relativeNode.root());
            if (element.isPresent()) {
                return revive(element.get(), relativeComponent);
            }
        }
        log.warn("Could not find element by id: {}", id);
        return Optional.empty();
    }

    private static Optional<Element> findElementById(String id, Node node) {
        if (node.hasAttr("id") && id.equals(node.attr("id"))) {
            return Optional.of((Element) node);
        } else {
            if (!node.childNodes().isEmpty()) {
                for (Node childNode : node.childNodes()) {
                    Optional<Element> found = findElementById(id, childNode);
                    if (found.isPresent()) {
                        return found;
                    }
                }
            }
            return Optional.empty();
        }
    }

    private static <T> Optional<HtmlElementAwareComponent> revive(Element rawElement, Component relativeComponent) {
        String id = rawElement.attr(ID);
        Optional<Class<Component>> componentClass = ComponentTagRegistry.getInstance().getComponentClassByTag(rawElement.tagName());
        if (componentClass.isPresent()) {
            Component component = createVaadinComponent(id, rawElement, relativeComponent, componentClass.get());
            HtmlElementAwareComponent htmlElementAwareComponent = new HtmlElementAwareComponent(component, rawElement);
            getComponentMap().put(id, htmlElementAwareComponent);
            return Optional.of(htmlElementAwareComponent);
        }
        throw new UnsupportedElementException("Unsupported element (" + id + "): " + rawElement.tagName());
    }

    private static Component createVaadinComponent(String id, Element rawElement, Component relativeComponent, Class<? extends Component> componentClass) {
        com.vaadin.flow.dom.Element vaadinElement = new com.vaadin.flow.dom.Element(rawElement.tagName());
        vaadinElement.attachShadow();
        Component component = Component.from(vaadinElement, componentClass);
        component.setId(id);
        if (relativeComponent.getParent().isPresent()) {
            Component domBindComponent = relativeComponent.getParent().get();
            createSelfRemoveDomChangeEventListener(id, domBindComponent.getElement(), vaadinElement);
            return component;
        }
        throw new InternalResolverException("Cannot create component, domBind not found");
    }

    private static void createSelfRemoveDomChangeEventListener(String id,
                                                               com.vaadin.flow.dom.Element domBindElement,
                                                               com.vaadin.flow.dom.Element componentElement) {

        Mutable<DomEventListener> domEventListener = new MutableObject<>();
        DomListenerRegistration domListenerRegistration = domBindElement.addEventListener("dom-change",
            event -> {
                if (domEventListener.getValue() != null) {
                    domEventListener.getValue().handleEvent(event);
                }
            });

        domEventListener.setValue(event -> {
            domBindElement.getNode()
                .getFeature(VirtualChildrenList.class)
                .append(componentElement.getNode(), NodeProperties.INJECT_BY_ID, id);
            domListenerRegistration.remove();
            domEventListener.setValue(null);
        });
    }

}
