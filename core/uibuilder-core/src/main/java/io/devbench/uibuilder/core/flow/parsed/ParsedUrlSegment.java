/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.parsed;

import io.devbench.uibuilder.core.flow.exceptions.AlmostFinalValueModifiedAgainException;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;


@Getter
public class ParsedUrlSegment extends ParsedFlowElement {

    @NotNull
    private final String navigationTarget;

    @NotNull
    private final String htmlPath;

    private String route;

    public ParsedUrlSegment(
        @NotNull String id,
        @NotNull Supplier<Optional<String>> alternativeFlowId,
        @NotNull String navigationTarget,
        @NotNull String htmlPath
    ) {
        super(id, alternativeFlowId);
        this.navigationTarget = Objects.requireNonNull(navigationTarget);
        this.htmlPath = Objects.requireNonNull(htmlPath);
        this.route = null;
    }

    public void setRoute(String route) {
        if (this.route == null) {
            synchronized (this) {
                if (this.route == null) {
                    this.route = Objects.requireNonNull(route);
                    return;
                }
            }
        }
        throw new AlmostFinalValueModifiedAgainException("The `route` attribute of the flow [" + getId() + "] is already set.");
    }

    @NotNull
    public String getRoute() {
        return Objects.requireNonNull(route);
    }
}
