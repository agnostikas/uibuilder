/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import io.devbench.uibuilder.core.exceptions.StateNodeSyntheticPropertyException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;


public class TopLevelSyntheticProperty implements AbstractPropertyMetadata<Object> {

    private final String name;
    private final Object value;
    private final ClassMetadata classMetadata;

    public TopLevelSyntheticProperty(String name, ClassMetadata classMetadata) {
        if (name == null) {
            throw new StateNodeSyntheticPropertyException("Top level property cannot be instantiated with null value or name");
        }
        this.name = name;
        this.classMetadata = classMetadata;
        this.value = classMetadata.getInstance();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <VALUE> VALUE getValue() {
        return (VALUE) value;
    }

    @Override
    public <VALUE> void setValue(VALUE value) {
        throw new StateNodeSyntheticPropertyException("Top level property value cannot be changed");
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> ClassMetadata<T> typeMeta() {
        return classMetadata;
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return false;
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
        return null;
    }

    @Override
    public AbstractPropertyMetadata<Object> clone(Object instance) {
        return null;
    }

    @Override
    public Object getInstance() {
        return new Object();
    }

    @Override
    public void setInstance(Object instance) {
        throw new StateNodeSyntheticPropertyException("Cannot set instance on top level property");
    }

    @Override
    public Class<?> getType() {
        return classMetadata.getTargetClass();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ParameterizedType getParameterizedType() {
        return null;
    }

    @Override
    public boolean isSynthetic() {
        return true;
    }
}
