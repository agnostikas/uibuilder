/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.helger.commons.io.resource.ClassPathResource;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.page.exceptions.InvalidDocumentException;
import org.jsoup.Jsoup;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class ReadDocumentToContextProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        InputStream htmlInputStream = new ClassPathResource(context.getHtmlPath()).getInputStream();
        if (htmlInputStream == null) {
            throw new InvalidDocumentException(context.getHtmlPath());
        }
        try {
            context.setDocument(Jsoup.parse(htmlInputStream, StandardCharsets.UTF_8.name(), ""));
            context.getDocument().outputSettings().indentAmount(0).prettyPrint(false);
            context
                .getDocument().getElementsByAttribute("contextId")
                .stream()
                .map(element -> element.attr("contextId"))
                .filter(Objects::nonNull)
                .filter(attr -> !attr.isEmpty())
                .forEach(attr -> context.getPageContextIds().add(attr));
        } catch (Exception e) {
            throw new InvalidDocumentException(context.getHtmlPath(), e);
        }
    }

}
