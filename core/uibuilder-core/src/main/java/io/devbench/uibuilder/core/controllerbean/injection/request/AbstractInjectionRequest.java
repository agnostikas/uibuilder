/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.request;

import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Data;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.flow.FlowParameter;
import io.devbench.uibuilder.core.flow.FlowParameterProvider;
import io.devbench.uibuilder.core.flow.QueryParameter;
import io.devbench.uibuilder.core.utils.LazyInitialized;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;
import static io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils.*;

abstract class AbstractInjectionRequest implements InjectionRequest {

    @NotNull
    private final Method method;
    private final LazyInitialized<Boolean> valid;
    private final LazyInitialized<List<Map.Entry<Parameter, Object>>> parameterToValue;


    AbstractInjectionRequest(@NotNull Method method) {
        this.method = Objects.requireNonNull(method);
        this.parameterToValue = new LazyInitialized<>(this::getParametersToValues);
        this.valid = new LazyInitialized<>(this::allParameterValueValid);
    }

    private List<Map.Entry<Parameter, Object>> getParametersToValues() {
        return Arrays.stream(method.getParameters())
            .map(this::findParameterByAnnotation)
            .collect(Collectors.toList());
    }

    private Map.Entry<Parameter, Object> findParameterByAnnotation(Parameter parameter) {
        return Pair.of(parameter, getValueByParameter(parameter));
    }

    @Nullable
    protected Object getValueByParameter(Parameter parameter) {
        if (parameter.isAnnotationPresent(UIComponent.class)) {
            return getInjectionPointValueForParameter(method, parameter, ControllerBeanManager.getInstance().getAllCachedComponentInjectionPoint());
        } else if (parameter.isAnnotationPresent(FlowParameter.class)) {
            FlowParameter urlParameter = parameter.getAnnotation(FlowParameter.class);
            return FlowParameterProvider.getInstance().getUrlParameter(urlParameter.value()).orElse(null);
        }else if (parameter.isAnnotationPresent(QueryParameter.class)) {
            return getQueryParameterValueByParameter(parameter);
        } else {
            return null;
        }
    }

    @Nullable
    private Object getQueryParameterValueByParameter(Parameter parameter) {
        QueryParameter annotation = parameter.getAnnotation(QueryParameter.class);
        List<String> queryParameters = FlowParameterProvider.getInstance().getQueryParameters(annotation.value());
        if (Collection.class.isAssignableFrom(parameter.getType())) {
            return queryParameters;
        } else if (!queryParameters.isEmpty()){
            return queryParameters.get(queryParameters.size() - 1);
        } else {
            return null;
        }
    }

    private boolean checkMethodLevelNonNullRequirementForParameters() {
        if (method.isAnnotationPresent(CallOnNonNull.class)) {
            return parameterToValue.get().stream().allMatch(paramValueEntry -> paramValueEntry.getValue() != null);
        }
        return true;
    }

    private boolean allParameterValueValid() {
        return checkMethodLevelNonNullRequirementForParameters() &&
            parameterToValue.get().stream()
                .allMatch(paramValueEntry -> INJECTION_ANNOTATION_VALIDATORS.stream()
                    .allMatch(injectionAnnotationValidator ->
                        injectionAnnotationValidator.test(paramValueEntry.getKey().getAnnotations(), paramValueEntry.getValue())));
    }

    public final Object[] getParameters() {
        return parameterToValue.get().stream().map(Map.Entry::getValue).toArray();
    }

    @Override
    public final boolean isValid() {
        return valid.get();
    }

    protected Optional<String> findClientParameterName(@NotNull Parameter parameter) {
        String parameterName = null;
        if (parameter.isAnnotationPresent(Item.class)) {
            parameterName = "item";
        } else if (parameter.isAnnotationPresent(Value.class)) {
            parameterName = parameter.getAnnotation(Value.class).value();
        } else if (parameter.isAnnotationPresent(Data.class)) {
            parameterName = EVENT_DATASET_PREFIX + parameter.getAnnotation(Data.class).value();
        }
        return Optional.ofNullable(parameterName);
    }
}
