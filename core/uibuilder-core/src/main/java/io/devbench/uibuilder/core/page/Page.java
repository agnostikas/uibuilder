/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.internal.ReflectTools;
import io.devbench.uibuilder.api.exceptions.ProgressInterruptException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedTheme;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.FlowTarget;
import io.devbench.uibuilder.core.flow.exceptions.InterruptFlowNavigationException;
import io.devbench.uibuilder.i18n.core.I;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Tag(Page.TAG_NAME)
@NpmPackage(value = "moment-timezone", version = "0.5.33")
@NpmPackage(value = "moment", version = "2.24.0")
@JsModule("./uibuilder-page/uibuilder-page.js")
public class Page extends Component implements HasComponents, HasStyle, FlowTarget, LocaleChangeObserver {

    public static final String TAG_NAME = "uibuilder-page";

    private StateNodeManager parentStateNodeManager;
    private DomBind domBind;
    private PageLoader pageLoader;
    private Locale lastLocale;

    public Page() {
        addListener(UserTimeZoneUpload.class, Page::onTimeZoneUpload);
    }

    private static void onTimeZoneUpload(UserTimeZoneUpload timeZoneUpload) {
        ZoneId zoneId = ZoneId.of(timeZoneUpload.getTimeZoneId());
        I.setTimeZoneProvider(() -> zoneId);
    }

    public void loadPage(String path) {
        onRootPage(this::importFallback);
        try {
            tryToUnloadContent();

            lastLocale = I.getActiveLocale();
            domBind = new DomBind();
            this.add(domBind);

            tryToLoadHtml(path);
        } catch (ProgressInterruptException ex) {
            log.warn("Page loading progress interrupted, with the following exception message: " + ex.getMessage());
            throw new InterruptFlowNavigationException(ex);
        }
    }

    public void processFragment(String htmlFragment, Component parentComponent, List<String> additionalBindings) {
        pageLoader.loadFragment(htmlFragment, parentComponent, additionalBindings);
    }

    @Override
    public String getComponentId() {
        if (!getId().isPresent()) {
            setId(UUID.randomUUID().toString());
        }
        return getId().get();
    }

    @Override
    public void loadContent(String htmlPath) {
        loadPage(htmlPath);
    }

    @Override
    public void unloadContent() {
        try {
            tryToUnloadContent();
        } catch (ProgressInterruptException ex) {
            log.warn("Page unloading progress interrupted, with the following exception message: " + ex.getMessage());
            throw new InterruptFlowNavigationException(ex);
        }
    }

    private Optional<Page> findPage(@NotNull PageLoader pageLoader) {
        PageLoaderContext context = pageLoader.getContext();
        if (context != null) {
            DomBind domBind = context.getDomBind();
            if (domBind != null) {
                return domBind.getParent()
                    .filter(Page.class::isInstance)
                    .map(Page.class::cast);
            }
        }
        return Optional.empty();
    }

    private String createJsImportPromise() {
        return ReflectTools.createInstance(ConfigurationBasedTheme.class)
            .getThemeJsImports()
            .stream()
            .map(jsImport -> ".then(() => import('" + jsImport + "'))")
            .collect(Collectors.joining());
    }

    private void importFallback() {
        UI currentUI = UI.getCurrent();
        String appId = currentUI.getInternals().getAppId();
        String themeJsImports = createJsImportPromise();

        currentUI.getPage().addDynamicImport(
            "var fallback = window.Vaadin.Flow.fallbacks ? window.Vaadin.Flow.fallbacks['" + appId + "'] : null;" +
                "if (fallback && fallback.loadFallback) {" +
                "   return fallback.loadFallback()" + themeJsImports + ";" +
                "} else {" +
                "   return Promise.resolve(0)" + themeJsImports + ";" +
                "}");
    }

    private void onRootPage(Runnable runnable) {
        if (!getParent()
            .filter(DomBind.class::isInstance)
            .flatMap(Component::getParent)
            .filter(Page.class::isInstance)
            .isPresent()) {
            runnable.run();
        }
    }

    private Optional<Page> findParentPage(@NotNull Page page) {
        return page.getParent()
            .filter(DomBind.class::isInstance)
            .flatMap(Component::getParent)
            .filter(Page.class::isInstance)
            .map(Page.class::cast);
    }

    private void unloadChildren() {
        new ArrayList<>(FlowManager.getCurrent().getPageLoaders())
            .forEach(loader -> findPage(loader)
                .flatMap(this::findParentPage)
                .ifPresent(parentPage -> {
                    if (parentPage == this) {
                        loader.unload();
                    }
                }));
    }

    private void tryToUnloadContent() {
        unloadChildren();

        if (pageLoader != null) {
            pageLoader.unload();
        }
        if (domBind != null) {
            remove(domBind);
            domBind = null;
        }
    }

    private void tryToLoadHtml(String htmlPath) {
        pageLoader = new PageLoader(htmlPath, domBind, parentStateNodeManager);
        pageLoader.load();
    }

    public StateNodeManager getStateNodeManager() {
        return pageLoader.getContext().getStateNodeManager();
    }

    @Override
    public void localeChange(LocaleChangeEvent event) {
        if (Objects.equals(lastLocale, event.getLocale())) {
            return;
        }

        if (pageLoader != null) {
            ControllerBeanManager.getInstance().callLocaleChangeObservers(event);
            lastLocale = event.getLocale();
            FlowManager.getCurrent().reload();
        }
    }

    public void setParentStateNodeManager(StateNodeManager parentStateNodeManager) {
        this.parentStateNodeManager = parentStateNodeManager;
    }

    @DomEvent("user-timezone-upload")
    public static class UserTimeZoneUpload extends ComponentEvent<Page> {

        private final String timeZoneId;

        public UserTimeZoneUpload(
            Page source,
            boolean fromClient,
            @EventData("event.detail.value") String timeZoneId
        ) {
            super(source, fromClient);
            this.timeZoneId = timeZoneId;
        }

        public String getTimeZoneId() {
            return timeZoneId;
        }

    }

    public static Optional<Page> findComponentParentPage(Component component) {
        Component currentComponent = component;
        while (currentComponent != null && !(currentComponent instanceof Page)) {
            currentComponent = currentComponent.getParent().orElse(null);
        }
        return Optional.ofNullable((Page) currentComponent);
    }
}
