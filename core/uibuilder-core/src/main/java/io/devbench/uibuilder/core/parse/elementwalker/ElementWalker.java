/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.parse.elementwalker;

import io.devbench.uibuilder.api.parse.backendtemplate.TemplateParser;
import io.devbench.uibuilder.api.parse.elementwalker.ElementProcessor;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;


public class ElementWalker {

    private final Element root;

    private final List<ElementProcessor> processors;


    private ElementWalker(Element root) {
        this.root = root;
        this.processors = new ArrayList<>();
    }


    public static ElementWalker of(Element root) {
        return new ElementWalker(root);
    }

    public ElementWalker withProcessor(ElementProcessor processor) {
        processors.add(processor);
        return this;
    }

    public void walk() {
        processElement(root);
    }

    private void processElement(Element element) {
        Elements children = element.children();
        for (ElementProcessor elementProcessor : processors) {
            Elements replacedElements = elementProcessor.process(element);
            if (TemplateParser.isElementsModified(replacedElements, element)) {
                int index = element.siblingIndex();
                Element parent = element.parent();
                for (int i = 0; i < replacedElements.size(); i++) {
                    Element replacementElement = replacedElements.get(i);
                    parent.insertChildren(index + i, replacementElement);
                }
                element.remove();
                children = replacedElements;
                break;
            }
        }
        children.forEach(this::processElement);
    }

}
