/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.templateparser;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.api.parse.backendtemplate.TemplateParser;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.MethodMetadata;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MethodReferenceParser implements TemplateParser {

    private static final Pattern METHOD_REFERENCE_PATTERN = Pattern.compile("\\{\\{(\\w+)::(\\w+)}}");

    @Override
    public boolean canProcess(Element element) {
        return true;
    }

    @Override
    public Elements processTemplate(Element element) {
        ControllerBeanManager controllerBeanManager = ControllerBeanManager.getInstance();
        for (Attribute attribute : element.attributes()) {
            if (attribute.getValue() == null) {
                continue;
            }
            Matcher matcher = METHOD_REFERENCE_PATTERN.matcher(attribute.getValue());
            if (matcher.matches()) {
                String controllerBeanName = matcher.group(1);
                String methodName = matcher.group(2);
                Class<?> controllerBeanClazz = controllerBeanManager.getBeanMetadata(controllerBeanName).getTargetClass();
                Object controllerBean = controllerBeanManager.getControllerBean(controllerBeanName);
                processAttribute(
                    element,
                    attribute.getKey(),
                    new UIBuilderMethodReference(controllerBeanClazz, controllerBean, getMethodHandlesWithName(controllerBeanClazz, methodName))
                );
            }
        }
        return new Elements(element);
    }

    private Map<MethodType, MethodHandle> getMethodHandlesWithName(Class<?> controllerBeanClazz, String methodName) {
        return ClassMetadata
            .ofClass(controllerBeanClazz)
            .getMethods()
            .stream()
            .filter(it -> Objects.equals(it.getName(), methodName))
            .map(this::tryToUnreflectMethod)
            .collect(Collectors.toMap(
                MethodHandle::type,
                Function.identity()
            ));
    }

    @NotNull
    private MethodHandle tryToUnreflectMethod(MethodMetadata<?> methodMetadata) {
        try {
            return methodMetadata.unreflect();
        } catch (IllegalAccessException e) {
            throw new IllegalReflectiveAccessException(e);
        }
    }

    private void processAttribute(Element element, String key, UIBuilderMethodReference value) {
        final String id = UUID.randomUUID().toString();
        VaadinSession.getCurrent().setAttribute(id, value);
        element.attr(key, id);
    }

}

