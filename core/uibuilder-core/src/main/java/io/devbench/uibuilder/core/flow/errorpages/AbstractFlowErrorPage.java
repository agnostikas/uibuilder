/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.errorpages;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.HasErrorParameter;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.flow.FlowTarget;

import java.util.function.Function;

public abstract class AbstractFlowErrorPage<T extends Exception> extends Div implements HasErrorParameter<T> {

    private final FlowManager flowManager;
    private final int errorCode;
    private final Function<FlowManager, String> errorPagePathSupplier;

    protected AbstractFlowErrorPage(int errorCode, Function<FlowManager, String> errorPagePathSupplier) {
        this.errorCode = errorCode;
        this.errorPagePathSupplier = errorPagePathSupplier;
        this.flowManager = FlowManager.getCurrent();
        add((Component) flowManager.getRegisteredFlowTargets().get("UI"));
    }

    @Override
    public int setErrorParameter(BeforeEnterEvent event, ErrorParameter<T> parameter) {
        loadErrorPage(errorPagePathSupplier.apply(flowManager));
        return errorCode;
    }

    private void loadErrorPage(String htmlPath) {
        FlowTarget uiFlowTarget = flowManager.getRegisteredFlowTargets().get("UI");
        uiFlowTarget.loadContent(htmlPath);
    }

}
