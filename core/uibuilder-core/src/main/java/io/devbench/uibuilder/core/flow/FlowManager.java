/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.helger.commons.io.resource.ClassPathResource;
import com.vaadin.flow.component.UI;
import io.devbench.uibuilder.core.flow.exceptions.FlowDefinitionNotFound;
import io.devbench.uibuilder.core.flow.exceptions.FlowTargetNotExistsException;
import io.devbench.uibuilder.core.flow.exceptions.InterruptFlowNavigationException;
import io.devbench.uibuilder.core.flow.parsed.ParsedFlowElement;
import io.devbench.uibuilder.core.flow.parsed.ParsedUrlSegment;
import io.devbench.uibuilder.core.flow.xml.*;
import io.devbench.uibuilder.core.page.PageLoader;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.core.session.context.UIContextStored;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@UIContextStored
public final class FlowManager {

    public static final Pattern FLOW_PATTERN = Pattern.compile("flow:([A-Za-z][A-Za-z0-9]*)");

    private static Collection<Class<?>> contextClasses = new HashSet<>();

    static {
        contextClasses.addAll(Arrays.asList(
            FlowElement.class, FlowDefinition.class, FlowDefinitions.class, FlowInclude.class
        ));
    }

    @Getter
    private final FlowDefinitions flowDefinitions;
    private final Map<String, FlowTarget> registeredFlowTargets = new HashMap<>();

    private final Map<Pattern, List<ParsedFlowElement>> flowTargetsByUrl;
    private final Map<String, ParsedFlowElement> flowTargetsById;
    private final Map<String, PageLoader> pageLoaders = new HashMap<>();

    private AtomicReference<List<ParsedFlowElement>> currentUrlSegments = new AtomicReference<>();

    public FlowManager(String flowDefinitionPath) {
        setCurrent();
        this.flowDefinitions = loadFlowDefinition(flowDefinitionPath);
        flowTargetsByUrl = Collections.unmodifiableMap(parseFlowTargetsByUrl(flowDefinitions));
        flowTargetsById = Collections.unmodifiableMap(
            flowTargetsByUrl
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toMap(ParsedFlowElement::getId, Function.identity()))
        );
    }

    public static void registerClassesToContext(Class<?>... contextClasses) {
        FlowManager.contextClasses.addAll(Arrays.asList(contextClasses));
    }

    public static FlowManager getCurrent() {
        return UIContext.getContext().get(FlowManager.class);
    }

    private Map<Pattern, List<ParsedFlowElement>> parseFlowTargetsByUrl(FlowDefinitions flowDefinitions) {
        Map<String, List<ParsedFlowElement>> collector = new HashMap<>();
        parseFlowDefinitions(flowDefinitions, null, flowDefinitions.getRoute(), collector);
        return collector
            .entrySet()
            .stream()
            .flatMap(pair -> pair.getValue().stream().map(value -> Pair.of(pair.getKey(), value)))
            .map(entry -> Pair.of(createPatternBasedOnUrlPath(entry.getKey(), entry.getValue()), entry.getValue()))
            .collect(Collectors.toMap(Map.Entry::getKey, e -> Collections.singletonList(e.getValue())));
    }

    private void setCurrent() {
        UIContext.getContext().addToActiveUIContextAs(FlowManager.class, this);
    }

    private FlowDefinitions loadFlowDefinition(String flowDefinitionPath) {
        InputStream flowInputStream = new ClassPathResource(flowDefinitionPath).getInputStream();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(contextClasses.toArray(new Class[0]));
            return (FlowDefinitions) jaxbContext.createUnmarshaller().unmarshal(flowInputStream);
        } catch (JAXBException | ClassCastException | NullPointerException | IllegalArgumentException e) {
            throw new IllegalArgumentException("Cannot load flow definition from: " + flowDefinitionPath, e);
        }
    }

    private void parseFlowDefinitions(
        @NotNull FlowElement flowElement,
        @Nullable ParsedFlowElement parentUrlSegment,
        @NotNull String parentUrlPath,
        @NotNull Map<String, List<ParsedFlowElement>> collector
    ) {
        if (flowElement instanceof FlowInclude) {
            FlowInclude flowInclude = (FlowInclude) flowElement;
            FlowDefinitions includedFlowDefinitions = loadFlowDefinition(flowInclude.getPath());
            includedFlowDefinitions.getSubFlows().forEach(it -> it.setTarget(flowInclude.getTarget()));
            String urlPath = createCorrectUrlPath(parentUrlPath, flowInclude.getRoute());
            includedFlowDefinitions.getSubFlows().forEach(it -> parseFlowDefinitions(it, parentUrlSegment, urlPath, collector));

            Optional<Pair<String, ParsedFlowElement>> urlSegmentWithPattern =
                getRootUrlSegmentWithPatternForInclude(flowInclude.getId(), includedFlowDefinitions, urlPath, parentUrlSegment);
            if (urlSegmentWithPattern.isPresent()) {
                putUrlSegmentWithPatternToCollector(collector, urlSegmentWithPattern.get());
                parseSubFlowDefinitions(flowElement, collector, urlSegmentWithPattern.get());
            }
        } else {
            Pair<String, ParsedFlowElement> urlSegmentWithPattern = getUrlSegmentWithPattern(flowElement, parentUrlPath, parentUrlSegment);
            putUrlSegmentWithPatternToCollector(collector, urlSegmentWithPattern);
            parseSubFlowDefinitions(flowElement, collector, urlSegmentWithPattern);
        }

    }

    private Optional<Pair<String, ParsedFlowElement>> getRootUrlSegmentWithPatternForInclude(
        String id,
        FlowDefinitions includedFlowDefinitions,
        String parentUrlPath,
        ParsedFlowElement parentUrlSegment
    ) {
        FlowDefinition rootFlowElement = findRootFlowDefinition(includedFlowDefinitions);
        if (rootFlowElement == null) {
            return Optional.empty();
        } else {
            Pair<String, ParsedFlowElement> urlSegmentWithPattern = getUrlSegmentWithPattern(rootFlowElement, parentUrlPath, parentUrlSegment);
            urlSegmentWithPattern.getRight().setId(Optional.ofNullable(id).orElseGet(() -> UUID.randomUUID().toString()));
            return Optional.of(urlSegmentWithPattern);
        }
    }

    @Nullable
    private FlowDefinition findRootFlowDefinition(HasSubFlow hasSubFlow) {
        return hasSubFlow
            .getSubFlows()
            .stream()
            .filter(FlowDefinition.class::isInstance)
            .map(FlowDefinition.class::cast)
            .filter(it -> Objects.equals(it.getRoute(), ""))
            .map(rootFlowDefinition -> {
                FlowDefinition subRoot = findRootFlowDefinition(rootFlowDefinition);
                if (subRoot != null) {
                    return subRoot;
                }
                return rootFlowDefinition;
            })
            .findFirst()
            .orElse(null);
    }

    private void parseSubFlowDefinitions(
        @NotNull FlowElement flowElement,
        @NotNull Map<String, List<ParsedFlowElement>> collector,
        Pair<String, ParsedFlowElement> urlSegmentWithPattern
    ) {
        if (flowElement instanceof HasSubFlow) {
            HasSubFlow hasSubFlow = (HasSubFlow) flowElement;
            if (flowElement instanceof FlowTargetInherited) {
                ((FlowTargetInherited) flowElement).setInheritedTargetOnEachChildIfNecessary();
            }
            hasSubFlow.getSubFlows().forEach(it -> parseFlowDefinitions(it, urlSegmentWithPattern.getRight(), urlSegmentWithPattern.getLeft(), collector));
        }
    }

    private void putUrlSegmentWithPatternToCollector(
        @NotNull Map<String, List<ParsedFlowElement>> collector,
        Pair<String, ParsedFlowElement> urlSegmentWithPattern
    ) {
        if (urlSegmentWithPattern.getRight() != null && StringUtils.isNotBlank(urlSegmentWithPattern.getLeft())) {
            collector.putIfAbsent(urlSegmentWithPattern.getLeft(), new ArrayList<>());
            collector.get(urlSegmentWithPattern.getLeft()).add(urlSegmentWithPattern.getRight());
        }
    }

    private Pattern createPatternBasedOnUrlPath(String route, ParsedFlowElement flowElement) {
        StringBuilder urlPattern = new StringBuilder();
        urlPattern.append("^");
        int parameterIndex = 0;
        for (int i = 0; i < route.length(); i++) {
            if (route.charAt(i) == '{') {
                String parameterName = getParameterNameFromIndex(route, i);
                i += parameterName.length() + 1;
                flowElement.defineParameter(parameterName, parameterIndex++);
                urlPattern.append("([^/]+?)");
            } else {
                urlPattern.append(route.charAt(i));
            }
        }
        urlPattern.append("$");
        return Pattern.compile(urlPattern.toString());
    }

    private String getParameterNameFromIndex(String route, int index) {
        StringBuilder parameterName = new StringBuilder();
        while (route.charAt(index) != '}') {
            parameterName.append(route.charAt(index));
            index++;
        }
        return parameterName.deleteCharAt(0).toString();
    }

    private Pair<String, ParsedFlowElement> getUrlSegmentWithPattern(FlowElement flowElement, String parentUrlPath, ParsedFlowElement parentUrlSegment) {
        if (flowElement instanceof FlowDefinition) {
            FlowDefinition flowDefinition = (FlowDefinition) flowElement;
            parentUrlPath = createCorrectUrlPath(parentUrlPath, flowDefinition.getRoute());
            if (flowDefinition.getHtml() != null) {
                ParsedFlowElement currentUrlSegment = flowElement.convertToParsedElement();
                if (currentUrlSegment instanceof ParsedUrlSegment) {
                    ((ParsedUrlSegment) currentUrlSegment).setRoute(parentUrlPath);
                }
                currentUrlSegment.setParentElement(parentUrlSegment);
                return Pair.of(
                    parentUrlPath,
                    currentUrlSegment
                );
            }
        }
        ParsedFlowElement currentUrlSegment = flowElement.convertToParsedElement();
        currentUrlSegment.setParentElement(parentUrlSegment);
        return Pair.of(parentUrlPath, currentUrlSegment);
    }

    private String createCorrectUrlPath(String parentUrlPath, String route) {
        StringBuilder sb = new StringBuilder();
        sb.append(parentUrlPath);
        if (!parentUrlPath.endsWith("/")) {
            sb.append('/');
        }
        if (!route.isEmpty() && route.charAt(0) == '/') {
            sb.append(route.substring(1));
        } else {
            sb.append(route);
        }

        if (sb.length() > 1 && sb.lastIndexOf("/") == sb.length() - 1) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }

    public void registerPageLoader(@NotNull String htmlPath, @NotNull PageLoader pageLoader) {
        PageLoader previousPageLoader = pageLoaders.put(htmlPath, pageLoader);
        if (previousPageLoader != null) {
            previousPageLoader.unload();
        }
    }

    public Collection<PageLoader> getPageLoaders() {
        return Collections.unmodifiableCollection(pageLoaders.values());
    }

    public boolean isPageContextActive(@NotNull String contextId) {
        return pageLoaders.values().stream().anyMatch(pageLoader -> pageLoader.getPageContextIds().contains(contextId));
    }

    public Optional<PageLoader> findPageLoader(@NotNull String htmlPath) {
        return Optional.ofNullable(pageLoaders.get(htmlPath));
    }

    public void reload() {
        getCurrentUrlSegments()
            .stream()
            .filter(ParsedUrlSegment.class::isInstance)
            .map(ParsedUrlSegment.class::cast)
            .forEach(parsedUrlSegment -> {
                PageLoader pageLoader = pageLoaders.get(parsedUrlSegment.getHtmlPath());
                if (pageLoader != null) {
                    pageLoader.unload();
                }
                registeredFlowTargets.get(parsedUrlSegment.getNavigationTarget()).loadContent(parsedUrlSegment.getHtmlPath());
            });
    }

    private List<ParsedFlowElement> getCurrentUrlSegments() {
        currentUrlSegments.compareAndSet(null, new ArrayList<>());
        return currentUrlSegments.get();
    }

    /**
     * @deprecated Use the {@link NavigationBuilder} instead of this method
     */
    @Deprecated
    public void navigate(String flowId, String... parameters) {
        ParsedUrlSegment urlSegment = (ParsedUrlSegment) flowTargetsById.get(flowId);
        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        Map<String, String> oldParameters = flowParameterProvider.getAllUrlParameters();
        flowParameterProvider.clear();
        for (int i = 0; i < parameters.length; i++) {
            flowParameterProvider.setUrlParameter(urlSegment.getParameterForIndex(i), parameters[i]);
        }
        Set<String> changedParameterNames = calculateChangedParameterNames(oldParameters, flowParameterProvider.getAllUrlParameters());
        navigate(flowId, changedParameterNames);
    }

    private Set<String> calculateChangedParameterNames(Map<String, String> oldParameters, Map<String, String> newParameters) {
        Set<String> retSet = new HashSet<>();
        oldParameters
            .entrySet()
            .stream()
            .filter(it -> !Objects.equals(it.getValue(), newParameters.get(it.getKey())))
            .map(Map.Entry::getKey)
            .forEach(retSet::add);

        newParameters
            .entrySet()
            .stream()
            .filter(it -> !Objects.equals(it.getValue(), oldParameters.get(it.getKey())))
            .map(Map.Entry::getKey)
            .forEach(retSet::add);

        return retSet;
    }

    public void navigate(String flowId) {
        navigate(flowId, false);
    }

    public void navigate(String flowId, boolean clearQueryParameters) {
        List<ParsedFlowElement> urlSegmentsAtStart = getCurrentUrlSegments();
        List<ParsedFlowElement> pathToIdUrlSegments = findBestMatchingUrlSegmentsByFlowId(flowId);
        Set<String> parametersAtStart = urlSegmentsAtStart.stream().flatMap(ParsedFlowElement::getParameterNames).collect(Collectors.toSet());
        Set<String> parametersAtTarget = pathToIdUrlSegments.stream().flatMap(ParsedFlowElement::getParameterNames).collect(Collectors.toSet());
        Set<String> difference = Sets.difference(parametersAtStart, parametersAtTarget);
        FlowParameterProvider.getInstance().removeParameters((key, value) -> difference.contains(key));
        if (clearQueryParameters) {
            FlowParameterProvider.getInstance().clearQueryParameters();
        }
        navigate(flowId, Collections.emptySet());
    }

    private void navigate(String flowId, Set<String> changedParameterNames) {
        List<ParsedFlowElement> urlSegmentsAtStart = getCurrentUrlSegments();
        List<ParsedFlowElement> pathToIdUrlSegments = findBestMatchingUrlSegmentsByFlowId(flowId);
        List<Pair<ParsedUrlSegment, ParsedUrlSegment>> difference = calculateDifferenceToFlowId(pathToIdUrlSegments, changedParameterNames);

        if (!difference.isEmpty()) {
            try {
                tryToLoadContentBasedOnUrlSegments(difference);
                currentUrlSegments.compareAndSet(urlSegmentsAtStart, pathToIdUrlSegments);
                changeUrlBySegments((ParsedUrlSegment) flowTargetsById.get(flowId));
            } catch (InterruptFlowNavigationException ex) {
                log.debug("Flow navigation interrupted", ex);
            }
        }
    }

    private List<ParsedFlowElement> findBestMatchingUrlSegmentsByFlowId(String flowId) {
        return buildParsedUrlSegmentListWithParents(flowTargetsById.get(flowId));
    }

    private void changeUrlBySegments(ParsedUrlSegment pathToIdUrlSegments) {
        pushState(pathToIdUrlSegments.getRoute());
    }

    private void tryToLoadContentBasedOnUrlSegments(Iterable<? extends Pair<ParsedUrlSegment, ParsedUrlSegment>> difference) {
        for (Pair<ParsedUrlSegment, ParsedUrlSegment> segmentPair : difference) {
            ParsedUrlSegment left = segmentPair.getLeft();
            ParsedUrlSegment right = segmentPair.getRight();
            FlowTarget leftFlowTarget = null;
            if (left != null) {
                leftFlowTarget = registeredFlowTargets.get(left.getNavigationTarget());
            }
            if (right != null) {
                FlowTarget rightFlowTarget = getRegisteredFlowTargets().get(right.getNavigationTarget());
                if (leftFlowTarget != null && leftFlowTarget != rightFlowTarget) {
                    leftFlowTarget.unloadContent();
                }
                rightFlowTarget.loadContent(right.getHtmlPath());
            } else if (leftFlowTarget != null) {
                leftFlowTarget.unloadContent();
            }
        }
    }

    private List<Pair<ParsedUrlSegment, ParsedUrlSegment>> calculateDifferenceToFlowId(
        List<ParsedFlowElement> fullUrlSegmentsToId,
        Set<String> changedParameterNames
    ) {
        List<Pair<ParsedUrlSegment, ParsedUrlSegment>> list = new ArrayList<>();
        int bound = Math.max(fullUrlSegmentsToId.size(), getCurrentUrlSegments().size());
        for (int i = 0; i < bound; i++) {
            ParsedUrlSegment currentUrlSegmentAtIndex = getAtIndexOfType(i, getCurrentUrlSegments(), ParsedUrlSegment.class);
            ParsedUrlSegment toLoadUrlSegmentAtIndex = getAtIndexOfType(i, fullUrlSegmentsToId, ParsedUrlSegment.class);
            if (!list.isEmpty()) {
                list.add(Pair.of(currentUrlSegmentAtIndex, toLoadUrlSegmentAtIndex));
            } else if (!Objects.equals(currentUrlSegmentAtIndex, toLoadUrlSegmentAtIndex)) {
                list.add(Pair.of(currentUrlSegmentAtIndex, toLoadUrlSegmentAtIndex));
            } else if (urlSegmentContainsChangedParameter(currentUrlSegmentAtIndex, changedParameterNames)) {
                list.add(Pair.of(currentUrlSegmentAtIndex, toLoadUrlSegmentAtIndex));

            }
        }
        return list;
    }

    private boolean urlSegmentContainsChangedParameter(ParsedUrlSegment urlSegment, Set<String> changedParameterNames) {
        return changedParameterNames
            .stream()
            .map(parameterName -> "{" + parameterName + "}")
            .anyMatch(urlPart -> urlSegment.getRoute().contains(urlPart));
    }

    private <T> T getAtIndexOfType(int index, List<ParsedFlowElement> listToIndex, Class<T> type) {
        return listToIndex.stream().filter(type::isInstance).map(type::cast).skip(index).findFirst().orElse(null);
    }

    private void pushState(String location) {
        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        StringBuilder urlWithParameters = new StringBuilder();
        for (int i = 0; i < location.length(); i++) {
            if (i == 0 && location.charAt(i) == '/') {
                continue;
            }
            if (location.charAt(i) == '{') {
                String parameterName = getParameterNameFromIndex(location, i);
                i += parameterName.length() + 1;
                String parameterValue = flowParameterProvider
                    .getUrlParameter(parameterName)
                    .orElseThrow(NoSuchElementException::new);
                urlWithParameters.append(parameterValue);
            } else {
                urlWithParameters.append(location.charAt(i));
            }
        }
        if (!flowParameterProvider.getAllQueryParameters().isEmpty()) {
            String queryPart = flowParameterProvider
                .getAllQueryParameters()
                .entrySet()
                .stream()
                .flatMap(this::mapEntriesToKeyValuePairs)
                .collect(Collectors.joining("&", "?", ""));
            urlWithParameters.append(queryPart);
        }
        UI.getCurrent().getPage().getHistory().pushState(null, urlWithParameters.toString());
    }

    private Stream<String> mapEntriesToKeyValuePairs(Map.Entry<String, List<String>> entry) {
        return entry
            .getValue()
            .stream()
            .map(value -> entry.getKey() + "=" + value);
    }

    void handleUrlRequest(String relativeUrl) {
        handleUrlRequest(relativeUrl, Collections.emptyMap());
    }

    public void handleUrlRequest(String relativeUrl, Map<String, List<String>> queryParameters) {
        FlowParameterProvider.getInstance().clear();
        queryParameters.forEach(FlowParameterProvider.getInstance()::setQueryParameters);

        List<ParsedFlowElement> savedUrlSegments = getCurrentUrlSegments();

        if (!relativeUrl.startsWith("/")) {
            relativeUrl = "/" + relativeUrl;
        }

        List<ParsedFlowElement> parsedUrlSegments = findBestMatchingUrlSegmentsByUrl(relativeUrl);

        if (!parsedUrlSegments.isEmpty()) {
            try {
                tryToUnloadCurrentContent(savedUrlSegments);
                tryToLoadParsedUrlSegments(parsedUrlSegments);
                currentUrlSegments.compareAndSet(savedUrlSegments, parsedUrlSegments);
            } catch (InterruptFlowNavigationException ex) {
                log.debug("Flow navigation interrupted", ex);
            } finally {
                if (!getCurrentUrlSegments().isEmpty()) {
                    ParsedUrlSegment urlSegment = getCurrentUrlSegments()
                        .stream()
                        .filter(ParsedUrlSegment.class::isInstance)
                        .map(ParsedUrlSegment.class::cast)
                        .reduce((first, second) -> second)
                        .orElseThrow(NoSuchElementException::new);
                    changeUrlBySegments(urlSegment);
                }
            }
        } else {
            throw new FlowDefinitionNotFound();
        }
    }

    private List<ParsedFlowElement> findBestMatchingUrlSegmentsByUrl(String relativeUrl) {
        Optional<Pair<Matcher, ParsedUrlSegment>> matcherAndUrlSegmentPairs = flowTargetsByUrl
            .entrySet()
            .stream()
            .filter(entry -> entry.getValue().stream().anyMatch(ParsedUrlSegment.class::isInstance))
            .flatMap(entry -> entry.getValue().stream().map(innerEntry -> Pair.of(entry.getKey(), (ParsedUrlSegment) innerEntry)))
            .map(pair -> Pair.of(pair.getKey().matcher(relativeUrl), pair.getValue()))
            .filter(it -> it.getLeft().matches())
            .max(Comparator.comparingInt(element -> countElementsToRoot(element.getValue(), 0)));

        if (matcherAndUrlSegmentPairs.isPresent()) {
            ParsedFlowElement urlSegment = matcherAndUrlSegmentPairs.get().getRight();
            Matcher matcher = matcherAndUrlSegmentPairs.get().getLeft();
            for (int i = 0; i < matcher.groupCount(); i++) {
                String parameterName = urlSegment.getParameterForIndex(i);
                FlowParameterProvider.getInstance().setUrlParameter(parameterName, matcher.group(i + 1));
            }
            return buildParsedUrlSegmentListWithParents(urlSegment);
        } else {
            return Collections.emptyList();
        }
    }

    private int countElementsToRoot(@NotNull ParsedFlowElement element, final int elementDistanceFromLeaf) {
        if (element.getParentElement() == null) {
            return elementDistanceFromLeaf;
        } else {
            return countElementsToRoot(element.getParentElement(), elementDistanceFromLeaf + 1);
        }
    }


    private List<ParsedFlowElement> buildParsedUrlSegmentListWithParents(ParsedFlowElement urlSegment) {
        if (urlSegment == null) {
            return Collections.emptyList();
        } else if (urlSegment.calcAlternativeFlowId().isPresent()) {
            return findBestMatchingUrlSegmentsByFlowId(urlSegment.calcAlternativeFlowId().get());
        } else {
            List<ParsedFlowElement> retList = new ArrayList<>();
            while (urlSegment != null) {
                retList.add(urlSegment);
                urlSegment = urlSegment.getParentElement();
            }
            return Lists.reverse(retList);
        }
    }

    private void tryToUnloadCurrentContent(List<ParsedFlowElement> currentlyShownContentsUrlSegments) {
        Lists
            .reverse(currentlyShownContentsUrlSegments)
            .stream()
            .filter(ParsedUrlSegment.class::isInstance)
            .map(ParsedUrlSegment.class::cast)
            .forEach(segment -> registeredFlowTargets.get(segment.getNavigationTarget()).unloadContent());
    }

    private void tryToLoadParsedUrlSegments(List<ParsedFlowElement> parsedUrlSegments) {
        for (ParsedFlowElement parsedFlowElement : parsedUrlSegments) {
            if (parsedFlowElement instanceof ParsedUrlSegment) {
                ParsedUrlSegment parsedUrlSegment = (ParsedUrlSegment) parsedFlowElement;
                if (registeredFlowTargets.containsKey(parsedUrlSegment.getNavigationTarget())) {
                    registeredFlowTargets.get(parsedUrlSegment.getNavigationTarget()).loadContent(parsedUrlSegment.getHtmlPath());
                } else {
                    throw new FlowTargetNotExistsException(
                        "The target defined in flow does not exists, or haven't been registered, target: " + parsedUrlSegment.getNavigationTarget());
                }
            }
        }
    }

    public void registerFlowTarget(String id, FlowTarget flowTarget) {
        this.registeredFlowTargets.put(id, flowTarget);
    }

    public Map<String, FlowTarget> getRegisteredFlowTargets() {
        return Collections.unmodifiableMap(registeredFlowTargets);
    }

    void navigate(String flowTargetId, Map<String, String> urlParameters, Map<String, List<String>> queryParameters) {
        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        Map<String, String> oldUrlParameters = flowParameterProvider.getAllUrlParameters();
        flowParameterProvider.clear();
        urlParameters.forEach(flowParameterProvider::setUrlParameter);
        queryParameters.forEach(flowParameterProvider::setQueryParameters);
        Set<String> changedParameterNames = calculateChangedParameterNames(oldUrlParameters, flowParameterProvider.getAllUrlParameters());
        navigate(flowTargetId, changedParameterNames);
    }

    void unloadPageFromCurrentFlowManager(PageLoaderContext context) {
        pageLoaders.remove(context.getHtmlPath());
    }
}
