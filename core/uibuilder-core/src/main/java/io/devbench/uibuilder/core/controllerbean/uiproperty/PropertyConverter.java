/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import java.io.Serializable;

/**
 * DO NOT IMPLEMENT THIS! Implement one of the sub interfaces:
 * - {@link StringPropertyConverter} if the property can be converted to simple string format
 * - {@link JsonPropertyConverter} if the property is complex type that can only be converted to a structured json format
 *

 */
public interface PropertyConverter<PROPERTY_TYPE, JSON_OR_STRING extends Serializable> {
    JSON_OR_STRING convertTo(PROPERTY_TYPE value);

    PROPERTY_TYPE convertFrom(JSON_OR_STRING value);
}
