/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.UIEventHandlerContext;
import io.devbench.uibuilder.core.page.PageLoaderContext;

import java.util.Optional;

public interface EventProcessor {

    default Optional<UIEventHandlerContext> findUiEventHandlerContextForEventListenerProperty(PageLoaderContext context, String eventListenerPropertyName) {
        String afterLoadMethodName = context.getPageElement().attr(eventListenerPropertyName);
        return ControllerBeanManager.getInstance().getEventHandlerContext(afterLoadMethodName);
    }

    default void runEventHandlerNamedInEventListenerProperty(PageLoaderContext context, String eventListenerPropertyName) {
        findUiEventHandlerContextForEventListenerProperty(context, eventListenerPropertyName)
            .ifPresent(eventHandlerContext -> {
                eventHandlerContext.callEventHandlerWithItem(null);
            });
    }
}
