/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.BasicTypeValue;
import elemental.json.JsonValue;
import io.devbench.uibuilder.core.controllerbean.uiproperty.JsonPropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.PropertyConverters;
import io.devbench.uibuilder.core.exceptions.StateNodeInvalidReferenceException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


public class SimpleNode extends BindingNode {


    SimpleNode(String name, AbstractPropertyMetadata<? super Object> property, ClassMetadata<?> classMetadata, StateNodeManager manager) {
        super(name, property, classMetadata, manager);
    }

    @Override
    public Serializable populateValues() {
        if (isLeaf()) {
            if (null == property.getInstance()) {
                return null;
            } else {
                return PropertyConverters.getConverterFor(property).convertTo(property.getValue());
            }
        } else {
            throw new StateNodeInvalidReferenceException("Cannot reference simple property as a non leaf item in bindings");
        }
    }

    @Override
    protected Serializable populateCollectionValues() {
        if (isLeaf()) {
            stateNode = new StateNode(BasicTypeValue.class);
            BasicTypeValue valueHolder = stateNode.getFeature(BasicTypeValue.class);
            valueHolder.setValue(PropertyConverters.getConverterFor(property).convertTo(property.getValue()));
            manager.registerValueProvider(node -> (node != null && node.getId() == stateNode.getId()) ? valueHolder.getValue() : null);
            return stateNode;
        } else {
            throw new StateNodeInvalidReferenceException("Cannot reference simple property as a non leaf item in bindings");
        }
    }

    @Override
    public List<BindingNodeSyncError> synchronizeProperty(@NotNull String parentPropertyPath) {
        try {
            if (stateNode != null) {
                BasicTypeValue valueHolder = stateNode.getFeature(BasicTypeValue.class);
                PropertyConverter<?, ?> converter = PropertyConverters.getConverterFor(property);
                Serializable propertyValue = converter.convertTo(property.getValue());
                if (!Objects.equals(propertyValue, valueHolder.getValue())) {
                    updatePropertyValue(valueHolder.getValue(), converter);
                }
            }
            return Collections.emptyList();
        } catch (Exception e) {
            return Collections.singletonList(
                new BindingNodeSyncError(
                    this,
                    e,
                    calcPropertyPath(parentPropertyPath, this)
                )
            );
        }
    }

    public Optional<BindingNodeSyncError> synchronizePropertyWithValue(Serializable value, String parentPropertyPath) {
        try {
            PropertyConverter<?, ?> converter = PropertyConverters.getConverterFor(property);
            Serializable propertyValue = null;
            if (property.getInstance() != null) {
                propertyValue = converter.convertTo(property.getValue());
            }
            if (!Objects.equals(propertyValue, value)) {
                updatePropertyValue(value, converter);
            }
            return Optional.empty();
        } catch (Exception e) {
            return Optional.of(
                new BindingNodeSyncError(
                    this,
                    e,
                    calcPropertyPath(parentPropertyPath, this)
                )
            );
        }
    }

    private void updatePropertyValue(Serializable value, PropertyConverter<?, ?> converter) {
        if (null == value) {
            property.setValue(null);
            return;
        }
        if (converter instanceof JsonPropertyConverter) {
            property.setValue(((JsonPropertyConverter<?>) converter).convertFrom((JsonValue) value));
        } else {
            property.setValue(((PropertyConverter<?, Serializable>) converter).convertFrom(value));
        }

    }

    @Override
    public void synchronizeStateNode() {
        if (stateNode != null) {
            BasicTypeValue valueHolder = stateNode.getFeature(BasicTypeValue.class);
            PropertyConverter<?, ?> converter = PropertyConverters.getConverterFor(property);
            Serializable newValue = converter.convertTo(property.getValue());
            if (!Objects.equals(valueHolder.getValue(), newValue)) {
                valueHolder.setValue(PropertyConverters.getConverterFor(property).convertTo(property.getValue()));
            }
        }
    }

    public Serializable getConvertedPropertyValue() {
        PropertyConverter<?, ?> converter = PropertyConverters.getConverterFor(property);
        if (property.getInstance() != null) {
            return converter.convertTo(property.getValue());
        }
        return null;
    }

    @Override
    public BindingNode cloneWithProperty(AbstractPropertyMetadata<? super Object> property) {
        return new SimpleNode(name, property, classMetadata, manager);
    }
}
