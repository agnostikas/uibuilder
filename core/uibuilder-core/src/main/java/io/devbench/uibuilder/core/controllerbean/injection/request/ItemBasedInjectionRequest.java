/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.request;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.Data;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Source;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import org.jetbrains.annotations.Nullable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;
import static io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils.*;

public final class ItemBasedInjectionRequest extends AbstractInjectionRequest {

    private final Object item;
    private final Object source;
    private final Map<String, Object> values;

    public ItemBasedInjectionRequest(Object item, Object source, Method method, Map<String, Object> values) {
        super(method);
        this.item = item;
        this.source = source;
        this.values = values;
    }

    @Override
    @Nullable
    protected Object getValueByParameter(Parameter parameter) {
        if (parameter.isAnnotationPresent(Value.class)) {
            return values.get(parameter.getAnnotation(Value.class).value());
        } else if (parameter.isAnnotationPresent(Data.class)) {
            return values.get(EVENT_DATASET_PREFIX + parameter.getAnnotation(Data.class).value());
        } else if (parameter.isAnnotationPresent(Item.class)) {
            return getCollectionAwareItem(item, parameter.getType());
        } else if (parameter.isAnnotationPresent(Source.class)) {
            return source;
        } else {
            return super.getValueByParameter(parameter);
        }
    }
}
