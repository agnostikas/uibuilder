/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import io.devbench.uibuilder.core.controllerbean.statenodemanager.ControllerBeanBasedStateNodeManager;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.*;

@Data
public class PageLoaderContext {

    @NotNull
    private final List<String> pageContextIds = new ArrayList<>();
    private final String htmlPath;
    private final DomBind domBind;
    private final StateNodeManager stateNodeManager;
    private final Set<String> bindings = new HashSet<>();
    private final Set<String> flowNavigationMethods = new HashSet<>();

    private Document document;
    private Element pageElement;

    public PageLoaderContext() {
        this(null, null);
    }

    public PageLoaderContext(String htmlPath, DomBind domBind) {
        this(htmlPath, domBind, null);
    }

    public PageLoaderContext(String htmlPath, DomBind domBind, StateNodeManager parentStateNodeManager) {
        this.htmlPath = htmlPath;
        this.domBind = domBind;
        stateNodeManager = new ControllerBeanBasedStateNodeManager(parentStateNodeManager);
    }
}
