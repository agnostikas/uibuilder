/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jsoup.nodes.Element;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

@Data
@Builder
@AllArgsConstructor
public class ElementParserContext {

    private final Element element;
    private final Component parentComponent;
    private final Map<String, ComponentInjectionPoint> componentInjectionPoints;
    private final StateNodeManager stateNodeManager;
    private final Set<String> bindings;
    private final com.vaadin.flow.dom.Element mainContainerElement;
    private final ElementParsingComponentManager parent;
    private final Consumer<String> availableFlowTargetEventConsumer;
    private final String htmlPath;
    private final List<ElementBindingHandlerDescriptor> bindingHandlerDescriptors;

}
