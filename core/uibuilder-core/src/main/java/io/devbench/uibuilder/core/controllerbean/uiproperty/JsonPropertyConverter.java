/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import elemental.json.JsonValue;

/**
 * Implement this, for complex types, that cannot be converted as a simple string.
 * The properties converted with this type of converter cannot be refreshed from the client side.
 */
public interface JsonPropertyConverter<PROPERTY_TYPE> extends PropertyConverter<PROPERTY_TYPE, JsonValue> {

}
