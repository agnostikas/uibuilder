/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

final class MetadataReflectionUtils {

    static <CLZ_T> List<CLZ_T> getAllClassMember(Class<?> clz, Function<Class<?>, CLZ_T[]> function) {
        List<CLZ_T> items = new ArrayList<>();
        for (Class<?> clazz = clz; clazz != null; clazz = clazz.getSuperclass()) {
            items.addAll(Arrays.asList(function.apply(clazz)));
        }
        return items;
    }

    static Field getField(Class<?> clz, String name) throws NoSuchFieldException {
        Class<?> clazz = clz;
        while (clazz != null) {
            try {
                return clazz.getDeclaredField(name);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }
        throw new NoSuchFieldException(name);
    }

}
