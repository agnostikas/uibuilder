/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.i18n.core.I;
import org.jsoup.nodes.Element;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TranslateStaticTextsProcessor extends PageLoaderProcessor {

    @Override
    public void process(PageLoaderContext context) {
        translateElement(context.getPageElement());
    }

    private void translateElement(Element element) {
        Pattern p = Pattern.compile("tr\\((['\"])(.+?)\\1\\)", Pattern.DOTALL);
        Matcher htmlMatcher = p.matcher(element.html());
        element.html(remapAllMatchWith(htmlMatcher));
    }

    private String remapAllMatchWith(Matcher matcher) {
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, I.tr(matcher.group(2)));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

}
