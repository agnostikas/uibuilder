/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;
import java.util.UUID;

@Tag("uibuilder-dom-bind")
@JsModule("./uibuilder-dom-bind/uibuilder-dom-bind.js")
@JsModule("./uibuilder/util/common/applyCurrentThemeModule.js")
@JsModule("./uibuilder/util/common/eventResponseHandler.js")
public class DomBind extends Component implements HasStyle {

    @DomEvent("innerHTMLSet")
    public static class InnerHtmlSetEvent extends ComponentEvent<DomBind> {

        @Getter
        private final boolean fragment;

        public InnerHtmlSetEvent(DomBind source, boolean fromClient) {
            this(source, fromClient, false);
        }

        public InnerHtmlSetEvent(DomBind source, boolean fromClient, @EventData("event.detail.fragment") boolean fragment) {
            super(source, fromClient);
            this.fragment = fragment;
        }
    }

    private Template template;

    @Getter
    private String plainHtml;

    public DomBind() {
        super();
        setupDefaults();
        getElement().attachShadow();
    }

    private void setupDefaults() {
        this.setId(UUID.randomUUID().toString());
        template = new Template();
    }

    public void setPlainHtml(String plainHtml) {
        this.plainHtml = plainHtml;
        template.setPlainHtml(plainHtml);

        getElement().callJsFunction("_setupInnerHTML", plainHtml);
    }

    public void dispatchInnerHtmlFragmentSet(String fragmentHtml, String parentId) {
        getElement().callJsFunction("_setupInnerHTMLFragment", fragmentHtml, parentId);
    }

    public Registration addInnerHtmlSetEventListener(ComponentEventListener<InnerHtmlSetEvent> listener) {
        return addListener(InnerHtmlSetEvent.class, listener);
    }

}
