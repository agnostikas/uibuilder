/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.dynamictheme;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.theme.AbstractTheme;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ConfigurationBasedTheme implements AbstractTheme {

    private static final String THEME_CLASS = "theme.class";
    private static final String THEME_VARIANT = "theme.variant";
    private static final String DEFAULT_THEME_CLASS_NAME = "io.devbench.uibuilder.theme.lumo.UibuilderLumo";
    private static final String THEME_ATTRIBUTE = "theme";

    @Setter
    private static String defaultThemeClassName = DEFAULT_THEME_CLASS_NAME;

    @Getter(AccessLevel.PACKAGE)
    private final AbstractTheme delegatedTheme;
    private final Config config;

    @Getter
    private List<String> themeJsImports;

    public ConfigurationBasedTheme() {
        config = ConfigFactory.load();
        delegatedTheme = Objects.requireNonNull(loadThemeInstance(), "Cannot load the configured theme nor the default theme");
        themeJsImports = collectThemeStylesImports();
    }

    private AbstractTheme loadThemeInstance() {
        AbstractTheme themeInstance = null;
        if (config.hasPath(THEME_CLASS)) {
            themeInstance = instantiateTheme(config.getString(THEME_CLASS));
        }

        if (themeInstance == null) {
            themeInstance = instantiateTheme(defaultThemeClassName);
        }
        return themeInstance;
    }

    private List<String> collectThemeStylesImports() {
        if (UI.getCurrent() != null) {
            return collectSuperClasses()
                .map(clazz -> clazz.getDeclaredAnnotationsByType(JsModule.class))
                .flatMap(Arrays::stream)
                .map(JsModule::value)
                .filter(jsImport -> jsImport.startsWith("./"))
                .map(jsImport -> jsImport.substring(1))
                .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private AbstractTheme instantiateTheme(String themeClassName) {
        try {
            return (AbstractTheme) Class.forName(themeClassName).newInstance();
        } catch (ClassNotFoundException | ClassCastException | InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    private String getThemeVariant() {
        if (config.hasPath(THEME_VARIANT)) {
            return config.getString(THEME_VARIANT);
        }
        return "";
    }

    public String getThemeUrl() {
        return this.delegatedTheme.getThemeUrl();
    }

    private Stream<Class<?>> collectSuperClasses() {
        List<Class<?>> classes = new ArrayList<>();
        Class<?> currentClass = delegatedTheme.getClass();
        while (!currentClass.equals(Object.class)) {
            classes.add(currentClass);
            currentClass = currentClass.getSuperclass();
        }
        Collections.reverse(classes);
        return classes.stream();
    }

    public List<String> getHeaderInlineContents() {
        return delegatedTheme.getHeaderInlineContents();
    }

    public String getBaseUrl() {
        return this.delegatedTheme.getBaseUrl();
    }

    public String translateUrl(String url) {
        return this.delegatedTheme.translateUrl(url);
    }

    @Override
    public Map<String, String> getHtmlAttributes(String variant) {
        Map<String, String> bodyAttributes = new HashMap<>(this.delegatedTheme.getHtmlAttributes(variant));
        bodyAttributes.put(THEME_ATTRIBUTE, getThemeVariant());
        return bodyAttributes;
    }
}
