/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty.converters;

import io.devbench.uibuilder.core.templateparser.UIBuilderMethodReference;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;
import java.util.Map;
import java.util.function.Function;

public class FunctionPropertyConverter extends BaseMethodPropertyConverter<Function<?, ?>> {

    @SuppressWarnings("unchecked")
    public FunctionPropertyConverter() {
        super((Class) Function.class);
    }

    @Override
    protected Function<?, ?> createImplementationFromBestMatchingMethodHandle(
        UIBuilderMethodReference methodReference,
        Map<MethodType, MethodHandle> methodHandles
    ) {
        return (obj) -> {
            MethodHandle methodHandle = methodHandles
                .entrySet()
                .stream()
                // TODO #218 - deal with inheritance and stuff.
                .filter(it -> it.getKey().parameterType(1).isAssignableFrom(obj.getClass()))
                .map(Map.Entry::getValue)
                .collect(methodCollector());
            return tryToInvokeMethodHandle(() -> methodHandle.invoke(methodReference.getControllerBean(), obj));
        };
    }

    @Override
    protected int getParameterCount() {
        return 2;
    }

}
