/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.validator;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;

import java.lang.annotation.Annotation;
import java.util.Arrays;

public class InjectionNotNullAnnotationValidator implements InjectionAnnotationValidator {

    @Override
    public boolean test(Annotation[] annotations, Object o) {
        return annotations.length == 0 || Arrays.stream(annotations).map(Annotation::getClass).allMatch(a -> isValid(a, o));
    }

    private boolean isValid(Class<? extends Annotation> a, Object o) {
        if (CallOnNonNull.class.isAssignableFrom(a)) {
            return o != null;
        } else {
            return true;
        }
    }
}
