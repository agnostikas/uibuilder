/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean;

import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.PropertyMetadata;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerBeanManager implements Serializable {

    protected static String[] PROVIDER_CLASS_FQNS = {"javax.inject.Provider", "org.springframework.beans.factory.ObjectProvider"};

    private final Set<Class<?>> controllerBeanClasses;

    private final ControllerBeanProvidersHolder providers;

    private final List<ComponentInjectionPoint> componentInjectionPoints;

    private final List<UIEventHandlerContext> eventHandlersCache;


    private ControllerBeanManager(ControllerBeanProvidersHolder providers, Class<? extends Annotation> controllerBeanAnnotation) {
        this.providers = providers;
        controllerBeanClasses = Collections.unmodifiableSet(MemberScanner.getInstance().findClassesByAnnotation(controllerBeanAnnotation));
        eventHandlersCache = cacheEventHandlerContexts(controllerBeanClasses);
        componentInjectionPoints = fetchComponentInjectionPoints(controllerBeanClasses);
    }

    public static ControllerBeanManager getInstance() {
        return SingletonManager.getInstanceOf(ControllerBeanManager.class);
    }

    public static ControllerBeanManager registerInstanceToContext(Object context,
                                                                  ControllerBeanProvidersHolder providers,
                                                                  Class<? extends Annotation> controllerBeanAnnotation) {
        ControllerBeanManager instance = new ControllerBeanManager(providers, controllerBeanAnnotation);
        SingletonManager.registerSingletonToContext(context, instance);
        return instance;
    }

    public static ControllerBeanManager registerToActiveContext(ControllerBeanProvidersHolder providers, Class<? extends Annotation> controllerBeanAnnotation) {
        ControllerBeanManager instance = new ControllerBeanManager(providers, controllerBeanAnnotation);
        SingletonManager.registerSingleton(instance);
        return instance;
    }


    private List<UIEventHandlerContext> cacheEventHandlerContexts(Set<Class<?>> classes) {
        return Collections.unmodifiableList(classes.stream()
            .flatMap(
                cls ->
                    Arrays.stream(cls.getMethods())
                        .filter(method -> method.isAnnotationPresent(UIEventHandler.class))
                        .map(method -> UIEventHandlerContext.buildFromControllerBeanMethod(providers.getBeanNameByClass().get(cls), method))
            ).collect(Collectors.toList()));
    }

    private Stream<Map<Method, Parameter>> collectMethodUIComponentParameters(Method method) {
        return Arrays.stream(method.getParameters())
            .filter(parameter -> parameter.isAnnotationPresent(UIComponent.class))
            .map(parameter -> createMutableSingletonMap(method, parameter));
    }

    private boolean isEventHandlerMethodWithUIComponentParameter(Method method) {
        return method.isAnnotationPresent(UIEventHandler.class) &&
            Arrays.stream(method.getParameters()).anyMatch(parameter -> parameter.isAnnotationPresent(UIComponent.class));
    }

    private boolean isUIComponentIdAnnotatedField(Field field) {
        return field.isAnnotationPresent(UIComponent.class);
    }

    private String getComponentId(Map<?, Parameter> parameterMap) {
        return parameterMap.values().iterator().next().getAnnotation(UIComponent.class).value();
    }

    private Map<Method, Parameter> createMutableSingletonMap(Method method, Parameter parameter) {
        Map<Method, Parameter> mutableMap = new HashMap<>();
        mutableMap.put(method, parameter);
        return mutableMap;
    }

    private Collector<Map<Method, Parameter>, ?, Map<String, Map<Method, Parameter>>> groupMethodParameterEntriesByComponentId() {
        return Collectors.toMap(this::getComponentId, methodParameterMap -> methodParameterMap, (map1, map2) -> {
            map1.putAll(map2);
            return map1;
        });
    }

    private void scanInjectionPointsInEventHandlerMethodParameters(Set<Class<?>> classes, Collection<ComponentInjectionPoint> collectedInjectionPoints) {
        classes.stream()
            .flatMap(cls -> Arrays.stream(cls.getDeclaredMethods()))
            .filter(this::isEventHandlerMethodWithUIComponentParameter)
            .flatMap(this::collectMethodUIComponentParameters)
            .collect(groupMethodParameterEntriesByComponentId())
            .entrySet()
            .stream()
            .map(methodParameterEntry -> new ComponentInjectionPoint(methodParameterEntry.getKey(), methodParameterEntry.getValue()))
            .forEach(collectedInjectionPoints::add);
    }

    private void scanInjectionPointsInClassMemberFields(Set<Class<?>> classes, Collection<ComponentInjectionPoint> collectedInjectionPoints) {
        classes.stream()
            .map(ClassMetadata::ofClass)
            .flatMap(cls -> cls.getProperties().stream())
            .map(PropertyMetadata::getField)
            .filter(Objects::nonNull)
            .filter(this::isUIComponentIdAnnotatedField)
            .flatMap(this::collectUIComponentFieldProviderType)
            .collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.mapping(Map.Entry::getValue, Collectors.toSet())))
            .entrySet()
            .stream()
            .map(componentIdClassesEntry -> new ComponentInjectionPoint(componentIdClassesEntry.getKey(), componentIdClassesEntry.getValue()))
            .forEach(collectedInjectionPoints::add);
    }

    private List<ComponentInjectionPoint> fetchComponentInjectionPoints(Set<Class<?>> classes) {
        List<ComponentInjectionPoint> collectedInjectionPoints = new ArrayList<>();
        scanInjectionPointsInEventHandlerMethodParameters(classes, collectedInjectionPoints);
        scanInjectionPointsInClassMemberFields(classes, collectedInjectionPoints);
        return mergeInjectionPointsById(collectedInjectionPoints);
    }

    private List<ComponentInjectionPoint> mergeInjectionPointsById(List<ComponentInjectionPoint> collectedInjectionPoints) {
        Map<String, ComponentInjectionPoint> injectionPointsIdMap = new HashMap<>();
        collectedInjectionPoints.forEach(componentInjectionPoint -> {
            String id = componentInjectionPoint.getId();
            ComponentInjectionPoint injectionPoint = injectionPointsIdMap.computeIfAbsent(id, idWithoutInjectionPoint -> componentInjectionPoint);
            Set<? extends Class<?>> targetClasses = componentInjectionPoint.getTargetClasses();
            if (targetClasses != null && !targetClasses.isEmpty()) {
                injectionPoint.setTargetClasses(targetClasses);
            }
        });
        return Collections.unmodifiableList(new ArrayList<>(injectionPointsIdMap.values()));
    }

    private Stream<Map.Entry<String, Class<?>>> collectUIComponentFieldProviderType(Field field) {
        Map<String, Class<?>> fieldTypeMap = new HashMap<>();

        String componentId = field.getAnnotation(UIComponent.class).value();
        String fieldTypeFQName = field.getType().getName();
        if (Arrays.asList(PROVIDER_CLASS_FQNS).contains(fieldTypeFQName)) {
            PropertyMetadata.findGenericType(field).ifPresent(genericType -> fieldTypeMap.put(componentId, genericType));
        }

        return fieldTypeMap.entrySet().stream();
    }

    public Map<String, ComponentInjectionPoint> findInjectionPointsById() {
        return Collections.unmodifiableMap(componentInjectionPoints.stream()
            .collect(Collectors.toMap(ComponentInjectionPoint::getId,
                Function.identity())));
    }

    public List<ComponentInjectionPoint> getAllCachedComponentInjectionPoint() {
        return componentInjectionPoints;
    }

    @SuppressWarnings("unchecked")
    public <T> T getControllerBean(String name) {
        return (T) providers.getBeanInstanceByName().get(name);
    }

    @SuppressWarnings("unchecked")
    public <T> T getControllerBean(Class<?> clazz) {
        return (T) providers.getBeanInstanceByClass().get(clazz);
    }

    public Set<String> getEventHandlerQualifiedNames() {
        return eventHandlersCache.stream()
            .map(UIEventHandlerContext::getEventHandlerQualifiedName)
            .collect(Collectors.toSet());
    }

    public Map<String, String> getEvenHandlerNormalizedNameToQualifiedNameMap() {
        return getEventHandlerQualifiedNames().stream()
            .collect(Collectors.toMap(name -> name.replaceAll("::", "_"), Function.identity()));
    }

    @SuppressWarnings("unchecked")
    public <T> ClassMetadata<T> getBeanMetadata(String beanName) {
        Object instance = providers.getBeanInstanceByName().get(beanName);
        return ClassMetadata.ofValue((T) instance);
    }

    public void callLocaleChangeObservers(LocaleChangeEvent event) {
        controllerBeanClasses.stream()
            .filter(LocaleChangeObserver.class::isAssignableFrom)
            .map(cls -> providers.getBeanInstanceByClass().get(cls))
            .map(LocaleChangeObserver.class::cast)
            .forEach(observer -> observer.localeChange(event));
    }

    public Optional<UIEventHandlerContext> getEventHandlerContext(@NotNull String qualifiedName) {
        Objects.requireNonNull(qualifiedName, "Parameter qualifiedName must not be null.");
        return eventHandlersCache.stream()
            .filter(eventHandler -> qualifiedName.equals(eventHandler.getEventHandlerQualifiedName()))
            .filter(eventHandler -> eventHandler.getCustomEventQualifiers().isEmpty())
            .filter(eventHandler -> eventHandler.getEventQualifiers().isEmpty())
            .findAny();
    }

    public Optional<UIEventHandlerContext> getEventHandlerContext(@NotNull String qualifiedName, @NotNull EventQualifier qualifier) {
        Objects.requireNonNull(qualifiedName, "Parameter qualifiedName must not be null.");
        Objects.requireNonNull(qualifier, "Parameter qualifier must not be null.");
        return eventHandlersCache.stream()
            .filter(eventHandler -> qualifiedName.equals(eventHandler.getEventHandlerQualifiedName()))
            .filter(eventHandler -> eventHandler.getEventQualifiers().contains(qualifier))
            .findAny();
    }

    public Optional<UIEventHandlerContext> getEventHandlerContext(@NotNull String qualifiedName, @NotNull String customQualifier) {
        Objects.requireNonNull(qualifiedName, "Parameter qualifiedName must not be null.");
        Objects.requireNonNull(customQualifier, "Parameter customQualifier must not be null.");
        return eventHandlersCache.stream()
            .filter(eventHandler -> qualifiedName.equals(eventHandler.getEventHandlerQualifiedName()))
            .filter(eventHandler -> eventHandler.getCustomEventQualifiers().contains(customQualifier))
            .findAny();
    }

    public void callEventHandlers(String qualifiedName, StateNodeManager stateNodeManager, DomEvent event) {
        eventHandlersCache.stream()
            .filter(eventHandler -> qualifiedName.equals(eventHandler.getEventHandlerQualifiedName()))
            .forEach(eventHandler -> eventHandler.callEventHandler(stateNodeManager, event));
    }
}
