/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.request;

import com.google.common.collect.Sets;
import io.devbench.uibuilder.core.controllerbean.injection.validator.HasMainAnnotationInjectionValidator;
import io.devbench.uibuilder.core.controllerbean.injection.validator.InjectionAnnotationValidator;
import io.devbench.uibuilder.core.controllerbean.injection.validator.InjectionNotNullAnnotationValidator;

import java.util.Collection;
import java.util.Collections;

public interface InjectionRequest {

    Collection<InjectionAnnotationValidator> INJECTION_ANNOTATION_VALIDATORS =
        Collections.unmodifiableCollection(Sets.newHashSet(new HasMainAnnotationInjectionValidator(), new InjectionNotNullAnnotationValidator()));

    boolean isValid();

    Object[] getParameters();
}
