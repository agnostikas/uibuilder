/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

public class StateNodeManager {

    public static final Pattern BINDING_PATTERN = Pattern.compile("\\{\\{(\\w+(\\.\\w+)*)}}");

    private final StateNodeManager parentStateNodeManager;
    private final Collection<StateNodeManager> childStateNodeManagers = new ArrayList<>();

    protected Map<String, BindingNode> bindingNodes;

    private List<Function<StateNode, ? super Object>> valueProviders;

    private final Function<String, ClassMetadata<?>> classMetadataProvider;

    public StateNodeManager(Function<String, ClassMetadata<?>> classMetadataProvider) {
        this(classMetadataProvider, null);
    }

    public StateNodeManager(Function<String, ClassMetadata<?>> classMetadataProvider, StateNodeManager parentStateNodeManager) {
        this.classMetadataProvider = classMetadataProvider;
        this.parentStateNodeManager = parentStateNodeManager;
        bindingNodes = new HashMap<>();
        valueProviders = new ArrayList<>();

        if (parentStateNodeManager != null) {
            parentStateNodeManager.childStateNodeManagers.add(this);
        }
    }

    public void addBindingPath(String path) {
        String[] bindingPath = path.split("\\.");
        if (bindingPath.length > 1) {
            BindingNode node = bindingNodes.computeIfAbsent(bindingPath[0],
                name -> new BeanNode(name, new TopLevelSyntheticProperty(name, classMetadataProvider.apply(name)), classMetadataProvider.apply(name), this));
            node.addPath(bindingPath, 0);
        }
    }

    public BindingNode getNodeForPath(String path) {
        String[] bindingPath = path.split("\\.");
        if (bindingPath.length > 1) {
            BindingNode node = bindingNodes.get(bindingPath[0]);
            if (node != null) {
                return node.getNodeAt(bindingPath);
            }
        }
        return null;
    }

    public Map<String, Serializable> populatePropertyValues() {
        Map<String, Serializable> propertyValues = new HashMap<>();
        visitAllStateNodeManagers(
            stateNodeManager -> stateNodeManager.bindingNodes.values()
                .forEach(bindingNode -> propertyValues.put(bindingNode.getName(), bindingNode.populateValues()))
        );
        return propertyValues;
    }

    public List<BindingNodeSyncError> synchronizeProperties() {
        List<BindingNodeSyncError> errors = new ArrayList<>();
        visitAllStateNodeManagers(
            stateNodeManager -> stateNodeManager.bindingNodes.values()
                .forEach(bindingNode -> errors.addAll(bindingNode.synchronizeProperty("")))
        );
        return errors;
    }

    public void synchronizeStateNodes() {
        visitAllStateNodeManagers(
            stateNodeManager -> stateNodeManager.bindingNodes.values()
                .forEach(BindingNode::synchronizeStateNode)
        );
    }

    private void visitAllStateNodeManagers(Consumer<StateNodeManager> consumer) {
        visitStateNodeManagerSubTree(consumer, getRootStateNodeManager());
    }

    private StateNodeManager getRootStateNodeManager() {
        StateNodeManager stateNodeManager = this;
        while (stateNodeManager.parentStateNodeManager != null) {
            stateNodeManager = stateNodeManager.parentStateNodeManager;
        }
        return stateNodeManager;
    }

    private void visitStateNodeManagerSubTree(Consumer<StateNodeManager> consumer, StateNodeManager entry) {
        consumer.accept(entry);
        for (StateNodeManager child : entry.childStateNodeManagers) {
            visitStateNodeManagerSubTree(consumer, child);
        }
    }

    public Set<String> getPropertyNames() {
        return bindingNodes.keySet();
    }

    public Object getActualValueFromStateNode(StateNode node) {
        return valueProviders.stream()
            .map(provider -> provider.apply(node))
            .filter(Objects::nonNull)
            .findAny().orElse(null);
    }

    void registerValueProvider(Function<StateNode, ? super Object> provider) {
        valueProviders.add(provider);
    }

    public void unload() {
        if (parentStateNodeManager != null) {
            parentStateNodeManager.childStateNodeManagers.remove(this);
        }
    }
}
