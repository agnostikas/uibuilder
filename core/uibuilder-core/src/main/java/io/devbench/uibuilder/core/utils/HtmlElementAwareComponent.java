/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.components.HasRawElement;
import org.jsoup.nodes.Element;

import static io.devbench.uibuilder.core.utils.ElementCollector.*;

public class HtmlElementAwareComponent {

    private Component component;
    private Element element;

    HtmlElementAwareComponent(Component component, Element element) {
        this.component = component;
        this.element = element;
        ensureId();
        ensureRawElement();
    }

    private void ensureId() {
        if (!component.getId().isPresent() && element.hasAttr(ID)) {
            component.setId(element.attr(ID));
        }
    }

    private void ensureRawElement() {
        if (component instanceof HasRawElement) {
            ((HasRawElement) component).setRawElement(element);
        }
    }

    public Component getComponent() {
        return component;
    }

    public Element getElement() {
        return element;
    }

}
