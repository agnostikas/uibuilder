/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.singleton;

import com.vaadin.flow.server.VaadinServlet;
import io.devbench.uibuilder.api.singleton.ContextAwareSingletonHolder;

import javax.servlet.ServletContext;

/**
 * Context in Pure case is the {@code ServletContext} which can be accessed from {@code VaadinServlet}
 *
 */
public class PureContextAwareSingletonHolder implements ContextAwareSingletonHolder {

    @Override
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public <T> void storeInstance(Class<T> instanceClass, T instance) {
        VaadinServlet current = VaadinServlet.getCurrent();
        if (current != null) {
            ServletContext servletContext = current.getServletContext();
            if (servletContext != null) {
                synchronized (servletContext) {
                    servletContext.setAttribute(instanceClass.getName(), instance);
                }
            }
        }
    }

}
