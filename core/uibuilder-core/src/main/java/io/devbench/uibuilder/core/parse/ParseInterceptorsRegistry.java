/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.parse;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.api.singleton.SingletonManager;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
public class ParseInterceptorsRegistry {

    private Set<ParseInterceptor> interceptors;

    private ParseInterceptorsRegistry() {
        initialize();
    }

    private void initialize() {
        interceptors = Collections.unmodifiableSet(
            MemberScanner.getInstance().findClassesBySuperType(ParseInterceptor.class).stream()
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()) && !Modifier.isInterface(clazz.getModifiers()))
                .map(this::tryToInstantiateParseInterceptorClass)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet())
        );
    }

    private ParseInterceptor tryToInstantiateParseInterceptorClass(Class<? extends ParseInterceptor> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassCastException e) {
            log.warn("Cannot initialize interceptor class: " + clazz.getSimpleName(), e);
            return null;
        }
    }

    public Set<ParseInterceptor> getInterceptorsByElement(Element element) {
        return interceptors.stream().filter(interceptors -> interceptors.isApplicable(element)).collect(Collectors.toSet());
    }

    public static ParseInterceptorsRegistry getInstance() {
        return SingletonManager.getInstanceOf(ParseInterceptorsRegistry.class);
    }

    public static ParseInterceptorsRegistry registerToActiveContext() {
        ParseInterceptorsRegistry instance = new ParseInterceptorsRegistry();
        SingletonManager.registerSingleton(instance);
        return instance;
    }

    public static ParseInterceptorsRegistry registerToContext(Object context) {
        ParseInterceptorsRegistry instance = new ParseInterceptorsRegistry();
        SingletonManager.registerSingletonToContext(context, instance);
        return instance;
    }
}
