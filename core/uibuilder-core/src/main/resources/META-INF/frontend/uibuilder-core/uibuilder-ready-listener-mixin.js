(function () {
    window.Uibuilder = window.Uibuilder || {};

    window.Uibuilder.ReadyListenerMixin = superClass => class UibuilderReadyListenerMixin extends superClass {
        _uibuilderReady() {
            this.dispatchEvent(new CustomEvent("uibuilderReady"));
        }
    }
})();

