/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServiceInitListener;
import io.devbench.uibuilder.api.components.HasRawElementComponent;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.api.exceptions.UnsupportedElementException;
import io.devbench.uibuilder.core.startup.ComponentTagRegistry;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, JsoupExtension.class, SingletonProviderForTestsExtension.class})
class ElementCollectorTest {

    @Mock
    @SingletonInstance(ComponentTagRegistry.class)
    private ComponentTagRegistry componentTagRegistry;

    @Mock
    private VaadinService vaadinService;

    private static final String TEST_COMPONENT_TAG = "test-component";
    private static final String TEST_ID = "test-id";

    @BeforeEach
    void setUp() {
        ElementCollector.getComponentMap().clear();
        doReturn(Optional.of(Grid.class)).when(componentTagRegistry).getComponentClassByTag("vaadin-grid");
        when(vaadinService.getInstantiator()).thenReturn(createInstantiator());
    }

    @Test
    @DisplayName("should register only the valid component")
    void test_should_register_only_the_valid_component() {
        TestComponent component = new TestComponent();
        Element testElement = createTestElement(TEST_ID);

        ElementCollector.register(component, testElement);
        Optional<HtmlElementAwareComponent> heac = ElementCollector.getById(TEST_ID);
        Optional<HtmlElementAwareComponent> heacNotExists = ElementCollector.getById(TEST_ID + "-not-exists");

        assertAll(
            () -> assertTrue(heac.isPresent()),
            () -> assertFalse(heacNotExists.isPresent()),
            () -> assertTrue(heac.get().getComponent().getId().isPresent()),
            () -> assertEquals(TEST_ID, heac.get().getComponent().getId().get()),
            () -> assertTrue(heac.get().getComponent() instanceof HasRawElementComponent),
            () -> assertNotNull(((HasRawElementComponent) heac.get().getComponent()).getRawElement()),
            () -> assertSame(testElement, ((HasRawElementComponent) heac.get().getComponent()).getRawElement())
        );
    }

    @Test
    @DisplayName("should not register component when both component and element is missing ID")
    void test_should_not_register_component_when_both_component_and_element_is_missing_id() {
        TestComponent component = new TestComponent();
        Element testElement = createTestElement();

        ElementCollector.register(component, testElement);
        assertTrue(ElementCollector.getComponentMap().isEmpty());
    }

    @Test
    @DisplayName("should construct a valid grid")
    void test_should_construct_a_valid_grid(@LoadElement("/element-collector-test.html") Element page) {
        Element element = page.getElementById(TEST_ID);
        TestComponent component = new TestComponent();
        component.setId(TEST_ID);
        component.setRawElement(element);

        TestDomBindComponent domBindComponent = new TestDomBindComponent();
        domBindComponent.getElement().appendChild(component.getElement());

        Optional<HtmlElementAwareComponent> heac = ElementCollector.getById("grid-id", component);

        assertTrue(heac.isPresent());
        HtmlElementAwareComponent elemAndComp = heac.get();
        assertAll(
            () -> assertNotNull(elemAndComp.getComponent()),
            () -> assertNotNull(elemAndComp.getElement()),
            () -> assertEquals("grid-id", elemAndComp.getComponent().getId().orElse(null)),
            () -> assertEquals("grid-id", elemAndComp.getElement().attr(ElementCollector.ID))
        );
    }

    @Test
    @DisplayName("should return already created grid")
    void test_should_return_already_created_grid(@LoadElement("/element-collector-test.html") Element page) {
        Element element = page.getElementById(TEST_ID);
        TestComponent component = new TestComponent();
        component.setId(TEST_ID);
        component.setRawElement(element);

        ElementCollector.register(new Grid(), page.getElementById("grid-id"));

        TestDomBindComponent domBindComponent = new TestDomBindComponent();
        domBindComponent.getElement().appendChild(component.getElement());

        Optional<HtmlElementAwareComponent> heac = ElementCollector.getById("grid-id", component);

        assertTrue(heac.isPresent());
        HtmlElementAwareComponent elemAndComp = heac.get();
        assertAll(
            () -> assertNotNull(elemAndComp.getComponent()),
            () -> assertNotNull(elemAndComp.getElement()),
            () -> assertEquals("grid-id", elemAndComp.getComponent().getId().orElse(null)),
            () -> assertEquals("grid-id", elemAndComp.getElement().attr(ElementCollector.ID))
        );
    }

    @Test
    @DisplayName("should throw exception if no dom-bind has been found")
    void test_should_throw_exception_if_no_dom_bind_has_been_found(@LoadElement("/element-collector-test.html") Element page) {
        Element element = page.getElementById(TEST_ID);
        TestComponent component = new TestComponent();
        component.setId(TEST_ID);
        component.setRawElement(element);

        assertThrows(InternalResolverException.class, () -> ElementCollector.getById("grid-id", component));
    }

    @Test
    @DisplayName("should throw exception if trying to revive an unsupported component")
    void test_should_throw_exception_if_trying_to_revive_an_unsupported_component(@LoadElement("/element-collector-test.html") Element page) {
        Element element = page.getElementById(TEST_ID);
        TestComponent component = new TestComponent();
        component.setId(TEST_ID);
        component.setRawElement(element);

        assertThrows(UnsupportedElementException.class, () -> ElementCollector.getById("unsup-id", component));
    }

    @Test
    @DisplayName("should return empty optional if getting component with a non existent ID")
    void test_should_return_empty_optional_if_getting_component_with_a_non_existent_id(@LoadElement("/element-collector-test.html") Element page) {
        Element element = page.getElementById(TEST_ID);
        TestComponent component = new TestComponent();
        component.setId(TEST_ID);
        component.setRawElement(element);

        Optional<HtmlElementAwareComponent> heac = ElementCollector.getById("id-that-do-not-exists", component);

        assertFalse(heac.isPresent());
    }

    private Element createTestElement() {
        return createElement(TEST_COMPONENT_TAG);
    }

    private Element createTestElement(String id) {
        return createElement(TEST_COMPONENT_TAG, id);
    }

    private Element createTestElement(String id, String htmlContent) {
        return createElement(TEST_COMPONENT_TAG, id, htmlContent);
    }

    private Element createElement(String tag, String id) {
        return createElement(tag, id, null);
    }

    private Element createElement(String tag) {
        return createElement(tag, null, null);
    }

    private Element createElement(String tag, String id, String htmlContent) {
        Element element = new Element(tag);
        if (id != null) {
            element.attr(ElementCollector.ID, id);
        }
        if (htmlContent != null) {
            element.html(htmlContent);
        }
        return element;
    }

    private Instantiator createInstantiator() {
        return new Instantiator() {
            @Override
            public boolean init(VaadinService service) {
                return true;
            }

            @Override
            public Stream<VaadinServiceInitListener> getServiceInitListeners() {
                return Stream.empty();
            }

            @Override
            public <T> T getOrCreate(Class<T> type) {
                if (Grid.class.isAssignableFrom(type)) {
                    return (T) new Grid();
                } else if (ComboBox.class.isAssignableFrom(type)) {
                    return (T) new ComboBox();
                }
                throw new UnsupportedOperationException("Cannot test vaadin component type: " + type);
            }

            @Override
            public <T extends Component> T createComponent(Class<T> aClass) {
                return getOrCreate(aClass);
            }
        };
    }

    @Tag(TEST_COMPONENT_TAG)
    public static class TestComponent extends HasRawElementComponent {

    }

    @Tag("dom-bind")
    public static class TestDomBindComponent extends Component {

    }
}
