/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.errorpages;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.page.Page;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class FlowDefinitionNotFoundErrorPageTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private FlowManager flowManager;

    @Mock
    private UIContext uiContext;

    private Page page;

    @BeforeEach
    public void beforeEach() {
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);
        page = spy(new Page());
        when(uiContext.get(FlowManager.class)).thenReturn(flowManager);
        when(flowManager.getRegisteredFlowTargets().get("UI")).thenReturn(page);
    }

    @Test
    public void should_load_default_page_not_found_html_by_default() {
        doNothing().when(page).loadContent("page_not_found.html");

        FlowDefinitionNotFoundErrorPage testObj = new FlowDefinitionNotFoundErrorPage();
        testObj.setErrorParameter(null, null);

        verify(page).loadContent("page_not_found.html");
    }

    @Test
    public void should_load_custom_page_not_found_html_by_default() {
        when(flowManager.getFlowDefinitions().getPageNotFound()).thenReturn("PageNotFound");
        doNothing().when(page).loadContent("PageNotFound");

        FlowDefinitionNotFoundErrorPage testObj = new FlowDefinitionNotFoundErrorPage();
        testObj.setErrorParameter(null, null);

        verify(page).loadContent("PageNotFound");
    }


}
