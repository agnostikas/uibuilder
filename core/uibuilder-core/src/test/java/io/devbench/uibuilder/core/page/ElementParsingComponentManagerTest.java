package io.devbench.uibuilder.core.page;
/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.core.parse.ParseInterceptorsRegistry;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class, JsoupExtension.class})
public class ElementParsingComponentManagerTest {

    @Mock
    ElementParserContext elementParserContext;
    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;
    @LoadElement(value = "/tags.html", id = "noscript")
    private Element noscriptPageElement;
    @LoadElement(value = "/tags.html", id = "script")
    private Element scriptPageElement;

    @LoadElement(value = "/tags.html", id = "div")
    private Element divPageElement;

    @BeforeEach
    private void setup() {
        Set<Class<? extends ParseInterceptor>> classes = new HashSet<>(Collections.singletonList(TestParseInterceptor.class));
        when(memberScanner.findClassesBySuperType(ParseInterceptor.class)).thenReturn(classes);
        ParseInterceptorsRegistry.registerToActiveContext();
    }

    @Test
    void shouldElementParsingComponentManagerSkipNoScriptTag() {
        when(elementParserContext.getElement()).thenReturn(noscriptPageElement);
        final ElementParsingComponentManager elementParsingComponentManager = new ElementParsingComponentManager(elementParserContext);
        assertDoesNotThrow(elementParsingComponentManager::manageComponent);
    }

    @Test
    void shouldElementParsingComponentManagerSkipScriptTag() {
        when(elementParserContext.getElement()).thenReturn(scriptPageElement);
        final ElementParsingComponentManager elementParsingComponentManager = new ElementParsingComponentManager(elementParserContext);
        assertDoesNotThrow(elementParsingComponentManager::manageComponent);
    }

    @Test
    void shouldElementParsingComponentManagerHandleDivTag() {
        when(elementParserContext.getElement()).thenReturn(divPageElement);
        final ElementParsingComponentManager elementParsingComponentManager = new ElementParsingComponentManager(elementParserContext);
        assertDoesNotThrow(elementParsingComponentManager::manageComponent);
    }

    public static class TestParseInterceptor implements ParseInterceptor {

        @Override
        public void intercept(Component component, Element element) {

        }

        @Override
        public boolean isApplicable(Element element) {
            return false;
        }
    }
}
