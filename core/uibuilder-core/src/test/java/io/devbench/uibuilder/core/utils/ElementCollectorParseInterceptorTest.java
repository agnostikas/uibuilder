/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ElementCollectorParseInterceptorTest {

    private ElementCollectorParseInterceptor testObj;

    @BeforeEach
    void setUp() {
        testObj = new ElementCollectorParseInterceptor();
    }

    @Test
    @DisplayName("should be applicable if ID attribute is present on element")
    void test_should_be_applicable_if_id_attribute_is_present_on_element() {
        Element element = new Element("test-element");
        element.attr(ElementCollector.ID, "test-id");
        assertTrue(testObj.isApplicable(element));
    }

    @Test
    @DisplayName("should NOT be applicable if ID attribute is missing on element")
    void test_should_not_be_applicable_if_id_attribute_is_missing_on_element() {
        Element element = new Element("test-element");
        assertFalse(testObj.isApplicable(element));
    }

    @Test
    @DisplayName("should register component during intercept")
    void test_should_register_component_during_intercept() {
        TestComponent component = new TestComponent();
        Element element = new Element("test-element");
        element.attr(ElementCollector.ID, "test-id");

        System.gc();
        testObj.intercept(component, element);

        Optional<HtmlElementAwareComponent> heac = ElementCollector.getById("test-id");

        assertAll(
            () -> assertTrue(heac.isPresent()),
            () -> assertTrue(heac.get().getComponent().getId().isPresent()),
            () -> assertEquals("test-id", heac.get().getComponent().getId().get())
        );
    }

    @Tag("test-comp")
    public static class TestComponent extends Component {

    }

}
