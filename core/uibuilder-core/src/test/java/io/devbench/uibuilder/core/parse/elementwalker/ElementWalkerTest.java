/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.parse.elementwalker;

import io.devbench.uibuilder.api.parse.elementwalker.ElementProcessor;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(JsoupExtension.class)
class ElementWalkerTest {

    @LoadElement(value = "/theseBootsAreMadeForWalking.html", elemantName = "boot-component")
    private Element testRoot;


    @Test
    @DisplayName("Should walk the element and read elements in natural order")
    public void should_walk_the_element_and_read_elements_in_natural_order() {
        List<String> walkedElementTags = new ArrayList<>();
        ElementWalker.of(testRoot)
            .withProcessor(element -> {
                walkedElementTags.add(element.tagName());
                return new Elements(element);
            }).walk();

        assertEquals(
            "boot-component, walkable-tag, sub-tag, change-this, same-level-element",
            walkedElementTags.stream().collect(Collectors.joining(", "))
        );
    }

    @Test
    @DisplayName("Should be able to change elements during the walk")
    public void should_be_able_to_change_elements_during_the_walk() {
        ElementWalker.of(testRoot)
            .withProcessor(element -> {
                if ("change-this".equals(element.tagName())) {
                    Element replacement = new Element("replacement-component");
                    replacement.html("<with-some-child-element></with-some-child-element>");
                    return new Elements(replacement);
                }
                return new Elements(element);
            }).walk();
        String outerHtml = testRoot.outerHtml();
        System.out.println(outerHtml);
        assertAll(
            () -> assertTrue(outerHtml.contains("<replacement-component>")),
            () -> assertTrue(outerHtml.contains("<with-some-child-element>")),
            () -> assertFalse(outerHtml.contains("<change-this"))

        );
    }

    @Test
    @DisplayName("Should process direct replacements during walk")
    void test_should_process_direct_replacements_during_walk(@LoadElement(value = "/processReplacementElementWalkerTest.html", elemantName = "root")
                                                                 Element processReplacementTestRoot) {
        ElementProcessor processIfYes = element -> {
            if ("to-replace".equals(element.tagName())) {
                if ("YES".equals(element.attr("name"))) {
                    return element.children().size() == 1 ?
                        new Elements(element.child(0)) :
                        new Elements(element.children());
                } else {
                    return new Elements();
                }
            }
            return new Elements(element);
        };

        ElementWalker.of(processReplacementTestRoot)
            .withProcessor(processIfYes)
            .walk();

        Element walkableTag = processReplacementTestRoot.getElementsByTag("walkable-tag").first();
        assertEquals(6, walkableTag.children().size());
        assertEquals("first-tag", walkableTag.child(0).tagName());
        assertEquals("inner-one", walkableTag.child(1).tagName());
        assertEquals("inner-four", walkableTag.child(2).tagName());
        assertEquals("inner-two", walkableTag.child(3).tagName());
        assertEquals("inner-three", walkableTag.child(4).tagName());
        assertEquals("last-tag", walkableTag.child(5).tagName());
        assertTrue(processReplacementTestRoot.getElementsByTag("to-replace").isEmpty());
    }

}
