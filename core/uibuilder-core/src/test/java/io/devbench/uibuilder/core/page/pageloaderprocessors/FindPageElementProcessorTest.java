/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.page.exceptions.PageDefinitionMissingException;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, JsoupExtension.class})
class FindPageElementProcessorTest {

    public static final String UIBUILDER_PAGE = "uibuilder-page";

    @Mock
    private Document document;

    @LoadElement("/page-test-element.html")
    private Element element;

    private FindPageElementProcessor testObj;

    private PageLoaderContext context;

    @BeforeEach
    private void setup() {
        testObj = new FindPageElementProcessor();
        context = new PageLoaderContext("/some-nonexistent-html.html", null);
        context.setDocument(document);
    }

    @Test
    @DisplayName("Should find page element in document and remove comments from it")
    public void should_find_page_element_in_document_and_remove_comments_from_it() {
        when(document.getElementsByTag(UIBUILDER_PAGE)).thenReturn(new Elements(Collections.singleton(element)));

        testObj.process(context);

        assertEquals("<html>\n" +
            " <head></head>\n" +
            " <body>\n" +
            "  <uibuilder-page>  \n" +
            "   <div>\n" +
            "    content\n" +
            "   </div>  \n" +
            "  </uibuilder-page> \n" +
            " </body>\n" +
            "</html>", context.getPageElement().toString());
    }

    @Test
    @DisplayName("Should throw exception, when there is no page element found in the documenr")
    public void should_throw_exception_when_there_is_no_page_element_found_in_the_documenr() {
        when(document.getElementsByTag(UIBUILDER_PAGE)).thenReturn(new Elements());

        assertEquals("The definied html does not contain a page definition. Path: /some-nonexistent-html.html",
            assertThrows(PageDefinitionMissingException.class, () -> testObj.process(context))
                .getMessage());
    }

}
