/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.parse;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.PageTransformer;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class PageTransformerRegistryTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Mock
    private Element mockElement;

    private PageTransformersRegistry testObj;

    private void setup() {
        Set<Class<? extends PageTransformer>> classes = new HashSet<>(Arrays.asList(TestPageTransformer.class, TestNotApplicablePageTransformer.class));
        when(memberScanner.findClassesBySuperType(PageTransformer.class)).thenReturn(classes);
        testObj = PageTransformersRegistry.registerToActiveContext();
    }

    @Test
    @DisplayName("get instance should return the same object")
    void test_get_instance_should_return_the_same_object() {
        setup();
        assertSame(testObj, PageTransformersRegistry.getInstance());
    }

    @Test
    @DisplayName("Should find the applicable parse interceptor instances by the given element")
    public void should_find_the_applicable_parse_interceptor_instances_by_the_given_element() {
        setup();
        Set<PageTransformer> transformers = testObj.getPageTransformerByElement(mockElement);

        assertAll(
            () -> assertEquals(1, transformers.size()),
            () -> assertTrue(transformers.iterator().next() instanceof TestPageTransformer),
            () -> assertSame(mockElement, TestPageTransformer.lastElement)
        );
    }

    @Test
    @DisplayName("should skip transformer if cannot be instantiated")
    void test_should_skip_transformer_if_cannot_be_instantiated() {
        Set<Class<? extends PageTransformer>> classes = new HashSet<>(
            Arrays.asList(TestPageTransformer.class, TestNotApplicablePageTransformer.class, TestNotInstantiablePageTransformer.class));
        when(memberScanner.findClassesBySuperType(PageTransformer.class)).thenReturn(classes);
        testObj = PageTransformersRegistry.registerToActiveContext();

        Set<PageTransformer> transformers = testObj.getPageTransformerByElement(mockElement);

        assertAll(
            () -> assertEquals(1, transformers.size()),
            () -> assertTrue(transformers.iterator().next() instanceof TestPageTransformer),
            () -> assertSame(mockElement, TestPageTransformer.lastElement)
        );
    }

    public static class TestNotInstantiablePageTransformer implements PageTransformer {

        TestNotInstantiablePageTransformer() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isApplicable(Element element) {
            return true;
        }

        @Override
        public void transform(Element element) {

        }
    }

    public static class TestPageTransformer implements PageTransformer {

        private static Element lastElement;

        @Override
        public void transform(Element element) {

        }

        @Override
        public boolean isApplicable(Element element) {
            lastElement = element;
            return true;
        }
    }

    public static class TestNotApplicablePageTransformer implements PageTransformer {

        @Override
        public void transform(Element element) {

        }

        @Override
        public boolean isApplicable(Element element) {
            return false;
        }
    }

}
