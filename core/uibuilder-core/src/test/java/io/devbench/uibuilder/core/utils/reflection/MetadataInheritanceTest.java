/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestChild;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestChildOne;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestChildTwo;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestParent;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
public class MetadataInheritanceTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Test
    @DisplayName("Should only return the fixProp")
    void test_should_only_return_the_fix_prop() {
        ClassMetadata<TestParent> classMetadata = ClassMetadata.ofClass(TestParent.class);
        Optional<PropertyMetadata<?>> propFix = classMetadata.property("child.fixProp");
        Optional<PropertyMetadata<?>> propOne = classMetadata.property("child.propOne");
        Optional<PropertyMetadata<?>> propTwo = classMetadata.property("child.propTwo");
        assertTrue(propFix.isPresent(), "fixProp should be found in child");
        assertFalse(propOne.isPresent(), "propOne should not be found in child");
        assertFalse(propTwo.isPresent(), "propTwo should not be found in child");
    }

    @Test
    @DisplayName("Should only return the fixProp with instance")
    void test_should_only_return_the_fix_prop_with_instance() {
        TestParent testParent = new TestParent();
        testParent.setChild(new TestChild());

        ClassMetadata<TestParent> classMetadata = ClassMetadata.ofClass(TestParent.class).withInstance(testParent);
        Optional<PropertyMetadata<?>> propFix = classMetadata.property("child.fixProp");
        Optional<PropertyMetadata<?>> propOne = classMetadata.property("child.propOne");
        Optional<PropertyMetadata<?>> propTwo = classMetadata.property("child.propTwo");
        assertTrue(propFix.isPresent(), "fixProp should be found in child");
        assertFalse(propOne.isPresent(), "propOne should not be found in child");
        assertFalse(propTwo.isPresent(), "propTwo should not be found in child");
    }

    @Test
    @DisplayName("Should return propOne with proper instance")
    void test_should_return_prop_one_with_proper_instance() {
        TestParent testParent = new TestParent();
        testParent.setChild(new TestChildOne());

        ClassMetadata<TestParent> classMetadata = ClassMetadata.ofClass(TestParent.class).withInstance(testParent);
        Optional<PropertyMetadata<?>> propFix = classMetadata.property("child.fixProp");
        Optional<PropertyMetadata<?>> propOne = classMetadata.property("child.propOne");
        Optional<PropertyMetadata<?>> propTwo = classMetadata.property("child.propTwo");
        assertTrue(propFix.isPresent(), "fixProp should be found in child");
        assertTrue(propOne.isPresent(), "propOne should be found in child");
        assertFalse(propTwo.isPresent(), "propTwo should not be found in child");
    }

    @Test
    @DisplayName("Should return propTwo with propert instance")
    void test_should_return_prop_two_with_propert_instance() {
        TestParent testParent = new TestParent();
        testParent.setChild(new TestChildTwo());

        ClassMetadata<TestParent> classMetadata = ClassMetadata.ofClass(TestParent.class).withInstance(testParent);
        Optional<PropertyMetadata<?>> propFix = classMetadata.property("child.fixProp");
        Optional<PropertyMetadata<?>> propOne = classMetadata.property("child.propOne");
        Optional<PropertyMetadata<?>> propTwo = classMetadata.property("child.propTwo");
        assertTrue(propFix.isPresent(), "fixProp should be found in child");
        assertFalse(propOne.isPresent(), "propOne should not be found in child");
        assertTrue(propTwo.isPresent(), "propTwo should be found in child");
    }

    @Test
    @DisplayName("Should return all of the possible properties of a class if classmetadata has been created with enabled inheritance")
    void test_should_return_all_of_the_possible_properties_of_a_class_if_classmetadata_has_been_created_with_enabled_inheritance() {
        Set<Class<? extends TestChild>> testChildClasses = new HashSet<>(Arrays.asList(TestChildOne.class, TestChildTwo.class));
        Mockito.doReturn(testChildClasses).when(memberScanner).findClassesBySuperType(TestChild.class);

        ClassMetadata<TestParent> parentClassMetadata = new ClassMetadata<>(TestParent.class, true);

        Optional<PropertyMetadata<?>> propFix = parentClassMetadata.property("child.fixProp");
        Optional<PropertyMetadata<?>> propOne = parentClassMetadata.property("child.propOne");
        Optional<PropertyMetadata<?>> propTwo = parentClassMetadata.property("child.propTwo");
        Optional<PropertyMetadata<?>> propOther = parentClassMetadata.property("child.surelyDoesntExists");

        assertTrue(propFix.isPresent(), "fixProp should be found in child");
        assertTrue(propOne.isPresent(), "propOne should be found in child");
        assertTrue(propTwo.isPresent(), "propTwo should be found in child");
        assertFalse(propOther.isPresent(), "propOther should NOT be found in child");
    }

}
