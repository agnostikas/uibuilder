/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;
import io.devbench.uibuilder.core.controllerbean.injection.validator.InjectionNotNullAnnotationValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.stream.Stream;

class InjectionNotNullAnnotationValidatorTest {

    @Test
    void shouldBeValidWithoutNonNullAnyHow() {
        Stream.of(
            get(getAnnotation("a"), null),
            get(getAnnotation("a"), new Object()),
            get(getAnnotation("a"), "something"),
            get(getAnnotation("c"), null),
            get(getAnnotation("c"), new Object()),
            get(getAnnotation("c"), "something")
        ).forEach(Assertions::assertTrue);
    }

    @Test
    void shouldBeInvalidWithNonNullAndNull() {
        Stream.of(
            get(getAnnotation("b"), null),
            get(getAnnotation("d"), null)
        ).forEach(Assertions::assertFalse);
    }

    private boolean get(Annotation[] as, Object o) {
        return new InjectionNotNullAnnotationValidator().test(as, o);
    }

    private Annotation[] getAnnotation(final String name) {
        return getMethod(name).getParameters()[0].getAnnotations();
    }

    private Method getMethod(final String name) {
        try {
            return TestClass.class.getMethod(name, Object.class);
        } catch (NoSuchMethodException e) {
            throw new AssertionError("Method not exist, name: " + name, e);
        }
    }

    class TestClass {
        public void a(Object o) {
        }

        public void b(@CallOnNonNull Object o) {
        }

        public void c(@Mock Object o) {
        }

        public void d(@Mock @CallOnNonNull Object o) {
        }
    }

}
