/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.core.exceptions.StateNodeBeanSynchronizationException;
import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.io.Serializable;
import java.util.*;

import static io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManagerTest.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
public class StateNodeManagerInstanceFactoryTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    private StateNodeManager testObj;

    private BoundPojo instance;

    private ClassMetadata<BoundPojo> classMetadata;


    @BeforeEach
    void setup() {
        instance = new BoundPojo();
        classMetadata = ClassMetadata.ofClass(BoundPojo.class).withInstance(instance);
        testObj = new StateNodeManager(name -> classMetadata);
    }

    @Test
    @DisplayName("Should create detail on synchronization")
    void test_should_create_detail_on_synchronization() {
        instance.detail = null;

        Set<BeanNodeInstanceFactory> beanNodeInstanceFactories = new HashSet<>(Collections.singletonList(new TestBeanNodeInstanceFactory()));
        doReturn(beanNodeInstanceFactories).when(memberScanner).findInstancesBySuperType(BeanNodeInstanceFactory.class);

        testObj.addBindingPath("pojo.detail.data");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();
        StateNode selectedDetailNode = (StateNode) getPropertyValueOf(propertyValues.get("pojo"), "detail");
        ElementPropertyMap.getModel(selectedDetailNode).setProperty("data", "new value");

        List<BindingNodeSyncError> bindingNodeSyncErrors = testObj.synchronizeProperties();

        assertTrue(bindingNodeSyncErrors.isEmpty(), "There should not be any sync error");
        assertNotNull(instance.detail, "detail field of the test object should not be null");
        assertNotNull(instance.detail.data, "data field ot the detail property of the test object should not be null");
        assertEquals("new value", instance.detail.data);
    }

    @Test
    @DisplayName("Should indicate error, if value set on frontend to a property residing in a bean that is null on the backend")
    void should_indicate_error_if_value_set_on_frontend_to_a_property_residing_in_a_bean_that_is_null_on_the_backend() {
        instance.detail = null;
        doReturn(Collections.emptySet()).when(memberScanner).findInstancesBySuperType(BeanNodeInstanceFactory.class);

        testObj.addBindingPath("pojo.detail.data");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode selectedDetailNode = (StateNode) getPropertyValueOf(propertyValues.get("pojo"), "detail");
        ElementPropertyMap.getModel(selectedDetailNode).setProperty("data", "new value");

        List<BindingNodeSyncError> bindingNodeSyncErrors = testObj.synchronizeProperties();
        assertTrue(bindingNodeSyncErrors.get(0).getException() instanceof StateNodeBeanSynchronizationException);
        assertEquals("pojo.detail.data", bindingNodeSyncErrors.get(0).getPropertyPath());
        assertEquals("Cannot set property on null bean: detail", bindingNodeSyncErrors.get(0).getException().getMessage());
    }

    public static class TestBeanNodeInstanceFactory implements BeanNodeInstanceFactory<Detail> {
        @Override
        public boolean isApplicable(Class<Detail> clz) {
            return Detail.class.isAssignableFrom(clz);
        }

        @Override
        public Detail create(AbstractPropertyMetadata<Detail> propertyMetadata) {
            return new Detail();
        }
    }

    @Data
    public static class BoundPojo {

        private String name;
        private Detail detail;

    }

    @Data
    public static class Detail {

        private String data;

    }

}
