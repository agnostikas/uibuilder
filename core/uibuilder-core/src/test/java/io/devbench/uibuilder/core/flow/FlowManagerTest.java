/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import io.devbench.uibuilder.core.flow.exceptions.FlowDefinitionNotFound;
import io.devbench.uibuilder.core.flow.exceptions.FlowTargetNotExistsException;
import io.devbench.uibuilder.core.flow.exceptions.FlowUnauthorized;
import io.devbench.uibuilder.core.flow.exceptions.InterruptFlowNavigationException;
import io.devbench.uibuilder.core.flow.parsed.ParsedFlowElement;
import io.devbench.uibuilder.core.flow.parsed.ParsedUrlSegment;
import io.devbench.uibuilder.core.flow.xml.FlowDefinition;
import io.devbench.uibuilder.core.flow.xml.FlowElement;
import io.devbench.uibuilder.core.flow.xml.FlowTargetInherited;
import io.devbench.uibuilder.core.flow.xml.HasSubFlow;
import io.devbench.uibuilder.core.page.PageLoader;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mockito;

import javax.xml.bind.annotation.*;
import java.util.*;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class FlowManagerTest {

    @BeforeEach
    public void setup() {
        when(UI.getCurrent().getPage()).thenReturn(mock(Page.class, RETURNS_DEEP_STUBS));
        UIContext.getContext();
        CustomAuthFlowElement.isAuthenticated = true;
    }

    @Test
    public void should_register_the_newly_instantiated_flow_manager_to_the_ui_context() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        assertEquals(flowManager, FlowManager.getCurrent());
    }

    @Test
    public void should_load_correct_pages_from_url_based_on_simple_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/static");

        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/static.html");
    }

    @Test
    public void should_throw_page_not_found_exception_when_loading_non_existing_url() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        Assertions.assertThrows(FlowDefinitionNotFound.class, () -> flowManager.handleUrlRequest("/non-existing-url"));
    }

    @Test
    public void should_load_correct_page_even_if_url_does_not_start_with_slash() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("static");

        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/static.html");
    }

    @Test
    public void should_load_and_unload_correct_pages_from_url_based_on_simple_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/static");
        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/static.html");

        flowManager.handleUrlRequest("/empty");
        verify(flowTarget).unloadContent();
        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/empty.html");
    }

    @Test
    public void should_handle_navigation_correctly_based_on_simple_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("empty");
        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/empty.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "empty");
    }

    @Test
    public void should_load_correct_pages_from_url_based_on_complex_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/complex/complex-flow-definition.xml");
        FlowTarget uiFlowTarget = mock(FlowTarget.class);
        FlowTarget flowTarget1 = mock(FlowTarget.class);
        FlowTarget flowTarget2 = mock(FlowTarget.class);

        flowManager.registerFlowTarget("UI", uiFlowTarget);
        flowManager.registerFlowTarget("mainTarget", flowTarget1);
        flowManager.registerFlowTarget("mainTarget2", flowTarget2);
        flowManager.handleUrlRequest("/grid/uibuilder/collection/simple");

        verify(uiFlowTarget).loadContent("/FlowManagerTest/complex/base.html");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/collection/simple.html");
    }

    @Test
    public void should_load_and_unload_correct_pages_from_url_based_on_complex_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/complex/complex-flow-definition.xml");
        FlowTarget uiFlowTarget = mock(FlowTarget.class);
        FlowTarget flowTarget1 = mock(FlowTarget.class);
        FlowTarget flowTarget2 = mock(FlowTarget.class);

        flowManager.registerFlowTarget("UI", uiFlowTarget);
        flowManager.registerFlowTarget("mainTarget", flowTarget1);
        flowManager.registerFlowTarget("mainTarget2", flowTarget2);
        flowManager.handleUrlRequest("/grid/uibuilder/collection/simple");

        verify(uiFlowTarget).loadContent("/FlowManagerTest/complex/base.html");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/collection/simple.html");

        flowManager.handleUrlRequest("/grid/uibuilder/database/simple");
        verify(flowTarget1).unloadContent();
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/base2.html");
        verify(flowTarget2).loadContent("/FlowManagerTest/complex/database/simple.html");
    }

    @Test
    public void should_navigate_to_correct_pages_with_id_based_on_complex_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/complex/complex-flow-definition.xml");
        FlowTarget uiFlowTarget = mock(FlowTarget.class);
        FlowTarget flowTarget1 = mock(FlowTarget.class);
        FlowTarget flowTarget2 = mock(FlowTarget.class);

        flowManager.registerFlowTarget("UI", uiFlowTarget);
        flowManager.registerFlowTarget("mainTarget", flowTarget1);
        flowManager.registerFlowTarget("mainTarget2", flowTarget2);
        flowManager.navigate("collectionSimpleGrid");

        verify(uiFlowTarget).loadContent("/FlowManagerTest/complex/base.html");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/collection/simple.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "grid/uibuilder/collection/simple");
    }

    @Test
    public void should_load_and_unload_when_navigationg_correct_pages_with_id_based_on_complex_flow_definitions() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/complex/complex-flow-definition.xml");
        FlowTarget uiFlowTarget = mock(FlowTarget.class);
        FlowTarget flowTarget1 = mock(FlowTarget.class);
        FlowTarget flowTarget2 = mock(FlowTarget.class);

        flowManager.registerFlowTarget("UI", uiFlowTarget);
        flowManager.registerFlowTarget("mainTarget", flowTarget1);
        flowManager.registerFlowTarget("mainTarget2", flowTarget2);
        flowManager.navigate("collectionSimpleGrid");

        verify(uiFlowTarget).loadContent("/FlowManagerTest/complex/base.html");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/collection/simple.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "grid/uibuilder/collection/simple");

        flowManager.navigate("simpleGrid");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/base2.html");
        verify(flowTarget2).loadContent("/FlowManagerTest/complex/database/simple.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "grid/uibuilder/database/simple");
    }

    @Test
    public void reload_should_reload_all_flowtarget_again() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/complex/complex-flow-definition.xml");
        FlowTarget uiFlowTarget = mock(FlowTarget.class);
        FlowTarget flowTarget1 = mock(FlowTarget.class);
        FlowTarget flowTarget2 = mock(FlowTarget.class);

        flowManager.registerFlowTarget("UI", uiFlowTarget);
        flowManager.registerFlowTarget("mainTarget", flowTarget1);
        flowManager.registerFlowTarget("mainTarget2", flowTarget2);
        flowManager.handleUrlRequest("/grid/uibuilder/collection/simple");

        verify(uiFlowTarget).loadContent("/FlowManagerTest/complex/base.html");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/collection/simple.html");
        reset(uiFlowTarget, flowTarget1);

        PageLoader basePageLoader = mock(PageLoader.class);
        PageLoader simplePageLoader = mock(PageLoader.class);
        flowManager.registerPageLoader("/FlowManagerTest/complex/base.html", basePageLoader);
        flowManager.registerPageLoader("/FlowManagerTest/complex/collection/simple.html", simplePageLoader);

        flowManager.reload();
        verify(basePageLoader).unload();
        verify(simplePageLoader).unload();
        verify(uiFlowTarget).loadContent("/FlowManagerTest/complex/base.html");
        verify(flowTarget1).loadContent("/FlowManagerTest/complex/collection/simple.html");
    }

    @Test
    @DisplayName("Should unload page when reregistered")
    void test_should_unload_page_when_reregistered() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        PageLoader pageLoader = mock(PageLoader.class);
        PageLoader newPageLoader = mock(PageLoader.class);
        flowManager.registerPageLoader("testPath", pageLoader);
        flowManager.registerPageLoader("testPath", newPageLoader);

        verify(pageLoader).unload();
        verify(newPageLoader, never()).unload();

        Optional<PageLoader> foundPageLoader = flowManager.findPageLoader("testPath");
        assertTrue(foundPageLoader.isPresent());
        assertSame(newPageLoader, foundPageLoader.get());
    }

    @Test
    public void should_not_change_url_when_navigation_failed() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        doThrow(InterruptFlowNavigationException.class).when(flowTarget).loadContent("/FlowManagerTest/simple/html/empty.html");

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("empty");
        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/empty.html");
        verifyNoMoreInteractions(UI.getCurrent().getPage().getHistory());
    }

    @Test
    public void should_not_navigate_to_new_page_based_on_url_when_interrupt_exception_thrown() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/static");
        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/static.html");

        reset(flowTarget);

        doThrow(InterruptFlowNavigationException.class).when(flowTarget).loadContent("/FlowManagerTest/simple/html/empty.html");
        flowManager.handleUrlRequest("/empty");
        verify(flowTarget).unloadContent();
        verify(flowTarget).loadContent("/FlowManagerTest/simple/html/empty.html");

        verify(UI.getCurrent().getPage().getHistory(), atLeastOnce()).pushState(null, "static");
    }

    @Test
    public void should_redirect_page_when_custom_element_has_an_alternative_flow_id() {
        FlowManager.registerClassesToContext(CustomFlowElement.class);
        FlowManager flowManager = new FlowManager("/FlowManagerTest/redirects/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/admin");

        verify(flowTarget).loadContent("/FlowManagerTest/redirects/html/empty.html");
    }

    public static class CustomParsedFlowElement extends ParsedUrlSegment {

        CustomParsedFlowElement(
            @NotNull String id,
            @NotNull Supplier<Optional<String>> alternativeFlowId,
            @NotNull String navigationTarget,
            @NotNull String htmlPath
        ) {
            super(id, alternativeFlowId, navigationTarget, htmlPath);
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name = "customFlow")
    public static class CustomFlowElement extends FlowDefinition {

        @XmlAttribute
        private String fallbackId;

        @Override
        public CustomParsedFlowElement convertToParsedElement() {
            return new CustomParsedFlowElement(UUID.randomUUID().toString(), () -> Optional.ofNullable(fallbackId), "", "");
        }
    }

    public static class CustomParsedAuthFlowElement extends ParsedFlowElement {
        CustomParsedAuthFlowElement(@NotNull String id, @NotNull Supplier<Optional<String>> alternativeFlowId) {
            super(id, alternativeFlowId);
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name = "customAuthenticatedWrapper")
    public static class CustomAuthFlowElement extends FlowElement implements FlowTargetInherited, HasSubFlow {

        public static boolean isAuthenticated = true;

        @Getter
        @XmlElementRef
        private List<FlowElement> subFlows = new ArrayList<>();

        @Getter
        @XmlAttribute
        private String fallbackId;

        @Override
        public CustomParsedAuthFlowElement convertToParsedElement() {
            return new CustomParsedAuthFlowElement(UUID.randomUUID().toString(), this::provideFallbackId);
        }

        @NotNull
        private Optional<String> provideFallbackId() {
            if (isAuthenticated) {
                return Optional.ofNullable(fallbackId);
            } else {
                throw new FlowUnauthorized();
            }
        }
    }

    @Test
    public void should_correctly_call_unload_on_pages_when_te_parsed_segments_contains_non_url_segments() {
        FlowManager.registerClassesToContext(CustomAuthFlowElement.class);
        FlowManager flowManager = new FlowManager("/FlowManagerTest/unload-wrapper/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/user/details");

        verify(flowTarget).loadContent("/FlowManagerTest/redirects/html/userPage.html");

        flowManager.navigate("empty");
        InOrder inOrderFlowTarget = Mockito.inOrder(flowTarget);
        inOrderFlowTarget.verify(flowTarget).loadContent("/FlowManagerTest/redirects/html/empty.html");
    }

    @Test
    public void should_dismiss_internal_flow_elements_when_loading_base_url() {
        FlowManager.registerClassesToContext(CustomAuthFlowElement.class);
        FlowManager flowManager = new FlowManager("/FlowManagerTest/load-wrapper/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/");
        verify(flowTarget).loadContent("/FlowManagerTest/redirects/html/base.html");
        verify(flowTarget, never()).loadContent("/FlowManagerTest/redirects/html/empty.html");
    }

    @Test
    public void should_throw_flow_target_not_found_exception_when_target_is_not_yet_defined() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/flow-target-missing/flow.xml");
        Assertions.assertThrows(FlowTargetNotExistsException.class, () -> flowManager.handleUrlRequest("/empty"));
    }

    @Test
    public void should_load_urls_with_parameters_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.handleUrlRequest("/companies/test-company");

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company");
    }

    @Test
    public void should_navigate_to_flow_with_parameters_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("companyDetails", Collections.singletonMap("company-id", "test-company"), Collections.emptyMap());

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company");
    }

    @Test
    public void should_navigate_to_flow_with_query_parameters_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate(
            "companyDetails",
            Collections.singletonMap("company-id", "test-company"),
            Collections.singletonMap("redirect-url", Collections.singletonList("companies"))
        );

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        assertEquals(Collections.singletonList("companies"), flowParameterProvider.getQueryParameters("redirect-url"));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company?redirect-url=companies");
    }

    @Test
    public void should_navigate_to_flow_with_multiple_query_parameters_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        Map<String, List<String>> queryParameters = new LinkedHashMap<>();
        queryParameters.put("redirect-url", Collections.singletonList("companies"));
        queryParameters.put("some-other", Collections.singletonList("query-param"));
        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("companyDetails", Collections.singletonMap("company-id", "test-company"), queryParameters);

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        assertEquals(Collections.singletonList("companies"), flowParameterProvider.getQueryParameters("redirect-url"));
        assertEquals(Collections.singletonList("query-param"), flowParameterProvider.getQueryParameters("some-other"));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company?redirect-url=companies&some-other=query-param");
    }

    @Test
    public void should_clear_the_flow_parameters_when_navigating_to_a_flow_element_where_no_parameters_provided() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("companyDetails", Collections.singletonMap("company-id", "test-company"), Collections.emptyMap());

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company");

        flowManager.navigate("companyList");
        assertEquals(Optional.empty(), flowParameterProvider.getUrlParameter("company-id"));

        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_list.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies");
    }

    @Test
    public void should_clear_the_flow_parameters_when_navigating_to_a_flow_element_by_url_where_no_parameters_provided() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("companyDetails", Collections.singletonMap("company-id", "test-company"), Collections.emptyMap());

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company");

        flowManager.handleUrlRequest("companies");
        assertEquals(Optional.empty(), flowParameterProvider.getUrlParameter("company-id"));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_list.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies");
    }

    @Test
    public void should_keep_the_same_flow_parameters_when_navigating_to_a_flow_element_by_url_where_some_parameters_the_same() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put("contact-id", "test-contact");
        urlParameters.put("company-id", "test-company");

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("companyContacts", urlParameters, Collections.emptyMap());

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        assertEquals("test-contact", flowParameterProvider.getUrlParameter("contact-id").orElse(null));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_contacts.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company/contacts/test-contact");

        flowManager.navigate("companyDetails");
        assertEquals(Optional.of("test-company"), flowParameterProvider.getUrlParameter("company-id"));
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company");
    }

    @Test
    public void when_just_the_parameter_changes_flow_should_unload_the_parts_after_the_url_parameter_and_change_the_url_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow-complex.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);
        FlowTarget baseTarget = mock(FlowTarget.class);
        FlowTarget companyDetailsTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.registerFlowTarget("companyDetails", companyDetailsTarget);
        flowManager.registerFlowTarget("base", baseTarget);

        flowManager.navigate("companyDeletion", Collections.singletonMap("company-id", "test-company"), Collections.emptyMap());

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        verify(flowTarget).loadContent("/base.html");
        verify(baseTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(companyDetailsTarget).loadContent("/FlowManagerTest/parameters/html/company_deletion.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company/delete");

        reset(flowTarget, baseTarget, companyDetailsTarget);

        flowManager.navigate("companyDeletion", Collections.singletonMap("company-id", "test-company-2"), Collections.emptyMap());

        assertEquals("test-company-2", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        verify(baseTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(companyDetailsTarget).loadContent("/FlowManagerTest/parameters/html/company_deletion.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/test-company-2/delete");
        verifyNoMoreInteractions(flowTarget);
    }

    @Test
    public void should_handle_included_flow_definitions_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/include/flow.xml");
        FlowTarget mainMenuTarget = mock(FlowTarget.class);
        FlowTarget uiTarget = mock(FlowTarget.class);
        FlowTarget subFlowDefinitionTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainMenuWithUIBuilderPage", mainMenuTarget);
        flowManager.registerFlowTarget("basePageWithMenu", subFlowDefinitionTarget);
        flowManager.registerFlowTarget("UI", uiTarget);
        flowManager.handleUrlRequest("/module-a/info");

        verify(uiTarget).loadContent("/webapp/base.html");
        verify(mainMenuTarget).loadContent("/webapp/main.html");
        verify(subFlowDefinitionTarget).loadContent("/webapp/info.html");
    }


    @Test
    public void should_handle_included_flow_definitions_correctly_when_the_included_maps_to_the_root() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/include/flow.xml");
        FlowTarget mainMenuTarget = mock(FlowTarget.class);
        FlowTarget uiTarget = mock(FlowTarget.class);
        FlowTarget subFlowDefinitionTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainMenuWithUIBuilderPage", mainMenuTarget);
        flowManager.registerFlowTarget("basePageWithMenu", subFlowDefinitionTarget);
        flowManager.registerFlowTarget("UI", uiTarget);
        flowManager.handleUrlRequest("/module-a");

        verify(uiTarget).loadContent("/webapp/base.html");
        verify(mainMenuTarget).loadContent("/webapp/main.html");
    }

    @Test
    public void should_handle_query_parameters_in_url_requests_correctly() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget mainTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", mainTarget);
        flowManager.handleUrlRequest("/companies/company-a", Collections.singletonMap("referer", Collections.singletonList("admin-a")));

        verify(mainTarget).loadContent("/FlowManagerTest/parameters/html/company_details.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "companies/company-a?referer=admin-a");
    }


    @Test
    public void should_unload_all_content_when_the_user_navigates_back_to_the_parent_page() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/back_to_parent/flow.xml");
        FlowTarget mainTarget = mock(FlowTarget.class);
        FlowTarget uiTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainTarget", mainTarget);
        flowManager.registerFlowTarget("UI", uiTarget);
        flowManager.handleUrlRequest("/child");

        verify(uiTarget).loadContent("/FlowManagerTest/back_to_parent/base.html");
        verify(mainTarget).loadContent("/FlowManagerTest/back_to_parent/child.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "child");

        flowManager.navigate("base");

        verify(mainTarget).unloadContent();
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "");
    }

    @Test
    public void should_handle_included_flow_definitions_correctly_even_when_included_root_element_is_not_a_flow_definition() {
        FlowManager.registerClassesToContext(CustomAuthFlowElement.class);

        FlowManager flowManager = new FlowManager("/FlowManagerTest/include-with-custom-element/flow.xml");
        FlowTarget mainMenuTarget = mock(FlowTarget.class);
        FlowTarget uiTarget = mock(FlowTarget.class);
        FlowTarget subFlowDefinitionTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget("mainMenuWithUIBuilderPage", mainMenuTarget);
        flowManager.registerFlowTarget("basePageWithMenu", subFlowDefinitionTarget);
        flowManager.registerFlowTarget("UI", uiTarget);
        flowManager.handleUrlRequest("/module-a/info");

        verify(uiTarget).loadContent("/webapp/base.html");
        verify(mainMenuTarget).loadContent("/webapp/main.html");
        verify(subFlowDefinitionTarget).loadContent("/webapp/info.html");
    }

    @Test
    public void should_handle_the_url_changes_correctly_if_a_loaded_page_navigates_somewhere_else_in_the_on_before_load_method() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);
        ArgumentCaptor<String> urlArgCaptor = ArgumentCaptor.forClass(String.class);

        flowManager.registerFlowTarget("mainTarget", flowTarget);

        doAnswer(invocation -> {
            flowManager.navigate("empty");
            return null;
        }).when(flowTarget).loadContent("/FlowManagerTest/simple/html/static.html");

        flowManager.handleUrlRequest("/static");

        verify(UI.getCurrent().getPage().getHistory(), atLeastOnce()).pushState(any(), urlArgCaptor.capture());

        List<String> urlArguments = urlArgCaptor.getAllValues();
        assertEquals("empty", urlArguments.get(urlArguments.size() - 1));
    }

    @Test
    public void when_multiple_inner_flow_elements_and_a_leaf_are_defined_on_the_root_url_we_should_load_the_correct_flow_leaf() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/enhanced/multiple_inner_root_and_a_leaf.xml");
        FlowTarget uiFlowTarget = mock(FlowTarget.class);
        FlowTarget rootPageTarget = mock(FlowTarget.class);
        FlowTarget mobileTarget = mock(FlowTarget.class);

        flowManager.registerFlowTarget(FlowDefinition.UI, uiFlowTarget);
        flowManager.registerFlowTarget("rootPageTarget", rootPageTarget);
        flowManager.registerFlowTarget("mobileTarget", mobileTarget);

        flowManager.handleUrlRequest("/");

        verify(uiFlowTarget).loadContent("/base.html");
        verify(rootPageTarget).loadContent("/mobile-wrapper.html");
        verify(mobileTarget).loadContent("/main.html");
    }

    @Test
    @DisplayName("Should clear query parameters when called with clearQueryParameters true")
    void test_should_clear_query_parameters_when_called_with_clear_query_parameters_true() {
        FlowManager flowManager = new FlowManager("/FlowManagerTest/parameters/flow.xml");
        FlowTarget flowTarget = mock(FlowTarget.class);

        Map<String, List<String>> queryParameters = new LinkedHashMap<>();
        queryParameters.put("some-param", Collections.singletonList("query-param"));
        flowManager.registerFlowTarget("mainTarget", flowTarget);
        flowManager.navigate("companyDetails", Collections.singletonMap("company-id", "test-company"), queryParameters);

        FlowParameterProvider flowParameterProvider = FlowParameterProvider.getInstance();
        assertEquals("test-company", flowParameterProvider.getUrlParameter("company-id").orElse(null));
        assertEquals(Collections.singletonList("query-param"), flowParameterProvider.getQueryParameters("some-param"));

        flowManager.navigate("otherSection", true);

        assertTrue(flowParameterProvider.getAllQueryParameters().isEmpty());
        verify(flowTarget).loadContent("/FlowManagerTest/parameters/html/other.html");
        verify(UI.getCurrent().getPage().getHistory()).pushState(null, "other");
    }

    @Nested
    public class FlowIncludeNavigationWithIdShouldAllowCollapsedIds {

        @Test
        public void should_navigate_to_the_included_element_by_id_correctly() {
            FlowManager flowManager = new FlowManager("/FlowManagerTest/InclusionTests/flow.xml");
            FlowTarget uiTarget = mock(FlowTarget.class);

            flowManager.registerFlowTarget(FlowDefinition.UI, uiTarget);

            flowManager.navigate("mgmc");

            verify(uiTarget).loadContent("mgmc.html");
            verify(UI.getCurrent().getPage().getHistory(), atLeastOnce()).pushState(any(), eq("mgmc"));
        }

        @Test
        public void should_navigate_to_the_included_element_by_the_included_id_correctly() {
            FlowManager flowManager = new FlowManager("/FlowManagerTest/InclusionTests/flow.xml");
            FlowTarget uiTarget = mock(FlowTarget.class);

            flowManager.registerFlowTarget(FlowDefinition.UI, uiTarget);

            flowManager.navigate("mgmcRoot");

            verify(uiTarget).loadContent("mgmc.html");
            verify(UI.getCurrent().getPage().getHistory(), atLeastOnce()).pushState(any(), eq("mgmc"));
        }

        @Test
        public void should_navigate_to_the_included_element_by_url_correctly() {
            FlowManager flowManager = new FlowManager("/FlowManagerTest/InclusionTests/flow.xml");
            FlowTarget uiTarget = mock(FlowTarget.class);

            flowManager.registerFlowTarget(FlowDefinition.UI, uiTarget);

            flowManager.handleUrlRequest("/mgmc");

            verify(uiTarget).loadContent("mgmc.html");
            verify(UI.getCurrent().getPage().getHistory(), atLeastOnce()).pushState(any(), eq("mgmc"));
        }
    }

    @Nested
    @DisplayName("When navigating to a restricted flow leaf without permission we should throw the corresponding Exception")
    public class ShouldThrowUnauthenticatedExceptionWhenNavigatingWithoutPermission {

        @Test
        public void when_the_flow_element_throws_an_exception_on_url_request_it_is_rethrown_and_nothing_is_loaded() {
            FlowManager.registerClassesToContext(CustomAuthFlowElement.class);
            CustomAuthFlowElement.isAuthenticated = false;
            FlowManager flowManager = new FlowManager("/FlowManagerTest/unload-wrapper/flow.xml");
            FlowTarget flowTarget = mock(FlowTarget.class);

            flowManager.registerFlowTarget("mainTarget", flowTarget);
            assertThrows(FlowUnauthorized.class, () -> flowManager.handleUrlRequest("/user/details"));

            verify(flowTarget, never()).loadContent("/FlowManagerTest/redirects/html/userPage.html");
        }

        @Test
        public void when_the_flow_element_throws_an_exception_on_navigation_it_is_rethrown_and_nothing_is_loaded() {
            FlowManager.registerClassesToContext(CustomAuthFlowElement.class);
            CustomAuthFlowElement.isAuthenticated = false;
            FlowManager flowManager = new FlowManager("/FlowManagerTest/unload-wrapper/flow.xml");
            FlowTarget flowTarget = mock(FlowTarget.class);

            flowManager.registerFlowTarget("mainTarget", flowTarget);
            assertThrows(FlowUnauthorized.class, () -> flowManager.navigate("userPage"));

            verify(flowTarget, never()).loadContent("/FlowManagerTest/redirects/html/userPage.html");
        }

    }
}
