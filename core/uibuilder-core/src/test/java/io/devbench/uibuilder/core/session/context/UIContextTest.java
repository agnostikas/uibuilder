/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.session.context;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class UIContextTest {

    private UIContext testObj;

    @BeforeEach
    private void setup() {
        testObj = UIContext.getContext();
        when(UI.getCurrent().getUIId()).thenReturn(1);
        testObj.getSessionSeparatedUIContextMap().clear();
    }

    @Test
    @DisplayName("Should check if the instance passed to the ui context is ui context stored, and store it to the current ui instance")
    public void should_check_if_the_instance_passed_to_the_ui_context_is_ui_context_stored_and_store_it_to_the_current_ui_instance() {
        TestUIContextElement contextElement = new TestUIContextElement();

        testObj.addToActiveUIContext(contextElement);

        assertThrows(IllegalStateException.class, () -> testObj.addToActiveUIContext(new TestNonUIContextElement()));

        Map<Class<?>, Map<Integer, Object>> contextMap = testObj.getSessionSeparatedUIContextMap();

        assertAll(
            () -> assertEquals(1, contextMap.size()),
            () -> assertEquals(1, contextMap.get(TestUIContextElement.class).size()),
            () -> assertTrue(contextMap.get(TestUIContextElement.class).containsKey(1)),
            () -> assertSame(contextElement, contextMap.get(TestUIContextElement.class).get(1))
        );
    }

    @Test
    @DisplayName("Should add detach listener to ui that removes the context element from the context map")
    public void should_add_detach_listener_to_ui_that_removes_the_context_element_from_the_context_map() {
        TestUIContextElement contextElement = new TestUIContextElement();

        testObj.addToActiveUIContext(contextElement);

        Map<Class<?>, Map<Integer, Object>> contextMap = testObj.getSessionSeparatedUIContextMap();

        assertTrue(contextMap.get(TestUIContextElement.class).values().contains(contextElement));

        ArgumentCaptor<ComponentEventListener<DetachEvent>> detachListenerCaptor = ArgumentCaptor.forClass(ComponentEventListener.class);
        verify(UI.getCurrent()).addDetachListener(detachListenerCaptor.capture());

        DetachEvent event = mock(DetachEvent.class);
        when(event.getSource()).thenReturn(UI.getCurrent());
        detachListenerCaptor.getValue().onComponentEvent(event);

        assertFalse(contextMap.get(TestUIContextElement.class).values().contains(contextElement));
    }

    @Test
    @DisplayName("Should get the stored instance from the ui context with the specified type, if it is available in the context")
    public void should_get_the_stored_instance_from_the_ui_context_with_the_specified_type_if_it_is_available_in_the_context() {
        TestUIContextElement shouldBeNull = testObj.get(TestUIContextElement.class);

        testObj.addToActiveUIContext(new TestUIContextElement());

        TestUIContextElement shouldBeNonNull = testObj.get(TestUIContextElement.class);

        assertAll(
            () -> assertNull(shouldBeNull),
            () -> assertNotNull(shouldBeNonNull)
        );
    }

    @Test
    @DisplayName("Should get the element from the context or compute it using the supplier and store it to the context")
    public void should_get_the_element_from_the_context_or_compute_it_using_the_supplier_and_store_it_to_the_context() {
        TestUIContextElement element = testObj.computeIfAbsent(TestUIContextElement.class, TestUIContextElement::new);

        TestUIContextElement otherElement = testObj.computeIfAbsent(TestUIContextElement.class, TestUIContextElement::new);

        assertSame(element, otherElement);
    }

    @Test
    @DisplayName("Should store element marked as one of the super class, and when asked for the super class it should return the instance")
    public void should_store_element_marked_as_one_of_the_super_class_and_when_asked_for_the_super_class_it_should_return_the_instance() {
        TestSubClassElement subElement = new TestSubClassElement();
        testObj.addToActiveUIContextAs(TestUIContextElement.class, subElement);

        TestUIContextElement shouldBeSubElement = testObj.get(TestUIContextElement.class);
        TestSubClassElement shouldBeNull = testObj.get(TestSubClassElement.class);

        assertAll(
            () -> assertSame(subElement, shouldBeSubElement),
            () -> assertNull(shouldBeNull)
        );
    }


    @UIContextStored
    public static class TestUIContextElement {}

    public static class TestSubClassElement extends TestUIContextElement {}

    public static class TestNonUIContextElement {}

}
