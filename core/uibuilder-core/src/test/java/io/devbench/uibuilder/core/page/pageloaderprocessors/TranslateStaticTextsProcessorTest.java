/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.i18n.core.I;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(JsoupExtension.class)
class TranslateStaticTextsProcessorTest {

    @LoadElement(value = "/page-with-translatable-texts-in-it.html", id = "page")
    private Element pageElement;

    private TranslateStaticTextsProcessor testObj = new TranslateStaticTextsProcessor();

    @Test
    @DisplayName("Should collect and translate all texts marked with tr")
    public void should_collect_and_translate_all_texts_marked_with_tr() {
        I.setLanguageProvider(() -> new Locale("hu"));
        I.loadTranslations("/");

        PageLoaderContext context = new PageLoaderContext();
        context.setPageElement(pageElement);

        testObj.process(context);

        assertAll(
            () -> assertEquals("Fordíts le", context.getPageElement().getElementById("textfield").attr("value")),
            () -> assertEquals("Fordíts engem is le", context.getPageElement().getElementById("div").text()),
            () -> assertEquals("tr(\"don't translate me')", context.getPageElement().getElementById("wrongTr").text())
        );
    }

}
