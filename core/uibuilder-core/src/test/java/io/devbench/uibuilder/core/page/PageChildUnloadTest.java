/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PageChildUnloadTest {

    @InjectMocks
    private Page testObj;

    @Mock
    private DomBind domBind;

    @Mock
    private PageLoader pageLoader;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private FlowManager flowManager;

    @Mock
    private UIContext uiContext;

    @BeforeEach
    void setUp() {
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);
        when(uiContext.get(FlowManager.class)).thenReturn(flowManager);
    }

    @Test
    @DisplayName("Should unload child pages if exists")
    void test_should_unload_child_pages_if_exists() {
        testObj = spy(testObj);
        doNothing().when(testObj).remove(any(Component.class));

        Page childPage1 = mock(Page.class);
        Page childPage2 = mock(Page.class);
        PageLoader childPageLoader1 = mockPageLoader(childPage1);
        PageLoader childPageLoader2 = mockPageLoader(childPage2);

        mockParentPage(childPage2, testObj);

        Collection<PageLoader> mockPageLoaders = Arrays.asList(childPageLoader1, childPageLoader2);
        when(flowManager.getPageLoaders()).thenReturn(mockPageLoaders);

        testObj.unloadContent();

        verify(pageLoader).unload();
        verify(testObj).remove(domBind);
        verify(childPageLoader2).unload();
        verify(childPageLoader1, never()).unload();
    }

    @NotNull
    private PageLoader mockPageLoader(@Nullable Page page) {
        PageLoader loader = mock(PageLoader.class);
        PageLoaderContext loaderContext = mock(PageLoaderContext.class);
        DomBind domBind = mock(DomBind.class);

        when(loader.getContext()).thenReturn(loaderContext);
        when(loaderContext.getDomBind()).thenReturn(domBind);
        when(domBind.getParent()).thenReturn(Optional.ofNullable(page));

        return loader;
    }

    private void mockParentPage(@NotNull Page page, @Nullable Page parentPage) {
        DomBind domBind = mock(DomBind.class);
        when(domBind.getParent()).thenReturn(Optional.ofNullable(parentPage));
        when(page.getParent()).thenReturn(Optional.of(domBind));
    }

}
