/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.page.exceptions.InvalidDocumentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReadDocumentToContextProcessorTest {

    private ReadDocumentToContextProcessor testObj;

    @BeforeEach
    public void setup() {
        testObj = new ReadDocumentToContextProcessor();
    }

    @Test
    @DisplayName("Should load html document to context, specified by the htmlPath attribute of the context")
    public void should_load_html_document_to_context_specified_by_the_html_path_attribute_of_the_context() {
        PageLoaderContext context = new PageLoaderContext("/page-test-element.html", null);

        testObj.process(context);

        assertAll(
            () -> assertNotNull(context.getDocument()),
            () -> assertEquals("uibuilder-page", context.getDocument().body().getAllElements().get(1).tagName())
        );
    }

    @Test
    @DisplayName("Should throw exception if the document is invalid")
    public void should_throw_exception_if_the_document_is_invalid() {
        PageLoaderContext context = new PageLoaderContext("/invalid-path.html", null);
        InvalidDocumentException exception = assertThrows(InvalidDocumentException.class, () -> testObj.process(context));
        assertEquals("Invalid html, exception thrown during page loading. Path: /invalid-path.html", exception.getMessage());
    }

}
