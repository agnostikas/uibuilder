/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty.converters;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.templateparser.UIBuilderMethodReference;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.MethodMetadata;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class FunctionPropertyConverterTest {

    private final FunctionPropertyConverter testObj = new FunctionPropertyConverter();

    @Test
    public void should_convert_supplier_to_string_and_string_to_the_same_supplier_correctly() {
        Function<?, ?> supplier = (Function<?, ?> & Serializable) (v) -> null;

        String supplierId = testObj.convertTo(supplier);
        Function<?, ?> actual = testObj.apply(supplierId);

        assertEquals(supplier, actual);
    }

    public static class TestControllerBean {
        public boolean withObjectCalled = false;
        public boolean withStringCalled = false;

        public String fooBar(Object o) {
            withObjectCalled = true;
            return "";
        }

        public String fooBar(String s) {
            withStringCalled = true;
            return s;
        }
    }

    @Test
    public void should_supply_suppliers_from_uiuilder_method_reference_objects_correctly() {
        TestControllerBean testControllerBean = new TestControllerBean();

        UIBuilderMethodReference uiBuilderMethodReference = new UIBuilderMethodReference(
            TestControllerBean.class,
            testControllerBean,
            ClassMetadata.ofValue(testControllerBean)
                .getMethods()
                .stream()
                .filter(it -> Objects.equals(it.getName(), "fooBar"))
                .map(this::unreflect)
                .collect(Collectors.toMap(
                    MethodHandle::type,
                    Function.identity()
                ))
        );

        VaadinSession.getCurrent().setAttribute("test", uiBuilderMethodReference);

        assertFalse(testControllerBean.withObjectCalled);
        assertFalse(testControllerBean.withStringCalled);
        ((Function) testObj.apply("test")).apply("string");
        assertFalse(testControllerBean.withObjectCalled);
        assertTrue(testControllerBean.withStringCalled);
    }

    public static class TestControllerBeanWithNotEqualParameterMethodHanlde {
        public boolean withObjectCalled = false;

        public String fooBar(Object o) {
            withObjectCalled = true;
            return "";
        }
    }

    @Test
    public void should_supply_suppliers_from_uibuilder_method_reference_when_the_parameters_assignable_in_runtime_but_has_different_static_type() {
        TestControllerBeanWithNotEqualParameterMethodHanlde testControllerBean = new TestControllerBeanWithNotEqualParameterMethodHanlde();

        UIBuilderMethodReference uiBuilderMethodReference = new UIBuilderMethodReference(
            TestControllerBeanWithNotEqualParameterMethodHanlde.class,
            testControllerBean,
            ClassMetadata.ofValue(testControllerBean)
                .getMethods()
                .stream()
                .filter(it -> Objects.equals(it.getName(), "fooBar"))
                .map(this::unreflect)
                .collect(Collectors.toMap(
                    MethodHandle::type,
                    Function.identity()
                ))
        );

        VaadinSession.getCurrent().setAttribute("test", uiBuilderMethodReference);

        assertFalse(testControllerBean.withObjectCalled);
        ((Function) testObj.apply("test")).apply("string");
        assertTrue(testControllerBean.withObjectCalled);
    }

    @SneakyThrows
    private MethodHandle unreflect(MethodMetadata<?> it) {
        return it.unreflect();
    }

}
