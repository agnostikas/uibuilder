/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow.parsed;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ParsedFlowElementTest {

    @Test
    public void should_return_empty_optional_when_this_flow_element_has_no_parent_and_alternative_flow_id_is_null() {
        ParsedFlowElement testObj = new ParsedFlowElement(UUID.randomUUID().toString(), null, Optional::empty);
        assertEquals(Optional.empty(), testObj.calcAlternativeFlowId());
    }

    @Test
    public void should_return_suppliers_optional_when_this_flow_element_has_no_parent_and_alternative_flow_id_is_available() {
        ParsedFlowElement testObj = new ParsedFlowElement(UUID.randomUUID().toString(), null, () -> Optional.of("alternativeId"));
        assertEquals(Optional.of("alternativeId"), testObj.calcAlternativeFlowId());
    }

    @Test
    public void should_return_suppliers_optional_when_this_flow_element_has_a_parent_but_that_parent_has_no_alternative_flow_id() {
        ParsedFlowElement parentObj = mock(ParsedFlowElement.class);
        ParsedFlowElement testObj = new ParsedFlowElement(UUID.randomUUID().toString(), parentObj, () -> Optional.of("alternativeId"));
        when(parentObj.calcAlternativeFlowId()).thenReturn(Optional.empty());
        assertEquals(Optional.of("alternativeId"), testObj.calcAlternativeFlowId());
    }

    @Test
    public void should_return_parents_optional_when_this_flow_element_has_a_parent_and_that_parent_has_alternative_flow_id() {
        ParsedFlowElement parentObj = mock(ParsedFlowElement.class);
        ParsedFlowElement testObj = new ParsedFlowElement(UUID.randomUUID().toString(), parentObj, () -> Optional.of("alternativeId"));
        when(parentObj.calcAlternativeFlowId()).thenReturn(Optional.of("parentAlternativeFlowId"));
        assertEquals(Optional.of("parentAlternativeFlowId"), testObj.calcAlternativeFlowId());
    }

}
