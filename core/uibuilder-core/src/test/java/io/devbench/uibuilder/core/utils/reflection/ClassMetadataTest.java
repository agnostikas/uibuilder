/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.core.utils.reflection.testclasses.TestAnnotation;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestClass;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestExtendedClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ClassMetadataTest {

    private ClassMetadata<? extends TestClass> testObj;

    @Test
    @DisplayName("Should use the given instance, and cache the properties and methods on it")
    public void should_use_the_given_instance_and_cache_properties_and_methods_on_it() {
        TestClass testInstance = new TestClass();
        testObj = ClassMetadata.ofClass(TestClass.class).withInstance(testInstance);

        assertAll(
            () -> assertNotNull(testObj.getInstance()),
            () -> assertSame(testInstance, testObj.getInstance()),
            () -> assertNotNull(testObj.getPropertyCache()),
            () -> assertFalse(testObj.getPropertyCache().isEmpty()),
            () -> assertNotNull(testObj.getMethods()),
            () -> assertFalse(testObj.getMethods().isEmpty())
        );
    }

    @Test
    public void should_return_the_same_cached_object_for_the_same_class() {
        assertSame(ClassMetadata.ofClass(TestClass.class), ClassMetadata.ofClass(TestClass.class));
    }

    @Test
    @DisplayName("Should find property by path")
    public void should_find_property_by_path() {
        testObj = ClassMetadata.ofClass(TestClass.class).withInstance(new TestClass());

        Optional<PropertyMetadata<?>> property = testObj.property("testInnerClass.value");
        assertAll(
            () -> assertTrue(property.isPresent()),
            () -> assertEquals("value", property.get().getName())
        );

        testObj.getInstance().getTestInnerClass().setInner(testObj.getInstance().getTestInnerClass());

        Optional<PropertyMetadata<?>> deepProperty = testObj.property("testInnerClass.inner.inner.inner.value");
        assertAll(
            () -> assertTrue(deepProperty.isPresent()),
            () -> assertEquals("value", deepProperty.get().getName())
        );
    }

    @Test
    @DisplayName("Should get property value by the property path")
    public void should_get_property_value_by_its_path() {
        TestClass testInstance = new TestClass();
        testInstance.getTestInnerClass().setInner(testInstance.getTestInnerClass());
        testObj = ClassMetadata.ofClass(TestClass.class).withInstance(testInstance);

        Optional<String> optionalValue = testObj.getPropertyValue("testInnerClass.inner.inner.inner.inner.inner.value");
        assertAll(
            () -> assertTrue(optionalValue.isPresent()),
            () -> assertEquals("testValue", optionalValue.get())
        );
    }

    @Test
    @DisplayName("Should set property value by the property path")
    public void should_set_property_value_by_property_path() {
        TestClass testInstance = new TestClass();
        testInstance.getTestInnerClass().setInner(testInstance.getTestInnerClass());
        testObj = ClassMetadata.ofClass(TestClass.class).withInstance(testInstance);

        testObj.setPropertyValue("testInnerClass.inner.inner.value", "set by test");

        assertEquals("set by test", testInstance.getTestInnerClass().getValue());
    }

    @Test
    @DisplayName("Should return Optional.empty, if property cannot be found, by the given path")
    public void should_return_empty_optional_if_property_cannot_be_found() {
        testObj = ClassMetadata.ofClass(TestClass.class);

        Optional<PropertyMetadata<?>> property = testObj.property("testInnerClass.invalid_property_path");

        assertFalse(property.isPresent());
    }

    @Test
    @DisplayName("Should find annotations defined on class")
    public void should_find_the_annotations_defined_on_class() {
        testObj = ClassMetadata.ofClass(TestClass.class);

        TestAnnotation annotation = testObj.getAnnotation(TestAnnotation.class);

        assertAll(
            () -> assertNotNull(annotation),
            () -> assertEquals("on test class", annotation.value())
        );
    }

    @Test
    @DisplayName("Should find super class fields and methods")
    void should_find_super_class_fields_and_methods() {
        testObj = ClassMetadata.ofClass(TestExtendedClass.class);

        assertTrue(testObj.property("testInnerClass").isPresent(), "Super class field should be available");
        assertTrue(testObj.property("extended").isPresent(), "Directly declared field should be available");
        assertTrue(testObj.getMethods().stream().map(MethodMetadata::getName).anyMatch("testMethod"::equals), "testMethod should be found");
    }

    public static class UninstantiableClass {
        private UninstantiableClass() {
        }
    }

}
