/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestAnnotation;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestClass;
import io.devbench.uibuilder.core.utils.reflection.testclasses.TestInnerClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


class PropertyMetadataTest {

    private TestClass instance;

    private PropertyMetadata<TestClass> testObj;

    @BeforeEach
    public void setup() {
        instance = new TestClass();
    }

    @Test
    @DisplayName("Should build metadata from field information")
    public void should_build_metadata_from_field_information() throws NoSuchFieldException {
        Field field = TestClass.class.getDeclaredField("testInnerClass");

        testObj = new PropertyMetadata<>(instance, TestClass.class, field);

        assertAll(
            () -> assertEquals("testInnerClass", testObj.getName()),
            () -> assertSame(field, testObj.getField()),
            () -> assertNotNull(testObj.getGetter()),
            () -> assertNotNull(testObj.getSetter()),
            () -> assertEquals("getTestInnerClass", testObj.getGetter().getName()),
            () -> assertEquals("setTestInnerClass", testObj.getSetter().getName()),
            () -> assertEquals(TestInnerClass.class, testObj.getType())
        );
    }

    @Test
    @DisplayName("Should build metadata from getter")
    public void should_build_metadata_from_getter() throws NoSuchMethodException, NoSuchFieldException {
        Method getter = TestClass.class.getDeclaredMethod("getTestInnerClass");
        Field field = TestClass.class.getDeclaredField("testInnerClass");

        testObj = new PropertyMetadata<>(instance, TestClass.class, getter);

        assertAll(
            () -> assertEquals("testInnerClass", testObj.getName()),
            () -> assertNotNull(testObj.getField()),
            () -> assertNotNull(testObj.getGetter()),
            () -> assertNotNull(testObj.getSetter()),
            () -> assertEquals(field, testObj.getField()),
            () -> assertSame(getter, testObj.getGetter()),
            () -> assertEquals("getTestInnerClass", testObj.getGetter().getName()),
            () -> assertEquals("setTestInnerClass", testObj.getSetter().getName()),
            () -> assertEquals(TestInnerClass.class, testObj.getType())
        );
    }

    @Test
    @DisplayName("Should get value of property")
    public void should_get_value_of_property() throws NoSuchFieldException {
        Field field = TestClass.class.getDeclaredField("testInnerClass");

        testObj = new PropertyMetadata<>(instance, TestClass.class, field);

        TestInnerClass value = testObj.getValue();

        assertAll(
            () -> assertNotNull(value),
            () -> assertSame(instance.getTestInnerClass(), value)
        );
    }

    @Test
    @DisplayName("Should get value of property, even if the field is unaccessable and there is no getter for it")
    public void should_get_value_of_property_even_if_field_is_unaccessable_and_there_is_no_getter() throws NoSuchFieldException {
        Field field = TestClassWithUnaccessableField.class.getDeclaredField("testField");

        PropertyMetadata<TestClassWithUnaccessableField> testObj =
            new PropertyMetadata<>(new TestClassWithUnaccessableField(), TestClassWithUnaccessableField.class, field);

        assertEquals("test value", testObj.getValue());
    }

    @Test
    @DisplayName("Should throw exception, if trying to access the value of property and the instance was not set before")
    public void should_throw_exception_if_trying_to_access_the_value_and_there_is_no_instance_set() throws NoSuchFieldException {
        Field field = TestClass.class.getDeclaredField("testInnerClass");

        testObj = new PropertyMetadata<>(null, TestClass.class, field);

        InternalResolverException exception = assertThrows(InternalResolverException.class, () -> testObj.getValue());

        assertEquals("Instance is not set on metadata, to handle request the " +
            "type is: io.devbench.uibuilder.core.utils.reflection.testclasses.TestInnerClass" +
            " name is: testInnerClass " +
            "containerClass is: io.devbench.uibuilder.core.utils.reflection.testclasses.TestClass " +
            "getter is: public io.devbench.uibuilder.core.utils.reflection.testclasses.TestInnerClass " +
            "io.devbench.uibuilder.core.utils.reflection.testclasses.TestClass.getTestInnerClass() " +
            "filed is: private io.devbench.uibuilder.core.utils.reflection.testclasses.TestInnerClass " +
            "io.devbench.uibuilder.core.utils.reflection.testclasses.TestClass.testInnerClass", exception.getMessage());
    }

    @Test
    @DisplayName("Should set value of property")
    public void should_set_value_of_property() throws NoSuchFieldException {
        Field field = TestClassWithUnaccessableField.class.getDeclaredField("testField");

        TestClassWithUnaccessableField instance = new TestClassWithUnaccessableField();

        PropertyMetadata<TestClassWithUnaccessableField> testObj = new PropertyMetadata<>(instance, TestClassWithUnaccessableField.class, field);

        testObj.setValue("value from test");

        assertEquals("value from test", instance.testField);
    }

    @Test
    @DisplayName("Should find and get annotation, if its on the field, or on the getter")
    public void should_get_annotation_find_and_get_annotation_if_on_field_or_on_getter() throws NoSuchFieldException {
        PropertyMetadata<AnnotationOnField> testObjAnnotationOnField =
            new PropertyMetadata<>(null, AnnotationOnField.class, AnnotationOnField.class.getDeclaredField("testField"));
        PropertyMetadata<AnnotationOnGetter> testObjAnnotationOnGetter =
            new PropertyMetadata<>(null, AnnotationOnGetter.class, AnnotationOnGetter.class.getDeclaredField("testField"));

        assertAll(
            () -> assertTrue(testObjAnnotationOnField.isAnnotationPresent(TestAnnotation.class)),
            () -> assertEquals("annotation on field", testObjAnnotationOnField.getAnnotation(TestAnnotation.class).value()),
            () -> assertTrue(testObjAnnotationOnGetter.isAnnotationPresent(TestAnnotation.class)),
            () -> assertEquals("annotation on getter", testObjAnnotationOnGetter.getAnnotation(TestAnnotation.class).value())
        );
    }

    @Test
    @DisplayName("Should get type metadata of property type")
    public void should_get_type_metadata_of_property_type() throws NoSuchFieldException {
        instance.setTestInnerClass(null);
        testObj = new PropertyMetadata<>(instance, TestClass.class, TestClass.class.getDeclaredField("testInnerClass"));

        ClassMetadata<TestInnerClass> typeMeta = testObj.typeMeta();

        assertAll(
            () -> assertNotNull(typeMeta),
            () -> assertEquals(TestInnerClass.class, typeMeta.getTargetClass()),
            () -> assertNull(typeMeta.getInstance())
        );
    }

    @Test
    @DisplayName("Should collect parameter type information of property if present")
    public void should_collect_parameter_type_information_of_property_if_present() throws NoSuchFieldException {
        PropertyMetadata<PropertyWithGenerics> testObj =
            new PropertyMetadata<>(null, PropertyWithGenerics.class, PropertyWithGenerics.class.getDeclaredField("testField"));

        ParameterizedType parameterizedType = testObj.getParameterizedType();

        assertAll(
            () -> assertNotNull(parameterizedType),
            () -> assertEquals(String.class, parameterizedType.getActualTypeArguments()[0])
        );
    }

    @Test
    @DisplayName("Should get first parameterized type if present")
    void test_should_get_first_parameterized_type_if_present() throws NoSuchFieldException {
        PropertyMetadata<PropertyWithGenerics> testObj =
            new PropertyMetadata<>(null, PropertyWithGenerics.class, PropertyWithGenerics.class.getDeclaredField("testField"));

        Optional<Class<?>> firstParameterizedType = testObj.getFirstParameterizedType();

        assertNotNull(firstParameterizedType);
        assertTrue(firstParameterizedType.isPresent());
        Class<?> typeClass = firstParameterizedType.get();

        assertNotNull(typeClass);
        assertEquals(typeClass, String.class);
    }

    @Test
    @DisplayName("Should create a clone for the same property with a different instance")
    public void should_create_a_clone_for_the_same_property_with_a_different_instance() throws NoSuchFieldException {
        testObj = new PropertyMetadata<>(instance, TestClass.class, TestClass.class.getDeclaredField("testInnerClass"));
        TestClass newInstance = new TestClass();
        PropertyMetadata<TestClass> newInstanceProperty = (PropertyMetadata<TestClass>) testObj.clone(newInstance);

        assertSame(newInstance, newInstanceProperty.getInstance());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Should not throw exception when parsing through generic without parameterized type")
    public void should_not_throw_exception_when_parsing_through_generic_without_parameterized_type() throws NoSuchFieldException {
        AnotherTestWithGenerics instance = new AnotherTestWithGenerics();
        testObj = new PropertyMetadata(instance, instance.getClass(), TestWithGenerics.class.getDeclaredField("protectedField"));
        assertNotNull(testObj);
    }

    public interface TestInterface {

    }

    public static class TestClassWithUnaccessableField implements TestInterface {
        private String testField = "test value";
    }

    public static class AnnotationOnField {
        @TestAnnotation("annotation on field")
        private String testField;
    }

    public static class AnnotationOnGetter {
        private String testField;

        @TestAnnotation("annotation on getter")
        public String getTestField() {
            return this.testField;
        }
    }

    public static class PropertyWithGenerics {
        private List<String> testField;
    }

    public abstract static class TestWithGenerics<T extends TestInterface> {
        protected T protectedField;
    }

    public static class AnotherTestWithGenerics extends TestWithGenerics<TestClassWithUnaccessableField> {
        private String accessableField;
    }

}
