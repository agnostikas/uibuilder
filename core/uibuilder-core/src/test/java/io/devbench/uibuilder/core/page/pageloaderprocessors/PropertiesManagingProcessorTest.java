/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.SerializableConsumer;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.internal.ExecutionContext;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.StateTree;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import elemental.json.JsonArray;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.page.DomBind;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.FieldSetter;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class PropertiesManagingProcessorTest {

    @Mock
    private DomBind domBind;

    @Mock
    private Element domBindElement;

    @Mock
    private StateNode domBindNode;

    @Mock
    private ElementPropertyMap domBindPropertyMap;

    @Mock
    private DomListenerRegistration domListenerRegistration;

    @Mock
    private StateNodeManager stateNodeManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private UI mockUI;

    @Mock
    private StateTree stateTree;

    @Mock
    private Page vaadinPage;


    private PropertiesManagingProcessor testObj;

    private PageLoaderContext context;

    @BeforeEach
    private void setup() throws Exception {
        testObj = new PropertiesManagingProcessor();

        when(domBind.getElement()).thenReturn(domBindElement);
        when(domBindElement.addEventListener(eq("innerHTMLSet"), any())).thenReturn(domListenerRegistration);

        when(stateNodeManager.getPropertyNames()).thenReturn(new HashSet<>(Arrays.asList("controllerBean1", "controllerBean2")));
        when(domBindElement.getNode()).thenReturn(domBindNode);

        when(domBindNode.getFeature(ElementPropertyMap.class)).thenReturn(domBindPropertyMap);


        context = new PageLoaderContext(null, domBind);
        FieldSetter.setField(context, PageLoaderContext.class.getDeclaredField("stateNodeManager"), stateNodeManager);
        context.getBindings().addAll(Arrays.asList("controllerBean1.value1", "controllerBean2.value2"));
    }

    @Test
    @DisplayName("Should set innerHTML change listener to register properties, to propage and listen to changes")
    public void should_set_inner_html_change_listener_to_register_properties_to_propage_and_listen_to_changes() {
        testObj.process(context);
        verify(domBindElement).addEventListener(eq("innerHTMLSet"), any(DomEventListener.class));
    }

    @Test
    @DisplayName("Should collect and register properties to syncronize from frontend")
    public void should_collect_and_register_properties_to_syncronize_from_frontend() {
        ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);
        testObj.process(context);
        verify(domBindElement).addEventListener(eq("innerHTMLSet"), listenerCaptor.capture());

        listenerCaptor.getValue().handleEvent(mock(DomEvent.class));

        ArgumentCaptor<SerializablePredicate<String>> propertyPredicateCaptor = ArgumentCaptor.forClass(SerializablePredicate.class);
        verify(domBindPropertyMap).setUpdateFromClientFilter(propertyPredicateCaptor.capture());

        assertBindingValuesAcceptedByPropertyUpdateFilter(propertyPredicateCaptor.getValue());

        ArgumentCaptor<SerializableConsumer<UI>> uiConsumerCaptor = ArgumentCaptor.forClass(SerializableConsumer.class);

        verify(domBindNode, times(2)).runWhenAttached(uiConsumerCaptor.capture());

        when(mockUI.getInternals().getStateTree()).thenReturn(stateTree);
        when(stateTree.beforeClientResponse(eq(domBindNode), any())).thenReturn(mock(StateTree.ExecutionRegistration.class));

        uiConsumerCaptor.getAllValues().forEach(consumer -> consumer.accept(mockUI));

        ArgumentCaptor<SerializableConsumer<ExecutionContext>> contextConsumerCaptor = ArgumentCaptor.forClass(SerializableConsumer.class);

        verify(stateTree, times(2)).beforeClientResponse(eq(domBindNode), contextConsumerCaptor.capture());

        ExecutionContext ctx = mock(ExecutionContext.class);
        when(ctx.getUI()).thenReturn(mockUI);
        when(mockUI.getPage()).thenReturn(vaadinPage);
        contextConsumerCaptor.getAllValues().forEach(consumer -> consumer.accept(ctx));

        verify(vaadinPage).executeJs(
            eq("this.registerUpdatableModelProperties($0, $1)"),
            eq(domBindElement),
            argThat((JsonArray jsonArray) ->
                jsonArray.length() == 2
                    && jsonArray.get(0).asString().equals("controllerBean2.value2")
                    && jsonArray.get(1).asString().equals("controllerBean1.value1")
            ));

        verify(vaadinPage).executeJs(
            eq("this.populateModelProperties($0, $1)"),
            eq(domBindElement),
            argThat((JsonArray jsonArray) -> jsonArray.length() == 0));
    }

    private void assertBindingValuesAcceptedByPropertyUpdateFilter(SerializablePredicate<String> filter) {
        assertAll(
            () -> assertTrue(filter.test("controllerBean1")),
            () -> assertTrue(filter.test("controllerBean2")),
            () -> assertTrue(filter.test("controllerBean1.value1")),
            () -> assertTrue(filter.test("controllerBean2.value2")),
            () -> assertFalse(filter.test("non.valid.binding"))
        );
    }

}
