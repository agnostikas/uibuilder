/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.PageTransformer;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.parse.PageTransformersRegistry;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class PageTransformProcessorTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    private PageTransformProcessor testObj;

    @BeforeEach
    void setUp() {
        Set<Class<? extends PageTransformer>> classes = new HashSet<>(Arrays.asList(TestPageTransformer1.class, TestPageTransformer2.class));
        when(memberScanner.findClassesBySuperType(PageTransformer.class)).thenReturn(classes);
        PageTransformersRegistry.registerToActiveContext();

        testObj = new PageTransformProcessor();
    }

    @Test
    @DisplayName("should run transformers")
    void test_should_run_transformers() {
        Element element = Jsoup.parse("<run-test-1 id=\"one\"></run-test-1><run-test-2 id=\"two\"></run-test-2>");

        PageLoaderContext context = mock(PageLoaderContext.class);
        doReturn(element).when(context).getPageElement();

        testObj.process(context);

        assertTrue(element.getElementById("one").hasAttr("test1"));
        assertEquals("done", element.getElementById("one").attr("test1"));
        assertFalse(element.getElementById("one").hasAttr("test2"));

        assertTrue(element.getElementById("two").hasAttr("test2"));
        assertEquals("finished", element.getElementById("two").attr("test2"));
        assertFalse(element.getElementById("two").hasAttr("test1"));
    }

    public static class TestPageTransformer1 implements PageTransformer {

        @Override
        public void transform(Element element) {
            element.attr("test1", "done");
        }

        @Override
        public boolean isApplicable(Element element) {
            return element.tagName().equals("run-test-1");
        }
    }

    public static class TestPageTransformer2 implements PageTransformer {

        @Override
        public void transform(Element element) {
            element.attr("test2", "finished");
        }

        @Override
        public boolean isApplicable(Element element) {
            return element.tagName().equals("run-test-2");
        }
    }

}
