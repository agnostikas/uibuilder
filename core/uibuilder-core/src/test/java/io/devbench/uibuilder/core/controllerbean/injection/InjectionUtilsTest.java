/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection;

import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.server.VaadinSession;
import elemental.json.Json;
import elemental.json.JsonObject;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.session.context.UIContext;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Consumer;

import static io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InjectionUtilsTest {

    private ItemResolver testItemResolver = (ignore) -> Optional.of("TEST_ANSWER");

    @Test
    public void shouldGetACollectionFromASingleObject() {
        final Object hello = getCollectionAwareItem("Hello", Collection.class);
        assertTrue(hello instanceof Collection);
    }

    @Test
    public void shouldGetAListFromASingleObject() {
        final Object hello = getCollectionAwareItem("Hello", List.class);
        assertTrue(hello instanceof List);
    }

    @Test
    public void shouldGetASetFromASingleObject() {
        final Object hello = getCollectionAwareItem("Hello", List.class);
        assertTrue(hello instanceof List);
    }

    @Test
    public void shouldGetACollectionFromACollection() {
        final Object hello = getCollectionAwareItem(Arrays.asList("Hello"), Collection.class);
        assertTrue(hello instanceof Collection);
    }

    @Test
    public void shouldGetACollectionFromAList() {
        final Object hello = getCollectionAwareItem(Arrays.asList("Hello"), Collection.class);
        assertTrue(hello instanceof Collection);
    }

    @Test
    public void shouldGetACollectionFromASet() {
        final Object hello = getCollectionAwareItem(new HashSet<>(Arrays.asList("Hello")),
            Collection.class);
        assertTrue(hello instanceof Collection);
    }

    @Test
    public void shouldGetNullWhenItemIsNull() {
        assertNull(getCollectionAwareItem(null,
            Collection.class));
    }

    @Test
    public void shouldThrowExceptionWhenNotAssignable() {
        assertThrows(InternalResolverException.class,
            () -> getCollectionAwareItem(new HashSet<>(Arrays.asList("Hello")),
                List.class));
    }

    @Test
    void should_return_the_correct_item_if_can_resolve() {
        final UIContext uiContext = mock(UIContext.class);
        VaadinSession.getCurrent().setAttribute("UIBuilderSessionContext", uiContext);
        when(uiContext.get(ItemResolver.class)).thenReturn(testItemResolver);

        assertEquals("TEST_ANSWER", InjectionUtils.getClientParameterValue("item", mock(StateNodeManager.class),
            createTestData(value -> value.put("event.detail.item", Json.createObject())), String.class));
    }

    @Test
    void should_return_the_json_item_if_type_is_json_object() {
        JsonObject jsonItem = Json.createObject();
        assertEquals(jsonItem, InjectionUtils.getClientParameterValue("item", mock(StateNodeManager.class),
            createTestData(value -> value.put("event.detail.item", jsonItem)), JsonObject.class));
    }

    @Test
    void should_return_null_if_event_data_is_not_filled() {
        final DomEvent testEvent = createTestData();
        assertNull(InjectionUtils.getClientParameterValue("item", mock(StateNodeManager.class), testEvent, String.class));
        assertNull(InjectionUtils.getClientParameterValue("detail.value", mock(StateNodeManager.class), testEvent, String.class));
    }

    @Test
    void should_return_null_if_event_data_is_a_json_null() {
        assertNull(InjectionUtils.getClientParameterValue("item", mock(StateNodeManager.class),
            createTestData(value -> value.put("event.detail.item", Json.createNull())), String.class));
        assertNull(InjectionUtils.getClientParameterValue("value", mock(StateNodeManager.class),
            createTestData(value -> value.put("event.detail.value", Json.createNull())), String.class));
    }

    @SafeVarargs
    @NotNull
    private final DomEvent createTestData(Consumer<JsonObject>... prepares) {
        final JsonObject item = Json.createObject();
        Arrays.stream(prepares).filter(Objects::nonNull).forEach(prepare -> prepare.accept(item));
        return new DomEvent(mock(Element.class), "test_event", item);
    }
}
