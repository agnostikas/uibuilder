/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection.request;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.CallOnNonNull;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Data;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils;
import io.devbench.uibuilder.core.flow.FlowParameter;
import io.devbench.uibuilder.core.flow.FlowParameterProvider;
import io.devbench.uibuilder.core.flow.QueryParameter;
import io.devbench.uibuilder.core.session.context.UIContext;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AbstractInjectionRequestTest {

    private static class TestInjectionRequest extends AbstractInjectionRequest {
        TestInjectionRequest(@NotNull Method method) {
            super(method);
        }
    }

    @SuppressWarnings("unused")
    private static class TestClass {
        public void fooBarMethod(@FlowParameter("fooBarParameter") String f, @FlowParameter("missingParameter") String e) {

        }

        public void fooBarMethod2(@QueryParameter("fooBarParameter") String f, @QueryParameter("missingParameter") String e) {

        }

        public void fooBarMethod3(@QueryParameter("fooBarParameter") List<String> f, @QueryParameter("missingParameter") String e) {

        }

        public void testCallOnNonNull(@CallOnNonNull @QueryParameter("param") String parameter) {

        }

        @CallOnNonNull
        public void testCallOnNonNullMethod(@QueryParameter("param1") String parameter1, @QueryParameter("param2") String parameter2) {

        }
    }

    @SuppressWarnings("unused")
    private static class ValueTestClass {

        public void testMethod(@Item TestClass testItem, @Value("detail.test.value") String testValue, @Data("other.data") String otherTestData) {

        }

    }

    @Test
    @DisplayName("injection request should not be valid, of parameter is null")
    void test_injection_request_should_not_be_valid_of_parameter_is_null() throws Exception {
        FlowParameterProvider flowParameterProvider = mock(FlowParameterProvider.class);
        UIContext.getContext().addToActiveUIContextAs(FlowParameterProvider.class, flowParameterProvider);

        when(flowParameterProvider.getQueryParameters("param"))
            .thenReturn(Collections.singletonList("NonNullValue"))
            .thenReturn(Collections.emptyList());

        TestInjectionRequest testObj;
        testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("testCallOnNonNull", String.class));
        assertTrue(testObj.isValid());

        testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("testCallOnNonNull", String.class));
        assertFalse(testObj.isValid());
    }

    @Test
    @DisplayName("injection request should not be valid, of parameter is null when method is annotated")
    void test_injection_request_should_not_be_valid_of_parameter_is_null_when_method_is_annotated() throws Exception {
        FlowParameterProvider flowParameterProvider = mock(FlowParameterProvider.class);
        UIContext.getContext().addToActiveUIContextAs(FlowParameterProvider.class, flowParameterProvider);

        when(flowParameterProvider.getQueryParameters("param1"))
            .thenReturn(Collections.singletonList("NonNullValue"))
            .thenReturn(Collections.emptyList())
            .thenReturn(Collections.emptyList())
            .thenReturn(Collections.singletonList("NonNullValue"));

        when(flowParameterProvider.getQueryParameters("param2"))
            .thenReturn(Collections.emptyList())
            .thenReturn(Collections.emptyList())
            .thenReturn(Collections.singletonList("NonNullValue"))
            .thenReturn(Collections.singletonList("NonNullValue"));

        TestInjectionRequest testObj;

        testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("testCallOnNonNullMethod", String.class, String.class));
        assertFalse(testObj.isValid());

        testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("testCallOnNonNullMethod", String.class, String.class));
        assertFalse(testObj.isValid());

        testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("testCallOnNonNullMethod", String.class, String.class));
        assertFalse(testObj.isValid());

        testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("testCallOnNonNullMethod", String.class, String.class));
        assertTrue(testObj.isValid());
    }

    @Test
    public void should_handle_flow_parameter_annotated_method_parameter_correctly() throws Exception {
        FlowParameterProvider flowParameterProvider = mock(FlowParameterProvider.class);
        UIContext.getContext().addToActiveUIContextAs(FlowParameterProvider.class, flowParameterProvider);

        TestInjectionRequest testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("fooBarMethod", String.class, String.class));

        when(flowParameterProvider.getUrlParameter("fooBarParameter")).thenReturn(Optional.of("TestingTestingEverythingSeemsToBeInOrder"));
        when(flowParameterProvider.getUrlParameter("missingParameter")).thenReturn(Optional.empty());

        assertTrue(testObj.isValid());
        assertEquals(Arrays.asList("TestingTestingEverythingSeemsToBeInOrder", null), Arrays.asList(testObj.getParameters()));

        verify(flowParameterProvider).getUrlParameter("fooBarParameter");
        verify(flowParameterProvider).getUrlParameter("missingParameter");
    }

    @Test
    public void should_handle_query_parameter_annotated_method_parameter_correctly() throws Exception {
        FlowParameterProvider flowParameterProvider = mock(FlowParameterProvider.class);
        UIContext.getContext().addToActiveUIContextAs(FlowParameterProvider.class, flowParameterProvider);

        TestInjectionRequest testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("fooBarMethod2", String.class, String.class));

        when(flowParameterProvider.getQueryParameters("fooBarParameter")).thenReturn(Collections.singletonList("TestingTestingEverythingSeemsToBeInOrder"));
        when(flowParameterProvider.getQueryParameters("missingParameter")).thenReturn(Collections.emptyList());

        assertTrue(testObj.isValid());
        assertEquals(Arrays.asList("TestingTestingEverythingSeemsToBeInOrder", null), Arrays.asList(testObj.getParameters()));

        verify(flowParameterProvider).getQueryParameters("fooBarParameter");
        verify(flowParameterProvider).getQueryParameters("missingParameter");
    }

    @Test
    public void should_handle_query_parameter_annotated_method_parameter_correctly_when_parameter_has_multiple_value() throws Exception {
        FlowParameterProvider flowParameterProvider = mock(FlowParameterProvider.class);
        UIContext.getContext().addToActiveUIContextAs(FlowParameterProvider.class, flowParameterProvider);

        TestInjectionRequest testObj = new TestInjectionRequest(TestClass.class.getDeclaredMethod("fooBarMethod3", List.class, String.class));

        when(flowParameterProvider.getQueryParameters("fooBarParameter"))
            .thenReturn(Arrays.asList("TestingTestingEverythingSeemsToBeInOrder", "AllRightGordon"));
        when(flowParameterProvider.getQueryParameters("missingParameter"))
            .thenReturn(Arrays.asList("YourSuitShouldKeepYouComfortableThroughAllThis", "TheSpecimenWillBeDelivered"));

        assertTrue(testObj.isValid());
        assertEquals(
            Arrays.asList(Arrays.asList("TestingTestingEverythingSeemsToBeInOrder", "AllRightGordon"), "TheSpecimenWillBeDelivered"),
            Arrays.asList(testObj.getParameters())
        );

        verify(flowParameterProvider).getQueryParameters("fooBarParameter");
        verify(flowParameterProvider).getQueryParameters("missingParameter");
    }

    @Test
    @DisplayName("Should resolve parameter name from annotations")
    void test_should_resolve_parameter_name_from_annotations() throws Exception {

        Method testMethod = ValueTestClass.class.getDeclaredMethod("testMethod", TestClass.class, String.class, String.class);
        TestInjectionRequest testObj = new TestInjectionRequest(testMethod);

        Optional<String> itemParameterName = testObj.findClientParameterName(testMethod.getParameters()[0]);
        Optional<String> valueParameterName = testObj.findClientParameterName(testMethod.getParameters()[1]);
        Optional<String> dataParameterName = testObj.findClientParameterName(testMethod.getParameters()[2]);

        assertTrue(itemParameterName.isPresent());
        assertTrue(valueParameterName.isPresent());
        assertTrue(dataParameterName.isPresent());

        assertEquals("item", itemParameterName.get());
        assertEquals("detail.test.value", valueParameterName.get());
        assertEquals(InjectionUtils.EVENT_DATASET_PREFIX + "other.data", dataParameterName.get());
    }

}
