/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.injectionpoint.ComponentInjectionPoint;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanProvidersHolder.ControllerBeanNameProviderByClass;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.controllerbean.uiproperty.TestProvider;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.test.controllerbean.MockControllerBean;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class ControllerBeanManagerTest {

    private static String[] ORIGINAL_PROVIDER_CLASS_NAMES;

    private ControllerBeanManager testObj;

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ControllerBeanProvidersHolder providers;

    @BeforeAll
    static void beforeAll() {
        ORIGINAL_PROVIDER_CLASS_NAMES = ControllerBeanManager.PROVIDER_CLASS_FQNS;
        ControllerBeanManager.PROVIDER_CLASS_FQNS = new String[]{"io.devbench.uibuilder.core.controllerbean.uiproperty.TestProvider"};
    }

    @AfterAll
    static void afterAll() {
        ControllerBeanManager.PROVIDER_CLASS_FQNS = ORIGINAL_PROVIDER_CLASS_NAMES;
    }

    @BeforeEach
    private void setup() {
        Set<Class<?>> classes = Sets.newHashSet(TestBean1.class, TestBean2.class);

        ControllerBeanNameProviderByClass nameProviderByClass = mock(ControllerBeanNameProviderByClass.class);

        when(memberScanner.findClassesByAnnotation(MockControllerBean.class)).thenReturn(classes);
        when(providers.getBeanNameByClass()).thenReturn(nameProviderByClass);
        when(nameProviderByClass.get(any(Class.class)))
            .thenAnswer(invocation -> StringUtils.uncapitalize(((Class<?>) invocation.getArgument(0)).getSimpleName()));

        testObj = ControllerBeanManager.registerToActiveContext(providers, MockControllerBean.class);
    }

    @BeforeEach
    private void resetMocks() {
        Mockito.reset(providers);
    }

    @Test
    @DisplayName("Should call provider to fetch bean by name")
    public void should_call_provider_to_fetch_bean_by_name() {
        TestBean1 testBean = new TestBean1();
        when(providers.getBeanInstanceByName().get("testBean1")).thenReturn(testBean);

        Object testBean1 = testObj.getControllerBean("testBean1");

        assertSame(testBean, testBean1);
        verify(providers.getBeanInstanceByName()).get("testBean1");
    }

    @Test
    @DisplayName("Should call provider to fetch bean by class")
    public void should_call_provider_to_fetch_bean_by_class() {
        TestBean1 testBean = new TestBean1();
        when(providers.getBeanInstanceByClass().get(TestBean1.class)).thenReturn(testBean);

        Object testBean1 = testObj.getControllerBean(TestBean1.class);

        assertSame(testBean, testBean1);
        verify(providers.getBeanInstanceByClass()).get(TestBean1.class);
    }

    @Test
    @DisplayName("Should find all eventhandler names")
    public void should_find_all_eventhandler_names() {
        Set<String> eventHandlerNames = testObj.getEventHandlerQualifiedNames();
        assertAll(
            () -> assertEquals(4, eventHandlerNames.size()),
            () -> assertTrue(eventHandlerNames.containsAll(Arrays.asList(
                "testBean1::testEventHandler", "testBean1::testEventHandler2",
                "testBean2::testEventHandler"
            )))
        );
    }

    @Test
    @DisplayName("Should get bean metadata by bean name")
    public void should_get_bean_metadata_by_bean_name() {
        TestBean1 testBean1 = new TestBean1();
        when(providers.getBeanInstanceByName().get("testBean")).thenReturn(testBean1);
        ClassMetadata<?> beanMetadata = testObj.getBeanMetadata("testBean");
        assertAll(
            () -> assertNotNull(beanMetadata),
            () -> assertEquals(TestBean1.class, beanMetadata.getTargetClass()),
            () -> assertSame(testBean1, beanMetadata.getInstance())
        );
    }

    @Test
    @DisplayName("Should call locale change observers with a specified event in the active context")
    public void should_call_locale_change_observers_with_a_specified_event_in_the_active_context() {
        TestBean2 testBean2Mock = mock(TestBean2.class);
        when(providers.getBeanInstanceByClass().get(TestBean2.class)).thenReturn(testBean2Mock);
        LocaleChangeEvent localChangeEvent = mock(LocaleChangeEvent.class);
        testObj.callLocaleChangeObservers(localChangeEvent);
        verify(testBean2Mock).localeChange(localChangeEvent);
    }

    @Test
    @DisplayName("Should get the event handler context by it's qualified name")
    public void should_get_the_event_handler_context_by_it_s_qualified_name() {
        Optional<UIEventHandlerContext> optionalEventHandlerContext = testObj.getEventHandlerContext("testBean1::testEventHandler");
        assertAll(
            () -> assertTrue(optionalEventHandlerContext.isPresent()),
            () -> assertEquals(TestBean1.class.getDeclaredMethod("testEventHandler1"), optionalEventHandlerContext.get().getMethod())
        );
    }

    @Test
    @DisplayName("Should not find the event handler when there's a handler with the corresponding name but the qualifiers do not match")
    public void should_not_find_anything_on_matching_name_and_unmatching_qualifiers() {
        Optional<UIEventHandlerContext> optionalEventHandlerContext = testObj.getEventHandlerContext("testBean1::onCustomQualifier");
        assertAll(
            () -> assertFalse(optionalEventHandlerContext.isPresent())
        );
    }

    @Test
    @DisplayName("Should find the event handler when there's a handler with the corresponding name and the qualifiers match")
    public void should_correct_handler_on_matching_name_and_matching_qualifiers() {
        Optional<UIEventHandlerContext> optionalEventHandlerContext = testObj.getEventHandlerContext("testBean1::onCustomQualifier", EventQualifier.REQUESTED);
        assertAll(
            () -> assertTrue(optionalEventHandlerContext.isPresent()),
            () -> assertEquals(TestBean1.class.getDeclaredMethod("onCustomQualifier"), optionalEventHandlerContext.get().getMethod())
        );
    }

    @Test
    @DisplayName("Should find the event handler when there's a handler with the corresponding name and the custom qualifiers match")
    public void should_correct_handler_on_matching_name_and_matching_custom_qualifiers() {
        Optional<UIEventHandlerContext> optionalEventHandlerContext = testObj.getEventHandlerContext("testBean1::onCustomQualifier", "foo");
        assertAll(
            () -> assertTrue(optionalEventHandlerContext.isPresent()),
            () -> assertEquals(TestBean1.class.getDeclaredMethod("onCustomQualifier"), optionalEventHandlerContext.get().getMethod())
        );
    }

    @Test
    @DisplayName("Should find all injection points in all registered controller bean classes")
    public void should_find_all_injection_points_in_all_registered_controller_bean_classes() {
        Map<String, ComponentInjectionPoint> injectionPoints = testObj.findInjectionPointsById();
        assertAll(
            () -> assertNotNull(injectionPoints),
            () -> assertEquals(4, injectionPoints.size()),
            () -> assertEquals(Button.class, injectionPoints.get("id1").getUnambiguousType()),
            () -> {
                Method method = TestBean2.class.getDeclaredMethod("testEventHandler3", Button.class);
                Parameter parameter = method.getParameters()[0];
                assertTrue(injectionPoints.get("id2").isParameterMatching(method, parameter));
            }
        );
    }

    @Test
    @DisplayName("Should call event handlers and inject components into them (test simple way injections)")
    public void should_call_event_handlers_and_inject_components_into_them_test_simple_way_injections_() {
        UI currentUI = UI.getCurrent();
        VaadinSession vaadinSessionMock = Mockito.mock(VaadinSession.class);
        WrappedSession wrappedSessionMock = Mockito.mock(WrappedSession.class);
        Mockito.doReturn(vaadinSessionMock).when(currentUI).getSession();
        Mockito.doReturn(wrappedSessionMock).when(vaadinSessionMock).getSession();
        Mockito.doReturn("12").when(wrappedSessionMock).getId();
        Mockito.doReturn(112).when(currentUI).getUIId();

        Map<String, ComponentInjectionPoint> injectionPoints = testObj.findInjectionPointsById();
        Button button1 = mock(Button.class);
        injectionPoints.get("id1").setValue(button1);

        TestBean1 testBean = mock(TestBean1.class);
        when(providers.getBeanInstanceByName().get("testBean1")).thenReturn(testBean);

        testObj.callEventHandlers("testBean1::testEventHandler2", mock(StateNodeManager.class), mock(DomEvent.class));

        verify(testBean).testEventHandler2(button1);
    }

    @Test
    @DisplayName("Should contain field injection points")
    void test_should_contain_field_injection_points() {
        checkInjectionPointsInControllerBeans();
    }

    private void checkInjectionPointsInControllerBeans() {
        Map<String, ComponentInjectionPoint> injectionPoints = testObj.findInjectionPointsById();

        assertNotNull(injectionPoints);

        ComponentInjectionPoint testTextFieldInjectionPoint = injectionPoints.get("test-text-field");
        assertNotNull(testTextFieldInjectionPoint);
        Class<?> testTextFieldType = testTextFieldInjectionPoint.getUnambiguousType();
        assertNotNull(testTextFieldType);
        assertTrue(TextField.class.isAssignableFrom(testTextFieldType));

        ComponentInjectionPoint testGridInjectionPoint = injectionPoints.get("test-grid");
        assertNotNull(testGridInjectionPoint);
        Class<?> testGridType = testGridInjectionPoint.getUnambiguousType();
        assertNotNull(testGridType);
        assertTrue(Grid.class.isAssignableFrom(testGridType));
    }

    @Test
    @DisplayName("Should contain field injection points from superclass too")
    void should_contain_field_injection_points_from_superclass_too() {
        reset(memberScanner);
        when(memberScanner.findClassesByAnnotation(MockControllerBean.class)).thenReturn(Collections.singleton(TestBean3.class));
        testObj = ControllerBeanManager.registerToActiveContext(providers, MockControllerBean.class);
        checkInjectionPointsInControllerBeans();
    }

    @Test
    @DisplayName("Should map together the normalized names and the qualified names of the event handlers")
    public void should_map_together_the_normalized_names_and_the_qualified_names_of_the_event_handlers() {
        Map<String, String> normalizedNameToQualifiedNameMap = testObj.getEvenHandlerNormalizedNameToQualifiedNameMap();
        assertAll(
            () -> assertNotNull(normalizedNameToQualifiedNameMap),
            () -> assertEquals(4, normalizedNameToQualifiedNameMap.size()),
            () -> assertEquals("testBean1::testEventHandler", normalizedNameToQualifiedNameMap.get("testBean1_testEventHandler")),
            () -> assertEquals("testBean1::testEventHandler2", normalizedNameToQualifiedNameMap.get("testBean1_testEventHandler2")),
            () -> assertEquals("testBean2::testEventHandler", normalizedNameToQualifiedNameMap.get("testBean2_testEventHandler"))
        );
    }


    @MockControllerBean
    public static class TestBean1 {

        @UIComponent("test-text-field")
        private TestProvider<TextField> testTextField;

        @UIComponent("test-grid")
        private TestProvider<Grid<String>> testGrid;

        @UIEventHandler("testEventHandler")
        public void testEventHandler1() {

        }

        @UIEventHandler(value = "onCustomQualifier", qualifiers = EventQualifier.REQUESTED, customQualifiers = {"foo", "bar"})
        public void onCustomQualifier() {

        }

        @UIEventHandler("testEventHandler2")
        public void testEventHandler2(@UIComponent("id1") Button button) {

        }

    }

    @MockControllerBean
    public static class TestBean2 implements LocaleChangeObserver {

        @UIEventHandler("testEventHandler")
        public void testEventHandler3(@UIComponent("id2") Button button) {

        }

        @Override
        public void localeChange(LocaleChangeEvent event) {

        }
    }

    public abstract static class TestAbstractSuperBean {

        @UIComponent("test-text-field")
        private TestProvider<TextField> textField;

    }

    @MockControllerBean
    public static class TestBean3 extends TestAbstractSuperBean {

        @UIComponent("test-grid")
        private TestProvider<Grid<String>> testGrid;

    }

}
