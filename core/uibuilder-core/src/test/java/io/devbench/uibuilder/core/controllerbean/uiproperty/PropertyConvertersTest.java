/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty;

import io.devbench.uibuilder.core.utils.reflection.AbstractPropertyMetadata;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import lombok.Data;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


class PropertyConvertersTest {

    @Test
    @DisplayName("Should be int convertible")
    void should_int_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("intValue");
        Object result = converter.convertFrom("11");
        assertTrue(result instanceof Integer);
        assertEquals(11, result);
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Int to String should throw number format exception")
    void test_int_to_string_should_throw_number_format_exception() {
        PropertyConverter<?, String> converter = getConverterByFieldName("intValue");
        assertThrows(NumberFormatException.class, () -> converter.convertFrom("11a"));
    }

    @Test
    @DisplayName("Should be Integer convertible")
    void should_integer_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("integerValue");
        Object result = converter.convertFrom("11");
        assertTrue(result instanceof Integer);
        assertEquals(11, result);
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Integer to string should throw number format exception")
    void test_integer_to_string_should_throw_number_format_exception() {
        PropertyConverter<?, String> converter = getConverterByFieldName("integerValue");
        assertThrows(NumberFormatException.class, () -> converter.convertFrom("11a"));
    }

    private void testBoolean(PropertyConverter<?, String> converter) {
        Object result = converter.convertFrom("true");
        assertTrue(result instanceof Boolean);
        assertEquals(Boolean.TRUE, result);

        result = converter.convertFrom("tRUe");
        assertTrue(result instanceof Boolean);
        assertEquals(Boolean.TRUE, result);

        result = converter.convertFrom("false");
        assertTrue(result instanceof Boolean);
        assertEquals(Boolean.FALSE, result);

        result = converter.convertFrom("anything else");
        assertTrue(result instanceof Boolean);
        assertEquals(Boolean.FALSE, result);

        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be bool convertible")
    void should_bool_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("priBoolean");
        testBoolean(converter);
    }


    @Test
    @DisplayName("Should be Boolean convertible")
    void should_boolean_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("booleanValue");
        testBoolean(converter);
    }

    @Test
    @DisplayName("Should be long convertible")
    void should_long_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("primitiveLongValue");
        assertEquals(11L, converter.convertFrom("11"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be Long convertible")
    void should_Long_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("longValue");
        assertEquals(11L, converter.convertFrom("11"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be double convertible")
    void should_double_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("primitiveDoubleValue");
        assertEquals(11d, converter.convertFrom("11"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be Double convertible")
    void should_Double_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("doubleValue");
        assertEquals(11d, converter.convertFrom("11"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be enum convertible")
    void should_enum_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("enumValue");
        assertEquals(TestEnum.EGY, converter.convertFrom("EGY"));
        assertEquals(TestEnum.KETTO, converter.convertFrom("KETTO"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be BigInteger convertible")
    void should_BigInteger_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("bigIntegerValue");
        assertEquals(BigInteger.ONE, converter.convertFrom("1"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be BigDecimal convertible")
    void should_BigDecimal_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("bigDecimalValue");
        assertEquals(BigDecimal.ONE, converter.convertFrom("1"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be LocalDate convertible")
    void should_LocalDate_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("localDate");
        assertEquals(LocalDate.parse("2000-01-01"), converter.convertFrom("2000-01-01"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be LocalDateTime convertible")
    void should_LocalDateTime_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("localDateTime");
        assertEquals(LocalDateTime.parse("2000-01-01T14:00:00.000"), converter.convertFrom("2000-01-01T14:00:00.000"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be UUID convertible")
    void should_UUID_convertible() {
        PropertyConverter<?, String> converter = getConverterByFieldName("uuid");
        assertEquals(UUID.fromString("61b598e2-d6dd-4e9b-8986-e6875ab0abda"), converter.convertFrom("61b598e2-d6dd-4e9b-8986-e6875ab0abda"));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be DATE convertible")
    @SuppressWarnings("unchecked")
    void should_DATE_convertible() {
        StringPropertyConverter<Date> converter = (StringPropertyConverter<Date>) getConverterByFieldName("date");
        assertEquals(Date.from(LocalDate.parse("2000-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()),
            converter.convertFrom(converter.convertTo(Date.from(LocalDate.parse("2000-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()))));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should be empty string convert to null")
    void Should_be_empty_string_convert_to_null() {
        PropertyConverter<?, String> converter = getConverterByFieldName("uuid");
        assertNull(converter.convertFrom(""));
        assertNull(converter.convertFrom(null));
    }

    @Test
    @DisplayName("Should convert any type to corresponding string representation")
    void test_should_convert_any_type_to_corresponding_string_representation() {
        assertNull(PropertyConverters.convertToString(null));

        String string = PropertyConverters.convertToString("string");
        String integer = PropertyConverters.convertToString(101);
        String bool = PropertyConverters.convertToString(false);

        assertEquals("string", string);
        assertEquals("101", integer);
        assertEquals("false", bool);
    }

    @Test
    @DisplayName("Should convert string to corresponding type")
    void test_should_convert_string_to_corresponding_type() {
        assertNull(PropertyConverters.convertToObject(null, "string"));
        assertNull(PropertyConverters.convertToObject(String.class, null));
        assertNull(PropertyConverters.convertToObject(null, null));

        Integer integer = PropertyConverters.convertToObject(Integer.class, "101");
        Boolean bool = PropertyConverters.convertToObject(Boolean.class, "true");
        LocalDateTime localDateTime = PropertyConverters.convertToObject(LocalDateTime.class, "2021-04-07T12:16:17.123456");

        assertNotNull(integer);
        assertEquals(101, integer);

        assertNotNull(bool);
        assertTrue(bool);

        assertNotNull(localDateTime);
        assertEquals(2021, localDateTime.getYear());
        assertEquals(4, localDateTime.getMonthValue());
        assertEquals(7, localDateTime.getDayOfMonth());
        assertEquals(12, localDateTime.getHour());
        assertEquals(16, localDateTime.getMinute());
        assertEquals(17, localDateTime.getSecond());
        assertEquals(123456000, localDateTime.getNano());
    }

    @Test
    @DisplayName("Should convert an any type collection to string collection")
    void test_should_convert_an_any_type_collection_to_string_collection() {
        List<String> stringList = PropertyConverters.convertToStringList(Arrays.asList(10, "ok", true));
        assertEquals(3, stringList.size());
        assertEquals("10", stringList.get(0));
        assertEquals("ok", stringList.get(1));
        assertEquals("true", stringList.get(2));
    }

    @Test
    @DisplayName("Should convert a string list to the specified type of object list")
    void test_should_convert_a_string_list_to_the_specified_type_of_object_list() {
        List<Integer> integers = PropertyConverters.convertToObjectList(Integer.class, Arrays.asList("15", "25", "35"));

        assertEquals(3, integers.size());
        assertEquals(15, integers.get(0));
        assertEquals(25, integers.get(1));
        assertEquals(35, integers.get(2));
    }

    @Test
    @DisplayName("Should return empty list if the string list cannot be converted to object list")
    void test_should_return_empty_list_if_the_string_list_cannot_be_converted_to_object_list() {
        List<Integer> integers = PropertyConverters.convertToObjectList(Integer.class, Arrays.asList("15.3", "25.1", "35.7"));
        assertNotNull(integers);
        assertTrue(integers.isEmpty());
    }


    private PropertyConverter<?, String> getConverterByFieldName(String name) {
        ClassMetadata<ClassToRunTestsOn> classMeta = ClassMetadata.ofClass(ClassToRunTestsOn.class);
        AbstractPropertyMetadata<?> property = classMeta.property(name).orElse(null);
        assert property != null;
        @SuppressWarnings("unchecked")
        PropertyConverter<?, String> converter = (PropertyConverter<?, String>) PropertyConverters.getConverterFor(property);
        assertNotNull(converter);
        return converter;
    }

    enum TestEnum {EGY, KETTO}

    @Data
    public static class ClassToRunTestsOn {
        private int intValue = 10;
        private Integer integerValue = 10;
        private long primitiveLongValue = 10;
        private Long longValue = 10L;
        private double primitiveDoubleValue = 10;
        private Double doubleValue = 10d;
        private TestEnum enumValue = TestEnum.EGY;
        private BigInteger bigIntegerValue = BigInteger.ONE;
        private BigDecimal bigDecimalValue = BigDecimal.ONE;
        private Date date = Date.from(LocalDate.parse("2000-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant());
        private LocalDate localDate = LocalDate.parse("2000-01-01");
        private LocalDateTime localDateTime = LocalDateTime.parse("2000-01-01T14:00:00.000");
        private UUID uuid = UUID.randomUUID();
        private Boolean booleanValue;
        private boolean priBoolean;
    }

}
