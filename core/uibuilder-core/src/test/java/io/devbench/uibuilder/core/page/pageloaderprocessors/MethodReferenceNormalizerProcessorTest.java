/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(JsoupExtension.class)
class MethodReferenceNormalizerProcessorTest {

    @LoadElement(value = "/page-element-with-methods.html", id = "test-page")
    private Element pageElement;

    private MethodReferenceNormalizerProcessor testObj = new MethodReferenceNormalizerProcessor();


    @Test
    @DisplayName("Should replace all method references with their normalized names in page element")
    public void should_replace_all_method_references_with_their_normalized_names_in_page_element() {
        PageLoaderContext context = new PageLoaderContext();
        context.setPageElement(pageElement);

        testObj.process(context);

        assertAll(
            () -> assertEquals("testBean_testMethod1", pageElement.getElementsByTag("vaadin-button").get(0).attr("on-click")),
            () -> assertEquals("testBean_testMethod2", pageElement.getElementsByTag("vaadin-button").get(1).attr("on-click"))
        );
    }

}
