/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.utils.reflection;

import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MethodMetadataTest {

    private MethodMetadata<MethodMetadataTestClass> testObj;


    @Test
    @DisplayName("Should call method with definied parameters, and return the result")
    public void should_call_method_with_definied_parameters_and_return_the_result() throws NoSuchMethodException {
        MethodMetadataTestClass instance = new MethodMetadataTestClass();
        testObj = new MethodMetadata<>(null, MethodMetadataTestClass.class, instance,
            MethodMetadataTestClass.class.getDeclaredMethod("runInTests", String.class, String.class));

        Boolean result = testObj.invoke("p1", "p2");

        assertAll(
            () -> assertEquals(true, result),
            () -> assertEquals("method ran with parameters: p1, p2", instance.checkAfterTest)
        );
    }

    @Test
    @DisplayName("Should throw exception, if invoke called on method meta without instance")
    public void should_throw_exception_if_invoke_called_on_method_meta_without_instance() throws NoSuchMethodException {
        testObj = new MethodMetadata<>(null, MethodMetadataTestClass.class, null,
            MethodMetadataTestClass.class.getDeclaredMethod("runInTests", String.class, String.class));

        InternalResolverException exception = assertThrows(InternalResolverException.class, () -> testObj.invoke("doesn't", "matter :)"));

        assertEquals("Instance is not set on metadata, to handle request", exception.getMessage());
    }


    public static class MethodMetadataTestClass {
        private String checkAfterTest;

        public boolean runInTests(String firstParam, String secondParam) {
            checkAfterTest = String.format("method ran with parameters: %s, %s", firstParam, secondParam);
            return true;
        }
    }

}
