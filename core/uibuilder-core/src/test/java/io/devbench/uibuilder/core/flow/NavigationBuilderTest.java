/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.flow;

import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NavigationBuilderTest {

    @Mock
    private FlowManager flowManager;

    @BeforeEach
    public void setup() {
        UIContext.getContext().addToActiveUIContextAs(FlowManager.class, flowManager);
    }

    @Test
    public void should_add_url_and_query_parameters_correctly() {
        NavigationBuilder
            .to("fooBar")
            .urlParameter("foo", "bar")
            .queryParameter("xyz", "zzz")
            .navigate();

        verify(flowManager).navigate("fooBar", Collections.singletonMap("foo", "bar"), Collections.singletonMap("xyz", Arrays.asList("zzz")));
    }

    @Test
    public void should_not_deal_with_null_parameters() {
        NavigationBuilder
            .to("fooBar")
            .urlParameter("foo", null)
            .queryParameter("xyz", null)
            .navigate();

        verify(flowManager).navigate("fooBar", Collections.emptyMap(), Collections.emptyMap());
    }

    @Test
    public void null_parameters_should_unset_the_previously_set_values_url_parameters() {
        NavigationBuilder
            .to("fooBar")
            .urlParameter("foo", "bar")
            .queryParameter("xyz", "zzz")
            .urlParameter("foo", null)
            .navigate();

        verify(flowManager).navigate("fooBar", Collections.emptyMap(), Collections.singletonMap("xyz", Arrays.asList("zzz")));
    }

    @Test
    public void should_skip_the_null_values_in_query_parameters() {
        NavigationBuilder
            .to("fooBar")
            .urlParameter("foo", "bar")
            .queryParameter("xyz", "zzz")
            .queryParameter("xyz", null)
            .navigate();

        verify(flowManager).navigate("fooBar", Collections.singletonMap("foo", "bar"), Collections.singletonMap("xyz", Arrays.asList("zzz")));
    }

}
