/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.typesafe.config.Config;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.dom.*;
import com.vaadin.flow.function.SerializableConsumer;
import com.vaadin.flow.internal.ExecutionContext;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.StateTree;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.UIEventHandlerContext;
import io.devbench.uibuilder.core.controllerbean.statenodemanager.StateNodeManager;
import io.devbench.uibuilder.core.page.DomBind;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.internal.util.reflection.FieldSetter;

import java.io.File;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class CustomEventHandlersRegisteringProcessorTest {

    public static final String EVENT_TYPE_REGEX = "uibuilder-custom-event-testBean(1|2|3)::testHandler\\1";

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private DomBind domBind;

    @Mock
    private Element domBindElement;

    @Mock
    private StateNode domBindStateNode;

    @Mock
    private StateNodeManager stateNodeManager;

    @Mock
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    private CustomEventHandlersRegisteringProcessor testObj;

    private PageLoaderContext context;

    @BeforeEach
    private void setup() throws Exception {
        testObj = new CustomEventHandlersRegisteringProcessor();

        when(domBind.getId()).thenReturn(Optional.of("testPage"));

        UIEventHandlerContext eventHandlerContextMock = mock(UIEventHandlerContext.class);
        when(controllerBeanManager.getEventHandlerContext("testBean1::testHandler1")).thenReturn(Optional.of(eventHandlerContextMock));
        when(eventHandlerContextMock.createJsObjectFromRequestedParameters()).thenReturn("{test1: 'value1'}");
        when(eventHandlerContextMock.getDebounceTimeout()).thenReturn(-1);
        when(eventHandlerContextMock.getEventDataExpressions()).thenReturn(Collections.emptyList());

        UIEventHandlerContext eventHandlerContextMock2 = mock(UIEventHandlerContext.class);
        when(controllerBeanManager.getEventHandlerContext("testBean2::testHandler2")).thenReturn(Optional.of(eventHandlerContextMock2));
        when(eventHandlerContextMock2.createJsObjectFromRequestedParameters()).thenReturn("{test2: 'value2'}");
        when(eventHandlerContextMock2.getDebounceTimeout()).thenReturn(-1);
        when(eventHandlerContextMock2.getEventDataExpressions()).thenReturn(Collections.emptyList());

        UIEventHandlerContext eventHandlerContextMock3 = mock(UIEventHandlerContext.class);
        when(controllerBeanManager.getEventHandlerContext("testBean3::testHandler3")).thenReturn(Optional.of(eventHandlerContextMock3));
        when(eventHandlerContextMock3.createJsObjectFromRequestedParameters()).thenReturn("{test3: 'value3'}");
        when(eventHandlerContextMock3.getDebounceTimeout()).thenReturn(200);
        when(eventHandlerContextMock3.getEventDataExpressions()).thenReturn(Collections.emptyList());

        when(controllerBeanManager.getEventHandlerQualifiedNames()).thenReturn(Sets.newSet(
            "testBean1::testHandler1", "testBean2::testHandler2", "testBean3::testHandler3"
        ));
        when(domBind.getElement()).thenReturn(domBindElement);
        when(domBindElement.getNode()).thenReturn(domBindStateNode);
        when(domBindElement.getStateProvider()).thenReturn(mock(ElementStateProvider.class));

        DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(domBindElement.addEventListener(matches(EVENT_TYPE_REGEX), any())).thenReturn(registration);

        context = new PageLoaderContext(null, domBind);
        FieldSetter.setField(context, PageLoaderContext.class.getDeclaredField("stateNodeManager"), stateNodeManager);
    }

    @Test
    @DisplayName("Should register event handler specific event for all event handler in controller beans")
    public void should_register_event_handler_specific_events_for_all_event_handler_in_controller_beans() {
        ArgumentCaptor<SerializableConsumer<UI>> uiConsumerCaptor = ArgumentCaptor.forClass(SerializableConsumer.class);

        doNothing().when(domBindStateNode).runWhenAttached(any());

        testObj.process(context);

        verify(domBindStateNode, times(3)).runWhenAttached(uiConsumerCaptor.capture());

        uiConsumerCaptor.getAllValues().forEach(this::checkUICallsForConsumer);
    }

    private void checkUICallsForConsumer(SerializableConsumer<UI> consumer) {
        UI mockUI = mock(UI.class, Answers.RETURNS_DEEP_STUBS);
        ArgumentCaptor<SerializableConsumer<ExecutionContext>> contextConsumerCaptor = ArgumentCaptor.forClass(SerializableConsumer.class);
        when(mockUI.getInternals().getStateTree().beforeClientResponse(eq(domBindStateNode), any())).thenReturn(mock(StateTree.ExecutionRegistration.class));

        ExecutionContext executionContext = mock(ExecutionContext.class, Answers.RETURNS_DEEP_STUBS);
        Page mockPage = mock(Page.class);
        when(executionContext.getUI().getPage()).thenReturn(mockPage);

        consumer.accept(mockUI);

        verify(mockUI.getInternals().getStateTree()).beforeClientResponse(eq(domBindStateNode), contextConsumerCaptor.capture());

        contextConsumerCaptor.getValue().accept(executionContext);

        verify(mockPage, atLeastOnce()).executeJs(
            matches("document\\.getElementById\\('testPage'\\).testBean(1|2|3)_testHandler\\1 = function\\(event\\) \\{ " +
                "this\\.dispatchEvent\\(new CustomEvent\\('uibuilder-custom-event-testBean\\1::testHandler\\1', \\{test\\1: 'value\\1'\\}\\)\\) " +
                "\\}"));
    }

    @Test
    @DisplayName("Should register event handler listeners that calls ui event handlers and synchronizes properties and statenodes respectively")
    public void should_register_event_handler_listeners_that_calls_ui_event_handlers_and_synchronizes_properties_and_statenodes_respectively() {
        ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);

        testObj.process(context);

        verify(domBindElement, times(3)).addEventListener(matches(EVENT_TYPE_REGEX), listenerCaptor.capture());

        listenerCaptor.getAllValues().forEach(listener -> listener.handleEvent(mock(DomEvent.class)));

        verify(stateNodeManager, times(3)).synchronizeProperties();
        verify(controllerBeanManager, times(3)).callEventHandlers(matches("testBean(1|2|3)::testHandler\\1"), eq(stateNodeManager), any(DomEvent.class));
        verify(stateNodeManager, times(3)).synchronizeStateNodes();
    }

    @Test
    @DisplayName("Should register event handler event as a property synchronizer event")
    public void should_register_event_handler_event_as_a_property_synchronizer_event() {
        testObj.process(context);
        verify(domBindElement.getStateProvider(), times(3))
            .addSynchronizedProperty(
                eq(domBindElement.getNode()), matches("uibuilder-custom-event-testBean(1|2|3)::testHandler\\1"), eq(DisabledUpdateMode.ALWAYS));
    }

    @Test
    @DisplayName("Should register event handler with debounce timeout set")
    public void should_register_event_handler_with_debounce_timeout_set() {
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(domBind.getElement().addEventListener(eq("uibuilder-custom-event-testBean3::testHandler3"), any(DomEventListener.class)))
            .thenReturn(registration);

        testObj.process(context);

        verify(registration).debounce(200, DebouncePhase.TRAILING);
    }

    @Test
    @DisplayName("Should register event handler with default debounce")
    public void should_register_event_handler_with_default_debounce() {
        testObj = spy(testObj);
        final Config config = mock(Config.class);
        when(testObj.getConfig()).thenReturn(config);
        when(config.hasPath("eventHandlers.debounce")).thenReturn(true);
        when(config.getInt("eventHandlers.debounce")).thenReturn(100);
        final DomListenerRegistration registration = mock(DomListenerRegistration.class);
        when(domBind.getElement().addEventListener(eq("uibuilder-custom-event-testBean2::testHandler2"), any(DomEventListener.class)))
            .thenReturn(registration);
        final DomListenerRegistration registration2 = mock(DomListenerRegistration.class);
        when(domBind.getElement().addEventListener(eq("uibuilder-custom-event-testBean3::testHandler3"), any(DomEventListener.class)))
            .thenReturn(registration2);

        testObj.process(context);

        assertAll(
            () -> verify(registration).debounce(100, DebouncePhase.TRAILING),
            () -> verify(registration2).debounce(200, DebouncePhase.TRAILING)
        );

    }

}
