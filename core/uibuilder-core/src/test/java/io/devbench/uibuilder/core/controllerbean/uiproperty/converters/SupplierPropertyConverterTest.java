/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty.converters;

import com.vaadin.flow.server.VaadinSession;
import io.devbench.uibuilder.core.templateparser.UIBuilderMethodReference;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import io.devbench.uibuilder.core.utils.reflection.MethodMetadata;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class SupplierPropertyConverterTest {

    private SupplierPropertyConverter testObj = new SupplierPropertyConverter();

    @Test
    public void should_convert_supplier_to_string_and_string_to_the_same_supplier_correctly() {
        Supplier supplier = (Supplier<?> & Serializable) () -> null;

        String supplierId = testObj.convertTo(supplier);
        Supplier actual = testObj.apply(supplierId);

        assertEquals(supplier, actual);
    }

    public static class TestControllerBean {
        public boolean called = false;

        public Object method() {
            called = true;
            return null;
        }
    }

    @Test

    public void should_supply_suppliers_from_uiuilder_method_reference_objects_correctly() {
        TestControllerBean testControllerBean = new TestControllerBean();
        UIBuilderMethodReference uiBuilderMethodReference = new UIBuilderMethodReference(
            TestControllerBean.class,
            testControllerBean,
            ClassMetadata.ofValue(testControllerBean)
                .getMethods()
                .stream()
                .filter(it -> Objects.equals(it.getName(), "method"))
                .map(this::unreflect)
                .collect(Collectors.toMap(
                    MethodHandle::type,
                    Function.identity()
                ))
        );

        VaadinSession.getCurrent().setAttribute("test", uiBuilderMethodReference);

        assertFalse(testControllerBean.called);
        testObj.apply("test").get();
        assertTrue(testControllerBean.called);
    }

    public static class Base {
        public boolean baseCalled = false;

        public Object foo() {
            baseCalled = true;
            return new Object();
        }
    }

    public static class Child extends Base {
        public boolean childCalled = false;

        @Override
        public Object foo() {
            childCalled = true;
            return new Object();
        }
    }

    @Test
    public void should_collect_correct_method_when_inheritance_is_present() {
        Child child = new Child();

        UIBuilderMethodReference uiBuilderMethodReference = new UIBuilderMethodReference(
            Child.class,
            child,
            ClassMetadata.ofValue(child)
                .getMethods()
                .stream()
                .filter(it -> Objects.equals(it.getName(), "foo"))
                .map(this::unreflect)
                .collect(Collectors.toMap(
                    MethodHandle::type,
                    Function.identity()
                ))
        );

        VaadinSession.getCurrent().setAttribute("test", uiBuilderMethodReference);

        assertFalse(child.childCalled);
        assertFalse(child.baseCalled);
        testObj.apply("test").get();
        assertTrue(child.childCalled);
        assertFalse(child.baseCalled);
    }

    @SneakyThrows
    private MethodHandle unreflect(MethodMetadata<?> it) {
        return it.unreflect();
    }
}
