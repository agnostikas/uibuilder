/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.dynamictheme;

import com.typesafe.config.ConfigFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.server.WebBrowser;
import com.vaadin.flow.theme.AbstractTheme;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.theme.lumo.UibuilderLumo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class ConfigurationBasedThemeTest {

    @Mock
    private AbstractTheme theme;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private UI mockUI;

    private ConfigurationBasedTheme testObj;

    @BeforeEach
    private void setup() {
        MockTheme.mockDelegate = theme;
        writeContentToConfig(
            "theme.class=io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedThemeTest$MockTheme\n" +
                "theme.variant=dark");
        ConfigFactory.invalidateCaches();
        testObj = new ConfigurationBasedTheme();
    }

    @Test
    @DisplayName("Should load the theme as a delegate specified in the configuration")
    public void should_load_the_theme_as_a_delegate_specified_in_the_configuration() {
        assertEquals(MockTheme.class, testObj.getDelegatedTheme().getClass());
    }

    @Test
    @DisplayName("Should load the default theme, if the configured theme cannot be loaded")
    public void should_load_the_default_theme_if_the_configured_theme_cannot_be_loaded() {
        writeContentToConfig("theme.class=invalid.package.InvalidThemeClass");
        ConfigFactory.invalidateCaches();

        testObj = new ConfigurationBasedTheme();

        assertEquals(UibuilderLumo.class, testObj.getDelegatedTheme().getClass());
    }

    @Test
    @DisplayName("Should throw exception when the configured theme or the default theme cannot be loaded")
    public void should_throw_exception_when_the_configured_theme_or_the_default_theme_cannot_be_loaded() {
        ConfigurationBasedTheme.setDefaultThemeClassName("invalid.InvalidDefaultTheme");
        writeContentToConfig("theme.class=invalid.package.InvalidThemeClass");
        ConfigFactory.invalidateCaches();

        assertEquals(
            "Cannot load the configured theme nor the default theme",
            assertThrows(NullPointerException.class, ConfigurationBasedTheme::new).getMessage()
        );
    }

    @Test
    @DisplayName("Should delegate the getThemeUrl, getBaseUrl and translateUrl to the configured theme")
    public void should_delegate_the_get_theme_url_get_base_url_and_translate_url_to_the_configured_theme() {
        when(theme.getThemeUrl()).thenReturn("testThemeUrl");
        when(theme.getBaseUrl()).thenReturn("testBaseUrl");
        when(theme.translateUrl("testUrl")).thenReturn("translatedUrl");

        String themeUrl = testObj.getThemeUrl();
        String baseUrl1 = testObj.getBaseUrl();
        String translatedUrl = testObj.translateUrl("testUrl");

        assertAll(
            () -> assertEquals("testThemeUrl", themeUrl),
            () -> assertEquals("testBaseUrl", baseUrl1),
            () -> assertEquals("translatedUrl", translatedUrl),
            () -> verify(theme).getThemeUrl(),
            () -> verify(theme).getBaseUrl(),
            () -> verify(theme).translateUrl("testUrl")
        );
    }

    @Test
    @DisplayName("Should delegate the getBodyAttributes call to the configured theme, and add the variant configured as a 'theme' attribute to it")
    public void should_delegate_the_get_body_attributes_call_to_the_configured_theme_and_add_the_variant_configured_as_a_theme_attribute_to_it() {
        Map<String, String> bodyAttributes = new HashMap<>();
        bodyAttributes.put("testAttributeName", "testAttributeValue");
        when(theme.getHtmlAttributes(null)).thenReturn(bodyAttributes);

        Map<String, String> fullAttributeMap = testObj.getHtmlAttributes(null);

        assertAll(
            () -> verify(theme).getHtmlAttributes(null),
            () -> assertEquals("testAttributeValue", fullAttributeMap.get("testAttributeName")),
            () -> assertEquals("dark", fullAttributeMap.get("theme"))
        );
    }

    @Test
    @DisplayName("Should add empty string to the body attributes, if the configuration doesn't contain theme variant setting")
    public void should_add_empty_string_to_the_body_attributes_if_the_configuration_doesn_t_contain_theme_variant_setting() {
        writeContentToConfig("theme.class=io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedThemeTest$MockTheme");
        ConfigFactory.invalidateCaches();

        testObj = new ConfigurationBasedTheme();

        when(theme.getHtmlAttributes(null)).thenReturn(Collections.emptyMap());

        Map<String, String> fullAttributeMap = testObj.getHtmlAttributes(null);

        assertAll(
            () -> verify(theme).getHtmlAttributes(null),
            () -> assertEquals(1, fullAttributeMap.size()),
            () -> assertEquals("", fullAttributeMap.get("theme"))
        );
    }

    @Test
    @DisplayName("Should delegate the getHeaderInlineContents to the configured theme")
    public void should_delegate_the_get_header_inline_contents_to_the_configured_theme() {
        WebBrowser browser = mock(WebBrowser.class);
        Page page = mock(Page.class);
        when(UI.getCurrent().getPage()).thenReturn(page);
        when(UI.getCurrent().getSession().getBrowser()).thenReturn(browser);
        when(UI.getCurrent().getSession().getService().resolveResource("test/html/import.html", browser)).thenReturn("resolvedUrl");

        when(theme.getHeaderInlineContents()).thenReturn(Collections.emptyList());

        List<String> inlineContents = testObj.getHeaderInlineContents();

        assertAll(
            () -> verify(theme).getHeaderInlineContents(),
            () -> assertEquals(Collections.emptyList(), inlineContents)
        );
    }

    @Test
    @DisplayName("Should collect all JS from the configured theme and it's super classes")
    public void should_collect_all_js_import_from_the_configured_theme_and_it_s_super_classes() {
        writeContentToConfig("theme.class=io.devbench.uibuilder.core.dynamictheme.ConfigurationBasedThemeTest$SubClassMockTheme");
        ConfigFactory.invalidateCaches();

        WebBrowser browser = mock(WebBrowser.class);
        Page page = mock(Page.class);
        when(UI.getCurrent().getPage()).thenReturn(page);
        when(UI.getCurrent().getSession().getBrowser()).thenReturn(browser);

        testObj = new ConfigurationBasedTheme();

        when(theme.getHeaderInlineContents()).thenReturn(Collections.emptyList());

        List<String> themeJsImports = testObj.getThemeJsImports();
        assertAll(
            () -> assertEquals(3, themeJsImports.size()),
            () -> assertIterableEquals(
                Arrays.asList(
                    "/test/html/import.js",
                    "/sub/test/import1.js",
                    "/sub/test/import2.js"
                ),
                themeJsImports)
        );
    }

    private void writeContentToConfig(String content) {
        try (PrintWriter writer = new PrintWriter(new File(this.getClass().getResource("/application.properties").getPath()))) {
            writer.append(content);
            writer.flush();
        } catch (Throwable throwable) {
            fail("Configuration content cannot be written.", throwable);
        }
    }


    @JsModule("./test/html/import.js")
    public static class MockTheme implements AbstractTheme {

        private static AbstractTheme mockDelegate;

        @Override
        public List<String> getHeaderInlineContents() {
            return mockDelegate.getHeaderInlineContents();
        }

        @Override
        public Map<String, String> getHtmlAttributes(String variant) {
            return mockDelegate.getHtmlAttributes(variant);
        }

        @Override
        public String translateUrl(String url) {
            return mockDelegate.translateUrl(url);
        }

        @Override
        public String getBaseUrl() {
            return mockDelegate.getBaseUrl();
        }

        @Override
        public String getThemeUrl() {
            return mockDelegate.getThemeUrl();
        }
    }

    @JsModule("./sub/test/import1.js")
    @JsModule("./sub/test/import2.js")
    public static class SubClassMockTheme extends MockTheme {
    }

}
