/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.SerializableConsumer;
import com.vaadin.flow.internal.ExecutionContext;
import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.StateTree;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.page.DomBind;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.core.session.context.UIContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class FlowNavigationMethodsRegisteringProcessorTest {

    @Mock
    private Page mockPage;

    @Mock
    private StateTree stateTree;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private UI mockUI;

    @Mock
    private Element domBindElement;

    @Mock
    private StateNode domBindStateNode;

    @Mock
    private DomBind domBind;

    @Mock
    private FlowManager flowManager;

    private FlowNavigationMethodsRegisteringProcessor testObj;

    private PageLoaderContext context;

    @BeforeEach
    private void setup() {
        testObj = new FlowNavigationMethodsRegisteringProcessor();
        context = new PageLoaderContext(null, domBind);
        context.getFlowNavigationMethods().addAll(Arrays.asList("____flow_navigation_to____test_method1", "____flow_navigation_to____test_method2"));
        when(domBind.getElement()).thenReturn(domBindElement);
        when(domBindElement.getNode()).thenReturn(domBindStateNode);
        when(domBind.getId()).thenReturn(Optional.of("test-id"));
        when(mockUI.getInternals().getStateTree()).thenReturn(stateTree);
        when(mockUI.getPage()).thenReturn(mockPage);
    }

    @Test
    @DisplayName("Should register flow navigation functions, that send event containing the navigation function name, to the dom bind node")
    public void should_register_flow_navigation_functions_that_send_events_containing_the_navigation_function_name_to_the_dom_bind_node() {
        ArgumentCaptor<SerializableConsumer<UI>> uiConsumerCaptor = ArgumentCaptor.forClass(SerializableConsumer.class);

        testObj.process(context);

        verify(domBindStateNode, times(2)).runWhenAttached(uiConsumerCaptor.capture());

        ArgumentCaptor<SerializableConsumer<ExecutionContext>> contextConsumerCaptor = ArgumentCaptor.forClass(SerializableConsumer.class);
        uiConsumerCaptor.getAllValues().forEach(consumer -> consumer.accept(mockUI));

        verify(stateTree, times(2)).beforeClientResponse(eq(domBindStateNode), contextConsumerCaptor.capture());

        ExecutionContext executionContext = mock(ExecutionContext.class);
        when(executionContext.getUI()).thenReturn(mockUI);
        contextConsumerCaptor.getAllValues().forEach(consumer -> consumer.accept(executionContext));

        verify(mockPage, times(2)).executeJs(matches(
            "document\\.getElementById\\('test-id'\\)\\.____flow_navigation_to____test_method(1|2) = function\\(event\\) \\{ " +
                "this\\.dispatchEvent\\(new CustomEvent\\('uibuilder-custom-event-____flow_navigation_to____test_method\\1', \\{\\}\\)\\) " +
                "\\}"
        ));
    }

    @Test
    @DisplayName("Should register event listeners for the event containing the flow method names, sent by the registered javascript functions")
    public void should_register_event_listeners_for_the_events_containing_the_flow_method_names_sent_by_the_registered_javascript_functions() {
        mockUI = UI.getCurrent();
        UIContext.getContext().addToActiveUIContextAs(FlowManager.class, flowManager);

        ArgumentCaptor<DomEventListener> listenerCaptor = ArgumentCaptor.forClass(DomEventListener.class);

        testObj.process(context);

        verify(domBindElement, times(2))
            .addEventListener(matches("uibuilder-custom-event-____flow_navigation_to____test_method(1|2)"), listenerCaptor.capture());

        listenerCaptor.getAllValues().forEach(listener -> listener.handleEvent(mock(DomEvent.class)));

        verify(flowManager, times(2)).navigate(matches("test_method(1|2)"), eq(true));
    }

}
