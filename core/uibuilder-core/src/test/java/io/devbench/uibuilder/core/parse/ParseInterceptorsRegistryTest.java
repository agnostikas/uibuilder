/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.parse;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.api.parse.ParseInterceptor;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class ParseInterceptorsRegistryTest {

    @Mock
    @SingletonInstance(MemberScanner.class)
    private MemberScanner memberScanner;

    @Mock
    private Element mockElement;

    private ParseInterceptorsRegistry testObj;

    @BeforeEach
    private void setup() {
        Set<Class<? extends ParseInterceptor>> classes = new HashSet<>(Arrays.asList(TestParseInterceptor.class, TestNotApplicableParseInterceptor.class));
        when(memberScanner.findClassesBySuperType(ParseInterceptor.class)).thenReturn(classes);
        testObj = ParseInterceptorsRegistry.registerToActiveContext();
    }

    @Test
    @DisplayName("Should find the applicable parse interceptor instances by the given element")
    public void should_find_the_applicable_parse_interceptor_instances_by_the_given_element() {
        Set<ParseInterceptor> interceptors = testObj.getInterceptorsByElement(mockElement);

        assertAll(
            () -> assertEquals(1, interceptors.size()),
            () -> assertTrue(interceptors.iterator().next() instanceof TestParseInterceptor),
            () -> assertSame(mockElement, TestParseInterceptor.lastElement)
        );
    }


    public static class TestParseInterceptor implements ParseInterceptor {

        private static Element lastElement;

        @Override
        public void intercept(Component component, Element element) { }

        @Override
        public boolean isApplicable(Element element) {
            lastElement = element;
            return true;
        }
    }

    public static class TestNotApplicableParseInterceptor implements ParseInterceptor {

        @Override
        public void intercept(Component component, Element element) { }

        @Override
        public boolean isApplicable(Element element) {
            return false;
        }
    }

}
