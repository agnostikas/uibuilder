/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page;

import com.vaadin.flow.component.Component;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.flow.FlowManager;
import io.devbench.uibuilder.core.page.pageloaderprocessors.PageLoaderProcessor;
import io.devbench.uibuilder.core.page.pageloaderprocessors.PageUnloadProcessor;
import io.devbench.uibuilder.core.parse.ParseInterceptorsRegistry;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
class PageLoaderTest {

    @Mock
    private DomBind mockDomBind;

    @Mock
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    @Mock
    @SingletonInstance(ParseInterceptorsRegistry.class)
    private ParseInterceptorsRegistry parseInterceptorsRegistry;

    private FlowManager flowManager;

    private MockPageLoader testObj;

    @BeforeEach
    void setup() {
        flowManager = new FlowManager("/FlowManagerTest/simple/flow.xml");
        MockPageLoader.clearProcessorsList();
        testObj = new MockPageLoader("testHtmlPath", mockDomBind);
    }

    @Test
    @DisplayName("Should register page loader to html path in the current manager")
    void test_should_register_page_loader_to_html_path_in_the_current_manager() {
        assertTrue(flowManager.findPageLoader("testHtmlPath").isPresent());
    }

    @Test
    @DisplayName("Should register page loader processor to page loader")
    public void should_register_page_loader_processor_to_page_loader() {
        MockProcessor1 mockProcessor1 = new MockProcessor1();
        MockPageLoader.registerProcessor(mockProcessor1);
        assertAll(
            () -> assertEquals(1, MockPageLoader.getProcessors().size()),
            () -> assertSame(mockProcessor1, MockPageLoader.getProcessors().get(0))
        );
    }

    @Test
    @DisplayName("Should register page unload processor to page loader")
    public void should_register_page_unload_processor_to_page_loader() {
        MockUnloadProcessor1 mockUnloadProcessor1 = new MockUnloadProcessor1();
        MockPageLoader.registerUnloadProcessor(mockUnloadProcessor1);

        assertAll(
            () -> assertEquals(1, MockPageLoader.getUnloadProcessors().size()),
            () -> assertSame(mockUnloadProcessor1, MockPageLoader.getUnloadProcessors().get(0))
        );
    }

    @Test
    @DisplayName("Should register page loader processor after the specified processor class")
    public void should_register_page_loader_processor_after_the_specified_processor_class() {
        MockProcessor1 mockProcessor1 = new MockProcessor1();
        MockPageLoader.registerProcessor(mockProcessor1);
        MockProcessor2 mockProcessor2 = new MockProcessor2();
        MockPageLoader.registerProcessorAfter(MockProcessor1.class, mockProcessor2);
        assertAll(
            () -> assertEquals(2, MockPageLoader.getProcessors().size()),
            () -> assertSame(mockProcessor1, MockPageLoader.getProcessors().get(0)),
            () -> assertSame(mockProcessor2, MockPageLoader.getProcessors().get(1))
        );
    }

    @Test
    @DisplayName("Should register page unload processor after the specified unload processor class")
    public void should_register_page_unload_processor_after_the_specified_unload_processor_class() {
        MockUnloadProcessor1 mockUnloadProcessor1 = new MockUnloadProcessor1();
        MockUnloadProcessor2 mockUnloadProcessor2 = new MockUnloadProcessor2();
        MockPageLoader.registerUnloadProcessor(mockUnloadProcessor1);
        MockPageLoader.registerUnloadProcessorAfter(MockUnloadProcessor1.class, mockUnloadProcessor2);

        assertAll(
            () -> assertEquals(2, MockPageLoader.getUnloadProcessors().size()),
            () -> assertSame(mockUnloadProcessor1, MockPageLoader.getUnloadProcessors().get(0)),
            () -> assertSame(mockUnloadProcessor2, MockPageLoader.getUnloadProcessors().get(1))
        );
    }

    @Test
    @DisplayName("Should throw exception when the specified page loader processor is not in the list, during the registration of a processor")
    public void should_throw_exception_when_the_specified_page_loader_processor_is_not_in_the_list_during_the_registration_of_a_processor() {
        MockProcessor2 mockProcessor2 = new MockProcessor2();
        InternalResolverException exception = assertThrows(InternalResolverException.class,
            () -> MockPageLoader.registerProcessorAfter(MockProcessor1.class, mockProcessor2));
        assertEquals("No processor found with class name: MockProcessor1", exception.getMessage());
    }

    @Test
    @DisplayName("Should call the process method in all registered page loader processors, when loading a page, with the correct context")
    public void should_call_the_process_method_in_all_registered_page_loader_processors_when_loading_a_page_with_the_correct_context() {
        MockProcessor1 mockProcessor1 = mock(MockProcessor1.class);
        ArgumentCaptor<PageLoaderContext> contextCaptor = ArgumentCaptor.forClass(PageLoaderContext.class);
        doNothing().when(mockProcessor1).process(contextCaptor.capture());
        MockProcessor2 mockProcessor2 = mock(MockProcessor2.class);

        MockPageLoader.registerProcessor(mockProcessor1);
        MockPageLoader.registerProcessor(mockProcessor2);

        testObj.load();

        assertAll(
            () -> verify(mockProcessor1).process(any()),
            () -> verify(mockProcessor2).process(any()),
            () -> assertNotNull(contextCaptor.getValue()),
            () -> assertEquals("testHtmlPath", contextCaptor.getValue().getHtmlPath()),
            () -> assertSame(mockDomBind, contextCaptor.getValue().getDomBind()),
            () -> assertNotNull(contextCaptor.getValue().getStateNodeManager()),
            () -> assertNotNull(contextCaptor.getValue().getBindings()),
            () -> assertNotNull(contextCaptor.getValue().getFlowNavigationMethods())
        );
    }

    @Test
    @DisplayName("Should call the page unload processors with the correct context")
    public void should_call_the_page_unload_processors_with_the_correct_context() {
        MockUnloadProcessor1 mockProcessor1 = mock(MockUnloadProcessor1.class);
        ArgumentCaptor<PageLoaderContext> contextCaptor = ArgumentCaptor.forClass(PageLoaderContext.class);
        doNothing().when(mockProcessor1).process(contextCaptor.capture());
        MockUnloadProcessor2 mockProcessor2 = mock(MockUnloadProcessor2.class);

        MockPageLoader.registerUnloadProcessor(mockProcessor1);
        MockPageLoader.registerUnloadProcessor(mockProcessor2);

        testObj.unload();

        assertAll(
            () -> verify(mockProcessor1).process(any()),
            () -> verify(mockProcessor2).process(any()),
            () -> assertNotNull(contextCaptor.getValue()),
            () -> assertEquals("testHtmlPath", contextCaptor.getValue().getHtmlPath()),
            () -> assertSame(mockDomBind, contextCaptor.getValue().getDomBind()),
            () -> assertNotNull(contextCaptor.getValue().getStateNodeManager()),
            () -> assertNotNull(contextCaptor.getValue().getBindings()),
            () -> assertNotNull(contextCaptor.getValue().getFlowNavigationMethods())
        );
    }

    @Test
    @DisplayName("Should load fragment and return modified html")
    void test_should_load_fragment_and_return_modified_html() {
        Component parentComponent = mock(Component.class);
        doReturn(Optional.of("parent-id")).when(parentComponent).getId();
        List<String> additionalBindings = Collections.emptyList();

        String html = "<div>" +
            "<uibuilder-combobox id=\"cb-id-1\">" +
            "   <template>" +
            "       {{item.text}}" +
            "   </template>" +
            "</uibuilder-combobox>" +
            "</div>";

        doReturn(Collections.emptyMap()).when(controllerBeanManager).findInjectionPointsById();
        doReturn(Collections.emptySet()).when(parseInterceptorsRegistry).getInterceptorsByElement(any(Element.class));

        testObj.loadFragment(html, parentComponent, additionalBindings);

        ArgumentCaptor<String> htmlArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parentIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockDomBind).dispatchInnerHtmlFragmentSet(htmlArgumentCaptor.capture(), parentIdArgumentCaptor.capture());

        String modifiedHtml = htmlArgumentCaptor.getValue();
        String parentId = parentIdArgumentCaptor.getValue();

        assertNotNull(modifiedHtml);
        assertNotNull(parentId);

        assertEquals("parent-id", parentId);
        assertEquals("<div>\n" +
                " <uibuilder-combobox id=\"cb-id-1\"> \n" +
                "  <template>\n" +
                "    {{item.text}} \n" +
                "  </template>\n" +
                " </uibuilder-combobox>\n" +
                "</div>",
            modifiedHtml);
    }

    static class MockPageLoader extends PageLoader {

        MockPageLoader(String htmlPath, DomBind domBind) {
            super(htmlPath, domBind, null);
        }

        static List<PageLoaderProcessor> getProcessors() {
            return pageLoaderProcessors;
        }

        static List<PageUnloadProcessor> getUnloadProcessors() {
            return pageUnloadProcessors;
        }

        static void clearProcessorsList() {
            pageLoaderProcessors.clear();
            pageUnloadProcessors.clear();
        }

    }

    static class MockProcessor1 extends PageLoaderProcessor {
        @Override
        public void process(PageLoaderContext context) {

        }
    }

    static class MockProcessor2 extends PageLoaderProcessor {
        @Override
        public void process(PageLoaderContext context) {

        }
    }

    static class MockUnloadProcessor1 extends PageUnloadProcessor {
        @Override
        public void process(PageLoaderContext context) {
        }
    }

    static class MockUnloadProcessor2 extends PageUnloadProcessor {
        @Override
        public void process(PageLoaderContext context) {
        }
    }

}
