/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean;

import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.api.exceptions.InternalResolverException;
import io.devbench.uibuilder.core.controllerbean.injection.InjectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UIEventHandlerContextTest {

    @Test
    @DisplayName("should return null if item is null")
    void test_should_return_null_if_item_is_null() {
        assertNull(InjectionUtils.getCollectionAwareItem(null, null));
    }

    @Test
    @DisplayName("should return null if parameter is single item and collection is empty")
    void test_should_return_null_if_parameter_is_single_item_and_collection_is_empty() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSingleItem", TestItem.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        assertNull(InjectionUtils.getCollectionAwareItem(Collections.emptySet(), parameter.getType()));
    }

    @Test
    @DisplayName("should return null if parameter is single item and collection contains more than 1 item")
    void test_should_return_null_if_parameter_is_single_item_and_collection_contains_more_than_1_item() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSingleItem", TestItem.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        UIEventHandlerContext context = UIEventHandlerContext.buildFromControllerBeanMethod("test", method);
        List<TestItem> items = Arrays.asList(new TestItem(), new TestItem());

        assertNull(InjectionUtils.getCollectionAwareItem(items, parameter.getType()));
    }

    @Test
    @DisplayName("should return the only item if parameter is single item and collection contains exactly one item")
    void test_should_return_the_only_item_if_parameter_is_single_item_and_collection_contains_exactly_one_item() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSingleItem", TestItem.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        TestItem item = new TestItem();
        List<TestItem> items = Arrays.asList(item);

        assertAll(
            () -> assertNotNull(InjectionUtils.getCollectionAwareItem(items, parameter.getType())),
            () -> assertSame(item, InjectionUtils.getCollectionAwareItem(items, parameter.getType()))
        );
    }

    @Test
    @DisplayName("Should call method with item and values")
    void test_should_call_method_with_item_and_values() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSingleItemAndValues", TestItem.class, String.class, Integer.class);
        TestItem item = new TestItem();

        Map<String, Object> values = new HashMap<>();
        values.put("name", "The name");
        values.put("age", 30);

        AtomicReference<TestItem> itemReference = new AtomicReference<>();
        AtomicReference<String> nameReference = new AtomicReference<>();
        AtomicReference<Integer> ageReference = new AtomicReference<>();

        ControllerBeanManager controllerBeanManager = mock(ControllerBeanManager.class);
        TestControllerBean testControllerBean = new TestControllerBean() {
            @Override
            public void testMethodWithSingleItemAndValues(TestItem item, String name, Integer age) {
                itemReference.set(item);
                nameReference.set(name);
                ageReference.set(age);
            }
        };
        doReturn(testControllerBean).when(controllerBeanManager).getControllerBean("test");

        UIEventHandlerContext context = UIEventHandlerContext.buildFromControllerBeanMethod("test", method);
        context = Mockito.spy(context);
        doReturn(controllerBeanManager).when(context).getControllerBeanManager();

        context.callEventHandlerWithItem(item, null, values);

        assertSame(item, itemReference.get());
        assertEquals("The name", nameReference.get());
        assertEquals(30, ageReference.get());
    }

    @Test
    @DisplayName("should return unmodifiable collection if parameter is a collection and item is collection too")
    void test_should_return_unmodifiable_collection_if_parameter_is_a_collection_and_item_is_collection_too() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithCollectionItem", Collection.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        TestItem item1 = new TestItem();
        TestItem item2 = new TestItem();
        List<TestItem> items = Arrays.asList(item1, item2);

        Object result = InjectionUtils.getCollectionAwareItem(items, parameter.getType());

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result instanceof Collection),
            () -> assertEquals(2, ((Collection) result).size()),
            () -> assertNotSame(items, result),
            () -> assertThrows(UnsupportedOperationException.class, () -> ((Collection) result).add(new TestItem()), "Result collection should be unmodifiable")
        );
    }

    @Test
    @DisplayName("should return unmodifiable list if parameter is a list and item is collection too")
    void test_should_return_unmodifiable_list_if_parameter_is_a_list_and_item_is_collection_too() throws Exception{
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithListItem", List.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        TestItem item1 = new TestItem();
        TestItem item2 = new TestItem();
        List<TestItem> items = Arrays.asList(item1, item2);

        Object result = InjectionUtils.getCollectionAwareItem(items, parameter.getType());

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result instanceof Collection),
            () -> assertEquals(2, ((Collection) result).size()),
            () -> assertNotSame(items, result),
            () -> assertTrue(result instanceof List, "Result should be an unmodifiable list"),
            () -> assertThrows(UnsupportedOperationException.class, () -> ((Collection) result).add(new TestItem()), "Result collection should be unmodifiable")
        );
    }

    @Test
    @DisplayName("should return unmodifiable set if parameter is a set and item is collection too")
    void test_should_return_unmodifiable_set_if_parameter_is_a_set_and_item_is_collection_too() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSetItem", Set.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        TestItem item1 = new TestItem();
        TestItem item2 = new TestItem();
        Set<TestItem> items = new HashSet<>(Arrays.asList(item1, item2));

        Object result = InjectionUtils.getCollectionAwareItem(items, parameter.getType());

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result instanceof Collection),
            () -> assertEquals(2, ((Collection) result).size()),
            () -> assertNotSame(items, result),
            () -> assertTrue(result instanceof Set, "Result should be an unmodifiable set"),
            () -> assertThrows(UnsupportedOperationException.class, () -> ((Collection) result).add(new TestItem()), "Result collection should be unmodifiable")
        );
    }

    @Test
    @DisplayName("should return item if paramer is single item and item is single item too")
    void test_should_return_item_if_paramer_is_single_item_and_item_is_single_item_too() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSingleItem", TestItem.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        TestItem item = new TestItem();
        Object result = InjectionUtils.getCollectionAwareItem(item, parameter.getType());

        assertAll(
            () -> assertNotNull(result),
            () -> assertSame(item, result)
        );
    }

    @Test
    @DisplayName("should return singleton collection if parameter is a collection and item is a single item")
    void test_should_return_singleton_collection_if_parameter_is_a_collection_and_item_is_a_single_item() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithCollectionItem", Collection.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        UIEventHandlerContext context = UIEventHandlerContext.buildFromControllerBeanMethod("test", method);

        TestItem item = new TestItem();
        Object result = InjectionUtils.getCollectionAwareItem(item, parameter.getType());

        assertAll(
            () -> assertNotNull(result),
            () -> assertTrue(result instanceof Collection),
            () -> assertEquals(1, ((Collection) result).size()),
            () -> assertSame(item, ((Collection) result).iterator().next())
        );
    }

    @Test
    @DisplayName("should throw exception if parameter type is not assignable from item type")
    void test_should_throw_exception_if_parameter_type_is_not_assignable_from_item_type() throws Exception {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithSetItem", Set.class);
        Parameter parameter = Arrays.stream(method.getParameters()).filter(p -> p.isAnnotationPresent(Item.class)).findFirst().get();
        UIEventHandlerContext context = UIEventHandlerContext.buildFromControllerBeanMethod("test", method);

        Assertions.assertThrows(
            InternalResolverException.class,
            () -> InjectionUtils.getCollectionAwareItem(Arrays.asList(new TestItem(), new TestItem()), parameter.getType()),
            "Should throw exception if item is not assignable from parameter type");
    }

    @Test
    @DisplayName("Should collect debounce information")
    public void should_collect_debounce_information() throws NoSuchMethodException {
        Method method = TestControllerBean.class.getDeclaredMethod("testMethodWithoutItem");
        UIEventHandlerContext context = UIEventHandlerContext.buildFromControllerBeanMethod("test", method);
        assertEquals(100, context.getDebounceTimeout());
    }

    public static class TestItem {

    }

    public static class TestControllerBean {

        @UIEventHandler(value = "wihtoutItem", debounce = 100)
        public void testMethodWithoutItem() {

        }

        @UIEventHandler(value = "singleItem")
        public void testMethodWithSingleItem(@Item TestItem item) {

        }

        @UIEventHandler(value = "collectionItem")
        public void testMethodWithCollectionItem(@Item Collection<TestItem> items) {

        }

        @UIEventHandler(value = "setItem")
        public void testMethodWithSetItem(@Item Set<TestItem> items) {

        }

        @UIEventHandler(value = "listItem")
        public void testMethodWithListItem(@Item List<TestItem> items) {

        }

        @UIEventHandler(value = "itemWithValues")
        public void testMethodWithSingleItemAndValues(@Item TestItem item, @Value("name") String name, @Value("age") Integer age) {

        }

    }

}
