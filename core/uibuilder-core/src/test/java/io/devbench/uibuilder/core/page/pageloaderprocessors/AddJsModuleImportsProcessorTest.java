/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.router.Router;
import com.vaadin.flow.server.*;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class AddJsModuleImportsProcessorTest {

    @Mock
    private Document document;

    @Mock
    private Page page;

    @Mock
    private UI mockUI;

    @Mock
    private VaadinSession session;

    @Mock
    private VaadinServletService service;

    @Mock
    private WebBrowser browser;

    @Mock
    private Router router;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private VaadinServlet servlet;

    private PageLoaderContext context;

    private AddJsModuleImportsProcessor testObj;

    @BeforeEach
    private void setup() {
        VaadinService.setCurrent(service);
        when(service.getServlet()).thenReturn(servlet);
        when(servlet.getServletContext().getContextPath()).thenReturn("");
        when(mockUI.getPage()).thenReturn(page);
        when(mockUI.getSession()).thenReturn(session);
        when(session.getService()).thenReturn(service);
        when(session.getBrowser()).thenReturn(browser);
        when(mockUI.getRouter()).thenReturn(router);

        context = new PageLoaderContext();
        context.setDocument(document);

        testObj = new AddJsModuleImportsProcessor();
    }

}
