/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.injection;

import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Source;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Value;
import io.devbench.uibuilder.core.controllerbean.injection.validator.HasMainAnnotationInjectionValidator;
import io.devbench.uibuilder.core.controllerbean.injection.validator.InjectionAnnotation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.validation.constraints.NotNull;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


class CommonInjectionValidatorTest {

    @Test
    public void shouldBeKnown() {
        Stream.of(
            get(getAnnotation("a")),
            get(getAnnotation("b")),
            get(getAnnotation("c")),
            get(getAnnotation("d")))
            .forEach(annotationTable -> {
                assertFalse(annotationTable.isEmpty());
                Assertions.assertTrue(HasMainAnnotationInjectionValidator.isValid(annotationTable));
            });
    }

    @Test
    public void shouldBeUnKnown() {
        Stream.of(
            get(getAnnotation("e")),
            get(getAnnotation("g")),
            get(getAnnotation("h")))
            .forEach(annotationTable -> {
                assertFalse(annotationTable.isEmpty());
                assertFalse(HasMainAnnotationInjectionValidator.isValid(annotationTable));
            });
    }

    @Test
    public void shouldBeInvalidBecauseHasTwoMainAnnotation() {
        Stream.of(
            get(getAnnotation("ab")),
            get(getAnnotation("ac")),
            get(getAnnotation("ad")))
            .forEach(annotationTable -> {
                assertFalse(annotationTable.isEmpty());
                assertFalse(HasMainAnnotationInjectionValidator.isValid(annotationTable));
            });
    }

    @Test
    public void shouldBeValidBecauseHasOnlyOneMainAnnotationAndSomeAccessory() {
        assertFalse(get(getAnnotation("ae")).isEmpty());
        assertTrue(HasMainAnnotationInjectionValidator.isValid(get(getAnnotation("ae"))));

        assertFalse(get(getAnnotation("ag")).isEmpty());
        assertTrue(HasMainAnnotationInjectionValidator.isValid(get(getAnnotation("ag"))));

        assertFalse(get(getAnnotation("aj")).isEmpty());
        assertTrue(HasMainAnnotationInjectionValidator.isValid(get(getAnnotation("aj"))));
    }

    @Test
    public void shouldBeInvalidBecauseDoNotHaveAnyAnnotation() {
        Stream.of(get(getAnnotation("x"))).forEach(annotationTable -> {
            assertTrue(annotationTable.isEmpty());
            assertFalse(HasMainAnnotationInjectionValidator.isValid(annotationTable));
        });
    }

    private Map<Annotation, Optional<InjectionAnnotation>> get(Annotation[] as) {
        return new HasMainAnnotationInjectionValidator().apply(as);
    }

    private Annotation[] getAnnotation(final String name) {
        return getMethod(name).getParameters()[0].getAnnotations();
    }

    private Method getMethod(final String name) {
        try {
            return TestClass.class.getMethod(name, Object.class);
        } catch (NoSuchMethodException e) {
            throw new AssertionError("Method not exist, name: " + name, e);
        }
    }


    static class TestClass {

        public void a(@Item Object o) {
        }

        public void b(@Source Object o) {
        }

        public void c(@UIComponent("") Object o) {
        }

        public void d(@Value("") Object o) {
        }

        public void e(@javax.annotation.Nonnull Object o) {
        }

        public void g(@NotNull Object o) {
        }

        public void h(@Mock Object o) {
        }

        public void x(Object o) {
        }

        public void ab(@Item @Source Object o) {
        }

        public void ac(@Item @UIComponent("") Object o) {
        }

        public void ad(@Item @Value("") Object o) {
        }

        public void ae(@Item @javax.annotation.Nonnull Object o) {
        }

        public void ag(@Item @NotNull Object o) {
        }

        public void aj(@Item @Mock Object o) {
        }
    }
}
