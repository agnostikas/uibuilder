/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.statenodemanager;

import com.vaadin.flow.internal.StateNode;
import com.vaadin.flow.internal.nodefeature.BasicTypeValue;
import com.vaadin.flow.internal.nodefeature.ElementPropertyMap;
import com.vaadin.flow.internal.nodefeature.ModelList;
import io.devbench.uibuilder.core.controllerbean.uiproperty.ItemConverter;
import io.devbench.uibuilder.core.controllerbean.uiproperty.StringPropertyConverter;
import io.devbench.uibuilder.core.exceptions.StateNodeBeanSynchronizationException;
import io.devbench.uibuilder.core.exceptions.StateNodeInvalidReferenceException;
import io.devbench.uibuilder.core.utils.reflection.ClassMetadata;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;


class StateNodeManagerTest {

    private StateNodeManager testObj;

    private BoundPojo instance;

    private ClassMetadata<BoundPojo> classMetadata;

    protected static Object getPropertyValueOf(Serializable stateNode, String propertyName) {
        if (stateNode instanceof StateNode) {
            Serializable property = ElementPropertyMap.getModel((StateNode) stateNode).getProperty(propertyName);
            if (property != null) {
                return property;
            } else {
                fail(String.format("The requested property '%s' is null in %s", propertyName, stateNode));
            }
        } else {
            fail("Passed statenode isn't a statenode: " + stateNode);
        }
        return new Object();
    }

    @BeforeEach
    void setup() {
        instance = new BoundPojo();
        classMetadata = ClassMetadata.ofClass(BoundPojo.class).withInstance(instance);
        testObj = new StateNodeManager(name -> classMetadata);
    }

    @Test
    @DisplayName("Should add binding path to state node manager")
    public void should_add_binding_path_to_state_node_manager() {
        testObj.addBindingPath("pojo.name");

        assertAll(
            () -> assertFalse(testObj.bindingNodes.isEmpty()),
            () -> assertTrue(testObj.bindingNodes.containsKey("pojo"))
        );
    }

    @Test
    @DisplayName("Should find the correct binding nodes for the specified path")
    public void should_find_the_correct_binding_nodes_for_the_specified_path() {
        instance.name = "test pojo";
        instance.selectedDetail = new Detail();
        instance.children = new ArrayList<>();
        instance.detailSet = new HashSet<>();

        addPathsToManager("pojo.name", "pojo.selectedDetail.description", "pojo.children", "pojo.detailSet");

        BindingNode nameBindingNode = testObj.getNodeForPath("pojo.name");
        BindingNode selectedDetailBindingNode = testObj.getNodeForPath("pojo.selectedDetail");
        BindingNode detailSetBindingNode = testObj.getNodeForPath("pojo.detailSet");
        BindingNode childrenBindingNode = testObj.getNodeForPath("pojo.children");

        assertAll(
            () -> assertNotNull(nameBindingNode),
            () -> assertNotNull(selectedDetailBindingNode),
            () -> assertNotNull(detailSetBindingNode),
            () -> assertNotNull(childrenBindingNode),
            () -> assertTrue(nameBindingNode instanceof SimpleNode),
            () -> assertTrue(selectedDetailBindingNode instanceof BeanNode),
            () -> assertTrue(detailSetBindingNode instanceof CollectionNode),
            () -> assertTrue(childrenBindingNode instanceof CollectionNode)
        );
    }

    @Test
    @DisplayName("Should find the correct binding nodes for path, even if the beans and collections are null")
    public void should_find_the_correct_binding_nodes_for_path_even_if_the_beans_and_collections_are_null() {
        addPathsToManager("pojo.name", "pojo.selectedDetail.description", "pojo.children", "pojo.detailSet");

        BindingNode nameBindingNode = testObj.getNodeForPath("pojo.name");
        BindingNode selectedDetailBindingNode = testObj.getNodeForPath("pojo.selectedDetail");
        BindingNode detailSetBindingNode = testObj.getNodeForPath("pojo.detailSet");
        BindingNode childrenBindingNode = testObj.getNodeForPath("pojo.children");

        assertAll(
            () -> assertNotNull(nameBindingNode),
            () -> assertNotNull(selectedDetailBindingNode),
            () -> assertNotNull(detailSetBindingNode),
            () -> assertNotNull(childrenBindingNode),
            () -> assertTrue(nameBindingNode instanceof SimpleNode),
            () -> assertTrue(selectedDetailBindingNode instanceof BeanNode),
            () -> assertTrue(detailSetBindingNode instanceof CollectionNode),
            () -> assertTrue(childrenBindingNode instanceof CollectionNode)
        );
    }

    @Test
    @DisplayName("Should populate the values of bound properties")
    public void should_populate_the_values_of_bound_properties() {
        instance.name = "test pojo";
        instance.selectedDetail = new Detail();
        instance.children = new ArrayList<>();
        instance.detailSet = new HashSet<>();

        addPathsToManager("pojo.name", "pojo.selectedDetail.description", "pojo.children", "pojo.detailSet");

        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        assertAll(
            () -> assertNotNull(propertyValues),
            () -> assertNotNull(propertyValues.get("pojo")),
            () -> assertEquals(StateNode.class, propertyValues.get("pojo").getClass()),
            () -> assertIterableEquals(
                Stream.of("name", "selectedDetail", "children", "detailSet").collect(Collectors.toSet()),
                ((StateNode) propertyValues.get("pojo")).getFeature(ElementPropertyMap.class).getPropertyNames().collect(Collectors.toSet())
            ),
            () -> {
                Serializable pojo = propertyValues.get("pojo");
                assertAll(
                    () -> assertEquals("test pojo", getPropertyValueOf(pojo, "name"), "name property value does not match"),
                    () -> assertEquals(StateNode.class,
                        getPropertyValueOf(pojo, "selectedDetail").getClass(), "selectedDetail property value isn't a state node"),
                    () -> assertEquals(StateNode.class,
                        getPropertyValueOf(pojo, "children").getClass(), "children property value isn't a state node"),
                    () -> assertTrue(((StateNode)
                        getPropertyValueOf(pojo, "children")).hasFeature(ModelList.class), "children property does not contain ModelList feature"),
                    () -> assertEquals(StateNode.class,
                        getPropertyValueOf(pojo, "detailSet").getClass(), "detailSet property value isn't a state node"),
                    () -> assertTrue(((StateNode)
                        getPropertyValueOf(pojo, "detailSet")).hasFeature(ModelList.class), "detailSet property does not contain ModelList feature")
                );
            }
        );
    }

    @Test
    @DisplayName("Should synchronize properties based on state node changes")
    public void should_synchronize_properties_based_on_state_node_changes() {
        instance.name = "test pojo";

        addPathsToManager("pojo.name");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();
        ((StateNode) propertyValues.get("pojo")).getFeature(ElementPropertyMap.class).setProperty("name", "changed value");

        testObj.synchronizeProperties();

        assertEquals("changed value", instance.name);
    }

    @Test
    @DisplayName("Should synchronize statenode based on property changes")
    public void should_synchronize_statenode_based_on_property_changes() {
        instance.name = "test pojo";

        addPathsToManager("pojo.name");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();
        instance.name = "changed value";

        testObj.synchronizeStateNodes();

        assertEquals("changed value", ((StateNode) propertyValues.get("pojo")).getFeature(ElementPropertyMap.class).getProperty("name"));
    }

    @Test
    @DisplayName("Should synchronize containing beans nullity as null value on lower level bindings")
    public void should_synchronize_containing_beans_nullity_as_null_value_on_lower_level_bindings() {
        instance.selectedDetail = new Detail();
        instance.selectedDetail.createDate = LocalDate.now();

        addPathsToManager("pojo.selectedDetail.createDate");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        instance.selectedDetail = null;

        testObj.synchronizeStateNodes();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode selectedDetailNode = (StateNode) ElementPropertyMap.getModel(pojoNode).getProperty("selectedDetail");
        Serializable createDateValue = ElementPropertyMap.getModel(selectedDetailNode).getProperty("createDate");

        assertNull(createDateValue);
    }

    @Test
    @DisplayName("Should handle containing bean value becoming null and later getting a new value even if there was a synchronization between those changes")
    public void should_handle_containing_bean_value_becoming_null_and_later_getting_a_new_value_even_if_there_was_a_synchronization_between_those_changes() {
        instance.selectedDetail = new Detail();
        instance.selectedDetail.description = "old value";

        addPathsToManager("pojo.selectedDetail.description");

        instance.selectedDetail = null;

        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        testObj.synchronizeStateNodes();

        instance.selectedDetail = new Detail();
        instance.selectedDetail.description = "new value";

        testObj.synchronizeStateNodes();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode selectedDetailNode = (StateNode) ElementPropertyMap.getModel(pojoNode).getProperty("selectedDetail");
        Serializable descriptionValue = ElementPropertyMap.getModel(selectedDetailNode).getProperty("description");

        assertEquals("new value", descriptionValue);
    }

    @Test
    @DisplayName("Should populate list values from the backend")
    public void should_populate_list_values_from_the_backend() {
        BoundPojo child1 = new BoundPojo();
        child1.name = "child1";
        instance.children = new ArrayList<>(Collections.singleton(child1));

        addPathsToManager("pojo.children");
        CollectionNode childrenNode = (CollectionNode) testObj.getNodeForPath("pojo.children");
        childrenNode.addCollectionPath("item.name");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode childrenStateNode = (StateNode) getPropertyValueOf(pojoNode, "children");

        assertAll(
            () -> assertNotNull(childrenStateNode),
            () -> assertTrue(childrenStateNode.hasFeature(ModelList.class)),
            () -> assertEquals(1, childrenStateNode.getFeature(ModelList.class).size()),
            () -> assertTrue(childrenStateNode.getFeature(ModelList.class).get(0).hasFeature(ElementPropertyMap.class)),
            () -> assertEquals("child1", childrenStateNode.getFeature(ModelList.class).get(0).getFeature(ElementPropertyMap.class).getProperty("name"))
        );
    }

    @Test
    @DisplayName("Should populate list with simple values in it from backend")
    public void should_populate_list_with_simple_values_in_it_from_backend() {
        instance.values = Arrays.asList("value1", "value2");

        StateNode valuesStateNode = createCollectionStateNodeWithSimpleElementBoundTo("pojo", "values");

        assertAll(
            () -> assertNotNull(valuesStateNode),
            () -> assertTrue(valuesStateNode.hasFeature(ModelList.class)),
            () -> assertEquals(2, valuesStateNode.getFeature(ModelList.class).size()),
            () -> assertTrue(valuesStateNode.getFeature(ModelList.class).get(0).hasFeature(BasicTypeValue.class)),
            () -> assertTrue(valuesStateNode.getFeature(ModelList.class).get(1).hasFeature(BasicTypeValue.class)),
            () -> assertEquals("value1", valuesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue()),
            () -> assertEquals("value2", valuesStateNode.getFeature(ModelList.class).get(1).getFeature(BasicTypeValue.class).getValue())
        );
    }

    @Test
    @DisplayName("Should synchronize list value changes from the backend")
    public void should_synchronize_list_value_changes_from_the_backend() {
        BoundPojo child1 = new BoundPojo();
        child1.name = "child1";
        instance.children = new ArrayList<>(Collections.singleton(child1));

        addPathsToManager("pojo.children");
        CollectionNode childrenNode = (CollectionNode) testObj.getNodeForPath("pojo.children");
        childrenNode.addCollectionPath("item.name");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        BoundPojo child2 = new BoundPojo();
        child2.name = "child2";

        instance.children.add(child2);

        testObj.synchronizeStateNodes();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode childrenStateNode = (StateNode) getPropertyValueOf(pojoNode, "children");

        assertAll(
            () -> assertNotNull(childrenStateNode),
            () -> assertTrue(childrenStateNode.hasFeature(ModelList.class)),
            () -> assertEquals(2, childrenStateNode.getFeature(ModelList.class).size()),
            () -> assertEquals("child1", childrenStateNode.getFeature(ModelList.class).get(0).getFeature(ElementPropertyMap.class).getProperty("name")),
            () -> assertEquals("child2", childrenStateNode.getFeature(ModelList.class).get(1).getFeature(ElementPropertyMap.class).getProperty("name"))
        );
    }

    @Test
    @DisplayName("Should populate values from set")
    public void should_populate_values_from_set() {
        Detail detail1 = new Detail();
        detail1.description = "detail1";
        Detail detail2 = new Detail();
        detail2.description = "detail2";
        instance.detailSet = new HashSet<>(Arrays.asList(detail1, detail2));

        addPathsToManager("pojo.detailSet");
        CollectionNode detailSetNode = (CollectionNode) testObj.getNodeForPath("pojo.detailSet");
        detailSetNode.addCollectionPath("item.description");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode detailSetStateNode = (StateNode) getPropertyValueOf(pojoNode, "detailSet");

        assertAll(
            () -> assertNotNull(detailSetStateNode),
            () -> assertTrue(detailSetStateNode.hasFeature(ModelList.class)),
            () -> assertEquals(2, detailSetStateNode.getFeature(ModelList.class).size()),
            () -> {
                List<String> correctDetailValues = Arrays.asList("detail1", "detail2");

                ModelList modelList = detailSetStateNode.getFeature(ModelList.class);
                List<Serializable> descriptionValues = IntStream.range(0, modelList.size())
                    .boxed()
                    .map(modelList::get)
                    .map(ElementPropertyMap::getModel)
                    .map(propertyMap -> propertyMap.getProperty("description"))
                    .collect(Collectors.toList());
                assertAll(
                    () -> assertTrue(correctDetailValues.containsAll(descriptionValues)),
                    () -> assertTrue(descriptionValues.containsAll(correctDetailValues))
                );
            }
        );
    }

    @Test
    @DisplayName("Should set the value on the backend that changed on the frontend in a list, if its a list containing simple elements")
    public void should_set_the_value_on_the_backend_that_changed_on_the_frontend_in_a_list_if_its_a_list_containing_simple_elements() {
        instance.values = Arrays.asList("value1", "value2");

        StateNode valuesStateNode = createCollectionStateNodeWithSimpleElementBoundTo("pojo", "values");

        valuesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).setValue("changed value1");

        testObj.synchronizeProperties();

        assertAll(
            () -> assertEquals("changed value1", valuesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue()),
            () -> assertEquals("value2", valuesStateNode.getFeature(ModelList.class).get(1).getFeature(BasicTypeValue.class).getValue())
        );
    }

    @Test
    @DisplayName("Should handle collections with nulls in them when populated to the frontend")
    public void should_handle_collections_with_nulls_in_them_when_populated_to_the_frontend() {
        instance.values = Arrays.asList("value1", "value2", null, null);

        StateNode valuesStateNode = createCollectionStateNodeWithSimpleElementBoundTo("pojo", "values");

        assertAll(
            () -> assertEquals("value1", valuesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue()),
            () -> assertEquals("value2", valuesStateNode.getFeature(ModelList.class).get(1).getFeature(BasicTypeValue.class).getValue()),
            () -> assertNull(valuesStateNode.getFeature(ModelList.class).get(2).getFeature(BasicTypeValue.class).getValue()),
            () -> assertNull(valuesStateNode.getFeature(ModelList.class).get(3).getFeature(BasicTypeValue.class).getValue())
        );
    }

    @Test
    @DisplayName("Should indicate error when added or removed a collection on the frontend")
    public void should_indicate_error_when_added_or_removed_a_collection_on_the_frontend() {
        instance.values = Arrays.asList("value1", "value2");

        StateNode valuesStateNode = createCollectionStateNodeWithSimpleElementBoundTo("pojo", "values");

        StateNode newStateNode = new StateNode(BasicTypeValue.class);
        newStateNode.getFeature(BasicTypeValue.class).setValue("new value");

        valuesStateNode.getFeature(ModelList.class).add(newStateNode);

        List<BindingNodeSyncError> addBindingNodeSyncErrors = testObj.synchronizeProperties();

        valuesStateNode.getFeature(ModelList.class).remove(2);
        valuesStateNode.getFeature(ModelList.class).remove(1);

        List<BindingNodeSyncError> removeBindingNodeSyncErrors = testObj.synchronizeProperties();

        assertAll(
            () -> assertTrue(addBindingNodeSyncErrors.get(0).getException() instanceof StateNodeBeanSynchronizationException),
            () -> assertTrue(removeBindingNodeSyncErrors.get(0).getException() instanceof StateNodeBeanSynchronizationException),
            () -> assertEquals("pojo.values", addBindingNodeSyncErrors.get(0).getPropertyPath()),
            () -> assertEquals("pojo.values", removeBindingNodeSyncErrors.get(0).getPropertyPath()),
            () -> assertEquals(
                "Bound collections not allowed to be modified directly on the client side",
                addBindingNodeSyncErrors.get(0).getException().getMessage()
            ),
            () -> assertEquals(
                "Bound collections not allowed to be modified directly on the client side",
                removeBindingNodeSyncErrors.get(0).getException().getMessage()
            )
        );
    }

    @Test
    @DisplayName("Should handle collections with implicitly convertible type arguments")
    public void should_handle_collections_with_implicitly_convertible_type_arguments() {
        instance.changeDates = Collections.singletonList(LocalDate.of(1970, 1, 1));

        StateNode changeDatesStateNode = createCollectionStateNodeWithSimpleElementBoundTo("pojo", "changeDates");

        assertEquals("1970-01-01", changeDatesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue());
    }

    @Test
    @DisplayName("Should handle collections with annotation driven convertible items in them")
    public void should_handle_collections_with_annotation_driven_convertible_items_in_them() {
        UUID uuid1 = UUID.fromString("cffe7278-54a3-4ba8-880e-8487ee5e69b0");
        instance.ids = new ArrayList<>(Collections.singletonList(new IdHolder(uuid1)));

        StateNode idsStateNode = createCollectionStateNodeWithSimpleElementBoundTo("pojo", "ids");

        StateNode uuid1StateNode = idsStateNode.getFeature(ModelList.class).get(0);
        String changedUuidString = "87c063a4-8121-42e3-97cf-fb703743d4a5";
        uuid1StateNode.getFeature(BasicTypeValue.class).setValue(changedUuidString);

        testObj.synchronizeProperties();

        assertEquals(UUID.fromString(changedUuidString), instance.ids.get(0).id);
    }

    @Test
    @DisplayName("Should handle collections containing collections")
    public void should_handle_collections_containing_collections() {
        instance.nameCandidates = new ArrayList<>(Collections.singletonList(Collections.singletonList("test1")));

        addPathsToManager("pojo.nameCandidates");
        CollectionNode topCollectionNode = (CollectionNode) testObj.getNodeForPath("pojo.nameCandidates");
        topCollectionNode.addCollectionPath("item");
        CollectionNode prototypeNode = (CollectionNode) topCollectionNode.getPrototypeNode();
        prototypeNode.addCollectionPath("item");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode nameCandidatesStateNode = (StateNode) getPropertyValueOf(pojoNode, "nameCandidates");
        StateNode secondLevelCollectionHolderStateNode = nameCandidatesStateNode.getFeature(ModelList.class).get(0);

        assertAll(
            () -> assertNotNull(secondLevelCollectionHolderStateNode),
            () -> assertTrue(secondLevelCollectionHolderStateNode.hasFeature(ModelList.class)),
            () -> assertEquals("test1", secondLevelCollectionHolderStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue())
        );
    }

    @Test
    @DisplayName("Should handle collections containing items with collection properties")
    public void should_handle_collections_containing_items_with_collection_properties() {
        Detail detail = new Detail();
        detail.values = Collections.singletonList("test value");
        instance.detailSet = Collections.singleton(detail);

        addPathsToManager("pojo.detailSet");
        CollectionNode topCollectionNode = (CollectionNode) testObj.getNodeForPath("pojo.detailSet");
        topCollectionNode.addCollectionPath("item.values");
        CollectionNode prototypeNode = (CollectionNode) topCollectionNode.getNodeAt(new String[]{"item", "values"});
        prototypeNode.addCollectionPath("item");

        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode detailSetStateNode = (StateNode) getPropertyValueOf(pojoNode, "detailSet");
        StateNode itemStateNode = detailSetStateNode.getFeature(ModelList.class).get(0);
        StateNode valuesStateNode = (StateNode) itemStateNode.getFeature(ElementPropertyMap.class).getProperty("values");
        Serializable value = valuesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue();

        assertEquals("test value", value);
    }

    @Test
    @DisplayName("Should handle collections with multi-level item bindings")
    public void should_handle_collections_with_multi_level_item_bindings() {
        BoundPojo child = new BoundPojo();
        child.selectedDetail = new Detail();
        child.selectedDetail.description = "test description";
        instance.children = Collections.singletonList(child);

        addPathsToManager("pojo.children");
        CollectionNode childrenNode = (CollectionNode) testObj.getNodeForPath("pojo.children");
        childrenNode.addCollectionPath("item.selectedDetail.description");

        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode childrenStateNode = (StateNode) getPropertyValueOf(pojoNode, "children");
        StateNode selectedDetailStateNode = (StateNode) childrenStateNode
            .getFeature(ModelList.class).get(0)
            .getFeature(ElementPropertyMap.class)
            .getProperty("selectedDetail");
        Serializable description = selectedDetailStateNode.getFeature(ElementPropertyMap.class).getProperty("description");

        assertEquals("test description", description);
    }

    @Test
    @DisplayName("Should throw exception if parameter type in collection is a generic type")
    public void should_throw_exception_if_parameter_type_in_collection_is_a_generic_type() {
        ClassWithParameterType<String> testInstance = new ClassWithParameterType<>();
        testInstance.list = new ArrayList<>();
        ClassMetadata<ClassWithParameterType> classMetadata = ClassMetadata.ofClass(ClassWithParameterType.class).withInstance(testInstance);
        testObj = new StateNodeManager(name -> classMetadata);

        StateNodeInvalidReferenceException exception = assertThrows(StateNodeInvalidReferenceException.class, () -> testObj.addBindingPath("pojo.list"));
        assertEquals("Actual parameter type is not class or parameterized type in referenced collection, type parameters: T", exception.getMessage());
    }

    @Test
    @DisplayName("Should throw exception if parameter type in collection is wildcard")
    public void should_throw_exception_if_parameter_type_in_collection_is_wildcard() {
        ClassWithWildcardListInIt testInstance = new ClassWithWildcardListInIt();
        testInstance.list = new ArrayList<>();
        ClassMetadata<ClassWithWildcardListInIt> classMetadata = ClassMetadata.ofClass(ClassWithWildcardListInIt.class).withInstance(testInstance);
        testObj = new StateNodeManager(name -> classMetadata);

        StateNodeInvalidReferenceException exception = assertThrows(StateNodeInvalidReferenceException.class, () -> testObj.addBindingPath("pojo.list"));
        assertEquals("Actual parameter type is not class or parameterized type in referenced collection, type parameters: ?", exception.getMessage());
    }

    @Test
    @DisplayName("Should handle null value collections as empty collections")
    public void should_handle_null_value_collections_as_empty_collections() {
        instance.detailSet = null;
        addPathsToManager("pojo.detailSet");
        CollectionNode detailSetNode = (CollectionNode) testObj.getNodeForPath("pojo.detailSet");
        detailSetNode.addCollectionPath("item.description");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode detailSetStateNode = (StateNode) getPropertyValueOf(pojoNode, "detailSet");

        assertEquals(0, detailSetStateNode.getFeature(ModelList.class).size());

        Detail detail = new Detail();
        detail.description = "detail";
        instance.detailSet = new HashSet<>(Collections.singletonList(detail));

        testObj.synchronizeStateNodes();

        assertAll(
            () -> assertEquals(1, detailSetStateNode.getFeature(ModelList.class).size()),
            () -> assertEquals("detail", detailSetStateNode.getFeature(ModelList.class).get(0).getFeature(ElementPropertyMap.class).getProperty("description"))
        );
    }

    @Test
    @DisplayName("Should handle null beans when contains collection in them as if the collection would be an empty collection")
    public void should_handle_null_beans_when_contains_collection_in_them_as_if_the_collection_would_be_an_empty_collection() {
        instance.selectedDetail = null;

        addPathsToManager("pojo.selectedDetail.values");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode selectedDetailStateNode = (StateNode) getPropertyValueOf(pojoNode, "selectedDetail");
        StateNode valuesStateNode = (StateNode) getPropertyValueOf(selectedDetailStateNode, "values");

        assertEquals(0, valuesStateNode.getFeature(ModelList.class).size());

        instance.selectedDetail = new Detail();
        instance.selectedDetail.values = new ArrayList<>(Collections.singletonList("value1"));

        testObj.synchronizeStateNodes();

        assertAll(
            () -> assertEquals(1, valuesStateNode.getFeature(ModelList.class).size()),
            () -> assertEquals("value1", valuesStateNode.getFeature(ModelList.class).get(0).getFeature(BasicTypeValue.class).getValue())
        );
    }

    @Test
    @DisplayName("Should collect the registered top level property names")
    public void should_collect_the_registered_top_level_property_names() {
        BoundPojo pojo1 = new BoundPojo();
        BoundPojo pojo2 = new BoundPojo();
        BoundPojo pojo3 = new BoundPojo();

        ClassMetadata<BoundPojo> pojo1ClassMeta = ClassMetadata.ofClass(BoundPojo.class).withInstance(pojo1);
        ClassMetadata<BoundPojo> pojo2ClassMeta = ClassMetadata.ofClass(BoundPojo.class).withInstance(pojo2);
        ClassMetadata<BoundPojo> pojo3ClassMeta = ClassMetadata.ofClass(BoundPojo.class).withInstance(pojo3);

        testObj = new StateNodeManager(name -> {
            switch (name) {
                case "pojo1":
                    return pojo1ClassMeta;
                case "pojo2":
                    return pojo2ClassMeta;
                default:
                    return pojo3ClassMeta;
            }
        });

        testObj.addBindingPath("pojo1.name");
        testObj.addBindingPath("pojo2.values");

        Set<String> propertyNames = testObj.getPropertyNames();

        assertAll(
            () -> assertEquals(2, propertyNames.size()),
            () -> assertTrue(propertyNames.containsAll(Arrays.asList("pojo1", "pojo2")))
        );
    }

    @Test
    @DisplayName("Should get actual values from state node converted to the original type")
    public void should_get_actual_values_from_state_node_converted_to_the_original_type() {
        instance.selectedDetail = new Detail();
        instance.selectedDetail.createDate = LocalDate.of(1970, 1, 1);

        addPathsToManager("pojo.selectedDetail.createDate");

        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();
        StateNode pojoStateNode = (StateNode) propertyValues.get("pojo");
        StateNode selectedDetailStateNode = (StateNode) getPropertyValueOf(pojoStateNode, "selectedDetail");

        Object selectedDetailAsObject = testObj.getActualValueFromStateNode(selectedDetailStateNode);

        assertAll(
            () -> assertEquals(Detail.class, selectedDetailAsObject.getClass()),
            () -> assertEquals(LocalDate.of(1970, 1, 1), ((Detail) selectedDetailAsObject).getCreateDate())
        );
    }

    @Test
    @DisplayName("Should not try to synchronize simple elements in beans if the bean is null, or its property has no container instance, and the statenode property didn't changed")
    public void should_not_try_to_synchronize_simple_elements_in_beans_if_the_bean_is_null_or_its_property_has_no_container_instance() {
        instance.selectedDetail = new Detail();
        instance.selectedDetail.createDate = LocalDate.now();

        addPathsToManager("pojo.selectedDetail.createDate");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        instance.selectedDetail = null;

        testObj.synchronizeStateNodes();

        StateNode pojoNode = (StateNode) propertyValues.get("pojo");
        StateNode selectedDetailNode = (StateNode) ElementPropertyMap.getModel(pojoNode).getProperty("selectedDetail");
        ElementPropertyMap.getModel(selectedDetailNode).setProperty("createDate", null);

        testObj.synchronizeProperties(); //no exceptions thrown

        assertNull(instance.selectedDetail);
    }

    @Test
    public void should_converty_null_parent_to_empty() {
        BoundPojo myInstance = new BoundPojo();
        myInstance.selectedDetail = new Detail();
        ClassMetadata myClassMetadata = ClassMetadata.ofClass(BoundPojo.class).withInstance(myInstance);
        final StateNodeManager stateNodeManager = new StateNodeManager(name -> myClassMetadata);
        stateNodeManager.addBindingPath("pojo.selectedDetail.idHolder.id");
        Map<String, Serializable> propertyValues = stateNodeManager.populatePropertyValues();
        final BindingNode nodeForPath = stateNodeManager.getNodeForPath("pojo.selectedDetail.idHolder.id");
        final Optional<Object> propertyValue = nodeForPath.classMetadata.getPropertyValue("pojo.selectedDetail.idHolder.id");
        stateNodeManager.synchronizeStateNodes();
        assertFalse(propertyValue.isPresent());
        assertNotNull(nodeForPath);
    }

    private StateNode createCollectionStateNodeWithSimpleElementBoundTo(String bind1, String bind2) {
        String fullBind = bind1 + "." + bind2;
        addPathsToManager(fullBind);
        CollectionNode valuesNode = (CollectionNode) testObj.getNodeForPath(fullBind);
        valuesNode.addCollectionPath("item");
        Map<String, Serializable> propertyValues = testObj.populatePropertyValues();

        StateNode pojoNode = (StateNode) propertyValues.get(bind1);
        return (StateNode) getPropertyValueOf(pojoNode, bind2);
    }

    private void addPathsToManager(String... bindings) {
        Arrays.stream(bindings).forEach(testObj::addBindingPath);
    }


    @Data
    public static class ClassWithWildcardListInIt {
        private List<?> list;
    }

    @Data
    public static class ClassWithParameterType<T> {
        private List<T> list;
    }

    @Data
    public static class BoundPojo {

        private String name;

        private Detail selectedDetail;

        private List<BoundPojo> children;

        private Set<Detail> detailSet;

        private List<String> values;

        private List<LocalDate> changeDates;

        @ItemConverter(IdHolderConverter.class)
        private List<IdHolder> ids;

        private List<List<String>> nameCandidates;

    }

    @Data
    @AllArgsConstructor
    public static class IdHolder {
        private UUID id;
    }

    public static class IdHolderConverter implements StringPropertyConverter<IdHolder> {

        @Override
        public String convertTo(IdHolder value) {
            return value.id.toString();
        }

        @Override
        public IdHolder apply(@NotNull String value) {
            return new IdHolder(UUID.fromString(value));
        }
    }

    @Data
    public static class Detail {

        private String description;

        private LocalDate createDate;

        private List<String> values;

        private IdHolder idHolder;
    }

}
