/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.member;

import io.devbench.uibuilder.api.member.scanner.FindByAnnotationFunction;
import io.devbench.uibuilder.api.member.scanner.FindClassesBySuperTypeFunction;
import io.devbench.uibuilder.api.member.scanner.MemberScanner;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * This has to be in the core module, because the SingletonManager needs a implementation
 * to work, and the test module has one, but the test module depends on the api module.
 * So this test cannot be placed in the api module.
 */
@ExtendWith({MockitoExtension.class, SingletonProviderForTestsExtension.class})
public class MemberScannerTest {

    @Mock
    private FindByAnnotationFunction findByAnnotationFunction;

    @Mock
    private FindClassesBySuperTypeFunction findBySuperTypeFunction;

    private MemberScanner testObj;

    @BeforeEach
    private void setup() {
        testObj = MemberScanner.registerToActiveContext(findByAnnotationFunction, findBySuperTypeFunction);
    }

    @Test
    @DisplayName("Should delegate find by annotation requests to the function set during the registration of the instance")
    public void should_delegate_find_by_annotation_requests_to_the_function_set_during_the_registration_of_the_instance() {
        Set mockSet = mock(Set.class);
        when(findByAnnotationFunction.apply(Test.class, false)).thenReturn(mockSet);

        Set<Class<?>> result = testObj.findClassesByAnnotation(Test.class);

        assertAll(
            () -> verify(findByAnnotationFunction).apply(Test.class, false),
            () -> assertSame(mockSet, result)
        );
    }

    @Test
    @DisplayName("Should delegate find classes or interfaces by annotation requests to the registered function")
    public void should_delegate_find_classes_or_interfaces_by_annotation_requests_to_the_registered_function() {
        Set mockSet = mock(Set.class);
        when(findByAnnotationFunction.apply(Test.class, true)).thenReturn(mockSet);

        Set<Class<?>> result = testObj.findClassesOrInterfacesByAnnotation(Test.class);

        assertAll(
            () -> verify(findByAnnotationFunction).apply(Test.class, true),
            () -> assertSame(mockSet, result)
        );
    }

    @Test
    @DisplayName("Should delegate find by super type to the function set during the registration of the instance")
    public void should_delegate_find_by_super_type_to_the_function_set_during_the_registration_of_the_instance() {
        Set<Class<? extends Number>> set = Collections.singleton(Integer.class);
        when(findBySuperTypeFunction.apply(Number.class, false)).thenReturn(set);

        Set<Class<? extends Number>> result = testObj.findClassesBySuperType(Number.class);

        assertAll(
            () -> verify(findBySuperTypeFunction).apply(Number.class, false),
            () -> assertSame(set, result)
        );
    }

    @Test
    @DisplayName("Should delecate find classes or interfaces by supertype request, with interfaces allowed to the registered function")
    public void should_delecate_find_classes_or_interfaces_by_supertype_request_with_interfaces_allowed_to_the_registered_function() {
        Set<Class<? extends Iterable>> set = Collections.singleton(Collection.class);
        when(findBySuperTypeFunction.apply(Iterable.class, true)).thenReturn(set);

        Set<Class<? extends Iterable>> result = testObj.findClassesOrInterfacesBySuperType(Iterable.class);

        assertAll(
            () -> verify(findBySuperTypeFunction).apply(Iterable.class, true),
            () -> assertSame(set, result)
        );
    }

}
