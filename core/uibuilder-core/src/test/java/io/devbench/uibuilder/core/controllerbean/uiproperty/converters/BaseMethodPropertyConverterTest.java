/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty.converters;

import org.junit.jupiter.api.Test;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class BaseMethodPropertyConverterTest {

    @Test
    public void method_handle_corrector_should_return_the_method_handle_from_the_inherited_class() throws NoSuchMethodException, IllegalAccessException {
        class TestClass {
            public void foobar(String s) {
            }
        }

        class InheritedClass extends TestClass {
            public void foobar(String s) {
            }
        }

        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        final MethodHandle handleFromBaseClass = lookup.unreflect(TestClass.class.getDeclaredMethod("foobar", String.class));
        final MethodHandle handleFromInheritedClass = lookup.unreflect(InheritedClass.class.getDeclaredMethod("foobar", String.class));
        final MethodHandle chosenOne = Stream
            .of(handleFromBaseClass, handleFromInheritedClass)
            .collect(BaseMethodPropertyConverter.methodCollector());

        assertEquals(handleFromInheritedClass, chosenOne);
    }

    @Test
    public void method_handle_corrector_should_return_the_method_handle_with_the_most_specific_parameter()
        throws NoSuchMethodException, IllegalAccessException {

        class TestClass {
            public void foobar(Number number) {
            }

            public void foobar(Integer integer) {
            }
        }

        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        final MethodHandle handleWithGenericParameter = lookup.unreflect(TestClass.class.getDeclaredMethod("foobar", Number.class));
        final MethodHandle handleWithMoreSpecificParameter = lookup.unreflect(TestClass.class.getDeclaredMethod("foobar", Integer.class));
        final MethodHandle chosenOne = Stream
            .of(handleWithGenericParameter, handleWithMoreSpecificParameter, handleWithGenericParameter)
            .collect(BaseMethodPropertyConverter.methodCollector());

        assertEquals(handleWithMoreSpecificParameter, chosenOne);
    }

}
