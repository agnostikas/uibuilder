/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.controllerbean.uiproperty.converters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static org.junit.jupiter.api.Assertions.*;

class LocalDateTimePropertyConverterTest {

    private final LocalDateTimePropertyConverter testObj = new LocalDateTimePropertyConverter();

    @Test
    @DisplayName("Should convert local date time string to object")
    void test_should_convert_local_date_time_string_to_object() {
        LocalDateTime localDateTime = testObj.convertFrom("2021-04-07T11:39:37.123456");
        assertNotNull(localDateTime);

        assertEquals(37, localDateTime.getSecond());
        assertEquals(123456000, localDateTime.getNano());
    }

    @Test
    @DisplayName("Should convert local date time object to string")
    void test_should_convert_local_date_time_object_to_string() {
        LocalDateTime localDateTime = LocalDateTime.parse("2021-04-07T11:39:37.123456", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        String localDateTimeString = testObj.convertTo(localDateTime);

        assertNotNull(localDateTimeString);
        assertEquals("2021-04-07T11:39:37.123", localDateTimeString);
    }

    @Test
    @DisplayName("should return null if input parameter is null")
    void test_should_return_null_if_input_parameter_is_null() {
        assertNull(testObj.convertFrom(null));
        assertNull(testObj.convertTo(null));
    }

}
