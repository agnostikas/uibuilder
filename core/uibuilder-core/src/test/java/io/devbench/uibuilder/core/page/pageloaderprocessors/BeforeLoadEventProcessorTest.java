/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.core.page.pageloaderprocessors;

import io.devbench.uibuilder.core.controllerbean.ControllerBeanManager;
import io.devbench.uibuilder.core.controllerbean.UIEventHandlerContext;
import io.devbench.uibuilder.core.page.PageLoaderContext;
import io.devbench.uibuilder.test.annotations.LoadElement;
import io.devbench.uibuilder.test.extensions.JsoupExtension;
import io.devbench.uibuilder.test.extensions.MockitoExtension;
import io.devbench.uibuilder.test.singleton.SingletonInstance;
import io.devbench.uibuilder.test.singleton.SingletonProviderForTestsExtension;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, JsoupExtension.class, SingletonProviderForTestsExtension.class})
class BeforeLoadEventProcessorTest {

    @LoadElement(value = "/page-with-listeners.html", id = "page")
    private Element pageElement;

    @Mock
    @SingletonInstance(ControllerBeanManager.class)
    private ControllerBeanManager controllerBeanManager;

    @Mock
    private UIEventHandlerContext eventHandlerContext;

    private BeforeLoadEventProcessor testObj;

    private PageLoaderContext context;

    @BeforeEach
    private void setup() {
        testObj = new BeforeLoadEventProcessor();
        context = new PageLoaderContext(null, null);
        context.setPageElement(pageElement);

        when(controllerBeanManager.getEventHandlerContext("testBean::beforeLoad")).thenReturn(Optional.of(eventHandlerContext));
    }

    @Test
    @DisplayName("Should read on-before-render attribute and call controller bean event handler accordingly")
    public void should_read_on_before_render_attribute_and_call_controller_bean_event_handler_accordingly() {
        testObj.process(context);

        verify(controllerBeanManager).getEventHandlerContext("testBean::beforeLoad");
        verify(eventHandlerContext).callEventHandlerWithItem(null);
    }
}
