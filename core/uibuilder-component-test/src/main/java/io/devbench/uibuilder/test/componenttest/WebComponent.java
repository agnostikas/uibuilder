/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.componenttest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.Map;

public class WebComponent {
    private final RemoteWebDriver driver;
    private final WebElement element;

    public WebComponent(RemoteWebDriver driver, WebElement element) {
        this.driver = driver;
        this.element = element;
    }

    public WebComponent(RemoteWebDriver driver, By by) {
        this.driver = driver;
        this.element = driver.findElement(by);
    }

    public void scrollIntoView() {
        driver.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    @SuppressWarnings("unchecked")
    public Map<String, WebElement> getShadowDomElements() {
        return (Map<String, WebElement>) driver.executeScript("return arguments[0].$", element);
    }

    public WebElement getShadowDomElementByCssSelector(String selector) {
        return (WebElement) driver.executeScript("return arguments[0].shadowRoot.querySelector(arguments[1])", element, selector);
    }

    public WebElement getElement() {
        return element;
    }
}
