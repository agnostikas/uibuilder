/*
 *
 * Copyright © 2018 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.uibuilder.test.componenttest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class UIbuilderMenuItem extends WebComponent {
    public UIbuilderMenuItem(RemoteWebDriver driver, WebElement element) {
        super(driver, element);
    }

    public UIbuilderMenuItem(RemoteWebDriver driver, By by) {
        super(driver, by);
    }

    public WebElement getButton() {
        return getShadowDomElementByCssSelector("*[part='button']");
    }
}
